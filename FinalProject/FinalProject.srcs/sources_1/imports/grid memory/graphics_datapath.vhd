----------------------------------------------------------------------------------
-- Name:	Jacob Dunn
-- Date:	Spring 2022
-- File:    graphics_datapath.vhd
-- HW:	    Final Project
-- Pupr:	Checkers  
--
-- Doc:	C2C Carrig and C2C Bean assisted with various parts of the project
-- 	
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.NUMERIC_STD.ALL;
library UNIMACRO;
use UNIMACRO.vcomponents.all;
use work.GraphicsParts.all;		
-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity graphics_datapath is
    Port ( clk : in  STD_LOGIC;
           reset_n : in  STD_LOGIC;  
           ac_mclk : out STD_LOGIC;
           ac_adc_sdata : in STD_LOGIC;
           ac_dac_sdata : out STD_LOGIC;
           ac_bclk : out STD_LOGIC;
           ac_lrclk : out STD_LOGIC;
           scl : inout STD_LOGIC;
           sda : inout STD_LOGIC;   
           Lbus_out, Rbus_out: out std_logic_vector(17 downto 0);
           exLbus, exRbus: in signed(15 downto 0);
           tmds : out  STD_LOGIC_VECTOR (3 downto 0);
           tmdsb : out  STD_LOGIC_VECTOR (3 downto 0);
		   fsmCol: in std_logic_vector(5 downto 0);
           fsmRow: in std_logic_vector(4 downto 0);
           fsmWen : in std_logic;
           fsmData: in std_logic_vector(15 downto 0);
           fsmData2 : in std_logic_vector(15 downto 0);
           exCol : in  STD_LOGIC_VECTOR (5 downto 0);
           exRow : in  STD_LOGIC_VECTOR (4 downto 0);
           exWen, exSel : in  STD_LOGIC;
           exData : in  STD_LOGIC_VECTOR (15 downto 0);
           btn : in std_logic_vector(4 downto 0);
           cursorRow, cursorCol : out unsigned(2 downto 0);
           sw : in std_logic_vector(7 downto 0);
           ch1, ch2 : out std_logic_vector(15 downto 0);
           base, interp : in signed(15 downto 0) );
end graphics_datapath;

architecture Behavioral of graphics_datapath is
	
    signal muxData, muxData2, data_word_in, data_word_in2, data_word_out, data_word_out2: std_logic_vector(15 downto 0); 
    signal read_BRAM0, read_BRAM1 : std_logic_vector(15 downto 0);
	signal muxWen, Wen0, Wen1 : std_logic;
	signal reset: std_logic;
	signal ch_1, ch_2 : std_logic_vector(15 downto 0);
	signal row, column: unsigned(9 downto 0);
	signal uMuxCol: unsigned(5 downto 0);
    signal uMuxRow: unsigned(4 downto 0);
	signal muxCol : std_logic_vector(5 downto 0);
	signal muxRow : std_logic_vector(4 downto 0);	
    signal graphic_write_addr, graphic_read_addr0, graphic_read_addr1, column_stdLogicVector, row_stdLogicVector: std_logic_vector(9 downto 0);
	signal v_synch : std_logic;
	signal cursor_Row, cursor_Col : unsigned(2 downto 0) := "100";
	signal old_button : std_logic_vector(4 downto 0);
	signal ready, ready_sig : std_logic := '1';
	signal L_bus_in, R_bus_in, L_bus_out, R_bus_out: std_logic_vector(17 downto 0);
    signal UnL_bus_out, UnR_bus_out, UnL_bus_out2, UnR_bus_out2: unsigned(17 downto 0);
    signal exLbus2, exRbus2 : signed(17 downto 0);


begin

	reset <= not reset_n;

    muxRow <= exRow when exSel = '1' else fsmRow;
    muxCol <= exCol when exSel = '1' else fsmCol;
    uMuxRow <= unsigned(muxRow);
    uMuxCol <= unsigned(muxCol);
    graphic_write_addr <= "0000" & std_logic_vector( uMuxRow(2 downto 0) & uMuxCol(2 downto 0));

    muxData <= exData when exSel = '1' else fsmData;
    data_word_in <= MuxData;       
    
    muxWen <= exWen when exSel = '1' else fsmWen;
      
    Wen0 <= '1' when (muxCol(5) = '0' and muxWen = '1') else '0';
    
    	                
    column_stdLogicVector <= std_logic_vector(column);
    row_stdLogicVector <= std_logic_vector(row);
    graphic_read_addr0 <= "0000" & std_logic_vector( row(7 downto 5) & column(7 downto 5));
    graphic_read_addr1 <= "0000" & std_logic_vector( cursor_Row & cursor_Col);
    
    
    data_word_out <= read_BRAM0;
    data_word_out2 <= read_BRAM1;
    
    ch_1 <= data_word_out;
    ch1 <= ch_1; --data for scopeface
    ch_2 <= data_word_out2;
    ch2 <= ch_2; --data for FSM
    
    cursorRow <= cursor_Row;
    cursorCol <= cursor_Col;
    
    exLbus2 <= base & "00";
    exRbus2 <= interp & "00";
    
    process (clk)
    begin
    if (rising_edge(clk)) then
        if reset_n = '0' then
        L_bus_in <= (others => '0');
        R_bus_in <= (others => '0');                
        elsif(ready = '1') then
        L_bus_in <= std_logic_vector(exLbus2); --input to audio codec
        R_bus_in <= std_logic_vector(exRbus2);
        UnL_bus_out2 <= UnL_bus_out;
        UnR_bus_out2 <= UnR_bus_out;
        end if;
    end if;
    end process;
    
    button : process (clk) --move cursor around screen
      begin        
        if rising_edge(clk) then
            if (reset_n = '0') then
                cursor_Row <= "100";
                cursor_Col <= "100";
            end if;
            
            if    ( (btn(0) = '1' ) and not (btn = old_button) ) then
                cursor_Row <= cursor_Row - 1;
            elsif ( (btn(1) = '1' ) and not (btn = old_button) ) then
                cursor_Col <= cursor_Col - 1;
            elsif ( (btn(2) = '1' ) and not (btn = old_button) ) then
                cursor_Row <= cursor_Row + 1;
            elsif ( (btn(3) = '1' ) and not (btn = old_button) ) then
                cursor_Col <= cursor_Col + 1;
            end if;
            old_button <= btn;
        end if;
    end process button;
    

	-----------------------------------------------------------------------------------------
    -- BRAM_SDP_MACRO: 
	--     Store enough samples to display at least one sweep of the monitor [640x480]
	--         The RAM has 18kb (kilo BITS).  So if each word is
	--	       16 bits wide, this means that we will have 1k words per BRAM and need 10 bits of address.
	--         We are encoding 2-bits per pixel, so a 16 bit word will contain 8 pixels.
	--         The lower 512 BRAM entries will make a 8 pixel wide column (8 columns),
	--         and the higher 512 BRAM entries will make the adjacent 8 pixel wide column (8 columns).
	--         Therefore, to fill all 640 columns, we will need 40 BRAMs.
	-----------------------------------------------------------------------------------------

	
	BRAM0 : BRAM_SDP_MACRO   -- 16 bits wide x 1024 entries = 16,384 bits, so inside 18Kb
		generic map (
            BRAM_SIZE => "18Kb",            -- Target BRAM, "18Kb" or "36Kb"
            DEVICE => "7SERIES",            -- Target device: "VIRTEX5", "VIRTEX6", "SPARTAN6", "7SERIES"
            DO_REG => 0,                    -- Optional output register (0 or 1)
            --INIT => X"000000000000000000",  -- Initial values on output port
            INIT_FILE => "NONE",            -- Initial values on output port
            WRITE_WIDTH => 16,              -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
            READ_WIDTH => 16,               -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
            SIM_COLLISION_CHECK => "NONE",  -- Collision check enable "ALL", "WARNING_ONLY", "GENERATE_X_ONLY" or "NONE"
            SRVAL => X"000000000000000000", -- Set/Reset value for port output
            -- Here is where you insert the INIT_xx declarations to specify the initial contents of the RAM
            INIT_00 => X"0001000000010000000100000001000000000001000000010000000100000001",
            INIT_01 => X"0000000000000000000000000000000000000001000000010000000100000001",
            INIT_02 => X"0002000000020000000200000002000000000000000000000000000000000000",
            INIT_03 => X"0002000000020000000200000002000000000002000000020000000200000002")
    port map (
            DO => read_BRAM0,    -- Output read data port, width defined by READ_WIDTH parameter
            RDADDR => graphic_read_addr0,-- Input address, width defined by port depth
            RDCLK => clk,                   -- 1-bit input clock
            RST => reset,                 -- active high reset
            RDEN => '1',                    -- read enable
            REGCE => '1',                   -- 1-bit input read output register enable
            DI => data_word_in,    --wrLmux,                   -- Input data port, width defined by WRITE_WIDTH parameter
            WE => "11",                     -- Input write enable, width defined by write port depth
            --WE => "00",                     -- Input write enable, width defined by write port depth
            WRADDR => graphic_write_addr,                -- Input write address, width defined by write port depth
            WRCLK => clk,                   -- 1-bit input write clock
            WREN => Wen0);              -- 1-bit input write port enable
            
    	BRAM1 : BRAM_SDP_MACRO   -- 16 bits wide x 1024 entries = 16,384 bits, so inside 18Kb
                generic map (
                    BRAM_SIZE => "18Kb",            -- Target BRAM, "18Kb" or "36Kb"
                    DEVICE => "7SERIES",            -- Target device: "VIRTEX5", "VIRTEX6", "SPARTAN6", "7SERIES"
                    DO_REG => 0,                    -- Optional output register (0 or 1)
                    --INIT => X"000000000000000000",  -- Initial values on output port
                    INIT_FILE => "NONE",            -- Initial values on output port
                    WRITE_WIDTH => 16,              -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
                    READ_WIDTH => 16,               -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
                    SIM_COLLISION_CHECK => "NONE",  -- Collision check enable "ALL", "WARNING_ONLY", "GENERATE_X_ONLY" or "NONE"
                    SRVAL => X"000000000000000000", -- Set/Reset value for port output
                    -- Here is where you insert the INIT_xx declarations to specify the initial contents of the RAM
                    INIT_00 => X"0001000000010000000100000001000000000001000000010000000100000001",
                    INIT_01 => X"0000000000000000000000000000000000000001000000010000000100000001",
                    INIT_02 => X"0002000000020000000200000002000000000000000000000000000000000000",
                    INIT_03 => X"0002000000020000000200000002000000000002000000020000000200000002")
            port map (
                    DO => read_BRAM1,    -- Output read data port, width defined by READ_WIDTH parameter
                    RDADDR => graphic_read_addr1,-- Input address, width defined by port depth
                    RDCLK => clk,                   -- 1-bit input clock
                    RST => reset,                 -- active high reset
                    RDEN => '1',                    -- read enable
                    REGCE => '1',                   -- 1-bit input read output register enable
                    DI => data_word_in,    --wrLmux,                   -- Input data port, width defined by WRITE_WIDTH parameter
                    WE => "11",                     -- Input write enable, width defined by write port depth
                    --WE => "00",                     -- Input write enable, width defined by write port depth
                    WRADDR => graphic_write_addr,                -- Input write address, width defined by write port depth
                    WRCLK => clk,                   -- 1-bit input write clock
                    WREN => Wen0);              -- 1-bit input write port enable

video_inst: video port map( 
		clk =>clk,
		reset_n => reset_n,
        tmds => tmds,
		tmdsb => tmdsb,
		row => row,
		column => column,
		ch1 => ch_1,
		ch1_enb => '1',
		ch2 => ch_2,
		ch2_enb => '1',
		v_synch => v_synch, 
		cursorRow => cursor_Row,
		cursorCol => cursor_Col);

wrapper_inst: Audio_Codec_Wrapper port map( 
        clk => clk,
        reset_n => reset_n,
        ac_mclk => ac_mclk,
        ac_adc_sdata => ac_adc_sdata,
        ac_dac_sdata => ac_dac_sdata,
        ac_bclk => ac_bclk,
        ac_lrclk => ac_lrclk,
        ready => ready,
        L_bus_in => L_bus_in,
        R_bus_in => R_bus_in,
        L_bus_out => open,
        R_bus_out => open,
        scl => scl,
        sda => sda,
        sim_live => '1');  

end Behavioral;

