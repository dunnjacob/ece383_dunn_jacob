----------------------------------------------------------------------------------
-- Name:	George York
-- Date:	Spring 2020
-- File:    graphics.vhd
-- HW:	    GRAPHIC MEMORY example: 2 bits per pixel, for 640x480 display, using 40 BRAMS
-- Pupr:	need to update!!!!.  
--
-- Doc:	Adapted from Dr Coulston's Lab exercise
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.NUMERIC_STD.ALL;
library UNIMACRO;
use UNIMACRO.vcomponents.all;
library UNISIM;
use UNISIM.VComponents.all;
use work.graphicsParts.all;		


entity graphics is
    Port ( clk : in  STD_LOGIC;
           reset_n : in  STD_LOGIC;
           ac_mclk : out STD_LOGIC;
           ac_adc_sdata : in STD_LOGIC;
           ac_dac_sdata : out STD_LOGIC;
           ac_bclk : out STD_LOGIC;
           ac_lrclk : out STD_LOGIC;
           scl : inout STD_LOGIC;
           sda : inout STD_LOGIC;    
           btn: in	STD_LOGIC_VECTOR(4 downto 0);
  		   tmds : out  STD_LOGIC_VECTOR (3 downto 0);
           tmdsb : out  STD_LOGIC_VECTOR (3 downto 0);
           sw : in std_logic_vector(7 downto 0) );
           --pulse, latch, data : in std_logic;
end graphics;

architecture behavior of graphics is
	

	signal fsmCol_sig : std_logic_vector(5 downto 0);
    signal fsmRow_sig : std_logic_vector(4 downto 0);
    signal fsmWen_sig : std_logic;
    signal fsmData_sig: std_logic_vector(15 downto 0);
	signal exSel_sig: std_logic;
	signal cursorRow_sig, cursorCol_sig : unsigned(2 downto 0);
	signal ch1_sig, ch2_sig : std_logic_vector(15 downto 0);
	signal cw : std_logic_vector(4 downto 0);
	signal sw1 : std_logic;
	signal base, interp : signed(15 downto 0);
	signal latch, pulse, data : std_logic;
	signal button : std_logic_vector(7 downto 0);

begin
    exSel_sig <= '0';
    
	datapath: graphics_datapath port map(
		clk => clk,
		reset_n => reset_n,
        ac_mclk => ac_mclk,
        ac_adc_sdata => ac_adc_sdata,
        ac_dac_sdata => ac_dac_sdata,
        ac_bclk => ac_bclk,
        ac_lrclk => ac_lrclk,
		exLbus => base,
        exRbus => interp,
		tmds => tmds,
		tmdsb => tmdsb,
		fsmCol => fsmCol_sig,
        fsmRow => fsmRow_sig,
        fsmWen => fsmWen_sig,
        fsmData => fsmData_sig,
		exCol => "000000",
		exRow => "00000",
		exWen => '0',
		exSel => exSel_sig,
		exData => "0000000000000000",
		btn => btn,
		cursorRow => cursorRow_sig,
		cursorCol => cursorCol_sig,
		sw => sw,
		ch1 => ch1_sig,
		ch2 => ch2_sig,
		base => base,
		interp => interp );	
		
	datapath4: lab4_datapath 
        port map(
            clk => clk,
            reset_n => reset_n,
            switch => sw,
            cw => cw,
            btn => btn, 
            base => base,
            interp => interp );
			  
	control: graphics_fsm port  map( 
		clk => clk,
		reset_n => reset_n,
		fsmCol => fsmCol_sig,
        fsmRow => fsmRow_sig,
        fsmWen => fsmWen_sig,
        fsmData => fsmData_sig,
        cursorRow => cursorRow_sig,
        cursorCol => cursorCol_sig,
        btn => btn,
        sw => sw,
        ch1 => ch1_sig,
        ch2 => ch2_sig );
        
    control4: lab4_fsm 
        port map( 
            clk => clk,
            reset_n => reset_n,
            sw => '1',
            cw => cw);
            
--    controller: nes_fsm
--        port map(
--            clk => clk,
--            latch => latch,
--            pulse => pulse,
--            data => data,
--            button => button);

end behavior;
