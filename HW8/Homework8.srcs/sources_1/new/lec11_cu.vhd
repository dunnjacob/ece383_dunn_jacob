------------------------------------------------------------------------
------ Name:	Jacob Dunn
------ Date:	Feb 6, 2022
------ File:	lec11_cu.vhdl
------ HW:	Lecture 11
------	Crs:	ECE 383
------
------ Purp:	Control unit for keyboard scancode
------
------ Documentation:	C2C Carrig and C2C Some assisted with the development
------                      of this file.
------
------ Academic Integrity Statement: I certify that, while others may have 
------ assisted me in brain storming, debugging and validating this program, 
------ the program itself is my own work. I understand that submitting code 
------ which is the work of other individuals is a violation of the honor   
------ code.  I also understand that if I knowingly give my original work to 
------ another individual is also a violation of the honor code. 
----------------------------------------------------------------------------- 


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity lec11_cu is
	Port(	clk: in  STD_LOGIC;
            reset : in  STD_LOGIC;
            kbClk: in std_logic;
            cw: out STD_LOGIC_VECTOR(3 downto 0);
            sw: in STD_LOGIC;
            busy: out std_logic );
end lec11_cu;

architecture Behavioral of lec11_cu is

type state_type is (clrBusy, wait1, setBusy, initFor, cmpCount, wait2, shift, wait3, inc, scan);
signal state: state_type;

begin    
    process(clk)
    begin
            if (reset = '0') then
                state <= clrBusy;
            
            elsif (state = clrBusy) then
                state <= wait1;

            elsif (state = wait1) then
                if(kbClk = '0') then
                    state <= setBusy;
                else
                    state <= wait1;
                end if;

            elsif (state = setBusy) then
                state <= initFor;

            elsif (state = initFor) then
                state <= cmpCount;
   
            elsif (state = cmpCount) then
                if(sw = '1') then
                    state <= scan;
                else
                    state <= wait2;
                end if;
                
            elsif (state = wait2) then
                if(kbClk = '0') then
                    state <= shift;
                else
                    state <= wait2;
                end if;
                
            elsif (state = shift) then
                state <= wait3;
                              
            elsif (state = wait3) then
                if(kbClk = '1') then
                    state <= inc;
                else
                    state <= wait3;
                end if;
  
            elsif (state = inc) then
                state <= cmpCount;

            elsif (state = scan) then
                state <= clrBusy;
            end if;                                          
    end process;
    
    busy <= '0'  when (state = clrBusy) or (state = wait1) else '1';
    cw <= "0000" when (state = wait1) or (state = setBusy) or (state = cmpCount) or (state = wait2) or (state = wait3) else
          "0001" when (state = inc) else
          "0011" when (state = initFor) or (state = clrBusy) else
          "0100" when (state = shift) else
          "1000" when (state = scan);

end Behavioral;
