--------------------------------------------------------------------
-- Name:	Jacob Dunn
-- Date:	7 January 2022
-- Course:	ECE383
-- File:	HW2
-- HW:		HW2 Jacob Dunn
--
-- Purp: VHDL code for problem 2 on HW2
--
-- Doc:	None
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity ScancodeDecoder is
  Port (D: in std_logic_vector(7 downto 0);
        H: out std_logic_vector(3 downto 0));
end ScancodeDecoder;

architecture Behavioral of ScancodeDecoder is

begin
    H <=    "0000" when D = x"45" else
            "0001" when D = x"16" else
            "0010" when D = x"1E" else
            "0011" when D = x"26" else
            "0100" when D = x"25" else
            "0101" when D = x"2E" else
            "0110" when D = x"36" else
            "0111" when D = x"3D" else
            "1000" when D = x"3E" else
            "1001" when D = x"46" else
            "0000";

end Behavioral;
