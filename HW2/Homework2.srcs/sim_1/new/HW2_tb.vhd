--------------------------------------------------------------------
-- Name:	Jacob Dunn
-- Date:	7 January 2022
-- Course:	ECE383
-- File:	HW2_tb
-- HW:		HW2 Jacob Dunn
--
-- Purp: VHDL code for problem 2 on HW2
--
-- Doc:	None
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity ScancodeDecoder_tb is
end ScancodeDecoder_tb;

architecture Behavioral of ScancodeDecoder_tb is
    component ScancodeDecoder
    port(D : in std_logic_vector(7 downto 0);
         H : out std_logic_vector(3 downto 0) );
    end component;
    
    signal a, b, c, d, e, f, g, h, w ,x, y, z : std_logic;
    signal testVector: std_logic_vector(7 downto 0);
    signal testAnswer: std_logic_vector(3 downto 0);
    --signal w1: std_logic_vector(7 downto 0);
    --signal w2: std_logic_vector(3 downto 0);
    
        CONSTANT TEST_ELEMENTS: integer := 10;
	    SUBTYPE INPUT is std_logic_vector(7 downto 0);
        TYPE TEST_INPUT_VECTOR is array (1 to TEST_ELEMENTS) of INPUT;
        SIGNAL TEST_INPUT: TEST_INPUT_VECTOR := (x"45", x"16", x"1E", x"26", x"25", x"2E", x"36", x"3D", x"3E", x"46");
    
        SUBTYPE OUTPUT is std_logic_vector(3 downto 0);
        TYPE TEST_OUTPUT_VECTOR is array (1 to TEST_ELEMENTS) of OUTPUT;
        SIGNAL TEST_OUTPUT: TEST_OUTPUT_VECTOR := ("0000", "0001", "0010", "0011", "0100", "0101", "0110", "0111", "1000", "1001");
    
        SIGNAL i : integer;
        
begin
    uut: ScancodeDecoder PORT MAP (
        D(0) => a,
        D(1) => b,
        D(2) => c,
        D(3) => d,
        D(4) => e,
        D(5) => f,
        D(6) => g,
        D(7) => h,
        
        H(0) => w,
        H(1) => x,
        H(2) => y,
        H(3) => z );
        
        a <= testVector(7);
        b <= testVector(6);
        c <= testVector(5);
        d <= testVector(4);
        e <= testVector(3);
        f <= testVector(2);
        g <= testVector(1);
        h <= testVector(0);
        
        w <= testAnswer(3);
        x <= testAnswer(2);
        y <= testAnswer(1);
        z <= testAnswer(0);
                    
        process
        begin
            for i in 1 to TEST_ELEMENTS loop
                testVector <= test_input(i);
                testAnswer <= test_output(i);
                wait for 1 us;
            end loop;
                    
        end process tb;
                
end Behavioral;
