--------------------------------------------------------------------
-- Name:	Jacob Dunn
-- Date:	20 January 2022
-- Course:	ECE383
-- File:	Lab1
-- HW:		Lab1 Jacob Dunn
--
-- Purp: Video Synchronization
--
-- Doc:	C2C Carrig explained to me how to optiamlly implement the trigger
--      design and helped me to understand what the finished project should
--      look like.
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity lab1 is
    Port ( clk : in  STD_LOGIC;
           reset_n : in  STD_LOGIC;
		   btn: in	STD_LOGIC_VECTOR(4 downto 0);
           tmds : out  STD_LOGIC_VECTOR (3 downto 0);
           tmdsb : out  STD_LOGIC_VECTOR (3 downto 0));
end lab1;

architecture structure of lab1 is

	signal trigger_time, trigger_volt, row, column: unsigned(9 downto 0);
	signal old_button, button_activity: std_logic_vector(4 downto 0);
	signal ch1_wave, ch2_wave: std_logic;
	
	component video is
    Port ( clk : in  STD_LOGIC;
           reset_n : in  STD_LOGIC;
           tmds : out  STD_LOGIC_VECTOR (3 downto 0);
           tmdsb : out  STD_LOGIC_VECTOR (3 downto 0);
			  trigger_time: in unsigned(9 downto 0);
			  trigger_volt: in unsigned (9 downto 0);
			  row: out unsigned(9 downto 0);
			  column: out unsigned(9 downto 0);
			  ch1: in std_logic;
			  ch1_enb: in std_logic;
			  ch2: in std_logic;
			  ch2_enb: in std_logic);
	end component;

begin


	------------------------------------------------------------------------------
	-- the variable button_activity will contain a '1' in any position which 
	-- has been pressed or released.  The buttons are all nominally 0
	-- and equal to 1 when pressed.
	------------------------------------------------------------------------------
    button_activity(4) <= btn(4);
    button_activity(3) <= btn(3);
    button_activity(2) <= btn(2);
    button_activity(1) <= btn(1);
    button_activity(0) <= btn(0);
    
	------------------------------------------------------------------------------
	-- If a button has been pressed then increment or decrement the trigger vars
	------------------------------------------------------------------------------
	button : process (clk)
      begin        
        if rising_edge(clk) then
            if (reset_n = '0') then
                trigger_volt <= "0010111001"; --185
                trigger_time <= "0100100010"; --290
            end if;
            
            if    ( (btn(0) = '1' ) and not (btn = old_button) ) then
                trigger_volt <= trigger_volt - 5;
            elsif ( (btn(1) = '1' ) and not (btn = old_button) ) then
                trigger_time <= trigger_time - 5;
            elsif ( (btn(2) = '1' ) and not (btn = old_button) ) then
                trigger_volt <= trigger_volt + 5;
            elsif ( (btn(3) = '1' ) and not (btn = old_button) ) then
                trigger_time <= trigger_time + 5;
            end if;
            old_button <= btn;
        end if;
    end process button;
	   
	------------------------------------------------------------------------------
	------------------------------------------------------------------------------
	video_inst: video port map( 
		clk => clk,
		reset_n => reset_n,
		tmds => tmds,
		tmdsb => tmdsb,
		trigger_time => trigger_time,
		trigger_volt => trigger_volt,
		row => row, 
		column => column,
		ch1 => ch1_wave,
		ch1_enb => ch1_wave,
		ch2 => ch2_wave,
		ch2_enb => ch2_wave); 
	
end structure;
