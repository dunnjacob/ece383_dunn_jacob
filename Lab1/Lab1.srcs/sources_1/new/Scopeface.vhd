--------------------------------------------------------------------
-- Name:	Jacob Dunn
-- Date:	20 January 2022
-- Course:	ECE383
-- File:	Scopeface.vhd
-- HW:		Lab1 Jacob Dunn
--
-- Purp: Video Synchronization
--
-- Doc:	C2C Carrig explained how to use modulo for drawing the lines and 
--      the optimal method to develop the trigger triangles.
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity Scopeface is
    Port (    trigger_time: in unsigned(9 downto 0);
              trigger_volt: in unsigned (9 downto 0);
              row: in unsigned(9 downto 0);
              column: in unsigned(9 downto 0);
              ch1: in std_logic;
              ch1_enb: in std_logic;
              ch2: in std_logic;
              ch2_enb: in std_logic;
              R : out std_logic_vector(7 downto 0);
              G : out std_logic_vector(7 downto 0);
              B : out std_logic_vector(7 downto 0) 
              );
end Scopeface;

architecture Behavioral of Scopeface is

begin
    
    R <= "11111111" when    ( (column mod 60 = 20) and (row >= 20) and (row < 421) ) or 
                            ( (row mod 50 = 20) and (column >= 20) and (column < 621) and (row < 421) ) or
                            ( (row mod 10 = 0) and (column = 319 or column = 321) and 
                              (row >= 20) and (row < 421) ) or
                            ( (column mod 15 = 5) and (row = 219 or row = 221) and
                              (column >= 20) and (column < 621) ) or 
                            
                            ( (column = trigger_time) and (row > 20) and (row < 24) ) or
                            ( (column = trigger_time + 1) and (row > 20) and (row < 23) ) or
                            ( (column = trigger_time - 1) and (row > 20) and (row < 23) ) or
                            ( (column = trigger_time + 2) and (row > 20) and (row < 22) ) or
                            ( (column = trigger_time - 2) and (row > 20) and (row < 22) ) or
                            ( (column = trigger_time + 3) and (row > 20) and (row < 21) ) or
                            ( (column = trigger_time - 3) and (row > 20) and (row < 21) ) or
                            
                            ( (row = trigger_volt) and (column > 20) and (column < 24) ) or
                            ( (row = trigger_volt + 1) and (column > 20) and (column < 23) ) or
                            ( (row = trigger_volt - 1) and (column > 20) and (column < 23) ) or
                            ( (row = trigger_volt + 2) and (column > 20) and (column < 22) ) or
                            ( (row = trigger_volt - 2) and (column > 20) and (column < 22) ) or
                            ( (row = trigger_volt + 3) and (column > 20) and (column < 21) ) or
                            ( (row = trigger_volt - 3) and (column > 20) and (column < 21) ) or
                            
                              (ch1 = '1')
                               else (others => '0');
                               
    G <= "11111111" when    ( (column mod 60 = 20) and (row >= 20) and (row < 421) ) or 
                            ( (row mod 50 = 20) and (column >= 20) and (column < 621) and (row < 421) ) or
                            ( (row mod 10 = 0) and (column = 319 or column = 321) and 
                              (row >= 20) and (row < 421) ) or
                            ( (column mod 15 = 5) and (row = 219 or row = 221) and
                              (column >= 20) and (column < 621) ) or
                              
                              (ch2 = '1')
                               else (others => '0');
                               
    B <= "11111111" when    ( (column mod 60 = 20) and (row >= 20) and (row < 421) ) or 
                            ( (row mod 50 = 20) and (column >= 20) and (column < 621) and (row < 421) ) or
                            ( (row mod 10 = 0) and (column = 319 or column = 321) and 
                              (row >= 20) and (row < 421) ) or
                            ( (column mod 15 = 5) and (row = 219 or row = 221) and
                              (column >= 20) and (column < 621) )
                               else (others => '0');
    
end Behavioral;
