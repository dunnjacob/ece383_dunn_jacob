--------------------------------------------------------------------
-- Name:	Jacob Dunn
-- Date:	20 January 2022
-- Course:	ECE383
-- File:	vga.vhd
-- HW:		Lab1 Jacob Dunn
--
-- Purp: Video Synchronization
--
-- Doc:	C2C Carrig and I collaborated while completing this file.
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity vga is
  Port (    clk: in  STD_LOGIC;
			reset_n : in  STD_LOGIC;
			h_sync : out  STD_LOGIC;
			v_sync : out  STD_LOGIC; 
			blank : out  STD_LOGIC;
			r: out STD_LOGIC_VECTOR(7 downto 0);
			g: out STD_LOGIC_VECTOR(7 downto 0);
			b: out STD_LOGIC_VECTOR(7 downto 0);
			trigger_time: in unsigned(9 downto 0);
			trigger_volt: in unsigned (9 downto 0);
			row: out unsigned(9 downto 0);
			column: out unsigned(9 downto 0);
			ch1: in std_logic;
			ch1_enb: in std_logic;
			ch2: in std_logic;
			ch2_enb: in std_logic   
			);
end vga;

architecture Behavioral of vga is

signal processRow : unsigned(9 downto 0) := (others => '0');
signal processCol : unsigned(9 downto 0) := (others => '0');
signal processRoll : std_logic := '0';
signal ctrl : std_logic := '1';
signal roll : std_logic := '0';

signal h_blank : std_logic := '0';
signal v_blank : std_logic := '0';

    component Scopeface is
    Port (    trigger_time: in unsigned(9 downto 0);
              trigger_volt: in unsigned (9 downto 0);
              row: in unsigned(9 downto 0);
              column: in unsigned(9 downto 0);
              ch1: in std_logic;
              ch1_enb: in std_logic;
              ch2: in std_logic;
              ch2_enb: in std_logic;
              R : out std_logic_vector(7 downto 0);
              G : out std_logic_vector(7 downto 0);
              B : out std_logic_vector(7 downto 0));
    end component;

begin

    counterCol : process (clk)
       begin
         if rising_edge(clk) then
             if (reset_n = '0') then
                 processCol <= (others => '0');
             elsif ( (processCol < 799) and (ctrl = '1') ) then
                 processCol <= processCol + 1;
             elsif ( (processCol = 799) and (ctrl = '1') ) then
                 processCol <= (others => '0');
             end if;
         end if;
     end process counterCol;
     column <= processCol;    
     roll <= '1' when (processCol = 799) and (ctrl = '1') else '0';

    counterRow : process (clk)
      begin
        if rising_edge(clk) then
            if reset_n = '0' then
                processRow <= (others => '0');
            elsif ( (processRow < 524) and (roll = '1') ) then
                processRow <= processRow + 1;
            elsif ( (processRow = 524) and (roll = '1') ) then
                processRow <= (others => '0');
            end if;
        end if;
    end process counterRow;
    row <= processRow;   
    
   h_sync <= '0' when (processCol > 655 and processCol < 752) else '1';
   v_sync <= '0' when (processRow > 489 and processRow < 492) else '1';
   h_blank <= '1' when (processCol > 639 and processCol < 800) else '0';
   v_blank <= '1' when (processRow > 479 and processRow < 525) else '0';
   blank <= h_blank or v_blank;
   
   Face : Scopeface
        port map(
            r => R,
            g => G,
            b => B,
            trigger_time => trigger_time,
            trigger_volt => trigger_volt,
            row => processRow,
            column => processCol,
            ch1 => ch1,
            ch1_enb => ch1_enb,
            ch2 => ch2,
            ch2_enb => ch2_enb
        );

end Behavioral;
