--------------------------------------------------------------------
-- Name:	Jacob Dunn
-- Date:	15 January 2022
-- Course:	ECE383
-- File:	HW4
-- HW:		HW4 Jacob Dunn
--
-- Purp: VHDL code for HW4
--
-- Doc:	None
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity HW4 is
   port (clk, reset, ctrl : in std_logic;
         Q1, Q0 : out unsigned(2 downto 0) ); 
end HW4;

architecture Behavioral of HW4 is

component counter is
   port (clk, reset, ctrl : in std_logic;
         Q : out unsigned(2 downto 0); 
         roll : out std_logic);
end component counter; 

signal glueRoll : std_logic := '0';
signal otherRoll : std_logic := '0';

begin

    LSB : counter
        port map(
            clk => clk,
            reset => reset,
            ctrl => ctrl,
            roll => glueRoll,
            Q => Q0 );
    
    MSB : counter
        port map(
            clk => clk,
            reset => reset,
            ctrl => glueRoll,
            roll => otherRoll,
            Q => Q1 );

end Behavioral;
