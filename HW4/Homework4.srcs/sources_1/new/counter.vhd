--------------------------------------------------------------------
-- Name:	Jacob Dunn
-- Date:	20 January 2022
-- Course:	ECE383
-- File:	counter.vhd
-- HW:		Lab1 Jacob Dunn
--
-- Purp: Video Synchronization
--
-- Doc:	None.
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity counter is
   port (clk, reset, ctrl : in std_logic;
         Q : out unsigned(2 downto 0); 
         roll : out std_logic);
end counter;

architecture Behavioral of counter is

signal w : unsigned(2 downto 0) := (others => '0');

-----------------------------------------------------
	--		ctrl
	--		0			hold
	--		1			Q + 1 mod 5
-----------------------------------------------------

begin

    counter : process (clk, reset)
      begin
        if rising_edge(clk) then
            if reset = '0' then
                w <= (others => '0');
                --roll <= '0';
            elsif ( (w < 4) and (ctrl = '1') ) then
                w <= w + 1;
                --roll <= '0';
            elsif ( (w = 4) and (ctrl = '1') ) then
                w <= (others => '0');
                --roll <= '1';
            end if;
        end if;
    end process counter;
    
    roll <= '1' when (w = 4) and (ctrl = '1') else '0';

    Q <= w;

end Behavioral;
