--------------------------------------------------------------------
-- Name:	Jacob Dunn
-- Date:	15 January 2022
-- Course:	ECE383
-- File:	HW4_tb
-- HW:		HW4 Jacob Dunn
--
-- Purp: VHDL code for HW4
--
-- Doc:	None
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
 
ENTITY lec4_tb IS
END lec4_tb;
 
ARCHITECTURE behavior OF lec4_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT HW4
    PORT(
         clk : IN  std_logic;
         reset : IN  std_logic;
         ctrl: in std_logic;
         Q1 : OUT  unsigned(2 downto 0);
         Q0 : OUT  unsigned(2 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal reset : std_logic := '0';
   signal ctrl : std_logic := '0';

 	--Outputs
   signal w0 : unsigned(2 downto 0) := (others => '0');
   signal w1 : unsigned(2 downto 0) := (others => '0');

   -- Clock period definitions
   constant clk_period : time := 500 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: HW4 PORT MAP (
          clk => clk,
          reset => reset,
	      ctrl => ctrl,
          Q1 => w1,
          Q0 => w0
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 
 	
	ctrl <= '1', '0' after 15us, '1' after 16us, '0' after 17us, '1' after 18us;
	reset <= '0', '1' after 1us;

END;
