--------------------------------------------------------------------
-- Name:	Jacob Dunn
-- Date:	12 January 2022
-- Course:	ECE383
-- File:	HW3
-- HW:		HW3 Jacob Dunn
--
-- Purp: VHDL code for problem 5 on HW3
--
-- Doc:	C2C Carrig and I worked on this assignment together
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity HW3 is
  Port ( num : in std_logic_vector(7 downto 0);
           z : out std_logic);
end HW3;

architecture Behavioral of HW3 is

begin    
    --z <= '1' when num(7 downto 0) = num(7 downto 0) else '0';
    z <= '1' when num = x"00" else
         '1' when num = x"11" else
         '1' when num = x"22" else
         '1' when num = x"33" else
         '1' when num = x"44" else
         '1' when num = x"55" else
         '1' when num = x"66" else
         '1' when num = x"77" else
         '1' when num = x"88" else
         '1' when num = x"99" else
         '1' when num = x"AA" else
         '1' when num = x"BB" else
         '1' when num = x"CC" else
         '1' when num = x"DD" else
         '1' when num = x"EE" else
         '1' when num = x"FF" else
         '0';
                  

         
    
end Behavioral;
