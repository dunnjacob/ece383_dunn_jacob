--------------------------------------------------------------------
-- Name:	Jacob Dunn
-- Date:	12 January 2022
-- Course:	ECE383
-- File:	HW3_tb
-- HW:		HW3 Jacob Dunn
--
-- Purp: VHDL code for problem 5 on HW3
--
-- Doc:	Worked with C2C Carrig
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity HW3_tb is
end HW3_tb;

architecture Behavioral of HW3_tb is
    component HW3
    Port ( num : in std_logic_vector(7 downto 0);
             z : out std_logic);
    end component;
    
    signal w1 : std_logic_vector(7 downto 0);
    signal w2 : std_logic;
    
        CONSTANT TEST_ELEMENTS: integer := 10;
        SUBTYPE INPUT is std_logic_vector(7 downto 0);
        TYPE TEST_INPUT_VECTOR is array (1 to TEST_ELEMENTS) of INPUT;
        SIGNAL TEST_INPUT: TEST_INPUT_VECTOR := ("00000000", "00010001", "00100010", "00101000", "00110011", "00111010", "01000100", "01000110", "01001000", "01010101");

        SUBTYPE OUTPUT is std_logic;
        TYPE TEST_OUTPUT_VECTOR is array (1 to TEST_ELEMENTS) of OUTPUT;
        SIGNAL TEST_OUTPUT: TEST_OUTPUT_VECTOR := ('1', '1', '1', '0', '1', '0', '1', '0', '0', '1');

        SIGNAL i : integer;
begin


	UUT:	HW3 port map (w1, w2);

	tb : PROCESS
	BEGIN
	for i in 1 to TEST_ELEMENTS loop
	
		w1 <= TEST_INPUT(i);
		w2 <= TEST_OUTPUT(i);
		
		wait for 10 ns; 
				
	end loop;
	
	---------------------------
	-- Just halt the simulator
	---------------------------
	assert TRUE = FALSE 
		report "---------------Self-checking testbench completed.  Nominal circuit behavior---------------"
		severity failure;
			
	END PROCESS tb;

end Behavioral;
