--------------------------------------------------------------------
-- Name:	Jacob Dunn
-- Date:	9 Feb 2022
-- File:	hw8b_cu.vhdl
-- HW:	    hw8b
-- Crs:	    ECE 383
--
-- Purp:	A FSM for HW8b keyboard scan unit
--
-- Documentation: C2C Carrig looked over my FSM and helped with some nextState values.
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
------------------------------------------------------------------------- 
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity hw8b_cu is
	Port(	clk: in  STD_LOGIC;
			reset : in  STD_LOGIC;
			cw: out STD_LOGIC_VECTOR(5 downto 0);
			sw: in STD_LOGIC_VECTOR(1 downto 0);
			ACQ: out std_logic;
			REQ: in std_logic);
end hw8b_cu;

architecture behavior of hw8b_cu is

	type state_type is (waitREQ, setKEY, setACK, waitREQ2, clearACK, clearMatch, init4, cmpCount, setMBR, MBRCmp, countInc, loopInc);
	signal state: state_type;
	signal bigCW: std_logic_vector(6 downto 0);
	
begin
	
    state_process: process(clk)
	 begin
		if (rising_edge(clk)) then
			if (reset = '0') then 
				state <= waitREQ;
			else
				case state is
					when waitREQ =>
						if (REQ = '1') then state <= setKEY; else state <= waitREQ; end if;
                    when setKEY =>
                        state <= setACK;
                    when setACK =>
                        state <= waitREQ2;
                    when waitREQ2 =>
                        if (REQ = '0') then state <= clearACK; else state <= waitREQ2; end if;
                    when clearACK =>
                        state <= clearMatch;
                    when clearMatch =>
                        state <= init4;
                    when init4 =>
                        state <= cmpCount;
                    when cmpCount =>
                        if (sw(0) = '0') then state <= setMBR; else state <= waitREQ; end if;
                    when setMBR =>
                        state <= MBRCmp;
                    when MBRCmp =>
                        if (sw(1) = '1') then state <= countInc; else state <= loopInc; end if;
                    when countInc =>
                        state <= loopInc;
                    when loopInc =>
                        state <= cmpCount;
				end case;
			end if;
		end if;
	end process;


	------------------------------------------------------------------------------
	--			OUTPUT EQUATIONS
	--	
	--		bits 6          bit 5			bit 4,3					bit 2,1          bit 0              
	--		ACQ 			MBR         	Loop Counter		    Match Counter    Key
	--		0 - not busy	1 - load        00 hold	        	    00 hold          1-load
	--		1 - busy		0 - hold    	01 count up				01 count up      0-hold
	--										10 unused			    10 unused	
	--										11 reset			    11 reset
	------------------------------------------------------------------------------	
	bigCW <= "0000000" when state = waitREQ or state = clearACK or state = cmpCount or state = MBRCmp else
			 "0000001" when state = setKEY else
			 "1000000" when state = setACK or state = waitREQ2 else
			 "0000110" when state = clearMatch else
			 "0011000" when state = init4 else
			 "0100000" when state = setMBR else
			 "0000010" when state = countInc else
			 "0001000" when state = loopInc;

	cw <= bigCW(5 downto 0);
	ACQ <= bigCW(6);
	
end behavior;	