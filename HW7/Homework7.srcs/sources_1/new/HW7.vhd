--------------------------------------------------------------------
-- Name:	Jacob Dunn
-- Date:	2 February 2022
-- Course:	ECE383
-- File:	HW7
-- HW:		HW7 Jacob Dunn
--
-- Purp: VHDL code for Homework 7
--
-- Doc:	C2C Carrig and I worked on this assignment together
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity button_debounce is
Port(   clk: in STD_LOGIC;
        reset : in STD_LOGIC;
        button: in STD_LOGIC;
        action: out STD_LOGIC );
end button_debounce;

architecture Behavioral of button_debounce is

signal state: std_logic_vector(2 downto 0) := "000";
signal nextState: std_logic_vector(2 downto 0) := "000";
signal counter: integer := 0;

begin      
        -----------------------------------------------------------------------------
        --        state
        --        000            no button press
        --        001            wait 20 ms then high
        --        010            button press
        --        011            wait 20 ms then low
        --        100            action high
        -----------------------------------------------------------------------------
        process(clk)
        begin
            if (state = "000") then 
                if (button = '1') then
                    nextState <= "001";
                end if;
                action <= '0';
            end if;
            if (state = "001") then
                if (counter = 4000) then
                    counter <= 0;
                    nextState <= "010";
                else
                    counter <= counter + 1;
                end if;
            end if;
            if (state = "010") then
                if (button = '0') then
                    nextState <= "011";
                end if;
            end if;
            if (state = "011") then
                if (counter = 4000) then
                    counter <= 0;
                    nextState <= "100";
                else
                    counter <= counter + 1;
                end if;
            end if;
            if (state = "100") then
                action <= '1';
                nextState <= "000";
            end if;                
            
            state <= nextState;
            
        end process;
end Behavioral;
