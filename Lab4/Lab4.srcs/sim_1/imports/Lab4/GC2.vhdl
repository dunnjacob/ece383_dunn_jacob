--------------------------------------------------------------------------------
-- Name:	Jacob Dunn
-- Purp:	Lab4 testbench
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
 
ENTITY GC2_tb IS
END GC2_tb;
 
ARCHITECTURE behavior OF GC2_tb IS 

component lab4_datapath
    Port (  clk, reset_n : in std_logic;
            cw : in std_logic_vector(4 downto 0);
            btn : in std_logic_vector(4 downto 0);
            switch : in std_logic_vector(7 downto 0);
            base, interp : out signed(15 downto 0) );
    end component;
    
component lab4_fsm
    Port ( clk, reset_n : in std_logic;
           sw : in std_logic; 
           cw : out std_logic_vector(4 downto 0) );
    end component;
 
   --signals for the counter
   signal clk, reset, ready : std_logic := '0';
   signal cw : std_logic_vector(4 downto 0) := "00000";
   signal sw : std_logic_vector(2 downto 0);
   signal btn : std_logic_vector(4 downto 0);
   signal switch : std_logic_vector (7 downto 0);
   signal base, interp : signed(15 downto 0);
 	--Outputs

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
   -- signals to play with VHDL math and syntax

    
BEGIN

    	-- Instantiate the Unit Under Test (UUT)
    datapath4: lab4_datapath PORT MAP (
        clk => clk,
        reset_n => reset,
        cw => cw,
        btn => btn,
        switch => switch,
        base => base, 
        interp => interp);
        
    fsm4: lab4_fsm PORT MAP (
        clk => clk,
        reset_n => reset,
        sw => ready,
        cw => cw );

    clk_process :process
    begin
	    clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
    end process;
    
	reset <= '0', '1' after 20ns;
	ready <= '0', '1' after 30ns;
	btn <= "00001";
	switch <= "00000010";

END;
