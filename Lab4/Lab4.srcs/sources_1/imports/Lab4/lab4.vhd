--------------------------------------------------------------------
-- Name:	Jacob Dunn
-- Date:	Apr 16, 2022
-- File:	lab4.vhd
-- HW:	    Lab4
-- Crs:	    ECE 383
--
-- Purp:	This is the main file for lab2
--
-- Documentation: None.
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
------------------------------------------------------------------------- 
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.NUMERIC_STD.ALL;
library UNIMACRO;
use UNIMACRO.vcomponents.all;
use work.lab2Parts.all;		


entity lab4 is
    Port ( clk : in  STD_LOGIC;
           reset_n : in  STD_LOGIC;
		   ac_mclk : out STD_LOGIC;
		   ac_adc_sdata : in STD_LOGIC;
		   ac_dac_sdata : out STD_LOGIC;
		   ac_bclk : out STD_LOGIC;
		   ac_lrclk : out STD_LOGIC;
           scl : inout STD_LOGIC;
           sda : inout STD_LOGIC;
		   tmds : out  STD_LOGIC_VECTOR (3 downto 0);
           tmdsb : out  STD_LOGIC_VECTOR (3 downto 0);
		   btn : in	STD_LOGIC_VECTOR(4 downto 0);
		   switch : in std_logic_vector(7 downto 0) );
end lab4;

architecture behavior of lab4 is

	signal sw: std_logic_vector(2 downto 0);
	signal cw1: std_logic_vector (2 downto 0);
	signal cw2: std_logic_vector (4 downto 0);
	signal interp, base : signed(15 downto 0);
	
begin

	datapath2: lab2_datapath 
	port map(
		clk => clk,
		reset_n => reset_n,
		ac_mclk => ac_mclk,
		ac_adc_sdata => ac_adc_sdata,
		ac_dac_sdata => ac_dac_sdata,
		ac_bclk => ac_bclk,
		ac_lrclk => ac_lrclk,
        scl => scl,
        sda => sda,
		tmds => tmds,
		tmdsb => tmdsb,
		sw => sw,
		cw => cw1,
		btn => btn, 
		exWrAddr => "0000000000",
		exWen => '0',
		exSel => '0',
		Lbus_out => open,
		Rbus_out => open,
		exLbus => base,
		exRbus => interp,		
		flagQ => OPEN,
		flagClear => "00000000");	
			  
	control2: lab2_fsm 
	port map( 
		clk => clk,
		reset_n => reset_n,
		sw => sw,
		cw => cw1);
		
	datapath4: lab4_datapath 
    port map(
        clk => clk,
        reset_n => reset_n,
        switch => switch,
        cw => cw2,
        btn => btn, 
        base => base,
        interp => interp );    
              
    control4: lab4_fsm 
    port map( 
        clk => clk,
        reset_n => reset_n,
        sw => sw(1),
        cw => cw2);
end behavior;
