--------------------------------------------------------------------
-- Name:	Jacob Dunn
-- Date:	Feb 15, 2022
-- File:	lab2_fsm.vhd
-- HW:	    Lab2
-- Crs:	    ECE 383
--
-- Purp:	This is the control unit for lab2
--
-- Documentation: None.
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
------------------------------------------------------------------------- 


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity lab2_fsm is
    Port ( clk, reset_n : in std_logic;
           sw : in std_logic_vector(2 downto 0); 
           cw : out std_logic_vector(2 downto 0) );
end lab2_fsm;

architecture Behavioral of lab2_fsm is

type state_type is (init, waitTrigger, waitReady, write, countInc, countCmp);
signal state: state_type;

begin

     state_process: process(clk)
	 begin
		if (rising_edge(clk)) then
			if (reset_n = '0') then 
				state <= init;
			else
				case state is
					when init => 
					   state <= waitTrigger;
					when waitTrigger =>
					   if (sw(0) = '0') then state <= waitTrigger; else state <= waitReady; end if;
                    when waitReady =>
                        if (sw(1) = '0') then state <= waitReady; else state <= write; end if;
                    when write =>
                        state <= countInc;
                    when countInc =>
                        state <= countCmp;
                    when countCmp =>
                    if (sw(2) = '0') then state <= init; else state <= waitReady; end if;
				end case;
			end if;
		end if;
	end process;


	---------------------------------------------
	--		OUTPUT EQUATIONS - cw
	--	
	--		bit 2, 1            bit 0
	--		counter 			wrENB
	--		00 - hold           0 - hold
	--		01 - count up       1 - write
	--      10 - unused
	--      11 - reset 
	---------------------------------------------
	cw <=    "000" when state = waitTrigger or state = waitReady or state = countCmp else
	         "001" when state = write else
	         "010" when state = countInc else
			 "110" when state = init;

end Behavioral;
