--------------------------------------------------------------------
-- Name:	Jacob Dunn
-- Date:	Feb 22, 2022
-- File:	flagRegister.vhd
-- HW:	    Lab2
-- Crs:	    ECE 383
--
-- Purp:	This is the flagRegister file for lab2
--
-- Documentation: None.
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
------------------------------------------------------------------------- 


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity flagRegister is
    Generic (N: integer := 8);
    Port( clk: in  STD_LOGIC;
          reset_n : in  STD_LOGIC;
          set, clear: in std_logic;
          Q: out std_logic_vector(N-1 downto 0));
end flagRegister;

architecture Behavioral of flagRegister is

begin

    flagRegister_process: process(clk)
    begin
        if (reset_n = '1') then
            if (rising_edge(clk)) then
                if (set = '0') and (clear = '1') then
                    Q <= "00000000";
                elsif (set = '1') and (clear = '0') then
                    Q <= "00000001";
                end if;
            end if;
        else
            Q <= "00000000";
        end if;        
    end process;

end Behavioral;
