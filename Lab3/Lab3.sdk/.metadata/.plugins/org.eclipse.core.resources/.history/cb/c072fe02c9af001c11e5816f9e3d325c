/*--------------------------------------------------------------------
-- Name:	Maj Jeff Falkinburg
-- Date:	Feb 19, 2017
-- File:	lec19.c
-- Event:	Lecture 19
-- Crs:		ECE 383
--
-- Purp:	MicroBlaze Tutorial that implements a custom IP with interrupt
--			to MicroBlaze.
--
-- Documentation:	MicroBlaze Tutorial
--
-- Academic Integrity Statement: I certify that, while others may have
-- assisted me in brain storming, debugging and validating this program,
-- the program itself is my own work. I understand that submitting code
-- which is the work of other individuals is a violation of the honor
-- code.  I also understand that if I knowingly give my original work to
-- another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/
/***************************** Include Files ********************************/

#include "xparameters.h"
#include "stdio.h"
#include "xstatus.h"

#include "platform.h"
#include "xil_printf.h"						// Contains xil_printf
#include <xuartlite_l.h>					// Contains XUartLite_RecvByte
#include <xil_io.h>							// Contains Xil_Out8 and its variations
#include <xil_exception.h>

/************************** Constant Definitions ****************************/

/*
 * The following constants define the slave registers used for our Counter PCORE
 */
#define countBase		0x44a00000
#define countQReg		countBase			// 8 LSBs of slv_reg0 read=Q, write=D
#define	countCtrlReg	countBase+4			// 2 LSBs of slv_reg1 are control
#define	countRollReg	countBase+8			// 1 LSBs of slv_reg2 for roll flag
#define	countClearReg	countBase+0xc		// 1 LSBs of slv_reg3 (0) roll clear flag
#define Lbusout    		countBase+20
#define Rbusout   		countBase+24
#define exWen			countBase+28
#define exWrAddr		countBase+32
#define exLbus			countBase+36
#define exRbus			countBase+40
#define FlagQ			countBase+44
#define FlagClear		countBase+40
#define TrigVolt   		countBase+52        //10 LSBs of slv_reg13 for trig_volt value (0x34)
#define TrigTime   		countBase+56        //10 LSBs of slv_reg14 for trig_volt value (0x38)

/*
 * The following constants define the Counter commands
 */
#define count_HOLD		0x00		// The control bits are defined in the VHDL
#define	count_COUNT		0x01		// code contained in lec18.vhdl.  They are
#define	count_LOAD		0x02		// added here to centralize the bit values in
#define count_RESET		0x03		// a single place.

#define printf xil_printf			/* A smaller footprint printf */

#define	uartRegAddr			0x40600000		// read <= RX, write => TX

/************************** Function Prototypes ****************************/
void myISR(void);

/************************** Variable Definitions **************************/
/*
 * The following are declared globally so they are zeroed and so they are
 * easily accessible from a debugger
 */
u16 isrCount = 0;
int array_L[1024], array_R[1024];
int start, trigger, i = 0;

int main(void) {

	unsigned char c;

	init_platform();

	print("Welcome to Lecture 19\n\r");

    microblaze_register_handler((XInterruptHandler) myISR, (void *) 0);
    microblaze_enable_interrupts();

    Xil_Out8(countClearReg, 0x01);					// Clear the flag and then you MUST
	Xil_Out8(countClearReg, 0x00);					// allow the flag to be reset later

    while(1) {

    	c=XUartLite_RecvByte(uartRegAddr);

		switch(c) {

    		/*-------------------------------------------------
    		 * Reply with the help menu
    		 *-------------------------------------------------
			 */
    		case '?':
    			printf("--------------------------\r\n");
    			printf("    Trig_volt = %x\r\n", Xil_In16(TrigVolt));
    			printf("    Trig_time = %x\r\n", Xil_In16(TrigTime));
    			printf("    Lbusout = %x\r\n", Xil_In16(Lbusout));
    			printf("    Rbusout = %x\r\n", Xil_In16(Rbusout));
    			printf("--------------------------\r\n");
    			printf("?: help menu\r\n");
    			printf("o: k\r\n");
    			printf("f:   flush terminal\r\n");
    			printf("m:   Load Array Values\r\n");
    			printf("p:   Print Array Values\r\n");
    			break;

			/*-------------------------------------------------
			 * Basic I/O loopback
			 *-------------------------------------------------
			 */
    		case 'o':
    			printf("k \r\n");
    			break;

			/*-------------------------------------------------
			 * Gate Check 3
			 *-------------------------------------------------
			 */
    		/*case 'd':                      // some of these will be XIL commands
				for (int i=0;i<1024;i++) {
				   Xil_Out16(exWrAddr, i); // set BRAM address
				   Xil_Out16(exLbus, 185); // row for horz line
										   // need to shift to upper 10bits?
				   Xil_Out16(exRbus, i);   // diagnonal line [need to shift?]
				   Xil_Out8(exWen, 0x01);    // write data to address in BRAM
				   Xil_Out8(exWen, 0x00);    // turn off write
				}
			break;*/

			case 'm':     // some of these will be XIL commands

				  for (int i=0;i<1024;i++) {
				  while(FlagQ==0){}; //wait on ready
				  array_L[i] = Xil_In16(Lbusout); // read audio values
				  array_R[i] = Xil_In16(Rbusout);
				  Xil_Out8(countClearReg, 0x01); // Clear the flag and then you MUST
				  Xil_Out8(countClearReg, 0x00); // allow the flag to be reset later
				}
			break;

			case 'p':
				for (int i=0; i<1024; i++){
					printf("%x\r\n",array_L[i]);
				}
			break;

			/*-------------------------------------------------
			 * Clear the terminal window
			 *-------------------------------------------------
			 */
            case 'f':
            	for (c=0; c<40; c++) printf("\r\n");
               	break;

			/*-------------------------------------------------
			 * Clear the terminal window
			 *-------------------------------------------------
			 */

			case 't':
				i = Xil_In16(TrigTime) - 20;
				while(trigger == 0 && i<1023){
					if(array_L[i] >= Xil_In16(TrigVolt) && array_L[i-1] <= Xil_In16(TrigVolt)){
						trigger = i;
						i++;
					}
				}

				start = trigger - (Xil_In16(TrigTime) - 20);

				if(start < 0){ start = 0;}

				for(i=0; i<600; i++){
					array_L[i] = array_L[i+start];
				}

				Xil_Out8(countClearReg, 0x01); // Clear the flag and then you MUST
				Xil_Out8(countClearReg, 0x00); // allow the flag to be reset later


			/*-------------------------------------------------
			 * Unknown character was
			 *-------------------------------------------------
			 */
    		default:
    			printf("unrecognized character: %c\r\n",c);
    			break;
    	} // end case





    } // end while 1

    cleanup_platform();

    return 0;
} // end main


void myISR(void) {
	isrCount = isrCount + 1;
/*	while (isrCount < 1024) {
  	    array_L[isrCount] = Xil_In16(Lbusout); // read audio values
	    array_R[isrCount] = Xil_In16(Rbusout);
	}*/
	    Xil_Out8(countClearReg, 0x01);					// Clear the flag and then you MUST
	    Xil_Out8(countClearReg, 0x00);					// allow the flag to be reset later
}

