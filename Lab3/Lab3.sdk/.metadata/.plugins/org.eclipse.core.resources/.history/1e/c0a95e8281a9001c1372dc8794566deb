/*--------------------------------------------------------------------
-- Name:	Maj Jeff Falkinburg
-- Date:	Feb 19, 2017
-- File:	lec19.c
-- Event:	Lecture 19
-- Crs:		ECE 383
--
-- Purp:	MicroBlaze Tutorial that implements a custom IP with interrupt
--			to MicroBlaze.
--
-- Documentation:	MicroBlaze Tutorial
--
-- Academic Integrity Statement: I certify that, while others may have
-- assisted me in brain storming, debugging and validating this program,
-- the program itself is my own work. I understand that submitting code
-- which is the work of other individuals is a violation of the honor
-- code.  I also understand that if I knowingly give my original work to
-- another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/
/***************************** Include Files ********************************/

#include "xparameters.h"
#include "stdio.h"
#include "xstatus.h"

#include "platform.h"
#include "xil_printf.h"						// Contains xil_printf
#include <xuartlite_l.h>					// Contains XUartLite_RecvByte
#include <xil_io.h>							// Contains Xil_Out8 and its variations
#include <xil_exception.h>

/************************** Constant Definitions ****************************/

/*
 * The following constants define the slave registers used for Read
 */
#define readBase		0x44a00000
#define	ctrlReg			readBase+4			// 2 LSBs of slv_reg1 are control
#define	clearReg		readBase+0xc		// 1 LSBs of slv_reg3 (0) roll clear flag
#define Lbusout    		readBase+20
#define Rbusout   		readBase+24
#define TrigVolt   		readBase+52        //10 LSBs of slv_reg13 for trig_volt value (0x34)
#define TrigTime   		readBase+56        //10 LSBs of slv_reg14 for trig_volt value (0x38)

/*
 * The following constants define the slave registers used for Write
 */
#define writeBase		0x44a00000
#define exWrAddr		writebase+32
#define exWen			writebase+28
#define exLbus			writebase+36
#define exRbus			writebase+40

#define printf xil_printf			/* A smaller footprint printf */

#define	uartRegAddr			0x40600000		// read <= RX, write => TX

/************************** Function Prototypes ****************************/
void myISR(void);

/************************** Variable Definitions **************************/
/*
 * The following are declared globally so they are zeroed and so they are
 * easily accessible from a debugger
 */
u16 isrCount = 0;

int main(void) {

	unsigned char c;

	init_platform();

	print("Welcome to Lecture 19\n\r");

    microblaze_enable_interrupts();

    while(1) {

    	c=XUartLite_RecvByte(uartRegAddr);

		switch(c) {

    		/*-------------------------------------------------
    		 * Reply with the help menu
    		 *-------------------------------------------------
			 */
    		case '?':
    			printf("--------------------------\r\n");
    			printf("    Trig_volt = %x\r\n", Xil_In16(TrigVolt));
    			printf("    Trig_time = %x\r\n", Xil_In16(TrigTime));
    			printf("    Lbusout = %x\r\n", Xil_In16(Lbusout));
    			printf("    Rbusout = %x\r\n", Xil_In16(Rbusout));
    			printf("--------------------------\r\n");
    			printf("?: help menu\r\n");
    			printf("o: k\r\n");
    			printf("f:   flush terminal\r\n");
    			break;

			/*-------------------------------------------------
			 * Basic I/O loopback
			 *-------------------------------------------------
			 */
    		case 'o':
    			printf("k \r\n");
    			break;

    	    /*-------------------------------------------------
    	     * Gate Check 3
    	     *-------------------------------------------------
    	     */
    		case 'd':     // some of these will be XIL commands
				for (int i=0;i<1024;i++) {
				   exWrAddr = i; // set BRAM address
				   exLBus = 185; // row for horz line
								 // need to shift to upper 10bits?
				   exRBus = i;   // diagnonal line [need to shift?]
				   exWen = 1;    // write data to address in BRAM
				   exWen = 0;    // turn off write
				}
			break;

			/*-------------------------------------------------
			 * Clear the terminal window
			 *-------------------------------------------------
			 */
            case 'f':
            	for (c=0; c<40; c++) printf("\r\n");
               	break;

			/*-------------------------------------------------
			 * Unknown character was
			 *-------------------------------------------------
			 */
    		default:
    			printf("unrecognized character: %c\r\n",c);
    			break;
    	} // end case

    } // end while 1

    cleanup_platform();

    return 0;
} // end main
