// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
// Date        : Thu Mar 17 19:15:09 2022
// Host        : DESKTOP-DNF5IPP running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_my_lab3_ip_0_1_sim_netlist.v
// Design      : design_1_my_lab3_ip_0_1
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a200tsbg484-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Audio_Codec_Wrapper
   (ac_mclk,
    ac_bclk,
    BCLK_int_reg,
    ac_lrclk,
    E,
    \UnL_bus_out2_reg[10] ,
    w_sw,
    CO,
    DIBDI,
    \L_bus_in_reg[17] ,
    D,
    \sdp_bl.ramb18_dp_bl.ram18_bl ,
    \R_bus_in_reg[17] ,
    ac_dac_sdata,
    scl,
    sda,
    s00_axi_aclk,
    ac_adc_sdata,
    s00_axi_aresetn,
    Q,
    \R_bus_in_reg[17]_0 ,
    \trigger_volt_reg[9] ,
    \slv_reg9_reg[15] ,
    switch,
    \trigger_volt_reg[8] ,
    \slv_reg10_reg[15] ,
    S,
    lopt);
  output ac_mclk;
  output ac_bclk;
  output BCLK_int_reg;
  output ac_lrclk;
  output [0:0]E;
  output [0:0]\UnL_bus_out2_reg[10] ;
  output [0:0]w_sw;
  output [0:0]CO;
  output [15:0]DIBDI;
  output [17:0]\L_bus_in_reg[17] ;
  output [0:0]D;
  output [15:0]\sdp_bl.ramb18_dp_bl.ram18_bl ;
  output [17:0]\R_bus_in_reg[17] ;
  output ac_dac_sdata;
  inout scl;
  inout sda;
  input s00_axi_aclk;
  input ac_adc_sdata;
  input s00_axi_aresetn;
  input [17:0]Q;
  input [17:0]\R_bus_in_reg[17]_0 ;
  input [0:0]\trigger_volt_reg[9] ;
  input [15:0]\slv_reg9_reg[15] ;
  input [0:0]switch;
  input [7:0]\trigger_volt_reg[8] ;
  input [15:0]\slv_reg10_reg[15] ;
  input [0:0]S;
  input lopt;

  wire BCLK_int_reg;
  wire [0:0]CO;
  wire [0:0]D;
  wire [15:0]DIBDI;
  wire [0:0]E;
  wire [17:0]\L_bus_in_reg[17] ;
  wire [17:0]Q;
  wire [17:0]\R_bus_in_reg[17] ;
  wire [17:0]\R_bus_in_reg[17]_0 ;
  wire [0:0]S;
  wire [0:0]\UnL_bus_out2_reg[10] ;
  wire ac_adc_sdata;
  wire ac_bclk;
  wire ac_dac_sdata;
  wire ac_lrclk;
  wire [2:0]ac_lrclk_count;
  wire \ac_lrclk_count[0]_i_1_n_0 ;
  wire \ac_lrclk_count[1]_i_1_n_0 ;
  wire \ac_lrclk_count[2]_i_1_n_0 ;
  wire ac_lrclk_sig_prev_reg_n_0;
  wire ac_mclk;
  wire audio_inout_n_3;
  wire audio_inout_n_4;
  wire clk_50;
  wire lopt;
  wire s00_axi_aclk;
  wire s00_axi_aresetn;
  wire scl;
  wire sda;
  wire [15:0]\sdp_bl.ramb18_dp_bl.ram18_bl ;
  wire [15:0]\slv_reg10_reg[15] ;
  wire [15:0]\slv_reg9_reg[15] ;
  wire [0:0]switch;
  wire [7:0]\trigger_volt_reg[8] ;
  wire [0:0]\trigger_volt_reg[9] ;
  wire [0:0]w_sw;

  LUT2 #(
    .INIT(4'h8)) 
    \UnL_bus_out2[17]_i_1 
       (.I0(s00_axi_aresetn),
        .I1(E),
        .O(\UnL_bus_out2_reg[10] ));
  (* SOFT_HLUTNM = "soft_lutpair77" *) 
  LUT4 #(
    .INIT(16'h8A20)) 
    \ac_lrclk_count[0]_i_1 
       (.I0(s00_axi_aresetn),
        .I1(ac_lrclk_sig_prev_reg_n_0),
        .I2(ac_lrclk),
        .I3(ac_lrclk_count[0]),
        .O(\ac_lrclk_count[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair77" *) 
  LUT5 #(
    .INIT(32'h8A20AA00)) 
    \ac_lrclk_count[1]_i_1 
       (.I0(s00_axi_aresetn),
        .I1(ac_lrclk_sig_prev_reg_n_0),
        .I2(ac_lrclk),
        .I3(ac_lrclk_count[1]),
        .I4(ac_lrclk_count[0]),
        .O(\ac_lrclk_count[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8AAAAAAA20000000)) 
    \ac_lrclk_count[2]_i_1 
       (.I0(s00_axi_aresetn),
        .I1(ac_lrclk_sig_prev_reg_n_0),
        .I2(ac_lrclk),
        .I3(ac_lrclk_count[1]),
        .I4(ac_lrclk_count[0]),
        .I5(ac_lrclk_count[2]),
        .O(\ac_lrclk_count[2]_i_1_n_0 ));
  FDRE \ac_lrclk_count_reg[0] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ac_lrclk_count[0]_i_1_n_0 ),
        .Q(ac_lrclk_count[0]),
        .R(1'b0));
  FDRE \ac_lrclk_count_reg[1] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ac_lrclk_count[1]_i_1_n_0 ),
        .Q(ac_lrclk_count[1]),
        .R(1'b0));
  FDRE \ac_lrclk_count_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ac_lrclk_count[2]_i_1_n_0 ),
        .Q(ac_lrclk_count[2]),
        .R(1'b0));
  FDRE ac_lrclk_sig_prev_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(audio_inout_n_3),
        .Q(ac_lrclk_sig_prev_reg_n_0),
        .R(1'b0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_i2s_ctl audio_inout
       (.CO(CO),
        .D(D),
        .DIBDI(DIBDI),
        .E(E),
        .\L_bus_in_reg[17] (\L_bus_in_reg[17] ),
        .Q(Q),
        .\R_bus_in_reg[17] (\R_bus_in_reg[17] ),
        .\R_bus_in_reg[17]_0 (\R_bus_in_reg[17]_0 ),
        .S(S),
        .SR(BCLK_int_reg),
        .ac_adc_sdata(ac_adc_sdata),
        .ac_bclk(ac_bclk),
        .ac_dac_sdata(ac_dac_sdata),
        .ac_lrclk(ac_lrclk),
        .ac_lrclk_count(ac_lrclk_count),
        .ac_lrclk_sig_prev_reg(audio_inout_n_3),
        .ac_lrclk_sig_prev_reg_0(ac_lrclk_sig_prev_reg_n_0),
        .ready_sig_reg(audio_inout_n_4),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_aresetn(s00_axi_aresetn),
        .\sdp_bl.ramb18_dp_bl.ram18_bl (\sdp_bl.ramb18_dp_bl.ram18_bl ),
        .\slv_reg10_reg[15] (\slv_reg10_reg[15] ),
        .\slv_reg9_reg[15] (\slv_reg9_reg[15] ),
        .switch(switch),
        .\trigger_volt_reg[8] (\trigger_volt_reg[8] ),
        .\trigger_volt_reg[9] (\trigger_volt_reg[9] ),
        .w_sw(w_sw));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_1 audiocodec_master_clock
       (.clk_in1(s00_axi_aclk),
        .clk_out1(ac_mclk),
        .clk_out2(clk_50),
        .lopt(lopt),
        .resetn(s00_axi_aresetn));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_audio_init initialize_audio
       (.SR(BCLK_int_reg),
        .clk_out2(clk_50),
        .s00_axi_aresetn(s00_axi_aresetn),
        .scl(scl),
        .sda(sda));
  FDRE ready_sig_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(audio_inout_n_4),
        .Q(E),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Scopeface
   (CO,
    \encoded_reg[8] ,
    \encoded_reg[8]_0 ,
    \encoded_reg[8]_1 ,
    \encoded_reg[8]_2 ,
    \encoded_reg[8]_3 ,
    \encoded_reg[8]_4 ,
    \encoded_reg[8]_5 ,
    \encoded_reg[8]_6 ,
    \encoded_reg[8]_7 ,
    \encoded_reg[8]_8 ,
    \encoded_reg[8]_9 ,
    \encoded_reg[8]_10 ,
    \encoded_reg[8]_11 ,
    \encoded_reg[9] ,
    encoded0_in,
    \encoded_reg[9]_0 ,
    \encoded_reg[9]_1 ,
    \encoded_reg[9]_2 ,
    S,
    \trigger_time_reg[0] ,
    \trigger_time_reg[8] ,
    \trigger_time_reg[7] ,
    \trigger_time_reg[8]_0 ,
    \trigger_volt_reg[9] ,
    \trigger_volt_reg[9]_0 ,
    \trigger_volt_reg[8] ,
    \trigger_volt_reg[7] ,
    \trigger_volt_reg[7]_0 ,
    Q,
    \processRow_reg[0] ,
    \processCol_reg[9] ,
    \processCol_reg[8] ,
    \processCol_reg[6] ,
    \dc_bias_reg[3] ,
    \dc_bias_reg[1] ,
    \dc_bias_reg[1]_0 ,
    \processCol_reg[0] ,
    \processRow_reg[7] ,
    switch,
    \processRow_reg[9] ,
    \dc_bias_reg[1]_1 ,
    \processCol_reg[1] ,
    \processCol_reg[2] ,
    \processRow_reg[2] ,
    \processRow_reg[7]_0 ,
    \processRow_reg[4] ,
    \processRow_reg[9]_0 ,
    \processCol_reg[9]_0 ,
    \processRow_reg[9]_1 ,
    \processRow_reg[7]_1 ,
    \processRow_reg[4]_0 ,
    \dc_bias_reg[3]_0 ,
    \dc_bias_reg[3]_1 ,
    \dc_bias_reg[3]_2 );
  output [0:0]CO;
  output [0:0]\encoded_reg[8] ;
  output [0:0]\encoded_reg[8]_0 ;
  output [0:0]\encoded_reg[8]_1 ;
  output [0:0]\encoded_reg[8]_2 ;
  output [0:0]\encoded_reg[8]_3 ;
  output [0:0]\encoded_reg[8]_4 ;
  output [0:0]\encoded_reg[8]_5 ;
  output [0:0]\encoded_reg[8]_6 ;
  output [0:0]\encoded_reg[8]_7 ;
  output \encoded_reg[8]_8 ;
  output \encoded_reg[8]_9 ;
  output \encoded_reg[8]_10 ;
  output \encoded_reg[8]_11 ;
  output \encoded_reg[9] ;
  output [0:0]encoded0_in;
  output \encoded_reg[9]_0 ;
  output \encoded_reg[9]_1 ;
  output \encoded_reg[9]_2 ;
  input [3:0]S;
  input [3:0]\trigger_time_reg[0] ;
  input [3:0]\trigger_time_reg[8] ;
  input [3:0]\trigger_time_reg[7] ;
  input [3:0]\trigger_time_reg[8]_0 ;
  input [3:0]\trigger_volt_reg[9] ;
  input [3:0]\trigger_volt_reg[9]_0 ;
  input [3:0]\trigger_volt_reg[8] ;
  input [3:0]\trigger_volt_reg[7] ;
  input [3:0]\trigger_volt_reg[7]_0 ;
  input [9:0]Q;
  input [0:0]\processRow_reg[0] ;
  input [9:0]\processCol_reg[9] ;
  input \processCol_reg[8] ;
  input \processCol_reg[6] ;
  input [1:0]\dc_bias_reg[3] ;
  input \dc_bias_reg[1] ;
  input \dc_bias_reg[1]_0 ;
  input \processCol_reg[0] ;
  input \processRow_reg[7] ;
  input [1:0]switch;
  input [0:0]\processRow_reg[9] ;
  input \dc_bias_reg[1]_1 ;
  input \processCol_reg[1] ;
  input \processCol_reg[2] ;
  input \processRow_reg[2] ;
  input \processRow_reg[7]_0 ;
  input \processRow_reg[4] ;
  input [0:0]\processRow_reg[9]_0 ;
  input \processCol_reg[9]_0 ;
  input \processRow_reg[9]_1 ;
  input \processRow_reg[7]_1 ;
  input \processRow_reg[4]_0 ;
  input \dc_bias_reg[3]_0 ;
  input [0:0]\dc_bias_reg[3]_1 ;
  input [0:0]\dc_bias_reg[3]_2 ;

  wire [0:0]CO;
  wire [9:0]Q;
  wire R10_carry_n_1;
  wire R10_carry_n_2;
  wire R10_carry_n_3;
  wire R13_carry_n_1;
  wire R13_carry_n_2;
  wire R13_carry_n_3;
  wire R14_carry_n_1;
  wire R14_carry_n_2;
  wire R14_carry_n_3;
  wire R15_carry_n_1;
  wire R15_carry_n_2;
  wire R15_carry_n_3;
  wire R16_carry_n_1;
  wire R16_carry_n_2;
  wire R16_carry_n_3;
  wire R17_carry_n_1;
  wire R17_carry_n_2;
  wire R17_carry_n_3;
  wire \R21_inferred__0/i___20_carry__0_n_3 ;
  wire \R21_inferred__0/i___20_carry__0_n_6 ;
  wire \R21_inferred__0/i___20_carry__0_n_7 ;
  wire \R21_inferred__0/i___20_carry_n_0 ;
  wire \R21_inferred__0/i___20_carry_n_1 ;
  wire \R21_inferred__0/i___20_carry_n_2 ;
  wire \R21_inferred__0/i___20_carry_n_3 ;
  wire \R21_inferred__0/i___20_carry_n_4 ;
  wire \R21_inferred__0/i___20_carry_n_5 ;
  wire \R21_inferred__0/i___20_carry_n_6 ;
  wire \R21_inferred__0/i___20_carry_n_7 ;
  wire \R21_inferred__0/i__carry__0_n_0 ;
  wire \R21_inferred__0/i__carry__0_n_1 ;
  wire \R21_inferred__0/i__carry__0_n_2 ;
  wire \R21_inferred__0/i__carry__0_n_3 ;
  wire \R21_inferred__0/i__carry__0_n_4 ;
  wire \R21_inferred__0/i__carry__1_n_0 ;
  wire \R21_inferred__0/i__carry__1_n_2 ;
  wire \R21_inferred__0/i__carry__1_n_3 ;
  wire \R21_inferred__0/i__carry__1_n_5 ;
  wire \R21_inferred__0/i__carry__1_n_6 ;
  wire \R21_inferred__0/i__carry__1_n_7 ;
  wire \R21_inferred__0/i__carry_n_0 ;
  wire \R21_inferred__0/i__carry_n_1 ;
  wire \R21_inferred__0/i__carry_n_2 ;
  wire \R21_inferred__0/i__carry_n_3 ;
  wire R22__23_carry_i_1_n_0;
  wire R22__23_carry_i_2_n_0;
  wire R22__23_carry_i_3_n_0;
  wire R22__23_carry_i_4_n_0;
  wire R22__23_carry_n_2;
  wire R22__23_carry_n_3;
  wire R22__23_carry_n_5;
  wire R22__23_carry_n_6;
  wire R22__23_carry_n_7;
  wire R22__29_carry__0_i_1_n_0;
  wire R22__29_carry__0_i_2_n_0;
  wire R22__29_carry__0_i_3_n_0;
  wire R22__29_carry__0_n_2;
  wire R22__29_carry__0_n_3;
  wire R22__29_carry__0_n_5;
  wire R22__29_carry__0_n_6;
  wire R22__29_carry__0_n_7;
  wire R22__29_carry_i_1_n_0;
  wire R22__29_carry_i_2_n_0;
  wire R22__29_carry_i_3_n_0;
  wire R22__29_carry_n_0;
  wire R22__29_carry_n_1;
  wire R22__29_carry_n_2;
  wire R22__29_carry_n_3;
  wire R22__29_carry_n_4;
  wire R22__29_carry_n_5;
  wire R22__29_carry_n_6;
  wire R22__29_carry_n_7;
  wire R22_carry__0_i_1_n_0;
  wire R22_carry__0_i_2_n_0;
  wire R22_carry__0_i_3_n_0;
  wire R22_carry__0_i_4_n_0;
  wire R22_carry__0_n_0;
  wire R22_carry__0_n_1;
  wire R22_carry__0_n_2;
  wire R22_carry__0_n_3;
  wire R22_carry__0_n_4;
  wire R22_carry__1_i_1_n_0;
  wire R22_carry__1_n_0;
  wire R22_carry__1_n_2;
  wire R22_carry__1_n_3;
  wire R22_carry__1_n_5;
  wire R22_carry__1_n_6;
  wire R22_carry__1_n_7;
  wire R22_carry_i_1_n_0;
  wire R22_carry_i_2_n_0;
  wire R22_carry_i_3_n_0;
  wire R22_carry_n_0;
  wire R22_carry_n_1;
  wire R22_carry_n_2;
  wire R22_carry_n_3;
  wire R6_carry_n_1;
  wire R6_carry_n_2;
  wire R6_carry_n_3;
  wire R7_carry_n_1;
  wire R7_carry_n_2;
  wire R7_carry_n_3;
  wire R8_carry_n_1;
  wire R8_carry_n_2;
  wire R8_carry_n_3;
  wire R9_carry_n_1;
  wire R9_carry_n_2;
  wire R9_carry_n_3;
  wire [3:0]S;
  wire \dc_bias[3]_i_11_n_0 ;
  wire \dc_bias[3]_i_12_n_0 ;
  wire \dc_bias[3]_i_23_n_0 ;
  wire \dc_bias[3]_i_24_n_0 ;
  wire \dc_bias_reg[1] ;
  wire \dc_bias_reg[1]_0 ;
  wire \dc_bias_reg[1]_1 ;
  wire [1:0]\dc_bias_reg[3] ;
  wire \dc_bias_reg[3]_0 ;
  wire [0:0]\dc_bias_reg[3]_1 ;
  wire [0:0]\dc_bias_reg[3]_2 ;
  wire [0:0]encoded0_in;
  wire [0:0]\encoded_reg[8] ;
  wire [0:0]\encoded_reg[8]_0 ;
  wire [0:0]\encoded_reg[8]_1 ;
  wire \encoded_reg[8]_10 ;
  wire \encoded_reg[8]_11 ;
  wire [0:0]\encoded_reg[8]_2 ;
  wire [0:0]\encoded_reg[8]_3 ;
  wire [0:0]\encoded_reg[8]_4 ;
  wire [0:0]\encoded_reg[8]_5 ;
  wire [0:0]\encoded_reg[8]_6 ;
  wire [0:0]\encoded_reg[8]_7 ;
  wire \encoded_reg[8]_8 ;
  wire \encoded_reg[8]_9 ;
  wire \encoded_reg[9] ;
  wire \encoded_reg[9]_0 ;
  wire \encoded_reg[9]_1 ;
  wire \encoded_reg[9]_2 ;
  wire i___20_carry__0_i_1_n_0;
  wire i___20_carry__0_i_2_n_0;
  wire i___20_carry_i_1_n_0;
  wire i___20_carry_i_2_n_0;
  wire i___20_carry_i_3_n_0;
  wire i___20_carry_i_4_n_0;
  wire i__carry__0_i_1_n_0;
  wire i__carry__0_i_2_n_0;
  wire i__carry__0_i_3_n_0;
  wire i__carry_i_1__0_n_0;
  wire i__carry_i_2_n_0;
  wire i__carry_i_3_n_0;
  wire \processCol_reg[0] ;
  wire \processCol_reg[1] ;
  wire \processCol_reg[2] ;
  wire \processCol_reg[6] ;
  wire \processCol_reg[8] ;
  wire [9:0]\processCol_reg[9] ;
  wire \processCol_reg[9]_0 ;
  wire [0:0]\processRow_reg[0] ;
  wire \processRow_reg[2] ;
  wire \processRow_reg[4] ;
  wire \processRow_reg[4]_0 ;
  wire \processRow_reg[7] ;
  wire \processRow_reg[7]_0 ;
  wire \processRow_reg[7]_1 ;
  wire [0:0]\processRow_reg[9] ;
  wire [0:0]\processRow_reg[9]_0 ;
  wire \processRow_reg[9]_1 ;
  wire [1:0]switch;
  wire [3:0]\trigger_time_reg[0] ;
  wire [3:0]\trigger_time_reg[7] ;
  wire [3:0]\trigger_time_reg[8] ;
  wire [3:0]\trigger_time_reg[8]_0 ;
  wire [3:0]\trigger_volt_reg[7] ;
  wire [3:0]\trigger_volt_reg[7]_0 ;
  wire [3:0]\trigger_volt_reg[8] ;
  wire [3:0]\trigger_volt_reg[9] ;
  wire [3:0]\trigger_volt_reg[9]_0 ;
  wire [3:0]NLW_R10_carry_O_UNCONNECTED;
  wire [3:0]NLW_R13_carry_O_UNCONNECTED;
  wire [3:0]NLW_R14_carry_O_UNCONNECTED;
  wire [3:0]NLW_R15_carry_O_UNCONNECTED;
  wire [3:0]NLW_R16_carry_O_UNCONNECTED;
  wire [3:0]NLW_R17_carry_O_UNCONNECTED;
  wire [3:1]\NLW_R21_inferred__0/i___20_carry__0_CO_UNCONNECTED ;
  wire [3:2]\NLW_R21_inferred__0/i___20_carry__0_O_UNCONNECTED ;
  wire [3:0]\NLW_R21_inferred__0/i__carry_O_UNCONNECTED ;
  wire [2:0]\NLW_R21_inferred__0/i__carry__0_O_UNCONNECTED ;
  wire [2:2]\NLW_R21_inferred__0/i__carry__1_CO_UNCONNECTED ;
  wire [3:3]\NLW_R21_inferred__0/i__carry__1_O_UNCONNECTED ;
  wire [3:2]NLW_R22__23_carry_CO_UNCONNECTED;
  wire [3:3]NLW_R22__23_carry_O_UNCONNECTED;
  wire [3:2]NLW_R22__29_carry__0_CO_UNCONNECTED;
  wire [3:3]NLW_R22__29_carry__0_O_UNCONNECTED;
  wire [3:0]NLW_R22_carry_O_UNCONNECTED;
  wire [2:0]NLW_R22_carry__0_O_UNCONNECTED;
  wire [2:2]NLW_R22_carry__1_CO_UNCONNECTED;
  wire [3:3]NLW_R22_carry__1_O_UNCONNECTED;
  wire [3:0]NLW_R6_carry_O_UNCONNECTED;
  wire [3:0]NLW_R7_carry_O_UNCONNECTED;
  wire [3:0]NLW_R8_carry_O_UNCONNECTED;
  wire [3:0]NLW_R9_carry_O_UNCONNECTED;

  CARRY4 R10_carry
       (.CI(1'b0),
        .CO({\encoded_reg[8]_3 ,R10_carry_n_1,R10_carry_n_2,R10_carry_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_R10_carry_O_UNCONNECTED[3:0]),
        .S(\trigger_volt_reg[9] ));
  CARRY4 R13_carry
       (.CI(1'b0),
        .CO({\encoded_reg[8]_2 ,R13_carry_n_1,R13_carry_n_2,R13_carry_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_R13_carry_O_UNCONNECTED[3:0]),
        .S(\trigger_time_reg[8]_0 ));
  CARRY4 R14_carry
       (.CI(1'b0),
        .CO({\encoded_reg[8]_1 ,R14_carry_n_1,R14_carry_n_2,R14_carry_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_R14_carry_O_UNCONNECTED[3:0]),
        .S(\trigger_time_reg[7] ));
  CARRY4 R15_carry
       (.CI(1'b0),
        .CO({\encoded_reg[8]_0 ,R15_carry_n_1,R15_carry_n_2,R15_carry_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_R15_carry_O_UNCONNECTED[3:0]),
        .S(\trigger_time_reg[8] ));
  CARRY4 R16_carry
       (.CI(1'b0),
        .CO({\encoded_reg[8] ,R16_carry_n_1,R16_carry_n_2,R16_carry_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_R16_carry_O_UNCONNECTED[3:0]),
        .S(\trigger_time_reg[0] ));
  CARRY4 R17_carry
       (.CI(1'b0),
        .CO({CO,R17_carry_n_1,R17_carry_n_2,R17_carry_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_R17_carry_O_UNCONNECTED[3:0]),
        .S(S));
  CARRY4 \R21_inferred__0/i___20_carry 
       (.CI(1'b0),
        .CO({\R21_inferred__0/i___20_carry_n_0 ,\R21_inferred__0/i___20_carry_n_1 ,\R21_inferred__0/i___20_carry_n_2 ,\R21_inferred__0/i___20_carry_n_3 }),
        .CYINIT(1'b1),
        .DI({\processCol_reg[9] [4:2],1'b1}),
        .O({\R21_inferred__0/i___20_carry_n_4 ,\R21_inferred__0/i___20_carry_n_5 ,\R21_inferred__0/i___20_carry_n_6 ,\R21_inferred__0/i___20_carry_n_7 }),
        .S({i___20_carry_i_1_n_0,i___20_carry_i_2_n_0,i___20_carry_i_3_n_0,i___20_carry_i_4_n_0}));
  CARRY4 \R21_inferred__0/i___20_carry__0 
       (.CI(\R21_inferred__0/i___20_carry_n_0 ),
        .CO({\NLW_R21_inferred__0/i___20_carry__0_CO_UNCONNECTED [3:1],\R21_inferred__0/i___20_carry__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,\processCol_reg[9] [5]}),
        .O({\NLW_R21_inferred__0/i___20_carry__0_O_UNCONNECTED [3:2],\R21_inferred__0/i___20_carry__0_n_6 ,\R21_inferred__0/i___20_carry__0_n_7 }),
        .S({1'b0,1'b0,i___20_carry__0_i_1_n_0,i___20_carry__0_i_2_n_0}));
  CARRY4 \R21_inferred__0/i__carry 
       (.CI(1'b0),
        .CO({\R21_inferred__0/i__carry_n_0 ,\R21_inferred__0/i__carry_n_1 ,\R21_inferred__0/i__carry_n_2 ,\R21_inferred__0/i__carry_n_3 }),
        .CYINIT(1'b0),
        .DI({\processCol_reg[9] [6:4],1'b0}),
        .O(\NLW_R21_inferred__0/i__carry_O_UNCONNECTED [3:0]),
        .S({i__carry_i_1__0_n_0,i__carry_i_2_n_0,i__carry_i_3_n_0,\processCol_reg[9] [3]}));
  CARRY4 \R21_inferred__0/i__carry__0 
       (.CI(\R21_inferred__0/i__carry_n_0 ),
        .CO({\R21_inferred__0/i__carry__0_n_0 ,\R21_inferred__0/i__carry__0_n_1 ,\R21_inferred__0/i__carry__0_n_2 ,\R21_inferred__0/i__carry__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,\processCol_reg[9] [9:7]}),
        .O({\R21_inferred__0/i__carry__0_n_4 ,\NLW_R21_inferred__0/i__carry__0_O_UNCONNECTED [2:0]}),
        .S({\processCol_reg[9] [6],i__carry__0_i_1_n_0,i__carry__0_i_2_n_0,i__carry__0_i_3_n_0}));
  CARRY4 \R21_inferred__0/i__carry__1 
       (.CI(\R21_inferred__0/i__carry__0_n_0 ),
        .CO({\R21_inferred__0/i__carry__1_n_0 ,\NLW_R21_inferred__0/i__carry__1_CO_UNCONNECTED [2],\R21_inferred__0/i__carry__1_n_2 ,\R21_inferred__0/i__carry__1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_R21_inferred__0/i__carry__1_O_UNCONNECTED [3],\R21_inferred__0/i__carry__1_n_5 ,\R21_inferred__0/i__carry__1_n_6 ,\R21_inferred__0/i__carry__1_n_7 }),
        .S({1'b1,\processCol_reg[9] [9:7]}));
  CARRY4 R22__23_carry
       (.CI(1'b0),
        .CO({NLW_R22__23_carry_CO_UNCONNECTED[3:2],R22__23_carry_n_2,R22__23_carry_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,R22__23_carry_i_1_n_0,1'b0}),
        .O({NLW_R22__23_carry_O_UNCONNECTED[3],R22__23_carry_n_5,R22__23_carry_n_6,R22__23_carry_n_7}),
        .S({1'b0,R22__23_carry_i_2_n_0,R22__23_carry_i_3_n_0,R22__23_carry_i_4_n_0}));
  LUT2 #(
    .INIT(4'hB)) 
    R22__23_carry_i_1
       (.I0(R22_carry__1_n_5),
        .I1(R22_carry__0_n_4),
        .O(R22__23_carry_i_1_n_0));
  LUT4 #(
    .INIT(16'hD22D)) 
    R22__23_carry_i_2
       (.I0(R22_carry__1_n_0),
        .I1(R22_carry__1_n_7),
        .I2(R22_carry__1_n_6),
        .I3(R22_carry__0_n_4),
        .O(R22__23_carry_i_2_n_0));
  LUT4 #(
    .INIT(16'h2DD2)) 
    R22__23_carry_i_3
       (.I0(R22_carry__0_n_4),
        .I1(R22_carry__1_n_5),
        .I2(R22_carry__1_n_0),
        .I3(R22_carry__1_n_7),
        .O(R22__23_carry_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    R22__23_carry_i_4
       (.I0(R22_carry__1_n_5),
        .I1(R22_carry__0_n_4),
        .O(R22__23_carry_i_4_n_0));
  CARRY4 R22__29_carry
       (.CI(1'b0),
        .CO({R22__29_carry_n_0,R22__29_carry_n_1,R22__29_carry_n_2,R22__29_carry_n_3}),
        .CYINIT(1'b1),
        .DI(Q[3:0]),
        .O({R22__29_carry_n_4,R22__29_carry_n_5,R22__29_carry_n_6,R22__29_carry_n_7}),
        .S({R22__29_carry_i_1_n_0,R22__29_carry_i_2_n_0,R22__29_carry_i_3_n_0,\processRow_reg[0] }));
  CARRY4 R22__29_carry__0
       (.CI(R22__29_carry_n_0),
        .CO({NLW_R22__29_carry__0_CO_UNCONNECTED[3:2],R22__29_carry__0_n_2,R22__29_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,Q[5:4]}),
        .O({NLW_R22__29_carry__0_O_UNCONNECTED[3],R22__29_carry__0_n_5,R22__29_carry__0_n_6,R22__29_carry__0_n_7}),
        .S({1'b0,R22__29_carry__0_i_1_n_0,R22__29_carry__0_i_2_n_0,R22__29_carry__0_i_3_n_0}));
  LUT2 #(
    .INIT(4'h9)) 
    R22__29_carry__0_i_1
       (.I0(Q[6]),
        .I1(R22__23_carry_n_5),
        .O(R22__29_carry__0_i_1_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    R22__29_carry__0_i_2
       (.I0(Q[5]),
        .I1(R22__23_carry_n_6),
        .O(R22__29_carry__0_i_2_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    R22__29_carry__0_i_3
       (.I0(Q[4]),
        .I1(R22__23_carry_n_7),
        .O(R22__29_carry__0_i_3_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    R22__29_carry_i_1
       (.I0(Q[3]),
        .I1(R22_carry__1_n_6),
        .O(R22__29_carry_i_1_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    R22__29_carry_i_2
       (.I0(Q[2]),
        .I1(R22_carry__1_n_7),
        .O(R22__29_carry_i_2_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    R22__29_carry_i_3
       (.I0(Q[1]),
        .I1(R22_carry__0_n_4),
        .O(R22__29_carry_i_3_n_0));
  CARRY4 R22_carry
       (.CI(1'b0),
        .CO({R22_carry_n_0,R22_carry_n_1,R22_carry_n_2,R22_carry_n_3}),
        .CYINIT(1'b0),
        .DI({Q[4:2],1'b0}),
        .O(NLW_R22_carry_O_UNCONNECTED[3:0]),
        .S({R22_carry_i_1_n_0,R22_carry_i_2_n_0,R22_carry_i_3_n_0,Q[1]}));
  CARRY4 R22_carry__0
       (.CI(R22_carry_n_0),
        .CO({R22_carry__0_n_0,R22_carry__0_n_1,R22_carry__0_n_2,R22_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI(Q[8:5]),
        .O({R22_carry__0_n_4,NLW_R22_carry__0_O_UNCONNECTED[2:0]}),
        .S({R22_carry__0_i_1_n_0,R22_carry__0_i_2_n_0,R22_carry__0_i_3_n_0,R22_carry__0_i_4_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    R22_carry__0_i_1
       (.I0(Q[8]),
        .I1(Q[6]),
        .O(R22_carry__0_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    R22_carry__0_i_2
       (.I0(Q[7]),
        .I1(Q[5]),
        .O(R22_carry__0_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    R22_carry__0_i_3
       (.I0(Q[6]),
        .I1(Q[4]),
        .O(R22_carry__0_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    R22_carry__0_i_4
       (.I0(Q[5]),
        .I1(Q[3]),
        .O(R22_carry__0_i_4_n_0));
  CARRY4 R22_carry__1
       (.CI(R22_carry__0_n_0),
        .CO({R22_carry__1_n_0,NLW_R22_carry__1_CO_UNCONNECTED[2],R22_carry__1_n_2,R22_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,Q[9]}),
        .O({NLW_R22_carry__1_O_UNCONNECTED[3],R22_carry__1_n_5,R22_carry__1_n_6,R22_carry__1_n_7}),
        .S({1'b1,Q[9:8],R22_carry__1_i_1_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    R22_carry__1_i_1
       (.I0(Q[9]),
        .I1(Q[7]),
        .O(R22_carry__1_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    R22_carry_i_1
       (.I0(Q[4]),
        .I1(Q[2]),
        .O(R22_carry_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    R22_carry_i_2
       (.I0(Q[3]),
        .I1(Q[1]),
        .O(R22_carry_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    R22_carry_i_3
       (.I0(Q[2]),
        .I1(Q[0]),
        .O(R22_carry_i_3_n_0));
  CARRY4 R6_carry
       (.CI(1'b0),
        .CO({\encoded_reg[8]_7 ,R6_carry_n_1,R6_carry_n_2,R6_carry_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_R6_carry_O_UNCONNECTED[3:0]),
        .S(\trigger_volt_reg[7]_0 ));
  CARRY4 R7_carry
       (.CI(1'b0),
        .CO({\encoded_reg[8]_6 ,R7_carry_n_1,R7_carry_n_2,R7_carry_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_R7_carry_O_UNCONNECTED[3:0]),
        .S(\trigger_volt_reg[7] ));
  CARRY4 R8_carry
       (.CI(1'b0),
        .CO({\encoded_reg[8]_5 ,R8_carry_n_1,R8_carry_n_2,R8_carry_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_R8_carry_O_UNCONNECTED[3:0]),
        .S(\trigger_volt_reg[8] ));
  CARRY4 R9_carry
       (.CI(1'b0),
        .CO({\encoded_reg[8]_4 ,R9_carry_n_1,R9_carry_n_2,R9_carry_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_R9_carry_O_UNCONNECTED[3:0]),
        .S(\trigger_volt_reg[9]_0 ));
  LUT6 #(
    .INIT(64'h00C3AAC355C355C3)) 
    \TDMS_encoder_blue/encoded[9]_i_1 
       (.I0(\dc_bias_reg[3] [1]),
        .I1(\processRow_reg[4]_0 ),
        .I2(\processCol_reg[8] ),
        .I3(\processCol_reg[6] ),
        .I4(\dc_bias_reg[3]_0 ),
        .I5(\encoded_reg[8]_9 ),
        .O(\encoded_reg[9]_0 ));
  LUT3 #(
    .INIT(8'hD7)) 
    \TDMS_encoder_green/encoded[9]_i_1 
       (.I0(\processCol_reg[6] ),
        .I1(\dc_bias_reg[3]_1 ),
        .I2(\encoded_reg[9] ),
        .O(\encoded_reg[9]_1 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'h7777D077)) 
    \TDMS_encoder_red/encoded[9]_i_1 
       (.I0(\processCol_reg[6] ),
        .I1(\dc_bias_reg[3]_2 ),
        .I2(\dc_bias_reg[1]_0 ),
        .I3(\encoded_reg[8]_9 ),
        .I4(\processCol_reg[0] ),
        .O(\encoded_reg[9]_2 ));
  LUT5 #(
    .INIT(32'hFFFFFFFD)) 
    \dc_bias[3]_i_11 
       (.I0(\processRow_reg[7]_1 ),
        .I1(\dc_bias[3]_i_23_n_0 ),
        .I2(\R21_inferred__0/i___20_carry_n_5 ),
        .I3(\R21_inferred__0/i___20_carry__0_n_7 ),
        .I4(\R21_inferred__0/i___20_carry_n_7 ),
        .O(\dc_bias[3]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000200200)) 
    \dc_bias[3]_i_12 
       (.I0(\processCol_reg[9]_0 ),
        .I1(\dc_bias[3]_i_24_n_0 ),
        .I2(R22__29_carry_n_6),
        .I3(R22__29_carry__0_n_7),
        .I4(R22__29_carry__0_n_5),
        .I5(\processRow_reg[9]_1 ),
        .O(\dc_bias[3]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000D00)) 
    \dc_bias[3]_i_2 
       (.I0(\processCol_reg[1] ),
        .I1(\processCol_reg[2] ),
        .I2(\dc_bias[3]_i_12_n_0 ),
        .I3(\dc_bias[3]_i_11_n_0 ),
        .I4(\processRow_reg[2] ),
        .I5(\processRow_reg[7]_0 ),
        .O(\encoded_reg[9] ));
  LUT4 #(
    .INIT(16'hFBBF)) 
    \dc_bias[3]_i_23 
       (.I0(\processCol_reg[9] [0]),
        .I1(\R21_inferred__0/i___20_carry_n_4 ),
        .I2(\R21_inferred__0/i___20_carry__0_n_6 ),
        .I3(\R21_inferred__0/i___20_carry_n_6 ),
        .O(\dc_bias[3]_i_23_n_0 ));
  LUT4 #(
    .INIT(16'hFFFD)) 
    \dc_bias[3]_i_24 
       (.I0(R22__29_carry_n_5),
        .I1(R22__29_carry_n_4),
        .I2(R22__29_carry__0_n_6),
        .I3(R22__29_carry_n_7),
        .O(\dc_bias[3]_i_24_n_0 ));
  LUT5 #(
    .INIT(32'h04000404)) 
    \dc_bias[3]_i_5 
       (.I0(\processRow_reg[2] ),
        .I1(\dc_bias[3]_i_11_n_0 ),
        .I2(\dc_bias[3]_i_12_n_0 ),
        .I3(\processCol_reg[2] ),
        .I4(\processCol_reg[1] ),
        .O(\encoded_reg[8]_9 ));
  LUT5 #(
    .INIT(32'h00007F00)) 
    \encoded[8]_i_1__0 
       (.I0(\processRow_reg[7] ),
        .I1(switch[1]),
        .I2(\processRow_reg[9] ),
        .I3(\encoded_reg[8]_9 ),
        .I4(\dc_bias_reg[1]_1 ),
        .O(\encoded_reg[8]_11 ));
  LUT6 #(
    .INIT(64'h0000000015550000)) 
    \encoded[8]_i_1__1 
       (.I0(\processRow_reg[4] ),
        .I1(\processRow_reg[9]_0 ),
        .I2(switch[0]),
        .I3(\processRow_reg[7] ),
        .I4(\encoded_reg[8]_9 ),
        .I5(\dc_bias_reg[1]_0 ),
        .O(encoded0_in));
  LUT6 #(
    .INIT(64'hEEEEEAEEAAAAA2AA)) 
    \encoded[8]_i_2 
       (.I0(\processCol_reg[8] ),
        .I1(\processCol_reg[6] ),
        .I2(\dc_bias_reg[3] [0]),
        .I3(\dc_bias_reg[1] ),
        .I4(\dc_bias_reg[3] [1]),
        .I5(\encoded_reg[8]_9 ),
        .O(\encoded_reg[8]_8 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'h44C4)) 
    \encoded[8]_i_2__1 
       (.I0(\processCol_reg[6] ),
        .I1(\dc_bias_reg[1]_0 ),
        .I2(\encoded_reg[8]_9 ),
        .I3(\processCol_reg[0] ),
        .O(\encoded_reg[8]_10 ));
  LUT6 #(
    .INIT(64'hFF0100FE00FEFF01)) 
    i___20_carry__0_i_1
       (.I0(\R21_inferred__0/i__carry__1_n_5 ),
        .I1(\R21_inferred__0/i__carry__1_n_7 ),
        .I2(\R21_inferred__0/i__carry__1_n_6 ),
        .I3(\R21_inferred__0/i__carry__0_n_4 ),
        .I4(\R21_inferred__0/i__carry__1_n_0 ),
        .I5(\processCol_reg[9] [6]),
        .O(i___20_carry__0_i_1_n_0));
  LUT5 #(
    .INIT(32'h5556AAA9)) 
    i___20_carry__0_i_2
       (.I0(\processCol_reg[9] [5]),
        .I1(\R21_inferred__0/i__carry__1_n_7 ),
        .I2(\R21_inferred__0/i__carry__0_n_4 ),
        .I3(\R21_inferred__0/i__carry__1_n_6 ),
        .I4(\R21_inferred__0/i__carry__1_n_5 ),
        .O(i___20_carry__0_i_2_n_0));
  LUT4 #(
    .INIT(16'h56A9)) 
    i___20_carry_i_1
       (.I0(\processCol_reg[9] [4]),
        .I1(\R21_inferred__0/i__carry__0_n_4 ),
        .I2(\R21_inferred__0/i__carry__1_n_7 ),
        .I3(\R21_inferred__0/i__carry__1_n_6 ),
        .O(i___20_carry_i_1_n_0));
  LUT3 #(
    .INIT(8'h69)) 
    i___20_carry_i_2
       (.I0(\processCol_reg[9] [3]),
        .I1(\R21_inferred__0/i__carry__1_n_7 ),
        .I2(\R21_inferred__0/i__carry__0_n_4 ),
        .O(i___20_carry_i_2_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    i___20_carry_i_3
       (.I0(\processCol_reg[9] [2]),
        .I1(\R21_inferred__0/i__carry__0_n_4 ),
        .O(i___20_carry_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i___20_carry_i_4
       (.I0(\processCol_reg[9] [1]),
        .O(i___20_carry_i_4_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    i__carry__0_i_1
       (.I0(\processCol_reg[9] [9]),
        .I1(\processCol_reg[9] [5]),
        .O(i__carry__0_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    i__carry__0_i_2
       (.I0(\processCol_reg[9] [8]),
        .I1(\processCol_reg[9] [4]),
        .O(i__carry__0_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    i__carry__0_i_3
       (.I0(\processCol_reg[9] [7]),
        .I1(\processCol_reg[9] [3]),
        .O(i__carry__0_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    i__carry_i_1__0
       (.I0(\processCol_reg[9] [6]),
        .I1(\processCol_reg[9] [2]),
        .O(i__carry_i_1__0_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    i__carry_i_2
       (.I0(\processCol_reg[9] [5]),
        .I1(\processCol_reg[9] [1]),
        .O(i__carry_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    i__carry_i_3
       (.I0(\processCol_reg[9] [4]),
        .I1(\processCol_reg[9] [0]),
        .O(i__carry_i_3_n_0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_TDMS_encoder
   (D,
    \encoded_reg[9]_0 ,
    Q,
    \encoded_reg[8]_0 ,
    \dc_bias_reg[2]_0 ,
    CLK,
    \dc_bias_reg[3]_0 ,
    \dc_bias_reg[3]_1 ,
    \dc_bias_reg[3]_2 ,
    \processRow_reg[2] ,
    \processCol_reg[6] ,
    SR);
  output [3:0]D;
  output \encoded_reg[9]_0 ;
  output [1:0]Q;
  output \encoded_reg[8]_0 ;
  input \dc_bias_reg[2]_0 ;
  input CLK;
  input \dc_bias_reg[3]_0 ;
  input \dc_bias_reg[3]_1 ;
  input \dc_bias_reg[3]_2 ;
  input \processRow_reg[2] ;
  input \processCol_reg[6] ;
  input [0:0]SR;

  wire CLK;
  wire [3:0]D;
  wire [1:0]Q;
  wire [0:0]SR;
  wire \dc_bias[0]_i_1__1_n_0 ;
  wire \dc_bias[1]_i_1_n_0 ;
  wire \dc_bias[2]_i_1_n_0 ;
  wire \dc_bias[3]_i_1_n_0 ;
  wire \dc_bias_reg[2]_0 ;
  wire \dc_bias_reg[3]_0 ;
  wire \dc_bias_reg[3]_1 ;
  wire \dc_bias_reg[3]_2 ;
  wire \dc_bias_reg_n_0_[0] ;
  wire \dc_bias_reg_n_0_[1] ;
  wire \encoded[8]_i_1_n_0 ;
  wire \encoded_reg[8]_0 ;
  wire \encoded_reg[9]_0 ;
  wire \processCol_reg[6] ;
  wire \processRow_reg[2] ;

  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \dc_bias[0]_i_1__1 
       (.I0(Q[1]),
        .I1(\dc_bias_reg_n_0_[0] ),
        .O(\dc_bias[0]_i_1__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT4 #(
    .INIT(16'h69AA)) 
    \dc_bias[1]_i_1 
       (.I0(\dc_bias_reg_n_0_[1] ),
        .I1(\dc_bias_reg_n_0_[0] ),
        .I2(\processRow_reg[2] ),
        .I3(Q[1]),
        .O(\dc_bias[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT5 #(
    .INIT(32'h807F02FD)) 
    \dc_bias[2]_i_1 
       (.I0(Q[1]),
        .I1(\dc_bias_reg_n_0_[1] ),
        .I2(\dc_bias_reg_n_0_[0] ),
        .I3(Q[0]),
        .I4(\processRow_reg[2] ),
        .O(\dc_bias[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT5 #(
    .INIT(32'h070F0F2F)) 
    \dc_bias[3]_i_1 
       (.I0(Q[1]),
        .I1(\processRow_reg[2] ),
        .I2(Q[0]),
        .I3(\dc_bias_reg_n_0_[0] ),
        .I4(\dc_bias_reg_n_0_[1] ),
        .O(\dc_bias[3]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \dc_bias_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(\dc_bias[0]_i_1__1_n_0 ),
        .Q(\dc_bias_reg_n_0_[0] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \dc_bias_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(\dc_bias[1]_i_1_n_0 ),
        .Q(\dc_bias_reg_n_0_[1] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \dc_bias_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(\dc_bias[2]_i_1_n_0 ),
        .Q(Q[0]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \dc_bias_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(\dc_bias[3]_i_1_n_0 ),
        .Q(Q[1]),
        .R(SR));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    \encoded[8]_i_1 
       (.I0(\processRow_reg[2] ),
        .I1(Q[1]),
        .I2(\dc_bias_reg_n_0_[1] ),
        .I3(\dc_bias_reg_n_0_[0] ),
        .I4(Q[0]),
        .I5(\processCol_reg[6] ),
        .O(\encoded[8]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \encoded[8]_i_4__0 
       (.I0(\dc_bias_reg_n_0_[1] ),
        .I1(\dc_bias_reg_n_0_[0] ),
        .O(\encoded_reg[8]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT4 #(
    .INIT(16'h0001)) 
    \encoded[9]_i_3 
       (.I0(Q[1]),
        .I1(\dc_bias_reg_n_0_[1] ),
        .I2(\dc_bias_reg_n_0_[0] ),
        .I3(Q[0]),
        .O(\encoded_reg[9]_0 ));
  FDRE \encoded_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(\dc_bias_reg[3]_1 ),
        .Q(D[0]),
        .R(1'b0));
  FDRE \encoded_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(\dc_bias_reg[3]_0 ),
        .Q(D[1]),
        .R(1'b0));
  FDSE \encoded_reg[8] 
       (.C(CLK),
        .CE(1'b1),
        .D(\dc_bias_reg[2]_0 ),
        .Q(D[2]),
        .S(\encoded[8]_i_1_n_0 ));
  FDRE \encoded_reg[9] 
       (.C(CLK),
        .CE(1'b1),
        .D(\dc_bias_reg[3]_2 ),
        .Q(D[3]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "TDMS_encoder" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_TDMS_encoder_0
   (D,
    Q,
    \encoded_reg[8]_0 ,
    \dc_bias_reg[1]_0 ,
    CLK,
    \dc_bias_reg[3]_0 ,
    \processCol_reg[6] ,
    \processCol_reg[1] ,
    SR);
  output [3:0]D;
  output [0:0]Q;
  output \encoded_reg[8]_0 ;
  input \dc_bias_reg[1]_0 ;
  input CLK;
  input \dc_bias_reg[3]_0 ;
  input \processCol_reg[6] ;
  input \processCol_reg[1] ;
  input [0:0]SR;

  wire CLK;
  wire [3:0]D;
  wire [0:0]Q;
  wire [0:0]SR;
  wire \dc_bias[0]_i_1__0_n_0 ;
  wire \dc_bias[1]_i_1__0_n_0 ;
  wire \dc_bias[2]_i_1__0_n_0 ;
  wire \dc_bias[3]_i_1__0_n_0 ;
  wire \dc_bias_reg[1]_0 ;
  wire \dc_bias_reg[3]_0 ;
  wire \dc_bias_reg_n_0_[0] ;
  wire \dc_bias_reg_n_0_[1] ;
  wire \dc_bias_reg_n_0_[2] ;
  wire \encoded[0]_i_1__0_n_0 ;
  wire \encoded[2]_i_1__0_n_0 ;
  wire \encoded[8]_i_2__0_n_0 ;
  wire \encoded_reg[8]_0 ;
  wire \processCol_reg[1] ;
  wire \processCol_reg[6] ;

  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \dc_bias[0]_i_1__0 
       (.I0(Q),
        .I1(\dc_bias_reg_n_0_[0] ),
        .O(\dc_bias[0]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT4 #(
    .INIT(16'h69AA)) 
    \dc_bias[1]_i_1__0 
       (.I0(\dc_bias_reg_n_0_[1] ),
        .I1(\dc_bias_reg_n_0_[0] ),
        .I2(\processCol_reg[1] ),
        .I3(Q),
        .O(\dc_bias[1]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT5 #(
    .INIT(32'h870F0F2D)) 
    \dc_bias[2]_i_1__0 
       (.I0(Q),
        .I1(\processCol_reg[1] ),
        .I2(\dc_bias_reg_n_0_[2] ),
        .I3(\dc_bias_reg_n_0_[1] ),
        .I4(\dc_bias_reg_n_0_[0] ),
        .O(\dc_bias[2]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT5 #(
    .INIT(32'h00027FFF)) 
    \dc_bias[3]_i_1__0 
       (.I0(Q),
        .I1(\processCol_reg[1] ),
        .I2(\dc_bias_reg_n_0_[1] ),
        .I3(\dc_bias_reg_n_0_[0] ),
        .I4(\dc_bias_reg_n_0_[2] ),
        .O(\dc_bias[3]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \dc_bias_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(\dc_bias[0]_i_1__0_n_0 ),
        .Q(\dc_bias_reg_n_0_[0] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \dc_bias_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(\dc_bias[1]_i_1__0_n_0 ),
        .Q(\dc_bias_reg_n_0_[1] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \dc_bias_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(\dc_bias[2]_i_1__0_n_0 ),
        .Q(\dc_bias_reg_n_0_[2] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \dc_bias_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(\dc_bias[3]_i_1__0_n_0 ),
        .Q(Q),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \encoded[0]_i_1__0 
       (.I0(Q),
        .I1(\processCol_reg[6] ),
        .O(\encoded[0]_i_1__0_n_0 ));
  LUT2 #(
    .INIT(4'hB)) 
    \encoded[2]_i_1__0 
       (.I0(Q),
        .I1(\processCol_reg[6] ),
        .O(\encoded[2]_i_1__0_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFEFF00FF00FF)) 
    \encoded[8]_i_2__0 
       (.I0(\dc_bias_reg_n_0_[1] ),
        .I1(\dc_bias_reg_n_0_[0] ),
        .I2(\dc_bias_reg_n_0_[2] ),
        .I3(\processCol_reg[6] ),
        .I4(Q),
        .I5(\processCol_reg[1] ),
        .O(\encoded[8]_i_2__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT5 #(
    .INIT(32'hFFFFFEFF)) 
    \encoded[8]_i_4 
       (.I0(\dc_bias_reg_n_0_[1] ),
        .I1(\dc_bias_reg_n_0_[0] ),
        .I2(\dc_bias_reg_n_0_[2] ),
        .I3(\processCol_reg[6] ),
        .I4(Q),
        .O(\encoded_reg[8]_0 ));
  FDRE \encoded_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(\encoded[0]_i_1__0_n_0 ),
        .Q(D[0]),
        .R(1'b0));
  FDRE \encoded_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(\encoded[2]_i_1__0_n_0 ),
        .Q(D[1]),
        .R(1'b0));
  FDSE \encoded_reg[8] 
       (.C(CLK),
        .CE(1'b1),
        .D(\encoded[8]_i_2__0_n_0 ),
        .Q(D[2]),
        .S(\dc_bias_reg[1]_0 ));
  FDRE \encoded_reg[9] 
       (.C(CLK),
        .CE(1'b1),
        .D(\dc_bias_reg[3]_0 ),
        .Q(D[3]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "TDMS_encoder" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_TDMS_encoder_1
   (D,
    \encoded_reg[8]_0 ,
    Q,
    \encoded_reg[8]_1 ,
    \encoded_reg[8]_2 ,
    \encoded_reg[8]_3 ,
    \encoded_reg[8]_4 ,
    \encoded_reg[8]_5 ,
    \encoded_reg[8]_6 ,
    \encoded_reg[8]_7 ,
    \encoded_reg[8]_8 ,
    \encoded_reg[8]_9 ,
    \encoded_reg[8]_10 ,
    \encoded_reg[8]_11 ,
    \encoded_reg[8]_12 ,
    \encoded_reg[8]_13 ,
    \encoded_reg[8]_14 ,
    \encoded_reg[8]_15 ,
    \encoded_reg[8]_16 ,
    encoded0_in,
    \processCol_reg[6] ,
    CLK,
    \dc_bias_reg[3]_0 ,
    \dc_bias_reg[3]_1 ,
    \trigger_volt_reg[6] ,
    \processCol_reg[6]_0 ,
    \trigger_time_reg[7] ,
    \trigger_time_reg[6] ,
    \trigger_time_reg[5] ,
    \trigger_time_reg[3] ,
    \trigger_time_reg[2] ,
    \trigger_time_reg[4] ,
    \processCol_reg[0] ,
    \processRow_reg[2] ,
    SR);
  output [3:0]D;
  output \encoded_reg[8]_0 ;
  output [0:0]Q;
  output \encoded_reg[8]_1 ;
  output \encoded_reg[8]_2 ;
  output \encoded_reg[8]_3 ;
  output \encoded_reg[8]_4 ;
  output \encoded_reg[8]_5 ;
  output \encoded_reg[8]_6 ;
  output \encoded_reg[8]_7 ;
  output \encoded_reg[8]_8 ;
  output \encoded_reg[8]_9 ;
  output \encoded_reg[8]_10 ;
  output \encoded_reg[8]_11 ;
  output \encoded_reg[8]_12 ;
  output \encoded_reg[8]_13 ;
  output \encoded_reg[8]_14 ;
  output \encoded_reg[8]_15 ;
  output \encoded_reg[8]_16 ;
  input [0:0]encoded0_in;
  input \processCol_reg[6] ;
  input CLK;
  input \dc_bias_reg[3]_0 ;
  input \dc_bias_reg[3]_1 ;
  input [6:0]\trigger_volt_reg[6] ;
  input \processCol_reg[6]_0 ;
  input \trigger_time_reg[7] ;
  input \trigger_time_reg[6] ;
  input [2:0]\trigger_time_reg[5] ;
  input \trigger_time_reg[3] ;
  input \trigger_time_reg[2] ;
  input \trigger_time_reg[4] ;
  input \processCol_reg[0] ;
  input \processRow_reg[2] ;
  input [0:0]SR;

  wire CLK;
  wire [3:0]D;
  wire [0:0]Q;
  wire R15_carry_i_10_n_0;
  wire [0:0]SR;
  wire \dc_bias[0]_i_1_n_0 ;
  wire \dc_bias[1]_i_1__1_n_0 ;
  wire \dc_bias[2]_i_1__1_n_0 ;
  wire \dc_bias[3]_i_2__0_n_0 ;
  wire \dc_bias_reg[3]_0 ;
  wire \dc_bias_reg[3]_1 ;
  wire \dc_bias_reg_n_0_[0] ;
  wire \dc_bias_reg_n_0_[1] ;
  wire \dc_bias_reg_n_0_[2] ;
  wire [0:0]encoded0_in;
  wire \encoded[2]_i_1__1_n_0 ;
  wire \encoded_reg[8]_0 ;
  wire \encoded_reg[8]_1 ;
  wire \encoded_reg[8]_10 ;
  wire \encoded_reg[8]_11 ;
  wire \encoded_reg[8]_12 ;
  wire \encoded_reg[8]_13 ;
  wire \encoded_reg[8]_14 ;
  wire \encoded_reg[8]_15 ;
  wire \encoded_reg[8]_16 ;
  wire \encoded_reg[8]_2 ;
  wire \encoded_reg[8]_3 ;
  wire \encoded_reg[8]_4 ;
  wire \encoded_reg[8]_5 ;
  wire \encoded_reg[8]_6 ;
  wire \encoded_reg[8]_7 ;
  wire \encoded_reg[8]_8 ;
  wire \encoded_reg[8]_9 ;
  wire \processCol_reg[0] ;
  wire \processCol_reg[6] ;
  wire \processCol_reg[6]_0 ;
  wire \processRow_reg[2] ;
  wire \trigger_time_reg[2] ;
  wire \trigger_time_reg[3] ;
  wire \trigger_time_reg[4] ;
  wire [2:0]\trigger_time_reg[5] ;
  wire \trigger_time_reg[6] ;
  wire \trigger_time_reg[7] ;
  wire [6:0]\trigger_volt_reg[6] ;

  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    R13_carry_i_5
       (.I0(\trigger_time_reg[5] [2]),
        .I1(\trigger_time_reg[4] ),
        .I2(\trigger_time_reg[2] ),
        .I3(\trigger_time_reg[5] [1]),
        .I4(\trigger_time_reg[3] ),
        .O(\encoded_reg[8]_6 ));
  LUT2 #(
    .INIT(4'h1)) 
    R13_carry_i_6
       (.I0(\trigger_time_reg[5] [1]),
        .I1(\trigger_time_reg[2] ),
        .O(\encoded_reg[8]_12 ));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT5 #(
    .INIT(32'h80000000)) 
    R14_carry_i_5
       (.I0(\trigger_time_reg[4] ),
        .I1(\trigger_time_reg[2] ),
        .I2(\trigger_time_reg[5] [1]),
        .I3(\trigger_time_reg[3] ),
        .I4(\trigger_time_reg[5] [2]),
        .O(\encoded_reg[8]_2 ));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT2 #(
    .INIT(4'h8)) 
    R14_carry_i_7
       (.I0(\trigger_time_reg[5] [1]),
        .I1(\trigger_time_reg[2] ),
        .O(\encoded_reg[8]_10 ));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT4 #(
    .INIT(16'h0001)) 
    R15_carry_i_10
       (.I0(\trigger_time_reg[3] ),
        .I1(\trigger_time_reg[5] [1]),
        .I2(\trigger_time_reg[2] ),
        .I3(\trigger_time_reg[4] ),
        .O(R15_carry_i_10_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    R15_carry_i_11
       (.I0(\trigger_time_reg[6] ),
        .I1(\trigger_time_reg[3] ),
        .I2(\trigger_time_reg[5] [1]),
        .I3(\trigger_time_reg[2] ),
        .I4(\trigger_time_reg[4] ),
        .I5(\trigger_time_reg[5] [2]),
        .O(\encoded_reg[8]_5 ));
  LUT4 #(
    .INIT(16'h0004)) 
    R15_carry_i_5
       (.I0(\trigger_time_reg[5] [2]),
        .I1(R15_carry_i_10_n_0),
        .I2(\trigger_time_reg[6] ),
        .I3(\trigger_time_reg[7] ),
        .O(\encoded_reg[8]_11 ));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT3 #(
    .INIT(8'h80)) 
    R16_carry_i_5
       (.I0(\trigger_time_reg[6] ),
        .I1(\encoded_reg[8]_2 ),
        .I2(\trigger_time_reg[7] ),
        .O(\encoded_reg[8]_3 ));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    R16_carry_i_6
       (.I0(\trigger_time_reg[7] ),
        .I1(\encoded_reg[8]_2 ),
        .I2(\trigger_time_reg[6] ),
        .I3(\trigger_time_reg[5] [0]),
        .O(\encoded_reg[8]_1 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    R16_carry_i_9
       (.I0(\trigger_time_reg[5] [2]),
        .I1(\trigger_time_reg[3] ),
        .I2(\trigger_time_reg[5] [1]),
        .I3(\trigger_time_reg[2] ),
        .I4(\trigger_time_reg[4] ),
        .I5(\trigger_time_reg[5] [0]),
        .O(\encoded_reg[8]_4 ));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT5 #(
    .INIT(32'h00000001)) 
    R6_carry_i_5
       (.I0(\trigger_volt_reg[6] [4]),
        .I1(\trigger_volt_reg[6] [2]),
        .I2(\trigger_volt_reg[6] [1]),
        .I3(\trigger_volt_reg[6] [3]),
        .I4(\trigger_volt_reg[6] [5]),
        .O(\encoded_reg[8]_15 ));
  LUT2 #(
    .INIT(4'h1)) 
    R6_carry_i_6
       (.I0(\trigger_volt_reg[6] [1]),
        .I1(\trigger_volt_reg[6] [2]),
        .O(\encoded_reg[8]_16 ));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT5 #(
    .INIT(32'h80000000)) 
    R7_carry_i_6
       (.I0(\trigger_volt_reg[6] [4]),
        .I1(\trigger_volt_reg[6] [2]),
        .I2(\trigger_volt_reg[6] [1]),
        .I3(\trigger_volt_reg[6] [3]),
        .I4(\trigger_volt_reg[6] [5]),
        .O(\encoded_reg[8]_8 ));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT2 #(
    .INIT(4'h8)) 
    R7_carry_i_8
       (.I0(\trigger_volt_reg[6] [1]),
        .I1(\trigger_volt_reg[6] [2]),
        .O(\encoded_reg[8]_13 ));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    R8_carry_i_5
       (.I0(\trigger_volt_reg[6] [5]),
        .I1(\trigger_volt_reg[6] [3]),
        .I2(\trigger_volt_reg[6] [1]),
        .I3(\trigger_volt_reg[6] [2]),
        .I4(\trigger_volt_reg[6] [4]),
        .I5(\trigger_volt_reg[6] [6]),
        .O(\encoded_reg[8]_14 ));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    R8_carry_i_9
       (.I0(\trigger_volt_reg[6] [2]),
        .I1(\trigger_volt_reg[6] [1]),
        .I2(\trigger_volt_reg[6] [0]),
        .O(\encoded_reg[8]_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    R9_carry_i_5
       (.I0(\trigger_volt_reg[6] [5]),
        .I1(\trigger_volt_reg[6] [3]),
        .I2(\trigger_volt_reg[6] [1]),
        .I3(\trigger_volt_reg[6] [2]),
        .I4(\trigger_volt_reg[6] [4]),
        .I5(\trigger_volt_reg[6] [6]),
        .O(\encoded_reg[8]_7 ));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \dc_bias[0]_i_1 
       (.I0(Q),
        .I1(\dc_bias_reg_n_0_[0] ),
        .O(\dc_bias[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT5 #(
    .INIT(32'h9699AAAA)) 
    \dc_bias[1]_i_1__1 
       (.I0(\dc_bias_reg_n_0_[1] ),
        .I1(\dc_bias_reg_n_0_[0] ),
        .I2(\processCol_reg[0] ),
        .I3(\processRow_reg[2] ),
        .I4(Q),
        .O(\dc_bias[1]_i_1__1_n_0 ));
  LUT6 #(
    .INIT(64'h5955555565665555)) 
    \dc_bias[2]_i_1__1 
       (.I0(\dc_bias_reg_n_0_[2] ),
        .I1(\dc_bias_reg_n_0_[1] ),
        .I2(\processCol_reg[0] ),
        .I3(\processRow_reg[2] ),
        .I4(Q),
        .I5(\dc_bias_reg_n_0_[0] ),
        .O(\dc_bias[2]_i_1__1_n_0 ));
  LUT6 #(
    .INIT(64'h0000DFFF008AFFFF)) 
    \dc_bias[3]_i_2__0 
       (.I0(Q),
        .I1(\processCol_reg[0] ),
        .I2(\processRow_reg[2] ),
        .I3(\dc_bias_reg_n_0_[0] ),
        .I4(\dc_bias_reg_n_0_[2] ),
        .I5(\dc_bias_reg_n_0_[1] ),
        .O(\dc_bias[3]_i_2__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \dc_bias_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(\dc_bias[0]_i_1_n_0 ),
        .Q(\dc_bias_reg_n_0_[0] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \dc_bias_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(\dc_bias[1]_i_1__1_n_0 ),
        .Q(\dc_bias_reg_n_0_[1] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \dc_bias_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(\dc_bias[2]_i_1__1_n_0 ),
        .Q(\dc_bias_reg_n_0_[2] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \dc_bias_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(\dc_bias[3]_i_2__0_n_0 ),
        .Q(Q),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \encoded[2]_i_1__1 
       (.I0(Q),
        .I1(\processCol_reg[6]_0 ),
        .O(\encoded[2]_i_1__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT5 #(
    .INIT(32'hFFFFFEFF)) 
    \encoded[8]_i_6__0 
       (.I0(\dc_bias_reg_n_0_[1] ),
        .I1(\dc_bias_reg_n_0_[0] ),
        .I2(\dc_bias_reg_n_0_[2] ),
        .I3(\processCol_reg[6]_0 ),
        .I4(Q),
        .O(\encoded_reg[8]_9 ));
  FDRE \encoded_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(\dc_bias_reg[3]_0 ),
        .Q(D[0]),
        .R(1'b0));
  FDRE \encoded_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(\encoded[2]_i_1__1_n_0 ),
        .Q(D[1]),
        .R(1'b0));
  FDSE \encoded_reg[8] 
       (.C(CLK),
        .CE(1'b1),
        .D(\processCol_reg[6] ),
        .Q(D[2]),
        .S(encoded0_in));
  FDRE \encoded_reg[9] 
       (.C(CLK),
        .CE(1'b1),
        .D(\dc_bias_reg[3]_1 ),
        .Q(D[3]),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_TWICtl
   (D,
    E,
    \initA_reg[6] ,
    initEn_reg,
    scl,
    sda,
    clk_out2,
    data_i,
    s00_axi_aresetn,
    stb,
    msg,
    \initWord_reg[6] ,
    Q,
    \delaycnt_reg[20] ,
    initEn_reg_0,
    \initWord_reg[21] ,
    \initA_reg[4] ,
    initEn);
  output [3:0]D;
  output [0:0]E;
  output [0:0]\initA_reg[6] ;
  output initEn_reg;
  inout scl;
  inout sda;
  input clk_out2;
  input [7:0]data_i;
  input s00_axi_aresetn;
  input stb;
  input msg;
  input \initWord_reg[6] ;
  input [3:0]Q;
  input \delaycnt_reg[20] ;
  input initEn_reg_0;
  input \initWord_reg[21] ;
  input \initA_reg[4] ;
  input initEn;

  wire [3:0]D;
  wire DONE_O_i_1_n_0;
  wire DONE_O_i_2_n_0;
  wire DONE_O_i_3_n_0;
  wire DONE_O_i_4_n_0;
  wire [0:0]E;
  wire ERR_O_i_1_n_0;
  wire ERR_O_i_2_n_0;
  wire \FSM_gray_state[0]_i_1_n_0 ;
  wire \FSM_gray_state[0]_i_2_n_0 ;
  wire \FSM_gray_state[1]_i_1_n_0 ;
  wire \FSM_gray_state[1]_i_2_n_0 ;
  wire \FSM_gray_state[2]_i_1_n_0 ;
  wire \FSM_gray_state[2]_i_2_n_0 ;
  wire \FSM_gray_state[3]_i_10_n_0 ;
  wire \FSM_gray_state[3]_i_11_n_0 ;
  wire \FSM_gray_state[3]_i_12_n_0 ;
  wire \FSM_gray_state[3]_i_13_n_0 ;
  wire \FSM_gray_state[3]_i_1_n_0 ;
  wire \FSM_gray_state[3]_i_2_n_0 ;
  wire \FSM_gray_state[3]_i_3_n_0 ;
  wire \FSM_gray_state[3]_i_4_n_0 ;
  wire \FSM_gray_state[3]_i_5_n_0 ;
  wire \FSM_gray_state[3]_i_6_n_0 ;
  wire \FSM_gray_state[3]_i_7_n_0 ;
  wire \FSM_gray_state[3]_i_8_n_0 ;
  wire \FSM_gray_state[3]_i_9_n_0 ;
  wire [3:0]Q;
  wire addrNData_i_1_n_0;
  wire addrNData_i_2_n_0;
  wire addrNData_reg_n_0;
  wire [2:0]bitCount;
  wire \bitCount[0]_i_1_n_0 ;
  wire \bitCount[1]_i_1_n_0 ;
  wire \bitCount[2]_i_1_n_0 ;
  wire [6:0]busFreeCnt0;
  wire busFreeCnt0_1;
  wire \busFreeCnt[1]_i_1_n_0 ;
  wire \busFreeCnt[6]_i_3_n_0 ;
  wire [6:0]busFreeCnt_reg__0;
  wire busState0;
  wire \busState[0]_i_1_n_0 ;
  wire \busState[1]_i_1_n_0 ;
  wire \busState_reg_n_0_[0] ;
  wire \busState_reg_n_0_[1] ;
  wire clk_out2;
  wire dScl;
  wire dataByte0;
  wire dataByte1;
  wire \dataByte[0]_i_1_n_0 ;
  wire \dataByte[1]_i_1_n_0 ;
  wire \dataByte[2]_i_1_n_0 ;
  wire \dataByte[3]_i_1_n_0 ;
  wire \dataByte[4]_i_1_n_0 ;
  wire \dataByte[5]_i_1_n_0 ;
  wire \dataByte[6]_i_1_n_0 ;
  wire \dataByte[7]_i_1_n_0 ;
  wire \dataByte[7]_i_2_n_0 ;
  wire \dataByte[7]_i_5_n_0 ;
  wire \dataByte[7]_i_6_n_0 ;
  wire \dataByte[7]_i_7_n_0 ;
  wire \dataByte_reg_n_0_[0] ;
  wire \dataByte_reg_n_0_[1] ;
  wire \dataByte_reg_n_0_[2] ;
  wire \dataByte_reg_n_0_[3] ;
  wire \dataByte_reg_n_0_[4] ;
  wire \dataByte_reg_n_0_[5] ;
  wire \dataByte_reg_n_0_[6] ;
  wire \dataByte_reg_n_0_[7] ;
  wire [7:0]data_i;
  wire ddSda;
  wire \delaycnt_reg[20] ;
  wire done;
  wire error;
  wire \initA_reg[4] ;
  wire [0:0]\initA_reg[6] ;
  wire initEn;
  wire initEn_i_2_n_0;
  wire initEn_reg;
  wire initEn_reg_0;
  wire \initWord_reg[21] ;
  wire \initWord_reg[6] ;
  wire int_Rst;
  wire int_Rst_i_1_n_0;
  wire msg;
  wire [0:0]p_0_in;
  wire rScl;
  wire rScl_i_1_n_0;
  wire rSda;
  wire rSda_i_1_n_0;
  wire rSda_i_2_n_0;
  wire rSda_i_3_n_0;
  wire rSda_i_4_n_0;
  wire rSda_i_5_n_0;
  wire s00_axi_aresetn;
  wire scl;
  wire [6:0]sclCnt0;
  wire sclCnt0_0;
  wire \sclCnt[1]_i_1_n_0 ;
  wire \sclCnt[6]_i_2_n_0 ;
  wire \sclCnt[6]_i_4_n_0 ;
  wire [6:0]sclCnt_reg__0;
  wire scl_INST_0_i_1_n_0;
  wire sda;
  wire sda_INST_0_i_1_n_0;
  (* RTL_KEEP = "yes" *) wire [3:0]state;
  wire \state[1]_i_2_n_0 ;
  wire stb;
  wire subState;
  wire \subState[0]_i_1_n_0 ;
  wire \subState[1]_i_1_n_0 ;
  wire \subState[1]_i_2_n_0 ;
  wire \subState[1]_i_3_n_0 ;
  wire \subState_reg_n_0_[0] ;
  wire \subState_reg_n_0_[1] ;

  LUT6 #(
    .INIT(64'hCCCFCCCCCCDDCCCC)) 
    DONE_O_i_1
       (.I0(\FSM_gray_state[3]_i_8_n_0 ),
        .I1(DONE_O_i_2_n_0),
        .I2(DONE_O_i_3_n_0),
        .I3(DONE_O_i_4_n_0),
        .I4(state[1]),
        .I5(state[0]),
        .O(DONE_O_i_1_n_0));
  LUT6 #(
    .INIT(64'h0010000000000000)) 
    DONE_O_i_2
       (.I0(state[1]),
        .I1(state[0]),
        .I2(\subState[1]_i_3_n_0 ),
        .I3(p_0_in),
        .I4(dScl),
        .I5(rSda),
        .O(DONE_O_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT2 #(
    .INIT(4'h2)) 
    DONE_O_i_3
       (.I0(addrNData_reg_n_0),
        .I1(p_0_in),
        .O(DONE_O_i_3_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFF7)) 
    DONE_O_i_4
       (.I0(\subState[1]_i_2_n_0 ),
        .I1(\subState_reg_n_0_[0] ),
        .I2(\subState_reg_n_0_[1] ),
        .I3(state[2]),
        .I4(state[3]),
        .O(DONE_O_i_4_n_0));
  FDRE DONE_O_reg
       (.C(clk_out2),
        .CE(1'b1),
        .D(DONE_O_i_1_n_0),
        .Q(done),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h8888F00088880000)) 
    ERR_O_i_1
       (.I0(ERR_O_i_2_n_0),
        .I1(state[0]),
        .I2(rSda),
        .I3(dScl),
        .I4(p_0_in),
        .I5(\FSM_gray_state[3]_i_6_n_0 ),
        .O(ERR_O_i_1_n_0));
  LUT6 #(
    .INIT(64'h0002000000000000)) 
    ERR_O_i_2
       (.I0(state[1]),
        .I1(state[3]),
        .I2(state[2]),
        .I3(\subState_reg_n_0_[1] ),
        .I4(\subState_reg_n_0_[0] ),
        .I5(\subState[1]_i_2_n_0 ),
        .O(ERR_O_i_2_n_0));
  FDRE ERR_O_reg
       (.C(clk_out2),
        .CE(1'b1),
        .D(ERR_O_i_1_n_0),
        .Q(error),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFFFFFFF44F55FF)) 
    \FSM_gray_state[0]_i_1 
       (.I0(\FSM_gray_state[0]_i_2_n_0 ),
        .I1(\FSM_gray_state[2]_i_2_n_0 ),
        .I2(state[2]),
        .I3(state[1]),
        .I4(state[0]),
        .I5(state[3]),
        .O(\FSM_gray_state[0]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hAAAAFBFF)) 
    \FSM_gray_state[0]_i_2 
       (.I0(state[2]),
        .I1(msg),
        .I2(int_Rst),
        .I3(stb),
        .I4(state[0]),
        .O(\FSM_gray_state[0]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00015555)) 
    \FSM_gray_state[1]_i_1 
       (.I0(state[3]),
        .I1(state[0]),
        .I2(state[1]),
        .I3(\FSM_gray_state[3]_i_3_n_0 ),
        .I4(\FSM_gray_state[1]_i_2_n_0 ),
        .O(\FSM_gray_state[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF0FF0BFF0BFF)) 
    \FSM_gray_state[1]_i_2 
       (.I0(int_Rst),
        .I1(stb),
        .I2(state[2]),
        .I3(state[1]),
        .I4(\FSM_gray_state[3]_i_9_n_0 ),
        .I5(state[0]),
        .O(\FSM_gray_state[1]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h004100FC0041003C)) 
    \FSM_gray_state[2]_i_1 
       (.I0(\FSM_gray_state[3]_i_3_n_0 ),
        .I1(state[0]),
        .I2(state[1]),
        .I3(state[3]),
        .I4(state[2]),
        .I5(\FSM_gray_state[2]_i_2_n_0 ),
        .O(\FSM_gray_state[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT5 #(
    .INIT(32'hEEEEFFEF)) 
    \FSM_gray_state[2]_i_2 
       (.I0(int_Rst),
        .I1(\dataByte_reg_n_0_[0] ),
        .I2(stb),
        .I3(msg),
        .I4(addrNData_reg_n_0),
        .O(\FSM_gray_state[2]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFCFCF8F8FFFCF8F8)) 
    \FSM_gray_state[3]_i_1 
       (.I0(\FSM_gray_state[3]_i_3_n_0 ),
        .I1(\FSM_gray_state[3]_i_4_n_0 ),
        .I2(\FSM_gray_state[3]_i_5_n_0 ),
        .I3(\FSM_gray_state[3]_i_6_n_0 ),
        .I4(\FSM_gray_state[3]_i_7_n_0 ),
        .I5(\FSM_gray_state[3]_i_8_n_0 ),
        .O(\FSM_gray_state[3]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFCCECFFF)) 
    \FSM_gray_state[3]_i_10 
       (.I0(\FSM_gray_state[3]_i_13_n_0 ),
        .I1(state[3]),
        .I2(state[2]),
        .I3(state[0]),
        .I4(state[1]),
        .O(\FSM_gray_state[3]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hDFFFFFFFFFFFFFFF)) 
    \FSM_gray_state[3]_i_11 
       (.I0(\subState_reg_n_0_[1] ),
        .I1(\subState_reg_n_0_[0] ),
        .I2(state[3]),
        .I3(state[2]),
        .I4(state[1]),
        .I5(state[0]),
        .O(\FSM_gray_state[3]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hAEAAAAAAAAAAAAAA)) 
    \FSM_gray_state[3]_i_12 
       (.I0(DONE_O_i_2_n_0),
        .I1(\busState_reg_n_0_[0] ),
        .I2(\busState_reg_n_0_[1] ),
        .I3(stb),
        .I4(subState),
        .I5(s00_axi_aresetn),
        .O(\FSM_gray_state[3]_i_12_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT3 #(
    .INIT(8'h7F)) 
    \FSM_gray_state[3]_i_13 
       (.I0(bitCount[0]),
        .I1(bitCount[1]),
        .I2(bitCount[2]),
        .O(\FSM_gray_state[3]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'h00000011000A0000)) 
    \FSM_gray_state[3]_i_2 
       (.I0(state[0]),
        .I1(\FSM_gray_state[3]_i_3_n_0 ),
        .I2(\FSM_gray_state[3]_i_9_n_0 ),
        .I3(state[3]),
        .I4(state[1]),
        .I5(state[2]),
        .O(\FSM_gray_state[3]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \FSM_gray_state[3]_i_3 
       (.I0(rSda),
        .I1(dScl),
        .I2(p_0_in),
        .O(\FSM_gray_state[3]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h4004)) 
    \FSM_gray_state[3]_i_4 
       (.I0(state[3]),
        .I1(state[2]),
        .I2(state[1]),
        .I3(state[0]),
        .O(\FSM_gray_state[3]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF4000FF00)) 
    \FSM_gray_state[3]_i_5 
       (.I0(\FSM_gray_state[3]_i_10_n_0 ),
        .I1(\subState_reg_n_0_[1] ),
        .I2(\subState_reg_n_0_[0] ),
        .I3(\subState[1]_i_2_n_0 ),
        .I4(\FSM_gray_state[3]_i_11_n_0 ),
        .I5(\FSM_gray_state[3]_i_12_n_0 ),
        .O(\FSM_gray_state[3]_i_5_n_0 ));
  LUT4 #(
    .INIT(16'h0001)) 
    \FSM_gray_state[3]_i_6 
       (.I0(state[3]),
        .I1(state[2]),
        .I2(state[0]),
        .I3(state[1]),
        .O(\FSM_gray_state[3]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair67" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \FSM_gray_state[3]_i_7 
       (.I0(\subState[1]_i_2_n_0 ),
        .I1(\subState_reg_n_0_[1] ),
        .I2(\subState_reg_n_0_[0] ),
        .O(\FSM_gray_state[3]_i_7_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    \FSM_gray_state[3]_i_8 
       (.I0(bitCount[2]),
        .I1(bitCount[0]),
        .I2(bitCount[1]),
        .O(\FSM_gray_state[3]_i_8_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT4 #(
    .INIT(16'h000E)) 
    \FSM_gray_state[3]_i_9 
       (.I0(addrNData_reg_n_0),
        .I1(stb),
        .I2(\dataByte_reg_n_0_[0] ),
        .I3(int_Rst),
        .O(\FSM_gray_state[3]_i_9_n_0 ));
  (* FSM_ENCODED_STATES = "stwrite:0000,stread:0010,ststart:0101,stidle:0001,stmnackstart:0111,stmack:0110,stsack:0011,stmnackstop:0100,ststop:1111" *) 
  (* KEEP = "yes" *) 
  FDRE \FSM_gray_state_reg[0] 
       (.C(clk_out2),
        .CE(\FSM_gray_state[3]_i_1_n_0 ),
        .D(\FSM_gray_state[0]_i_1_n_0 ),
        .Q(state[0]),
        .R(1'b0));
  (* FSM_ENCODED_STATES = "stwrite:0000,stread:0010,ststart:0101,stidle:0001,stmnackstart:0111,stmack:0110,stsack:0011,stmnackstop:0100,ststop:1111" *) 
  (* KEEP = "yes" *) 
  FDRE \FSM_gray_state_reg[1] 
       (.C(clk_out2),
        .CE(\FSM_gray_state[3]_i_1_n_0 ),
        .D(\FSM_gray_state[1]_i_1_n_0 ),
        .Q(state[1]),
        .R(1'b0));
  (* FSM_ENCODED_STATES = "stwrite:0000,stread:0010,ststart:0101,stidle:0001,stmnackstart:0111,stmack:0110,stsack:0011,stmnackstop:0100,ststop:1111" *) 
  (* KEEP = "yes" *) 
  FDRE \FSM_gray_state_reg[2] 
       (.C(clk_out2),
        .CE(\FSM_gray_state[3]_i_1_n_0 ),
        .D(\FSM_gray_state[2]_i_1_n_0 ),
        .Q(state[2]),
        .R(1'b0));
  (* FSM_ENCODED_STATES = "stwrite:0000,stread:0010,ststart:0101,stidle:0001,stmnackstart:0111,stmack:0110,stsack:0011,stmnackstop:0100,ststop:1111" *) 
  (* KEEP = "yes" *) 
  FDRE \FSM_gray_state_reg[3] 
       (.C(clk_out2),
        .CE(\FSM_gray_state[3]_i_1_n_0 ),
        .D(\FSM_gray_state[3]_i_2_n_0 ),
        .Q(state[3]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h0AEAEAEAEAEAEAEA)) 
    addrNData_i_1
       (.I0(addrNData_reg_n_0),
        .I1(\dataByte[7]_i_6_n_0 ),
        .I2(\subState[1]_i_2_n_0 ),
        .I3(\subState_reg_n_0_[1] ),
        .I4(\subState_reg_n_0_[0] ),
        .I5(addrNData_i_2_n_0),
        .O(addrNData_i_1_n_0));
  LUT4 #(
    .INIT(16'h1000)) 
    addrNData_i_2
       (.I0(state[3]),
        .I1(state[2]),
        .I2(state[0]),
        .I3(state[1]),
        .O(addrNData_i_2_n_0));
  FDRE addrNData_reg
       (.C(clk_out2),
        .CE(1'b1),
        .D(addrNData_i_1_n_0),
        .Q(addrNData_reg_n_0),
        .R(1'b0));
  LUT3 #(
    .INIT(8'hF6)) 
    \bitCount[0]_i_1 
       (.I0(bitCount[0]),
        .I1(dataByte0),
        .I2(dataByte1),
        .O(\bitCount[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT4 #(
    .INIT(16'hFFA6)) 
    \bitCount[1]_i_1 
       (.I0(bitCount[1]),
        .I1(dataByte0),
        .I2(bitCount[0]),
        .I3(dataByte1),
        .O(\bitCount[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT5 #(
    .INIT(32'hFFFFAAA6)) 
    \bitCount[2]_i_1 
       (.I0(bitCount[2]),
        .I1(dataByte0),
        .I2(bitCount[1]),
        .I3(bitCount[0]),
        .I4(dataByte1),
        .O(\bitCount[2]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b1)) 
    \bitCount_reg[0] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(\bitCount[0]_i_1_n_0 ),
        .Q(bitCount[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \bitCount_reg[1] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(\bitCount[1]_i_1_n_0 ),
        .Q(bitCount[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \bitCount_reg[2] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(\bitCount[2]_i_1_n_0 ),
        .Q(bitCount[2]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \busFreeCnt[0]_i_1 
       (.I0(busFreeCnt_reg__0[0]),
        .O(busFreeCnt0[0]));
  LUT2 #(
    .INIT(4'h9)) 
    \busFreeCnt[1]_i_1 
       (.I0(busFreeCnt_reg__0[1]),
        .I1(busFreeCnt_reg__0[0]),
        .O(\busFreeCnt[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT3 #(
    .INIT(8'hA9)) 
    \busFreeCnt[2]_i_1 
       (.I0(busFreeCnt_reg__0[2]),
        .I1(busFreeCnt_reg__0[0]),
        .I2(busFreeCnt_reg__0[1]),
        .O(busFreeCnt0[2]));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT4 #(
    .INIT(16'hAAA9)) 
    \busFreeCnt[3]_i_1 
       (.I0(busFreeCnt_reg__0[3]),
        .I1(busFreeCnt_reg__0[2]),
        .I2(busFreeCnt_reg__0[1]),
        .I3(busFreeCnt_reg__0[0]),
        .O(busFreeCnt0[3]));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT5 #(
    .INIT(32'hAAAAAAA9)) 
    \busFreeCnt[4]_i_1 
       (.I0(busFreeCnt_reg__0[4]),
        .I1(busFreeCnt_reg__0[3]),
        .I2(busFreeCnt_reg__0[0]),
        .I3(busFreeCnt_reg__0[1]),
        .I4(busFreeCnt_reg__0[2]),
        .O(busFreeCnt0[4]));
  LUT6 #(
    .INIT(64'hAAAAAAAAAAAAAAA9)) 
    \busFreeCnt[5]_i_1 
       (.I0(busFreeCnt_reg__0[5]),
        .I1(busFreeCnt_reg__0[4]),
        .I2(busFreeCnt_reg__0[2]),
        .I3(busFreeCnt_reg__0[1]),
        .I4(busFreeCnt_reg__0[0]),
        .I5(busFreeCnt_reg__0[3]),
        .O(busFreeCnt0[5]));
  LUT3 #(
    .INIT(8'hBF)) 
    \busFreeCnt[6]_i_1 
       (.I0(int_Rst),
        .I1(p_0_in),
        .I2(dScl),
        .O(busFreeCnt0_1));
  LUT2 #(
    .INIT(4'h6)) 
    \busFreeCnt[6]_i_2 
       (.I0(busFreeCnt_reg__0[6]),
        .I1(\busFreeCnt[6]_i_3_n_0 ),
        .O(busFreeCnt0[6]));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \busFreeCnt[6]_i_3 
       (.I0(busFreeCnt_reg__0[4]),
        .I1(busFreeCnt_reg__0[2]),
        .I2(busFreeCnt_reg__0[1]),
        .I3(busFreeCnt_reg__0[0]),
        .I4(busFreeCnt_reg__0[3]),
        .I5(busFreeCnt_reg__0[5]),
        .O(\busFreeCnt[6]_i_3_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \busFreeCnt_reg[0] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(busFreeCnt0[0]),
        .Q(busFreeCnt_reg__0[0]),
        .S(busFreeCnt0_1));
  FDRE #(
    .INIT(1'b0)) 
    \busFreeCnt_reg[1] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(\busFreeCnt[1]_i_1_n_0 ),
        .Q(busFreeCnt_reg__0[1]),
        .R(busFreeCnt0_1));
  FDSE #(
    .INIT(1'b1)) 
    \busFreeCnt_reg[2] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(busFreeCnt0[2]),
        .Q(busFreeCnt_reg__0[2]),
        .S(busFreeCnt0_1));
  FDSE #(
    .INIT(1'b1)) 
    \busFreeCnt_reg[3] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(busFreeCnt0[3]),
        .Q(busFreeCnt_reg__0[3]),
        .S(busFreeCnt0_1));
  FDSE #(
    .INIT(1'b1)) 
    \busFreeCnt_reg[4] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(busFreeCnt0[4]),
        .Q(busFreeCnt_reg__0[4]),
        .S(busFreeCnt0_1));
  FDSE #(
    .INIT(1'b1)) 
    \busFreeCnt_reg[5] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(busFreeCnt0[5]),
        .Q(busFreeCnt_reg__0[5]),
        .S(busFreeCnt0_1));
  FDSE #(
    .INIT(1'b1)) 
    \busFreeCnt_reg[6] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(busFreeCnt0[6]),
        .Q(busFreeCnt_reg__0[6]),
        .S(busFreeCnt0_1));
  LUT6 #(
    .INIT(64'h4555FFFF45550000)) 
    \busState[0]_i_1 
       (.I0(int_Rst),
        .I1(p_0_in),
        .I2(dScl),
        .I3(ddSda),
        .I4(busState0),
        .I5(\busState_reg_n_0_[0] ),
        .O(\busState[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0040FFFF00400000)) 
    \busState[1]_i_1 
       (.I0(p_0_in),
        .I1(dScl),
        .I2(ddSda),
        .I3(int_Rst),
        .I4(busState0),
        .I5(\busState_reg_n_0_[1] ),
        .O(\busState[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF4444F444)) 
    \busState[1]_i_2 
       (.I0(busFreeCnt_reg__0[6]),
        .I1(\busFreeCnt[6]_i_3_n_0 ),
        .I2(ddSda),
        .I3(dScl),
        .I4(p_0_in),
        .I5(int_Rst),
        .O(busState0));
  FDRE #(
    .INIT(1'b0)) 
    \busState_reg[0] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(\busState[0]_i_1_n_0 ),
        .Q(\busState_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \busState_reg[1] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(\busState[1]_i_1_n_0 ),
        .Q(\busState_reg_n_0_[1] ),
        .R(1'b0));
  FDRE dScl_reg
       (.C(clk_out2),
        .CE(1'b1),
        .D(scl),
        .Q(dScl),
        .R(1'b0));
  FDRE dSda_reg
       (.C(clk_out2),
        .CE(1'b1),
        .D(sda),
        .Q(p_0_in),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT4 #(
    .INIT(16'h88B8)) 
    \dataByte[0]_i_1 
       (.I0(p_0_in),
        .I1(\dataByte[7]_i_5_n_0 ),
        .I2(data_i[0]),
        .I3(\dataByte[7]_i_6_n_0 ),
        .O(\dataByte[0]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hBBB8)) 
    \dataByte[1]_i_1 
       (.I0(\dataByte_reg_n_0_[0] ),
        .I1(\dataByte[7]_i_5_n_0 ),
        .I2(data_i[1]),
        .I3(\dataByte[7]_i_6_n_0 ),
        .O(\dataByte[1]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hBBB8)) 
    \dataByte[2]_i_1 
       (.I0(\dataByte_reg_n_0_[1] ),
        .I1(\dataByte[7]_i_5_n_0 ),
        .I2(data_i[2]),
        .I3(\dataByte[7]_i_6_n_0 ),
        .O(\dataByte[2]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h88B8)) 
    \dataByte[3]_i_1 
       (.I0(\dataByte_reg_n_0_[2] ),
        .I1(\dataByte[7]_i_5_n_0 ),
        .I2(data_i[3]),
        .I3(\dataByte[7]_i_6_n_0 ),
        .O(\dataByte[3]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hBBB8)) 
    \dataByte[4]_i_1 
       (.I0(\dataByte_reg_n_0_[3] ),
        .I1(\dataByte[7]_i_5_n_0 ),
        .I2(data_i[4]),
        .I3(\dataByte[7]_i_6_n_0 ),
        .O(\dataByte[4]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hBBB8)) 
    \dataByte[5]_i_1 
       (.I0(\dataByte_reg_n_0_[4] ),
        .I1(\dataByte[7]_i_5_n_0 ),
        .I2(data_i[5]),
        .I3(\dataByte[7]_i_6_n_0 ),
        .O(\dataByte[5]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hBBB8)) 
    \dataByte[6]_i_1 
       (.I0(\dataByte_reg_n_0_[5] ),
        .I1(\dataByte[7]_i_5_n_0 ),
        .I2(data_i[6]),
        .I3(\dataByte[7]_i_6_n_0 ),
        .O(\dataByte[6]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \dataByte[7]_i_1 
       (.I0(dataByte1),
        .I1(dataByte0),
        .O(\dataByte[7]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h88B8)) 
    \dataByte[7]_i_2 
       (.I0(\dataByte_reg_n_0_[6] ),
        .I1(\dataByte[7]_i_5_n_0 ),
        .I2(data_i[7]),
        .I3(\dataByte[7]_i_6_n_0 ),
        .O(\dataByte[7]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0030004000000000)) 
    \dataByte[7]_i_3 
       (.I0(\dataByte[7]_i_7_n_0 ),
        .I1(state[1]),
        .I2(state[0]),
        .I3(state[3]),
        .I4(state[2]),
        .I5(\subState[1]_i_2_n_0 ),
        .O(dataByte1));
  LUT6 #(
    .INIT(64'h00C0000040000000)) 
    \dataByte[7]_i_4 
       (.I0(state[0]),
        .I1(\subState[1]_i_2_n_0 ),
        .I2(\subState_reg_n_0_[0] ),
        .I3(\subState_reg_n_0_[1] ),
        .I4(\subState[1]_i_3_n_0 ),
        .I5(state[1]),
        .O(dataByte0));
  LUT6 #(
    .INIT(64'hFFFFDFFFFFFFDF7F)) 
    \dataByte[7]_i_5 
       (.I0(\subState[1]_i_2_n_0 ),
        .I1(state[1]),
        .I2(state[0]),
        .I3(state[2]),
        .I4(state[3]),
        .I5(\dataByte[7]_i_7_n_0 ),
        .O(\dataByte[7]_i_5_n_0 ));
  LUT4 #(
    .INIT(16'h0400)) 
    \dataByte[7]_i_6 
       (.I0(state[1]),
        .I1(state[0]),
        .I2(state[3]),
        .I3(state[2]),
        .O(\dataByte[7]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair67" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \dataByte[7]_i_7 
       (.I0(\subState_reg_n_0_[0] ),
        .I1(\subState_reg_n_0_[1] ),
        .O(\dataByte[7]_i_7_n_0 ));
  FDRE \dataByte_reg[0] 
       (.C(clk_out2),
        .CE(\dataByte[7]_i_1_n_0 ),
        .D(\dataByte[0]_i_1_n_0 ),
        .Q(\dataByte_reg_n_0_[0] ),
        .R(1'b0));
  FDRE \dataByte_reg[1] 
       (.C(clk_out2),
        .CE(\dataByte[7]_i_1_n_0 ),
        .D(\dataByte[1]_i_1_n_0 ),
        .Q(\dataByte_reg_n_0_[1] ),
        .R(1'b0));
  FDRE \dataByte_reg[2] 
       (.C(clk_out2),
        .CE(\dataByte[7]_i_1_n_0 ),
        .D(\dataByte[2]_i_1_n_0 ),
        .Q(\dataByte_reg_n_0_[2] ),
        .R(1'b0));
  FDRE \dataByte_reg[3] 
       (.C(clk_out2),
        .CE(\dataByte[7]_i_1_n_0 ),
        .D(\dataByte[3]_i_1_n_0 ),
        .Q(\dataByte_reg_n_0_[3] ),
        .R(1'b0));
  FDRE \dataByte_reg[4] 
       (.C(clk_out2),
        .CE(\dataByte[7]_i_1_n_0 ),
        .D(\dataByte[4]_i_1_n_0 ),
        .Q(\dataByte_reg_n_0_[4] ),
        .R(1'b0));
  FDRE \dataByte_reg[5] 
       (.C(clk_out2),
        .CE(\dataByte[7]_i_1_n_0 ),
        .D(\dataByte[5]_i_1_n_0 ),
        .Q(\dataByte_reg_n_0_[5] ),
        .R(1'b0));
  FDRE \dataByte_reg[6] 
       (.C(clk_out2),
        .CE(\dataByte[7]_i_1_n_0 ),
        .D(\dataByte[6]_i_1_n_0 ),
        .Q(\dataByte_reg_n_0_[6] ),
        .R(1'b0));
  FDRE \dataByte_reg[7] 
       (.C(clk_out2),
        .CE(\dataByte[7]_i_1_n_0 ),
        .D(\dataByte[7]_i_2_n_0 ),
        .Q(\dataByte_reg_n_0_[7] ),
        .R(1'b0));
  FDRE ddSda_reg
       (.C(clk_out2),
        .CE(1'b1),
        .D(p_0_in),
        .Q(ddSda),
        .R(1'b0));
  LUT4 #(
    .INIT(16'hF444)) 
    \initA[6]_i_1 
       (.I0(Q[3]),
        .I1(initEn_reg_0),
        .I2(done),
        .I3(D[3]),
        .O(\initA_reg[6] ));
  LUT6 #(
    .INIT(64'hFFFF57FF00005400)) 
    initEn_i_1
       (.I0(Q[2]),
        .I1(initEn_i_2_n_0),
        .I2(initEn_reg_0),
        .I3(s00_axi_aresetn),
        .I4(Q[3]),
        .I5(initEn),
        .O(initEn_reg));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT4 #(
    .INIT(16'h0002)) 
    initEn_i_2
       (.I0(done),
        .I1(Q[2]),
        .I2(error),
        .I3(\initWord_reg[6] ),
        .O(initEn_i_2_n_0));
  LUT6 #(
    .INIT(64'hBBBBBBB3BBBBBBBB)) 
    int_Rst_i_1
       (.I0(int_Rst),
        .I1(s00_axi_aresetn),
        .I2(state[3]),
        .I3(state[2]),
        .I4(state[1]),
        .I5(state[0]),
        .O(int_Rst_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    int_Rst_reg
       (.C(clk_out2),
        .CE(1'b1),
        .D(int_Rst_i_1_n_0),
        .Q(int_Rst),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hFFBF003C)) 
    rScl_i_1
       (.I0(state[3]),
        .I1(\subState_reg_n_0_[1] ),
        .I2(\subState_reg_n_0_[0] ),
        .I3(subState),
        .I4(rScl),
        .O(rScl_i_1_n_0));
  LUT4 #(
    .INIT(16'h0100)) 
    rScl_i_2
       (.I0(state[3]),
        .I1(state[2]),
        .I2(state[1]),
        .I3(state[0]),
        .O(subState));
  FDRE #(
    .INIT(1'b1)) 
    rScl_reg
       (.C(clk_out2),
        .CE(1'b1),
        .D(rScl_i_1_n_0),
        .Q(rScl),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFFAABA0000AA8A)) 
    rSda_i_1
       (.I0(rSda_i_2_n_0),
        .I1(state[3]),
        .I2(rSda_i_3_n_0),
        .I3(\dataByte[7]_i_6_n_0 ),
        .I4(rSda_i_4_n_0),
        .I5(rSda),
        .O(rSda_i_1_n_0));
  LUT6 #(
    .INIT(64'h0088FFC000AA55D1)) 
    rSda_i_2
       (.I0(state[3]),
        .I1(\FSM_gray_state[3]_i_6_n_0 ),
        .I2(\dataByte_reg_n_0_[7] ),
        .I3(\subState_reg_n_0_[0] ),
        .I4(\subState_reg_n_0_[1] ),
        .I5(rSda_i_5_n_0),
        .O(rSda_i_2_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    rSda_i_3
       (.I0(\subState_reg_n_0_[0] ),
        .I1(\subState_reg_n_0_[1] ),
        .O(rSda_i_3_n_0));
  LUT6 #(
    .INIT(64'hFF00000002020202)) 
    rSda_i_4
       (.I0(state[0]),
        .I1(state[1]),
        .I2(state[2]),
        .I3(\subState_reg_n_0_[0] ),
        .I4(\subState_reg_n_0_[1] ),
        .I5(state[3]),
        .O(rSda_i_4_n_0));
  LUT4 #(
    .INIT(16'h0400)) 
    rSda_i_5
       (.I0(state[3]),
        .I1(state[2]),
        .I2(state[0]),
        .I3(state[1]),
        .O(rSda_i_5_n_0));
  FDRE #(
    .INIT(1'b1)) 
    rSda_reg
       (.C(clk_out2),
        .CE(1'b1),
        .D(rSda_i_1_n_0),
        .Q(rSda),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    \sclCnt[0]_i_1 
       (.I0(sclCnt_reg__0[0]),
        .O(sclCnt0[0]));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \sclCnt[1]_i_1 
       (.I0(sclCnt_reg__0[1]),
        .I1(sclCnt_reg__0[0]),
        .O(\sclCnt[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT3 #(
    .INIT(8'hA9)) 
    \sclCnt[2]_i_1 
       (.I0(sclCnt_reg__0[2]),
        .I1(sclCnt_reg__0[0]),
        .I2(sclCnt_reg__0[1]),
        .O(sclCnt0[2]));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT4 #(
    .INIT(16'hAAA9)) 
    \sclCnt[3]_i_1 
       (.I0(sclCnt_reg__0[3]),
        .I1(sclCnt_reg__0[2]),
        .I2(sclCnt_reg__0[1]),
        .I3(sclCnt_reg__0[0]),
        .O(sclCnt0[3]));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT5 #(
    .INIT(32'hAAAAAAA9)) 
    \sclCnt[4]_i_1 
       (.I0(sclCnt_reg__0[4]),
        .I1(sclCnt_reg__0[3]),
        .I2(sclCnt_reg__0[0]),
        .I3(sclCnt_reg__0[1]),
        .I4(sclCnt_reg__0[2]),
        .O(sclCnt0[4]));
  LUT6 #(
    .INIT(64'hAAAAAAAAAAAAAAA9)) 
    \sclCnt[5]_i_1 
       (.I0(sclCnt_reg__0[5]),
        .I1(sclCnt_reg__0[4]),
        .I2(sclCnt_reg__0[2]),
        .I3(sclCnt_reg__0[1]),
        .I4(sclCnt_reg__0[0]),
        .I5(sclCnt_reg__0[3]),
        .O(sclCnt0[5]));
  LUT5 #(
    .INIT(32'hFFFF0002)) 
    \sclCnt[6]_i_1 
       (.I0(state[0]),
        .I1(state[1]),
        .I2(state[2]),
        .I3(state[3]),
        .I4(\subState[1]_i_2_n_0 ),
        .O(sclCnt0_0));
  LUT2 #(
    .INIT(4'hB)) 
    \sclCnt[6]_i_2 
       (.I0(dScl),
        .I1(rScl),
        .O(\sclCnt[6]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair68" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \sclCnt[6]_i_3 
       (.I0(sclCnt_reg__0[6]),
        .I1(\sclCnt[6]_i_4_n_0 ),
        .O(sclCnt0[6]));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \sclCnt[6]_i_4 
       (.I0(sclCnt_reg__0[4]),
        .I1(sclCnt_reg__0[2]),
        .I2(sclCnt_reg__0[1]),
        .I3(sclCnt_reg__0[0]),
        .I4(sclCnt_reg__0[3]),
        .I5(sclCnt_reg__0[5]),
        .O(\sclCnt[6]_i_4_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \sclCnt_reg[0] 
       (.C(clk_out2),
        .CE(\sclCnt[6]_i_2_n_0 ),
        .D(sclCnt0[0]),
        .Q(sclCnt_reg__0[0]),
        .S(sclCnt0_0));
  FDSE #(
    .INIT(1'b0)) 
    \sclCnt_reg[1] 
       (.C(clk_out2),
        .CE(\sclCnt[6]_i_2_n_0 ),
        .D(\sclCnt[1]_i_1_n_0 ),
        .Q(sclCnt_reg__0[1]),
        .S(sclCnt0_0));
  FDSE #(
    .INIT(1'b1)) 
    \sclCnt_reg[2] 
       (.C(clk_out2),
        .CE(\sclCnt[6]_i_2_n_0 ),
        .D(sclCnt0[2]),
        .Q(sclCnt_reg__0[2]),
        .S(sclCnt0_0));
  FDSE #(
    .INIT(1'b1)) 
    \sclCnt_reg[3] 
       (.C(clk_out2),
        .CE(\sclCnt[6]_i_2_n_0 ),
        .D(sclCnt0[3]),
        .Q(sclCnt_reg__0[3]),
        .S(sclCnt0_0));
  FDSE #(
    .INIT(1'b1)) 
    \sclCnt_reg[4] 
       (.C(clk_out2),
        .CE(\sclCnt[6]_i_2_n_0 ),
        .D(sclCnt0[4]),
        .Q(sclCnt_reg__0[4]),
        .S(sclCnt0_0));
  FDRE #(
    .INIT(1'b1)) 
    \sclCnt_reg[5] 
       (.C(clk_out2),
        .CE(\sclCnt[6]_i_2_n_0 ),
        .D(sclCnt0[5]),
        .Q(sclCnt_reg__0[5]),
        .R(sclCnt0_0));
  FDRE #(
    .INIT(1'b1)) 
    \sclCnt_reg[6] 
       (.C(clk_out2),
        .CE(\sclCnt[6]_i_2_n_0 ),
        .D(sclCnt0[6]),
        .Q(sclCnt_reg__0[6]),
        .R(sclCnt0_0));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    scl_INST_0
       (.I0(1'b0),
        .I1(scl_INST_0_i_1_n_0),
        .I2(1'b0),
        .I3(1'b0),
        .I4(1'b0),
        .I5(1'b0),
        .O(scl));
  LUT1 #(
    .INIT(2'h1)) 
    scl_INST_0_i_1
       (.I0(rScl),
        .O(scl_INST_0_i_1_n_0));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    sda_INST_0
       (.I0(1'b0),
        .I1(sda_INST_0_i_1_n_0),
        .I2(1'b0),
        .I3(1'b0),
        .I4(1'b0),
        .I5(1'b0),
        .O(sda));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT1 #(
    .INIT(2'h1)) 
    sda_INST_0_i_1
       (.I0(rSda),
        .O(sda_INST_0_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT5 #(
    .INIT(32'h0000AAFB)) 
    \state[0]_i_1 
       (.I0(Q[3]),
        .I1(Q[0]),
        .I2(Q[1]),
        .I3(Q[2]),
        .I4(error),
        .O(D[0]));
  LUT6 #(
    .INIT(64'hCCC0CCCC88808880)) 
    \state[1]_i_1 
       (.I0(Q[1]),
        .I1(\state[1]_i_2_n_0 ),
        .I2(\initWord_reg[6] ),
        .I3(\initA_reg[4] ),
        .I4(\initWord_reg[21] ),
        .I5(Q[0]),
        .O(D[1]));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT3 #(
    .INIT(8'h01)) 
    \state[1]_i_2 
       (.I0(error),
        .I1(Q[2]),
        .I2(Q[3]),
        .O(\state[1]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT4 #(
    .INIT(16'hC0F1)) 
    \state[2]_i_1 
       (.I0(\initWord_reg[6] ),
        .I1(Q[3]),
        .I2(error),
        .I3(Q[2]),
        .O(D[2]));
  LUT6 #(
    .INIT(64'h80CF00AA800F00AA)) 
    \state[3]_i_1 
       (.I0(done),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(Q[3]),
        .I4(Q[2]),
        .I5(\delaycnt_reg[20] ),
        .O(E));
  LUT6 #(
    .INIT(64'h0001000000000000)) 
    \state[3]_i_2 
       (.I0(\initWord_reg[21] ),
        .I1(error),
        .I2(Q[2]),
        .I3(Q[3]),
        .I4(Q[1]),
        .I5(Q[0]),
        .O(D[3]));
  LUT6 #(
    .INIT(64'h6666666666666606)) 
    \subState[0]_i_1 
       (.I0(\subState_reg_n_0_[0] ),
        .I1(\subState[1]_i_2_n_0 ),
        .I2(state[0]),
        .I3(state[1]),
        .I4(state[2]),
        .I5(state[3]),
        .O(\subState[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h6A6A006A6A6A6A6A)) 
    \subState[1]_i_1 
       (.I0(\subState_reg_n_0_[1] ),
        .I1(\subState[1]_i_2_n_0 ),
        .I2(\subState_reg_n_0_[0] ),
        .I3(state[0]),
        .I4(state[1]),
        .I5(\subState[1]_i_3_n_0 ),
        .O(\subState[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair68" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \subState[1]_i_2 
       (.I0(\sclCnt[6]_i_4_n_0 ),
        .I1(sclCnt_reg__0[6]),
        .O(\subState[1]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \subState[1]_i_3 
       (.I0(state[2]),
        .I1(state[3]),
        .O(\subState[1]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \subState_reg[0] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(\subState[0]_i_1_n_0 ),
        .Q(\subState_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \subState_reg[1] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(\subState[1]_i_1_n_0 ),
        .Q(\subState_reg_n_0_[1] ),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_audio_init
   (scl,
    sda,
    clk_out2,
    s00_axi_aresetn,
    SR);
  inout scl;
  inout sda;
  input clk_out2;
  input s00_axi_aresetn;
  input [0:0]SR;

  wire [0:0]SR;
  wire clk_out2;
  wire [6:6]data0;
  wire [7:0]data1;
  wire [7:0]data2;
  wire [7:0]data_i;
  wire \data_i[0]_i_1_n_0 ;
  wire \data_i[1]_i_1_n_0 ;
  wire \data_i[2]_i_1_n_0 ;
  wire \data_i[3]_i_1_n_0 ;
  wire \data_i[4]_i_1_n_0 ;
  wire \data_i[4]_i_2_n_0 ;
  wire \data_i[5]_i_1_n_0 ;
  wire \data_i[5]_i_2_n_0 ;
  wire \data_i[6]_i_1_n_0 ;
  wire \data_i[6]_i_2_n_0 ;
  wire \data_i[7]_i_1_n_0 ;
  wire delayEn;
  wire delayEn_i_1_n_0;
  wire delayEn_i_2_n_0;
  wire [31:0]delaycnt;
  wire delaycnt0;
  wire delaycnt0_carry__0_i_1_n_0;
  wire delaycnt0_carry__0_i_2_n_0;
  wire delaycnt0_carry__0_i_3_n_0;
  wire delaycnt0_carry__0_i_4_n_0;
  wire delaycnt0_carry__0_n_0;
  wire delaycnt0_carry__0_n_1;
  wire delaycnt0_carry__0_n_2;
  wire delaycnt0_carry__0_n_3;
  wire delaycnt0_carry__0_n_4;
  wire delaycnt0_carry__0_n_5;
  wire delaycnt0_carry__0_n_6;
  wire delaycnt0_carry__0_n_7;
  wire delaycnt0_carry__1_i_1_n_0;
  wire delaycnt0_carry__1_i_2_n_0;
  wire delaycnt0_carry__1_i_3_n_0;
  wire delaycnt0_carry__1_i_4_n_0;
  wire delaycnt0_carry__1_n_0;
  wire delaycnt0_carry__1_n_1;
  wire delaycnt0_carry__1_n_2;
  wire delaycnt0_carry__1_n_3;
  wire delaycnt0_carry__1_n_4;
  wire delaycnt0_carry__1_n_5;
  wire delaycnt0_carry__1_n_6;
  wire delaycnt0_carry__1_n_7;
  wire delaycnt0_carry__2_i_1_n_0;
  wire delaycnt0_carry__2_i_2_n_0;
  wire delaycnt0_carry__2_i_3_n_0;
  wire delaycnt0_carry__2_i_4_n_0;
  wire delaycnt0_carry__2_n_0;
  wire delaycnt0_carry__2_n_1;
  wire delaycnt0_carry__2_n_2;
  wire delaycnt0_carry__2_n_3;
  wire delaycnt0_carry__2_n_4;
  wire delaycnt0_carry__2_n_5;
  wire delaycnt0_carry__2_n_6;
  wire delaycnt0_carry__2_n_7;
  wire delaycnt0_carry__3_i_1_n_0;
  wire delaycnt0_carry__3_i_2_n_0;
  wire delaycnt0_carry__3_i_3_n_0;
  wire delaycnt0_carry__3_i_4_n_0;
  wire delaycnt0_carry__3_n_0;
  wire delaycnt0_carry__3_n_1;
  wire delaycnt0_carry__3_n_2;
  wire delaycnt0_carry__3_n_3;
  wire delaycnt0_carry__3_n_4;
  wire delaycnt0_carry__3_n_5;
  wire delaycnt0_carry__3_n_6;
  wire delaycnt0_carry__3_n_7;
  wire delaycnt0_carry__4_i_1_n_0;
  wire delaycnt0_carry__4_i_2_n_0;
  wire delaycnt0_carry__4_i_3_n_0;
  wire delaycnt0_carry__4_i_4_n_0;
  wire delaycnt0_carry__4_n_0;
  wire delaycnt0_carry__4_n_1;
  wire delaycnt0_carry__4_n_2;
  wire delaycnt0_carry__4_n_3;
  wire delaycnt0_carry__4_n_4;
  wire delaycnt0_carry__4_n_5;
  wire delaycnt0_carry__4_n_6;
  wire delaycnt0_carry__4_n_7;
  wire delaycnt0_carry__5_i_1_n_0;
  wire delaycnt0_carry__5_i_2_n_0;
  wire delaycnt0_carry__5_i_3_n_0;
  wire delaycnt0_carry__5_i_4_n_0;
  wire delaycnt0_carry__5_n_0;
  wire delaycnt0_carry__5_n_1;
  wire delaycnt0_carry__5_n_2;
  wire delaycnt0_carry__5_n_3;
  wire delaycnt0_carry__5_n_4;
  wire delaycnt0_carry__5_n_5;
  wire delaycnt0_carry__5_n_6;
  wire delaycnt0_carry__5_n_7;
  wire delaycnt0_carry__6_i_1_n_0;
  wire delaycnt0_carry__6_i_2_n_0;
  wire delaycnt0_carry__6_i_3_n_0;
  wire delaycnt0_carry__6_n_2;
  wire delaycnt0_carry__6_n_3;
  wire delaycnt0_carry__6_n_5;
  wire delaycnt0_carry__6_n_6;
  wire delaycnt0_carry__6_n_7;
  wire delaycnt0_carry_i_1_n_0;
  wire delaycnt0_carry_i_2_n_0;
  wire delaycnt0_carry_i_3_n_0;
  wire delaycnt0_carry_i_4_n_0;
  wire delaycnt0_carry_n_0;
  wire delaycnt0_carry_n_1;
  wire delaycnt0_carry_n_2;
  wire delaycnt0_carry_n_3;
  wire delaycnt0_carry_n_4;
  wire delaycnt0_carry_n_5;
  wire delaycnt0_carry_n_6;
  wire delaycnt0_carry_n_7;
  wire \delaycnt[0]_i_1_n_0 ;
  wire \initA[0]_i_1_n_0 ;
  wire \initA[6]_i_3_n_0 ;
  wire \initA_reg_n_0_[0] ;
  wire \initA_reg_n_0_[1] ;
  wire \initA_reg_n_0_[2] ;
  wire \initA_reg_n_0_[3] ;
  wire \initA_reg_n_0_[4] ;
  wire \initA_reg_n_0_[5] ;
  wire \initA_reg_n_0_[6] ;
  wire initEn;
  wire \initWord[0]_i_1_n_0 ;
  wire \initWord[10]_i_1_n_0 ;
  wire \initWord[11]_i_1_n_0 ;
  wire \initWord[12]_i_1_n_0 ;
  wire \initWord[13]_i_1_n_0 ;
  wire \initWord[14]_i_1_n_0 ;
  wire \initWord[15]_i_1_n_0 ;
  wire \initWord[16]_i_1_n_0 ;
  wire \initWord[17]_i_1_n_0 ;
  wire \initWord[18]_i_1_n_0 ;
  wire \initWord[19]_i_1_n_0 ;
  wire \initWord[20]_i_1_n_0 ;
  wire \initWord[21]_i_1_n_0 ;
  wire \initWord[23]_i_1_n_0 ;
  wire \initWord[30]_i_1_n_0 ;
  wire \initWord[30]_i_2_n_0 ;
  wire \initWord[30]_i_3_n_0 ;
  wire \initWord[6]_i_1_n_0 ;
  wire \initWord[8]_i_1_n_0 ;
  wire \initWord[9]_i_1_n_0 ;
  wire \initWord_reg_n_0_[0] ;
  wire \initWord_reg_n_0_[6] ;
  wire msg;
  wire msg0;
  wire [6:1]p_1_in__0;
  wire s00_axi_aresetn;
  wire scl;
  wire sda;
  wire \state[1]_i_3_n_0 ;
  wire \state[1]_i_4_n_0 ;
  wire \state[2]_i_2_n_0 ;
  wire \state[3]_i_10_n_0 ;
  wire \state[3]_i_11_n_0 ;
  wire \state[3]_i_12_n_0 ;
  wire \state[3]_i_13_n_0 ;
  wire \state[3]_i_3_n_0 ;
  wire \state[3]_i_4_n_0 ;
  wire \state[3]_i_5_n_0 ;
  wire \state[3]_i_6_n_0 ;
  wire \state[3]_i_7_n_0 ;
  wire \state[3]_i_8_n_0 ;
  wire \state[3]_i_9_n_0 ;
  wire \state_reg_n_0_[0] ;
  wire \state_reg_n_0_[1] ;
  wire \state_reg_n_0_[2] ;
  wire \state_reg_n_0_[3] ;
  wire stb;
  wire stb_i_1_n_0;
  wire twi_controller_n_0;
  wire twi_controller_n_1;
  wire twi_controller_n_2;
  wire twi_controller_n_3;
  wire twi_controller_n_4;
  wire twi_controller_n_5;
  wire twi_controller_n_6;
  wire [3:2]NLW_delaycnt0_carry__6_CO_UNCONNECTED;
  wire [3:3]NLW_delaycnt0_carry__6_O_UNCONNECTED;

  LUT6 #(
    .INIT(64'hF0AACC00FFFFFFFF)) 
    \data_i[0]_i_1 
       (.I0(data2[0]),
        .I1(data1[0]),
        .I2(\initWord_reg_n_0_[0] ),
        .I3(\state_reg_n_0_[0] ),
        .I4(\state_reg_n_0_[1] ),
        .I5(\data_i[4]_i_2_n_0 ),
        .O(\data_i[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0010110000100000)) 
    \data_i[1]_i_1 
       (.I0(\state_reg_n_0_[3] ),
        .I1(\state_reg_n_0_[2] ),
        .I2(data1[1]),
        .I3(\state_reg_n_0_[1] ),
        .I4(\state_reg_n_0_[0] ),
        .I5(data2[1]),
        .O(\data_i[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hF0CCAA00FFFFFFFF)) 
    \data_i[2]_i_1 
       (.I0(data1[2]),
        .I1(data2[2]),
        .I2(\initWord_reg_n_0_[6] ),
        .I3(\state_reg_n_0_[0] ),
        .I4(\state_reg_n_0_[1] ),
        .I5(\data_i[4]_i_2_n_0 ),
        .O(\data_i[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hF0AACC00FFFFFFFF)) 
    \data_i[3]_i_1 
       (.I0(data2[3]),
        .I1(data1[3]),
        .I2(\initWord_reg_n_0_[6] ),
        .I3(\state_reg_n_0_[0] ),
        .I4(\state_reg_n_0_[1] ),
        .I5(\data_i[4]_i_2_n_0 ),
        .O(\data_i[3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8AA08A0080A08000)) 
    \data_i[4]_i_1 
       (.I0(\data_i[4]_i_2_n_0 ),
        .I1(\initWord_reg_n_0_[6] ),
        .I2(\state_reg_n_0_[0] ),
        .I3(\state_reg_n_0_[1] ),
        .I4(data1[4]),
        .I5(data2[4]),
        .O(\data_i[4]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \data_i[4]_i_2 
       (.I0(\state_reg_n_0_[2] ),
        .I1(\state_reg_n_0_[3] ),
        .O(\data_i[4]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h80000000)) 
    \data_i[5]_i_1 
       (.I0(\state_reg_n_0_[2] ),
        .I1(\state_reg_n_0_[0] ),
        .I2(\state_reg_n_0_[1] ),
        .I3(\state_reg_n_0_[3] ),
        .I4(s00_axi_aresetn),
        .O(\data_i[5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hF0AACC00FFFFFFFF)) 
    \data_i[5]_i_2 
       (.I0(data2[5]),
        .I1(data1[5]),
        .I2(\initWord_reg_n_0_[6] ),
        .I3(\state_reg_n_0_[0] ),
        .I4(\state_reg_n_0_[1] ),
        .I5(\data_i[4]_i_2_n_0 ),
        .O(\data_i[5]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h80800000000010FF)) 
    \data_i[6]_i_1 
       (.I0(\state_reg_n_0_[1] ),
        .I1(\state_reg_n_0_[0] ),
        .I2(data0),
        .I3(\data_i[6]_i_2_n_0 ),
        .I4(\state_reg_n_0_[2] ),
        .I5(\state_reg_n_0_[3] ),
        .O(\data_i[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair69" *) 
  LUT5 #(
    .INIT(32'h053FF53F)) 
    \data_i[6]_i_2 
       (.I0(data1[7]),
        .I1(data2[6]),
        .I2(\state_reg_n_0_[1] ),
        .I3(\state_reg_n_0_[0] ),
        .I4(\initWord_reg_n_0_[6] ),
        .O(\data_i[6]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000A000C0000)) 
    \data_i[7]_i_1 
       (.I0(data1[7]),
        .I1(data2[7]),
        .I2(\state_reg_n_0_[2] ),
        .I3(\state_reg_n_0_[3] ),
        .I4(\state_reg_n_0_[1] ),
        .I5(\state_reg_n_0_[0] ),
        .O(\data_i[7]_i_1_n_0 ));
  FDRE \data_i_reg[0] 
       (.C(clk_out2),
        .CE(s00_axi_aresetn),
        .D(\data_i[0]_i_1_n_0 ),
        .Q(data_i[0]),
        .R(\data_i[5]_i_1_n_0 ));
  FDRE \data_i_reg[1] 
       (.C(clk_out2),
        .CE(s00_axi_aresetn),
        .D(\data_i[1]_i_1_n_0 ),
        .Q(data_i[1]),
        .R(1'b0));
  FDRE \data_i_reg[2] 
       (.C(clk_out2),
        .CE(s00_axi_aresetn),
        .D(\data_i[2]_i_1_n_0 ),
        .Q(data_i[2]),
        .R(\data_i[5]_i_1_n_0 ));
  FDRE \data_i_reg[3] 
       (.C(clk_out2),
        .CE(s00_axi_aresetn),
        .D(\data_i[3]_i_1_n_0 ),
        .Q(data_i[3]),
        .R(\data_i[5]_i_1_n_0 ));
  FDRE \data_i_reg[4] 
       (.C(clk_out2),
        .CE(s00_axi_aresetn),
        .D(\data_i[4]_i_1_n_0 ),
        .Q(data_i[4]),
        .R(1'b0));
  FDRE \data_i_reg[5] 
       (.C(clk_out2),
        .CE(s00_axi_aresetn),
        .D(\data_i[5]_i_2_n_0 ),
        .Q(data_i[5]),
        .R(\data_i[5]_i_1_n_0 ));
  FDRE \data_i_reg[6] 
       (.C(clk_out2),
        .CE(s00_axi_aresetn),
        .D(\data_i[6]_i_1_n_0 ),
        .Q(data_i[6]),
        .R(1'b0));
  FDRE \data_i_reg[7] 
       (.C(clk_out2),
        .CE(s00_axi_aresetn),
        .D(\data_i[7]_i_1_n_0 ),
        .Q(data_i[7]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hA2AAAEAA00000000)) 
    delayEn_i_1
       (.I0(delayEn),
        .I1(\state_reg_n_0_[2] ),
        .I2(\state_reg_n_0_[3] ),
        .I3(delayEn_i_2_n_0),
        .I4(\state[3]_i_3_n_0 ),
        .I5(s00_axi_aresetn),
        .O(delayEn_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair69" *) 
  LUT2 #(
    .INIT(4'h8)) 
    delayEn_i_2
       (.I0(\state_reg_n_0_[1] ),
        .I1(\state_reg_n_0_[0] ),
        .O(delayEn_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    delayEn_reg
       (.C(clk_out2),
        .CE(1'b1),
        .D(delayEn_i_1_n_0),
        .Q(delayEn),
        .R(1'b0));
  CARRY4 delaycnt0_carry
       (.CI(1'b0),
        .CO({delaycnt0_carry_n_0,delaycnt0_carry_n_1,delaycnt0_carry_n_2,delaycnt0_carry_n_3}),
        .CYINIT(delaycnt[0]),
        .DI(delaycnt[4:1]),
        .O({delaycnt0_carry_n_4,delaycnt0_carry_n_5,delaycnt0_carry_n_6,delaycnt0_carry_n_7}),
        .S({delaycnt0_carry_i_1_n_0,delaycnt0_carry_i_2_n_0,delaycnt0_carry_i_3_n_0,delaycnt0_carry_i_4_n_0}));
  CARRY4 delaycnt0_carry__0
       (.CI(delaycnt0_carry_n_0),
        .CO({delaycnt0_carry__0_n_0,delaycnt0_carry__0_n_1,delaycnt0_carry__0_n_2,delaycnt0_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI(delaycnt[8:5]),
        .O({delaycnt0_carry__0_n_4,delaycnt0_carry__0_n_5,delaycnt0_carry__0_n_6,delaycnt0_carry__0_n_7}),
        .S({delaycnt0_carry__0_i_1_n_0,delaycnt0_carry__0_i_2_n_0,delaycnt0_carry__0_i_3_n_0,delaycnt0_carry__0_i_4_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__0_i_1
       (.I0(delaycnt[8]),
        .O(delaycnt0_carry__0_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__0_i_2
       (.I0(delaycnt[7]),
        .O(delaycnt0_carry__0_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__0_i_3
       (.I0(delaycnt[6]),
        .O(delaycnt0_carry__0_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__0_i_4
       (.I0(delaycnt[5]),
        .O(delaycnt0_carry__0_i_4_n_0));
  CARRY4 delaycnt0_carry__1
       (.CI(delaycnt0_carry__0_n_0),
        .CO({delaycnt0_carry__1_n_0,delaycnt0_carry__1_n_1,delaycnt0_carry__1_n_2,delaycnt0_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI(delaycnt[12:9]),
        .O({delaycnt0_carry__1_n_4,delaycnt0_carry__1_n_5,delaycnt0_carry__1_n_6,delaycnt0_carry__1_n_7}),
        .S({delaycnt0_carry__1_i_1_n_0,delaycnt0_carry__1_i_2_n_0,delaycnt0_carry__1_i_3_n_0,delaycnt0_carry__1_i_4_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__1_i_1
       (.I0(delaycnt[12]),
        .O(delaycnt0_carry__1_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__1_i_2
       (.I0(delaycnt[11]),
        .O(delaycnt0_carry__1_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__1_i_3
       (.I0(delaycnt[10]),
        .O(delaycnt0_carry__1_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__1_i_4
       (.I0(delaycnt[9]),
        .O(delaycnt0_carry__1_i_4_n_0));
  CARRY4 delaycnt0_carry__2
       (.CI(delaycnt0_carry__1_n_0),
        .CO({delaycnt0_carry__2_n_0,delaycnt0_carry__2_n_1,delaycnt0_carry__2_n_2,delaycnt0_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI(delaycnt[16:13]),
        .O({delaycnt0_carry__2_n_4,delaycnt0_carry__2_n_5,delaycnt0_carry__2_n_6,delaycnt0_carry__2_n_7}),
        .S({delaycnt0_carry__2_i_1_n_0,delaycnt0_carry__2_i_2_n_0,delaycnt0_carry__2_i_3_n_0,delaycnt0_carry__2_i_4_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__2_i_1
       (.I0(delaycnt[16]),
        .O(delaycnt0_carry__2_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__2_i_2
       (.I0(delaycnt[15]),
        .O(delaycnt0_carry__2_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__2_i_3
       (.I0(delaycnt[14]),
        .O(delaycnt0_carry__2_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__2_i_4
       (.I0(delaycnt[13]),
        .O(delaycnt0_carry__2_i_4_n_0));
  CARRY4 delaycnt0_carry__3
       (.CI(delaycnt0_carry__2_n_0),
        .CO({delaycnt0_carry__3_n_0,delaycnt0_carry__3_n_1,delaycnt0_carry__3_n_2,delaycnt0_carry__3_n_3}),
        .CYINIT(1'b0),
        .DI(delaycnt[20:17]),
        .O({delaycnt0_carry__3_n_4,delaycnt0_carry__3_n_5,delaycnt0_carry__3_n_6,delaycnt0_carry__3_n_7}),
        .S({delaycnt0_carry__3_i_1_n_0,delaycnt0_carry__3_i_2_n_0,delaycnt0_carry__3_i_3_n_0,delaycnt0_carry__3_i_4_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__3_i_1
       (.I0(delaycnt[20]),
        .O(delaycnt0_carry__3_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__3_i_2
       (.I0(delaycnt[19]),
        .O(delaycnt0_carry__3_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__3_i_3
       (.I0(delaycnt[18]),
        .O(delaycnt0_carry__3_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__3_i_4
       (.I0(delaycnt[17]),
        .O(delaycnt0_carry__3_i_4_n_0));
  CARRY4 delaycnt0_carry__4
       (.CI(delaycnt0_carry__3_n_0),
        .CO({delaycnt0_carry__4_n_0,delaycnt0_carry__4_n_1,delaycnt0_carry__4_n_2,delaycnt0_carry__4_n_3}),
        .CYINIT(1'b0),
        .DI(delaycnt[24:21]),
        .O({delaycnt0_carry__4_n_4,delaycnt0_carry__4_n_5,delaycnt0_carry__4_n_6,delaycnt0_carry__4_n_7}),
        .S({delaycnt0_carry__4_i_1_n_0,delaycnt0_carry__4_i_2_n_0,delaycnt0_carry__4_i_3_n_0,delaycnt0_carry__4_i_4_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__4_i_1
       (.I0(delaycnt[24]),
        .O(delaycnt0_carry__4_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__4_i_2
       (.I0(delaycnt[23]),
        .O(delaycnt0_carry__4_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__4_i_3
       (.I0(delaycnt[22]),
        .O(delaycnt0_carry__4_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__4_i_4
       (.I0(delaycnt[21]),
        .O(delaycnt0_carry__4_i_4_n_0));
  CARRY4 delaycnt0_carry__5
       (.CI(delaycnt0_carry__4_n_0),
        .CO({delaycnt0_carry__5_n_0,delaycnt0_carry__5_n_1,delaycnt0_carry__5_n_2,delaycnt0_carry__5_n_3}),
        .CYINIT(1'b0),
        .DI(delaycnt[28:25]),
        .O({delaycnt0_carry__5_n_4,delaycnt0_carry__5_n_5,delaycnt0_carry__5_n_6,delaycnt0_carry__5_n_7}),
        .S({delaycnt0_carry__5_i_1_n_0,delaycnt0_carry__5_i_2_n_0,delaycnt0_carry__5_i_3_n_0,delaycnt0_carry__5_i_4_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__5_i_1
       (.I0(delaycnt[28]),
        .O(delaycnt0_carry__5_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__5_i_2
       (.I0(delaycnt[27]),
        .O(delaycnt0_carry__5_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__5_i_3
       (.I0(delaycnt[26]),
        .O(delaycnt0_carry__5_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__5_i_4
       (.I0(delaycnt[25]),
        .O(delaycnt0_carry__5_i_4_n_0));
  CARRY4 delaycnt0_carry__6
       (.CI(delaycnt0_carry__5_n_0),
        .CO({NLW_delaycnt0_carry__6_CO_UNCONNECTED[3:2],delaycnt0_carry__6_n_2,delaycnt0_carry__6_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,delaycnt[30:29]}),
        .O({NLW_delaycnt0_carry__6_O_UNCONNECTED[3],delaycnt0_carry__6_n_5,delaycnt0_carry__6_n_6,delaycnt0_carry__6_n_7}),
        .S({1'b0,delaycnt0_carry__6_i_1_n_0,delaycnt0_carry__6_i_2_n_0,delaycnt0_carry__6_i_3_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__6_i_1
       (.I0(delaycnt[31]),
        .O(delaycnt0_carry__6_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__6_i_2
       (.I0(delaycnt[30]),
        .O(delaycnt0_carry__6_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__6_i_3
       (.I0(delaycnt[29]),
        .O(delaycnt0_carry__6_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry_i_1
       (.I0(delaycnt[4]),
        .O(delaycnt0_carry_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry_i_2
       (.I0(delaycnt[3]),
        .O(delaycnt0_carry_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry_i_3
       (.I0(delaycnt[2]),
        .O(delaycnt0_carry_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry_i_4
       (.I0(delaycnt[1]),
        .O(delaycnt0_carry_i_4_n_0));
  (* SOFT_HLUTNM = "soft_lutpair73" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \delaycnt[0]_i_1 
       (.I0(delaycnt[0]),
        .O(\delaycnt[0]_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \delaycnt[31]_i_1 
       (.I0(delayEn),
        .O(delaycnt0));
  FDRE \delaycnt_reg[0] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(\delaycnt[0]_i_1_n_0 ),
        .Q(delaycnt[0]),
        .R(delaycnt0));
  FDSE \delaycnt_reg[10] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__1_n_6),
        .Q(delaycnt[10]),
        .S(delaycnt0));
  FDSE \delaycnt_reg[11] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__1_n_5),
        .Q(delaycnt[11]),
        .S(delaycnt0));
  FDSE \delaycnt_reg[12] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__1_n_4),
        .Q(delaycnt[12]),
        .S(delaycnt0));
  FDRE \delaycnt_reg[13] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__2_n_7),
        .Q(delaycnt[13]),
        .R(delaycnt0));
  FDSE \delaycnt_reg[14] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__2_n_6),
        .Q(delaycnt[14]),
        .S(delaycnt0));
  FDRE \delaycnt_reg[15] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__2_n_5),
        .Q(delaycnt[15]),
        .R(delaycnt0));
  FDRE \delaycnt_reg[16] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__2_n_4),
        .Q(delaycnt[16]),
        .R(delaycnt0));
  FDRE \delaycnt_reg[17] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__3_n_7),
        .Q(delaycnt[17]),
        .R(delaycnt0));
  FDRE \delaycnt_reg[18] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__3_n_6),
        .Q(delaycnt[18]),
        .R(delaycnt0));
  FDRE \delaycnt_reg[19] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__3_n_5),
        .Q(delaycnt[19]),
        .R(delaycnt0));
  FDRE \delaycnt_reg[1] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry_n_7),
        .Q(delaycnt[1]),
        .R(delaycnt0));
  FDRE \delaycnt_reg[20] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__3_n_4),
        .Q(delaycnt[20]),
        .R(delaycnt0));
  FDRE \delaycnt_reg[21] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__4_n_7),
        .Q(delaycnt[21]),
        .R(delaycnt0));
  FDRE \delaycnt_reg[22] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__4_n_6),
        .Q(delaycnt[22]),
        .R(delaycnt0));
  FDRE \delaycnt_reg[23] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__4_n_5),
        .Q(delaycnt[23]),
        .R(delaycnt0));
  FDRE \delaycnt_reg[24] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__4_n_4),
        .Q(delaycnt[24]),
        .R(delaycnt0));
  FDRE \delaycnt_reg[25] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__5_n_7),
        .Q(delaycnt[25]),
        .R(delaycnt0));
  FDRE \delaycnt_reg[26] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__5_n_6),
        .Q(delaycnt[26]),
        .R(delaycnt0));
  FDRE \delaycnt_reg[27] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__5_n_5),
        .Q(delaycnt[27]),
        .R(delaycnt0));
  FDRE \delaycnt_reg[28] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__5_n_4),
        .Q(delaycnt[28]),
        .R(delaycnt0));
  FDRE \delaycnt_reg[29] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__6_n_7),
        .Q(delaycnt[29]),
        .R(delaycnt0));
  FDRE \delaycnt_reg[2] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry_n_6),
        .Q(delaycnt[2]),
        .R(delaycnt0));
  FDRE \delaycnt_reg[30] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__6_n_6),
        .Q(delaycnt[30]),
        .R(delaycnt0));
  FDRE \delaycnt_reg[31] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__6_n_5),
        .Q(delaycnt[31]),
        .R(delaycnt0));
  FDRE \delaycnt_reg[3] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry_n_5),
        .Q(delaycnt[3]),
        .R(delaycnt0));
  FDRE \delaycnt_reg[4] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry_n_4),
        .Q(delaycnt[4]),
        .R(delaycnt0));
  FDRE \delaycnt_reg[5] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__0_n_7),
        .Q(delaycnt[5]),
        .R(delaycnt0));
  FDSE \delaycnt_reg[6] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__0_n_6),
        .Q(delaycnt[6]),
        .S(delaycnt0));
  FDSE \delaycnt_reg[7] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__0_n_5),
        .Q(delaycnt[7]),
        .S(delaycnt0));
  FDSE \delaycnt_reg[8] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__0_n_4),
        .Q(delaycnt[8]),
        .S(delaycnt0));
  FDRE \delaycnt_reg[9] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__1_n_7),
        .Q(delaycnt[9]),
        .R(delaycnt0));
  (* SOFT_HLUTNM = "soft_lutpair76" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \initA[0]_i_1 
       (.I0(\initA_reg_n_0_[0] ),
        .O(\initA[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair76" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \initA[1]_i_1 
       (.I0(\initA_reg_n_0_[0] ),
        .I1(\initA_reg_n_0_[1] ),
        .O(p_1_in__0[1]));
  (* SOFT_HLUTNM = "soft_lutpair74" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \initA[2]_i_1 
       (.I0(\initA_reg_n_0_[2] ),
        .I1(\initA_reg_n_0_[1] ),
        .I2(\initA_reg_n_0_[0] ),
        .O(p_1_in__0[2]));
  (* SOFT_HLUTNM = "soft_lutpair71" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \initA[3]_i_1 
       (.I0(\initA_reg_n_0_[2] ),
        .I1(\initA_reg_n_0_[0] ),
        .I2(\initA_reg_n_0_[1] ),
        .I3(\initA_reg_n_0_[3] ),
        .O(p_1_in__0[3]));
  (* SOFT_HLUTNM = "soft_lutpair71" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \initA[4]_i_1 
       (.I0(\initA_reg_n_0_[4] ),
        .I1(\initA_reg_n_0_[0] ),
        .I2(\initA_reg_n_0_[1] ),
        .I3(\initA_reg_n_0_[3] ),
        .I4(\initA_reg_n_0_[2] ),
        .O(p_1_in__0[4]));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \initA[5]_i_1 
       (.I0(\initA_reg_n_0_[5] ),
        .I1(\initA_reg_n_0_[2] ),
        .I2(\initA_reg_n_0_[3] ),
        .I3(\initA_reg_n_0_[1] ),
        .I4(\initA_reg_n_0_[0] ),
        .I5(\initA_reg_n_0_[4] ),
        .O(p_1_in__0[5]));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \initA[6]_i_2 
       (.I0(\initA_reg_n_0_[6] ),
        .I1(\initA_reg_n_0_[5] ),
        .I2(\initA_reg_n_0_[4] ),
        .I3(\initWord[30]_i_3_n_0 ),
        .I4(\initA_reg_n_0_[3] ),
        .I5(\initA_reg_n_0_[2] ),
        .O(p_1_in__0[6]));
  LUT5 #(
    .INIT(32'h80000000)) 
    \initA[6]_i_3 
       (.I0(\state[3]_i_3_n_0 ),
        .I1(initEn),
        .I2(\state_reg_n_0_[2] ),
        .I3(\state_reg_n_0_[0] ),
        .I4(\state_reg_n_0_[1] ),
        .O(\initA[6]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \initA_reg[0] 
       (.C(clk_out2),
        .CE(twi_controller_n_5),
        .D(\initA[0]_i_1_n_0 ),
        .Q(\initA_reg_n_0_[0] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \initA_reg[1] 
       (.C(clk_out2),
        .CE(twi_controller_n_5),
        .D(p_1_in__0[1]),
        .Q(\initA_reg_n_0_[1] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \initA_reg[2] 
       (.C(clk_out2),
        .CE(twi_controller_n_5),
        .D(p_1_in__0[2]),
        .Q(\initA_reg_n_0_[2] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \initA_reg[3] 
       (.C(clk_out2),
        .CE(twi_controller_n_5),
        .D(p_1_in__0[3]),
        .Q(\initA_reg_n_0_[3] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \initA_reg[4] 
       (.C(clk_out2),
        .CE(twi_controller_n_5),
        .D(p_1_in__0[4]),
        .Q(\initA_reg_n_0_[4] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \initA_reg[5] 
       (.C(clk_out2),
        .CE(twi_controller_n_5),
        .D(p_1_in__0[5]),
        .Q(\initA_reg_n_0_[5] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \initA_reg[6] 
       (.C(clk_out2),
        .CE(twi_controller_n_5),
        .D(p_1_in__0[6]),
        .Q(\initA_reg_n_0_[6] ),
        .R(SR));
  FDRE initEn_reg
       (.C(clk_out2),
        .CE(1'b1),
        .D(twi_controller_n_6),
        .Q(initEn),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair75" *) 
  LUT3 #(
    .INIT(8'hA8)) 
    \initWord[0]_i_1 
       (.I0(\initA_reg_n_0_[5] ),
        .I1(\initA_reg_n_0_[1] ),
        .I2(\initA_reg_n_0_[0] ),
        .O(\initWord[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h000600010C000A00)) 
    \initWord[10]_i_1 
       (.I0(\initA_reg_n_0_[1] ),
        .I1(\initA_reg_n_0_[0] ),
        .I2(\initA_reg_n_0_[5] ),
        .I3(\initA_reg_n_0_[4] ),
        .I4(\initA_reg_n_0_[2] ),
        .I5(\initA_reg_n_0_[3] ),
        .O(\initWord[10]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair70" *) 
  LUT5 #(
    .INIT(32'h11110010)) 
    \initWord[11]_i_1 
       (.I0(\initA_reg_n_0_[1] ),
        .I1(\initA_reg_n_0_[0] ),
        .I2(\initA_reg_n_0_[3] ),
        .I3(\initA_reg_n_0_[4] ),
        .I4(\initA_reg_n_0_[5] ),
        .O(\initWord[11]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000006)) 
    \initWord[12]_i_1 
       (.I0(\initA_reg_n_0_[2] ),
        .I1(\initA_reg_n_0_[3] ),
        .I2(\initA_reg_n_0_[0] ),
        .I3(\initA_reg_n_0_[1] ),
        .I4(\initA_reg_n_0_[5] ),
        .I5(\initA_reg_n_0_[4] ),
        .O(\initWord[12]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAABAAEEE10000010)) 
    \initWord[13]_i_1 
       (.I0(\initA_reg_n_0_[5] ),
        .I1(\initA_reg_n_0_[4] ),
        .I2(\initA_reg_n_0_[3] ),
        .I3(\initA_reg_n_0_[0] ),
        .I4(\initA_reg_n_0_[2] ),
        .I5(\initA_reg_n_0_[1] ),
        .O(\initWord[13]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000010180884)) 
    \initWord[14]_i_1 
       (.I0(\initA_reg_n_0_[2] ),
        .I1(\initA_reg_n_0_[3] ),
        .I2(\initA_reg_n_0_[1] ),
        .I3(\initA_reg_n_0_[0] ),
        .I4(\initA_reg_n_0_[4] ),
        .I5(\initA_reg_n_0_[5] ),
        .O(\initWord[14]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0010001800090000)) 
    \initWord[15]_i_1 
       (.I0(\initA_reg_n_0_[2] ),
        .I1(\initA_reg_n_0_[3] ),
        .I2(\initA_reg_n_0_[4] ),
        .I3(\initA_reg_n_0_[5] ),
        .I4(\initA_reg_n_0_[0] ),
        .I5(\initA_reg_n_0_[1] ),
        .O(\initWord[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h000078A70000A407)) 
    \initWord[16]_i_1 
       (.I0(\initA_reg_n_0_[3] ),
        .I1(\initA_reg_n_0_[1] ),
        .I2(\initA_reg_n_0_[0] ),
        .I3(\initA_reg_n_0_[4] ),
        .I4(\initA_reg_n_0_[5] ),
        .I5(\initA_reg_n_0_[2] ),
        .O(\initWord[16]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h3654371037102344)) 
    \initWord[17]_i_1 
       (.I0(\initA_reg_n_0_[5] ),
        .I1(\initA_reg_n_0_[1] ),
        .I2(\initA_reg_n_0_[4] ),
        .I3(\initA_reg_n_0_[0] ),
        .I4(\initA_reg_n_0_[3] ),
        .I5(\initA_reg_n_0_[2] ),
        .O(\initWord[17]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hDD9CC8C8D8D8C98D)) 
    \initWord[18]_i_1 
       (.I0(\initA_reg_n_0_[5] ),
        .I1(\initA_reg_n_0_[1] ),
        .I2(\initA_reg_n_0_[4] ),
        .I3(\initA_reg_n_0_[0] ),
        .I4(\initA_reg_n_0_[3] ),
        .I5(\initA_reg_n_0_[2] ),
        .O(\initWord[18]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h98B9B99A98B9BB88)) 
    \initWord[19]_i_1 
       (.I0(\initA_reg_n_0_[1] ),
        .I1(\initA_reg_n_0_[5] ),
        .I2(\initA_reg_n_0_[4] ),
        .I3(\initA_reg_n_0_[2] ),
        .I4(\initA_reg_n_0_[3] ),
        .I5(\initA_reg_n_0_[0] ),
        .O(\initWord[19]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h2221110122210113)) 
    \initWord[20]_i_1 
       (.I0(\initA_reg_n_0_[4] ),
        .I1(\initA_reg_n_0_[5] ),
        .I2(\initA_reg_n_0_[2] ),
        .I3(\initA_reg_n_0_[1] ),
        .I4(\initA_reg_n_0_[3] ),
        .I5(\initA_reg_n_0_[0] ),
        .O(\initWord[20]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h00000000C00EFEF8)) 
    \initWord[21]_i_1 
       (.I0(\initA_reg_n_0_[0] ),
        .I1(\initA_reg_n_0_[1] ),
        .I2(\initA_reg_n_0_[3] ),
        .I3(\initA_reg_n_0_[2] ),
        .I4(\initA_reg_n_0_[4] ),
        .I5(\initA_reg_n_0_[5] ),
        .O(\initWord[21]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0C00020200030000)) 
    \initWord[23]_i_1 
       (.I0(\initA_reg_n_0_[0] ),
        .I1(\initA_reg_n_0_[4] ),
        .I2(\initA_reg_n_0_[5] ),
        .I3(\initA_reg_n_0_[2] ),
        .I4(\initA_reg_n_0_[3] ),
        .I5(\initA_reg_n_0_[1] ),
        .O(\initWord[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0055005500550057)) 
    \initWord[30]_i_1 
       (.I0(\initA_reg_n_0_[5] ),
        .I1(\initA_reg_n_0_[2] ),
        .I2(\initA_reg_n_0_[3] ),
        .I3(\initA_reg_n_0_[6] ),
        .I4(\initA_reg_n_0_[4] ),
        .I5(\initWord[30]_i_3_n_0 ),
        .O(\initWord[30]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair75" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \initWord[30]_i_2 
       (.I0(\initA_reg_n_0_[1] ),
        .I1(\initA_reg_n_0_[5] ),
        .O(\initWord[30]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair74" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \initWord[30]_i_3 
       (.I0(\initA_reg_n_0_[0] ),
        .I1(\initA_reg_n_0_[1] ),
        .O(\initWord[30]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair70" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \initWord[6]_i_1 
       (.I0(\initA_reg_n_0_[1] ),
        .I1(\initA_reg_n_0_[5] ),
        .I2(\initA_reg_n_0_[0] ),
        .O(\initWord[6]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFF74BC00FF3CFF)) 
    \initWord[8]_i_1 
       (.I0(\initA_reg_n_0_[4] ),
        .I1(\initA_reg_n_0_[3] ),
        .I2(\initA_reg_n_0_[2] ),
        .I3(\initA_reg_n_0_[0] ),
        .I4(\initA_reg_n_0_[5] ),
        .I5(\initA_reg_n_0_[1] ),
        .O(\initWord[8]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h000003D4000030BC)) 
    \initWord[9]_i_1 
       (.I0(\initA_reg_n_0_[0] ),
        .I1(\initA_reg_n_0_[2] ),
        .I2(\initA_reg_n_0_[3] ),
        .I3(\initA_reg_n_0_[4] ),
        .I4(\initA_reg_n_0_[5] ),
        .I5(\initA_reg_n_0_[1] ),
        .O(\initWord[9]_i_1_n_0 ));
  FDRE \initWord_reg[0] 
       (.C(clk_out2),
        .CE(\initWord[30]_i_1_n_0 ),
        .D(\initWord[0]_i_1_n_0 ),
        .Q(\initWord_reg_n_0_[0] ),
        .R(1'b0));
  FDRE \initWord_reg[10] 
       (.C(clk_out2),
        .CE(\initWord[30]_i_1_n_0 ),
        .D(\initWord[10]_i_1_n_0 ),
        .Q(data2[2]),
        .R(1'b0));
  FDRE \initWord_reg[11] 
       (.C(clk_out2),
        .CE(\initWord[30]_i_1_n_0 ),
        .D(\initWord[11]_i_1_n_0 ),
        .Q(data2[3]),
        .R(1'b0));
  FDRE \initWord_reg[12] 
       (.C(clk_out2),
        .CE(\initWord[30]_i_1_n_0 ),
        .D(\initWord[12]_i_1_n_0 ),
        .Q(data2[4]),
        .R(1'b0));
  FDRE \initWord_reg[13] 
       (.C(clk_out2),
        .CE(\initWord[30]_i_1_n_0 ),
        .D(\initWord[13]_i_1_n_0 ),
        .Q(data2[5]),
        .R(1'b0));
  FDRE \initWord_reg[14] 
       (.C(clk_out2),
        .CE(\initWord[30]_i_1_n_0 ),
        .D(\initWord[14]_i_1_n_0 ),
        .Q(data2[6]),
        .R(1'b0));
  FDRE \initWord_reg[15] 
       (.C(clk_out2),
        .CE(\initWord[30]_i_1_n_0 ),
        .D(\initWord[15]_i_1_n_0 ),
        .Q(data2[7]),
        .R(1'b0));
  FDRE \initWord_reg[16] 
       (.C(clk_out2),
        .CE(\initWord[30]_i_1_n_0 ),
        .D(\initWord[16]_i_1_n_0 ),
        .Q(data1[0]),
        .R(1'b0));
  FDRE \initWord_reg[17] 
       (.C(clk_out2),
        .CE(\initWord[30]_i_1_n_0 ),
        .D(\initWord[17]_i_1_n_0 ),
        .Q(data1[1]),
        .R(1'b0));
  FDRE \initWord_reg[18] 
       (.C(clk_out2),
        .CE(\initWord[30]_i_1_n_0 ),
        .D(\initWord[18]_i_1_n_0 ),
        .Q(data1[2]),
        .R(1'b0));
  FDRE \initWord_reg[19] 
       (.C(clk_out2),
        .CE(\initWord[30]_i_1_n_0 ),
        .D(\initWord[19]_i_1_n_0 ),
        .Q(data1[3]),
        .R(1'b0));
  FDRE \initWord_reg[20] 
       (.C(clk_out2),
        .CE(\initWord[30]_i_1_n_0 ),
        .D(\initWord[20]_i_1_n_0 ),
        .Q(data1[4]),
        .R(1'b0));
  FDRE \initWord_reg[21] 
       (.C(clk_out2),
        .CE(\initWord[30]_i_1_n_0 ),
        .D(\initWord[21]_i_1_n_0 ),
        .Q(data1[5]),
        .R(1'b0));
  FDRE \initWord_reg[23] 
       (.C(clk_out2),
        .CE(\initWord[30]_i_1_n_0 ),
        .D(\initWord[23]_i_1_n_0 ),
        .Q(data1[7]),
        .R(1'b0));
  FDRE \initWord_reg[30] 
       (.C(clk_out2),
        .CE(\initWord[30]_i_1_n_0 ),
        .D(\initWord[30]_i_2_n_0 ),
        .Q(data0),
        .R(1'b0));
  FDRE \initWord_reg[6] 
       (.C(clk_out2),
        .CE(\initWord[30]_i_1_n_0 ),
        .D(\initWord[6]_i_1_n_0 ),
        .Q(\initWord_reg_n_0_[6] ),
        .R(1'b0));
  FDRE \initWord_reg[8] 
       (.C(clk_out2),
        .CE(\initWord[30]_i_1_n_0 ),
        .D(\initWord[8]_i_1_n_0 ),
        .Q(data2[0]),
        .R(1'b0));
  FDRE \initWord_reg[9] 
       (.C(clk_out2),
        .CE(\initWord[30]_i_1_n_0 ),
        .D(\initWord[9]_i_1_n_0 ),
        .Q(data2[1]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair72" *) 
  LUT4 #(
    .INIT(16'h0001)) 
    msg_i_1
       (.I0(\state_reg_n_0_[3] ),
        .I1(\state_reg_n_0_[2] ),
        .I2(\state_reg_n_0_[0] ),
        .I3(\state_reg_n_0_[1] ),
        .O(msg0));
  FDRE msg_reg
       (.C(clk_out2),
        .CE(s00_axi_aresetn),
        .D(msg0),
        .Q(msg),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFFEFFFFFFFFFFF)) 
    \state[1]_i_3 
       (.I0(\initA_reg_n_0_[4] ),
        .I1(\initA_reg_n_0_[6] ),
        .I2(\state[1]_i_4_n_0 ),
        .I3(\initA_reg_n_0_[5] ),
        .I4(\initA_reg_n_0_[0] ),
        .I5(\initA_reg_n_0_[1] ),
        .O(\state[1]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \state[1]_i_4 
       (.I0(\initA_reg_n_0_[3] ),
        .I1(\initA_reg_n_0_[2] ),
        .O(\state[1]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'h0EFF)) 
    \state[2]_i_2 
       (.I0(\initWord_reg_n_0_[6] ),
        .I1(\initWord_reg_n_0_[0] ),
        .I2(\state_reg_n_0_[0] ),
        .I3(\state_reg_n_0_[1] ),
        .O(\state[2]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \state[3]_i_10 
       (.I0(delaycnt[19]),
        .I1(delaycnt[18]),
        .I2(delaycnt[16]),
        .I3(delaycnt[17]),
        .O(\state[3]_i_10_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \state[3]_i_11 
       (.I0(delaycnt[27]),
        .I1(delaycnt[26]),
        .I2(delaycnt[25]),
        .I3(delaycnt[24]),
        .O(\state[3]_i_11_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair73" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    \state[3]_i_12 
       (.I0(delaycnt[3]),
        .I1(delaycnt[2]),
        .I2(delaycnt[1]),
        .I3(delaycnt[0]),
        .O(\state[3]_i_12_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \state[3]_i_13 
       (.I0(delaycnt[9]),
        .I1(delaycnt[8]),
        .I2(delaycnt[10]),
        .I3(delaycnt[11]),
        .O(\state[3]_i_13_n_0 ));
  LUT4 #(
    .INIT(16'h0004)) 
    \state[3]_i_3 
       (.I0(\state[3]_i_5_n_0 ),
        .I1(\state[3]_i_6_n_0 ),
        .I2(\state[3]_i_7_n_0 ),
        .I3(\state[3]_i_8_n_0 ),
        .O(\state[3]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \state[3]_i_4 
       (.I0(data1[5]),
        .I1(data1[4]),
        .I2(data1[3]),
        .I3(\state[3]_i_9_n_0 ),
        .O(\state[3]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \state[3]_i_5 
       (.I0(delaycnt[20]),
        .I1(delaycnt[22]),
        .I2(delaycnt[21]),
        .I3(delaycnt[23]),
        .I4(\state[3]_i_10_n_0 ),
        .O(\state[3]_i_5_n_0 ));
  LUT5 #(
    .INIT(32'h00000001)) 
    \state[3]_i_6 
       (.I0(delaycnt[29]),
        .I1(delaycnt[28]),
        .I2(delaycnt[30]),
        .I3(delaycnt[31]),
        .I4(\state[3]_i_11_n_0 ),
        .O(\state[3]_i_6_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \state[3]_i_7 
       (.I0(delaycnt[5]),
        .I1(delaycnt[6]),
        .I2(delaycnt[4]),
        .I3(delaycnt[7]),
        .I4(\state[3]_i_12_n_0 ),
        .O(\state[3]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \state[3]_i_8 
       (.I0(delaycnt[14]),
        .I1(delaycnt[15]),
        .I2(delaycnt[12]),
        .I3(delaycnt[13]),
        .I4(\state[3]_i_13_n_0 ),
        .O(\state[3]_i_8_n_0 ));
  LUT4 #(
    .INIT(16'hFFFD)) 
    \state[3]_i_9 
       (.I0(data1[1]),
        .I1(data1[0]),
        .I2(data1[7]),
        .I3(data1[2]),
        .O(\state[3]_i_9_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \state_reg[0] 
       (.C(clk_out2),
        .CE(twi_controller_n_4),
        .D(twi_controller_n_3),
        .Q(\state_reg_n_0_[0] ),
        .R(SR));
  FDSE #(
    .INIT(1'b1)) 
    \state_reg[1] 
       (.C(clk_out2),
        .CE(twi_controller_n_4),
        .D(twi_controller_n_2),
        .Q(\state_reg_n_0_[1] ),
        .S(SR));
  FDSE #(
    .INIT(1'b1)) 
    \state_reg[2] 
       (.C(clk_out2),
        .CE(twi_controller_n_4),
        .D(twi_controller_n_1),
        .Q(\state_reg_n_0_[2] ),
        .S(SR));
  FDRE #(
    .INIT(1'b0)) 
    \state_reg[3] 
       (.C(clk_out2),
        .CE(twi_controller_n_4),
        .D(twi_controller_n_0),
        .Q(\state_reg_n_0_[3] ),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair72" *) 
  LUT4 #(
    .INIT(16'h8055)) 
    stb_i_1
       (.I0(\state_reg_n_0_[2] ),
        .I1(\state_reg_n_0_[0] ),
        .I2(\state_reg_n_0_[1] ),
        .I3(\state_reg_n_0_[3] ),
        .O(stb_i_1_n_0));
  FDRE stb_reg
       (.C(clk_out2),
        .CE(s00_axi_aresetn),
        .D(stb_i_1_n_0),
        .Q(stb),
        .R(1'b0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_TWICtl twi_controller
       (.D({twi_controller_n_0,twi_controller_n_1,twi_controller_n_2,twi_controller_n_3}),
        .E(twi_controller_n_4),
        .Q({\state_reg_n_0_[3] ,\state_reg_n_0_[2] ,\state_reg_n_0_[1] ,\state_reg_n_0_[0] }),
        .clk_out2(clk_out2),
        .data_i(data_i),
        .\delaycnt_reg[20] (\state[3]_i_3_n_0 ),
        .\initA_reg[4] (\state[1]_i_3_n_0 ),
        .\initA_reg[6] (twi_controller_n_5),
        .initEn(initEn),
        .initEn_reg(twi_controller_n_6),
        .initEn_reg_0(\initA[6]_i_3_n_0 ),
        .\initWord_reg[21] (\state[3]_i_4_n_0 ),
        .\initWord_reg[6] (\state[2]_i_2_n_0 ),
        .msg(msg),
        .s00_axi_aresetn(s00_axi_aresetn),
        .scl(scl),
        .sda(sda),
        .stb(stb));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_0
   (clk_out1,
    clk_out2,
    clk_out3,
    resetn,
    clk_in1,
    lopt);
  output clk_out1;
  output clk_out2;
  output clk_out3;
  input resetn;
  input clk_in1;
  output lopt;

  wire clk_in1;
  wire clk_out1;
  wire clk_out2;
  wire clk_out3;
  wire lopt;
  wire resetn;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_0_clk_wiz inst
       (.clk_in1(clk_in1),
        .clk_out1(clk_out1),
        .clk_out2(clk_out2),
        .clk_out3(clk_out3),
        .lopt(lopt),
        .resetn(resetn));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_0_clk_wiz
   (clk_out1,
    clk_out2,
    clk_out3,
    resetn,
    clk_in1,
    lopt);
  output clk_out1;
  output clk_out2;
  output clk_out3;
  input resetn;
  input clk_in1;
  output lopt;

  wire clk_in1;
  wire clk_in1_clk_wiz_0;
  wire clk_out1;
  wire clk_out1_clk_wiz_0;
  wire clk_out2;
  wire clk_out2_clk_wiz_0;
  wire clk_out3;
  wire clk_out3_clk_wiz_0;
  wire clkfbout_buf_clk_wiz_0;
  wire clkfbout_clk_wiz_0;
  wire reset_high;
  wire resetn;
  wire NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT2_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT3_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED;
  wire NLW_mmcm_adv_inst_DRDY_UNCONNECTED;
  wire NLW_mmcm_adv_inst_LOCKED_UNCONNECTED;
  wire NLW_mmcm_adv_inst_PSDONE_UNCONNECTED;
  wire [15:0]NLW_mmcm_adv_inst_DO_UNCONNECTED;

  assign lopt = clk_in1_clk_wiz_0;
  (* box_type = "PRIMITIVE" *) 
  BUFG clkf_buf
       (.I(clkfbout_clk_wiz_0),
        .O(clkfbout_buf_clk_wiz_0));
  (* CAPACITANCE = "DONT_CARE" *) 
  (* IBUF_DELAY_VALUE = "0" *) 
  (* IFD_DELAY_VALUE = "AUTO" *) 
  (* box_type = "PRIMITIVE" *) 
  IBUF #(
    .IOSTANDARD("DEFAULT")) 
    clkin1_ibufg
       (.I(clk_in1),
        .O(clk_in1_clk_wiz_0));
  (* box_type = "PRIMITIVE" *) 
  BUFG clkout1_buf
       (.I(clk_out1_clk_wiz_0),
        .O(clk_out1));
  (* box_type = "PRIMITIVE" *) 
  BUFG clkout2_buf
       (.I(clk_out2_clk_wiz_0),
        .O(clk_out2));
  (* box_type = "PRIMITIVE" *) 
  BUFG clkout3_buf
       (.I(clk_out3_clk_wiz_0),
        .O(clk_out3));
  (* box_type = "PRIMITIVE" *) 
  MMCME2_ADV #(
    .BANDWIDTH("OPTIMIZED"),
    .CLKFBOUT_MULT_F(10.000000),
    .CLKFBOUT_PHASE(0.000000),
    .CLKFBOUT_USE_FINE_PS("FALSE"),
    .CLKIN1_PERIOD(10.000000),
    .CLKIN2_PERIOD(0.000000),
    .CLKOUT0_DIVIDE_F(40.000000),
    .CLKOUT0_DUTY_CYCLE(0.500000),
    .CLKOUT0_PHASE(0.000000),
    .CLKOUT0_USE_FINE_PS("FALSE"),
    .CLKOUT1_DIVIDE(8),
    .CLKOUT1_DUTY_CYCLE(0.500000),
    .CLKOUT1_PHASE(0.000000),
    .CLKOUT1_USE_FINE_PS("FALSE"),
    .CLKOUT2_DIVIDE(1),
    .CLKOUT2_DUTY_CYCLE(0.500000),
    .CLKOUT2_PHASE(0.000000),
    .CLKOUT2_USE_FINE_PS("FALSE"),
    .CLKOUT3_DIVIDE(1),
    .CLKOUT3_DUTY_CYCLE(0.500000),
    .CLKOUT3_PHASE(0.000000),
    .CLKOUT3_USE_FINE_PS("FALSE"),
    .CLKOUT4_CASCADE("FALSE"),
    .CLKOUT4_DIVIDE(1),
    .CLKOUT4_DUTY_CYCLE(0.500000),
    .CLKOUT4_PHASE(0.000000),
    .CLKOUT4_USE_FINE_PS("FALSE"),
    .CLKOUT5_DIVIDE(1),
    .CLKOUT5_DUTY_CYCLE(0.500000),
    .CLKOUT5_PHASE(0.000000),
    .CLKOUT5_USE_FINE_PS("FALSE"),
    .CLKOUT6_DIVIDE(1),
    .CLKOUT6_DUTY_CYCLE(0.500000),
    .CLKOUT6_PHASE(0.000000),
    .CLKOUT6_USE_FINE_PS("FALSE"),
    .COMPENSATION("ZHOLD"),
    .DIVCLK_DIVIDE(1),
    .IS_CLKINSEL_INVERTED(1'b0),
    .IS_PSEN_INVERTED(1'b0),
    .IS_PSINCDEC_INVERTED(1'b0),
    .IS_PWRDWN_INVERTED(1'b0),
    .IS_RST_INVERTED(1'b0),
    .REF_JITTER1(0.010000),
    .REF_JITTER2(0.010000),
    .SS_EN("FALSE"),
    .SS_MODE("CENTER_HIGH"),
    .SS_MOD_PERIOD(10000),
    .STARTUP_WAIT("FALSE")) 
    mmcm_adv_inst
       (.CLKFBIN(clkfbout_buf_clk_wiz_0),
        .CLKFBOUT(clkfbout_clk_wiz_0),
        .CLKFBOUTB(NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED),
        .CLKFBSTOPPED(NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED),
        .CLKIN1(clk_in1_clk_wiz_0),
        .CLKIN2(1'b0),
        .CLKINSEL(1'b1),
        .CLKINSTOPPED(NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED),
        .CLKOUT0(clk_out1_clk_wiz_0),
        .CLKOUT0B(NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED),
        .CLKOUT1(clk_out2_clk_wiz_0),
        .CLKOUT1B(clk_out3_clk_wiz_0),
        .CLKOUT2(NLW_mmcm_adv_inst_CLKOUT2_UNCONNECTED),
        .CLKOUT2B(NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED),
        .CLKOUT3(NLW_mmcm_adv_inst_CLKOUT3_UNCONNECTED),
        .CLKOUT3B(NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED),
        .CLKOUT4(NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED),
        .CLKOUT5(NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED),
        .CLKOUT6(NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED),
        .DADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DCLK(1'b0),
        .DEN(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DO(NLW_mmcm_adv_inst_DO_UNCONNECTED[15:0]),
        .DRDY(NLW_mmcm_adv_inst_DRDY_UNCONNECTED),
        .DWE(1'b0),
        .LOCKED(NLW_mmcm_adv_inst_LOCKED_UNCONNECTED),
        .PSCLK(1'b0),
        .PSDONE(NLW_mmcm_adv_inst_PSDONE_UNCONNECTED),
        .PSEN(1'b0),
        .PSINCDEC(1'b0),
        .PWRDWN(1'b0),
        .RST(reset_high));
  LUT1 #(
    .INIT(2'h1)) 
    mmcm_adv_inst_i_1
       (.I0(resetn),
        .O(reset_high));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_1
   (clk_out1,
    clk_out2,
    resetn,
    clk_in1,
    lopt);
  output clk_out1;
  output clk_out2;
  input resetn;
  input clk_in1;
  input lopt;

  wire clk_in1;
  wire clk_out1;
  wire clk_out2;
  wire lopt;
  wire resetn;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_1_clk_wiz inst
       (.clk_in1(clk_in1),
        .clk_out1(clk_out1),
        .clk_out2(clk_out2),
        .lopt(lopt),
        .resetn(resetn));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_1_clk_wiz
   (clk_out1,
    clk_out2,
    resetn,
    clk_in1,
    lopt);
  output clk_out1;
  output clk_out2;
  input resetn;
  input clk_in1;
  input lopt;

  wire clk_in1;
  wire clk_out1;
  wire clk_out1_clk_wiz_1;
  wire clk_out2;
  wire clk_out2_clk_wiz_1;
  wire clkfbout_buf_clk_wiz_1;
  wire clkfbout_clk_wiz_1;
  wire lopt;
  wire reset_high;
  wire resetn;
  wire NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT1B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT2_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT3_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED;
  wire NLW_mmcm_adv_inst_DRDY_UNCONNECTED;
  wire NLW_mmcm_adv_inst_LOCKED_UNCONNECTED;
  wire NLW_mmcm_adv_inst_PSDONE_UNCONNECTED;
  wire [15:0]NLW_mmcm_adv_inst_DO_UNCONNECTED;

  (* box_type = "PRIMITIVE" *) 
  BUFG clkf_buf
       (.I(clkfbout_clk_wiz_1),
        .O(clkfbout_buf_clk_wiz_1));
  (* box_type = "PRIMITIVE" *) 
  BUFG clkout1_buf
       (.I(clk_out1_clk_wiz_1),
        .O(clk_out1));
  (* box_type = "PRIMITIVE" *) 
  BUFG clkout2_buf
       (.I(clk_out2_clk_wiz_1),
        .O(clk_out2));
  (* box_type = "PRIMITIVE" *) 
  MMCME2_ADV #(
    .BANDWIDTH("OPTIMIZED"),
    .CLKFBOUT_MULT_F(10.000000),
    .CLKFBOUT_PHASE(0.000000),
    .CLKFBOUT_USE_FINE_PS("FALSE"),
    .CLKIN1_PERIOD(10.000000),
    .CLKIN2_PERIOD(0.000000),
    .CLKOUT0_DIVIDE_F(81.375000),
    .CLKOUT0_DUTY_CYCLE(0.500000),
    .CLKOUT0_PHASE(0.000000),
    .CLKOUT0_USE_FINE_PS("FALSE"),
    .CLKOUT1_DIVIDE(20),
    .CLKOUT1_DUTY_CYCLE(0.500000),
    .CLKOUT1_PHASE(0.000000),
    .CLKOUT1_USE_FINE_PS("FALSE"),
    .CLKOUT2_DIVIDE(1),
    .CLKOUT2_DUTY_CYCLE(0.500000),
    .CLKOUT2_PHASE(0.000000),
    .CLKOUT2_USE_FINE_PS("FALSE"),
    .CLKOUT3_DIVIDE(1),
    .CLKOUT3_DUTY_CYCLE(0.500000),
    .CLKOUT3_PHASE(0.000000),
    .CLKOUT3_USE_FINE_PS("FALSE"),
    .CLKOUT4_CASCADE("FALSE"),
    .CLKOUT4_DIVIDE(1),
    .CLKOUT4_DUTY_CYCLE(0.500000),
    .CLKOUT4_PHASE(0.000000),
    .CLKOUT4_USE_FINE_PS("FALSE"),
    .CLKOUT5_DIVIDE(1),
    .CLKOUT5_DUTY_CYCLE(0.500000),
    .CLKOUT5_PHASE(0.000000),
    .CLKOUT5_USE_FINE_PS("FALSE"),
    .CLKOUT6_DIVIDE(1),
    .CLKOUT6_DUTY_CYCLE(0.500000),
    .CLKOUT6_PHASE(0.000000),
    .CLKOUT6_USE_FINE_PS("FALSE"),
    .COMPENSATION("ZHOLD"),
    .DIVCLK_DIVIDE(1),
    .IS_CLKINSEL_INVERTED(1'b0),
    .IS_PSEN_INVERTED(1'b0),
    .IS_PSINCDEC_INVERTED(1'b0),
    .IS_PWRDWN_INVERTED(1'b0),
    .IS_RST_INVERTED(1'b0),
    .REF_JITTER1(0.010000),
    .REF_JITTER2(0.010000),
    .SS_EN("FALSE"),
    .SS_MODE("CENTER_HIGH"),
    .SS_MOD_PERIOD(10000),
    .STARTUP_WAIT("FALSE")) 
    mmcm_adv_inst
       (.CLKFBIN(clkfbout_buf_clk_wiz_1),
        .CLKFBOUT(clkfbout_clk_wiz_1),
        .CLKFBOUTB(NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED),
        .CLKFBSTOPPED(NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED),
        .CLKIN1(lopt),
        .CLKIN2(1'b0),
        .CLKINSEL(1'b1),
        .CLKINSTOPPED(NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED),
        .CLKOUT0(clk_out1_clk_wiz_1),
        .CLKOUT0B(NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED),
        .CLKOUT1(clk_out2_clk_wiz_1),
        .CLKOUT1B(NLW_mmcm_adv_inst_CLKOUT1B_UNCONNECTED),
        .CLKOUT2(NLW_mmcm_adv_inst_CLKOUT2_UNCONNECTED),
        .CLKOUT2B(NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED),
        .CLKOUT3(NLW_mmcm_adv_inst_CLKOUT3_UNCONNECTED),
        .CLKOUT3B(NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED),
        .CLKOUT4(NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED),
        .CLKOUT5(NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED),
        .CLKOUT6(NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED),
        .DADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DCLK(1'b0),
        .DEN(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DO(NLW_mmcm_adv_inst_DO_UNCONNECTED[15:0]),
        .DRDY(NLW_mmcm_adv_inst_DRDY_UNCONNECTED),
        .DWE(1'b0),
        .LOCKED(NLW_mmcm_adv_inst_LOCKED_UNCONNECTED),
        .PSCLK(1'b0),
        .PSDONE(NLW_mmcm_adv_inst_PSDONE_UNCONNECTED),
        .PSEN(1'b0),
        .PSINCDEC(1'b0),
        .PWRDWN(1'b0),
        .RST(reset_high));
  LUT1 #(
    .INIT(2'h1)) 
    mmcm_adv_inst_i_1
       (.I0(resetn),
        .O(reset_high));
endmodule

(* CHECK_LICENSE_TYPE = "design_1_my_lab3_ip_0_1,my_lab3_ip_v3_0,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "my_lab3_ip_v3_0,Vivado 2018.2" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (ac_mclk,
    ac_adc_sdata,
    ac_dac_sdata,
    ac_bclk,
    ac_lrclk,
    scl,
    sda,
    tmds,
    tmdsb,
    switch,
    btn,
    ready,
    s00_axi_awaddr,
    s00_axi_awprot,
    s00_axi_awvalid,
    s00_axi_awready,
    s00_axi_wdata,
    s00_axi_wstrb,
    s00_axi_wvalid,
    s00_axi_wready,
    s00_axi_bresp,
    s00_axi_bvalid,
    s00_axi_bready,
    s00_axi_araddr,
    s00_axi_arprot,
    s00_axi_arvalid,
    s00_axi_arready,
    s00_axi_rdata,
    s00_axi_rresp,
    s00_axi_rvalid,
    s00_axi_rready,
    s00_axi_aclk,
    s00_axi_aresetn);
  output ac_mclk;
  input ac_adc_sdata;
  output ac_dac_sdata;
  output ac_bclk;
  output ac_lrclk;
  inout scl;
  inout sda;
  output [3:0]tmds;
  output [3:0]tmdsb;
  input [4:0]switch;
  input [4:0]btn;
  output ready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWADDR" *) (* x_interface_parameter = "XIL_INTERFACENAME S00_AXI, WIZ_DATA_WIDTH 32, WIZ_NUM_REG 32, SUPPORTS_NARROW_BURST 0, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 100000000, ID_WIDTH 0, ADDR_WIDTH 7, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, NUM_READ_OUTSTANDING 2, NUM_WRITE_OUTSTANDING 2, MAX_BURST_LENGTH 1, PHASE 0.0, CLK_DOMAIN /clk_wiz_1_clk_out1, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0" *) input [6:0]s00_axi_awaddr;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWPROT" *) input [2:0]s00_axi_awprot;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWVALID" *) input s00_axi_awvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWREADY" *) output s00_axi_awready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WDATA" *) input [31:0]s00_axi_wdata;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WSTRB" *) input [3:0]s00_axi_wstrb;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WVALID" *) input s00_axi_wvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WREADY" *) output s00_axi_wready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI BRESP" *) output [1:0]s00_axi_bresp;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI BVALID" *) output s00_axi_bvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI BREADY" *) input s00_axi_bready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARADDR" *) input [6:0]s00_axi_araddr;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARPROT" *) input [2:0]s00_axi_arprot;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARVALID" *) input s00_axi_arvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARREADY" *) output s00_axi_arready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RDATA" *) output [31:0]s00_axi_rdata;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RRESP" *) output [1:0]s00_axi_rresp;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RVALID" *) output s00_axi_rvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RREADY" *) input s00_axi_rready;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 S00_AXI_CLK CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME S00_AXI_CLK, ASSOCIATED_BUSIF S00_AXI, ASSOCIATED_RESET s00_axi_aresetn, FREQ_HZ 100000000, PHASE 0.0, CLK_DOMAIN /clk_wiz_1_clk_out1" *) input s00_axi_aclk;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 S00_AXI_RST RST" *) (* x_interface_parameter = "XIL_INTERFACENAME S00_AXI_RST, POLARITY ACTIVE_LOW" *) input s00_axi_aresetn;

  wire \<const0> ;
  wire ac_adc_sdata;
  wire ac_bclk;
  wire ac_dac_sdata;
  wire ac_lrclk;
  wire ac_mclk;
  wire [4:0]btn;
  (* IBUF_LOW_PWR *) wire s00_axi_aclk;
  wire [6:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arready;
  wire s00_axi_arvalid;
  wire [6:0]s00_axi_awaddr;
  wire s00_axi_awready;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire s00_axi_wready;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;
  wire scl;
  wire sda;
  wire [4:0]switch;
  (* SLEW = "SLOW" *) wire [3:0]tmds;
  (* SLEW = "SLOW" *) wire [3:0]tmdsb;

  assign s00_axi_bresp[1] = \<const0> ;
  assign s00_axi_bresp[0] = \<const0> ;
  assign s00_axi_rresp[1] = \<const0> ;
  assign s00_axi_rresp[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_my_lab3_ip_v3_0 U0
       (.S_AXI_ARREADY(s00_axi_arready),
        .S_AXI_AWREADY(s00_axi_awready),
        .S_AXI_WREADY(s00_axi_wready),
        .ac_adc_sdata(ac_adc_sdata),
        .ac_bclk(ac_bclk),
        .ac_dac_sdata(ac_dac_sdata),
        .ac_lrclk(ac_lrclk),
        .ac_mclk(ac_mclk),
        .btn(btn),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_araddr(s00_axi_araddr[6:2]),
        .s00_axi_aresetn(s00_axi_aresetn),
        .s00_axi_arvalid(s00_axi_arvalid),
        .s00_axi_awaddr(s00_axi_awaddr[6:2]),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_bready(s00_axi_bready),
        .s00_axi_bvalid(s00_axi_bvalid),
        .s00_axi_rdata(s00_axi_rdata),
        .s00_axi_rready(s00_axi_rready),
        .s00_axi_rvalid(s00_axi_rvalid),
        .s00_axi_wdata(s00_axi_wdata),
        .s00_axi_wstrb(s00_axi_wstrb),
        .s00_axi_wvalid(s00_axi_wvalid),
        .scl(scl),
        .sda(sda),
        .switch(switch[2:0]),
        .tmds(tmds),
        .tmdsb(tmdsb));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_dvid
   (red_s,
    green_s,
    blue_s,
    clock_s,
    \encoded_reg[8] ,
    Q,
    \dc_bias_reg[0] ,
    \encoded_reg[8]_0 ,
    \encoded_reg[8]_1 ,
    \encoded_reg[8]_2 ,
    \encoded_reg[8]_3 ,
    \encoded_reg[8]_4 ,
    \encoded_reg[8]_5 ,
    \encoded_reg[8]_6 ,
    \encoded_reg[8]_7 ,
    \encoded_reg[9] ,
    \dc_bias_reg[0]_0 ,
    \encoded_reg[8]_8 ,
    \encoded_reg[8]_9 ,
    \encoded_reg[8]_10 ,
    \encoded_reg[8]_11 ,
    \encoded_reg[8]_12 ,
    \encoded_reg[8]_13 ,
    \encoded_reg[8]_14 ,
    \encoded_reg[8]_15 ,
    \encoded_reg[8]_16 ,
    \encoded_reg[8]_17 ,
    clk_out2,
    clk_out3,
    encoded0_in,
    \processCol_reg[6] ,
    CLK,
    \dc_bias_reg[3] ,
    \dc_bias_reg[3]_0 ,
    \dc_bias_reg[1] ,
    \dc_bias_reg[3]_1 ,
    \dc_bias_reg[2] ,
    \dc_bias_reg[3]_2 ,
    \dc_bias_reg[3]_3 ,
    \dc_bias_reg[3]_4 ,
    \trigger_volt_reg[6] ,
    \processCol_reg[6]_0 ,
    \processCol_reg[1] ,
    \trigger_time_reg[7] ,
    \trigger_time_reg[6] ,
    \trigger_time_reg[5] ,
    \trigger_time_reg[3] ,
    \trigger_time_reg[2] ,
    \trigger_time_reg[4] ,
    \processRow_reg[2] ,
    \processCol_reg[0] ,
    SR);
  output red_s;
  output green_s;
  output blue_s;
  output clock_s;
  output \encoded_reg[8] ;
  output [0:0]Q;
  output [0:0]\dc_bias_reg[0] ;
  output \encoded_reg[8]_0 ;
  output \encoded_reg[8]_1 ;
  output \encoded_reg[8]_2 ;
  output \encoded_reg[8]_3 ;
  output \encoded_reg[8]_4 ;
  output \encoded_reg[8]_5 ;
  output \encoded_reg[8]_6 ;
  output \encoded_reg[8]_7 ;
  output \encoded_reg[9] ;
  output [1:0]\dc_bias_reg[0]_0 ;
  output \encoded_reg[8]_8 ;
  output \encoded_reg[8]_9 ;
  output \encoded_reg[8]_10 ;
  output \encoded_reg[8]_11 ;
  output \encoded_reg[8]_12 ;
  output \encoded_reg[8]_13 ;
  output \encoded_reg[8]_14 ;
  output \encoded_reg[8]_15 ;
  output \encoded_reg[8]_16 ;
  output \encoded_reg[8]_17 ;
  input clk_out2;
  input clk_out3;
  input [0:0]encoded0_in;
  input \processCol_reg[6] ;
  input CLK;
  input \dc_bias_reg[3] ;
  input \dc_bias_reg[3]_0 ;
  input \dc_bias_reg[1] ;
  input \dc_bias_reg[3]_1 ;
  input \dc_bias_reg[2] ;
  input \dc_bias_reg[3]_2 ;
  input \dc_bias_reg[3]_3 ;
  input \dc_bias_reg[3]_4 ;
  input [6:0]\trigger_volt_reg[6] ;
  input \processCol_reg[6]_0 ;
  input \processCol_reg[1] ;
  input \trigger_time_reg[7] ;
  input \trigger_time_reg[6] ;
  input [2:0]\trigger_time_reg[5] ;
  input \trigger_time_reg[3] ;
  input \trigger_time_reg[2] ;
  input \trigger_time_reg[4] ;
  input \processRow_reg[2] ;
  input \processCol_reg[0] ;
  input [0:0]SR;

  wire CLK;
  wire D0;
  wire D1;
  wire [0:0]Q;
  wire [0:0]SR;
  wire TDMS_encoder_blue_n_0;
  wire TDMS_encoder_blue_n_1;
  wire TDMS_encoder_blue_n_2;
  wire TDMS_encoder_blue_n_3;
  wire TDMS_encoder_green_n_0;
  wire TDMS_encoder_green_n_1;
  wire TDMS_encoder_green_n_2;
  wire TDMS_encoder_green_n_3;
  wire TDMS_encoder_red_n_0;
  wire TDMS_encoder_red_n_1;
  wire TDMS_encoder_red_n_2;
  wire TDMS_encoder_red_n_3;
  wire blue_s;
  wire clk_out2;
  wire clk_out3;
  wire clock_s;
  wire [7:0]data1;
  wire [0:0]\dc_bias_reg[0] ;
  wire [1:0]\dc_bias_reg[0]_0 ;
  wire \dc_bias_reg[1] ;
  wire \dc_bias_reg[2] ;
  wire \dc_bias_reg[3] ;
  wire \dc_bias_reg[3]_0 ;
  wire \dc_bias_reg[3]_1 ;
  wire \dc_bias_reg[3]_2 ;
  wire \dc_bias_reg[3]_3 ;
  wire \dc_bias_reg[3]_4 ;
  wire [0:0]encoded0_in;
  wire \encoded_reg[8] ;
  wire \encoded_reg[8]_0 ;
  wire \encoded_reg[8]_1 ;
  wire \encoded_reg[8]_10 ;
  wire \encoded_reg[8]_11 ;
  wire \encoded_reg[8]_12 ;
  wire \encoded_reg[8]_13 ;
  wire \encoded_reg[8]_14 ;
  wire \encoded_reg[8]_15 ;
  wire \encoded_reg[8]_16 ;
  wire \encoded_reg[8]_17 ;
  wire \encoded_reg[8]_2 ;
  wire \encoded_reg[8]_3 ;
  wire \encoded_reg[8]_4 ;
  wire \encoded_reg[8]_5 ;
  wire \encoded_reg[8]_6 ;
  wire \encoded_reg[8]_7 ;
  wire \encoded_reg[8]_8 ;
  wire \encoded_reg[8]_9 ;
  wire \encoded_reg[9] ;
  wire green_s;
  wire [9:0]latched_blue;
  wire [9:0]latched_green;
  wire [9:0]latched_red;
  wire \processCol_reg[0] ;
  wire \processCol_reg[1] ;
  wire \processCol_reg[6] ;
  wire \processCol_reg[6]_0 ;
  wire \processRow_reg[2] ;
  wire red_s;
  wire [6:2]shift_blue;
  wire \shift_blue[0]_i_1_n_0 ;
  wire \shift_blue[1]_i_1_n_0 ;
  wire \shift_blue[3]_i_1_n_0 ;
  wire \shift_blue[5]_i_1_n_0 ;
  wire \shift_blue[7]_i_1_n_0 ;
  wire \shift_blue[7]_i_2_n_0 ;
  wire \shift_blue_reg_n_0_[0] ;
  wire \shift_blue_reg_n_0_[1] ;
  wire \shift_blue_reg_n_0_[2] ;
  wire \shift_blue_reg_n_0_[3] ;
  wire \shift_blue_reg_n_0_[4] ;
  wire \shift_blue_reg_n_0_[5] ;
  wire \shift_blue_reg_n_0_[6] ;
  wire \shift_blue_reg_n_0_[7] ;
  wire \shift_blue_reg_n_0_[8] ;
  wire \shift_blue_reg_n_0_[9] ;
  wire [1:0]shift_clock;
  wire [9:2]shift_clock__0;
  wire [6:2]shift_green;
  wire \shift_green[0]_i_1_n_0 ;
  wire \shift_green[1]_i_1_n_0 ;
  wire \shift_green[3]_i_1_n_0 ;
  wire \shift_green[5]_i_1_n_0 ;
  wire \shift_green[7]_i_1_n_0 ;
  wire \shift_green[7]_i_2_n_0 ;
  wire \shift_green_reg_n_0_[0] ;
  wire \shift_green_reg_n_0_[1] ;
  wire \shift_green_reg_n_0_[2] ;
  wire \shift_green_reg_n_0_[3] ;
  wire \shift_green_reg_n_0_[4] ;
  wire \shift_green_reg_n_0_[5] ;
  wire \shift_green_reg_n_0_[6] ;
  wire \shift_green_reg_n_0_[7] ;
  wire \shift_green_reg_n_0_[8] ;
  wire \shift_green_reg_n_0_[9] ;
  wire [6:2]shift_red;
  wire \shift_red[0]_i_1_n_0 ;
  wire \shift_red[1]_i_1_n_0 ;
  wire \shift_red[3]_i_1_n_0 ;
  wire \shift_red[5]_i_1_n_0 ;
  wire \shift_red[7]_i_1_n_0 ;
  wire \shift_red[7]_i_2_n_0 ;
  wire \shift_red[9]_i_1_n_0 ;
  wire \shift_red[9]_i_2_n_0 ;
  wire \trigger_time_reg[2] ;
  wire \trigger_time_reg[3] ;
  wire \trigger_time_reg[4] ;
  wire [2:0]\trigger_time_reg[5] ;
  wire \trigger_time_reg[6] ;
  wire \trigger_time_reg[7] ;
  wire [6:0]\trigger_volt_reg[6] ;
  wire NLW_ODDR2_blue_R_UNCONNECTED;
  wire NLW_ODDR2_blue_S_UNCONNECTED;
  wire NLW_ODDR2_clock_R_UNCONNECTED;
  wire NLW_ODDR2_clock_S_UNCONNECTED;
  wire NLW_ODDR2_green_R_UNCONNECTED;
  wire NLW_ODDR2_green_S_UNCONNECTED;
  wire NLW_ODDR2_red_R_UNCONNECTED;
  wire NLW_ODDR2_red_S_UNCONNECTED;

  (* XILINX_LEGACY_PRIM = "ODDR2" *) 
  (* XILINX_TRANSFORM_PINMAP = "D0:D1 D1:D2 C0:C" *) 
  (* __SRVAL = "TRUE" *) 
  (* box_type = "PRIMITIVE" *) 
  ODDR #(
    .DDR_CLK_EDGE("SAME_EDGE"),
    .INIT(1'b0),
    .SRTYPE("ASYNC")) 
    ODDR2_blue
       (.C(clk_out2),
        .CE(1'b1),
        .D1(\shift_blue_reg_n_0_[0] ),
        .D2(\shift_blue_reg_n_0_[1] ),
        .Q(blue_s),
        .R(NLW_ODDR2_blue_R_UNCONNECTED),
        .S(NLW_ODDR2_blue_S_UNCONNECTED));
  (* XILINX_LEGACY_PRIM = "ODDR2" *) 
  (* XILINX_TRANSFORM_PINMAP = "D0:D1 D1:D2 C0:C" *) 
  (* __SRVAL = "TRUE" *) 
  (* box_type = "PRIMITIVE" *) 
  ODDR #(
    .DDR_CLK_EDGE("SAME_EDGE"),
    .INIT(1'b0),
    .SRTYPE("ASYNC")) 
    ODDR2_clock
       (.C(clk_out2),
        .CE(1'b1),
        .D1(shift_clock[0]),
        .D2(shift_clock[1]),
        .Q(clock_s),
        .R(NLW_ODDR2_clock_R_UNCONNECTED),
        .S(NLW_ODDR2_clock_S_UNCONNECTED));
  (* XILINX_LEGACY_PRIM = "ODDR2" *) 
  (* XILINX_TRANSFORM_PINMAP = "D0:D1 D1:D2 C0:C" *) 
  (* __SRVAL = "TRUE" *) 
  (* box_type = "PRIMITIVE" *) 
  ODDR #(
    .DDR_CLK_EDGE("SAME_EDGE"),
    .INIT(1'b0),
    .SRTYPE("ASYNC")) 
    ODDR2_green
       (.C(clk_out2),
        .CE(1'b1),
        .D1(\shift_green_reg_n_0_[0] ),
        .D2(\shift_green_reg_n_0_[1] ),
        .Q(green_s),
        .R(NLW_ODDR2_green_R_UNCONNECTED),
        .S(NLW_ODDR2_green_S_UNCONNECTED));
  (* XILINX_LEGACY_PRIM = "ODDR2" *) 
  (* XILINX_TRANSFORM_PINMAP = "D0:D1 D1:D2 C0:C" *) 
  (* __SRVAL = "TRUE" *) 
  (* box_type = "PRIMITIVE" *) 
  ODDR #(
    .DDR_CLK_EDGE("SAME_EDGE"),
    .INIT(1'b0),
    .SRTYPE("ASYNC")) 
    ODDR2_red
       (.C(clk_out2),
        .CE(1'b1),
        .D1(D0),
        .D2(D1),
        .Q(red_s),
        .R(NLW_ODDR2_red_R_UNCONNECTED),
        .S(NLW_ODDR2_red_S_UNCONNECTED));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_TDMS_encoder TDMS_encoder_blue
       (.CLK(CLK),
        .D({TDMS_encoder_blue_n_0,TDMS_encoder_blue_n_1,TDMS_encoder_blue_n_2,TDMS_encoder_blue_n_3}),
        .Q(\dc_bias_reg[0]_0 ),
        .SR(SR),
        .\dc_bias_reg[2]_0 (\dc_bias_reg[2] ),
        .\dc_bias_reg[3]_0 (\dc_bias_reg[3]_2 ),
        .\dc_bias_reg[3]_1 (\dc_bias_reg[3]_3 ),
        .\dc_bias_reg[3]_2 (\dc_bias_reg[3]_4 ),
        .\encoded_reg[8]_0 (\encoded_reg[8]_17 ),
        .\encoded_reg[9]_0 (\encoded_reg[9] ),
        .\processCol_reg[6] (\processCol_reg[6]_0 ),
        .\processRow_reg[2] (\processRow_reg[2] ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_TDMS_encoder_0 TDMS_encoder_green
       (.CLK(CLK),
        .D({TDMS_encoder_green_n_0,TDMS_encoder_green_n_1,TDMS_encoder_green_n_2,TDMS_encoder_green_n_3}),
        .Q(Q),
        .SR(SR),
        .\dc_bias_reg[1]_0 (\dc_bias_reg[1] ),
        .\dc_bias_reg[3]_0 (\dc_bias_reg[3]_1 ),
        .\encoded_reg[8]_0 (\encoded_reg[8]_9 ),
        .\processCol_reg[1] (\processCol_reg[1] ),
        .\processCol_reg[6] (\processCol_reg[6]_0 ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_TDMS_encoder_1 TDMS_encoder_red
       (.CLK(CLK),
        .D({TDMS_encoder_red_n_0,TDMS_encoder_red_n_1,TDMS_encoder_red_n_2,TDMS_encoder_red_n_3}),
        .Q(\dc_bias_reg[0] ),
        .SR(SR),
        .\dc_bias_reg[3]_0 (\dc_bias_reg[3] ),
        .\dc_bias_reg[3]_1 (\dc_bias_reg[3]_0 ),
        .encoded0_in(encoded0_in),
        .\encoded_reg[8]_0 (\encoded_reg[8] ),
        .\encoded_reg[8]_1 (\encoded_reg[8]_0 ),
        .\encoded_reg[8]_10 (\encoded_reg[8]_10 ),
        .\encoded_reg[8]_11 (\encoded_reg[8]_11 ),
        .\encoded_reg[8]_12 (\encoded_reg[8]_12 ),
        .\encoded_reg[8]_13 (\encoded_reg[8]_13 ),
        .\encoded_reg[8]_14 (\encoded_reg[8]_14 ),
        .\encoded_reg[8]_15 (\encoded_reg[8]_15 ),
        .\encoded_reg[8]_16 (\encoded_reg[8]_16 ),
        .\encoded_reg[8]_2 (\encoded_reg[8]_1 ),
        .\encoded_reg[8]_3 (\encoded_reg[8]_2 ),
        .\encoded_reg[8]_4 (\encoded_reg[8]_3 ),
        .\encoded_reg[8]_5 (\encoded_reg[8]_4 ),
        .\encoded_reg[8]_6 (\encoded_reg[8]_5 ),
        .\encoded_reg[8]_7 (\encoded_reg[8]_6 ),
        .\encoded_reg[8]_8 (\encoded_reg[8]_7 ),
        .\encoded_reg[8]_9 (\encoded_reg[8]_8 ),
        .\processCol_reg[0] (\processCol_reg[0] ),
        .\processCol_reg[6] (\processCol_reg[6] ),
        .\processCol_reg[6]_0 (\processCol_reg[6]_0 ),
        .\processRow_reg[2] (\processRow_reg[2] ),
        .\trigger_time_reg[2] (\trigger_time_reg[2] ),
        .\trigger_time_reg[3] (\trigger_time_reg[3] ),
        .\trigger_time_reg[4] (\trigger_time_reg[4] ),
        .\trigger_time_reg[5] (\trigger_time_reg[5] ),
        .\trigger_time_reg[6] (\trigger_time_reg[6] ),
        .\trigger_time_reg[7] (\trigger_time_reg[7] ),
        .\trigger_volt_reg[6] (\trigger_volt_reg[6] ));
  FDRE #(
    .INIT(1'b0)) 
    \latched_blue_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(TDMS_encoder_blue_n_3),
        .Q(latched_blue[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_blue_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(TDMS_encoder_blue_n_2),
        .Q(latched_blue[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_blue_reg[8] 
       (.C(CLK),
        .CE(1'b1),
        .D(TDMS_encoder_blue_n_1),
        .Q(latched_blue[8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_blue_reg[9] 
       (.C(CLK),
        .CE(1'b1),
        .D(TDMS_encoder_blue_n_0),
        .Q(latched_blue[9]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_green_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(TDMS_encoder_green_n_3),
        .Q(latched_green[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_green_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(TDMS_encoder_green_n_2),
        .Q(latched_green[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_green_reg[8] 
       (.C(CLK),
        .CE(1'b1),
        .D(TDMS_encoder_green_n_1),
        .Q(latched_green[8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_green_reg[9] 
       (.C(CLK),
        .CE(1'b1),
        .D(TDMS_encoder_green_n_0),
        .Q(latched_green[9]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_red_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(TDMS_encoder_red_n_3),
        .Q(latched_red[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_red_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(TDMS_encoder_red_n_2),
        .Q(latched_red[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_red_reg[8] 
       (.C(CLK),
        .CE(1'b1),
        .D(TDMS_encoder_red_n_1),
        .Q(latched_red[8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_red_reg[9] 
       (.C(CLK),
        .CE(1'b1),
        .D(TDMS_encoder_red_n_0),
        .Q(latched_red[9]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \shift_blue[0]_i_1 
       (.I0(\shift_red[9]_i_1_n_0 ),
        .I1(\shift_blue_reg_n_0_[2] ),
        .O(\shift_blue[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \shift_blue[1]_i_1 
       (.I0(\shift_red[9]_i_1_n_0 ),
        .I1(\shift_blue_reg_n_0_[3] ),
        .O(\shift_blue[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \shift_blue[2]_i_1 
       (.I0(\shift_blue_reg_n_0_[4] ),
        .I1(\shift_red[9]_i_1_n_0 ),
        .I2(latched_blue[2]),
        .O(shift_blue[2]));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \shift_blue[3]_i_1 
       (.I0(\shift_red[9]_i_1_n_0 ),
        .I1(\shift_blue_reg_n_0_[5] ),
        .O(\shift_blue[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \shift_blue[4]_i_1 
       (.I0(\shift_blue_reg_n_0_[6] ),
        .I1(\shift_red[9]_i_1_n_0 ),
        .I2(latched_blue[2]),
        .O(shift_blue[4]));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \shift_blue[5]_i_1 
       (.I0(\shift_red[9]_i_1_n_0 ),
        .I1(\shift_blue_reg_n_0_[7] ),
        .O(\shift_blue[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \shift_blue[6]_i_1 
       (.I0(\shift_blue_reg_n_0_[8] ),
        .I1(\shift_red[9]_i_1_n_0 ),
        .I2(latched_blue[2]),
        .O(shift_blue[6]));
  LUT2 #(
    .INIT(4'h2)) 
    \shift_blue[7]_i_1 
       (.I0(latched_blue[0]),
        .I1(\shift_red[9]_i_1_n_0 ),
        .O(\shift_blue[7]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \shift_blue[7]_i_2 
       (.I0(\shift_red[9]_i_1_n_0 ),
        .I1(\shift_blue_reg_n_0_[9] ),
        .O(\shift_blue[7]_i_2_n_0 ));
  FDSE #(
    .INIT(1'b0)) 
    \shift_blue_reg[0] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(\shift_blue[0]_i_1_n_0 ),
        .Q(\shift_blue_reg_n_0_[0] ),
        .S(\shift_blue[7]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b0)) 
    \shift_blue_reg[1] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(\shift_blue[1]_i_1_n_0 ),
        .Q(\shift_blue_reg_n_0_[1] ),
        .S(\shift_blue[7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \shift_blue_reg[2] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_blue[2]),
        .Q(\shift_blue_reg_n_0_[2] ),
        .R(1'b0));
  FDSE #(
    .INIT(1'b0)) 
    \shift_blue_reg[3] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(\shift_blue[3]_i_1_n_0 ),
        .Q(\shift_blue_reg_n_0_[3] ),
        .S(\shift_blue[7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \shift_blue_reg[4] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_blue[4]),
        .Q(\shift_blue_reg_n_0_[4] ),
        .R(1'b0));
  FDSE #(
    .INIT(1'b0)) 
    \shift_blue_reg[5] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(\shift_blue[5]_i_1_n_0 ),
        .Q(\shift_blue_reg_n_0_[5] ),
        .S(\shift_blue[7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \shift_blue_reg[6] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_blue[6]),
        .Q(\shift_blue_reg_n_0_[6] ),
        .R(1'b0));
  FDSE #(
    .INIT(1'b0)) 
    \shift_blue_reg[7] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(\shift_blue[7]_i_2_n_0 ),
        .Q(\shift_blue_reg_n_0_[7] ),
        .S(\shift_blue[7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \shift_blue_reg[8] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(latched_blue[8]),
        .Q(\shift_blue_reg_n_0_[8] ),
        .R(\shift_red[9]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \shift_blue_reg[9] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(latched_blue[9]),
        .Q(\shift_blue_reg_n_0_[9] ),
        .R(\shift_red[9]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b1)) 
    \shift_clock_reg[0] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_clock__0[2]),
        .Q(shift_clock[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \shift_clock_reg[1] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_clock__0[3]),
        .Q(shift_clock[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \shift_clock_reg[2] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_clock__0[4]),
        .Q(shift_clock__0[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \shift_clock_reg[3] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_clock__0[5]),
        .Q(shift_clock__0[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \shift_clock_reg[4] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_clock__0[6]),
        .Q(shift_clock__0[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \shift_clock_reg[5] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_clock__0[7]),
        .Q(shift_clock__0[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \shift_clock_reg[6] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_clock__0[8]),
        .Q(shift_clock__0[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \shift_clock_reg[7] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_clock__0[9]),
        .Q(shift_clock__0[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \shift_clock_reg[8] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_clock[0]),
        .Q(shift_clock__0[8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \shift_clock_reg[9] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_clock[1]),
        .Q(shift_clock__0[9]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \shift_green[0]_i_1 
       (.I0(\shift_red[9]_i_1_n_0 ),
        .I1(\shift_green_reg_n_0_[2] ),
        .O(\shift_green[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \shift_green[1]_i_1 
       (.I0(\shift_red[9]_i_1_n_0 ),
        .I1(\shift_green_reg_n_0_[3] ),
        .O(\shift_green[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \shift_green[2]_i_1 
       (.I0(\shift_green_reg_n_0_[4] ),
        .I1(\shift_red[9]_i_1_n_0 ),
        .I2(latched_green[2]),
        .O(shift_green[2]));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \shift_green[3]_i_1 
       (.I0(\shift_red[9]_i_1_n_0 ),
        .I1(\shift_green_reg_n_0_[5] ),
        .O(\shift_green[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \shift_green[4]_i_1 
       (.I0(\shift_green_reg_n_0_[6] ),
        .I1(\shift_red[9]_i_1_n_0 ),
        .I2(latched_green[2]),
        .O(shift_green[4]));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \shift_green[5]_i_1 
       (.I0(\shift_red[9]_i_1_n_0 ),
        .I1(\shift_green_reg_n_0_[7] ),
        .O(\shift_green[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \shift_green[6]_i_1 
       (.I0(\shift_green_reg_n_0_[8] ),
        .I1(\shift_red[9]_i_1_n_0 ),
        .I2(latched_green[2]),
        .O(shift_green[6]));
  LUT2 #(
    .INIT(4'h2)) 
    \shift_green[7]_i_1 
       (.I0(latched_green[0]),
        .I1(\shift_red[9]_i_1_n_0 ),
        .O(\shift_green[7]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \shift_green[7]_i_2 
       (.I0(\shift_red[9]_i_1_n_0 ),
        .I1(\shift_green_reg_n_0_[9] ),
        .O(\shift_green[7]_i_2_n_0 ));
  FDSE #(
    .INIT(1'b0)) 
    \shift_green_reg[0] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(\shift_green[0]_i_1_n_0 ),
        .Q(\shift_green_reg_n_0_[0] ),
        .S(\shift_green[7]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b0)) 
    \shift_green_reg[1] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(\shift_green[1]_i_1_n_0 ),
        .Q(\shift_green_reg_n_0_[1] ),
        .S(\shift_green[7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \shift_green_reg[2] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_green[2]),
        .Q(\shift_green_reg_n_0_[2] ),
        .R(1'b0));
  FDSE #(
    .INIT(1'b0)) 
    \shift_green_reg[3] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(\shift_green[3]_i_1_n_0 ),
        .Q(\shift_green_reg_n_0_[3] ),
        .S(\shift_green[7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \shift_green_reg[4] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_green[4]),
        .Q(\shift_green_reg_n_0_[4] ),
        .R(1'b0));
  FDSE #(
    .INIT(1'b0)) 
    \shift_green_reg[5] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(\shift_green[5]_i_1_n_0 ),
        .Q(\shift_green_reg_n_0_[5] ),
        .S(\shift_green[7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \shift_green_reg[6] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_green[6]),
        .Q(\shift_green_reg_n_0_[6] ),
        .R(1'b0));
  FDSE #(
    .INIT(1'b0)) 
    \shift_green_reg[7] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(\shift_green[7]_i_2_n_0 ),
        .Q(\shift_green_reg_n_0_[7] ),
        .S(\shift_green[7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \shift_green_reg[8] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(latched_green[8]),
        .Q(\shift_green_reg_n_0_[8] ),
        .R(\shift_red[9]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \shift_green_reg[9] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(latched_green[9]),
        .Q(\shift_green_reg_n_0_[9] ),
        .R(\shift_red[9]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \shift_red[0]_i_1 
       (.I0(\shift_red[9]_i_1_n_0 ),
        .I1(data1[0]),
        .O(\shift_red[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \shift_red[1]_i_1 
       (.I0(\shift_red[9]_i_1_n_0 ),
        .I1(data1[1]),
        .O(\shift_red[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \shift_red[2]_i_1 
       (.I0(data1[2]),
        .I1(\shift_red[9]_i_1_n_0 ),
        .I2(latched_red[2]),
        .O(shift_red[2]));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \shift_red[3]_i_1 
       (.I0(\shift_red[9]_i_1_n_0 ),
        .I1(data1[3]),
        .O(\shift_red[3]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \shift_red[4]_i_1 
       (.I0(data1[4]),
        .I1(\shift_red[9]_i_1_n_0 ),
        .I2(latched_red[2]),
        .O(shift_red[4]));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \shift_red[5]_i_1 
       (.I0(\shift_red[9]_i_1_n_0 ),
        .I1(data1[5]),
        .O(\shift_red[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \shift_red[6]_i_1 
       (.I0(data1[6]),
        .I1(\shift_red[9]_i_1_n_0 ),
        .I2(latched_red[2]),
        .O(shift_red[6]));
  LUT2 #(
    .INIT(4'h2)) 
    \shift_red[7]_i_1 
       (.I0(latched_red[0]),
        .I1(\shift_red[9]_i_1_n_0 ),
        .O(\shift_red[7]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \shift_red[7]_i_2 
       (.I0(\shift_red[9]_i_1_n_0 ),
        .I1(data1[7]),
        .O(\shift_red[7]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hEFFFFFFF)) 
    \shift_red[9]_i_1 
       (.I0(\shift_red[9]_i_2_n_0 ),
        .I1(shift_clock__0[5]),
        .I2(shift_clock__0[4]),
        .I3(shift_clock__0[2]),
        .I4(shift_clock__0[3]),
        .O(\shift_red[9]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFEFFFFFFFFFFFF)) 
    \shift_red[9]_i_2 
       (.I0(shift_clock__0[8]),
        .I1(shift_clock__0[9]),
        .I2(shift_clock__0[6]),
        .I3(shift_clock__0[7]),
        .I4(shift_clock[1]),
        .I5(shift_clock[0]),
        .O(\shift_red[9]_i_2_n_0 ));
  FDSE #(
    .INIT(1'b0)) 
    \shift_red_reg[0] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(\shift_red[0]_i_1_n_0 ),
        .Q(D0),
        .S(\shift_red[7]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b0)) 
    \shift_red_reg[1] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(\shift_red[1]_i_1_n_0 ),
        .Q(D1),
        .S(\shift_red[7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \shift_red_reg[2] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_red[2]),
        .Q(data1[0]),
        .R(1'b0));
  FDSE #(
    .INIT(1'b0)) 
    \shift_red_reg[3] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(\shift_red[3]_i_1_n_0 ),
        .Q(data1[1]),
        .S(\shift_red[7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \shift_red_reg[4] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_red[4]),
        .Q(data1[2]),
        .R(1'b0));
  FDSE #(
    .INIT(1'b0)) 
    \shift_red_reg[5] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(\shift_red[5]_i_1_n_0 ),
        .Q(data1[3]),
        .S(\shift_red[7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \shift_red_reg[6] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_red[6]),
        .Q(data1[4]),
        .R(1'b0));
  FDSE #(
    .INIT(1'b0)) 
    \shift_red_reg[7] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(\shift_red[7]_i_2_n_0 ),
        .Q(data1[5]),
        .S(\shift_red[7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \shift_red_reg[8] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(latched_red[8]),
        .Q(data1[6]),
        .R(\shift_red[9]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \shift_red_reg[9] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(latched_red[9]),
        .Q(data1[7]),
        .R(\shift_red[9]_i_1_n_0 ));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_i2s_ctl
   (ac_bclk,
    SR,
    ac_lrclk,
    ac_lrclk_sig_prev_reg,
    ready_sig_reg,
    w_sw,
    CO,
    DIBDI,
    \L_bus_in_reg[17] ,
    D,
    \sdp_bl.ramb18_dp_bl.ram18_bl ,
    \R_bus_in_reg[17] ,
    ac_dac_sdata,
    s00_axi_aclk,
    ac_adc_sdata,
    s00_axi_aresetn,
    ac_lrclk_sig_prev_reg_0,
    Q,
    \R_bus_in_reg[17]_0 ,
    E,
    ac_lrclk_count,
    \trigger_volt_reg[9] ,
    \slv_reg9_reg[15] ,
    switch,
    \trigger_volt_reg[8] ,
    \slv_reg10_reg[15] ,
    S);
  output ac_bclk;
  output [0:0]SR;
  output ac_lrclk;
  output ac_lrclk_sig_prev_reg;
  output ready_sig_reg;
  output [0:0]w_sw;
  output [0:0]CO;
  output [15:0]DIBDI;
  output [17:0]\L_bus_in_reg[17] ;
  output [0:0]D;
  output [15:0]\sdp_bl.ramb18_dp_bl.ram18_bl ;
  output [17:0]\R_bus_in_reg[17] ;
  output ac_dac_sdata;
  input s00_axi_aclk;
  input ac_adc_sdata;
  input s00_axi_aresetn;
  input ac_lrclk_sig_prev_reg_0;
  input [17:0]Q;
  input [17:0]\R_bus_in_reg[17]_0 ;
  input [0:0]E;
  input [2:0]ac_lrclk_count;
  input [0:0]\trigger_volt_reg[9] ;
  input [15:0]\slv_reg9_reg[15] ;
  input [0:0]switch;
  input [7:0]\trigger_volt_reg[8] ;
  input [15:0]\slv_reg10_reg[15] ;
  input [0:0]S;

  wire BCLK_Fall_int;
  wire BCLK_int_i_2_n_0;
  wire [0:0]CO;
  wire Cnt_Bclk0;
  wire \Cnt_Bclk0_inferred__0/i__carry_n_3 ;
  wire \Cnt_Bclk[4]_i_1_n_0 ;
  wire [4:0]Cnt_Bclk_reg__0;
  wire [4:0]Cnt_Lrclk;
  wire \Cnt_Lrclk[0]_i_1_n_0 ;
  wire \Cnt_Lrclk[1]_i_1_n_0 ;
  wire \Cnt_Lrclk[2]_i_1_n_0 ;
  wire \Cnt_Lrclk[3]_i_1_n_0 ;
  wire \Cnt_Lrclk[4]_i_2_n_0 ;
  wire [0:0]D;
  wire [15:0]DIBDI;
  wire D_L_O_int;
  wire \D_R_O_int[23]_i_1_n_0 ;
  wire [31:14]Data_In_int;
  wire \Data_In_int[31]_i_1_n_0 ;
  wire \Data_In_int[31]_i_3_n_0 ;
  wire \Data_In_int_reg[12]_srl13___U0_my_lab3_ip_v2_0_S00_AXI_inst_datapath_wrapper_inst_audio_inout_Data_In_int_reg_r_11_n_0 ;
  wire \Data_In_int_reg[13]_U0_my_lab3_ip_v2_0_S00_AXI_inst_datapath_wrapper_inst_audio_inout_Data_In_int_reg_r_12_n_0 ;
  wire Data_In_int_reg_gate_n_0;
  wire Data_In_int_reg_r_0_n_0;
  wire Data_In_int_reg_r_10_n_0;
  wire Data_In_int_reg_r_11_n_0;
  wire Data_In_int_reg_r_12_n_0;
  wire Data_In_int_reg_r_1_n_0;
  wire Data_In_int_reg_r_2_n_0;
  wire Data_In_int_reg_r_3_n_0;
  wire Data_In_int_reg_r_4_n_0;
  wire Data_In_int_reg_r_5_n_0;
  wire Data_In_int_reg_r_6_n_0;
  wire Data_In_int_reg_r_7_n_0;
  wire Data_In_int_reg_r_8_n_0;
  wire Data_In_int_reg_r_9_n_0;
  wire Data_In_int_reg_r_n_0;
  wire \Data_Out_int[13]_i_1_n_0 ;
  wire \Data_Out_int[14]_i_1_n_0 ;
  wire \Data_Out_int[15]_i_1_n_0 ;
  wire \Data_Out_int[16]_i_1_n_0 ;
  wire \Data_Out_int[17]_i_1_n_0 ;
  wire \Data_Out_int[18]_i_1_n_0 ;
  wire \Data_Out_int[19]_i_1_n_0 ;
  wire \Data_Out_int[20]_i_1_n_0 ;
  wire \Data_Out_int[21]_i_1_n_0 ;
  wire \Data_Out_int[22]_i_1_n_0 ;
  wire \Data_Out_int[23]_i_1_n_0 ;
  wire \Data_Out_int[24]_i_1_n_0 ;
  wire \Data_Out_int[25]_i_1_n_0 ;
  wire \Data_Out_int[26]_i_1_n_0 ;
  wire \Data_Out_int[27]_i_1_n_0 ;
  wire \Data_Out_int[28]_i_1_n_0 ;
  wire \Data_Out_int[29]_i_1_n_0 ;
  wire \Data_Out_int[30]_i_1_n_0 ;
  wire \Data_Out_int[31]_i_1_n_0 ;
  wire \Data_Out_int[31]_i_2_n_0 ;
  wire \Data_Out_int[31]_i_3_n_0 ;
  wire \Data_Out_int_reg_n_0_[13] ;
  wire \Data_Out_int_reg_n_0_[14] ;
  wire \Data_Out_int_reg_n_0_[15] ;
  wire \Data_Out_int_reg_n_0_[16] ;
  wire \Data_Out_int_reg_n_0_[17] ;
  wire \Data_Out_int_reg_n_0_[18] ;
  wire \Data_Out_int_reg_n_0_[19] ;
  wire \Data_Out_int_reg_n_0_[20] ;
  wire \Data_Out_int_reg_n_0_[21] ;
  wire \Data_Out_int_reg_n_0_[22] ;
  wire \Data_Out_int_reg_n_0_[23] ;
  wire \Data_Out_int_reg_n_0_[24] ;
  wire \Data_Out_int_reg_n_0_[25] ;
  wire \Data_Out_int_reg_n_0_[26] ;
  wire \Data_Out_int_reg_n_0_[27] ;
  wire \Data_Out_int_reg_n_0_[28] ;
  wire \Data_Out_int_reg_n_0_[29] ;
  wire \Data_Out_int_reg_n_0_[30] ;
  wire [0:0]E;
  wire \FSM_onehot_state[1]_i_10_n_0 ;
  wire \FSM_onehot_state[1]_i_11_n_0 ;
  wire \FSM_onehot_state[1]_i_12_n_0 ;
  wire \FSM_onehot_state[1]_i_13_n_0 ;
  wire \FSM_onehot_state[1]_i_14_n_0 ;
  wire \FSM_onehot_state[1]_i_15_n_0 ;
  wire \FSM_onehot_state[1]_i_8_n_0 ;
  wire \FSM_onehot_state[1]_i_9_n_0 ;
  wire \FSM_onehot_state_reg[1]_i_4_n_0 ;
  wire \FSM_onehot_state_reg[1]_i_4_n_1 ;
  wire \FSM_onehot_state_reg[1]_i_4_n_2 ;
  wire \FSM_onehot_state_reg[1]_i_4_n_3 ;
  wire LRCLK_i_1_n_0;
  wire LRCLK_i_2_n_0;
  wire [17:0]\L_bus_in_reg[17] ;
  wire [17:0]Q;
  wire [17:0]\R_bus_in_reg[17] ;
  wire [17:0]\R_bus_in_reg[17]_0 ;
  wire [0:0]S;
  wire [0:0]SR;
  wire ac_adc_sdata;
  wire ac_bclk;
  wire ac_dac_sdata;
  wire ac_lrclk;
  wire [2:0]ac_lrclk_count;
  wire ac_lrclk_count0;
  wire ac_lrclk_sig_prev_reg;
  wire ac_lrclk_sig_prev_reg_0;
  wire i__carry_i_1_n_0;
  wire i__carry_i_2__0_n_0;
  wire [4:0]p_0_in__0;
  wire p_17_in;
  wire ready_sig_reg;
  wire s00_axi_aclk;
  wire s00_axi_aresetn;
  wire [15:0]\sdp_bl.ramb18_dp_bl.ram18_bl ;
  wire [15:0]\slv_reg10_reg[15] ;
  wire [15:0]\slv_reg9_reg[15] ;
  wire [0:0]switch;
  wire [7:0]\trigger_volt_reg[8] ;
  wire [0:0]\trigger_volt_reg[9] ;
  wire [0:0]w_sw;
  wire [3:2]\NLW_Cnt_Bclk0_inferred__0/i__carry_CO_UNCONNECTED ;
  wire [3:0]\NLW_Cnt_Bclk0_inferred__0/i__carry_O_UNCONNECTED ;
  wire [3:1]\NLW_FSM_onehot_state_reg[1]_i_2_CO_UNCONNECTED ;
  wire [3:0]\NLW_FSM_onehot_state_reg[1]_i_2_O_UNCONNECTED ;
  wire [3:0]\NLW_FSM_onehot_state_reg[1]_i_4_O_UNCONNECTED ;

  LUT1 #(
    .INIT(2'h1)) 
    BCLK_int_i_1
       (.I0(s00_axi_aresetn),
        .O(SR));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT2 #(
    .INIT(4'h6)) 
    BCLK_int_i_2
       (.I0(Cnt_Bclk0),
        .I1(ac_bclk),
        .O(BCLK_int_i_2_n_0));
  FDRE BCLK_int_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(BCLK_int_i_2_n_0),
        .Q(ac_bclk),
        .R(SR));
  CARRY4 \Cnt_Bclk0_inferred__0/i__carry 
       (.CI(1'b0),
        .CO({\NLW_Cnt_Bclk0_inferred__0/i__carry_CO_UNCONNECTED [3:2],Cnt_Bclk0,\Cnt_Bclk0_inferred__0/i__carry_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_Cnt_Bclk0_inferred__0/i__carry_O_UNCONNECTED [3:0]),
        .S({1'b0,1'b0,i__carry_i_1_n_0,i__carry_i_2__0_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    \Cnt_Bclk[0]_i_1 
       (.I0(Cnt_Bclk_reg__0[0]),
        .O(p_0_in__0[0]));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \Cnt_Bclk[1]_i_1 
       (.I0(Cnt_Bclk_reg__0[1]),
        .I1(Cnt_Bclk_reg__0[0]),
        .O(p_0_in__0[1]));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \Cnt_Bclk[2]_i_1 
       (.I0(Cnt_Bclk_reg__0[2]),
        .I1(Cnt_Bclk_reg__0[0]),
        .I2(Cnt_Bclk_reg__0[1]),
        .O(p_0_in__0[2]));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \Cnt_Bclk[3]_i_1 
       (.I0(Cnt_Bclk_reg__0[3]),
        .I1(Cnt_Bclk_reg__0[2]),
        .I2(Cnt_Bclk_reg__0[1]),
        .I3(Cnt_Bclk_reg__0[0]),
        .O(p_0_in__0[3]));
  LUT2 #(
    .INIT(4'hB)) 
    \Cnt_Bclk[4]_i_1 
       (.I0(Cnt_Bclk0),
        .I1(s00_axi_aresetn),
        .O(\Cnt_Bclk[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \Cnt_Bclk[4]_i_2 
       (.I0(Cnt_Bclk_reg__0[4]),
        .I1(Cnt_Bclk_reg__0[0]),
        .I2(Cnt_Bclk_reg__0[1]),
        .I3(Cnt_Bclk_reg__0[2]),
        .I4(Cnt_Bclk_reg__0[3]),
        .O(p_0_in__0[4]));
  FDRE \Cnt_Bclk_reg[0] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in__0[0]),
        .Q(Cnt_Bclk_reg__0[0]),
        .R(\Cnt_Bclk[4]_i_1_n_0 ));
  FDRE \Cnt_Bclk_reg[1] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in__0[1]),
        .Q(Cnt_Bclk_reg__0[1]),
        .R(\Cnt_Bclk[4]_i_1_n_0 ));
  FDRE \Cnt_Bclk_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in__0[2]),
        .Q(Cnt_Bclk_reg__0[2]),
        .R(\Cnt_Bclk[4]_i_1_n_0 ));
  FDRE \Cnt_Bclk_reg[3] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in__0[3]),
        .Q(Cnt_Bclk_reg__0[3]),
        .R(\Cnt_Bclk[4]_i_1_n_0 ));
  FDRE \Cnt_Bclk_reg[4] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in__0[4]),
        .Q(Cnt_Bclk_reg__0[4]),
        .R(\Cnt_Bclk[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \Cnt_Lrclk[0]_i_1 
       (.I0(Cnt_Lrclk[0]),
        .O(\Cnt_Lrclk[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \Cnt_Lrclk[1]_i_1 
       (.I0(Cnt_Lrclk[1]),
        .I1(Cnt_Lrclk[0]),
        .O(\Cnt_Lrclk[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \Cnt_Lrclk[2]_i_1 
       (.I0(Cnt_Lrclk[2]),
        .I1(Cnt_Lrclk[0]),
        .I2(Cnt_Lrclk[1]),
        .O(\Cnt_Lrclk[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \Cnt_Lrclk[3]_i_1 
       (.I0(Cnt_Lrclk[3]),
        .I1(Cnt_Lrclk[1]),
        .I2(Cnt_Lrclk[0]),
        .I3(Cnt_Lrclk[2]),
        .O(\Cnt_Lrclk[3]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \Cnt_Lrclk[4]_i_1 
       (.I0(Cnt_Bclk0),
        .I1(ac_bclk),
        .O(BCLK_Fall_int));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \Cnt_Lrclk[4]_i_2 
       (.I0(Cnt_Lrclk[4]),
        .I1(Cnt_Lrclk[2]),
        .I2(Cnt_Lrclk[0]),
        .I3(Cnt_Lrclk[1]),
        .I4(Cnt_Lrclk[3]),
        .O(\Cnt_Lrclk[4]_i_2_n_0 ));
  FDRE \Cnt_Lrclk_reg[0] 
       (.C(s00_axi_aclk),
        .CE(BCLK_Fall_int),
        .D(\Cnt_Lrclk[0]_i_1_n_0 ),
        .Q(Cnt_Lrclk[0]),
        .R(SR));
  FDRE \Cnt_Lrclk_reg[1] 
       (.C(s00_axi_aclk),
        .CE(BCLK_Fall_int),
        .D(\Cnt_Lrclk[1]_i_1_n_0 ),
        .Q(Cnt_Lrclk[1]),
        .R(SR));
  FDRE \Cnt_Lrclk_reg[2] 
       (.C(s00_axi_aclk),
        .CE(BCLK_Fall_int),
        .D(\Cnt_Lrclk[2]_i_1_n_0 ),
        .Q(Cnt_Lrclk[2]),
        .R(SR));
  FDRE \Cnt_Lrclk_reg[3] 
       (.C(s00_axi_aclk),
        .CE(BCLK_Fall_int),
        .D(\Cnt_Lrclk[3]_i_1_n_0 ),
        .Q(Cnt_Lrclk[3]),
        .R(SR));
  FDRE \Cnt_Lrclk_reg[4] 
       (.C(s00_axi_aclk),
        .CE(BCLK_Fall_int),
        .D(\Cnt_Lrclk[4]_i_2_n_0 ),
        .Q(Cnt_Lrclk[4]),
        .R(SR));
  LUT2 #(
    .INIT(4'h2)) 
    \D_L_O_int[23]_i_1 
       (.I0(ac_lrclk),
        .I1(\Data_In_int[31]_i_3_n_0 ),
        .O(D_L_O_int));
  FDRE \D_L_O_int_reg[10] 
       (.C(s00_axi_aclk),
        .CE(D_L_O_int),
        .D(Data_In_int[18]),
        .Q(\L_bus_in_reg[17] [4]),
        .R(SR));
  FDRE \D_L_O_int_reg[11] 
       (.C(s00_axi_aclk),
        .CE(D_L_O_int),
        .D(Data_In_int[19]),
        .Q(\L_bus_in_reg[17] [5]),
        .R(SR));
  FDRE \D_L_O_int_reg[12] 
       (.C(s00_axi_aclk),
        .CE(D_L_O_int),
        .D(Data_In_int[20]),
        .Q(\L_bus_in_reg[17] [6]),
        .R(SR));
  FDRE \D_L_O_int_reg[13] 
       (.C(s00_axi_aclk),
        .CE(D_L_O_int),
        .D(Data_In_int[21]),
        .Q(\L_bus_in_reg[17] [7]),
        .R(SR));
  FDRE \D_L_O_int_reg[14] 
       (.C(s00_axi_aclk),
        .CE(D_L_O_int),
        .D(Data_In_int[22]),
        .Q(\L_bus_in_reg[17] [8]),
        .R(SR));
  FDRE \D_L_O_int_reg[15] 
       (.C(s00_axi_aclk),
        .CE(D_L_O_int),
        .D(Data_In_int[23]),
        .Q(\L_bus_in_reg[17] [9]),
        .R(SR));
  FDRE \D_L_O_int_reg[16] 
       (.C(s00_axi_aclk),
        .CE(D_L_O_int),
        .D(Data_In_int[24]),
        .Q(\L_bus_in_reg[17] [10]),
        .R(SR));
  FDRE \D_L_O_int_reg[17] 
       (.C(s00_axi_aclk),
        .CE(D_L_O_int),
        .D(Data_In_int[25]),
        .Q(\L_bus_in_reg[17] [11]),
        .R(SR));
  FDRE \D_L_O_int_reg[18] 
       (.C(s00_axi_aclk),
        .CE(D_L_O_int),
        .D(Data_In_int[26]),
        .Q(\L_bus_in_reg[17] [12]),
        .R(SR));
  FDRE \D_L_O_int_reg[19] 
       (.C(s00_axi_aclk),
        .CE(D_L_O_int),
        .D(Data_In_int[27]),
        .Q(\L_bus_in_reg[17] [13]),
        .R(SR));
  FDRE \D_L_O_int_reg[20] 
       (.C(s00_axi_aclk),
        .CE(D_L_O_int),
        .D(Data_In_int[28]),
        .Q(\L_bus_in_reg[17] [14]),
        .R(SR));
  FDRE \D_L_O_int_reg[21] 
       (.C(s00_axi_aclk),
        .CE(D_L_O_int),
        .D(Data_In_int[29]),
        .Q(\L_bus_in_reg[17] [15]),
        .R(SR));
  FDRE \D_L_O_int_reg[22] 
       (.C(s00_axi_aclk),
        .CE(D_L_O_int),
        .D(Data_In_int[30]),
        .Q(\L_bus_in_reg[17] [16]),
        .R(SR));
  FDRE \D_L_O_int_reg[23] 
       (.C(s00_axi_aclk),
        .CE(D_L_O_int),
        .D(Data_In_int[31]),
        .Q(\L_bus_in_reg[17] [17]),
        .R(SR));
  FDRE \D_L_O_int_reg[6] 
       (.C(s00_axi_aclk),
        .CE(D_L_O_int),
        .D(Data_In_int[14]),
        .Q(\L_bus_in_reg[17] [0]),
        .R(SR));
  FDRE \D_L_O_int_reg[7] 
       (.C(s00_axi_aclk),
        .CE(D_L_O_int),
        .D(Data_In_int[15]),
        .Q(\L_bus_in_reg[17] [1]),
        .R(SR));
  FDRE \D_L_O_int_reg[8] 
       (.C(s00_axi_aclk),
        .CE(D_L_O_int),
        .D(Data_In_int[16]),
        .Q(\L_bus_in_reg[17] [2]),
        .R(SR));
  FDRE \D_L_O_int_reg[9] 
       (.C(s00_axi_aclk),
        .CE(D_L_O_int),
        .D(Data_In_int[17]),
        .Q(\L_bus_in_reg[17] [3]),
        .R(SR));
  LUT2 #(
    .INIT(4'h1)) 
    \D_R_O_int[23]_i_1 
       (.I0(ac_lrclk),
        .I1(\Data_In_int[31]_i_3_n_0 ),
        .O(\D_R_O_int[23]_i_1_n_0 ));
  FDRE \D_R_O_int_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\D_R_O_int[23]_i_1_n_0 ),
        .D(Data_In_int[18]),
        .Q(\R_bus_in_reg[17] [4]),
        .R(SR));
  FDRE \D_R_O_int_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\D_R_O_int[23]_i_1_n_0 ),
        .D(Data_In_int[19]),
        .Q(\R_bus_in_reg[17] [5]),
        .R(SR));
  FDRE \D_R_O_int_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\D_R_O_int[23]_i_1_n_0 ),
        .D(Data_In_int[20]),
        .Q(\R_bus_in_reg[17] [6]),
        .R(SR));
  FDRE \D_R_O_int_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\D_R_O_int[23]_i_1_n_0 ),
        .D(Data_In_int[21]),
        .Q(\R_bus_in_reg[17] [7]),
        .R(SR));
  FDRE \D_R_O_int_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\D_R_O_int[23]_i_1_n_0 ),
        .D(Data_In_int[22]),
        .Q(\R_bus_in_reg[17] [8]),
        .R(SR));
  FDRE \D_R_O_int_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\D_R_O_int[23]_i_1_n_0 ),
        .D(Data_In_int[23]),
        .Q(\R_bus_in_reg[17] [9]),
        .R(SR));
  FDRE \D_R_O_int_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\D_R_O_int[23]_i_1_n_0 ),
        .D(Data_In_int[24]),
        .Q(\R_bus_in_reg[17] [10]),
        .R(SR));
  FDRE \D_R_O_int_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\D_R_O_int[23]_i_1_n_0 ),
        .D(Data_In_int[25]),
        .Q(\R_bus_in_reg[17] [11]),
        .R(SR));
  FDRE \D_R_O_int_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\D_R_O_int[23]_i_1_n_0 ),
        .D(Data_In_int[26]),
        .Q(\R_bus_in_reg[17] [12]),
        .R(SR));
  FDRE \D_R_O_int_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\D_R_O_int[23]_i_1_n_0 ),
        .D(Data_In_int[27]),
        .Q(\R_bus_in_reg[17] [13]),
        .R(SR));
  FDRE \D_R_O_int_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\D_R_O_int[23]_i_1_n_0 ),
        .D(Data_In_int[28]),
        .Q(\R_bus_in_reg[17] [14]),
        .R(SR));
  FDRE \D_R_O_int_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\D_R_O_int[23]_i_1_n_0 ),
        .D(Data_In_int[29]),
        .Q(\R_bus_in_reg[17] [15]),
        .R(SR));
  FDRE \D_R_O_int_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\D_R_O_int[23]_i_1_n_0 ),
        .D(Data_In_int[30]),
        .Q(\R_bus_in_reg[17] [16]),
        .R(SR));
  FDRE \D_R_O_int_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\D_R_O_int[23]_i_1_n_0 ),
        .D(Data_In_int[31]),
        .Q(\R_bus_in_reg[17] [17]),
        .R(SR));
  FDRE \D_R_O_int_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\D_R_O_int[23]_i_1_n_0 ),
        .D(Data_In_int[14]),
        .Q(\R_bus_in_reg[17] [0]),
        .R(SR));
  FDRE \D_R_O_int_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\D_R_O_int[23]_i_1_n_0 ),
        .D(Data_In_int[15]),
        .Q(\R_bus_in_reg[17] [1]),
        .R(SR));
  FDRE \D_R_O_int_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\D_R_O_int[23]_i_1_n_0 ),
        .D(Data_In_int[16]),
        .Q(\R_bus_in_reg[17] [2]),
        .R(SR));
  FDRE \D_R_O_int_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\D_R_O_int[23]_i_1_n_0 ),
        .D(Data_In_int[17]),
        .Q(\R_bus_in_reg[17] [3]),
        .R(SR));
  LUT2 #(
    .INIT(4'h7)) 
    \Data_In_int[31]_i_1 
       (.I0(s00_axi_aresetn),
        .I1(\Data_In_int[31]_i_3_n_0 ),
        .O(\Data_In_int[31]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \Data_In_int[31]_i_2 
       (.I0(Cnt_Bclk0),
        .I1(ac_bclk),
        .O(p_17_in));
  LUT6 #(
    .INIT(64'hFFFFFFFEFFFFFFFF)) 
    \Data_In_int[31]_i_3 
       (.I0(Cnt_Lrclk[3]),
        .I1(Cnt_Lrclk[4]),
        .I2(Cnt_Lrclk[2]),
        .I3(Cnt_Lrclk[1]),
        .I4(Cnt_Lrclk[0]),
        .I5(BCLK_Fall_int),
        .O(\Data_In_int[31]_i_3_n_0 ));
  (* srl_bus_name = "\U0/my_lab3_ip_v2_0_S00_AXI_inst/datapath/wrapper_inst/audio_inout/Data_In_int_reg " *) 
  (* srl_name = "\U0/my_lab3_ip_v2_0_S00_AXI_inst/datapath/wrapper_inst/audio_inout/Data_In_int_reg[12]_srl13___U0_my_lab3_ip_v2_0_S00_AXI_inst_datapath_wrapper_inst_audio_inout_Data_In_int_reg_r_11 " *) 
  SRL16E \Data_In_int_reg[12]_srl13___U0_my_lab3_ip_v2_0_S00_AXI_inst_datapath_wrapper_inst_audio_inout_Data_In_int_reg_r_11 
       (.A0(1'b0),
        .A1(1'b0),
        .A2(1'b1),
        .A3(1'b1),
        .CE(p_17_in),
        .CLK(s00_axi_aclk),
        .D(ac_adc_sdata),
        .Q(\Data_In_int_reg[12]_srl13___U0_my_lab3_ip_v2_0_S00_AXI_inst_datapath_wrapper_inst_audio_inout_Data_In_int_reg_r_11_n_0 ));
  FDRE \Data_In_int_reg[13]_U0_my_lab3_ip_v2_0_S00_AXI_inst_datapath_wrapper_inst_audio_inout_Data_In_int_reg_r_12 
       (.C(s00_axi_aclk),
        .CE(p_17_in),
        .D(\Data_In_int_reg[12]_srl13___U0_my_lab3_ip_v2_0_S00_AXI_inst_datapath_wrapper_inst_audio_inout_Data_In_int_reg_r_11_n_0 ),
        .Q(\Data_In_int_reg[13]_U0_my_lab3_ip_v2_0_S00_AXI_inst_datapath_wrapper_inst_audio_inout_Data_In_int_reg_r_12_n_0 ),
        .R(1'b0));
  FDRE \Data_In_int_reg[14] 
       (.C(s00_axi_aclk),
        .CE(p_17_in),
        .D(Data_In_int_reg_gate_n_0),
        .Q(Data_In_int[14]),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE \Data_In_int_reg[15] 
       (.C(s00_axi_aclk),
        .CE(p_17_in),
        .D(Data_In_int[14]),
        .Q(Data_In_int[15]),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE \Data_In_int_reg[16] 
       (.C(s00_axi_aclk),
        .CE(p_17_in),
        .D(Data_In_int[15]),
        .Q(Data_In_int[16]),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE \Data_In_int_reg[17] 
       (.C(s00_axi_aclk),
        .CE(p_17_in),
        .D(Data_In_int[16]),
        .Q(Data_In_int[17]),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE \Data_In_int_reg[18] 
       (.C(s00_axi_aclk),
        .CE(p_17_in),
        .D(Data_In_int[17]),
        .Q(Data_In_int[18]),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE \Data_In_int_reg[19] 
       (.C(s00_axi_aclk),
        .CE(p_17_in),
        .D(Data_In_int[18]),
        .Q(Data_In_int[19]),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE \Data_In_int_reg[20] 
       (.C(s00_axi_aclk),
        .CE(p_17_in),
        .D(Data_In_int[19]),
        .Q(Data_In_int[20]),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE \Data_In_int_reg[21] 
       (.C(s00_axi_aclk),
        .CE(p_17_in),
        .D(Data_In_int[20]),
        .Q(Data_In_int[21]),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE \Data_In_int_reg[22] 
       (.C(s00_axi_aclk),
        .CE(p_17_in),
        .D(Data_In_int[21]),
        .Q(Data_In_int[22]),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE \Data_In_int_reg[23] 
       (.C(s00_axi_aclk),
        .CE(p_17_in),
        .D(Data_In_int[22]),
        .Q(Data_In_int[23]),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE \Data_In_int_reg[24] 
       (.C(s00_axi_aclk),
        .CE(p_17_in),
        .D(Data_In_int[23]),
        .Q(Data_In_int[24]),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE \Data_In_int_reg[25] 
       (.C(s00_axi_aclk),
        .CE(p_17_in),
        .D(Data_In_int[24]),
        .Q(Data_In_int[25]),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE \Data_In_int_reg[26] 
       (.C(s00_axi_aclk),
        .CE(p_17_in),
        .D(Data_In_int[25]),
        .Q(Data_In_int[26]),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE \Data_In_int_reg[27] 
       (.C(s00_axi_aclk),
        .CE(p_17_in),
        .D(Data_In_int[26]),
        .Q(Data_In_int[27]),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE \Data_In_int_reg[28] 
       (.C(s00_axi_aclk),
        .CE(p_17_in),
        .D(Data_In_int[27]),
        .Q(Data_In_int[28]),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE \Data_In_int_reg[29] 
       (.C(s00_axi_aclk),
        .CE(p_17_in),
        .D(Data_In_int[28]),
        .Q(Data_In_int[29]),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE \Data_In_int_reg[30] 
       (.C(s00_axi_aclk),
        .CE(p_17_in),
        .D(Data_In_int[29]),
        .Q(Data_In_int[30]),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE \Data_In_int_reg[31] 
       (.C(s00_axi_aclk),
        .CE(p_17_in),
        .D(Data_In_int[30]),
        .Q(Data_In_int[31]),
        .R(\Data_In_int[31]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    Data_In_int_reg_gate
       (.I0(\Data_In_int_reg[13]_U0_my_lab3_ip_v2_0_S00_AXI_inst_datapath_wrapper_inst_audio_inout_Data_In_int_reg_r_12_n_0 ),
        .I1(Data_In_int_reg_r_12_n_0),
        .O(Data_In_int_reg_gate_n_0));
  FDRE Data_In_int_reg_r
       (.C(s00_axi_aclk),
        .CE(p_17_in),
        .D(1'b1),
        .Q(Data_In_int_reg_r_n_0),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE Data_In_int_reg_r_0
       (.C(s00_axi_aclk),
        .CE(p_17_in),
        .D(Data_In_int_reg_r_n_0),
        .Q(Data_In_int_reg_r_0_n_0),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE Data_In_int_reg_r_1
       (.C(s00_axi_aclk),
        .CE(p_17_in),
        .D(Data_In_int_reg_r_0_n_0),
        .Q(Data_In_int_reg_r_1_n_0),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE Data_In_int_reg_r_10
       (.C(s00_axi_aclk),
        .CE(p_17_in),
        .D(Data_In_int_reg_r_9_n_0),
        .Q(Data_In_int_reg_r_10_n_0),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE Data_In_int_reg_r_11
       (.C(s00_axi_aclk),
        .CE(p_17_in),
        .D(Data_In_int_reg_r_10_n_0),
        .Q(Data_In_int_reg_r_11_n_0),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE Data_In_int_reg_r_12
       (.C(s00_axi_aclk),
        .CE(p_17_in),
        .D(Data_In_int_reg_r_11_n_0),
        .Q(Data_In_int_reg_r_12_n_0),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE Data_In_int_reg_r_2
       (.C(s00_axi_aclk),
        .CE(p_17_in),
        .D(Data_In_int_reg_r_1_n_0),
        .Q(Data_In_int_reg_r_2_n_0),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE Data_In_int_reg_r_3
       (.C(s00_axi_aclk),
        .CE(p_17_in),
        .D(Data_In_int_reg_r_2_n_0),
        .Q(Data_In_int_reg_r_3_n_0),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE Data_In_int_reg_r_4
       (.C(s00_axi_aclk),
        .CE(p_17_in),
        .D(Data_In_int_reg_r_3_n_0),
        .Q(Data_In_int_reg_r_4_n_0),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE Data_In_int_reg_r_5
       (.C(s00_axi_aclk),
        .CE(p_17_in),
        .D(Data_In_int_reg_r_4_n_0),
        .Q(Data_In_int_reg_r_5_n_0),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE Data_In_int_reg_r_6
       (.C(s00_axi_aclk),
        .CE(p_17_in),
        .D(Data_In_int_reg_r_5_n_0),
        .Q(Data_In_int_reg_r_6_n_0),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE Data_In_int_reg_r_7
       (.C(s00_axi_aclk),
        .CE(p_17_in),
        .D(Data_In_int_reg_r_6_n_0),
        .Q(Data_In_int_reg_r_7_n_0),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE Data_In_int_reg_r_8
       (.C(s00_axi_aclk),
        .CE(p_17_in),
        .D(Data_In_int_reg_r_7_n_0),
        .Q(Data_In_int_reg_r_8_n_0),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE Data_In_int_reg_r_9
       (.C(s00_axi_aclk),
        .CE(p_17_in),
        .D(Data_In_int_reg_r_8_n_0),
        .Q(Data_In_int_reg_r_9_n_0),
        .R(\Data_In_int[31]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hA8CC08CC)) 
    \Data_Out_int[13]_i_1 
       (.I0(\Data_Out_int[31]_i_3_n_0 ),
        .I1(Q[0]),
        .I2(ac_lrclk),
        .I3(s00_axi_aresetn),
        .I4(\R_bus_in_reg[17]_0 [0]),
        .O(\Data_Out_int[13]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBFB0FFFF8F800000)) 
    \Data_Out_int[14]_i_1 
       (.I0(\R_bus_in_reg[17]_0 [1]),
        .I1(ac_lrclk),
        .I2(\Data_Out_int[31]_i_3_n_0 ),
        .I3(\Data_Out_int_reg_n_0_[13] ),
        .I4(s00_axi_aresetn),
        .I5(Q[1]),
        .O(\Data_Out_int[14]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBFB0FFFF8F800000)) 
    \Data_Out_int[15]_i_1 
       (.I0(\R_bus_in_reg[17]_0 [2]),
        .I1(ac_lrclk),
        .I2(\Data_Out_int[31]_i_3_n_0 ),
        .I3(\Data_Out_int_reg_n_0_[14] ),
        .I4(s00_axi_aresetn),
        .I5(Q[2]),
        .O(\Data_Out_int[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBFB0FFFF8F800000)) 
    \Data_Out_int[16]_i_1 
       (.I0(\R_bus_in_reg[17]_0 [3]),
        .I1(ac_lrclk),
        .I2(\Data_Out_int[31]_i_3_n_0 ),
        .I3(\Data_Out_int_reg_n_0_[15] ),
        .I4(s00_axi_aresetn),
        .I5(Q[3]),
        .O(\Data_Out_int[16]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBFB0FFFF8F800000)) 
    \Data_Out_int[17]_i_1 
       (.I0(\R_bus_in_reg[17]_0 [4]),
        .I1(ac_lrclk),
        .I2(\Data_Out_int[31]_i_3_n_0 ),
        .I3(\Data_Out_int_reg_n_0_[16] ),
        .I4(s00_axi_aresetn),
        .I5(Q[4]),
        .O(\Data_Out_int[17]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBFB0FFFF8F800000)) 
    \Data_Out_int[18]_i_1 
       (.I0(\R_bus_in_reg[17]_0 [5]),
        .I1(ac_lrclk),
        .I2(\Data_Out_int[31]_i_3_n_0 ),
        .I3(\Data_Out_int_reg_n_0_[17] ),
        .I4(s00_axi_aresetn),
        .I5(Q[5]),
        .O(\Data_Out_int[18]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBFB0FFFF8F800000)) 
    \Data_Out_int[19]_i_1 
       (.I0(\R_bus_in_reg[17]_0 [6]),
        .I1(ac_lrclk),
        .I2(\Data_Out_int[31]_i_3_n_0 ),
        .I3(\Data_Out_int_reg_n_0_[18] ),
        .I4(s00_axi_aresetn),
        .I5(Q[6]),
        .O(\Data_Out_int[19]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBFB0FFFF8F800000)) 
    \Data_Out_int[20]_i_1 
       (.I0(\R_bus_in_reg[17]_0 [7]),
        .I1(ac_lrclk),
        .I2(\Data_Out_int[31]_i_3_n_0 ),
        .I3(\Data_Out_int_reg_n_0_[19] ),
        .I4(s00_axi_aresetn),
        .I5(Q[7]),
        .O(\Data_Out_int[20]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBFB0FFFF8F800000)) 
    \Data_Out_int[21]_i_1 
       (.I0(\R_bus_in_reg[17]_0 [8]),
        .I1(ac_lrclk),
        .I2(\Data_Out_int[31]_i_3_n_0 ),
        .I3(\Data_Out_int_reg_n_0_[20] ),
        .I4(s00_axi_aresetn),
        .I5(Q[8]),
        .O(\Data_Out_int[21]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBFB0FFFF8F800000)) 
    \Data_Out_int[22]_i_1 
       (.I0(\R_bus_in_reg[17]_0 [9]),
        .I1(ac_lrclk),
        .I2(\Data_Out_int[31]_i_3_n_0 ),
        .I3(\Data_Out_int_reg_n_0_[21] ),
        .I4(s00_axi_aresetn),
        .I5(Q[9]),
        .O(\Data_Out_int[22]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBFB0FFFF8F800000)) 
    \Data_Out_int[23]_i_1 
       (.I0(\R_bus_in_reg[17]_0 [10]),
        .I1(ac_lrclk),
        .I2(\Data_Out_int[31]_i_3_n_0 ),
        .I3(\Data_Out_int_reg_n_0_[22] ),
        .I4(s00_axi_aresetn),
        .I5(Q[10]),
        .O(\Data_Out_int[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBFB0FFFF8F800000)) 
    \Data_Out_int[24]_i_1 
       (.I0(\R_bus_in_reg[17]_0 [11]),
        .I1(ac_lrclk),
        .I2(\Data_Out_int[31]_i_3_n_0 ),
        .I3(\Data_Out_int_reg_n_0_[23] ),
        .I4(s00_axi_aresetn),
        .I5(Q[11]),
        .O(\Data_Out_int[24]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBFB0FFFF8F800000)) 
    \Data_Out_int[25]_i_1 
       (.I0(\R_bus_in_reg[17]_0 [12]),
        .I1(ac_lrclk),
        .I2(\Data_Out_int[31]_i_3_n_0 ),
        .I3(\Data_Out_int_reg_n_0_[24] ),
        .I4(s00_axi_aresetn),
        .I5(Q[12]),
        .O(\Data_Out_int[25]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBFB0FFFF8F800000)) 
    \Data_Out_int[26]_i_1 
       (.I0(\R_bus_in_reg[17]_0 [13]),
        .I1(ac_lrclk),
        .I2(\Data_Out_int[31]_i_3_n_0 ),
        .I3(\Data_Out_int_reg_n_0_[25] ),
        .I4(s00_axi_aresetn),
        .I5(Q[13]),
        .O(\Data_Out_int[26]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBFB0FFFF8F800000)) 
    \Data_Out_int[27]_i_1 
       (.I0(\R_bus_in_reg[17]_0 [14]),
        .I1(ac_lrclk),
        .I2(\Data_Out_int[31]_i_3_n_0 ),
        .I3(\Data_Out_int_reg_n_0_[26] ),
        .I4(s00_axi_aresetn),
        .I5(Q[14]),
        .O(\Data_Out_int[27]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBFB0FFFF8F800000)) 
    \Data_Out_int[28]_i_1 
       (.I0(\R_bus_in_reg[17]_0 [15]),
        .I1(ac_lrclk),
        .I2(\Data_Out_int[31]_i_3_n_0 ),
        .I3(\Data_Out_int_reg_n_0_[27] ),
        .I4(s00_axi_aresetn),
        .I5(Q[15]),
        .O(\Data_Out_int[28]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBFB0FFFF8F800000)) 
    \Data_Out_int[29]_i_1 
       (.I0(\R_bus_in_reg[17]_0 [16]),
        .I1(ac_lrclk),
        .I2(\Data_Out_int[31]_i_3_n_0 ),
        .I3(\Data_Out_int_reg_n_0_[28] ),
        .I4(s00_axi_aresetn),
        .I5(Q[16]),
        .O(\Data_Out_int[29]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBFB0FFFF8F800000)) 
    \Data_Out_int[30]_i_1 
       (.I0(\R_bus_in_reg[17]_0 [17]),
        .I1(ac_lrclk),
        .I2(\Data_Out_int[31]_i_3_n_0 ),
        .I3(\Data_Out_int_reg_n_0_[29] ),
        .I4(s00_axi_aresetn),
        .I5(Q[17]),
        .O(\Data_Out_int[30]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFF8F)) 
    \Data_Out_int[31]_i_1 
       (.I0(ac_bclk),
        .I1(Cnt_Bclk0),
        .I2(s00_axi_aresetn),
        .I3(\Data_Out_int[31]_i_3_n_0 ),
        .O(\Data_Out_int[31]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \Data_Out_int[31]_i_2 
       (.I0(\Data_Out_int_reg_n_0_[30] ),
        .I1(s00_axi_aresetn),
        .I2(\Data_Out_int[31]_i_3_n_0 ),
        .O(\Data_Out_int[31]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000002)) 
    \Data_Out_int[31]_i_3 
       (.I0(p_17_in),
        .I1(Cnt_Lrclk[3]),
        .I2(Cnt_Lrclk[4]),
        .I3(Cnt_Lrclk[2]),
        .I4(Cnt_Lrclk[1]),
        .I5(Cnt_Lrclk[0]),
        .O(\Data_Out_int[31]_i_3_n_0 ));
  FDRE \Data_Out_int_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\Data_Out_int[31]_i_1_n_0 ),
        .D(\Data_Out_int[13]_i_1_n_0 ),
        .Q(\Data_Out_int_reg_n_0_[13] ),
        .R(1'b0));
  FDRE \Data_Out_int_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\Data_Out_int[31]_i_1_n_0 ),
        .D(\Data_Out_int[14]_i_1_n_0 ),
        .Q(\Data_Out_int_reg_n_0_[14] ),
        .R(1'b0));
  FDRE \Data_Out_int_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\Data_Out_int[31]_i_1_n_0 ),
        .D(\Data_Out_int[15]_i_1_n_0 ),
        .Q(\Data_Out_int_reg_n_0_[15] ),
        .R(1'b0));
  FDRE \Data_Out_int_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\Data_Out_int[31]_i_1_n_0 ),
        .D(\Data_Out_int[16]_i_1_n_0 ),
        .Q(\Data_Out_int_reg_n_0_[16] ),
        .R(1'b0));
  FDRE \Data_Out_int_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\Data_Out_int[31]_i_1_n_0 ),
        .D(\Data_Out_int[17]_i_1_n_0 ),
        .Q(\Data_Out_int_reg_n_0_[17] ),
        .R(1'b0));
  FDRE \Data_Out_int_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\Data_Out_int[31]_i_1_n_0 ),
        .D(\Data_Out_int[18]_i_1_n_0 ),
        .Q(\Data_Out_int_reg_n_0_[18] ),
        .R(1'b0));
  FDRE \Data_Out_int_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\Data_Out_int[31]_i_1_n_0 ),
        .D(\Data_Out_int[19]_i_1_n_0 ),
        .Q(\Data_Out_int_reg_n_0_[19] ),
        .R(1'b0));
  FDRE \Data_Out_int_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\Data_Out_int[31]_i_1_n_0 ),
        .D(\Data_Out_int[20]_i_1_n_0 ),
        .Q(\Data_Out_int_reg_n_0_[20] ),
        .R(1'b0));
  FDRE \Data_Out_int_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\Data_Out_int[31]_i_1_n_0 ),
        .D(\Data_Out_int[21]_i_1_n_0 ),
        .Q(\Data_Out_int_reg_n_0_[21] ),
        .R(1'b0));
  FDRE \Data_Out_int_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\Data_Out_int[31]_i_1_n_0 ),
        .D(\Data_Out_int[22]_i_1_n_0 ),
        .Q(\Data_Out_int_reg_n_0_[22] ),
        .R(1'b0));
  FDRE \Data_Out_int_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\Data_Out_int[31]_i_1_n_0 ),
        .D(\Data_Out_int[23]_i_1_n_0 ),
        .Q(\Data_Out_int_reg_n_0_[23] ),
        .R(1'b0));
  FDRE \Data_Out_int_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\Data_Out_int[31]_i_1_n_0 ),
        .D(\Data_Out_int[24]_i_1_n_0 ),
        .Q(\Data_Out_int_reg_n_0_[24] ),
        .R(1'b0));
  FDRE \Data_Out_int_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\Data_Out_int[31]_i_1_n_0 ),
        .D(\Data_Out_int[25]_i_1_n_0 ),
        .Q(\Data_Out_int_reg_n_0_[25] ),
        .R(1'b0));
  FDRE \Data_Out_int_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\Data_Out_int[31]_i_1_n_0 ),
        .D(\Data_Out_int[26]_i_1_n_0 ),
        .Q(\Data_Out_int_reg_n_0_[26] ),
        .R(1'b0));
  FDRE \Data_Out_int_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\Data_Out_int[31]_i_1_n_0 ),
        .D(\Data_Out_int[27]_i_1_n_0 ),
        .Q(\Data_Out_int_reg_n_0_[27] ),
        .R(1'b0));
  FDRE \Data_Out_int_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\Data_Out_int[31]_i_1_n_0 ),
        .D(\Data_Out_int[28]_i_1_n_0 ),
        .Q(\Data_Out_int_reg_n_0_[28] ),
        .R(1'b0));
  FDRE \Data_Out_int_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\Data_Out_int[31]_i_1_n_0 ),
        .D(\Data_Out_int[29]_i_1_n_0 ),
        .Q(\Data_Out_int_reg_n_0_[29] ),
        .R(1'b0));
  FDRE \Data_Out_int_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\Data_Out_int[31]_i_1_n_0 ),
        .D(\Data_Out_int[30]_i_1_n_0 ),
        .Q(\Data_Out_int_reg_n_0_[30] ),
        .R(1'b0));
  FDRE \Data_Out_int_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\Data_Out_int[31]_i_1_n_0 ),
        .D(\Data_Out_int[31]_i_2_n_0 ),
        .Q(ac_dac_sdata),
        .R(1'b0));
  LUT4 #(
    .INIT(16'h44D4)) 
    \FSM_onehot_state[1]_i_10 
       (.I0(\trigger_volt_reg[8] [3]),
        .I1(\L_bus_in_reg[17] [13]),
        .I2(\L_bus_in_reg[17] [12]),
        .I3(\trigger_volt_reg[8] [2]),
        .O(\FSM_onehot_state[1]_i_10_n_0 ));
  LUT4 #(
    .INIT(16'h44D4)) 
    \FSM_onehot_state[1]_i_11 
       (.I0(\trigger_volt_reg[8] [1]),
        .I1(\L_bus_in_reg[17] [11]),
        .I2(\L_bus_in_reg[17] [10]),
        .I3(\trigger_volt_reg[8] [0]),
        .O(\FSM_onehot_state[1]_i_11_n_0 ));
  LUT4 #(
    .INIT(16'h6006)) 
    \FSM_onehot_state[1]_i_12 
       (.I0(\L_bus_in_reg[17] [17]),
        .I1(\trigger_volt_reg[8] [7]),
        .I2(\L_bus_in_reg[17] [16]),
        .I3(\trigger_volt_reg[8] [6]),
        .O(\FSM_onehot_state[1]_i_12_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \FSM_onehot_state[1]_i_13 
       (.I0(\L_bus_in_reg[17] [15]),
        .I1(\trigger_volt_reg[8] [5]),
        .I2(\L_bus_in_reg[17] [14]),
        .I3(\trigger_volt_reg[8] [4]),
        .O(\FSM_onehot_state[1]_i_13_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \FSM_onehot_state[1]_i_14 
       (.I0(\L_bus_in_reg[17] [13]),
        .I1(\trigger_volt_reg[8] [3]),
        .I2(\L_bus_in_reg[17] [12]),
        .I3(\trigger_volt_reg[8] [2]),
        .O(\FSM_onehot_state[1]_i_14_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \FSM_onehot_state[1]_i_15 
       (.I0(\L_bus_in_reg[17] [11]),
        .I1(\trigger_volt_reg[8] [1]),
        .I2(\L_bus_in_reg[17] [10]),
        .I3(\trigger_volt_reg[8] [0]),
        .O(\FSM_onehot_state[1]_i_15_n_0 ));
  LUT4 #(
    .INIT(16'h1171)) 
    \FSM_onehot_state[1]_i_8 
       (.I0(\trigger_volt_reg[8] [7]),
        .I1(\L_bus_in_reg[17] [17]),
        .I2(\L_bus_in_reg[17] [16]),
        .I3(\trigger_volt_reg[8] [6]),
        .O(\FSM_onehot_state[1]_i_8_n_0 ));
  LUT4 #(
    .INIT(16'h44D4)) 
    \FSM_onehot_state[1]_i_9 
       (.I0(\trigger_volt_reg[8] [5]),
        .I1(\L_bus_in_reg[17] [15]),
        .I2(\L_bus_in_reg[17] [14]),
        .I3(\trigger_volt_reg[8] [4]),
        .O(\FSM_onehot_state[1]_i_9_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \FSM_onehot_state[2]_i_2 
       (.I0(CO),
        .I1(\trigger_volt_reg[9] ),
        .O(w_sw));
  CARRY4 \FSM_onehot_state_reg[1]_i_2 
       (.CI(\FSM_onehot_state_reg[1]_i_4_n_0 ),
        .CO({\NLW_FSM_onehot_state_reg[1]_i_2_CO_UNCONNECTED [3:1],CO}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_FSM_onehot_state_reg[1]_i_2_O_UNCONNECTED [3:0]),
        .S({1'b0,1'b0,1'b0,S}));
  CARRY4 \FSM_onehot_state_reg[1]_i_4 
       (.CI(1'b0),
        .CO({\FSM_onehot_state_reg[1]_i_4_n_0 ,\FSM_onehot_state_reg[1]_i_4_n_1 ,\FSM_onehot_state_reg[1]_i_4_n_2 ,\FSM_onehot_state_reg[1]_i_4_n_3 }),
        .CYINIT(1'b1),
        .DI({\FSM_onehot_state[1]_i_8_n_0 ,\FSM_onehot_state[1]_i_9_n_0 ,\FSM_onehot_state[1]_i_10_n_0 ,\FSM_onehot_state[1]_i_11_n_0 }),
        .O(\NLW_FSM_onehot_state_reg[1]_i_4_O_UNCONNECTED [3:0]),
        .S({\FSM_onehot_state[1]_i_12_n_0 ,\FSM_onehot_state[1]_i_13_n_0 ,\FSM_onehot_state[1]_i_14_n_0 ,\FSM_onehot_state[1]_i_15_n_0 }));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    LRCLK_i_1
       (.I0(Cnt_Lrclk[4]),
        .I1(LRCLK_i_2_n_0),
        .I2(Cnt_Bclk0),
        .I3(ac_bclk),
        .I4(ac_lrclk),
        .O(LRCLK_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    LRCLK_i_2
       (.I0(Cnt_Lrclk[3]),
        .I1(Cnt_Lrclk[1]),
        .I2(Cnt_Lrclk[0]),
        .I3(Cnt_Lrclk[2]),
        .O(LRCLK_i_2_n_0));
  FDRE LRCLK_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(LRCLK_i_1_n_0),
        .Q(ac_lrclk),
        .R(SR));
  LUT1 #(
    .INIT(2'h1)) 
    \UnL_bus_out2[17]_i_2 
       (.I0(\L_bus_in_reg[17] [17]),
        .O(D));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT3 #(
    .INIT(8'hD8)) 
    ac_lrclk_sig_prev_i_1
       (.I0(s00_axi_aresetn),
        .I1(ac_lrclk),
        .I2(ac_lrclk_sig_prev_reg_0),
        .O(ac_lrclk_sig_prev_reg));
  LUT2 #(
    .INIT(4'h1)) 
    i__carry_i_1
       (.I0(Cnt_Bclk_reg__0[3]),
        .I1(Cnt_Bclk_reg__0[4]),
        .O(i__carry_i_1_n_0));
  LUT3 #(
    .INIT(8'h04)) 
    i__carry_i_2__0
       (.I0(Cnt_Bclk_reg__0[2]),
        .I1(Cnt_Bclk_reg__0[0]),
        .I2(Cnt_Bclk_reg__0[1]),
        .O(i__carry_i_2__0_n_0));
  LUT6 #(
    .INIT(64'hE2A2A2A2A2A2A2A2)) 
    ready_sig_i_1
       (.I0(E),
        .I1(s00_axi_aresetn),
        .I2(ac_lrclk_count0),
        .I3(ac_lrclk_count[1]),
        .I4(ac_lrclk_count[0]),
        .I5(ac_lrclk_count[2]),
        .O(ready_sig_reg));
  LUT2 #(
    .INIT(4'h2)) 
    ready_sig_i_2
       (.I0(ac_lrclk),
        .I1(ac_lrclk_sig_prev_reg_0),
        .O(ac_lrclk_count0));
  LUT3 #(
    .INIT(8'hB8)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_10__0 
       (.I0(\slv_reg10_reg[15] [6]),
        .I1(switch),
        .I2(\R_bus_in_reg[17] [8]),
        .O(\sdp_bl.ramb18_dp_bl.ram18_bl [6]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_11__0 
       (.I0(\slv_reg10_reg[15] [5]),
        .I1(switch),
        .I2(\R_bus_in_reg[17] [7]),
        .O(\sdp_bl.ramb18_dp_bl.ram18_bl [5]));
  LUT3 #(
    .INIT(8'h8B)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_12 
       (.I0(\slv_reg9_reg[15] [15]),
        .I1(switch),
        .I2(\L_bus_in_reg[17] [17]),
        .O(DIBDI[15]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_12__0 
       (.I0(\slv_reg10_reg[15] [4]),
        .I1(switch),
        .I2(\R_bus_in_reg[17] [6]),
        .O(\sdp_bl.ramb18_dp_bl.ram18_bl [4]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_13 
       (.I0(\slv_reg9_reg[15] [14]),
        .I1(switch),
        .I2(\L_bus_in_reg[17] [16]),
        .O(DIBDI[14]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_13__0 
       (.I0(\slv_reg10_reg[15] [3]),
        .I1(switch),
        .I2(\R_bus_in_reg[17] [5]),
        .O(\sdp_bl.ramb18_dp_bl.ram18_bl [3]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_14 
       (.I0(\slv_reg9_reg[15] [13]),
        .I1(switch),
        .I2(\L_bus_in_reg[17] [15]),
        .O(DIBDI[13]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_14__0 
       (.I0(\slv_reg10_reg[15] [2]),
        .I1(switch),
        .I2(\R_bus_in_reg[17] [4]),
        .O(\sdp_bl.ramb18_dp_bl.ram18_bl [2]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_15 
       (.I0(\slv_reg9_reg[15] [12]),
        .I1(switch),
        .I2(\L_bus_in_reg[17] [14]),
        .O(DIBDI[12]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_15__0 
       (.I0(\slv_reg10_reg[15] [1]),
        .I1(switch),
        .I2(\R_bus_in_reg[17] [3]),
        .O(\sdp_bl.ramb18_dp_bl.ram18_bl [1]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_16 
       (.I0(\slv_reg9_reg[15] [11]),
        .I1(switch),
        .I2(\L_bus_in_reg[17] [13]),
        .O(DIBDI[11]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_16__0 
       (.I0(\slv_reg10_reg[15] [0]),
        .I1(switch),
        .I2(\R_bus_in_reg[17] [2]),
        .O(\sdp_bl.ramb18_dp_bl.ram18_bl [0]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_17 
       (.I0(\slv_reg9_reg[15] [10]),
        .I1(switch),
        .I2(\L_bus_in_reg[17] [12]),
        .O(DIBDI[10]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_18 
       (.I0(\slv_reg9_reg[15] [9]),
        .I1(switch),
        .I2(\L_bus_in_reg[17] [11]),
        .O(DIBDI[9]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_19 
       (.I0(\slv_reg9_reg[15] [8]),
        .I1(switch),
        .I2(\L_bus_in_reg[17] [10]),
        .O(DIBDI[8]));
  LUT3 #(
    .INIT(8'hA3)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_1__0 
       (.I0(\slv_reg10_reg[15] [15]),
        .I1(\R_bus_in_reg[17] [17]),
        .I2(switch),
        .O(\sdp_bl.ramb18_dp_bl.ram18_bl [15]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_20 
       (.I0(\slv_reg9_reg[15] [7]),
        .I1(switch),
        .I2(\L_bus_in_reg[17] [9]),
        .O(DIBDI[7]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_21 
       (.I0(\slv_reg9_reg[15] [6]),
        .I1(switch),
        .I2(\L_bus_in_reg[17] [8]),
        .O(DIBDI[6]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_22 
       (.I0(\slv_reg9_reg[15] [5]),
        .I1(switch),
        .I2(\L_bus_in_reg[17] [7]),
        .O(DIBDI[5]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_23 
       (.I0(\slv_reg9_reg[15] [4]),
        .I1(switch),
        .I2(\L_bus_in_reg[17] [6]),
        .O(DIBDI[4]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_24 
       (.I0(\slv_reg9_reg[15] [3]),
        .I1(switch),
        .I2(\L_bus_in_reg[17] [5]),
        .O(DIBDI[3]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_25 
       (.I0(\slv_reg9_reg[15] [2]),
        .I1(switch),
        .I2(\L_bus_in_reg[17] [4]),
        .O(DIBDI[2]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_26 
       (.I0(\slv_reg9_reg[15] [1]),
        .I1(switch),
        .I2(\L_bus_in_reg[17] [3]),
        .O(DIBDI[1]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_27 
       (.I0(\slv_reg9_reg[15] [0]),
        .I1(switch),
        .I2(\L_bus_in_reg[17] [2]),
        .O(DIBDI[0]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_2__0 
       (.I0(\slv_reg10_reg[15] [14]),
        .I1(switch),
        .I2(\R_bus_in_reg[17] [16]),
        .O(\sdp_bl.ramb18_dp_bl.ram18_bl [14]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_3__0 
       (.I0(\slv_reg10_reg[15] [13]),
        .I1(switch),
        .I2(\R_bus_in_reg[17] [15]),
        .O(\sdp_bl.ramb18_dp_bl.ram18_bl [13]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_4__0 
       (.I0(\slv_reg10_reg[15] [12]),
        .I1(switch),
        .I2(\R_bus_in_reg[17] [14]),
        .O(\sdp_bl.ramb18_dp_bl.ram18_bl [12]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_5__0 
       (.I0(\slv_reg10_reg[15] [11]),
        .I1(switch),
        .I2(\R_bus_in_reg[17] [13]),
        .O(\sdp_bl.ramb18_dp_bl.ram18_bl [11]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_6__0 
       (.I0(\slv_reg10_reg[15] [10]),
        .I1(switch),
        .I2(\R_bus_in_reg[17] [12]),
        .O(\sdp_bl.ramb18_dp_bl.ram18_bl [10]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_7__0 
       (.I0(\slv_reg10_reg[15] [9]),
        .I1(switch),
        .I2(\R_bus_in_reg[17] [11]),
        .O(\sdp_bl.ramb18_dp_bl.ram18_bl [9]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_8__0 
       (.I0(\slv_reg10_reg[15] [8]),
        .I1(switch),
        .I2(\R_bus_in_reg[17] [10]),
        .O(\sdp_bl.ramb18_dp_bl.ram18_bl [8]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_9__0 
       (.I0(\slv_reg10_reg[15] [7]),
        .I1(switch),
        .I2(\R_bus_in_reg[17] [9]),
        .O(\sdp_bl.ramb18_dp_bl.ram18_bl [7]));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_lab2_datapath
   (SS,
    tmds,
    tmdsb,
    ac_mclk,
    ac_bclk,
    ac_lrclk,
    w_sw,
    Q,
    \i_sig_reg[6]_0 ,
    CO,
    \FSM_onehot_state_reg[1] ,
    ac_dac_sdata,
    scl,
    sda,
    s00_axi_aclk,
    ac_adc_sdata,
    WREN,
    s00_axi_aresetn,
    btn,
    \slv_reg9_reg[15] ,
    switch,
    \slv_reg8_reg[9] ,
    \slv_reg10_reg[15] ,
    SR,
    E,
    \FSM_onehot_state_reg[4] );
  output [0:0]SS;
  output [3:0]tmds;
  output [3:0]tmdsb;
  output ac_mclk;
  output ac_bclk;
  output ac_lrclk;
  output [2:0]w_sw;
  output [3:0]Q;
  output \i_sig_reg[6]_0 ;
  output [0:0]CO;
  output [0:0]\FSM_onehot_state_reg[1] ;
  output ac_dac_sdata;
  inout scl;
  inout sda;
  input s00_axi_aclk;
  input ac_adc_sdata;
  input WREN;
  input s00_axi_aresetn;
  input [4:0]btn;
  input [15:0]\slv_reg9_reg[15] ;
  input [2:0]switch;
  input [9:0]\slv_reg8_reg[9] ;
  input [15:0]\slv_reg10_reg[15] ;
  input [0:0]SR;
  input [0:0]E;
  input [1:0]\FSM_onehot_state_reg[4] ;

  wire [9:0]AddrRead;
  wire [0:0]CO;
  wire [15:0]DI;
  wire [17:0]D_L_O;
  wire [17:0]D_R_O;
  wire [0:0]E;
  wire \FSM_onehot_state[1]_i_16_n_0 ;
  wire \FSM_onehot_state[1]_i_17_n_0 ;
  wire \FSM_onehot_state[1]_i_18_n_0 ;
  wire \FSM_onehot_state[1]_i_19_n_0 ;
  wire \FSM_onehot_state[1]_i_20_n_0 ;
  wire \FSM_onehot_state[1]_i_21_n_0 ;
  wire \FSM_onehot_state[1]_i_22_n_0 ;
  wire \FSM_onehot_state[1]_i_23_n_0 ;
  wire \FSM_onehot_state[1]_i_5_n_0 ;
  wire \FSM_onehot_state[1]_i_7_n_0 ;
  wire [0:0]\FSM_onehot_state_reg[1] ;
  wire \FSM_onehot_state_reg[1]_i_6_n_0 ;
  wire \FSM_onehot_state_reg[1]_i_6_n_1 ;
  wire \FSM_onehot_state_reg[1]_i_6_n_2 ;
  wire \FSM_onehot_state_reg[1]_i_6_n_3 ;
  wire [1:0]\FSM_onehot_state_reg[4] ;
  wire [17:0]L_bus_in;
  wire [3:0]Q;
  wire [17:0]R_bus_in;
  wire [0:0]SR;
  wire [0:0]SS;
  wire [17:17]UnL_bus_out;
  wire UnL_bus_out2;
  wire [9:0]WRADDR;
  wire WREN;
  wire ac_adc_sdata;
  wire ac_bclk;
  wire ac_dac_sdata;
  wire ac_lrclk;
  wire ac_mclk;
  wire [4:0]btn;
  wire \i_sig[0]_i_1_n_0 ;
  wire \i_sig[1]_i_1_n_0 ;
  wire \i_sig[2]_i_1_n_0 ;
  wire \i_sig[3]_i_1_n_0 ;
  wire \i_sig[4]_i_1_n_0 ;
  wire \i_sig[4]_i_2_n_0 ;
  wire \i_sig[5]_i_1_n_0 ;
  wire \i_sig[6]_i_1_n_0 ;
  wire \i_sig[7]_i_1_n_0 ;
  wire \i_sig[8]_i_1_n_0 ;
  wire \i_sig[9]_i_3_n_0 ;
  wire \i_sig_reg[6]_0 ;
  wire \i_sig_reg_n_0_[0] ;
  wire \i_sig_reg_n_0_[1] ;
  wire \i_sig_reg_n_0_[2] ;
  wire \i_sig_reg_n_0_[3] ;
  wire \i_sig_reg_n_0_[4] ;
  wire \i_sig_reg_n_0_[5] ;
  wire leftChannelMemory_n_5;
  wire leftChannelMemory_n_6;
  wire lopt;
  wire [4:0]old_button;
  wire [8:0]p_0_in;
  wire [9:1]p_0_in__1;
  wire [7:0]p_1_in;
  wire [15:8]readL;
  wire [15:14]readR;
  wire rightChannelMemory_n_12;
  wire rightChannelMemory_n_13;
  wire [5:0]row;
  wire s00_axi_aclk;
  wire s00_axi_aresetn;
  wire scl;
  wire sda;
  wire [15:0]\slv_reg10_reg[15] ;
  wire [9:0]\slv_reg8_reg[9] ;
  wire [15:0]\slv_reg9_reg[15] ;
  wire [2:0]switch;
  wire [3:0]tmds;
  wire [3:0]tmdsb;
  wire [8:0]trigger_time0_in;
  wire \trigger_time[2]_i_1_n_0 ;
  wire \trigger_time[3]_i_1_n_0 ;
  wire \trigger_time[4]_i_1_n_0 ;
  wire \trigger_time[4]_i_2_n_0 ;
  wire \trigger_time[4]_i_3_n_0 ;
  wire \trigger_time[5]_i_2_n_0 ;
  wire \trigger_time[5]_i_3_n_0 ;
  wire \trigger_time[6]_i_1_n_0 ;
  wire \trigger_time[7]_i_1_n_0 ;
  wire \trigger_time[7]_i_2_n_0 ;
  wire \trigger_time[7]_i_3_n_0 ;
  wire \trigger_time[8]_i_1_n_0 ;
  wire \trigger_time[8]_i_3_n_0 ;
  wire \trigger_time[8]_i_4_n_0 ;
  wire \trigger_time[8]_i_5_n_0 ;
  wire \trigger_time[8]_i_6_n_0 ;
  wire \trigger_time[8]_i_7_n_0 ;
  wire \trigger_time[8]_i_8_n_0 ;
  wire \trigger_time[8]_i_9_n_0 ;
  wire \trigger_time[9]_i_1_n_0 ;
  wire \trigger_time[9]_i_2_n_0 ;
  wire \trigger_time_reg_n_0_[0] ;
  wire \trigger_time_reg_n_0_[1] ;
  wire \trigger_time_reg_n_0_[2] ;
  wire \trigger_time_reg_n_0_[3] ;
  wire \trigger_time_reg_n_0_[4] ;
  wire \trigger_time_reg_n_0_[5] ;
  wire \trigger_time_reg_n_0_[6] ;
  wire \trigger_time_reg_n_0_[7] ;
  wire \trigger_time_reg_n_0_[8] ;
  wire \trigger_time_reg_n_0_[9] ;
  wire \trigger_volt[0]_i_1_n_0 ;
  wire \trigger_volt[3]_i_2_n_0 ;
  wire \trigger_volt[4]_i_2_n_0 ;
  wire \trigger_volt[4]_i_3_n_0 ;
  wire \trigger_volt[5]_i_2_n_0 ;
  wire \trigger_volt[5]_i_3_n_0 ;
  wire \trigger_volt[7]_i_2_n_0 ;
  wire \trigger_volt[7]_i_3_n_0 ;
  wire \trigger_volt[8]_i_2_n_0 ;
  wire \trigger_volt[8]_i_3_n_0 ;
  wire \trigger_volt[8]_i_4_n_0 ;
  wire \trigger_volt[8]_i_5_n_0 ;
  wire \trigger_volt[9]_i_1_n_0 ;
  wire \trigger_volt[9]_i_3_n_0 ;
  wire \trigger_volt[9]_i_4_n_0 ;
  wire \trigger_volt[9]_i_5_n_0 ;
  wire \trigger_volt_reg_n_0_[0] ;
  wire [2:0]w_sw;
  wire wrapper_inst_n_43;
  wire wrapper_inst_n_44;
  wire wrapper_inst_n_45;
  wire wrapper_inst_n_46;
  wire wrapper_inst_n_47;
  wire wrapper_inst_n_48;
  wire wrapper_inst_n_49;
  wire wrapper_inst_n_50;
  wire wrapper_inst_n_51;
  wire wrapper_inst_n_52;
  wire wrapper_inst_n_53;
  wire wrapper_inst_n_54;
  wire wrapper_inst_n_55;
  wire wrapper_inst_n_56;
  wire wrapper_inst_n_57;
  wire wrapper_inst_n_58;
  wire [3:1]\NLW_FSM_onehot_state_reg[1]_i_3_CO_UNCONNECTED ;
  wire [3:0]\NLW_FSM_onehot_state_reg[1]_i_3_O_UNCONNECTED ;
  wire [3:0]\NLW_FSM_onehot_state_reg[1]_i_6_O_UNCONNECTED ;

  LUT4 #(
    .INIT(16'h22B2)) 
    \FSM_onehot_state[1]_i_16 
       (.I0(p_0_in[7]),
        .I1(p_1_in[7]),
        .I2(p_0_in[6]),
        .I3(p_1_in[6]),
        .O(\FSM_onehot_state[1]_i_16_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \FSM_onehot_state[1]_i_17 
       (.I0(p_0_in[5]),
        .I1(p_1_in[5]),
        .I2(p_0_in[4]),
        .I3(p_1_in[4]),
        .O(\FSM_onehot_state[1]_i_17_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \FSM_onehot_state[1]_i_18 
       (.I0(p_0_in[3]),
        .I1(p_1_in[3]),
        .I2(p_0_in[2]),
        .I3(p_1_in[2]),
        .O(\FSM_onehot_state[1]_i_18_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \FSM_onehot_state[1]_i_19 
       (.I0(p_0_in[1]),
        .I1(p_1_in[1]),
        .I2(p_0_in[0]),
        .I3(p_1_in[0]),
        .O(\FSM_onehot_state[1]_i_19_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \FSM_onehot_state[1]_i_20 
       (.I0(p_1_in[7]),
        .I1(p_0_in[7]),
        .I2(p_1_in[6]),
        .I3(p_0_in[6]),
        .O(\FSM_onehot_state[1]_i_20_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \FSM_onehot_state[1]_i_21 
       (.I0(p_1_in[5]),
        .I1(p_0_in[5]),
        .I2(p_1_in[4]),
        .I3(p_0_in[4]),
        .O(\FSM_onehot_state[1]_i_21_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \FSM_onehot_state[1]_i_22 
       (.I0(p_1_in[3]),
        .I1(p_0_in[3]),
        .I2(p_1_in[2]),
        .I3(p_0_in[2]),
        .O(\FSM_onehot_state[1]_i_22_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \FSM_onehot_state[1]_i_23 
       (.I0(p_1_in[1]),
        .I1(p_0_in[1]),
        .I2(p_1_in[0]),
        .I3(p_0_in[0]),
        .O(\FSM_onehot_state[1]_i_23_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \FSM_onehot_state[1]_i_5 
       (.I0(p_0_in[8]),
        .O(\FSM_onehot_state[1]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \FSM_onehot_state[1]_i_7 
       (.I0(p_0_in[8]),
        .O(\FSM_onehot_state[1]_i_7_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair82" *) 
  LUT5 #(
    .INIT(32'h7FFFFFFF)) 
    \FSM_onehot_state[2]_i_3 
       (.I0(Q[3]),
        .I1(Q[1]),
        .I2(\i_sig_reg[6]_0 ),
        .I3(Q[0]),
        .I4(Q[2]),
        .O(w_sw[2]));
  CARRY4 \FSM_onehot_state_reg[1]_i_3 
       (.CI(\FSM_onehot_state_reg[1]_i_6_n_0 ),
        .CO({\NLW_FSM_onehot_state_reg[1]_i_3_CO_UNCONNECTED [3:1],\FSM_onehot_state_reg[1] }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,p_0_in[8]}),
        .O(\NLW_FSM_onehot_state_reg[1]_i_3_O_UNCONNECTED [3:0]),
        .S({1'b0,1'b0,1'b0,\FSM_onehot_state[1]_i_7_n_0 }));
  CARRY4 \FSM_onehot_state_reg[1]_i_6 
       (.CI(1'b0),
        .CO({\FSM_onehot_state_reg[1]_i_6_n_0 ,\FSM_onehot_state_reg[1]_i_6_n_1 ,\FSM_onehot_state_reg[1]_i_6_n_2 ,\FSM_onehot_state_reg[1]_i_6_n_3 }),
        .CYINIT(1'b1),
        .DI({\FSM_onehot_state[1]_i_16_n_0 ,\FSM_onehot_state[1]_i_17_n_0 ,\FSM_onehot_state[1]_i_18_n_0 ,\FSM_onehot_state[1]_i_19_n_0 }),
        .O(\NLW_FSM_onehot_state_reg[1]_i_6_O_UNCONNECTED [3:0]),
        .S({\FSM_onehot_state[1]_i_20_n_0 ,\FSM_onehot_state[1]_i_21_n_0 ,\FSM_onehot_state[1]_i_22_n_0 ,\FSM_onehot_state[1]_i_23_n_0 }));
  FDRE \L_bus_in_reg[0] 
       (.C(s00_axi_aclk),
        .CE(w_sw[1]),
        .D(D_L_O[0]),
        .Q(L_bus_in[0]),
        .R(SS));
  FDRE \L_bus_in_reg[10] 
       (.C(s00_axi_aclk),
        .CE(w_sw[1]),
        .D(D_L_O[10]),
        .Q(L_bus_in[10]),
        .R(SS));
  FDRE \L_bus_in_reg[11] 
       (.C(s00_axi_aclk),
        .CE(w_sw[1]),
        .D(D_L_O[11]),
        .Q(L_bus_in[11]),
        .R(SS));
  FDRE \L_bus_in_reg[12] 
       (.C(s00_axi_aclk),
        .CE(w_sw[1]),
        .D(D_L_O[12]),
        .Q(L_bus_in[12]),
        .R(SS));
  FDRE \L_bus_in_reg[13] 
       (.C(s00_axi_aclk),
        .CE(w_sw[1]),
        .D(D_L_O[13]),
        .Q(L_bus_in[13]),
        .R(SS));
  FDRE \L_bus_in_reg[14] 
       (.C(s00_axi_aclk),
        .CE(w_sw[1]),
        .D(D_L_O[14]),
        .Q(L_bus_in[14]),
        .R(SS));
  FDRE \L_bus_in_reg[15] 
       (.C(s00_axi_aclk),
        .CE(w_sw[1]),
        .D(D_L_O[15]),
        .Q(L_bus_in[15]),
        .R(SS));
  FDRE \L_bus_in_reg[16] 
       (.C(s00_axi_aclk),
        .CE(w_sw[1]),
        .D(D_L_O[16]),
        .Q(L_bus_in[16]),
        .R(SS));
  FDRE \L_bus_in_reg[17] 
       (.C(s00_axi_aclk),
        .CE(w_sw[1]),
        .D(D_L_O[17]),
        .Q(L_bus_in[17]),
        .R(SS));
  FDRE \L_bus_in_reg[1] 
       (.C(s00_axi_aclk),
        .CE(w_sw[1]),
        .D(D_L_O[1]),
        .Q(L_bus_in[1]),
        .R(SS));
  FDRE \L_bus_in_reg[2] 
       (.C(s00_axi_aclk),
        .CE(w_sw[1]),
        .D(D_L_O[2]),
        .Q(L_bus_in[2]),
        .R(SS));
  FDRE \L_bus_in_reg[3] 
       (.C(s00_axi_aclk),
        .CE(w_sw[1]),
        .D(D_L_O[3]),
        .Q(L_bus_in[3]),
        .R(SS));
  FDRE \L_bus_in_reg[4] 
       (.C(s00_axi_aclk),
        .CE(w_sw[1]),
        .D(D_L_O[4]),
        .Q(L_bus_in[4]),
        .R(SS));
  FDRE \L_bus_in_reg[5] 
       (.C(s00_axi_aclk),
        .CE(w_sw[1]),
        .D(D_L_O[5]),
        .Q(L_bus_in[5]),
        .R(SS));
  FDRE \L_bus_in_reg[6] 
       (.C(s00_axi_aclk),
        .CE(w_sw[1]),
        .D(D_L_O[6]),
        .Q(L_bus_in[6]),
        .R(SS));
  FDRE \L_bus_in_reg[7] 
       (.C(s00_axi_aclk),
        .CE(w_sw[1]),
        .D(D_L_O[7]),
        .Q(L_bus_in[7]),
        .R(SS));
  FDRE \L_bus_in_reg[8] 
       (.C(s00_axi_aclk),
        .CE(w_sw[1]),
        .D(D_L_O[8]),
        .Q(L_bus_in[8]),
        .R(SS));
  FDRE \L_bus_in_reg[9] 
       (.C(s00_axi_aclk),
        .CE(w_sw[1]),
        .D(D_L_O[9]),
        .Q(L_bus_in[9]),
        .R(SS));
  FDRE \R_bus_in_reg[0] 
       (.C(s00_axi_aclk),
        .CE(w_sw[1]),
        .D(D_R_O[0]),
        .Q(R_bus_in[0]),
        .R(SS));
  FDRE \R_bus_in_reg[10] 
       (.C(s00_axi_aclk),
        .CE(w_sw[1]),
        .D(D_R_O[10]),
        .Q(R_bus_in[10]),
        .R(SS));
  FDRE \R_bus_in_reg[11] 
       (.C(s00_axi_aclk),
        .CE(w_sw[1]),
        .D(D_R_O[11]),
        .Q(R_bus_in[11]),
        .R(SS));
  FDRE \R_bus_in_reg[12] 
       (.C(s00_axi_aclk),
        .CE(w_sw[1]),
        .D(D_R_O[12]),
        .Q(R_bus_in[12]),
        .R(SS));
  FDRE \R_bus_in_reg[13] 
       (.C(s00_axi_aclk),
        .CE(w_sw[1]),
        .D(D_R_O[13]),
        .Q(R_bus_in[13]),
        .R(SS));
  FDRE \R_bus_in_reg[14] 
       (.C(s00_axi_aclk),
        .CE(w_sw[1]),
        .D(D_R_O[14]),
        .Q(R_bus_in[14]),
        .R(SS));
  FDRE \R_bus_in_reg[15] 
       (.C(s00_axi_aclk),
        .CE(w_sw[1]),
        .D(D_R_O[15]),
        .Q(R_bus_in[15]),
        .R(SS));
  FDRE \R_bus_in_reg[16] 
       (.C(s00_axi_aclk),
        .CE(w_sw[1]),
        .D(D_R_O[16]),
        .Q(R_bus_in[16]),
        .R(SS));
  FDRE \R_bus_in_reg[17] 
       (.C(s00_axi_aclk),
        .CE(w_sw[1]),
        .D(D_R_O[17]),
        .Q(R_bus_in[17]),
        .R(SS));
  FDRE \R_bus_in_reg[1] 
       (.C(s00_axi_aclk),
        .CE(w_sw[1]),
        .D(D_R_O[1]),
        .Q(R_bus_in[1]),
        .R(SS));
  FDRE \R_bus_in_reg[2] 
       (.C(s00_axi_aclk),
        .CE(w_sw[1]),
        .D(D_R_O[2]),
        .Q(R_bus_in[2]),
        .R(SS));
  FDRE \R_bus_in_reg[3] 
       (.C(s00_axi_aclk),
        .CE(w_sw[1]),
        .D(D_R_O[3]),
        .Q(R_bus_in[3]),
        .R(SS));
  FDRE \R_bus_in_reg[4] 
       (.C(s00_axi_aclk),
        .CE(w_sw[1]),
        .D(D_R_O[4]),
        .Q(R_bus_in[4]),
        .R(SS));
  FDRE \R_bus_in_reg[5] 
       (.C(s00_axi_aclk),
        .CE(w_sw[1]),
        .D(D_R_O[5]),
        .Q(R_bus_in[5]),
        .R(SS));
  FDRE \R_bus_in_reg[6] 
       (.C(s00_axi_aclk),
        .CE(w_sw[1]),
        .D(D_R_O[6]),
        .Q(R_bus_in[6]),
        .R(SS));
  FDRE \R_bus_in_reg[7] 
       (.C(s00_axi_aclk),
        .CE(w_sw[1]),
        .D(D_R_O[7]),
        .Q(R_bus_in[7]),
        .R(SS));
  FDRE \R_bus_in_reg[8] 
       (.C(s00_axi_aclk),
        .CE(w_sw[1]),
        .D(D_R_O[8]),
        .Q(R_bus_in[8]),
        .R(SS));
  FDRE \R_bus_in_reg[9] 
       (.C(s00_axi_aclk),
        .CE(w_sw[1]),
        .D(D_R_O[9]),
        .Q(R_bus_in[9]),
        .R(SS));
  FDRE \UnL_bus_out2_reg[10] 
       (.C(s00_axi_aclk),
        .CE(UnL_bus_out2),
        .D(D_L_O[10]),
        .Q(p_1_in[0]),
        .R(1'b0));
  FDRE \UnL_bus_out2_reg[11] 
       (.C(s00_axi_aclk),
        .CE(UnL_bus_out2),
        .D(D_L_O[11]),
        .Q(p_1_in[1]),
        .R(1'b0));
  FDRE \UnL_bus_out2_reg[12] 
       (.C(s00_axi_aclk),
        .CE(UnL_bus_out2),
        .D(D_L_O[12]),
        .Q(p_1_in[2]),
        .R(1'b0));
  FDRE \UnL_bus_out2_reg[13] 
       (.C(s00_axi_aclk),
        .CE(UnL_bus_out2),
        .D(D_L_O[13]),
        .Q(p_1_in[3]),
        .R(1'b0));
  FDRE \UnL_bus_out2_reg[14] 
       (.C(s00_axi_aclk),
        .CE(UnL_bus_out2),
        .D(D_L_O[14]),
        .Q(p_1_in[4]),
        .R(1'b0));
  FDRE \UnL_bus_out2_reg[15] 
       (.C(s00_axi_aclk),
        .CE(UnL_bus_out2),
        .D(D_L_O[15]),
        .Q(p_1_in[5]),
        .R(1'b0));
  FDRE \UnL_bus_out2_reg[16] 
       (.C(s00_axi_aclk),
        .CE(UnL_bus_out2),
        .D(D_L_O[16]),
        .Q(p_1_in[6]),
        .R(1'b0));
  FDRE \UnL_bus_out2_reg[17] 
       (.C(s00_axi_aclk),
        .CE(UnL_bus_out2),
        .D(UnL_bus_out),
        .Q(p_1_in[7]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair92" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \i_sig[0]_i_1 
       (.I0(\i_sig_reg_n_0_[0] ),
        .O(\i_sig[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair92" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \i_sig[1]_i_1 
       (.I0(\i_sig_reg_n_0_[1] ),
        .I1(\i_sig_reg_n_0_[0] ),
        .O(\i_sig[1]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h0F7F0080)) 
    \i_sig[2]_i_1 
       (.I0(\i_sig_reg_n_0_[1] ),
        .I1(\i_sig_reg_n_0_[0] ),
        .I2(\FSM_onehot_state_reg[4] [0]),
        .I3(\FSM_onehot_state_reg[4] [1]),
        .I4(\i_sig_reg_n_0_[2] ),
        .O(\i_sig[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair83" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \i_sig[3]_i_1 
       (.I0(\i_sig_reg_n_0_[3] ),
        .I1(\i_sig_reg_n_0_[1] ),
        .I2(\i_sig_reg_n_0_[0] ),
        .I3(\i_sig_reg_n_0_[2] ),
        .O(\i_sig[3]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h3708)) 
    \i_sig[4]_i_1 
       (.I0(\i_sig[4]_i_2_n_0 ),
        .I1(\FSM_onehot_state_reg[4] [0]),
        .I2(\FSM_onehot_state_reg[4] [1]),
        .I3(\i_sig_reg_n_0_[4] ),
        .O(\i_sig[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair83" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \i_sig[4]_i_2 
       (.I0(\i_sig_reg_n_0_[3] ),
        .I1(\i_sig_reg_n_0_[1] ),
        .I2(\i_sig_reg_n_0_[0] ),
        .I3(\i_sig_reg_n_0_[2] ),
        .O(\i_sig[4]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \i_sig[5]_i_1 
       (.I0(\i_sig_reg_n_0_[5] ),
        .I1(\i_sig_reg_n_0_[3] ),
        .I2(\i_sig_reg_n_0_[1] ),
        .I3(\i_sig_reg_n_0_[0] ),
        .I4(\i_sig_reg_n_0_[2] ),
        .I5(\i_sig_reg_n_0_[4] ),
        .O(\i_sig[5]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \i_sig[6]_i_1 
       (.I0(Q[0]),
        .I1(\i_sig_reg[6]_0 ),
        .O(\i_sig[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair86" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \i_sig[7]_i_1 
       (.I0(Q[1]),
        .I1(\i_sig_reg[6]_0 ),
        .I2(Q[0]),
        .O(\i_sig[7]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair86" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \i_sig[8]_i_1 
       (.I0(Q[2]),
        .I1(Q[0]),
        .I2(\i_sig_reg[6]_0 ),
        .I3(Q[1]),
        .O(\i_sig[8]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair82" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \i_sig[9]_i_3 
       (.I0(Q[3]),
        .I1(Q[1]),
        .I2(\i_sig_reg[6]_0 ),
        .I3(Q[0]),
        .I4(Q[2]),
        .O(\i_sig[9]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \i_sig[9]_i_4 
       (.I0(\i_sig_reg_n_0_[5] ),
        .I1(\i_sig_reg_n_0_[3] ),
        .I2(\i_sig_reg_n_0_[1] ),
        .I3(\i_sig_reg_n_0_[0] ),
        .I4(\i_sig_reg_n_0_[2] ),
        .I5(\i_sig_reg_n_0_[4] ),
        .O(\i_sig_reg[6]_0 ));
  FDRE \i_sig_reg[0] 
       (.C(s00_axi_aclk),
        .CE(E),
        .D(\i_sig[0]_i_1_n_0 ),
        .Q(\i_sig_reg_n_0_[0] ),
        .R(SR));
  FDRE \i_sig_reg[1] 
       (.C(s00_axi_aclk),
        .CE(E),
        .D(\i_sig[1]_i_1_n_0 ),
        .Q(\i_sig_reg_n_0_[1] ),
        .R(SR));
  FDSE \i_sig_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\i_sig[2]_i_1_n_0 ),
        .Q(\i_sig_reg_n_0_[2] ),
        .S(SS));
  FDRE \i_sig_reg[3] 
       (.C(s00_axi_aclk),
        .CE(E),
        .D(\i_sig[3]_i_1_n_0 ),
        .Q(\i_sig_reg_n_0_[3] ),
        .R(SR));
  FDSE \i_sig_reg[4] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\i_sig[4]_i_1_n_0 ),
        .Q(\i_sig_reg_n_0_[4] ),
        .S(SS));
  FDRE \i_sig_reg[5] 
       (.C(s00_axi_aclk),
        .CE(E),
        .D(\i_sig[5]_i_1_n_0 ),
        .Q(\i_sig_reg_n_0_[5] ),
        .R(SR));
  FDRE \i_sig_reg[6] 
       (.C(s00_axi_aclk),
        .CE(E),
        .D(\i_sig[6]_i_1_n_0 ),
        .Q(Q[0]),
        .R(SR));
  FDRE \i_sig_reg[7] 
       (.C(s00_axi_aclk),
        .CE(E),
        .D(\i_sig[7]_i_1_n_0 ),
        .Q(Q[1]),
        .R(SR));
  FDRE \i_sig_reg[8] 
       (.C(s00_axi_aclk),
        .CE(E),
        .D(\i_sig[8]_i_1_n_0 ),
        .Q(Q[2]),
        .R(SR));
  FDRE \i_sig_reg[9] 
       (.C(s00_axi_aclk),
        .CE(E),
        .D(\i_sig[9]_i_3_n_0 ),
        .Q(Q[3]),
        .R(SR));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_unimacro_BRAM_SDP_MACRO leftChannelMemory
       (.ADDRBWRADDR(WRADDR),
        .DIBDI(DI),
        .DOADO({readL[15:14],readL[10:8]}),
        .Q(AddrRead),
        .S(leftChannelMemory_n_5),
        .WREN(WREN),
        .\encoded_reg[8] (leftChannelMemory_n_6),
        .\processRow_reg[5] (row[5:3]),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_aresetn(SS));
  FDRE \old_button_reg[0] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(btn[0]),
        .Q(old_button[0]),
        .R(1'b0));
  FDRE \old_button_reg[1] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(btn[1]),
        .Q(old_button[1]),
        .R(1'b0));
  FDRE \old_button_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(btn[2]),
        .Q(old_button[2]),
        .R(1'b0));
  FDRE \old_button_reg[3] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(btn[3]),
        .Q(old_button[3]),
        .R(1'b0));
  FDRE \old_button_reg[4] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(btn[4]),
        .Q(old_button[4]),
        .R(1'b0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_unimacro_BRAM_SDP_MACRO__parameterized0 rightChannelMemory
       (.ADDRBWRADDR(WRADDR),
        .DIBDI({wrapper_inst_n_43,wrapper_inst_n_44,wrapper_inst_n_45,wrapper_inst_n_46,wrapper_inst_n_47,wrapper_inst_n_48,wrapper_inst_n_49,wrapper_inst_n_50,wrapper_inst_n_51,wrapper_inst_n_52,wrapper_inst_n_53,wrapper_inst_n_54,wrapper_inst_n_55,wrapper_inst_n_56,wrapper_inst_n_57,wrapper_inst_n_58}),
        .DOADO(readR),
        .Q(AddrRead),
        .S({rightChannelMemory_n_12,rightChannelMemory_n_13}),
        .WREN(WREN),
        .\i_sig_reg[2] (\i_sig_reg_n_0_[2] ),
        .\i_sig_reg[4] (\i_sig_reg_n_0_[4] ),
        .\i_sig_reg[9] ({Q,\i_sig_reg_n_0_[5] ,\i_sig_reg_n_0_[3] ,\i_sig_reg_n_0_[1] ,\i_sig_reg_n_0_[0] }),
        .\processRow_reg[5] (row),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_aresetn(SS),
        .\slv_reg8_reg[9] (\slv_reg8_reg[9] ),
        .switch(switch[2]));
  LUT6 #(
    .INIT(64'h0000000000000F04)) 
    \trigger_time[0]_i_1 
       (.I0(btn[2]),
        .I1(btn[3]),
        .I2(\trigger_time[8]_i_3_n_0 ),
        .I3(btn[1]),
        .I4(btn[0]),
        .I5(\trigger_time_reg_n_0_[0] ),
        .O(trigger_time0_in[0]));
  LUT5 #(
    .INIT(32'hFFFF417D)) 
    \trigger_time[1]_i_1 
       (.I0(\trigger_time[8]_i_8_n_0 ),
        .I1(\trigger_time_reg_n_0_[0] ),
        .I2(\trigger_time_reg_n_0_[1] ),
        .I3(\trigger_time[8]_i_5_n_0 ),
        .I4(\trigger_time[8]_i_4_n_0 ),
        .O(trigger_time0_in[1]));
  LUT5 #(
    .INIT(32'hCA0E0EAC)) 
    \trigger_time[2]_i_1 
       (.I0(\trigger_time[8]_i_5_n_0 ),
        .I1(\trigger_time[8]_i_8_n_0 ),
        .I2(\trigger_time_reg_n_0_[2] ),
        .I3(\trigger_time_reg_n_0_[0] ),
        .I4(\trigger_time_reg_n_0_[1] ),
        .O(\trigger_time[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hEEE33BBB22222888)) 
    \trigger_time[3]_i_1 
       (.I0(\trigger_time[8]_i_8_n_0 ),
        .I1(\trigger_time_reg_n_0_[3] ),
        .I2(\trigger_time_reg_n_0_[1] ),
        .I3(\trigger_time_reg_n_0_[0] ),
        .I4(\trigger_time_reg_n_0_[2] ),
        .I5(\trigger_time[8]_i_5_n_0 ),
        .O(\trigger_time[3]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h4FF84848)) 
    \trigger_time[4]_i_1 
       (.I0(\trigger_time[4]_i_2_n_0 ),
        .I1(\trigger_time[8]_i_8_n_0 ),
        .I2(\trigger_time_reg_n_0_[4] ),
        .I3(\trigger_time[4]_i_3_n_0 ),
        .I4(\trigger_time[8]_i_5_n_0 ),
        .O(\trigger_time[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair87" *) 
  LUT4 #(
    .INIT(16'hAA80)) 
    \trigger_time[4]_i_2 
       (.I0(\trigger_time_reg_n_0_[3] ),
        .I1(\trigger_time_reg_n_0_[1] ),
        .I2(\trigger_time_reg_n_0_[0] ),
        .I3(\trigger_time_reg_n_0_[2] ),
        .O(\trigger_time[4]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair87" *) 
  LUT4 #(
    .INIT(16'h0155)) 
    \trigger_time[4]_i_3 
       (.I0(\trigger_time_reg_n_0_[3] ),
        .I1(\trigger_time_reg_n_0_[1] ),
        .I2(\trigger_time_reg_n_0_[0] ),
        .I3(\trigger_time_reg_n_0_[2] ),
        .O(\trigger_time[4]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAAFBBFFBBFAABFFB)) 
    \trigger_time[5]_i_1 
       (.I0(\trigger_time[8]_i_4_n_0 ),
        .I1(\trigger_time[8]_i_5_n_0 ),
        .I2(\trigger_time[5]_i_2_n_0 ),
        .I3(\trigger_time_reg_n_0_[5] ),
        .I4(\trigger_time[8]_i_8_n_0 ),
        .I5(\trigger_time[5]_i_3_n_0 ),
        .O(trigger_time0_in[5]));
  (* SOFT_HLUTNM = "soft_lutpair80" *) 
  LUT5 #(
    .INIT(32'h00000057)) 
    \trigger_time[5]_i_2 
       (.I0(\trigger_time_reg_n_0_[2] ),
        .I1(\trigger_time_reg_n_0_[0] ),
        .I2(\trigger_time_reg_n_0_[1] ),
        .I3(\trigger_time_reg_n_0_[3] ),
        .I4(\trigger_time_reg_n_0_[4] ),
        .O(\trigger_time[5]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair80" *) 
  LUT5 #(
    .INIT(32'hEA000000)) 
    \trigger_time[5]_i_3 
       (.I0(\trigger_time_reg_n_0_[2] ),
        .I1(\trigger_time_reg_n_0_[0] ),
        .I2(\trigger_time_reg_n_0_[1] ),
        .I3(\trigger_time_reg_n_0_[3] ),
        .I4(\trigger_time_reg_n_0_[4] ),
        .O(\trigger_time[5]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h4FF84848)) 
    \trigger_time[6]_i_1 
       (.I0(\trigger_time[7]_i_2_n_0 ),
        .I1(\trigger_time[8]_i_8_n_0 ),
        .I2(\trigger_time_reg_n_0_[6] ),
        .I3(\trigger_time[7]_i_3_n_0 ),
        .I4(\trigger_time[8]_i_5_n_0 ),
        .O(\trigger_time[6]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hF8CFF8F048C048C0)) 
    \trigger_time[7]_i_1 
       (.I0(\trigger_time[7]_i_2_n_0 ),
        .I1(\trigger_time[8]_i_8_n_0 ),
        .I2(\trigger_time_reg_n_0_[7] ),
        .I3(\trigger_time_reg_n_0_[6] ),
        .I4(\trigger_time[7]_i_3_n_0 ),
        .I5(\trigger_time[8]_i_5_n_0 ),
        .O(\trigger_time[7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8888800000000000)) 
    \trigger_time[7]_i_2 
       (.I0(\trigger_time_reg_n_0_[4] ),
        .I1(\trigger_time_reg_n_0_[3] ),
        .I2(\trigger_time_reg_n_0_[1] ),
        .I3(\trigger_time_reg_n_0_[0] ),
        .I4(\trigger_time_reg_n_0_[2] ),
        .I5(\trigger_time_reg_n_0_[5] ),
        .O(\trigger_time[7]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000011111)) 
    \trigger_time[7]_i_3 
       (.I0(\trigger_time_reg_n_0_[4] ),
        .I1(\trigger_time_reg_n_0_[3] ),
        .I2(\trigger_time_reg_n_0_[1] ),
        .I3(\trigger_time_reg_n_0_[0] ),
        .I4(\trigger_time_reg_n_0_[2] ),
        .I5(\trigger_time_reg_n_0_[5] ),
        .O(\trigger_time[7]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h00000F04FFFFFFFF)) 
    \trigger_time[8]_i_1 
       (.I0(btn[2]),
        .I1(btn[3]),
        .I2(\trigger_time[8]_i_3_n_0 ),
        .I3(btn[1]),
        .I4(btn[0]),
        .I5(s00_axi_aresetn),
        .O(\trigger_time[8]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAFBBFAABFFBBFFB)) 
    \trigger_time[8]_i_2 
       (.I0(\trigger_time[8]_i_4_n_0 ),
        .I1(\trigger_time[8]_i_5_n_0 ),
        .I2(\trigger_time[8]_i_6_n_0 ),
        .I3(\trigger_time_reg_n_0_[8] ),
        .I4(\trigger_time[8]_i_7_n_0 ),
        .I5(\trigger_time[8]_i_8_n_0 ),
        .O(trigger_time0_in[8]));
  LUT5 #(
    .INIT(32'h41000041)) 
    \trigger_time[8]_i_3 
       (.I0(\trigger_time[8]_i_9_n_0 ),
        .I1(old_button[4]),
        .I2(btn[4]),
        .I3(btn[3]),
        .I4(old_button[3]),
        .O(\trigger_time[8]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair91" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \trigger_time[8]_i_4 
       (.I0(btn[0]),
        .I1(\trigger_time[8]_i_3_n_0 ),
        .O(\trigger_time[8]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair91" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \trigger_time[8]_i_5 
       (.I0(btn[1]),
        .I1(\trigger_time[8]_i_3_n_0 ),
        .O(\trigger_time[8]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair88" *) 
  LUT3 #(
    .INIT(8'h04)) 
    \trigger_time[8]_i_6 
       (.I0(\trigger_time_reg_n_0_[6] ),
        .I1(\trigger_time[7]_i_3_n_0 ),
        .I2(\trigger_time_reg_n_0_[7] ),
        .O(\trigger_time[8]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair88" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \trigger_time[8]_i_7 
       (.I0(\trigger_time_reg_n_0_[6] ),
        .I1(\trigger_time[7]_i_2_n_0 ),
        .I2(\trigger_time_reg_n_0_[7] ),
        .O(\trigger_time[8]_i_7_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair84" *) 
  LUT4 #(
    .INIT(16'h0010)) 
    \trigger_time[8]_i_8 
       (.I0(btn[1]),
        .I1(btn[2]),
        .I2(btn[3]),
        .I3(\trigger_time[8]_i_3_n_0 ),
        .O(\trigger_time[8]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'h6FF6FFFFFFFF6FF6)) 
    \trigger_time[8]_i_9 
       (.I0(btn[0]),
        .I1(old_button[0]),
        .I2(btn[2]),
        .I3(old_button[2]),
        .I4(btn[1]),
        .I5(old_button[1]),
        .O(\trigger_time[8]_i_9_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \trigger_time[9]_i_1 
       (.I0(\trigger_time[8]_i_4_n_0 ),
        .I1(\trigger_time[8]_i_1_n_0 ),
        .O(\trigger_time[9]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hCFFFF088CC440088)) 
    \trigger_time[9]_i_2 
       (.I0(\trigger_time[8]_i_6_n_0 ),
        .I1(\trigger_time[8]_i_5_n_0 ),
        .I2(\trigger_time[8]_i_7_n_0 ),
        .I3(\trigger_time_reg_n_0_[8] ),
        .I4(\trigger_time_reg_n_0_[9] ),
        .I5(\trigger_time[8]_i_8_n_0 ),
        .O(\trigger_time[9]_i_2_n_0 ));
  FDRE \trigger_time_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\trigger_time[8]_i_1_n_0 ),
        .D(trigger_time0_in[0]),
        .Q(\trigger_time_reg_n_0_[0] ),
        .R(1'b0));
  FDRE \trigger_time_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\trigger_time[8]_i_1_n_0 ),
        .D(trigger_time0_in[1]),
        .Q(\trigger_time_reg_n_0_[1] ),
        .R(1'b0));
  FDRE \trigger_time_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\trigger_time[8]_i_1_n_0 ),
        .D(\trigger_time[2]_i_1_n_0 ),
        .Q(\trigger_time_reg_n_0_[2] ),
        .R(\trigger_time[9]_i_1_n_0 ));
  FDRE \trigger_time_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\trigger_time[8]_i_1_n_0 ),
        .D(\trigger_time[3]_i_1_n_0 ),
        .Q(\trigger_time_reg_n_0_[3] ),
        .R(\trigger_time[9]_i_1_n_0 ));
  FDRE \trigger_time_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\trigger_time[8]_i_1_n_0 ),
        .D(\trigger_time[4]_i_1_n_0 ),
        .Q(\trigger_time_reg_n_0_[4] ),
        .R(\trigger_time[9]_i_1_n_0 ));
  FDRE \trigger_time_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\trigger_time[8]_i_1_n_0 ),
        .D(trigger_time0_in[5]),
        .Q(\trigger_time_reg_n_0_[5] ),
        .R(1'b0));
  FDRE \trigger_time_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\trigger_time[8]_i_1_n_0 ),
        .D(\trigger_time[6]_i_1_n_0 ),
        .Q(\trigger_time_reg_n_0_[6] ),
        .R(\trigger_time[9]_i_1_n_0 ));
  FDRE \trigger_time_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\trigger_time[8]_i_1_n_0 ),
        .D(\trigger_time[7]_i_1_n_0 ),
        .Q(\trigger_time_reg_n_0_[7] ),
        .R(\trigger_time[9]_i_1_n_0 ));
  FDRE \trigger_time_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\trigger_time[8]_i_1_n_0 ),
        .D(trigger_time0_in[8]),
        .Q(\trigger_time_reg_n_0_[8] ),
        .R(1'b0));
  FDRE \trigger_time_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\trigger_time[8]_i_1_n_0 ),
        .D(\trigger_time[9]_i_2_n_0 ),
        .Q(\trigger_time_reg_n_0_[9] ),
        .R(\trigger_time[9]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair79" *) 
  LUT5 #(
    .INIT(32'hDDCDFFFF)) 
    \trigger_volt[0]_i_1 
       (.I0(btn[0]),
        .I1(\trigger_time[8]_i_3_n_0 ),
        .I2(btn[2]),
        .I3(btn[1]),
        .I4(\trigger_volt_reg_n_0_[0] ),
        .O(\trigger_volt[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair78" *) 
  LUT4 #(
    .INIT(16'hEB28)) 
    \trigger_volt[1]_i_1 
       (.I0(\trigger_volt[9]_i_4_n_0 ),
        .I1(\trigger_volt_reg_n_0_[0] ),
        .I2(p_0_in[0]),
        .I3(\trigger_time[8]_i_4_n_0 ),
        .O(p_0_in__1[1]));
  (* SOFT_HLUTNM = "soft_lutpair78" *) 
  LUT5 #(
    .INIT(32'hB33E8222)) 
    \trigger_volt[2]_i_1 
       (.I0(\trigger_volt[9]_i_4_n_0 ),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(\trigger_volt_reg_n_0_[0] ),
        .I4(\trigger_time[8]_i_4_n_0 ),
        .O(p_0_in__1[2]));
  LUT6 #(
    .INIT(64'hE3E3E32F2F3E3E3E)) 
    \trigger_volt[3]_i_1 
       (.I0(\trigger_volt[3]_i_2_n_0 ),
        .I1(\trigger_time[8]_i_4_n_0 ),
        .I2(p_0_in[2]),
        .I3(\trigger_volt_reg_n_0_[0] ),
        .I4(p_0_in[0]),
        .I5(p_0_in[1]),
        .O(p_0_in__1[3]));
  (* SOFT_HLUTNM = "soft_lutpair84" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \trigger_volt[3]_i_2 
       (.I0(\trigger_time[8]_i_3_n_0 ),
        .I1(btn[2]),
        .I2(btn[1]),
        .O(\trigger_volt[3]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h0D707D7D)) 
    \trigger_volt[4]_i_1 
       (.I0(\trigger_volt[9]_i_4_n_0 ),
        .I1(\trigger_volt[4]_i_2_n_0 ),
        .I2(p_0_in[3]),
        .I3(\trigger_volt[4]_i_3_n_0 ),
        .I4(\trigger_time[8]_i_4_n_0 ),
        .O(p_0_in__1[4]));
  (* SOFT_HLUTNM = "soft_lutpair85" *) 
  LUT4 #(
    .INIT(16'hAA80)) 
    \trigger_volt[4]_i_2 
       (.I0(p_0_in[2]),
        .I1(\trigger_volt_reg_n_0_[0] ),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .O(\trigger_volt[4]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair85" *) 
  LUT4 #(
    .INIT(16'h0155)) 
    \trigger_volt[4]_i_3 
       (.I0(p_0_in[2]),
        .I1(\trigger_volt_reg_n_0_[0] ),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .O(\trigger_volt[4]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h0D707D7D)) 
    \trigger_volt[5]_i_1 
       (.I0(\trigger_volt[9]_i_4_n_0 ),
        .I1(\trigger_volt[5]_i_2_n_0 ),
        .I2(p_0_in[4]),
        .I3(\trigger_volt[5]_i_3_n_0 ),
        .I4(\trigger_time[8]_i_4_n_0 ),
        .O(p_0_in__1[5]));
  (* SOFT_HLUTNM = "soft_lutpair81" *) 
  LUT5 #(
    .INIT(32'hEA000000)) 
    \trigger_volt[5]_i_2 
       (.I0(p_0_in[1]),
        .I1(p_0_in[0]),
        .I2(\trigger_volt_reg_n_0_[0] ),
        .I3(p_0_in[2]),
        .I4(p_0_in[3]),
        .O(\trigger_volt[5]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair81" *) 
  LUT5 #(
    .INIT(32'h00000057)) 
    \trigger_volt[5]_i_3 
       (.I0(p_0_in[1]),
        .I1(p_0_in[0]),
        .I2(\trigger_volt_reg_n_0_[0] ),
        .I3(p_0_in[2]),
        .I4(p_0_in[3]),
        .O(\trigger_volt[5]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h4FF84848)) 
    \trigger_volt[6]_i_1 
       (.I0(\trigger_volt[7]_i_3_n_0 ),
        .I1(\trigger_time[8]_i_4_n_0 ),
        .I2(p_0_in[5]),
        .I3(\trigger_volt[7]_i_2_n_0 ),
        .I4(\trigger_volt[9]_i_4_n_0 ),
        .O(p_0_in__1[6]));
  LUT6 #(
    .INIT(64'h700570F07DF57DF5)) 
    \trigger_volt[7]_i_1 
       (.I0(\trigger_volt[9]_i_4_n_0 ),
        .I1(\trigger_volt[7]_i_2_n_0 ),
        .I2(p_0_in[6]),
        .I3(p_0_in[5]),
        .I4(\trigger_volt[7]_i_3_n_0 ),
        .I5(\trigger_time[8]_i_4_n_0 ),
        .O(p_0_in__1[7]));
  LUT6 #(
    .INIT(64'h8888800000000000)) 
    \trigger_volt[7]_i_2 
       (.I0(p_0_in[3]),
        .I1(p_0_in[2]),
        .I2(\trigger_volt_reg_n_0_[0] ),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(p_0_in[4]),
        .O(\trigger_volt[7]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000011111)) 
    \trigger_volt[7]_i_3 
       (.I0(p_0_in[3]),
        .I1(p_0_in[2]),
        .I2(\trigger_volt_reg_n_0_[0] ),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(p_0_in[4]),
        .O(\trigger_volt[7]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCFF8F0F8C048C048)) 
    \trigger_volt[8]_i_1 
       (.I0(\trigger_volt[8]_i_2_n_0 ),
        .I1(\trigger_time[8]_i_4_n_0 ),
        .I2(p_0_in[7]),
        .I3(p_0_in[6]),
        .I4(\trigger_volt[8]_i_3_n_0 ),
        .I5(\trigger_volt[9]_i_4_n_0 ),
        .O(p_0_in__1[8]));
  LUT6 #(
    .INIT(64'h0000000000000051)) 
    \trigger_volt[8]_i_2 
       (.I0(p_0_in[4]),
        .I1(p_0_in[1]),
        .I2(\trigger_volt[8]_i_4_n_0 ),
        .I3(p_0_in[2]),
        .I4(p_0_in[3]),
        .I5(p_0_in[5]),
        .O(\trigger_volt[8]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hA800000000000000)) 
    \trigger_volt[8]_i_3 
       (.I0(p_0_in[4]),
        .I1(p_0_in[1]),
        .I2(\trigger_volt[8]_i_5_n_0 ),
        .I3(p_0_in[2]),
        .I4(p_0_in[3]),
        .I5(p_0_in[5]),
        .O(\trigger_volt[8]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair90" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \trigger_volt[8]_i_4 
       (.I0(\trigger_volt_reg_n_0_[0] ),
        .I1(p_0_in[0]),
        .O(\trigger_volt[8]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair90" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \trigger_volt[8]_i_5 
       (.I0(\trigger_volt_reg_n_0_[0] ),
        .I1(p_0_in[0]),
        .O(\trigger_volt[8]_i_5_n_0 ));
  LUT5 #(
    .INIT(32'h5D5D5F5D)) 
    \trigger_volt[9]_i_1 
       (.I0(s00_axi_aresetn),
        .I1(btn[0]),
        .I2(\trigger_time[8]_i_3_n_0 ),
        .I3(btn[2]),
        .I4(btn[1]),
        .O(\trigger_volt[9]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hF8CFF8F048C048C0)) 
    \trigger_volt[9]_i_2 
       (.I0(\trigger_volt[9]_i_3_n_0 ),
        .I1(\trigger_volt[9]_i_4_n_0 ),
        .I2(p_0_in[8]),
        .I3(p_0_in[7]),
        .I4(\trigger_volt[9]_i_5_n_0 ),
        .I5(\trigger_time[8]_i_4_n_0 ),
        .O(p_0_in__1[9]));
  (* SOFT_HLUTNM = "soft_lutpair89" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \trigger_volt[9]_i_3 
       (.I0(p_0_in[5]),
        .I1(\trigger_volt[7]_i_2_n_0 ),
        .I2(p_0_in[6]),
        .O(\trigger_volt[9]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair79" *) 
  LUT4 #(
    .INIT(16'h0010)) 
    \trigger_volt[9]_i_4 
       (.I0(btn[0]),
        .I1(btn[1]),
        .I2(btn[2]),
        .I3(\trigger_time[8]_i_3_n_0 ),
        .O(\trigger_volt[9]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair89" *) 
  LUT3 #(
    .INIT(8'h04)) 
    \trigger_volt[9]_i_5 
       (.I0(p_0_in[5]),
        .I1(\trigger_volt[7]_i_3_n_0 ),
        .I2(p_0_in[6]),
        .O(\trigger_volt[9]_i_5_n_0 ));
  FDRE \trigger_volt_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\trigger_volt[9]_i_1_n_0 ),
        .D(\trigger_volt[0]_i_1_n_0 ),
        .Q(\trigger_volt_reg_n_0_[0] ),
        .R(1'b0));
  FDRE \trigger_volt_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\trigger_volt[9]_i_1_n_0 ),
        .D(p_0_in__1[1]),
        .Q(p_0_in[0]),
        .R(1'b0));
  FDRE \trigger_volt_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\trigger_volt[9]_i_1_n_0 ),
        .D(p_0_in__1[2]),
        .Q(p_0_in[1]),
        .R(1'b0));
  FDRE \trigger_volt_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\trigger_volt[9]_i_1_n_0 ),
        .D(p_0_in__1[3]),
        .Q(p_0_in[2]),
        .R(1'b0));
  FDRE \trigger_volt_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\trigger_volt[9]_i_1_n_0 ),
        .D(p_0_in__1[4]),
        .Q(p_0_in[3]),
        .R(1'b0));
  FDRE \trigger_volt_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\trigger_volt[9]_i_1_n_0 ),
        .D(p_0_in__1[5]),
        .Q(p_0_in[4]),
        .R(1'b0));
  FDRE \trigger_volt_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\trigger_volt[9]_i_1_n_0 ),
        .D(p_0_in__1[6]),
        .Q(p_0_in[5]),
        .R(1'b0));
  FDRE \trigger_volt_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\trigger_volt[9]_i_1_n_0 ),
        .D(p_0_in__1[7]),
        .Q(p_0_in[6]),
        .R(1'b0));
  FDRE \trigger_volt_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\trigger_volt[9]_i_1_n_0 ),
        .D(p_0_in__1[8]),
        .Q(p_0_in[7]),
        .R(1'b0));
  FDRE \trigger_volt_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\trigger_volt[9]_i_1_n_0 ),
        .D(p_0_in__1[9]),
        .Q(p_0_in[8]),
        .R(1'b0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_video video_inst
       (.DOADO({readL[15:14],readL[10:8]}),
        .Q(row),
        .S(leftChannelMemory_n_5),
        .lopt(lopt),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_aresetn(s00_axi_aresetn),
        .s00_axi_aresetn_0(SS),
        .\sdp_bl.ramb18_dp_bl.ram18_bl (AddrRead),
        .\sdp_bl.ramb18_dp_bl.ram18_bl_0 (leftChannelMemory_n_6),
        .\sdp_bl.ramb18_dp_bl.ram18_bl_1 (readR),
        .\sdp_bl.ramb18_dp_bl.ram18_bl_2 ({rightChannelMemory_n_12,rightChannelMemory_n_13}),
        .switch(switch[1:0]),
        .tmds(tmds),
        .tmdsb(tmdsb),
        .\trigger_time_reg[2] (\trigger_time_reg_n_0_[2] ),
        .\trigger_time_reg[3] (\trigger_time_reg_n_0_[3] ),
        .\trigger_time_reg[4] (\trigger_time_reg_n_0_[4] ),
        .\trigger_time_reg[6] (\trigger_time_reg_n_0_[6] ),
        .\trigger_time_reg[7] (\trigger_time_reg_n_0_[7] ),
        .\trigger_time_reg[8] ({\trigger_time_reg_n_0_[8] ,\trigger_time_reg_n_0_[5] ,\trigger_time_reg_n_0_[1] ,\trigger_time_reg_n_0_[0] }),
        .\trigger_time_reg[9] (\trigger_time_reg_n_0_[9] ),
        .\trigger_volt_reg[9] ({p_0_in,\trigger_volt_reg_n_0_[0] }));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Audio_Codec_Wrapper wrapper_inst
       (.BCLK_int_reg(SS),
        .CO(CO),
        .D(UnL_bus_out),
        .DIBDI(DI),
        .E(w_sw[1]),
        .\L_bus_in_reg[17] (D_L_O),
        .Q(L_bus_in),
        .\R_bus_in_reg[17] (D_R_O),
        .\R_bus_in_reg[17]_0 (R_bus_in),
        .S(\FSM_onehot_state[1]_i_5_n_0 ),
        .\UnL_bus_out2_reg[10] (UnL_bus_out2),
        .ac_adc_sdata(ac_adc_sdata),
        .ac_bclk(ac_bclk),
        .ac_dac_sdata(ac_dac_sdata),
        .ac_lrclk(ac_lrclk),
        .ac_mclk(ac_mclk),
        .lopt(lopt),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_aresetn(s00_axi_aresetn),
        .scl(scl),
        .sda(sda),
        .\sdp_bl.ramb18_dp_bl.ram18_bl ({wrapper_inst_n_43,wrapper_inst_n_44,wrapper_inst_n_45,wrapper_inst_n_46,wrapper_inst_n_47,wrapper_inst_n_48,wrapper_inst_n_49,wrapper_inst_n_50,wrapper_inst_n_51,wrapper_inst_n_52,wrapper_inst_n_53,wrapper_inst_n_54,wrapper_inst_n_55,wrapper_inst_n_56,wrapper_inst_n_57,wrapper_inst_n_58}),
        .\slv_reg10_reg[15] (\slv_reg10_reg[15] ),
        .\slv_reg9_reg[15] (\slv_reg9_reg[15] ),
        .switch(switch[2]),
        .\trigger_volt_reg[8] (p_0_in[7:0]),
        .\trigger_volt_reg[9] (\FSM_onehot_state_reg[1] ),
        .w_sw(w_sw[0]));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_lab2_fsm
   (SR,
    Q,
    WREN,
    E,
    s00_axi_aresetn,
    \slv_reg7_reg[0] ,
    switch,
    SS,
    s00_axi_aclk,
    \i_sig_reg[9] ,
    \i_sig_reg[5] ,
    w_sw,
    CO,
    \trigger_volt_reg[9] );
  output [0:0]SR;
  output [1:0]Q;
  output WREN;
  output [0:0]E;
  input s00_axi_aresetn;
  input [0:0]\slv_reg7_reg[0] ;
  input [0:0]switch;
  input [0:0]SS;
  input s00_axi_aclk;
  input [3:0]\i_sig_reg[9] ;
  input \i_sig_reg[5] ;
  input [2:0]w_sw;
  input [0:0]CO;
  input [0:0]\trigger_volt_reg[9] ;

  wire [0:0]CO;
  wire [0:0]E;
  wire \FSM_onehot_state[0]_i_1_n_0 ;
  wire \FSM_onehot_state[1]_i_1_n_0 ;
  wire \FSM_onehot_state[2]_i_1_n_0 ;
  wire \FSM_onehot_state[3]_i_1_n_0 ;
  (* RTL_KEEP = "yes" *) wire \FSM_onehot_state_reg_n_0_[0] ;
  (* RTL_KEEP = "yes" *) wire \FSM_onehot_state_reg_n_0_[1] ;
  (* RTL_KEEP = "yes" *) wire \FSM_onehot_state_reg_n_0_[2] ;
  (* RTL_KEEP = "yes" *) wire \FSM_onehot_state_reg_n_0_[3] ;
  (* RTL_KEEP = "yes" *) wire \FSM_onehot_state_reg_n_0_[4] ;
  (* RTL_KEEP = "yes" *) wire \FSM_onehot_state_reg_n_0_[5] ;
  wire [1:0]Q;
  wire [0:0]SR;
  wire [0:0]SS;
  wire WREN;
  wire \cw_reg[0]_i_1_n_0 ;
  wire \cw_reg[1]_i_1_n_0 ;
  wire \cw_reg[2]_i_1_n_0 ;
  wire \cw_reg[2]_i_2_n_0 ;
  wire \i_sig_reg[5] ;
  wire [3:0]\i_sig_reg[9] ;
  wire s00_axi_aclk;
  wire s00_axi_aresetn;
  wire [0:0]\slv_reg7_reg[0] ;
  wire [0:0]switch;
  wire [0:0]\trigger_volt_reg[9] ;
  wire [0:0]w_cw;
  wire [2:0]w_sw;

  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \FSM_onehot_state[0]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[5] ),
        .I1(\i_sig_reg[9] [2]),
        .I2(\i_sig_reg[9] [0]),
        .I3(\i_sig_reg[5] ),
        .I4(\i_sig_reg[9] [1]),
        .I5(\i_sig_reg[9] [3]),
        .O(\FSM_onehot_state[0]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hBFAA)) 
    \FSM_onehot_state[1]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[0] ),
        .I1(CO),
        .I2(\trigger_volt_reg[9] ),
        .I3(\FSM_onehot_state_reg_n_0_[1] ),
        .O(\FSM_onehot_state[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF444F444F444)) 
    \FSM_onehot_state[2]_i_1 
       (.I0(w_sw[1]),
        .I1(\FSM_onehot_state_reg_n_0_[2] ),
        .I2(\FSM_onehot_state_reg_n_0_[1] ),
        .I3(w_sw[0]),
        .I4(\FSM_onehot_state_reg_n_0_[5] ),
        .I5(w_sw[2]),
        .O(\FSM_onehot_state[2]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \FSM_onehot_state[3]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[2] ),
        .I1(w_sw[1]),
        .O(\FSM_onehot_state[3]_i_1_n_0 ));
  (* FSM_ENCODED_STATES = "countinc:010000,countcmp:100000,waittrigger:000010,write:001000,waitready:000100,init:000001" *) 
  (* KEEP = "yes" *) 
  FDSE #(
    .INIT(1'b1)) 
    \FSM_onehot_state_reg[0] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\FSM_onehot_state[0]_i_1_n_0 ),
        .Q(\FSM_onehot_state_reg_n_0_[0] ),
        .S(SS));
  (* FSM_ENCODED_STATES = "countinc:010000,countcmp:100000,waittrigger:000010,write:001000,waitready:000100,init:000001" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg[1] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\FSM_onehot_state[1]_i_1_n_0 ),
        .Q(\FSM_onehot_state_reg_n_0_[1] ),
        .R(SS));
  (* FSM_ENCODED_STATES = "countinc:010000,countcmp:100000,waittrigger:000010,write:001000,waitready:000100,init:000001" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\FSM_onehot_state[2]_i_1_n_0 ),
        .Q(\FSM_onehot_state_reg_n_0_[2] ),
        .R(SS));
  (* FSM_ENCODED_STATES = "countinc:010000,countcmp:100000,waittrigger:000010,write:001000,waitready:000100,init:000001" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg[3] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\FSM_onehot_state[3]_i_1_n_0 ),
        .Q(\FSM_onehot_state_reg_n_0_[3] ),
        .R(SS));
  (* FSM_ENCODED_STATES = "countinc:010000,countcmp:100000,waittrigger:000010,write:001000,waitready:000100,init:000001" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg[4] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\FSM_onehot_state_reg_n_0_[3] ),
        .Q(\FSM_onehot_state_reg_n_0_[4] ),
        .R(SS));
  (* FSM_ENCODED_STATES = "countinc:010000,countcmp:100000,waittrigger:000010,write:001000,waitready:000100,init:000001" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg[5] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\FSM_onehot_state_reg_n_0_[4] ),
        .Q(\FSM_onehot_state_reg_n_0_[5] ),
        .R(SS));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \cw_reg[0] 
       (.CLR(1'b0),
        .D(\cw_reg[0]_i_1_n_0 ),
        .G(\cw_reg[2]_i_2_n_0 ),
        .GE(1'b1),
        .Q(w_cw));
  LUT4 #(
    .INIT(16'h0002)) 
    \cw_reg[0]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[3] ),
        .I1(\FSM_onehot_state_reg_n_0_[5] ),
        .I2(\FSM_onehot_state_reg_n_0_[2] ),
        .I3(\FSM_onehot_state_reg_n_0_[1] ),
        .O(\cw_reg[0]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \cw_reg[1] 
       (.CLR(1'b0),
        .D(\cw_reg[1]_i_1_n_0 ),
        .G(\cw_reg[2]_i_2_n_0 ),
        .GE(1'b1),
        .Q(Q[0]));
  LUT4 #(
    .INIT(16'h0001)) 
    \cw_reg[1]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[5] ),
        .I1(\FSM_onehot_state_reg_n_0_[2] ),
        .I2(\FSM_onehot_state_reg_n_0_[1] ),
        .I3(\FSM_onehot_state_reg_n_0_[3] ),
        .O(\cw_reg[1]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \cw_reg[2] 
       (.CLR(1'b0),
        .D(\cw_reg[2]_i_1_n_0 ),
        .G(\cw_reg[2]_i_2_n_0 ),
        .GE(1'b1),
        .Q(Q[1]));
  LUT5 #(
    .INIT(32'h00000001)) 
    \cw_reg[2]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[4] ),
        .I1(\FSM_onehot_state_reg_n_0_[3] ),
        .I2(\FSM_onehot_state_reg_n_0_[5] ),
        .I3(\FSM_onehot_state_reg_n_0_[2] ),
        .I4(\FSM_onehot_state_reg_n_0_[1] ),
        .O(\cw_reg[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \cw_reg[2]_i_2 
       (.I0(\FSM_onehot_state_reg_n_0_[5] ),
        .I1(\FSM_onehot_state_reg_n_0_[2] ),
        .I2(\FSM_onehot_state_reg_n_0_[1] ),
        .I3(\FSM_onehot_state_reg_n_0_[4] ),
        .I4(\FSM_onehot_state_reg_n_0_[0] ),
        .I5(\FSM_onehot_state_reg_n_0_[3] ),
        .O(\cw_reg[2]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair93" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \i_sig[9]_i_1 
       (.I0(Q[0]),
        .I1(Q[1]),
        .I2(s00_axi_aresetn),
        .O(SR));
  (* SOFT_HLUTNM = "soft_lutpair93" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \i_sig[9]_i_2 
       (.I0(Q[0]),
        .I1(Q[1]),
        .O(E));
  LUT3 #(
    .INIT(8'hB8)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_1 
       (.I0(\slv_reg7_reg[0] ),
        .I1(switch),
        .I2(w_cw),
        .O(WREN));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_my_lab3_ip_v3_0
   (tmds,
    tmdsb,
    ac_mclk,
    ac_dac_sdata,
    ac_lrclk,
    ac_bclk,
    S_AXI_WREADY,
    S_AXI_AWREADY,
    S_AXI_ARREADY,
    s00_axi_rdata,
    s00_axi_rvalid,
    s00_axi_bvalid,
    scl,
    sda,
    s00_axi_aclk,
    ac_adc_sdata,
    btn,
    s00_axi_aresetn,
    s00_axi_wstrb,
    switch,
    s00_axi_awaddr,
    s00_axi_wdata,
    s00_axi_araddr,
    s00_axi_wvalid,
    s00_axi_awvalid,
    s00_axi_arvalid,
    s00_axi_bready,
    s00_axi_rready);
  output [3:0]tmds;
  output [3:0]tmdsb;
  output ac_mclk;
  output ac_dac_sdata;
  output ac_lrclk;
  output ac_bclk;
  output S_AXI_WREADY;
  output S_AXI_AWREADY;
  output S_AXI_ARREADY;
  output [31:0]s00_axi_rdata;
  output s00_axi_rvalid;
  output s00_axi_bvalid;
  inout scl;
  inout sda;
  input s00_axi_aclk;
  input ac_adc_sdata;
  input [4:0]btn;
  input s00_axi_aresetn;
  input [3:0]s00_axi_wstrb;
  input [2:0]switch;
  input [4:0]s00_axi_awaddr;
  input [31:0]s00_axi_wdata;
  input [4:0]s00_axi_araddr;
  input s00_axi_wvalid;
  input s00_axi_awvalid;
  input s00_axi_arvalid;
  input s00_axi_bready;
  input s00_axi_rready;

  wire S_AXI_ARREADY;
  wire S_AXI_AWREADY;
  wire S_AXI_WREADY;
  wire ac_adc_sdata;
  wire ac_bclk;
  wire ac_dac_sdata;
  wire ac_lrclk;
  wire ac_mclk;
  wire axi_bvalid_i_1_n_0;
  wire axi_rvalid_i_1_n_0;
  wire [4:0]btn;
  wire s00_axi_aclk;
  wire [4:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arvalid;
  wire [4:0]s00_axi_awaddr;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;
  wire scl;
  wire sda;
  wire [2:0]switch;
  wire [3:0]tmds;
  wire [3:0]tmdsb;

  LUT6 #(
    .INIT(64'h7444444444444444)) 
    axi_bvalid_i_1
       (.I0(s00_axi_bready),
        .I1(s00_axi_bvalid),
        .I2(S_AXI_WREADY),
        .I3(S_AXI_AWREADY),
        .I4(s00_axi_wvalid),
        .I5(s00_axi_awvalid),
        .O(axi_bvalid_i_1_n_0));
  LUT4 #(
    .INIT(16'h08F8)) 
    axi_rvalid_i_1
       (.I0(s00_axi_arvalid),
        .I1(S_AXI_ARREADY),
        .I2(s00_axi_rvalid),
        .I3(s00_axi_rready),
        .O(axi_rvalid_i_1_n_0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_my_lab3_ip_v3_0_S00_AXI my_lab3_ip_v2_0_S00_AXI_inst
       (.ac_adc_sdata(ac_adc_sdata),
        .ac_bclk(ac_bclk),
        .ac_dac_sdata(ac_dac_sdata),
        .ac_lrclk(ac_lrclk),
        .ac_mclk(ac_mclk),
        .axi_arready_reg_0(axi_rvalid_i_1_n_0),
        .axi_bvalid_reg_0(axi_bvalid_i_1_n_0),
        .btn(btn),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_araddr(s00_axi_araddr),
        .s00_axi_aresetn(s00_axi_aresetn),
        .s00_axi_arready(S_AXI_ARREADY),
        .s00_axi_arvalid(s00_axi_arvalid),
        .s00_axi_awaddr(s00_axi_awaddr),
        .s00_axi_awready(S_AXI_AWREADY),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_bvalid(s00_axi_bvalid),
        .s00_axi_rdata(s00_axi_rdata),
        .s00_axi_rvalid(s00_axi_rvalid),
        .s00_axi_wdata(s00_axi_wdata),
        .s00_axi_wready(S_AXI_WREADY),
        .s00_axi_wstrb(s00_axi_wstrb),
        .s00_axi_wvalid(s00_axi_wvalid),
        .scl(scl),
        .sda(sda),
        .switch(switch),
        .tmds(tmds),
        .tmdsb(tmdsb));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_my_lab3_ip_v3_0_S00_AXI
   (s00_axi_wready,
    s00_axi_awready,
    s00_axi_arready,
    s00_axi_bvalid,
    s00_axi_rvalid,
    s00_axi_rdata,
    tmds,
    tmdsb,
    ac_mclk,
    ac_dac_sdata,
    ac_lrclk,
    ac_bclk,
    scl,
    sda,
    s00_axi_aclk,
    ac_adc_sdata,
    axi_bvalid_reg_0,
    axi_arready_reg_0,
    btn,
    s00_axi_aresetn,
    s00_axi_wstrb,
    s00_axi_awaddr,
    s00_axi_wdata,
    s00_axi_araddr,
    s00_axi_wvalid,
    s00_axi_awvalid,
    s00_axi_arvalid,
    switch);
  output s00_axi_wready;
  output s00_axi_awready;
  output s00_axi_arready;
  output s00_axi_bvalid;
  output s00_axi_rvalid;
  output [31:0]s00_axi_rdata;
  output [3:0]tmds;
  output [3:0]tmdsb;
  output ac_mclk;
  output ac_dac_sdata;
  output ac_lrclk;
  output ac_bclk;
  inout scl;
  inout sda;
  input s00_axi_aclk;
  input ac_adc_sdata;
  input axi_bvalid_reg_0;
  input axi_arready_reg_0;
  input [4:0]btn;
  input s00_axi_aresetn;
  input [3:0]s00_axi_wstrb;
  input [4:0]s00_axi_awaddr;
  input [31:0]s00_axi_wdata;
  input [4:0]s00_axi_araddr;
  input s00_axi_wvalid;
  input s00_axi_awvalid;
  input s00_axi_arvalid;
  input [2:0]switch;

  wire RST;
  wire WREN;
  wire ac_adc_sdata;
  wire ac_bclk;
  wire ac_dac_sdata;
  wire ac_lrclk;
  wire ac_mclk;
  wire \axi_araddr_reg[2]_rep_n_0 ;
  wire \axi_araddr_reg[3]_rep_n_0 ;
  wire axi_arready0;
  wire axi_arready_reg_0;
  wire axi_awready0;
  wire axi_bvalid_reg_0;
  wire \axi_rdata[0]_i_10_n_0 ;
  wire \axi_rdata[0]_i_3_n_0 ;
  wire \axi_rdata[0]_i_5_n_0 ;
  wire \axi_rdata[0]_i_6_n_0 ;
  wire \axi_rdata[0]_i_7_n_0 ;
  wire \axi_rdata[0]_i_8_n_0 ;
  wire \axi_rdata[0]_i_9_n_0 ;
  wire \axi_rdata[10]_i_10_n_0 ;
  wire \axi_rdata[10]_i_3_n_0 ;
  wire \axi_rdata[10]_i_5_n_0 ;
  wire \axi_rdata[10]_i_6_n_0 ;
  wire \axi_rdata[10]_i_7_n_0 ;
  wire \axi_rdata[10]_i_8_n_0 ;
  wire \axi_rdata[10]_i_9_n_0 ;
  wire \axi_rdata[11]_i_10_n_0 ;
  wire \axi_rdata[11]_i_3_n_0 ;
  wire \axi_rdata[11]_i_5_n_0 ;
  wire \axi_rdata[11]_i_6_n_0 ;
  wire \axi_rdata[11]_i_7_n_0 ;
  wire \axi_rdata[11]_i_8_n_0 ;
  wire \axi_rdata[11]_i_9_n_0 ;
  wire \axi_rdata[12]_i_10_n_0 ;
  wire \axi_rdata[12]_i_3_n_0 ;
  wire \axi_rdata[12]_i_5_n_0 ;
  wire \axi_rdata[12]_i_6_n_0 ;
  wire \axi_rdata[12]_i_7_n_0 ;
  wire \axi_rdata[12]_i_8_n_0 ;
  wire \axi_rdata[12]_i_9_n_0 ;
  wire \axi_rdata[13]_i_10_n_0 ;
  wire \axi_rdata[13]_i_3_n_0 ;
  wire \axi_rdata[13]_i_5_n_0 ;
  wire \axi_rdata[13]_i_6_n_0 ;
  wire \axi_rdata[13]_i_7_n_0 ;
  wire \axi_rdata[13]_i_8_n_0 ;
  wire \axi_rdata[13]_i_9_n_0 ;
  wire \axi_rdata[14]_i_10_n_0 ;
  wire \axi_rdata[14]_i_3_n_0 ;
  wire \axi_rdata[14]_i_5_n_0 ;
  wire \axi_rdata[14]_i_6_n_0 ;
  wire \axi_rdata[14]_i_7_n_0 ;
  wire \axi_rdata[14]_i_8_n_0 ;
  wire \axi_rdata[14]_i_9_n_0 ;
  wire \axi_rdata[15]_i_10_n_0 ;
  wire \axi_rdata[15]_i_3_n_0 ;
  wire \axi_rdata[15]_i_5_n_0 ;
  wire \axi_rdata[15]_i_6_n_0 ;
  wire \axi_rdata[15]_i_7_n_0 ;
  wire \axi_rdata[15]_i_8_n_0 ;
  wire \axi_rdata[15]_i_9_n_0 ;
  wire \axi_rdata[16]_i_10_n_0 ;
  wire \axi_rdata[16]_i_3_n_0 ;
  wire \axi_rdata[16]_i_5_n_0 ;
  wire \axi_rdata[16]_i_6_n_0 ;
  wire \axi_rdata[16]_i_7_n_0 ;
  wire \axi_rdata[16]_i_8_n_0 ;
  wire \axi_rdata[16]_i_9_n_0 ;
  wire \axi_rdata[17]_i_10_n_0 ;
  wire \axi_rdata[17]_i_3_n_0 ;
  wire \axi_rdata[17]_i_5_n_0 ;
  wire \axi_rdata[17]_i_6_n_0 ;
  wire \axi_rdata[17]_i_7_n_0 ;
  wire \axi_rdata[17]_i_8_n_0 ;
  wire \axi_rdata[17]_i_9_n_0 ;
  wire \axi_rdata[18]_i_10_n_0 ;
  wire \axi_rdata[18]_i_3_n_0 ;
  wire \axi_rdata[18]_i_5_n_0 ;
  wire \axi_rdata[18]_i_6_n_0 ;
  wire \axi_rdata[18]_i_7_n_0 ;
  wire \axi_rdata[18]_i_8_n_0 ;
  wire \axi_rdata[18]_i_9_n_0 ;
  wire \axi_rdata[19]_i_10_n_0 ;
  wire \axi_rdata[19]_i_3_n_0 ;
  wire \axi_rdata[19]_i_5_n_0 ;
  wire \axi_rdata[19]_i_6_n_0 ;
  wire \axi_rdata[19]_i_7_n_0 ;
  wire \axi_rdata[19]_i_8_n_0 ;
  wire \axi_rdata[19]_i_9_n_0 ;
  wire \axi_rdata[1]_i_10_n_0 ;
  wire \axi_rdata[1]_i_3_n_0 ;
  wire \axi_rdata[1]_i_5_n_0 ;
  wire \axi_rdata[1]_i_6_n_0 ;
  wire \axi_rdata[1]_i_7_n_0 ;
  wire \axi_rdata[1]_i_8_n_0 ;
  wire \axi_rdata[1]_i_9_n_0 ;
  wire \axi_rdata[20]_i_10_n_0 ;
  wire \axi_rdata[20]_i_3_n_0 ;
  wire \axi_rdata[20]_i_5_n_0 ;
  wire \axi_rdata[20]_i_6_n_0 ;
  wire \axi_rdata[20]_i_7_n_0 ;
  wire \axi_rdata[20]_i_8_n_0 ;
  wire \axi_rdata[20]_i_9_n_0 ;
  wire \axi_rdata[21]_i_10_n_0 ;
  wire \axi_rdata[21]_i_3_n_0 ;
  wire \axi_rdata[21]_i_5_n_0 ;
  wire \axi_rdata[21]_i_6_n_0 ;
  wire \axi_rdata[21]_i_7_n_0 ;
  wire \axi_rdata[21]_i_8_n_0 ;
  wire \axi_rdata[21]_i_9_n_0 ;
  wire \axi_rdata[22]_i_10_n_0 ;
  wire \axi_rdata[22]_i_3_n_0 ;
  wire \axi_rdata[22]_i_5_n_0 ;
  wire \axi_rdata[22]_i_6_n_0 ;
  wire \axi_rdata[22]_i_7_n_0 ;
  wire \axi_rdata[22]_i_8_n_0 ;
  wire \axi_rdata[22]_i_9_n_0 ;
  wire \axi_rdata[23]_i_10_n_0 ;
  wire \axi_rdata[23]_i_3_n_0 ;
  wire \axi_rdata[23]_i_5_n_0 ;
  wire \axi_rdata[23]_i_6_n_0 ;
  wire \axi_rdata[23]_i_7_n_0 ;
  wire \axi_rdata[23]_i_8_n_0 ;
  wire \axi_rdata[23]_i_9_n_0 ;
  wire \axi_rdata[24]_i_10_n_0 ;
  wire \axi_rdata[24]_i_3_n_0 ;
  wire \axi_rdata[24]_i_5_n_0 ;
  wire \axi_rdata[24]_i_6_n_0 ;
  wire \axi_rdata[24]_i_7_n_0 ;
  wire \axi_rdata[24]_i_8_n_0 ;
  wire \axi_rdata[24]_i_9_n_0 ;
  wire \axi_rdata[25]_i_10_n_0 ;
  wire \axi_rdata[25]_i_3_n_0 ;
  wire \axi_rdata[25]_i_5_n_0 ;
  wire \axi_rdata[25]_i_6_n_0 ;
  wire \axi_rdata[25]_i_7_n_0 ;
  wire \axi_rdata[25]_i_8_n_0 ;
  wire \axi_rdata[25]_i_9_n_0 ;
  wire \axi_rdata[26]_i_10_n_0 ;
  wire \axi_rdata[26]_i_3_n_0 ;
  wire \axi_rdata[26]_i_5_n_0 ;
  wire \axi_rdata[26]_i_6_n_0 ;
  wire \axi_rdata[26]_i_7_n_0 ;
  wire \axi_rdata[26]_i_8_n_0 ;
  wire \axi_rdata[26]_i_9_n_0 ;
  wire \axi_rdata[27]_i_10_n_0 ;
  wire \axi_rdata[27]_i_3_n_0 ;
  wire \axi_rdata[27]_i_5_n_0 ;
  wire \axi_rdata[27]_i_6_n_0 ;
  wire \axi_rdata[27]_i_7_n_0 ;
  wire \axi_rdata[27]_i_8_n_0 ;
  wire \axi_rdata[27]_i_9_n_0 ;
  wire \axi_rdata[28]_i_10_n_0 ;
  wire \axi_rdata[28]_i_3_n_0 ;
  wire \axi_rdata[28]_i_5_n_0 ;
  wire \axi_rdata[28]_i_6_n_0 ;
  wire \axi_rdata[28]_i_7_n_0 ;
  wire \axi_rdata[28]_i_8_n_0 ;
  wire \axi_rdata[28]_i_9_n_0 ;
  wire \axi_rdata[29]_i_10_n_0 ;
  wire \axi_rdata[29]_i_3_n_0 ;
  wire \axi_rdata[29]_i_5_n_0 ;
  wire \axi_rdata[29]_i_6_n_0 ;
  wire \axi_rdata[29]_i_7_n_0 ;
  wire \axi_rdata[29]_i_8_n_0 ;
  wire \axi_rdata[29]_i_9_n_0 ;
  wire \axi_rdata[2]_i_10_n_0 ;
  wire \axi_rdata[2]_i_3_n_0 ;
  wire \axi_rdata[2]_i_5_n_0 ;
  wire \axi_rdata[2]_i_6_n_0 ;
  wire \axi_rdata[2]_i_7_n_0 ;
  wire \axi_rdata[2]_i_8_n_0 ;
  wire \axi_rdata[2]_i_9_n_0 ;
  wire \axi_rdata[30]_i_10_n_0 ;
  wire \axi_rdata[30]_i_3_n_0 ;
  wire \axi_rdata[30]_i_5_n_0 ;
  wire \axi_rdata[30]_i_6_n_0 ;
  wire \axi_rdata[30]_i_7_n_0 ;
  wire \axi_rdata[30]_i_8_n_0 ;
  wire \axi_rdata[30]_i_9_n_0 ;
  wire \axi_rdata[31]_i_10_n_0 ;
  wire \axi_rdata[31]_i_11_n_0 ;
  wire \axi_rdata[31]_i_4_n_0 ;
  wire \axi_rdata[31]_i_6_n_0 ;
  wire \axi_rdata[31]_i_7_n_0 ;
  wire \axi_rdata[31]_i_8_n_0 ;
  wire \axi_rdata[31]_i_9_n_0 ;
  wire \axi_rdata[3]_i_10_n_0 ;
  wire \axi_rdata[3]_i_3_n_0 ;
  wire \axi_rdata[3]_i_5_n_0 ;
  wire \axi_rdata[3]_i_6_n_0 ;
  wire \axi_rdata[3]_i_7_n_0 ;
  wire \axi_rdata[3]_i_8_n_0 ;
  wire \axi_rdata[3]_i_9_n_0 ;
  wire \axi_rdata[4]_i_10_n_0 ;
  wire \axi_rdata[4]_i_3_n_0 ;
  wire \axi_rdata[4]_i_5_n_0 ;
  wire \axi_rdata[4]_i_6_n_0 ;
  wire \axi_rdata[4]_i_7_n_0 ;
  wire \axi_rdata[4]_i_8_n_0 ;
  wire \axi_rdata[4]_i_9_n_0 ;
  wire \axi_rdata[5]_i_10_n_0 ;
  wire \axi_rdata[5]_i_3_n_0 ;
  wire \axi_rdata[5]_i_5_n_0 ;
  wire \axi_rdata[5]_i_6_n_0 ;
  wire \axi_rdata[5]_i_7_n_0 ;
  wire \axi_rdata[5]_i_8_n_0 ;
  wire \axi_rdata[5]_i_9_n_0 ;
  wire \axi_rdata[6]_i_10_n_0 ;
  wire \axi_rdata[6]_i_3_n_0 ;
  wire \axi_rdata[6]_i_5_n_0 ;
  wire \axi_rdata[6]_i_6_n_0 ;
  wire \axi_rdata[6]_i_7_n_0 ;
  wire \axi_rdata[6]_i_8_n_0 ;
  wire \axi_rdata[6]_i_9_n_0 ;
  wire \axi_rdata[7]_i_10_n_0 ;
  wire \axi_rdata[7]_i_3_n_0 ;
  wire \axi_rdata[7]_i_5_n_0 ;
  wire \axi_rdata[7]_i_6_n_0 ;
  wire \axi_rdata[7]_i_7_n_0 ;
  wire \axi_rdata[7]_i_8_n_0 ;
  wire \axi_rdata[7]_i_9_n_0 ;
  wire \axi_rdata[8]_i_10_n_0 ;
  wire \axi_rdata[8]_i_3_n_0 ;
  wire \axi_rdata[8]_i_5_n_0 ;
  wire \axi_rdata[8]_i_6_n_0 ;
  wire \axi_rdata[8]_i_7_n_0 ;
  wire \axi_rdata[8]_i_8_n_0 ;
  wire \axi_rdata[8]_i_9_n_0 ;
  wire \axi_rdata[9]_i_10_n_0 ;
  wire \axi_rdata[9]_i_3_n_0 ;
  wire \axi_rdata[9]_i_5_n_0 ;
  wire \axi_rdata[9]_i_6_n_0 ;
  wire \axi_rdata[9]_i_7_n_0 ;
  wire \axi_rdata[9]_i_8_n_0 ;
  wire \axi_rdata[9]_i_9_n_0 ;
  wire \axi_rdata_reg[0]_i_2_n_0 ;
  wire \axi_rdata_reg[0]_i_4_n_0 ;
  wire \axi_rdata_reg[10]_i_2_n_0 ;
  wire \axi_rdata_reg[10]_i_4_n_0 ;
  wire \axi_rdata_reg[11]_i_2_n_0 ;
  wire \axi_rdata_reg[11]_i_4_n_0 ;
  wire \axi_rdata_reg[12]_i_2_n_0 ;
  wire \axi_rdata_reg[12]_i_4_n_0 ;
  wire \axi_rdata_reg[13]_i_2_n_0 ;
  wire \axi_rdata_reg[13]_i_4_n_0 ;
  wire \axi_rdata_reg[14]_i_2_n_0 ;
  wire \axi_rdata_reg[14]_i_4_n_0 ;
  wire \axi_rdata_reg[15]_i_2_n_0 ;
  wire \axi_rdata_reg[15]_i_4_n_0 ;
  wire \axi_rdata_reg[16]_i_2_n_0 ;
  wire \axi_rdata_reg[16]_i_4_n_0 ;
  wire \axi_rdata_reg[17]_i_2_n_0 ;
  wire \axi_rdata_reg[17]_i_4_n_0 ;
  wire \axi_rdata_reg[18]_i_2_n_0 ;
  wire \axi_rdata_reg[18]_i_4_n_0 ;
  wire \axi_rdata_reg[19]_i_2_n_0 ;
  wire \axi_rdata_reg[19]_i_4_n_0 ;
  wire \axi_rdata_reg[1]_i_2_n_0 ;
  wire \axi_rdata_reg[1]_i_4_n_0 ;
  wire \axi_rdata_reg[20]_i_2_n_0 ;
  wire \axi_rdata_reg[20]_i_4_n_0 ;
  wire \axi_rdata_reg[21]_i_2_n_0 ;
  wire \axi_rdata_reg[21]_i_4_n_0 ;
  wire \axi_rdata_reg[22]_i_2_n_0 ;
  wire \axi_rdata_reg[22]_i_4_n_0 ;
  wire \axi_rdata_reg[23]_i_2_n_0 ;
  wire \axi_rdata_reg[23]_i_4_n_0 ;
  wire \axi_rdata_reg[24]_i_2_n_0 ;
  wire \axi_rdata_reg[24]_i_4_n_0 ;
  wire \axi_rdata_reg[25]_i_2_n_0 ;
  wire \axi_rdata_reg[25]_i_4_n_0 ;
  wire \axi_rdata_reg[26]_i_2_n_0 ;
  wire \axi_rdata_reg[26]_i_4_n_0 ;
  wire \axi_rdata_reg[27]_i_2_n_0 ;
  wire \axi_rdata_reg[27]_i_4_n_0 ;
  wire \axi_rdata_reg[28]_i_2_n_0 ;
  wire \axi_rdata_reg[28]_i_4_n_0 ;
  wire \axi_rdata_reg[29]_i_2_n_0 ;
  wire \axi_rdata_reg[29]_i_4_n_0 ;
  wire \axi_rdata_reg[2]_i_2_n_0 ;
  wire \axi_rdata_reg[2]_i_4_n_0 ;
  wire \axi_rdata_reg[30]_i_2_n_0 ;
  wire \axi_rdata_reg[30]_i_4_n_0 ;
  wire \axi_rdata_reg[31]_i_3_n_0 ;
  wire \axi_rdata_reg[31]_i_5_n_0 ;
  wire \axi_rdata_reg[3]_i_2_n_0 ;
  wire \axi_rdata_reg[3]_i_4_n_0 ;
  wire \axi_rdata_reg[4]_i_2_n_0 ;
  wire \axi_rdata_reg[4]_i_4_n_0 ;
  wire \axi_rdata_reg[5]_i_2_n_0 ;
  wire \axi_rdata_reg[5]_i_4_n_0 ;
  wire \axi_rdata_reg[6]_i_2_n_0 ;
  wire \axi_rdata_reg[6]_i_4_n_0 ;
  wire \axi_rdata_reg[7]_i_2_n_0 ;
  wire \axi_rdata_reg[7]_i_4_n_0 ;
  wire \axi_rdata_reg[8]_i_2_n_0 ;
  wire \axi_rdata_reg[8]_i_4_n_0 ;
  wire \axi_rdata_reg[9]_i_2_n_0 ;
  wire \axi_rdata_reg[9]_i_4_n_0 ;
  wire axi_wready0;
  wire [4:0]btn;
  wire datapath_n_15;
  wire datapath_n_16;
  wire datapath_n_17;
  wire datapath_n_18;
  wire datapath_n_19;
  wire fsm_n_0;
  wire fsm_n_4;
  wire [4:0]p_0_in;
  wire [31:7]p_1_in;
  wire [31:0]reg_data_out__0;
  wire s00_axi_aclk;
  wire [4:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arready;
  wire s00_axi_arvalid;
  wire [4:0]s00_axi_awaddr;
  wire s00_axi_awready;
  wire s00_axi_awvalid;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire s00_axi_wready;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;
  wire scl;
  wire sda;
  wire [4:0]sel0;
  wire [31:0]slv_reg0;
  wire [31:0]slv_reg1;
  wire [15:0]slv_reg10;
  wire \slv_reg10[15]_i_1_n_0 ;
  wire \slv_reg10[23]_i_1_n_0 ;
  wire \slv_reg10[31]_i_1_n_0 ;
  wire \slv_reg10[7]_i_1_n_0 ;
  wire [31:16]slv_reg10__0;
  wire [31:1]slv_reg12;
  wire \slv_reg12[15]_i_1_n_0 ;
  wire \slv_reg12[23]_i_1_n_0 ;
  wire \slv_reg12[31]_i_1_n_0 ;
  wire \slv_reg12[7]_i_1_n_0 ;
  wire [0:0]slv_reg12__0;
  wire \slv_reg1[15]_i_1_n_0 ;
  wire \slv_reg1[23]_i_1_n_0 ;
  wire \slv_reg1[31]_i_1_n_0 ;
  wire \slv_reg1[7]_i_1_n_0 ;
  wire [31:0]slv_reg2;
  wire [31:0]slv_reg20;
  wire \slv_reg20[15]_i_1_n_0 ;
  wire \slv_reg20[23]_i_1_n_0 ;
  wire \slv_reg20[31]_i_1_n_0 ;
  wire \slv_reg20[7]_i_1_n_0 ;
  wire [31:0]slv_reg21;
  wire \slv_reg21[15]_i_1_n_0 ;
  wire \slv_reg21[23]_i_1_n_0 ;
  wire \slv_reg21[31]_i_1_n_0 ;
  wire \slv_reg21[7]_i_1_n_0 ;
  wire [31:0]slv_reg22;
  wire \slv_reg22[15]_i_1_n_0 ;
  wire \slv_reg22[23]_i_1_n_0 ;
  wire \slv_reg22[31]_i_1_n_0 ;
  wire \slv_reg22[7]_i_1_n_0 ;
  wire [31:0]slv_reg23;
  wire \slv_reg23[15]_i_1_n_0 ;
  wire \slv_reg23[23]_i_1_n_0 ;
  wire \slv_reg23[31]_i_1_n_0 ;
  wire \slv_reg23[7]_i_1_n_0 ;
  wire [31:0]slv_reg24;
  wire \slv_reg24[15]_i_1_n_0 ;
  wire \slv_reg24[23]_i_1_n_0 ;
  wire \slv_reg24[31]_i_1_n_0 ;
  wire \slv_reg24[7]_i_1_n_0 ;
  wire [31:0]slv_reg25;
  wire \slv_reg25[15]_i_1_n_0 ;
  wire \slv_reg25[23]_i_1_n_0 ;
  wire \slv_reg25[31]_i_1_n_0 ;
  wire \slv_reg25[7]_i_1_n_0 ;
  wire [31:0]slv_reg26;
  wire \slv_reg26[15]_i_1_n_0 ;
  wire \slv_reg26[23]_i_1_n_0 ;
  wire \slv_reg26[31]_i_1_n_0 ;
  wire \slv_reg26[7]_i_1_n_0 ;
  wire [31:0]slv_reg27;
  wire \slv_reg27[15]_i_1_n_0 ;
  wire \slv_reg27[23]_i_1_n_0 ;
  wire \slv_reg27[31]_i_1_n_0 ;
  wire \slv_reg27[7]_i_1_n_0 ;
  wire [31:0]slv_reg28;
  wire \slv_reg28[15]_i_1_n_0 ;
  wire \slv_reg28[23]_i_1_n_0 ;
  wire \slv_reg28[31]_i_1_n_0 ;
  wire \slv_reg28[7]_i_1_n_0 ;
  wire [31:0]slv_reg29;
  wire \slv_reg29[15]_i_1_n_0 ;
  wire \slv_reg29[23]_i_1_n_0 ;
  wire \slv_reg29[31]_i_1_n_0 ;
  wire \slv_reg29[7]_i_1_n_0 ;
  wire \slv_reg2[15]_i_1_n_0 ;
  wire \slv_reg2[23]_i_1_n_0 ;
  wire \slv_reg2[31]_i_1_n_0 ;
  wire \slv_reg2[7]_i_1_n_0 ;
  wire [31:0]slv_reg3;
  wire [31:0]slv_reg30;
  wire \slv_reg30[15]_i_1_n_0 ;
  wire \slv_reg30[23]_i_1_n_0 ;
  wire \slv_reg30[31]_i_1_n_0 ;
  wire \slv_reg30[7]_i_1_n_0 ;
  wire [31:0]slv_reg31;
  wire \slv_reg31[15]_i_1_n_0 ;
  wire \slv_reg31[23]_i_1_n_0 ;
  wire \slv_reg31[31]_i_1_n_0 ;
  wire \slv_reg31[7]_i_1_n_0 ;
  wire \slv_reg3[15]_i_1_n_0 ;
  wire \slv_reg3[23]_i_1_n_0 ;
  wire \slv_reg3[31]_i_1_n_0 ;
  wire \slv_reg3[7]_i_1_n_0 ;
  wire [31:0]slv_reg4;
  wire \slv_reg4[15]_i_1_n_0 ;
  wire \slv_reg4[23]_i_1_n_0 ;
  wire \slv_reg4[31]_i_1_n_0 ;
  wire \slv_reg4[7]_i_1_n_0 ;
  wire [0:0]slv_reg7;
  wire \slv_reg7[15]_i_1_n_0 ;
  wire \slv_reg7[23]_i_1_n_0 ;
  wire \slv_reg7[31]_i_1_n_0 ;
  wire \slv_reg7[31]_i_2_n_0 ;
  wire \slv_reg7[7]_i_1_n_0 ;
  wire [31:1]slv_reg7__0;
  wire [9:0]slv_reg8;
  wire \slv_reg8[15]_i_1_n_0 ;
  wire \slv_reg8[23]_i_1_n_0 ;
  wire \slv_reg8[31]_i_1_n_0 ;
  wire \slv_reg8[31]_i_2_n_0 ;
  wire \slv_reg8[7]_i_1_n_0 ;
  wire [31:10]slv_reg8__0;
  wire [15:0]slv_reg9;
  wire \slv_reg9[15]_i_1_n_0 ;
  wire \slv_reg9[23]_i_1_n_0 ;
  wire \slv_reg9[31]_i_1_n_0 ;
  wire \slv_reg9[7]_i_1_n_0 ;
  wire [31:16]slv_reg9__0;
  wire slv_reg_rden;
  wire sw1;
  wire sw14_in;
  wire [2:0]switch;
  wire [3:0]tmds;
  wire [3:0]tmdsb;
  wire [2:1]w_cw;
  wire [2:0]w_sw;

  (* ORIG_CELL_NAME = "axi_araddr_reg[2]" *) 
  FDSE \axi_araddr_reg[2] 
       (.C(s00_axi_aclk),
        .CE(axi_arready0),
        .D(s00_axi_araddr[0]),
        .Q(sel0[0]),
        .S(RST));
  (* ORIG_CELL_NAME = "axi_araddr_reg[2]" *) 
  FDSE \axi_araddr_reg[2]_rep 
       (.C(s00_axi_aclk),
        .CE(axi_arready0),
        .D(s00_axi_araddr[0]),
        .Q(\axi_araddr_reg[2]_rep_n_0 ),
        .S(RST));
  (* ORIG_CELL_NAME = "axi_araddr_reg[3]" *) 
  FDSE \axi_araddr_reg[3] 
       (.C(s00_axi_aclk),
        .CE(axi_arready0),
        .D(s00_axi_araddr[1]),
        .Q(sel0[1]),
        .S(RST));
  (* ORIG_CELL_NAME = "axi_araddr_reg[3]" *) 
  FDSE \axi_araddr_reg[3]_rep 
       (.C(s00_axi_aclk),
        .CE(axi_arready0),
        .D(s00_axi_araddr[1]),
        .Q(\axi_araddr_reg[3]_rep_n_0 ),
        .S(RST));
  FDSE \axi_araddr_reg[4] 
       (.C(s00_axi_aclk),
        .CE(axi_arready0),
        .D(s00_axi_araddr[2]),
        .Q(sel0[2]),
        .S(RST));
  FDSE \axi_araddr_reg[5] 
       (.C(s00_axi_aclk),
        .CE(axi_arready0),
        .D(s00_axi_araddr[3]),
        .Q(sel0[3]),
        .S(RST));
  FDSE \axi_araddr_reg[6] 
       (.C(s00_axi_aclk),
        .CE(axi_arready0),
        .D(s00_axi_araddr[4]),
        .Q(sel0[4]),
        .S(RST));
  LUT2 #(
    .INIT(4'h2)) 
    axi_arready_i_1
       (.I0(s00_axi_arvalid),
        .I1(s00_axi_arready),
        .O(axi_arready0));
  FDRE axi_arready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_arready0),
        .Q(s00_axi_arready),
        .R(RST));
  FDRE \axi_awaddr_reg[2] 
       (.C(s00_axi_aclk),
        .CE(axi_awready0),
        .D(s00_axi_awaddr[0]),
        .Q(p_0_in[0]),
        .R(RST));
  FDRE \axi_awaddr_reg[3] 
       (.C(s00_axi_aclk),
        .CE(axi_awready0),
        .D(s00_axi_awaddr[1]),
        .Q(p_0_in[1]),
        .R(RST));
  FDRE \axi_awaddr_reg[4] 
       (.C(s00_axi_aclk),
        .CE(axi_awready0),
        .D(s00_axi_awaddr[2]),
        .Q(p_0_in[2]),
        .R(RST));
  FDRE \axi_awaddr_reg[5] 
       (.C(s00_axi_aclk),
        .CE(axi_awready0),
        .D(s00_axi_awaddr[3]),
        .Q(p_0_in[3]),
        .R(RST));
  FDRE \axi_awaddr_reg[6] 
       (.C(s00_axi_aclk),
        .CE(axi_awready0),
        .D(s00_axi_awaddr[4]),
        .Q(p_0_in[4]),
        .R(RST));
  LUT3 #(
    .INIT(8'h08)) 
    axi_awready_i_1
       (.I0(s00_axi_wvalid),
        .I1(s00_axi_awvalid),
        .I2(s00_axi_awready),
        .O(axi_awready0));
  FDRE axi_awready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_awready0),
        .Q(s00_axi_awready),
        .R(RST));
  FDRE axi_bvalid_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_bvalid_reg_0),
        .Q(s00_axi_bvalid),
        .R(RST));
  LUT6 #(
    .INIT(64'hB888FFFFB8880000)) 
    \axi_rdata[0]_i_1 
       (.I0(\axi_rdata_reg[0]_i_2_n_0 ),
        .I1(sel0[3]),
        .I2(sel0[2]),
        .I3(\axi_rdata[0]_i_3_n_0 ),
        .I4(sel0[4]),
        .I5(\axi_rdata_reg[0]_i_4_n_0 ),
        .O(reg_data_out__0[0]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[0]_i_10 
       (.I0(slv_reg10[0]),
        .I1(\axi_araddr_reg[3]_rep_n_0 ),
        .I2(slv_reg9[0]),
        .I3(\axi_araddr_reg[2]_rep_n_0 ),
        .I4(slv_reg8[0]),
        .O(\axi_rdata[0]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_3 
       (.I0(slv_reg23[0]),
        .I1(slv_reg22[0]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg21[0]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg20[0]),
        .O(\axi_rdata[0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_5 
       (.I0(slv_reg27[0]),
        .I1(slv_reg26[0]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg25[0]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg24[0]),
        .O(\axi_rdata[0]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_6 
       (.I0(slv_reg31[0]),
        .I1(slv_reg30[0]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg29[0]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg28[0]),
        .O(\axi_rdata[0]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h8830FFFF88300000)) 
    \axi_rdata[0]_i_7 
       (.I0(slv_reg7),
        .I1(\axi_araddr_reg[3]_rep_n_0 ),
        .I2(slv_reg4[0]),
        .I3(\axi_araddr_reg[2]_rep_n_0 ),
        .I4(sel0[2]),
        .I5(\axi_rdata[0]_i_9_n_0 ),
        .O(\axi_rdata[0]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'h04FF0400)) 
    \axi_rdata[0]_i_8 
       (.I0(\axi_araddr_reg[2]_rep_n_0 ),
        .I1(slv_reg12__0),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(sel0[2]),
        .I4(\axi_rdata[0]_i_10_n_0 ),
        .O(\axi_rdata[0]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_9 
       (.I0(slv_reg3[0]),
        .I1(slv_reg2[0]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg1[0]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg0[0]),
        .O(\axi_rdata[0]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hB888FFFFB8880000)) 
    \axi_rdata[10]_i_1 
       (.I0(\axi_rdata_reg[10]_i_2_n_0 ),
        .I1(sel0[3]),
        .I2(sel0[2]),
        .I3(\axi_rdata[10]_i_3_n_0 ),
        .I4(sel0[4]),
        .I5(\axi_rdata_reg[10]_i_4_n_0 ),
        .O(reg_data_out__0[10]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[10]_i_10 
       (.I0(slv_reg10[10]),
        .I1(\axi_araddr_reg[3]_rep_n_0 ),
        .I2(slv_reg9[10]),
        .I3(\axi_araddr_reg[2]_rep_n_0 ),
        .I4(slv_reg8__0[10]),
        .O(\axi_rdata[10]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[10]_i_3 
       (.I0(slv_reg23[10]),
        .I1(slv_reg22[10]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg21[10]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg20[10]),
        .O(\axi_rdata[10]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[10]_i_5 
       (.I0(slv_reg27[10]),
        .I1(slv_reg26[10]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg25[10]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg24[10]),
        .O(\axi_rdata[10]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[10]_i_6 
       (.I0(slv_reg31[10]),
        .I1(slv_reg30[10]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg29[10]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg28[10]),
        .O(\axi_rdata[10]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h8830FFFF88300000)) 
    \axi_rdata[10]_i_7 
       (.I0(slv_reg7__0[10]),
        .I1(\axi_araddr_reg[3]_rep_n_0 ),
        .I2(slv_reg4[10]),
        .I3(\axi_araddr_reg[2]_rep_n_0 ),
        .I4(sel0[2]),
        .I5(\axi_rdata[10]_i_9_n_0 ),
        .O(\axi_rdata[10]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'h04FF0400)) 
    \axi_rdata[10]_i_8 
       (.I0(\axi_araddr_reg[2]_rep_n_0 ),
        .I1(slv_reg12[10]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(sel0[2]),
        .I4(\axi_rdata[10]_i_10_n_0 ),
        .O(\axi_rdata[10]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[10]_i_9 
       (.I0(slv_reg3[10]),
        .I1(slv_reg2[10]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg1[10]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg0[10]),
        .O(\axi_rdata[10]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hB888FFFFB8880000)) 
    \axi_rdata[11]_i_1 
       (.I0(\axi_rdata_reg[11]_i_2_n_0 ),
        .I1(sel0[3]),
        .I2(sel0[2]),
        .I3(\axi_rdata[11]_i_3_n_0 ),
        .I4(sel0[4]),
        .I5(\axi_rdata_reg[11]_i_4_n_0 ),
        .O(reg_data_out__0[11]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[11]_i_10 
       (.I0(slv_reg10[11]),
        .I1(\axi_araddr_reg[3]_rep_n_0 ),
        .I2(slv_reg9[11]),
        .I3(\axi_araddr_reg[2]_rep_n_0 ),
        .I4(slv_reg8__0[11]),
        .O(\axi_rdata[11]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[11]_i_3 
       (.I0(slv_reg23[11]),
        .I1(slv_reg22[11]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg21[11]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg20[11]),
        .O(\axi_rdata[11]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[11]_i_5 
       (.I0(slv_reg27[11]),
        .I1(slv_reg26[11]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg25[11]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg24[11]),
        .O(\axi_rdata[11]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[11]_i_6 
       (.I0(slv_reg31[11]),
        .I1(slv_reg30[11]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg29[11]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg28[11]),
        .O(\axi_rdata[11]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h8830FFFF88300000)) 
    \axi_rdata[11]_i_7 
       (.I0(slv_reg7__0[11]),
        .I1(\axi_araddr_reg[3]_rep_n_0 ),
        .I2(slv_reg4[11]),
        .I3(\axi_araddr_reg[2]_rep_n_0 ),
        .I4(sel0[2]),
        .I5(\axi_rdata[11]_i_9_n_0 ),
        .O(\axi_rdata[11]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'h04FF0400)) 
    \axi_rdata[11]_i_8 
       (.I0(\axi_araddr_reg[2]_rep_n_0 ),
        .I1(slv_reg12[11]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(sel0[2]),
        .I4(\axi_rdata[11]_i_10_n_0 ),
        .O(\axi_rdata[11]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[11]_i_9 
       (.I0(slv_reg3[11]),
        .I1(slv_reg2[11]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg1[11]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg0[11]),
        .O(\axi_rdata[11]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hB888FFFFB8880000)) 
    \axi_rdata[12]_i_1 
       (.I0(\axi_rdata_reg[12]_i_2_n_0 ),
        .I1(sel0[3]),
        .I2(sel0[2]),
        .I3(\axi_rdata[12]_i_3_n_0 ),
        .I4(sel0[4]),
        .I5(\axi_rdata_reg[12]_i_4_n_0 ),
        .O(reg_data_out__0[12]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[12]_i_10 
       (.I0(slv_reg10[12]),
        .I1(\axi_araddr_reg[3]_rep_n_0 ),
        .I2(slv_reg9[12]),
        .I3(\axi_araddr_reg[2]_rep_n_0 ),
        .I4(slv_reg8__0[12]),
        .O(\axi_rdata[12]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[12]_i_3 
       (.I0(slv_reg23[12]),
        .I1(slv_reg22[12]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg21[12]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg20[12]),
        .O(\axi_rdata[12]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[12]_i_5 
       (.I0(slv_reg27[12]),
        .I1(slv_reg26[12]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg25[12]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg24[12]),
        .O(\axi_rdata[12]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[12]_i_6 
       (.I0(slv_reg31[12]),
        .I1(slv_reg30[12]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg29[12]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg28[12]),
        .O(\axi_rdata[12]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h8830FFFF88300000)) 
    \axi_rdata[12]_i_7 
       (.I0(slv_reg7__0[12]),
        .I1(\axi_araddr_reg[3]_rep_n_0 ),
        .I2(slv_reg4[12]),
        .I3(\axi_araddr_reg[2]_rep_n_0 ),
        .I4(sel0[2]),
        .I5(\axi_rdata[12]_i_9_n_0 ),
        .O(\axi_rdata[12]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'h04FF0400)) 
    \axi_rdata[12]_i_8 
       (.I0(\axi_araddr_reg[2]_rep_n_0 ),
        .I1(slv_reg12[12]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(sel0[2]),
        .I4(\axi_rdata[12]_i_10_n_0 ),
        .O(\axi_rdata[12]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[12]_i_9 
       (.I0(slv_reg3[12]),
        .I1(slv_reg2[12]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg1[12]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg0[12]),
        .O(\axi_rdata[12]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hB888FFFFB8880000)) 
    \axi_rdata[13]_i_1 
       (.I0(\axi_rdata_reg[13]_i_2_n_0 ),
        .I1(sel0[3]),
        .I2(sel0[2]),
        .I3(\axi_rdata[13]_i_3_n_0 ),
        .I4(sel0[4]),
        .I5(\axi_rdata_reg[13]_i_4_n_0 ),
        .O(reg_data_out__0[13]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[13]_i_10 
       (.I0(slv_reg10[13]),
        .I1(sel0[1]),
        .I2(slv_reg9[13]),
        .I3(sel0[0]),
        .I4(slv_reg8__0[13]),
        .O(\axi_rdata[13]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[13]_i_3 
       (.I0(slv_reg23[13]),
        .I1(slv_reg22[13]),
        .I2(sel0[1]),
        .I3(slv_reg21[13]),
        .I4(sel0[0]),
        .I5(slv_reg20[13]),
        .O(\axi_rdata[13]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[13]_i_5 
       (.I0(slv_reg27[13]),
        .I1(slv_reg26[13]),
        .I2(sel0[1]),
        .I3(slv_reg25[13]),
        .I4(sel0[0]),
        .I5(slv_reg24[13]),
        .O(\axi_rdata[13]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[13]_i_6 
       (.I0(slv_reg31[13]),
        .I1(slv_reg30[13]),
        .I2(sel0[1]),
        .I3(slv_reg29[13]),
        .I4(sel0[0]),
        .I5(slv_reg28[13]),
        .O(\axi_rdata[13]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h8830FFFF88300000)) 
    \axi_rdata[13]_i_7 
       (.I0(slv_reg7__0[13]),
        .I1(sel0[1]),
        .I2(slv_reg4[13]),
        .I3(sel0[0]),
        .I4(sel0[2]),
        .I5(\axi_rdata[13]_i_9_n_0 ),
        .O(\axi_rdata[13]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'h04FF0400)) 
    \axi_rdata[13]_i_8 
       (.I0(sel0[0]),
        .I1(slv_reg12[13]),
        .I2(sel0[1]),
        .I3(sel0[2]),
        .I4(\axi_rdata[13]_i_10_n_0 ),
        .O(\axi_rdata[13]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[13]_i_9 
       (.I0(slv_reg3[13]),
        .I1(slv_reg2[13]),
        .I2(sel0[1]),
        .I3(slv_reg1[13]),
        .I4(sel0[0]),
        .I5(slv_reg0[13]),
        .O(\axi_rdata[13]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hB888FFFFB8880000)) 
    \axi_rdata[14]_i_1 
       (.I0(\axi_rdata_reg[14]_i_2_n_0 ),
        .I1(sel0[3]),
        .I2(sel0[2]),
        .I3(\axi_rdata[14]_i_3_n_0 ),
        .I4(sel0[4]),
        .I5(\axi_rdata_reg[14]_i_4_n_0 ),
        .O(reg_data_out__0[14]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[14]_i_10 
       (.I0(slv_reg10[14]),
        .I1(sel0[1]),
        .I2(slv_reg9[14]),
        .I3(sel0[0]),
        .I4(slv_reg8__0[14]),
        .O(\axi_rdata[14]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[14]_i_3 
       (.I0(slv_reg23[14]),
        .I1(slv_reg22[14]),
        .I2(sel0[1]),
        .I3(slv_reg21[14]),
        .I4(sel0[0]),
        .I5(slv_reg20[14]),
        .O(\axi_rdata[14]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[14]_i_5 
       (.I0(slv_reg27[14]),
        .I1(slv_reg26[14]),
        .I2(sel0[1]),
        .I3(slv_reg25[14]),
        .I4(sel0[0]),
        .I5(slv_reg24[14]),
        .O(\axi_rdata[14]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[14]_i_6 
       (.I0(slv_reg31[14]),
        .I1(slv_reg30[14]),
        .I2(sel0[1]),
        .I3(slv_reg29[14]),
        .I4(sel0[0]),
        .I5(slv_reg28[14]),
        .O(\axi_rdata[14]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h8830FFFF88300000)) 
    \axi_rdata[14]_i_7 
       (.I0(slv_reg7__0[14]),
        .I1(sel0[1]),
        .I2(slv_reg4[14]),
        .I3(sel0[0]),
        .I4(sel0[2]),
        .I5(\axi_rdata[14]_i_9_n_0 ),
        .O(\axi_rdata[14]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'h04FF0400)) 
    \axi_rdata[14]_i_8 
       (.I0(sel0[0]),
        .I1(slv_reg12[14]),
        .I2(sel0[1]),
        .I3(sel0[2]),
        .I4(\axi_rdata[14]_i_10_n_0 ),
        .O(\axi_rdata[14]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[14]_i_9 
       (.I0(slv_reg3[14]),
        .I1(slv_reg2[14]),
        .I2(sel0[1]),
        .I3(slv_reg1[14]),
        .I4(sel0[0]),
        .I5(slv_reg0[14]),
        .O(\axi_rdata[14]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hB888FFFFB8880000)) 
    \axi_rdata[15]_i_1 
       (.I0(\axi_rdata_reg[15]_i_2_n_0 ),
        .I1(sel0[3]),
        .I2(sel0[2]),
        .I3(\axi_rdata[15]_i_3_n_0 ),
        .I4(sel0[4]),
        .I5(\axi_rdata_reg[15]_i_4_n_0 ),
        .O(reg_data_out__0[15]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[15]_i_10 
       (.I0(slv_reg10[15]),
        .I1(sel0[1]),
        .I2(slv_reg9[15]),
        .I3(sel0[0]),
        .I4(slv_reg8__0[15]),
        .O(\axi_rdata[15]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[15]_i_3 
       (.I0(slv_reg23[15]),
        .I1(slv_reg22[15]),
        .I2(sel0[1]),
        .I3(slv_reg21[15]),
        .I4(sel0[0]),
        .I5(slv_reg20[15]),
        .O(\axi_rdata[15]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[15]_i_5 
       (.I0(slv_reg27[15]),
        .I1(slv_reg26[15]),
        .I2(sel0[1]),
        .I3(slv_reg25[15]),
        .I4(sel0[0]),
        .I5(slv_reg24[15]),
        .O(\axi_rdata[15]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[15]_i_6 
       (.I0(slv_reg31[15]),
        .I1(slv_reg30[15]),
        .I2(sel0[1]),
        .I3(slv_reg29[15]),
        .I4(sel0[0]),
        .I5(slv_reg28[15]),
        .O(\axi_rdata[15]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h8830FFFF88300000)) 
    \axi_rdata[15]_i_7 
       (.I0(slv_reg7__0[15]),
        .I1(sel0[1]),
        .I2(slv_reg4[15]),
        .I3(sel0[0]),
        .I4(sel0[2]),
        .I5(\axi_rdata[15]_i_9_n_0 ),
        .O(\axi_rdata[15]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'h04FF0400)) 
    \axi_rdata[15]_i_8 
       (.I0(sel0[0]),
        .I1(slv_reg12[15]),
        .I2(sel0[1]),
        .I3(sel0[2]),
        .I4(\axi_rdata[15]_i_10_n_0 ),
        .O(\axi_rdata[15]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[15]_i_9 
       (.I0(slv_reg3[15]),
        .I1(slv_reg2[15]),
        .I2(sel0[1]),
        .I3(slv_reg1[15]),
        .I4(sel0[0]),
        .I5(slv_reg0[15]),
        .O(\axi_rdata[15]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hB888FFFFB8880000)) 
    \axi_rdata[16]_i_1 
       (.I0(\axi_rdata_reg[16]_i_2_n_0 ),
        .I1(sel0[3]),
        .I2(sel0[2]),
        .I3(\axi_rdata[16]_i_3_n_0 ),
        .I4(sel0[4]),
        .I5(\axi_rdata_reg[16]_i_4_n_0 ),
        .O(reg_data_out__0[16]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[16]_i_10 
       (.I0(slv_reg10__0[16]),
        .I1(sel0[1]),
        .I2(slv_reg9__0[16]),
        .I3(sel0[0]),
        .I4(slv_reg8__0[16]),
        .O(\axi_rdata[16]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[16]_i_3 
       (.I0(slv_reg23[16]),
        .I1(slv_reg22[16]),
        .I2(sel0[1]),
        .I3(slv_reg21[16]),
        .I4(sel0[0]),
        .I5(slv_reg20[16]),
        .O(\axi_rdata[16]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[16]_i_5 
       (.I0(slv_reg27[16]),
        .I1(slv_reg26[16]),
        .I2(sel0[1]),
        .I3(slv_reg25[16]),
        .I4(sel0[0]),
        .I5(slv_reg24[16]),
        .O(\axi_rdata[16]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[16]_i_6 
       (.I0(slv_reg31[16]),
        .I1(slv_reg30[16]),
        .I2(sel0[1]),
        .I3(slv_reg29[16]),
        .I4(sel0[0]),
        .I5(slv_reg28[16]),
        .O(\axi_rdata[16]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h8830FFFF88300000)) 
    \axi_rdata[16]_i_7 
       (.I0(slv_reg7__0[16]),
        .I1(sel0[1]),
        .I2(slv_reg4[16]),
        .I3(sel0[0]),
        .I4(sel0[2]),
        .I5(\axi_rdata[16]_i_9_n_0 ),
        .O(\axi_rdata[16]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'h04FF0400)) 
    \axi_rdata[16]_i_8 
       (.I0(sel0[0]),
        .I1(slv_reg12[16]),
        .I2(sel0[1]),
        .I3(sel0[2]),
        .I4(\axi_rdata[16]_i_10_n_0 ),
        .O(\axi_rdata[16]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[16]_i_9 
       (.I0(slv_reg3[16]),
        .I1(slv_reg2[16]),
        .I2(sel0[1]),
        .I3(slv_reg1[16]),
        .I4(sel0[0]),
        .I5(slv_reg0[16]),
        .O(\axi_rdata[16]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hB888FFFFB8880000)) 
    \axi_rdata[17]_i_1 
       (.I0(\axi_rdata_reg[17]_i_2_n_0 ),
        .I1(sel0[3]),
        .I2(sel0[2]),
        .I3(\axi_rdata[17]_i_3_n_0 ),
        .I4(sel0[4]),
        .I5(\axi_rdata_reg[17]_i_4_n_0 ),
        .O(reg_data_out__0[17]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[17]_i_10 
       (.I0(slv_reg10__0[17]),
        .I1(sel0[1]),
        .I2(slv_reg9__0[17]),
        .I3(sel0[0]),
        .I4(slv_reg8__0[17]),
        .O(\axi_rdata[17]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[17]_i_3 
       (.I0(slv_reg23[17]),
        .I1(slv_reg22[17]),
        .I2(sel0[1]),
        .I3(slv_reg21[17]),
        .I4(sel0[0]),
        .I5(slv_reg20[17]),
        .O(\axi_rdata[17]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[17]_i_5 
       (.I0(slv_reg27[17]),
        .I1(slv_reg26[17]),
        .I2(sel0[1]),
        .I3(slv_reg25[17]),
        .I4(sel0[0]),
        .I5(slv_reg24[17]),
        .O(\axi_rdata[17]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[17]_i_6 
       (.I0(slv_reg31[17]),
        .I1(slv_reg30[17]),
        .I2(sel0[1]),
        .I3(slv_reg29[17]),
        .I4(sel0[0]),
        .I5(slv_reg28[17]),
        .O(\axi_rdata[17]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h8830FFFF88300000)) 
    \axi_rdata[17]_i_7 
       (.I0(slv_reg7__0[17]),
        .I1(sel0[1]),
        .I2(slv_reg4[17]),
        .I3(sel0[0]),
        .I4(sel0[2]),
        .I5(\axi_rdata[17]_i_9_n_0 ),
        .O(\axi_rdata[17]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'h04FF0400)) 
    \axi_rdata[17]_i_8 
       (.I0(sel0[0]),
        .I1(slv_reg12[17]),
        .I2(sel0[1]),
        .I3(sel0[2]),
        .I4(\axi_rdata[17]_i_10_n_0 ),
        .O(\axi_rdata[17]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[17]_i_9 
       (.I0(slv_reg3[17]),
        .I1(slv_reg2[17]),
        .I2(sel0[1]),
        .I3(slv_reg1[17]),
        .I4(sel0[0]),
        .I5(slv_reg0[17]),
        .O(\axi_rdata[17]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hB888FFFFB8880000)) 
    \axi_rdata[18]_i_1 
       (.I0(\axi_rdata_reg[18]_i_2_n_0 ),
        .I1(sel0[3]),
        .I2(sel0[2]),
        .I3(\axi_rdata[18]_i_3_n_0 ),
        .I4(sel0[4]),
        .I5(\axi_rdata_reg[18]_i_4_n_0 ),
        .O(reg_data_out__0[18]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[18]_i_10 
       (.I0(slv_reg10__0[18]),
        .I1(sel0[1]),
        .I2(slv_reg9__0[18]),
        .I3(sel0[0]),
        .I4(slv_reg8__0[18]),
        .O(\axi_rdata[18]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[18]_i_3 
       (.I0(slv_reg23[18]),
        .I1(slv_reg22[18]),
        .I2(sel0[1]),
        .I3(slv_reg21[18]),
        .I4(sel0[0]),
        .I5(slv_reg20[18]),
        .O(\axi_rdata[18]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[18]_i_5 
       (.I0(slv_reg27[18]),
        .I1(slv_reg26[18]),
        .I2(sel0[1]),
        .I3(slv_reg25[18]),
        .I4(sel0[0]),
        .I5(slv_reg24[18]),
        .O(\axi_rdata[18]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[18]_i_6 
       (.I0(slv_reg31[18]),
        .I1(slv_reg30[18]),
        .I2(sel0[1]),
        .I3(slv_reg29[18]),
        .I4(sel0[0]),
        .I5(slv_reg28[18]),
        .O(\axi_rdata[18]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h8830FFFF88300000)) 
    \axi_rdata[18]_i_7 
       (.I0(slv_reg7__0[18]),
        .I1(sel0[1]),
        .I2(slv_reg4[18]),
        .I3(sel0[0]),
        .I4(sel0[2]),
        .I5(\axi_rdata[18]_i_9_n_0 ),
        .O(\axi_rdata[18]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'h04FF0400)) 
    \axi_rdata[18]_i_8 
       (.I0(sel0[0]),
        .I1(slv_reg12[18]),
        .I2(sel0[1]),
        .I3(sel0[2]),
        .I4(\axi_rdata[18]_i_10_n_0 ),
        .O(\axi_rdata[18]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[18]_i_9 
       (.I0(slv_reg3[18]),
        .I1(slv_reg2[18]),
        .I2(sel0[1]),
        .I3(slv_reg1[18]),
        .I4(sel0[0]),
        .I5(slv_reg0[18]),
        .O(\axi_rdata[18]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hB888FFFFB8880000)) 
    \axi_rdata[19]_i_1 
       (.I0(\axi_rdata_reg[19]_i_2_n_0 ),
        .I1(sel0[3]),
        .I2(sel0[2]),
        .I3(\axi_rdata[19]_i_3_n_0 ),
        .I4(sel0[4]),
        .I5(\axi_rdata_reg[19]_i_4_n_0 ),
        .O(reg_data_out__0[19]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[19]_i_10 
       (.I0(slv_reg10__0[19]),
        .I1(sel0[1]),
        .I2(slv_reg9__0[19]),
        .I3(sel0[0]),
        .I4(slv_reg8__0[19]),
        .O(\axi_rdata[19]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[19]_i_3 
       (.I0(slv_reg23[19]),
        .I1(slv_reg22[19]),
        .I2(sel0[1]),
        .I3(slv_reg21[19]),
        .I4(sel0[0]),
        .I5(slv_reg20[19]),
        .O(\axi_rdata[19]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[19]_i_5 
       (.I0(slv_reg27[19]),
        .I1(slv_reg26[19]),
        .I2(sel0[1]),
        .I3(slv_reg25[19]),
        .I4(sel0[0]),
        .I5(slv_reg24[19]),
        .O(\axi_rdata[19]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[19]_i_6 
       (.I0(slv_reg31[19]),
        .I1(slv_reg30[19]),
        .I2(sel0[1]),
        .I3(slv_reg29[19]),
        .I4(sel0[0]),
        .I5(slv_reg28[19]),
        .O(\axi_rdata[19]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h8830FFFF88300000)) 
    \axi_rdata[19]_i_7 
       (.I0(slv_reg7__0[19]),
        .I1(sel0[1]),
        .I2(slv_reg4[19]),
        .I3(sel0[0]),
        .I4(sel0[2]),
        .I5(\axi_rdata[19]_i_9_n_0 ),
        .O(\axi_rdata[19]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'h04FF0400)) 
    \axi_rdata[19]_i_8 
       (.I0(sel0[0]),
        .I1(slv_reg12[19]),
        .I2(sel0[1]),
        .I3(sel0[2]),
        .I4(\axi_rdata[19]_i_10_n_0 ),
        .O(\axi_rdata[19]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[19]_i_9 
       (.I0(slv_reg3[19]),
        .I1(slv_reg2[19]),
        .I2(sel0[1]),
        .I3(slv_reg1[19]),
        .I4(sel0[0]),
        .I5(slv_reg0[19]),
        .O(\axi_rdata[19]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hB888FFFFB8880000)) 
    \axi_rdata[1]_i_1 
       (.I0(\axi_rdata_reg[1]_i_2_n_0 ),
        .I1(sel0[3]),
        .I2(sel0[2]),
        .I3(\axi_rdata[1]_i_3_n_0 ),
        .I4(sel0[4]),
        .I5(\axi_rdata_reg[1]_i_4_n_0 ),
        .O(reg_data_out__0[1]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[1]_i_10 
       (.I0(slv_reg10[1]),
        .I1(\axi_araddr_reg[3]_rep_n_0 ),
        .I2(slv_reg9[1]),
        .I3(\axi_araddr_reg[2]_rep_n_0 ),
        .I4(slv_reg8[1]),
        .O(\axi_rdata[1]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[1]_i_3 
       (.I0(slv_reg23[1]),
        .I1(slv_reg22[1]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg21[1]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg20[1]),
        .O(\axi_rdata[1]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[1]_i_5 
       (.I0(slv_reg27[1]),
        .I1(slv_reg26[1]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg25[1]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg24[1]),
        .O(\axi_rdata[1]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[1]_i_6 
       (.I0(slv_reg31[1]),
        .I1(slv_reg30[1]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg29[1]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg28[1]),
        .O(\axi_rdata[1]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h8830FFFF88300000)) 
    \axi_rdata[1]_i_7 
       (.I0(slv_reg7__0[1]),
        .I1(\axi_araddr_reg[3]_rep_n_0 ),
        .I2(slv_reg4[1]),
        .I3(\axi_araddr_reg[2]_rep_n_0 ),
        .I4(sel0[2]),
        .I5(\axi_rdata[1]_i_9_n_0 ),
        .O(\axi_rdata[1]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'h04FF0400)) 
    \axi_rdata[1]_i_8 
       (.I0(\axi_araddr_reg[2]_rep_n_0 ),
        .I1(slv_reg12[1]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(sel0[2]),
        .I4(\axi_rdata[1]_i_10_n_0 ),
        .O(\axi_rdata[1]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[1]_i_9 
       (.I0(slv_reg3[1]),
        .I1(slv_reg2[1]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg1[1]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg0[1]),
        .O(\axi_rdata[1]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hB888FFFFB8880000)) 
    \axi_rdata[20]_i_1 
       (.I0(\axi_rdata_reg[20]_i_2_n_0 ),
        .I1(sel0[3]),
        .I2(sel0[2]),
        .I3(\axi_rdata[20]_i_3_n_0 ),
        .I4(sel0[4]),
        .I5(\axi_rdata_reg[20]_i_4_n_0 ),
        .O(reg_data_out__0[20]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[20]_i_10 
       (.I0(slv_reg10__0[20]),
        .I1(sel0[1]),
        .I2(slv_reg9__0[20]),
        .I3(sel0[0]),
        .I4(slv_reg8__0[20]),
        .O(\axi_rdata[20]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[20]_i_3 
       (.I0(slv_reg23[20]),
        .I1(slv_reg22[20]),
        .I2(sel0[1]),
        .I3(slv_reg21[20]),
        .I4(sel0[0]),
        .I5(slv_reg20[20]),
        .O(\axi_rdata[20]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[20]_i_5 
       (.I0(slv_reg27[20]),
        .I1(slv_reg26[20]),
        .I2(sel0[1]),
        .I3(slv_reg25[20]),
        .I4(sel0[0]),
        .I5(slv_reg24[20]),
        .O(\axi_rdata[20]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[20]_i_6 
       (.I0(slv_reg31[20]),
        .I1(slv_reg30[20]),
        .I2(sel0[1]),
        .I3(slv_reg29[20]),
        .I4(sel0[0]),
        .I5(slv_reg28[20]),
        .O(\axi_rdata[20]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h8830FFFF88300000)) 
    \axi_rdata[20]_i_7 
       (.I0(slv_reg7__0[20]),
        .I1(sel0[1]),
        .I2(slv_reg4[20]),
        .I3(sel0[0]),
        .I4(sel0[2]),
        .I5(\axi_rdata[20]_i_9_n_0 ),
        .O(\axi_rdata[20]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'h04FF0400)) 
    \axi_rdata[20]_i_8 
       (.I0(sel0[0]),
        .I1(slv_reg12[20]),
        .I2(sel0[1]),
        .I3(sel0[2]),
        .I4(\axi_rdata[20]_i_10_n_0 ),
        .O(\axi_rdata[20]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[20]_i_9 
       (.I0(slv_reg3[20]),
        .I1(slv_reg2[20]),
        .I2(sel0[1]),
        .I3(slv_reg1[20]),
        .I4(sel0[0]),
        .I5(slv_reg0[20]),
        .O(\axi_rdata[20]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hB888FFFFB8880000)) 
    \axi_rdata[21]_i_1 
       (.I0(\axi_rdata_reg[21]_i_2_n_0 ),
        .I1(sel0[3]),
        .I2(sel0[2]),
        .I3(\axi_rdata[21]_i_3_n_0 ),
        .I4(sel0[4]),
        .I5(\axi_rdata_reg[21]_i_4_n_0 ),
        .O(reg_data_out__0[21]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[21]_i_10 
       (.I0(slv_reg10__0[21]),
        .I1(sel0[1]),
        .I2(slv_reg9__0[21]),
        .I3(sel0[0]),
        .I4(slv_reg8__0[21]),
        .O(\axi_rdata[21]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[21]_i_3 
       (.I0(slv_reg23[21]),
        .I1(slv_reg22[21]),
        .I2(sel0[1]),
        .I3(slv_reg21[21]),
        .I4(sel0[0]),
        .I5(slv_reg20[21]),
        .O(\axi_rdata[21]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[21]_i_5 
       (.I0(slv_reg27[21]),
        .I1(slv_reg26[21]),
        .I2(sel0[1]),
        .I3(slv_reg25[21]),
        .I4(sel0[0]),
        .I5(slv_reg24[21]),
        .O(\axi_rdata[21]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[21]_i_6 
       (.I0(slv_reg31[21]),
        .I1(slv_reg30[21]),
        .I2(sel0[1]),
        .I3(slv_reg29[21]),
        .I4(sel0[0]),
        .I5(slv_reg28[21]),
        .O(\axi_rdata[21]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h8830FFFF88300000)) 
    \axi_rdata[21]_i_7 
       (.I0(slv_reg7__0[21]),
        .I1(sel0[1]),
        .I2(slv_reg4[21]),
        .I3(sel0[0]),
        .I4(sel0[2]),
        .I5(\axi_rdata[21]_i_9_n_0 ),
        .O(\axi_rdata[21]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'h04FF0400)) 
    \axi_rdata[21]_i_8 
       (.I0(sel0[0]),
        .I1(slv_reg12[21]),
        .I2(sel0[1]),
        .I3(sel0[2]),
        .I4(\axi_rdata[21]_i_10_n_0 ),
        .O(\axi_rdata[21]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[21]_i_9 
       (.I0(slv_reg3[21]),
        .I1(slv_reg2[21]),
        .I2(sel0[1]),
        .I3(slv_reg1[21]),
        .I4(sel0[0]),
        .I5(slv_reg0[21]),
        .O(\axi_rdata[21]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hB888FFFFB8880000)) 
    \axi_rdata[22]_i_1 
       (.I0(\axi_rdata_reg[22]_i_2_n_0 ),
        .I1(sel0[3]),
        .I2(sel0[2]),
        .I3(\axi_rdata[22]_i_3_n_0 ),
        .I4(sel0[4]),
        .I5(\axi_rdata_reg[22]_i_4_n_0 ),
        .O(reg_data_out__0[22]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[22]_i_10 
       (.I0(slv_reg10__0[22]),
        .I1(sel0[1]),
        .I2(slv_reg9__0[22]),
        .I3(sel0[0]),
        .I4(slv_reg8__0[22]),
        .O(\axi_rdata[22]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[22]_i_3 
       (.I0(slv_reg23[22]),
        .I1(slv_reg22[22]),
        .I2(sel0[1]),
        .I3(slv_reg21[22]),
        .I4(sel0[0]),
        .I5(slv_reg20[22]),
        .O(\axi_rdata[22]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[22]_i_5 
       (.I0(slv_reg27[22]),
        .I1(slv_reg26[22]),
        .I2(sel0[1]),
        .I3(slv_reg25[22]),
        .I4(sel0[0]),
        .I5(slv_reg24[22]),
        .O(\axi_rdata[22]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[22]_i_6 
       (.I0(slv_reg31[22]),
        .I1(slv_reg30[22]),
        .I2(sel0[1]),
        .I3(slv_reg29[22]),
        .I4(sel0[0]),
        .I5(slv_reg28[22]),
        .O(\axi_rdata[22]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h8830FFFF88300000)) 
    \axi_rdata[22]_i_7 
       (.I0(slv_reg7__0[22]),
        .I1(sel0[1]),
        .I2(slv_reg4[22]),
        .I3(sel0[0]),
        .I4(sel0[2]),
        .I5(\axi_rdata[22]_i_9_n_0 ),
        .O(\axi_rdata[22]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'h04FF0400)) 
    \axi_rdata[22]_i_8 
       (.I0(sel0[0]),
        .I1(slv_reg12[22]),
        .I2(sel0[1]),
        .I3(sel0[2]),
        .I4(\axi_rdata[22]_i_10_n_0 ),
        .O(\axi_rdata[22]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[22]_i_9 
       (.I0(slv_reg3[22]),
        .I1(slv_reg2[22]),
        .I2(sel0[1]),
        .I3(slv_reg1[22]),
        .I4(sel0[0]),
        .I5(slv_reg0[22]),
        .O(\axi_rdata[22]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hB888FFFFB8880000)) 
    \axi_rdata[23]_i_1 
       (.I0(\axi_rdata_reg[23]_i_2_n_0 ),
        .I1(sel0[3]),
        .I2(sel0[2]),
        .I3(\axi_rdata[23]_i_3_n_0 ),
        .I4(sel0[4]),
        .I5(\axi_rdata_reg[23]_i_4_n_0 ),
        .O(reg_data_out__0[23]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[23]_i_10 
       (.I0(slv_reg10__0[23]),
        .I1(sel0[1]),
        .I2(slv_reg9__0[23]),
        .I3(sel0[0]),
        .I4(slv_reg8__0[23]),
        .O(\axi_rdata[23]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[23]_i_3 
       (.I0(slv_reg23[23]),
        .I1(slv_reg22[23]),
        .I2(sel0[1]),
        .I3(slv_reg21[23]),
        .I4(sel0[0]),
        .I5(slv_reg20[23]),
        .O(\axi_rdata[23]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[23]_i_5 
       (.I0(slv_reg27[23]),
        .I1(slv_reg26[23]),
        .I2(sel0[1]),
        .I3(slv_reg25[23]),
        .I4(sel0[0]),
        .I5(slv_reg24[23]),
        .O(\axi_rdata[23]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[23]_i_6 
       (.I0(slv_reg31[23]),
        .I1(slv_reg30[23]),
        .I2(sel0[1]),
        .I3(slv_reg29[23]),
        .I4(sel0[0]),
        .I5(slv_reg28[23]),
        .O(\axi_rdata[23]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h8830FFFF88300000)) 
    \axi_rdata[23]_i_7 
       (.I0(slv_reg7__0[23]),
        .I1(sel0[1]),
        .I2(slv_reg4[23]),
        .I3(sel0[0]),
        .I4(sel0[2]),
        .I5(\axi_rdata[23]_i_9_n_0 ),
        .O(\axi_rdata[23]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'h04FF0400)) 
    \axi_rdata[23]_i_8 
       (.I0(sel0[0]),
        .I1(slv_reg12[23]),
        .I2(sel0[1]),
        .I3(sel0[2]),
        .I4(\axi_rdata[23]_i_10_n_0 ),
        .O(\axi_rdata[23]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[23]_i_9 
       (.I0(slv_reg3[23]),
        .I1(slv_reg2[23]),
        .I2(sel0[1]),
        .I3(slv_reg1[23]),
        .I4(sel0[0]),
        .I5(slv_reg0[23]),
        .O(\axi_rdata[23]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hB888FFFFB8880000)) 
    \axi_rdata[24]_i_1 
       (.I0(\axi_rdata_reg[24]_i_2_n_0 ),
        .I1(sel0[3]),
        .I2(sel0[2]),
        .I3(\axi_rdata[24]_i_3_n_0 ),
        .I4(sel0[4]),
        .I5(\axi_rdata_reg[24]_i_4_n_0 ),
        .O(reg_data_out__0[24]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[24]_i_10 
       (.I0(slv_reg10__0[24]),
        .I1(sel0[1]),
        .I2(slv_reg9__0[24]),
        .I3(sel0[0]),
        .I4(slv_reg8__0[24]),
        .O(\axi_rdata[24]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[24]_i_3 
       (.I0(slv_reg23[24]),
        .I1(slv_reg22[24]),
        .I2(sel0[1]),
        .I3(slv_reg21[24]),
        .I4(sel0[0]),
        .I5(slv_reg20[24]),
        .O(\axi_rdata[24]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[24]_i_5 
       (.I0(slv_reg27[24]),
        .I1(slv_reg26[24]),
        .I2(sel0[1]),
        .I3(slv_reg25[24]),
        .I4(sel0[0]),
        .I5(slv_reg24[24]),
        .O(\axi_rdata[24]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[24]_i_6 
       (.I0(slv_reg31[24]),
        .I1(slv_reg30[24]),
        .I2(sel0[1]),
        .I3(slv_reg29[24]),
        .I4(sel0[0]),
        .I5(slv_reg28[24]),
        .O(\axi_rdata[24]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h8830FFFF88300000)) 
    \axi_rdata[24]_i_7 
       (.I0(slv_reg7__0[24]),
        .I1(sel0[1]),
        .I2(slv_reg4[24]),
        .I3(sel0[0]),
        .I4(sel0[2]),
        .I5(\axi_rdata[24]_i_9_n_0 ),
        .O(\axi_rdata[24]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'h04FF0400)) 
    \axi_rdata[24]_i_8 
       (.I0(sel0[0]),
        .I1(slv_reg12[24]),
        .I2(sel0[1]),
        .I3(sel0[2]),
        .I4(\axi_rdata[24]_i_10_n_0 ),
        .O(\axi_rdata[24]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[24]_i_9 
       (.I0(slv_reg3[24]),
        .I1(slv_reg2[24]),
        .I2(sel0[1]),
        .I3(slv_reg1[24]),
        .I4(sel0[0]),
        .I5(slv_reg0[24]),
        .O(\axi_rdata[24]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hB888FFFFB8880000)) 
    \axi_rdata[25]_i_1 
       (.I0(\axi_rdata_reg[25]_i_2_n_0 ),
        .I1(sel0[3]),
        .I2(sel0[2]),
        .I3(\axi_rdata[25]_i_3_n_0 ),
        .I4(sel0[4]),
        .I5(\axi_rdata_reg[25]_i_4_n_0 ),
        .O(reg_data_out__0[25]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[25]_i_10 
       (.I0(slv_reg10__0[25]),
        .I1(sel0[1]),
        .I2(slv_reg9__0[25]),
        .I3(sel0[0]),
        .I4(slv_reg8__0[25]),
        .O(\axi_rdata[25]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[25]_i_3 
       (.I0(slv_reg23[25]),
        .I1(slv_reg22[25]),
        .I2(sel0[1]),
        .I3(slv_reg21[25]),
        .I4(sel0[0]),
        .I5(slv_reg20[25]),
        .O(\axi_rdata[25]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[25]_i_5 
       (.I0(slv_reg27[25]),
        .I1(slv_reg26[25]),
        .I2(sel0[1]),
        .I3(slv_reg25[25]),
        .I4(sel0[0]),
        .I5(slv_reg24[25]),
        .O(\axi_rdata[25]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[25]_i_6 
       (.I0(slv_reg31[25]),
        .I1(slv_reg30[25]),
        .I2(sel0[1]),
        .I3(slv_reg29[25]),
        .I4(sel0[0]),
        .I5(slv_reg28[25]),
        .O(\axi_rdata[25]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h8830FFFF88300000)) 
    \axi_rdata[25]_i_7 
       (.I0(slv_reg7__0[25]),
        .I1(sel0[1]),
        .I2(slv_reg4[25]),
        .I3(sel0[0]),
        .I4(sel0[2]),
        .I5(\axi_rdata[25]_i_9_n_0 ),
        .O(\axi_rdata[25]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'h04FF0400)) 
    \axi_rdata[25]_i_8 
       (.I0(sel0[0]),
        .I1(slv_reg12[25]),
        .I2(sel0[1]),
        .I3(sel0[2]),
        .I4(\axi_rdata[25]_i_10_n_0 ),
        .O(\axi_rdata[25]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[25]_i_9 
       (.I0(slv_reg3[25]),
        .I1(slv_reg2[25]),
        .I2(sel0[1]),
        .I3(slv_reg1[25]),
        .I4(sel0[0]),
        .I5(slv_reg0[25]),
        .O(\axi_rdata[25]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hB888FFFFB8880000)) 
    \axi_rdata[26]_i_1 
       (.I0(\axi_rdata_reg[26]_i_2_n_0 ),
        .I1(sel0[3]),
        .I2(sel0[2]),
        .I3(\axi_rdata[26]_i_3_n_0 ),
        .I4(sel0[4]),
        .I5(\axi_rdata_reg[26]_i_4_n_0 ),
        .O(reg_data_out__0[26]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[26]_i_10 
       (.I0(slv_reg10__0[26]),
        .I1(sel0[1]),
        .I2(slv_reg9__0[26]),
        .I3(sel0[0]),
        .I4(slv_reg8__0[26]),
        .O(\axi_rdata[26]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[26]_i_3 
       (.I0(slv_reg23[26]),
        .I1(slv_reg22[26]),
        .I2(sel0[1]),
        .I3(slv_reg21[26]),
        .I4(sel0[0]),
        .I5(slv_reg20[26]),
        .O(\axi_rdata[26]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[26]_i_5 
       (.I0(slv_reg27[26]),
        .I1(slv_reg26[26]),
        .I2(sel0[1]),
        .I3(slv_reg25[26]),
        .I4(sel0[0]),
        .I5(slv_reg24[26]),
        .O(\axi_rdata[26]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[26]_i_6 
       (.I0(slv_reg31[26]),
        .I1(slv_reg30[26]),
        .I2(sel0[1]),
        .I3(slv_reg29[26]),
        .I4(sel0[0]),
        .I5(slv_reg28[26]),
        .O(\axi_rdata[26]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h8830FFFF88300000)) 
    \axi_rdata[26]_i_7 
       (.I0(slv_reg7__0[26]),
        .I1(sel0[1]),
        .I2(slv_reg4[26]),
        .I3(sel0[0]),
        .I4(sel0[2]),
        .I5(\axi_rdata[26]_i_9_n_0 ),
        .O(\axi_rdata[26]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'h04FF0400)) 
    \axi_rdata[26]_i_8 
       (.I0(sel0[0]),
        .I1(slv_reg12[26]),
        .I2(sel0[1]),
        .I3(sel0[2]),
        .I4(\axi_rdata[26]_i_10_n_0 ),
        .O(\axi_rdata[26]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[26]_i_9 
       (.I0(slv_reg3[26]),
        .I1(slv_reg2[26]),
        .I2(sel0[1]),
        .I3(slv_reg1[26]),
        .I4(sel0[0]),
        .I5(slv_reg0[26]),
        .O(\axi_rdata[26]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hB888FFFFB8880000)) 
    \axi_rdata[27]_i_1 
       (.I0(\axi_rdata_reg[27]_i_2_n_0 ),
        .I1(sel0[3]),
        .I2(sel0[2]),
        .I3(\axi_rdata[27]_i_3_n_0 ),
        .I4(sel0[4]),
        .I5(\axi_rdata_reg[27]_i_4_n_0 ),
        .O(reg_data_out__0[27]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[27]_i_10 
       (.I0(slv_reg10__0[27]),
        .I1(sel0[1]),
        .I2(slv_reg9__0[27]),
        .I3(sel0[0]),
        .I4(slv_reg8__0[27]),
        .O(\axi_rdata[27]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[27]_i_3 
       (.I0(slv_reg23[27]),
        .I1(slv_reg22[27]),
        .I2(sel0[1]),
        .I3(slv_reg21[27]),
        .I4(sel0[0]),
        .I5(slv_reg20[27]),
        .O(\axi_rdata[27]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[27]_i_5 
       (.I0(slv_reg27[27]),
        .I1(slv_reg26[27]),
        .I2(sel0[1]),
        .I3(slv_reg25[27]),
        .I4(sel0[0]),
        .I5(slv_reg24[27]),
        .O(\axi_rdata[27]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[27]_i_6 
       (.I0(slv_reg31[27]),
        .I1(slv_reg30[27]),
        .I2(sel0[1]),
        .I3(slv_reg29[27]),
        .I4(sel0[0]),
        .I5(slv_reg28[27]),
        .O(\axi_rdata[27]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h8830FFFF88300000)) 
    \axi_rdata[27]_i_7 
       (.I0(slv_reg7__0[27]),
        .I1(sel0[1]),
        .I2(slv_reg4[27]),
        .I3(sel0[0]),
        .I4(sel0[2]),
        .I5(\axi_rdata[27]_i_9_n_0 ),
        .O(\axi_rdata[27]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'h04FF0400)) 
    \axi_rdata[27]_i_8 
       (.I0(sel0[0]),
        .I1(slv_reg12[27]),
        .I2(sel0[1]),
        .I3(sel0[2]),
        .I4(\axi_rdata[27]_i_10_n_0 ),
        .O(\axi_rdata[27]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[27]_i_9 
       (.I0(slv_reg3[27]),
        .I1(slv_reg2[27]),
        .I2(sel0[1]),
        .I3(slv_reg1[27]),
        .I4(sel0[0]),
        .I5(slv_reg0[27]),
        .O(\axi_rdata[27]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hB888FFFFB8880000)) 
    \axi_rdata[28]_i_1 
       (.I0(\axi_rdata_reg[28]_i_2_n_0 ),
        .I1(sel0[3]),
        .I2(sel0[2]),
        .I3(\axi_rdata[28]_i_3_n_0 ),
        .I4(sel0[4]),
        .I5(\axi_rdata_reg[28]_i_4_n_0 ),
        .O(reg_data_out__0[28]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[28]_i_10 
       (.I0(slv_reg10__0[28]),
        .I1(sel0[1]),
        .I2(slv_reg9__0[28]),
        .I3(sel0[0]),
        .I4(slv_reg8__0[28]),
        .O(\axi_rdata[28]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[28]_i_3 
       (.I0(slv_reg23[28]),
        .I1(slv_reg22[28]),
        .I2(sel0[1]),
        .I3(slv_reg21[28]),
        .I4(sel0[0]),
        .I5(slv_reg20[28]),
        .O(\axi_rdata[28]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[28]_i_5 
       (.I0(slv_reg27[28]),
        .I1(slv_reg26[28]),
        .I2(sel0[1]),
        .I3(slv_reg25[28]),
        .I4(sel0[0]),
        .I5(slv_reg24[28]),
        .O(\axi_rdata[28]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[28]_i_6 
       (.I0(slv_reg31[28]),
        .I1(slv_reg30[28]),
        .I2(sel0[1]),
        .I3(slv_reg29[28]),
        .I4(sel0[0]),
        .I5(slv_reg28[28]),
        .O(\axi_rdata[28]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h8830FFFF88300000)) 
    \axi_rdata[28]_i_7 
       (.I0(slv_reg7__0[28]),
        .I1(sel0[1]),
        .I2(slv_reg4[28]),
        .I3(sel0[0]),
        .I4(sel0[2]),
        .I5(\axi_rdata[28]_i_9_n_0 ),
        .O(\axi_rdata[28]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'h04FF0400)) 
    \axi_rdata[28]_i_8 
       (.I0(sel0[0]),
        .I1(slv_reg12[28]),
        .I2(sel0[1]),
        .I3(sel0[2]),
        .I4(\axi_rdata[28]_i_10_n_0 ),
        .O(\axi_rdata[28]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[28]_i_9 
       (.I0(slv_reg3[28]),
        .I1(slv_reg2[28]),
        .I2(sel0[1]),
        .I3(slv_reg1[28]),
        .I4(sel0[0]),
        .I5(slv_reg0[28]),
        .O(\axi_rdata[28]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hB888FFFFB8880000)) 
    \axi_rdata[29]_i_1 
       (.I0(\axi_rdata_reg[29]_i_2_n_0 ),
        .I1(sel0[3]),
        .I2(sel0[2]),
        .I3(\axi_rdata[29]_i_3_n_0 ),
        .I4(sel0[4]),
        .I5(\axi_rdata_reg[29]_i_4_n_0 ),
        .O(reg_data_out__0[29]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[29]_i_10 
       (.I0(slv_reg10__0[29]),
        .I1(sel0[1]),
        .I2(slv_reg9__0[29]),
        .I3(sel0[0]),
        .I4(slv_reg8__0[29]),
        .O(\axi_rdata[29]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[29]_i_3 
       (.I0(slv_reg23[29]),
        .I1(slv_reg22[29]),
        .I2(sel0[1]),
        .I3(slv_reg21[29]),
        .I4(sel0[0]),
        .I5(slv_reg20[29]),
        .O(\axi_rdata[29]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[29]_i_5 
       (.I0(slv_reg27[29]),
        .I1(slv_reg26[29]),
        .I2(sel0[1]),
        .I3(slv_reg25[29]),
        .I4(sel0[0]),
        .I5(slv_reg24[29]),
        .O(\axi_rdata[29]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[29]_i_6 
       (.I0(slv_reg31[29]),
        .I1(slv_reg30[29]),
        .I2(sel0[1]),
        .I3(slv_reg29[29]),
        .I4(sel0[0]),
        .I5(slv_reg28[29]),
        .O(\axi_rdata[29]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h8830FFFF88300000)) 
    \axi_rdata[29]_i_7 
       (.I0(slv_reg7__0[29]),
        .I1(sel0[1]),
        .I2(slv_reg4[29]),
        .I3(sel0[0]),
        .I4(sel0[2]),
        .I5(\axi_rdata[29]_i_9_n_0 ),
        .O(\axi_rdata[29]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'h04FF0400)) 
    \axi_rdata[29]_i_8 
       (.I0(sel0[0]),
        .I1(slv_reg12[29]),
        .I2(sel0[1]),
        .I3(sel0[2]),
        .I4(\axi_rdata[29]_i_10_n_0 ),
        .O(\axi_rdata[29]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[29]_i_9 
       (.I0(slv_reg3[29]),
        .I1(slv_reg2[29]),
        .I2(sel0[1]),
        .I3(slv_reg1[29]),
        .I4(sel0[0]),
        .I5(slv_reg0[29]),
        .O(\axi_rdata[29]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hB888FFFFB8880000)) 
    \axi_rdata[2]_i_1 
       (.I0(\axi_rdata_reg[2]_i_2_n_0 ),
        .I1(sel0[3]),
        .I2(sel0[2]),
        .I3(\axi_rdata[2]_i_3_n_0 ),
        .I4(sel0[4]),
        .I5(\axi_rdata_reg[2]_i_4_n_0 ),
        .O(reg_data_out__0[2]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[2]_i_10 
       (.I0(slv_reg10[2]),
        .I1(\axi_araddr_reg[3]_rep_n_0 ),
        .I2(slv_reg9[2]),
        .I3(\axi_araddr_reg[2]_rep_n_0 ),
        .I4(slv_reg8[2]),
        .O(\axi_rdata[2]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[2]_i_3 
       (.I0(slv_reg23[2]),
        .I1(slv_reg22[2]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg21[2]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg20[2]),
        .O(\axi_rdata[2]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[2]_i_5 
       (.I0(slv_reg27[2]),
        .I1(slv_reg26[2]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg25[2]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg24[2]),
        .O(\axi_rdata[2]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[2]_i_6 
       (.I0(slv_reg31[2]),
        .I1(slv_reg30[2]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg29[2]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg28[2]),
        .O(\axi_rdata[2]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h8830FFFF88300000)) 
    \axi_rdata[2]_i_7 
       (.I0(slv_reg7__0[2]),
        .I1(\axi_araddr_reg[3]_rep_n_0 ),
        .I2(slv_reg4[2]),
        .I3(\axi_araddr_reg[2]_rep_n_0 ),
        .I4(sel0[2]),
        .I5(\axi_rdata[2]_i_9_n_0 ),
        .O(\axi_rdata[2]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'h04FF0400)) 
    \axi_rdata[2]_i_8 
       (.I0(\axi_araddr_reg[2]_rep_n_0 ),
        .I1(slv_reg12[2]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(sel0[2]),
        .I4(\axi_rdata[2]_i_10_n_0 ),
        .O(\axi_rdata[2]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[2]_i_9 
       (.I0(slv_reg3[2]),
        .I1(slv_reg2[2]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg1[2]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg0[2]),
        .O(\axi_rdata[2]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hB888FFFFB8880000)) 
    \axi_rdata[30]_i_1 
       (.I0(\axi_rdata_reg[30]_i_2_n_0 ),
        .I1(sel0[3]),
        .I2(sel0[2]),
        .I3(\axi_rdata[30]_i_3_n_0 ),
        .I4(sel0[4]),
        .I5(\axi_rdata_reg[30]_i_4_n_0 ),
        .O(reg_data_out__0[30]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[30]_i_10 
       (.I0(slv_reg10__0[30]),
        .I1(sel0[1]),
        .I2(slv_reg9__0[30]),
        .I3(sel0[0]),
        .I4(slv_reg8__0[30]),
        .O(\axi_rdata[30]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[30]_i_3 
       (.I0(slv_reg23[30]),
        .I1(slv_reg22[30]),
        .I2(sel0[1]),
        .I3(slv_reg21[30]),
        .I4(sel0[0]),
        .I5(slv_reg20[30]),
        .O(\axi_rdata[30]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[30]_i_5 
       (.I0(slv_reg27[30]),
        .I1(slv_reg26[30]),
        .I2(sel0[1]),
        .I3(slv_reg25[30]),
        .I4(sel0[0]),
        .I5(slv_reg24[30]),
        .O(\axi_rdata[30]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[30]_i_6 
       (.I0(slv_reg31[30]),
        .I1(slv_reg30[30]),
        .I2(sel0[1]),
        .I3(slv_reg29[30]),
        .I4(sel0[0]),
        .I5(slv_reg28[30]),
        .O(\axi_rdata[30]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h8830FFFF88300000)) 
    \axi_rdata[30]_i_7 
       (.I0(slv_reg7__0[30]),
        .I1(sel0[1]),
        .I2(slv_reg4[30]),
        .I3(sel0[0]),
        .I4(sel0[2]),
        .I5(\axi_rdata[30]_i_9_n_0 ),
        .O(\axi_rdata[30]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'h04FF0400)) 
    \axi_rdata[30]_i_8 
       (.I0(sel0[0]),
        .I1(slv_reg12[30]),
        .I2(sel0[1]),
        .I3(sel0[2]),
        .I4(\axi_rdata[30]_i_10_n_0 ),
        .O(\axi_rdata[30]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[30]_i_9 
       (.I0(slv_reg3[30]),
        .I1(slv_reg2[30]),
        .I2(sel0[1]),
        .I3(slv_reg1[30]),
        .I4(sel0[0]),
        .I5(slv_reg0[30]),
        .O(\axi_rdata[30]_i_9_n_0 ));
  LUT3 #(
    .INIT(8'h08)) 
    \axi_rdata[31]_i_1 
       (.I0(s00_axi_arready),
        .I1(s00_axi_arvalid),
        .I2(s00_axi_rvalid),
        .O(slv_reg_rden));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[31]_i_10 
       (.I0(slv_reg3[31]),
        .I1(slv_reg2[31]),
        .I2(sel0[1]),
        .I3(slv_reg1[31]),
        .I4(sel0[0]),
        .I5(slv_reg0[31]),
        .O(\axi_rdata[31]_i_10_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[31]_i_11 
       (.I0(slv_reg10__0[31]),
        .I1(sel0[1]),
        .I2(slv_reg9__0[31]),
        .I3(sel0[0]),
        .I4(slv_reg8__0[31]),
        .O(\axi_rdata[31]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hB888FFFFB8880000)) 
    \axi_rdata[31]_i_2 
       (.I0(\axi_rdata_reg[31]_i_3_n_0 ),
        .I1(sel0[3]),
        .I2(sel0[2]),
        .I3(\axi_rdata[31]_i_4_n_0 ),
        .I4(sel0[4]),
        .I5(\axi_rdata_reg[31]_i_5_n_0 ),
        .O(reg_data_out__0[31]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[31]_i_4 
       (.I0(slv_reg23[31]),
        .I1(slv_reg22[31]),
        .I2(sel0[1]),
        .I3(slv_reg21[31]),
        .I4(sel0[0]),
        .I5(slv_reg20[31]),
        .O(\axi_rdata[31]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[31]_i_6 
       (.I0(slv_reg27[31]),
        .I1(slv_reg26[31]),
        .I2(sel0[1]),
        .I3(slv_reg25[31]),
        .I4(sel0[0]),
        .I5(slv_reg24[31]),
        .O(\axi_rdata[31]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[31]_i_7 
       (.I0(slv_reg31[31]),
        .I1(slv_reg30[31]),
        .I2(sel0[1]),
        .I3(slv_reg29[31]),
        .I4(sel0[0]),
        .I5(slv_reg28[31]),
        .O(\axi_rdata[31]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'h8830FFFF88300000)) 
    \axi_rdata[31]_i_8 
       (.I0(slv_reg7__0[31]),
        .I1(sel0[1]),
        .I2(slv_reg4[31]),
        .I3(sel0[0]),
        .I4(sel0[2]),
        .I5(\axi_rdata[31]_i_10_n_0 ),
        .O(\axi_rdata[31]_i_8_n_0 ));
  LUT5 #(
    .INIT(32'h04FF0400)) 
    \axi_rdata[31]_i_9 
       (.I0(sel0[0]),
        .I1(slv_reg12[31]),
        .I2(sel0[1]),
        .I3(sel0[2]),
        .I4(\axi_rdata[31]_i_11_n_0 ),
        .O(\axi_rdata[31]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hB888FFFFB8880000)) 
    \axi_rdata[3]_i_1 
       (.I0(\axi_rdata_reg[3]_i_2_n_0 ),
        .I1(sel0[3]),
        .I2(sel0[2]),
        .I3(\axi_rdata[3]_i_3_n_0 ),
        .I4(sel0[4]),
        .I5(\axi_rdata_reg[3]_i_4_n_0 ),
        .O(reg_data_out__0[3]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[3]_i_10 
       (.I0(slv_reg10[3]),
        .I1(\axi_araddr_reg[3]_rep_n_0 ),
        .I2(slv_reg9[3]),
        .I3(\axi_araddr_reg[2]_rep_n_0 ),
        .I4(slv_reg8[3]),
        .O(\axi_rdata[3]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[3]_i_3 
       (.I0(slv_reg23[3]),
        .I1(slv_reg22[3]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg21[3]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg20[3]),
        .O(\axi_rdata[3]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[3]_i_5 
       (.I0(slv_reg27[3]),
        .I1(slv_reg26[3]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg25[3]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg24[3]),
        .O(\axi_rdata[3]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[3]_i_6 
       (.I0(slv_reg31[3]),
        .I1(slv_reg30[3]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg29[3]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg28[3]),
        .O(\axi_rdata[3]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h8830FFFF88300000)) 
    \axi_rdata[3]_i_7 
       (.I0(slv_reg7__0[3]),
        .I1(\axi_araddr_reg[3]_rep_n_0 ),
        .I2(slv_reg4[3]),
        .I3(\axi_araddr_reg[2]_rep_n_0 ),
        .I4(sel0[2]),
        .I5(\axi_rdata[3]_i_9_n_0 ),
        .O(\axi_rdata[3]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'h04FF0400)) 
    \axi_rdata[3]_i_8 
       (.I0(\axi_araddr_reg[2]_rep_n_0 ),
        .I1(slv_reg12[3]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(sel0[2]),
        .I4(\axi_rdata[3]_i_10_n_0 ),
        .O(\axi_rdata[3]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[3]_i_9 
       (.I0(slv_reg3[3]),
        .I1(slv_reg2[3]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg1[3]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg0[3]),
        .O(\axi_rdata[3]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hB888FFFFB8880000)) 
    \axi_rdata[4]_i_1 
       (.I0(\axi_rdata_reg[4]_i_2_n_0 ),
        .I1(sel0[3]),
        .I2(sel0[2]),
        .I3(\axi_rdata[4]_i_3_n_0 ),
        .I4(sel0[4]),
        .I5(\axi_rdata_reg[4]_i_4_n_0 ),
        .O(reg_data_out__0[4]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[4]_i_10 
       (.I0(slv_reg10[4]),
        .I1(\axi_araddr_reg[3]_rep_n_0 ),
        .I2(slv_reg9[4]),
        .I3(\axi_araddr_reg[2]_rep_n_0 ),
        .I4(slv_reg8[4]),
        .O(\axi_rdata[4]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[4]_i_3 
       (.I0(slv_reg23[4]),
        .I1(slv_reg22[4]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg21[4]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg20[4]),
        .O(\axi_rdata[4]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[4]_i_5 
       (.I0(slv_reg27[4]),
        .I1(slv_reg26[4]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg25[4]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg24[4]),
        .O(\axi_rdata[4]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[4]_i_6 
       (.I0(slv_reg31[4]),
        .I1(slv_reg30[4]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg29[4]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg28[4]),
        .O(\axi_rdata[4]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h8830FFFF88300000)) 
    \axi_rdata[4]_i_7 
       (.I0(slv_reg7__0[4]),
        .I1(\axi_araddr_reg[3]_rep_n_0 ),
        .I2(slv_reg4[4]),
        .I3(\axi_araddr_reg[2]_rep_n_0 ),
        .I4(sel0[2]),
        .I5(\axi_rdata[4]_i_9_n_0 ),
        .O(\axi_rdata[4]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'h04FF0400)) 
    \axi_rdata[4]_i_8 
       (.I0(\axi_araddr_reg[2]_rep_n_0 ),
        .I1(slv_reg12[4]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(sel0[2]),
        .I4(\axi_rdata[4]_i_10_n_0 ),
        .O(\axi_rdata[4]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[4]_i_9 
       (.I0(slv_reg3[4]),
        .I1(slv_reg2[4]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg1[4]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg0[4]),
        .O(\axi_rdata[4]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hB888FFFFB8880000)) 
    \axi_rdata[5]_i_1 
       (.I0(\axi_rdata_reg[5]_i_2_n_0 ),
        .I1(sel0[3]),
        .I2(sel0[2]),
        .I3(\axi_rdata[5]_i_3_n_0 ),
        .I4(sel0[4]),
        .I5(\axi_rdata_reg[5]_i_4_n_0 ),
        .O(reg_data_out__0[5]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[5]_i_10 
       (.I0(slv_reg10[5]),
        .I1(\axi_araddr_reg[3]_rep_n_0 ),
        .I2(slv_reg9[5]),
        .I3(\axi_araddr_reg[2]_rep_n_0 ),
        .I4(slv_reg8[5]),
        .O(\axi_rdata[5]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[5]_i_3 
       (.I0(slv_reg23[5]),
        .I1(slv_reg22[5]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg21[5]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg20[5]),
        .O(\axi_rdata[5]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[5]_i_5 
       (.I0(slv_reg27[5]),
        .I1(slv_reg26[5]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg25[5]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg24[5]),
        .O(\axi_rdata[5]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[5]_i_6 
       (.I0(slv_reg31[5]),
        .I1(slv_reg30[5]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg29[5]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg28[5]),
        .O(\axi_rdata[5]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h8830FFFF88300000)) 
    \axi_rdata[5]_i_7 
       (.I0(slv_reg7__0[5]),
        .I1(\axi_araddr_reg[3]_rep_n_0 ),
        .I2(slv_reg4[5]),
        .I3(\axi_araddr_reg[2]_rep_n_0 ),
        .I4(sel0[2]),
        .I5(\axi_rdata[5]_i_9_n_0 ),
        .O(\axi_rdata[5]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'h04FF0400)) 
    \axi_rdata[5]_i_8 
       (.I0(\axi_araddr_reg[2]_rep_n_0 ),
        .I1(slv_reg12[5]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(sel0[2]),
        .I4(\axi_rdata[5]_i_10_n_0 ),
        .O(\axi_rdata[5]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[5]_i_9 
       (.I0(slv_reg3[5]),
        .I1(slv_reg2[5]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg1[5]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg0[5]),
        .O(\axi_rdata[5]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hB888FFFFB8880000)) 
    \axi_rdata[6]_i_1 
       (.I0(\axi_rdata_reg[6]_i_2_n_0 ),
        .I1(sel0[3]),
        .I2(sel0[2]),
        .I3(\axi_rdata[6]_i_3_n_0 ),
        .I4(sel0[4]),
        .I5(\axi_rdata_reg[6]_i_4_n_0 ),
        .O(reg_data_out__0[6]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[6]_i_10 
       (.I0(slv_reg10[6]),
        .I1(\axi_araddr_reg[3]_rep_n_0 ),
        .I2(slv_reg9[6]),
        .I3(\axi_araddr_reg[2]_rep_n_0 ),
        .I4(slv_reg8[6]),
        .O(\axi_rdata[6]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[6]_i_3 
       (.I0(slv_reg23[6]),
        .I1(slv_reg22[6]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg21[6]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg20[6]),
        .O(\axi_rdata[6]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[6]_i_5 
       (.I0(slv_reg27[6]),
        .I1(slv_reg26[6]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg25[6]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg24[6]),
        .O(\axi_rdata[6]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[6]_i_6 
       (.I0(slv_reg31[6]),
        .I1(slv_reg30[6]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg29[6]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg28[6]),
        .O(\axi_rdata[6]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h8830FFFF88300000)) 
    \axi_rdata[6]_i_7 
       (.I0(slv_reg7__0[6]),
        .I1(\axi_araddr_reg[3]_rep_n_0 ),
        .I2(slv_reg4[6]),
        .I3(\axi_araddr_reg[2]_rep_n_0 ),
        .I4(sel0[2]),
        .I5(\axi_rdata[6]_i_9_n_0 ),
        .O(\axi_rdata[6]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'h04FF0400)) 
    \axi_rdata[6]_i_8 
       (.I0(\axi_araddr_reg[2]_rep_n_0 ),
        .I1(slv_reg12[6]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(sel0[2]),
        .I4(\axi_rdata[6]_i_10_n_0 ),
        .O(\axi_rdata[6]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[6]_i_9 
       (.I0(slv_reg3[6]),
        .I1(slv_reg2[6]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg1[6]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg0[6]),
        .O(\axi_rdata[6]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hB888FFFFB8880000)) 
    \axi_rdata[7]_i_1 
       (.I0(\axi_rdata_reg[7]_i_2_n_0 ),
        .I1(sel0[3]),
        .I2(sel0[2]),
        .I3(\axi_rdata[7]_i_3_n_0 ),
        .I4(sel0[4]),
        .I5(\axi_rdata_reg[7]_i_4_n_0 ),
        .O(reg_data_out__0[7]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[7]_i_10 
       (.I0(slv_reg10[7]),
        .I1(\axi_araddr_reg[3]_rep_n_0 ),
        .I2(slv_reg9[7]),
        .I3(\axi_araddr_reg[2]_rep_n_0 ),
        .I4(slv_reg8[7]),
        .O(\axi_rdata[7]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[7]_i_3 
       (.I0(slv_reg23[7]),
        .I1(slv_reg22[7]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg21[7]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg20[7]),
        .O(\axi_rdata[7]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[7]_i_5 
       (.I0(slv_reg27[7]),
        .I1(slv_reg26[7]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg25[7]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg24[7]),
        .O(\axi_rdata[7]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[7]_i_6 
       (.I0(slv_reg31[7]),
        .I1(slv_reg30[7]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg29[7]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg28[7]),
        .O(\axi_rdata[7]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h8830FFFF88300000)) 
    \axi_rdata[7]_i_7 
       (.I0(slv_reg7__0[7]),
        .I1(\axi_araddr_reg[3]_rep_n_0 ),
        .I2(slv_reg4[7]),
        .I3(\axi_araddr_reg[2]_rep_n_0 ),
        .I4(sel0[2]),
        .I5(\axi_rdata[7]_i_9_n_0 ),
        .O(\axi_rdata[7]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'h04FF0400)) 
    \axi_rdata[7]_i_8 
       (.I0(\axi_araddr_reg[2]_rep_n_0 ),
        .I1(slv_reg12[7]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(sel0[2]),
        .I4(\axi_rdata[7]_i_10_n_0 ),
        .O(\axi_rdata[7]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[7]_i_9 
       (.I0(slv_reg3[7]),
        .I1(slv_reg2[7]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg1[7]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg0[7]),
        .O(\axi_rdata[7]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hB888FFFFB8880000)) 
    \axi_rdata[8]_i_1 
       (.I0(\axi_rdata_reg[8]_i_2_n_0 ),
        .I1(sel0[3]),
        .I2(sel0[2]),
        .I3(\axi_rdata[8]_i_3_n_0 ),
        .I4(sel0[4]),
        .I5(\axi_rdata_reg[8]_i_4_n_0 ),
        .O(reg_data_out__0[8]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[8]_i_10 
       (.I0(slv_reg10[8]),
        .I1(\axi_araddr_reg[3]_rep_n_0 ),
        .I2(slv_reg9[8]),
        .I3(\axi_araddr_reg[2]_rep_n_0 ),
        .I4(slv_reg8[8]),
        .O(\axi_rdata[8]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[8]_i_3 
       (.I0(slv_reg23[8]),
        .I1(slv_reg22[8]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg21[8]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg20[8]),
        .O(\axi_rdata[8]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[8]_i_5 
       (.I0(slv_reg27[8]),
        .I1(slv_reg26[8]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg25[8]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg24[8]),
        .O(\axi_rdata[8]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[8]_i_6 
       (.I0(slv_reg31[8]),
        .I1(slv_reg30[8]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg29[8]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg28[8]),
        .O(\axi_rdata[8]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h8830FFFF88300000)) 
    \axi_rdata[8]_i_7 
       (.I0(slv_reg7__0[8]),
        .I1(\axi_araddr_reg[3]_rep_n_0 ),
        .I2(slv_reg4[8]),
        .I3(\axi_araddr_reg[2]_rep_n_0 ),
        .I4(sel0[2]),
        .I5(\axi_rdata[8]_i_9_n_0 ),
        .O(\axi_rdata[8]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'h04FF0400)) 
    \axi_rdata[8]_i_8 
       (.I0(\axi_araddr_reg[2]_rep_n_0 ),
        .I1(slv_reg12[8]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(sel0[2]),
        .I4(\axi_rdata[8]_i_10_n_0 ),
        .O(\axi_rdata[8]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[8]_i_9 
       (.I0(slv_reg3[8]),
        .I1(slv_reg2[8]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg1[8]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg0[8]),
        .O(\axi_rdata[8]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hB888FFFFB8880000)) 
    \axi_rdata[9]_i_1 
       (.I0(\axi_rdata_reg[9]_i_2_n_0 ),
        .I1(sel0[3]),
        .I2(sel0[2]),
        .I3(\axi_rdata[9]_i_3_n_0 ),
        .I4(sel0[4]),
        .I5(\axi_rdata_reg[9]_i_4_n_0 ),
        .O(reg_data_out__0[9]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[9]_i_10 
       (.I0(slv_reg10[9]),
        .I1(\axi_araddr_reg[3]_rep_n_0 ),
        .I2(slv_reg9[9]),
        .I3(\axi_araddr_reg[2]_rep_n_0 ),
        .I4(slv_reg8[9]),
        .O(\axi_rdata[9]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[9]_i_3 
       (.I0(slv_reg23[9]),
        .I1(slv_reg22[9]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg21[9]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg20[9]),
        .O(\axi_rdata[9]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[9]_i_5 
       (.I0(slv_reg27[9]),
        .I1(slv_reg26[9]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg25[9]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg24[9]),
        .O(\axi_rdata[9]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[9]_i_6 
       (.I0(slv_reg31[9]),
        .I1(slv_reg30[9]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg29[9]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg28[9]),
        .O(\axi_rdata[9]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h8830FFFF88300000)) 
    \axi_rdata[9]_i_7 
       (.I0(slv_reg7__0[9]),
        .I1(\axi_araddr_reg[3]_rep_n_0 ),
        .I2(slv_reg4[9]),
        .I3(\axi_araddr_reg[2]_rep_n_0 ),
        .I4(sel0[2]),
        .I5(\axi_rdata[9]_i_9_n_0 ),
        .O(\axi_rdata[9]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'h04FF0400)) 
    \axi_rdata[9]_i_8 
       (.I0(\axi_araddr_reg[2]_rep_n_0 ),
        .I1(slv_reg12[9]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(sel0[2]),
        .I4(\axi_rdata[9]_i_10_n_0 ),
        .O(\axi_rdata[9]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[9]_i_9 
       (.I0(slv_reg3[9]),
        .I1(slv_reg2[9]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg1[9]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg0[9]),
        .O(\axi_rdata[9]_i_9_n_0 ));
  FDRE \axi_rdata_reg[0] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[0]),
        .Q(s00_axi_rdata[0]),
        .R(RST));
  MUXF7 \axi_rdata_reg[0]_i_2 
       (.I0(\axi_rdata[0]_i_5_n_0 ),
        .I1(\axi_rdata[0]_i_6_n_0 ),
        .O(\axi_rdata_reg[0]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[0]_i_4 
       (.I0(\axi_rdata[0]_i_7_n_0 ),
        .I1(\axi_rdata[0]_i_8_n_0 ),
        .O(\axi_rdata_reg[0]_i_4_n_0 ),
        .S(sel0[3]));
  FDRE \axi_rdata_reg[10] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[10]),
        .Q(s00_axi_rdata[10]),
        .R(RST));
  MUXF7 \axi_rdata_reg[10]_i_2 
       (.I0(\axi_rdata[10]_i_5_n_0 ),
        .I1(\axi_rdata[10]_i_6_n_0 ),
        .O(\axi_rdata_reg[10]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[10]_i_4 
       (.I0(\axi_rdata[10]_i_7_n_0 ),
        .I1(\axi_rdata[10]_i_8_n_0 ),
        .O(\axi_rdata_reg[10]_i_4_n_0 ),
        .S(sel0[3]));
  FDRE \axi_rdata_reg[11] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[11]),
        .Q(s00_axi_rdata[11]),
        .R(RST));
  MUXF7 \axi_rdata_reg[11]_i_2 
       (.I0(\axi_rdata[11]_i_5_n_0 ),
        .I1(\axi_rdata[11]_i_6_n_0 ),
        .O(\axi_rdata_reg[11]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[11]_i_4 
       (.I0(\axi_rdata[11]_i_7_n_0 ),
        .I1(\axi_rdata[11]_i_8_n_0 ),
        .O(\axi_rdata_reg[11]_i_4_n_0 ),
        .S(sel0[3]));
  FDRE \axi_rdata_reg[12] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[12]),
        .Q(s00_axi_rdata[12]),
        .R(RST));
  MUXF7 \axi_rdata_reg[12]_i_2 
       (.I0(\axi_rdata[12]_i_5_n_0 ),
        .I1(\axi_rdata[12]_i_6_n_0 ),
        .O(\axi_rdata_reg[12]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[12]_i_4 
       (.I0(\axi_rdata[12]_i_7_n_0 ),
        .I1(\axi_rdata[12]_i_8_n_0 ),
        .O(\axi_rdata_reg[12]_i_4_n_0 ),
        .S(sel0[3]));
  FDRE \axi_rdata_reg[13] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[13]),
        .Q(s00_axi_rdata[13]),
        .R(RST));
  MUXF7 \axi_rdata_reg[13]_i_2 
       (.I0(\axi_rdata[13]_i_5_n_0 ),
        .I1(\axi_rdata[13]_i_6_n_0 ),
        .O(\axi_rdata_reg[13]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[13]_i_4 
       (.I0(\axi_rdata[13]_i_7_n_0 ),
        .I1(\axi_rdata[13]_i_8_n_0 ),
        .O(\axi_rdata_reg[13]_i_4_n_0 ),
        .S(sel0[3]));
  FDRE \axi_rdata_reg[14] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[14]),
        .Q(s00_axi_rdata[14]),
        .R(RST));
  MUXF7 \axi_rdata_reg[14]_i_2 
       (.I0(\axi_rdata[14]_i_5_n_0 ),
        .I1(\axi_rdata[14]_i_6_n_0 ),
        .O(\axi_rdata_reg[14]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[14]_i_4 
       (.I0(\axi_rdata[14]_i_7_n_0 ),
        .I1(\axi_rdata[14]_i_8_n_0 ),
        .O(\axi_rdata_reg[14]_i_4_n_0 ),
        .S(sel0[3]));
  FDRE \axi_rdata_reg[15] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[15]),
        .Q(s00_axi_rdata[15]),
        .R(RST));
  MUXF7 \axi_rdata_reg[15]_i_2 
       (.I0(\axi_rdata[15]_i_5_n_0 ),
        .I1(\axi_rdata[15]_i_6_n_0 ),
        .O(\axi_rdata_reg[15]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[15]_i_4 
       (.I0(\axi_rdata[15]_i_7_n_0 ),
        .I1(\axi_rdata[15]_i_8_n_0 ),
        .O(\axi_rdata_reg[15]_i_4_n_0 ),
        .S(sel0[3]));
  FDRE \axi_rdata_reg[16] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[16]),
        .Q(s00_axi_rdata[16]),
        .R(RST));
  MUXF7 \axi_rdata_reg[16]_i_2 
       (.I0(\axi_rdata[16]_i_5_n_0 ),
        .I1(\axi_rdata[16]_i_6_n_0 ),
        .O(\axi_rdata_reg[16]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[16]_i_4 
       (.I0(\axi_rdata[16]_i_7_n_0 ),
        .I1(\axi_rdata[16]_i_8_n_0 ),
        .O(\axi_rdata_reg[16]_i_4_n_0 ),
        .S(sel0[3]));
  FDRE \axi_rdata_reg[17] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[17]),
        .Q(s00_axi_rdata[17]),
        .R(RST));
  MUXF7 \axi_rdata_reg[17]_i_2 
       (.I0(\axi_rdata[17]_i_5_n_0 ),
        .I1(\axi_rdata[17]_i_6_n_0 ),
        .O(\axi_rdata_reg[17]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[17]_i_4 
       (.I0(\axi_rdata[17]_i_7_n_0 ),
        .I1(\axi_rdata[17]_i_8_n_0 ),
        .O(\axi_rdata_reg[17]_i_4_n_0 ),
        .S(sel0[3]));
  FDRE \axi_rdata_reg[18] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[18]),
        .Q(s00_axi_rdata[18]),
        .R(RST));
  MUXF7 \axi_rdata_reg[18]_i_2 
       (.I0(\axi_rdata[18]_i_5_n_0 ),
        .I1(\axi_rdata[18]_i_6_n_0 ),
        .O(\axi_rdata_reg[18]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[18]_i_4 
       (.I0(\axi_rdata[18]_i_7_n_0 ),
        .I1(\axi_rdata[18]_i_8_n_0 ),
        .O(\axi_rdata_reg[18]_i_4_n_0 ),
        .S(sel0[3]));
  FDRE \axi_rdata_reg[19] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[19]),
        .Q(s00_axi_rdata[19]),
        .R(RST));
  MUXF7 \axi_rdata_reg[19]_i_2 
       (.I0(\axi_rdata[19]_i_5_n_0 ),
        .I1(\axi_rdata[19]_i_6_n_0 ),
        .O(\axi_rdata_reg[19]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[19]_i_4 
       (.I0(\axi_rdata[19]_i_7_n_0 ),
        .I1(\axi_rdata[19]_i_8_n_0 ),
        .O(\axi_rdata_reg[19]_i_4_n_0 ),
        .S(sel0[3]));
  FDRE \axi_rdata_reg[1] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[1]),
        .Q(s00_axi_rdata[1]),
        .R(RST));
  MUXF7 \axi_rdata_reg[1]_i_2 
       (.I0(\axi_rdata[1]_i_5_n_0 ),
        .I1(\axi_rdata[1]_i_6_n_0 ),
        .O(\axi_rdata_reg[1]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[1]_i_4 
       (.I0(\axi_rdata[1]_i_7_n_0 ),
        .I1(\axi_rdata[1]_i_8_n_0 ),
        .O(\axi_rdata_reg[1]_i_4_n_0 ),
        .S(sel0[3]));
  FDRE \axi_rdata_reg[20] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[20]),
        .Q(s00_axi_rdata[20]),
        .R(RST));
  MUXF7 \axi_rdata_reg[20]_i_2 
       (.I0(\axi_rdata[20]_i_5_n_0 ),
        .I1(\axi_rdata[20]_i_6_n_0 ),
        .O(\axi_rdata_reg[20]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[20]_i_4 
       (.I0(\axi_rdata[20]_i_7_n_0 ),
        .I1(\axi_rdata[20]_i_8_n_0 ),
        .O(\axi_rdata_reg[20]_i_4_n_0 ),
        .S(sel0[3]));
  FDRE \axi_rdata_reg[21] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[21]),
        .Q(s00_axi_rdata[21]),
        .R(RST));
  MUXF7 \axi_rdata_reg[21]_i_2 
       (.I0(\axi_rdata[21]_i_5_n_0 ),
        .I1(\axi_rdata[21]_i_6_n_0 ),
        .O(\axi_rdata_reg[21]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[21]_i_4 
       (.I0(\axi_rdata[21]_i_7_n_0 ),
        .I1(\axi_rdata[21]_i_8_n_0 ),
        .O(\axi_rdata_reg[21]_i_4_n_0 ),
        .S(sel0[3]));
  FDRE \axi_rdata_reg[22] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[22]),
        .Q(s00_axi_rdata[22]),
        .R(RST));
  MUXF7 \axi_rdata_reg[22]_i_2 
       (.I0(\axi_rdata[22]_i_5_n_0 ),
        .I1(\axi_rdata[22]_i_6_n_0 ),
        .O(\axi_rdata_reg[22]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[22]_i_4 
       (.I0(\axi_rdata[22]_i_7_n_0 ),
        .I1(\axi_rdata[22]_i_8_n_0 ),
        .O(\axi_rdata_reg[22]_i_4_n_0 ),
        .S(sel0[3]));
  FDRE \axi_rdata_reg[23] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[23]),
        .Q(s00_axi_rdata[23]),
        .R(RST));
  MUXF7 \axi_rdata_reg[23]_i_2 
       (.I0(\axi_rdata[23]_i_5_n_0 ),
        .I1(\axi_rdata[23]_i_6_n_0 ),
        .O(\axi_rdata_reg[23]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[23]_i_4 
       (.I0(\axi_rdata[23]_i_7_n_0 ),
        .I1(\axi_rdata[23]_i_8_n_0 ),
        .O(\axi_rdata_reg[23]_i_4_n_0 ),
        .S(sel0[3]));
  FDRE \axi_rdata_reg[24] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[24]),
        .Q(s00_axi_rdata[24]),
        .R(RST));
  MUXF7 \axi_rdata_reg[24]_i_2 
       (.I0(\axi_rdata[24]_i_5_n_0 ),
        .I1(\axi_rdata[24]_i_6_n_0 ),
        .O(\axi_rdata_reg[24]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[24]_i_4 
       (.I0(\axi_rdata[24]_i_7_n_0 ),
        .I1(\axi_rdata[24]_i_8_n_0 ),
        .O(\axi_rdata_reg[24]_i_4_n_0 ),
        .S(sel0[3]));
  FDRE \axi_rdata_reg[25] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[25]),
        .Q(s00_axi_rdata[25]),
        .R(RST));
  MUXF7 \axi_rdata_reg[25]_i_2 
       (.I0(\axi_rdata[25]_i_5_n_0 ),
        .I1(\axi_rdata[25]_i_6_n_0 ),
        .O(\axi_rdata_reg[25]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[25]_i_4 
       (.I0(\axi_rdata[25]_i_7_n_0 ),
        .I1(\axi_rdata[25]_i_8_n_0 ),
        .O(\axi_rdata_reg[25]_i_4_n_0 ),
        .S(sel0[3]));
  FDRE \axi_rdata_reg[26] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[26]),
        .Q(s00_axi_rdata[26]),
        .R(RST));
  MUXF7 \axi_rdata_reg[26]_i_2 
       (.I0(\axi_rdata[26]_i_5_n_0 ),
        .I1(\axi_rdata[26]_i_6_n_0 ),
        .O(\axi_rdata_reg[26]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[26]_i_4 
       (.I0(\axi_rdata[26]_i_7_n_0 ),
        .I1(\axi_rdata[26]_i_8_n_0 ),
        .O(\axi_rdata_reg[26]_i_4_n_0 ),
        .S(sel0[3]));
  FDRE \axi_rdata_reg[27] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[27]),
        .Q(s00_axi_rdata[27]),
        .R(RST));
  MUXF7 \axi_rdata_reg[27]_i_2 
       (.I0(\axi_rdata[27]_i_5_n_0 ),
        .I1(\axi_rdata[27]_i_6_n_0 ),
        .O(\axi_rdata_reg[27]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[27]_i_4 
       (.I0(\axi_rdata[27]_i_7_n_0 ),
        .I1(\axi_rdata[27]_i_8_n_0 ),
        .O(\axi_rdata_reg[27]_i_4_n_0 ),
        .S(sel0[3]));
  FDRE \axi_rdata_reg[28] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[28]),
        .Q(s00_axi_rdata[28]),
        .R(RST));
  MUXF7 \axi_rdata_reg[28]_i_2 
       (.I0(\axi_rdata[28]_i_5_n_0 ),
        .I1(\axi_rdata[28]_i_6_n_0 ),
        .O(\axi_rdata_reg[28]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[28]_i_4 
       (.I0(\axi_rdata[28]_i_7_n_0 ),
        .I1(\axi_rdata[28]_i_8_n_0 ),
        .O(\axi_rdata_reg[28]_i_4_n_0 ),
        .S(sel0[3]));
  FDRE \axi_rdata_reg[29] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[29]),
        .Q(s00_axi_rdata[29]),
        .R(RST));
  MUXF7 \axi_rdata_reg[29]_i_2 
       (.I0(\axi_rdata[29]_i_5_n_0 ),
        .I1(\axi_rdata[29]_i_6_n_0 ),
        .O(\axi_rdata_reg[29]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[29]_i_4 
       (.I0(\axi_rdata[29]_i_7_n_0 ),
        .I1(\axi_rdata[29]_i_8_n_0 ),
        .O(\axi_rdata_reg[29]_i_4_n_0 ),
        .S(sel0[3]));
  FDRE \axi_rdata_reg[2] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[2]),
        .Q(s00_axi_rdata[2]),
        .R(RST));
  MUXF7 \axi_rdata_reg[2]_i_2 
       (.I0(\axi_rdata[2]_i_5_n_0 ),
        .I1(\axi_rdata[2]_i_6_n_0 ),
        .O(\axi_rdata_reg[2]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[2]_i_4 
       (.I0(\axi_rdata[2]_i_7_n_0 ),
        .I1(\axi_rdata[2]_i_8_n_0 ),
        .O(\axi_rdata_reg[2]_i_4_n_0 ),
        .S(sel0[3]));
  FDRE \axi_rdata_reg[30] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[30]),
        .Q(s00_axi_rdata[30]),
        .R(RST));
  MUXF7 \axi_rdata_reg[30]_i_2 
       (.I0(\axi_rdata[30]_i_5_n_0 ),
        .I1(\axi_rdata[30]_i_6_n_0 ),
        .O(\axi_rdata_reg[30]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[30]_i_4 
       (.I0(\axi_rdata[30]_i_7_n_0 ),
        .I1(\axi_rdata[30]_i_8_n_0 ),
        .O(\axi_rdata_reg[30]_i_4_n_0 ),
        .S(sel0[3]));
  FDRE \axi_rdata_reg[31] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[31]),
        .Q(s00_axi_rdata[31]),
        .R(RST));
  MUXF7 \axi_rdata_reg[31]_i_3 
       (.I0(\axi_rdata[31]_i_6_n_0 ),
        .I1(\axi_rdata[31]_i_7_n_0 ),
        .O(\axi_rdata_reg[31]_i_3_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[31]_i_5 
       (.I0(\axi_rdata[31]_i_8_n_0 ),
        .I1(\axi_rdata[31]_i_9_n_0 ),
        .O(\axi_rdata_reg[31]_i_5_n_0 ),
        .S(sel0[3]));
  FDRE \axi_rdata_reg[3] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[3]),
        .Q(s00_axi_rdata[3]),
        .R(RST));
  MUXF7 \axi_rdata_reg[3]_i_2 
       (.I0(\axi_rdata[3]_i_5_n_0 ),
        .I1(\axi_rdata[3]_i_6_n_0 ),
        .O(\axi_rdata_reg[3]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[3]_i_4 
       (.I0(\axi_rdata[3]_i_7_n_0 ),
        .I1(\axi_rdata[3]_i_8_n_0 ),
        .O(\axi_rdata_reg[3]_i_4_n_0 ),
        .S(sel0[3]));
  FDRE \axi_rdata_reg[4] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[4]),
        .Q(s00_axi_rdata[4]),
        .R(RST));
  MUXF7 \axi_rdata_reg[4]_i_2 
       (.I0(\axi_rdata[4]_i_5_n_0 ),
        .I1(\axi_rdata[4]_i_6_n_0 ),
        .O(\axi_rdata_reg[4]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[4]_i_4 
       (.I0(\axi_rdata[4]_i_7_n_0 ),
        .I1(\axi_rdata[4]_i_8_n_0 ),
        .O(\axi_rdata_reg[4]_i_4_n_0 ),
        .S(sel0[3]));
  FDRE \axi_rdata_reg[5] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[5]),
        .Q(s00_axi_rdata[5]),
        .R(RST));
  MUXF7 \axi_rdata_reg[5]_i_2 
       (.I0(\axi_rdata[5]_i_5_n_0 ),
        .I1(\axi_rdata[5]_i_6_n_0 ),
        .O(\axi_rdata_reg[5]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[5]_i_4 
       (.I0(\axi_rdata[5]_i_7_n_0 ),
        .I1(\axi_rdata[5]_i_8_n_0 ),
        .O(\axi_rdata_reg[5]_i_4_n_0 ),
        .S(sel0[3]));
  FDRE \axi_rdata_reg[6] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[6]),
        .Q(s00_axi_rdata[6]),
        .R(RST));
  MUXF7 \axi_rdata_reg[6]_i_2 
       (.I0(\axi_rdata[6]_i_5_n_0 ),
        .I1(\axi_rdata[6]_i_6_n_0 ),
        .O(\axi_rdata_reg[6]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[6]_i_4 
       (.I0(\axi_rdata[6]_i_7_n_0 ),
        .I1(\axi_rdata[6]_i_8_n_0 ),
        .O(\axi_rdata_reg[6]_i_4_n_0 ),
        .S(sel0[3]));
  FDRE \axi_rdata_reg[7] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[7]),
        .Q(s00_axi_rdata[7]),
        .R(RST));
  MUXF7 \axi_rdata_reg[7]_i_2 
       (.I0(\axi_rdata[7]_i_5_n_0 ),
        .I1(\axi_rdata[7]_i_6_n_0 ),
        .O(\axi_rdata_reg[7]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[7]_i_4 
       (.I0(\axi_rdata[7]_i_7_n_0 ),
        .I1(\axi_rdata[7]_i_8_n_0 ),
        .O(\axi_rdata_reg[7]_i_4_n_0 ),
        .S(sel0[3]));
  FDRE \axi_rdata_reg[8] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[8]),
        .Q(s00_axi_rdata[8]),
        .R(RST));
  MUXF7 \axi_rdata_reg[8]_i_2 
       (.I0(\axi_rdata[8]_i_5_n_0 ),
        .I1(\axi_rdata[8]_i_6_n_0 ),
        .O(\axi_rdata_reg[8]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[8]_i_4 
       (.I0(\axi_rdata[8]_i_7_n_0 ),
        .I1(\axi_rdata[8]_i_8_n_0 ),
        .O(\axi_rdata_reg[8]_i_4_n_0 ),
        .S(sel0[3]));
  FDRE \axi_rdata_reg[9] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[9]),
        .Q(s00_axi_rdata[9]),
        .R(RST));
  MUXF7 \axi_rdata_reg[9]_i_2 
       (.I0(\axi_rdata[9]_i_5_n_0 ),
        .I1(\axi_rdata[9]_i_6_n_0 ),
        .O(\axi_rdata_reg[9]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[9]_i_4 
       (.I0(\axi_rdata[9]_i_7_n_0 ),
        .I1(\axi_rdata[9]_i_8_n_0 ),
        .O(\axi_rdata_reg[9]_i_4_n_0 ),
        .S(sel0[3]));
  FDRE axi_rvalid_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_arready_reg_0),
        .Q(s00_axi_rvalid),
        .R(RST));
  LUT3 #(
    .INIT(8'h08)) 
    axi_wready_i_1
       (.I0(s00_axi_wvalid),
        .I1(s00_axi_awvalid),
        .I2(s00_axi_wready),
        .O(axi_wready0));
  FDRE axi_wready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_wready0),
        .Q(s00_axi_wready),
        .R(RST));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_lab2_datapath datapath
       (.CO(sw14_in),
        .E(fsm_n_4),
        .\FSM_onehot_state_reg[1] (sw1),
        .\FSM_onehot_state_reg[4] (w_cw),
        .Q({datapath_n_15,datapath_n_16,datapath_n_17,datapath_n_18}),
        .SR(fsm_n_0),
        .SS(RST),
        .WREN(WREN),
        .ac_adc_sdata(ac_adc_sdata),
        .ac_bclk(ac_bclk),
        .ac_dac_sdata(ac_dac_sdata),
        .ac_lrclk(ac_lrclk),
        .ac_mclk(ac_mclk),
        .btn(btn),
        .\i_sig_reg[6]_0 (datapath_n_19),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_aresetn(s00_axi_aresetn),
        .scl(scl),
        .sda(sda),
        .\slv_reg10_reg[15] (slv_reg10),
        .\slv_reg8_reg[9] (slv_reg8),
        .\slv_reg9_reg[15] (slv_reg9),
        .switch(switch),
        .tmds(tmds),
        .tmdsb(tmdsb),
        .w_sw(w_sw));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_lab2_fsm fsm
       (.CO(sw14_in),
        .E(fsm_n_4),
        .Q(w_cw),
        .SR(fsm_n_0),
        .SS(RST),
        .WREN(WREN),
        .\i_sig_reg[5] (datapath_n_19),
        .\i_sig_reg[9] ({datapath_n_15,datapath_n_16,datapath_n_17,datapath_n_18}),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_aresetn(s00_axi_aresetn),
        .\slv_reg7_reg[0] (slv_reg7),
        .switch(switch[2]),
        .\trigger_volt_reg[9] (sw1),
        .w_sw(w_sw));
  LUT6 #(
    .INIT(64'h0000000000001000)) 
    \slv_reg0[15]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg8[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[1]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(p_1_in[15]));
  LUT6 #(
    .INIT(64'h0000000000001000)) 
    \slv_reg0[23]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg8[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(p_1_in[23]));
  LUT6 #(
    .INIT(64'h0000000000001000)) 
    \slv_reg0[31]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg8[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[3]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(p_1_in[31]));
  LUT6 #(
    .INIT(64'h0000000000001000)) 
    \slv_reg0[7]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg8[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[0]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(p_1_in[7]));
  FDRE \slv_reg0_reg[0] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg0[0]),
        .R(RST));
  FDRE \slv_reg0_reg[10] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg0[10]),
        .R(RST));
  FDRE \slv_reg0_reg[11] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg0[11]),
        .R(RST));
  FDRE \slv_reg0_reg[12] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg0[12]),
        .R(RST));
  FDRE \slv_reg0_reg[13] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg0[13]),
        .R(RST));
  FDRE \slv_reg0_reg[14] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg0[14]),
        .R(RST));
  FDRE \slv_reg0_reg[15] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg0[15]),
        .R(RST));
  FDRE \slv_reg0_reg[16] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg0[16]),
        .R(RST));
  FDRE \slv_reg0_reg[17] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg0[17]),
        .R(RST));
  FDRE \slv_reg0_reg[18] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg0[18]),
        .R(RST));
  FDRE \slv_reg0_reg[19] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg0[19]),
        .R(RST));
  FDRE \slv_reg0_reg[1] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg0[1]),
        .R(RST));
  FDRE \slv_reg0_reg[20] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg0[20]),
        .R(RST));
  FDRE \slv_reg0_reg[21] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg0[21]),
        .R(RST));
  FDRE \slv_reg0_reg[22] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg0[22]),
        .R(RST));
  FDRE \slv_reg0_reg[23] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg0[23]),
        .R(RST));
  FDRE \slv_reg0_reg[24] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg0[24]),
        .R(RST));
  FDRE \slv_reg0_reg[25] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg0[25]),
        .R(RST));
  FDRE \slv_reg0_reg[26] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg0[26]),
        .R(RST));
  FDRE \slv_reg0_reg[27] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg0[27]),
        .R(RST));
  FDRE \slv_reg0_reg[28] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg0[28]),
        .R(RST));
  FDRE \slv_reg0_reg[29] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg0[29]),
        .R(RST));
  FDRE \slv_reg0_reg[2] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg0[2]),
        .R(RST));
  FDRE \slv_reg0_reg[30] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg0[30]),
        .R(RST));
  FDRE \slv_reg0_reg[31] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg0[31]),
        .R(RST));
  FDRE \slv_reg0_reg[3] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg0[3]),
        .R(RST));
  FDRE \slv_reg0_reg[4] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg0[4]),
        .R(RST));
  FDRE \slv_reg0_reg[5] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg0[5]),
        .R(RST));
  FDRE \slv_reg0_reg[6] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg0[6]),
        .R(RST));
  FDRE \slv_reg0_reg[7] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg0[7]),
        .R(RST));
  FDRE \slv_reg0_reg[8] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg0[8]),
        .R(RST));
  FDRE \slv_reg0_reg[9] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg0[9]),
        .R(RST));
  LUT6 #(
    .INIT(64'h1000000000000000)) 
    \slv_reg10[15]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg8[31]_i_2_n_0 ),
        .I3(p_0_in[1]),
        .I4(p_0_in[3]),
        .I5(s00_axi_wstrb[1]),
        .O(\slv_reg10[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h1000000000000000)) 
    \slv_reg10[23]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg8[31]_i_2_n_0 ),
        .I3(p_0_in[1]),
        .I4(p_0_in[3]),
        .I5(s00_axi_wstrb[2]),
        .O(\slv_reg10[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h1000000000000000)) 
    \slv_reg10[31]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg8[31]_i_2_n_0 ),
        .I3(p_0_in[1]),
        .I4(p_0_in[3]),
        .I5(s00_axi_wstrb[3]),
        .O(\slv_reg10[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h1000000000000000)) 
    \slv_reg10[7]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg8[31]_i_2_n_0 ),
        .I3(p_0_in[1]),
        .I4(p_0_in[3]),
        .I5(s00_axi_wstrb[0]),
        .O(\slv_reg10[7]_i_1_n_0 ));
  FDRE \slv_reg10_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg10[0]),
        .R(RST));
  FDRE \slv_reg10_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg10[10]),
        .R(RST));
  FDRE \slv_reg10_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg10[11]),
        .R(RST));
  FDRE \slv_reg10_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg10[12]),
        .R(RST));
  FDRE \slv_reg10_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg10[13]),
        .R(RST));
  FDRE \slv_reg10_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg10[14]),
        .R(RST));
  FDRE \slv_reg10_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg10[15]),
        .R(RST));
  FDRE \slv_reg10_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg10__0[16]),
        .R(RST));
  FDRE \slv_reg10_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg10__0[17]),
        .R(RST));
  FDRE \slv_reg10_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg10__0[18]),
        .R(RST));
  FDRE \slv_reg10_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg10__0[19]),
        .R(RST));
  FDRE \slv_reg10_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg10[1]),
        .R(RST));
  FDRE \slv_reg10_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg10__0[20]),
        .R(RST));
  FDRE \slv_reg10_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg10__0[21]),
        .R(RST));
  FDRE \slv_reg10_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg10__0[22]),
        .R(RST));
  FDRE \slv_reg10_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg10__0[23]),
        .R(RST));
  FDRE \slv_reg10_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg10__0[24]),
        .R(RST));
  FDRE \slv_reg10_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg10__0[25]),
        .R(RST));
  FDRE \slv_reg10_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg10__0[26]),
        .R(RST));
  FDRE \slv_reg10_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg10__0[27]),
        .R(RST));
  FDRE \slv_reg10_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg10__0[28]),
        .R(RST));
  FDRE \slv_reg10_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg10__0[29]),
        .R(RST));
  FDRE \slv_reg10_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg10[2]),
        .R(RST));
  FDRE \slv_reg10_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg10__0[30]),
        .R(RST));
  FDRE \slv_reg10_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg10__0[31]),
        .R(RST));
  FDRE \slv_reg10_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg10[3]),
        .R(RST));
  FDRE \slv_reg10_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg10[4]),
        .R(RST));
  FDRE \slv_reg10_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg10[5]),
        .R(RST));
  FDRE \slv_reg10_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg10[6]),
        .R(RST));
  FDRE \slv_reg10_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg10[7]),
        .R(RST));
  FDRE \slv_reg10_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg10[8]),
        .R(RST));
  FDRE \slv_reg10_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg10[9]),
        .R(RST));
  LUT6 #(
    .INIT(64'h0000000040000000)) 
    \slv_reg12[15]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg8[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[1]),
        .I4(p_0_in[3]),
        .I5(p_0_in[1]),
        .O(\slv_reg12[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000040000000)) 
    \slv_reg12[23]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg8[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[2]),
        .I4(p_0_in[3]),
        .I5(p_0_in[1]),
        .O(\slv_reg12[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000040000000)) 
    \slv_reg12[31]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg8[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[3]),
        .I4(p_0_in[3]),
        .I5(p_0_in[1]),
        .O(\slv_reg12[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000040000000)) 
    \slv_reg12[7]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg8[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[0]),
        .I4(p_0_in[3]),
        .I5(p_0_in[1]),
        .O(\slv_reg12[7]_i_1_n_0 ));
  FDRE \slv_reg12_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg12__0),
        .R(RST));
  FDRE \slv_reg12_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg12[10]),
        .R(RST));
  FDRE \slv_reg12_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg12[11]),
        .R(RST));
  FDRE \slv_reg12_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg12[12]),
        .R(RST));
  FDRE \slv_reg12_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg12[13]),
        .R(RST));
  FDRE \slv_reg12_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg12[14]),
        .R(RST));
  FDRE \slv_reg12_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg12[15]),
        .R(RST));
  FDRE \slv_reg12_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg12[16]),
        .R(RST));
  FDRE \slv_reg12_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg12[17]),
        .R(RST));
  FDRE \slv_reg12_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg12[18]),
        .R(RST));
  FDRE \slv_reg12_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg12[19]),
        .R(RST));
  FDRE \slv_reg12_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg12[1]),
        .R(RST));
  FDRE \slv_reg12_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg12[20]),
        .R(RST));
  FDRE \slv_reg12_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg12[21]),
        .R(RST));
  FDRE \slv_reg12_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg12[22]),
        .R(RST));
  FDRE \slv_reg12_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg12[23]),
        .R(RST));
  FDRE \slv_reg12_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg12[24]),
        .R(RST));
  FDRE \slv_reg12_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg12[25]),
        .R(RST));
  FDRE \slv_reg12_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg12[26]),
        .R(RST));
  FDRE \slv_reg12_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg12[27]),
        .R(RST));
  FDRE \slv_reg12_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg12[28]),
        .R(RST));
  FDRE \slv_reg12_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg12[29]),
        .R(RST));
  FDRE \slv_reg12_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg12[2]),
        .R(RST));
  FDRE \slv_reg12_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg12[30]),
        .R(RST));
  FDRE \slv_reg12_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg12[31]),
        .R(RST));
  FDRE \slv_reg12_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg12[3]),
        .R(RST));
  FDRE \slv_reg12_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg12[4]),
        .R(RST));
  FDRE \slv_reg12_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg12[5]),
        .R(RST));
  FDRE \slv_reg12_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg12[6]),
        .R(RST));
  FDRE \slv_reg12_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg12[7]),
        .R(RST));
  FDRE \slv_reg12_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg12[8]),
        .R(RST));
  FDRE \slv_reg12_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg12[9]),
        .R(RST));
  LUT6 #(
    .INIT(64'h0000000000001000)) 
    \slv_reg1[15]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg7[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[1]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(\slv_reg1[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000001000)) 
    \slv_reg1[23]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg7[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(\slv_reg1[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000001000)) 
    \slv_reg1[31]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg7[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[3]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(\slv_reg1[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000001000)) 
    \slv_reg1[7]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg7[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[0]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(\slv_reg1[7]_i_1_n_0 ));
  FDRE \slv_reg1_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg1[0]),
        .R(RST));
  FDRE \slv_reg1_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg1[10]),
        .R(RST));
  FDRE \slv_reg1_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg1[11]),
        .R(RST));
  FDRE \slv_reg1_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg1[12]),
        .R(RST));
  FDRE \slv_reg1_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg1[13]),
        .R(RST));
  FDRE \slv_reg1_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg1[14]),
        .R(RST));
  FDRE \slv_reg1_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg1[15]),
        .R(RST));
  FDRE \slv_reg1_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg1[16]),
        .R(RST));
  FDRE \slv_reg1_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg1[17]),
        .R(RST));
  FDRE \slv_reg1_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg1[18]),
        .R(RST));
  FDRE \slv_reg1_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg1[19]),
        .R(RST));
  FDRE \slv_reg1_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg1[1]),
        .R(RST));
  FDRE \slv_reg1_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg1[20]),
        .R(RST));
  FDRE \slv_reg1_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg1[21]),
        .R(RST));
  FDRE \slv_reg1_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg1[22]),
        .R(RST));
  FDRE \slv_reg1_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg1[23]),
        .R(RST));
  FDRE \slv_reg1_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg1[24]),
        .R(RST));
  FDRE \slv_reg1_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg1[25]),
        .R(RST));
  FDRE \slv_reg1_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg1[26]),
        .R(RST));
  FDRE \slv_reg1_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg1[27]),
        .R(RST));
  FDRE \slv_reg1_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg1[28]),
        .R(RST));
  FDRE \slv_reg1_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg1[29]),
        .R(RST));
  FDRE \slv_reg1_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg1[2]),
        .R(RST));
  FDRE \slv_reg1_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg1[30]),
        .R(RST));
  FDRE \slv_reg1_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg1[31]),
        .R(RST));
  FDRE \slv_reg1_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg1[3]),
        .R(RST));
  FDRE \slv_reg1_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg1[4]),
        .R(RST));
  FDRE \slv_reg1_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg1[5]),
        .R(RST));
  FDRE \slv_reg1_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg1[6]),
        .R(RST));
  FDRE \slv_reg1_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg1[7]),
        .R(RST));
  FDRE \slv_reg1_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg1[8]),
        .R(RST));
  FDRE \slv_reg1_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg1[9]),
        .R(RST));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg20[15]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg8[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[1]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(\slv_reg20[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg20[23]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg8[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(\slv_reg20[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg20[31]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg8[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[3]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(\slv_reg20[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg20[7]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg8[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[0]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(\slv_reg20[7]_i_1_n_0 ));
  FDRE \slv_reg20_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg20[0]),
        .R(RST));
  FDRE \slv_reg20_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg20[10]),
        .R(RST));
  FDRE \slv_reg20_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg20[11]),
        .R(RST));
  FDRE \slv_reg20_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg20[12]),
        .R(RST));
  FDRE \slv_reg20_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg20[13]),
        .R(RST));
  FDRE \slv_reg20_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg20[14]),
        .R(RST));
  FDRE \slv_reg20_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg20[15]),
        .R(RST));
  FDRE \slv_reg20_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg20[16]),
        .R(RST));
  FDRE \slv_reg20_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg20[17]),
        .R(RST));
  FDRE \slv_reg20_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg20[18]),
        .R(RST));
  FDRE \slv_reg20_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg20[19]),
        .R(RST));
  FDRE \slv_reg20_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg20[1]),
        .R(RST));
  FDRE \slv_reg20_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg20[20]),
        .R(RST));
  FDRE \slv_reg20_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg20[21]),
        .R(RST));
  FDRE \slv_reg20_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg20[22]),
        .R(RST));
  FDRE \slv_reg20_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg20[23]),
        .R(RST));
  FDRE \slv_reg20_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg20[24]),
        .R(RST));
  FDRE \slv_reg20_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg20[25]),
        .R(RST));
  FDRE \slv_reg20_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg20[26]),
        .R(RST));
  FDRE \slv_reg20_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg20[27]),
        .R(RST));
  FDRE \slv_reg20_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg20[28]),
        .R(RST));
  FDRE \slv_reg20_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg20[29]),
        .R(RST));
  FDRE \slv_reg20_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg20[2]),
        .R(RST));
  FDRE \slv_reg20_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg20[30]),
        .R(RST));
  FDRE \slv_reg20_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg20[31]),
        .R(RST));
  FDRE \slv_reg20_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg20[3]),
        .R(RST));
  FDRE \slv_reg20_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg20[4]),
        .R(RST));
  FDRE \slv_reg20_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg20[5]),
        .R(RST));
  FDRE \slv_reg20_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg20[6]),
        .R(RST));
  FDRE \slv_reg20_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg20[7]),
        .R(RST));
  FDRE \slv_reg20_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg20[8]),
        .R(RST));
  FDRE \slv_reg20_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg20[9]),
        .R(RST));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg21[15]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg7[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[1]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(\slv_reg21[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg21[23]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg7[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(\slv_reg21[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg21[31]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg7[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[3]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(\slv_reg21[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg21[7]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg7[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[0]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(\slv_reg21[7]_i_1_n_0 ));
  FDRE \slv_reg21_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg21[0]),
        .R(RST));
  FDRE \slv_reg21_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg21[10]),
        .R(RST));
  FDRE \slv_reg21_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg21[11]),
        .R(RST));
  FDRE \slv_reg21_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg21[12]),
        .R(RST));
  FDRE \slv_reg21_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg21[13]),
        .R(RST));
  FDRE \slv_reg21_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg21[14]),
        .R(RST));
  FDRE \slv_reg21_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg21[15]),
        .R(RST));
  FDRE \slv_reg21_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg21[16]),
        .R(RST));
  FDRE \slv_reg21_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg21[17]),
        .R(RST));
  FDRE \slv_reg21_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg21[18]),
        .R(RST));
  FDRE \slv_reg21_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg21[19]),
        .R(RST));
  FDRE \slv_reg21_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg21[1]),
        .R(RST));
  FDRE \slv_reg21_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg21[20]),
        .R(RST));
  FDRE \slv_reg21_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg21[21]),
        .R(RST));
  FDRE \slv_reg21_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg21[22]),
        .R(RST));
  FDRE \slv_reg21_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg21[23]),
        .R(RST));
  FDRE \slv_reg21_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg21[24]),
        .R(RST));
  FDRE \slv_reg21_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg21[25]),
        .R(RST));
  FDRE \slv_reg21_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg21[26]),
        .R(RST));
  FDRE \slv_reg21_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg21[27]),
        .R(RST));
  FDRE \slv_reg21_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg21[28]),
        .R(RST));
  FDRE \slv_reg21_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg21[29]),
        .R(RST));
  FDRE \slv_reg21_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg21[2]),
        .R(RST));
  FDRE \slv_reg21_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg21[30]),
        .R(RST));
  FDRE \slv_reg21_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg21[31]),
        .R(RST));
  FDRE \slv_reg21_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg21[3]),
        .R(RST));
  FDRE \slv_reg21_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg21[4]),
        .R(RST));
  FDRE \slv_reg21_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg21[5]),
        .R(RST));
  FDRE \slv_reg21_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg21[6]),
        .R(RST));
  FDRE \slv_reg21_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg21[7]),
        .R(RST));
  FDRE \slv_reg21_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg21[8]),
        .R(RST));
  FDRE \slv_reg21_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg21[9]),
        .R(RST));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \slv_reg22[15]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg8[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[1]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(\slv_reg22[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \slv_reg22[23]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg8[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(\slv_reg22[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \slv_reg22[31]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg8[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[3]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(\slv_reg22[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \slv_reg22[7]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg8[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[0]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(\slv_reg22[7]_i_1_n_0 ));
  FDRE \slv_reg22_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg22[0]),
        .R(RST));
  FDRE \slv_reg22_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg22[10]),
        .R(RST));
  FDRE \slv_reg22_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg22[11]),
        .R(RST));
  FDRE \slv_reg22_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg22[12]),
        .R(RST));
  FDRE \slv_reg22_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg22[13]),
        .R(RST));
  FDRE \slv_reg22_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg22[14]),
        .R(RST));
  FDRE \slv_reg22_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg22[15]),
        .R(RST));
  FDRE \slv_reg22_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg22[16]),
        .R(RST));
  FDRE \slv_reg22_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg22[17]),
        .R(RST));
  FDRE \slv_reg22_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg22[18]),
        .R(RST));
  FDRE \slv_reg22_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg22[19]),
        .R(RST));
  FDRE \slv_reg22_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg22[1]),
        .R(RST));
  FDRE \slv_reg22_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg22[20]),
        .R(RST));
  FDRE \slv_reg22_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg22[21]),
        .R(RST));
  FDRE \slv_reg22_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg22[22]),
        .R(RST));
  FDRE \slv_reg22_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg22[23]),
        .R(RST));
  FDRE \slv_reg22_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg22[24]),
        .R(RST));
  FDRE \slv_reg22_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg22[25]),
        .R(RST));
  FDRE \slv_reg22_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg22[26]),
        .R(RST));
  FDRE \slv_reg22_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg22[27]),
        .R(RST));
  FDRE \slv_reg22_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg22[28]),
        .R(RST));
  FDRE \slv_reg22_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg22[29]),
        .R(RST));
  FDRE \slv_reg22_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg22[2]),
        .R(RST));
  FDRE \slv_reg22_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg22[30]),
        .R(RST));
  FDRE \slv_reg22_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg22[31]),
        .R(RST));
  FDRE \slv_reg22_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg22[3]),
        .R(RST));
  FDRE \slv_reg22_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg22[4]),
        .R(RST));
  FDRE \slv_reg22_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg22[5]),
        .R(RST));
  FDRE \slv_reg22_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg22[6]),
        .R(RST));
  FDRE \slv_reg22_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg22[7]),
        .R(RST));
  FDRE \slv_reg22_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg22[8]),
        .R(RST));
  FDRE \slv_reg22_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg22[9]),
        .R(RST));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \slv_reg23[15]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg7[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[1]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(\slv_reg23[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \slv_reg23[23]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg7[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(\slv_reg23[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \slv_reg23[31]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg7[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[3]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(\slv_reg23[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \slv_reg23[7]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg7[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[0]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(\slv_reg23[7]_i_1_n_0 ));
  FDRE \slv_reg23_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg23[0]),
        .R(RST));
  FDRE \slv_reg23_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg23[10]),
        .R(RST));
  FDRE \slv_reg23_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg23[11]),
        .R(RST));
  FDRE \slv_reg23_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg23[12]),
        .R(RST));
  FDRE \slv_reg23_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg23[13]),
        .R(RST));
  FDRE \slv_reg23_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg23[14]),
        .R(RST));
  FDRE \slv_reg23_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg23[15]),
        .R(RST));
  FDRE \slv_reg23_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg23[16]),
        .R(RST));
  FDRE \slv_reg23_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg23[17]),
        .R(RST));
  FDRE \slv_reg23_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg23[18]),
        .R(RST));
  FDRE \slv_reg23_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg23[19]),
        .R(RST));
  FDRE \slv_reg23_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg23[1]),
        .R(RST));
  FDRE \slv_reg23_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg23[20]),
        .R(RST));
  FDRE \slv_reg23_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg23[21]),
        .R(RST));
  FDRE \slv_reg23_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg23[22]),
        .R(RST));
  FDRE \slv_reg23_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg23[23]),
        .R(RST));
  FDRE \slv_reg23_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg23[24]),
        .R(RST));
  FDRE \slv_reg23_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg23[25]),
        .R(RST));
  FDRE \slv_reg23_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg23[26]),
        .R(RST));
  FDRE \slv_reg23_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg23[27]),
        .R(RST));
  FDRE \slv_reg23_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg23[28]),
        .R(RST));
  FDRE \slv_reg23_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg23[29]),
        .R(RST));
  FDRE \slv_reg23_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg23[2]),
        .R(RST));
  FDRE \slv_reg23_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg23[30]),
        .R(RST));
  FDRE \slv_reg23_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg23[31]),
        .R(RST));
  FDRE \slv_reg23_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg23[3]),
        .R(RST));
  FDRE \slv_reg23_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg23[4]),
        .R(RST));
  FDRE \slv_reg23_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg23[5]),
        .R(RST));
  FDRE \slv_reg23_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg23[6]),
        .R(RST));
  FDRE \slv_reg23_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg23[7]),
        .R(RST));
  FDRE \slv_reg23_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg23[8]),
        .R(RST));
  FDRE \slv_reg23_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg23[9]),
        .R(RST));
  LUT6 #(
    .INIT(64'h0020000000000000)) 
    \slv_reg24[15]_i_1 
       (.I0(\slv_reg8[31]_i_2_n_0 ),
        .I1(p_0_in[2]),
        .I2(p_0_in[4]),
        .I3(p_0_in[1]),
        .I4(p_0_in[3]),
        .I5(s00_axi_wstrb[1]),
        .O(\slv_reg24[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0020000000000000)) 
    \slv_reg24[23]_i_1 
       (.I0(\slv_reg8[31]_i_2_n_0 ),
        .I1(p_0_in[2]),
        .I2(p_0_in[4]),
        .I3(p_0_in[1]),
        .I4(p_0_in[3]),
        .I5(s00_axi_wstrb[2]),
        .O(\slv_reg24[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0020000000000000)) 
    \slv_reg24[31]_i_1 
       (.I0(\slv_reg8[31]_i_2_n_0 ),
        .I1(p_0_in[2]),
        .I2(p_0_in[4]),
        .I3(p_0_in[1]),
        .I4(p_0_in[3]),
        .I5(s00_axi_wstrb[3]),
        .O(\slv_reg24[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0020000000000000)) 
    \slv_reg24[7]_i_1 
       (.I0(\slv_reg8[31]_i_2_n_0 ),
        .I1(p_0_in[2]),
        .I2(p_0_in[4]),
        .I3(p_0_in[1]),
        .I4(p_0_in[3]),
        .I5(s00_axi_wstrb[0]),
        .O(\slv_reg24[7]_i_1_n_0 ));
  FDRE \slv_reg24_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg24[0]),
        .R(RST));
  FDRE \slv_reg24_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg24[10]),
        .R(RST));
  FDRE \slv_reg24_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg24[11]),
        .R(RST));
  FDRE \slv_reg24_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg24[12]),
        .R(RST));
  FDRE \slv_reg24_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg24[13]),
        .R(RST));
  FDRE \slv_reg24_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg24[14]),
        .R(RST));
  FDRE \slv_reg24_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg24[15]),
        .R(RST));
  FDRE \slv_reg24_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg24[16]),
        .R(RST));
  FDRE \slv_reg24_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg24[17]),
        .R(RST));
  FDRE \slv_reg24_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg24[18]),
        .R(RST));
  FDRE \slv_reg24_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg24[19]),
        .R(RST));
  FDRE \slv_reg24_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg24[1]),
        .R(RST));
  FDRE \slv_reg24_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg24[20]),
        .R(RST));
  FDRE \slv_reg24_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg24[21]),
        .R(RST));
  FDRE \slv_reg24_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg24[22]),
        .R(RST));
  FDRE \slv_reg24_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg24[23]),
        .R(RST));
  FDRE \slv_reg24_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg24[24]),
        .R(RST));
  FDRE \slv_reg24_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg24[25]),
        .R(RST));
  FDRE \slv_reg24_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg24[26]),
        .R(RST));
  FDRE \slv_reg24_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg24[27]),
        .R(RST));
  FDRE \slv_reg24_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg24[28]),
        .R(RST));
  FDRE \slv_reg24_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg24[29]),
        .R(RST));
  FDRE \slv_reg24_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg24[2]),
        .R(RST));
  FDRE \slv_reg24_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg24[30]),
        .R(RST));
  FDRE \slv_reg24_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg24[31]),
        .R(RST));
  FDRE \slv_reg24_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg24[3]),
        .R(RST));
  FDRE \slv_reg24_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg24[4]),
        .R(RST));
  FDRE \slv_reg24_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg24[5]),
        .R(RST));
  FDRE \slv_reg24_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg24[6]),
        .R(RST));
  FDRE \slv_reg24_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg24[7]),
        .R(RST));
  FDRE \slv_reg24_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg24[8]),
        .R(RST));
  FDRE \slv_reg24_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg24[9]),
        .R(RST));
  LUT6 #(
    .INIT(64'h0020000000000000)) 
    \slv_reg25[15]_i_1 
       (.I0(\slv_reg7[31]_i_2_n_0 ),
        .I1(p_0_in[2]),
        .I2(p_0_in[4]),
        .I3(p_0_in[1]),
        .I4(p_0_in[3]),
        .I5(s00_axi_wstrb[1]),
        .O(\slv_reg25[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0020000000000000)) 
    \slv_reg25[23]_i_1 
       (.I0(\slv_reg7[31]_i_2_n_0 ),
        .I1(p_0_in[2]),
        .I2(p_0_in[4]),
        .I3(p_0_in[1]),
        .I4(p_0_in[3]),
        .I5(s00_axi_wstrb[2]),
        .O(\slv_reg25[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0020000000000000)) 
    \slv_reg25[31]_i_1 
       (.I0(\slv_reg7[31]_i_2_n_0 ),
        .I1(p_0_in[2]),
        .I2(p_0_in[4]),
        .I3(p_0_in[1]),
        .I4(p_0_in[3]),
        .I5(s00_axi_wstrb[3]),
        .O(\slv_reg25[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0020000000000000)) 
    \slv_reg25[7]_i_1 
       (.I0(\slv_reg7[31]_i_2_n_0 ),
        .I1(p_0_in[2]),
        .I2(p_0_in[4]),
        .I3(p_0_in[1]),
        .I4(p_0_in[3]),
        .I5(s00_axi_wstrb[0]),
        .O(\slv_reg25[7]_i_1_n_0 ));
  FDRE \slv_reg25_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg25[0]),
        .R(RST));
  FDRE \slv_reg25_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg25[10]),
        .R(RST));
  FDRE \slv_reg25_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg25[11]),
        .R(RST));
  FDRE \slv_reg25_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg25[12]),
        .R(RST));
  FDRE \slv_reg25_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg25[13]),
        .R(RST));
  FDRE \slv_reg25_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg25[14]),
        .R(RST));
  FDRE \slv_reg25_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg25[15]),
        .R(RST));
  FDRE \slv_reg25_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg25[16]),
        .R(RST));
  FDRE \slv_reg25_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg25[17]),
        .R(RST));
  FDRE \slv_reg25_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg25[18]),
        .R(RST));
  FDRE \slv_reg25_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg25[19]),
        .R(RST));
  FDRE \slv_reg25_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg25[1]),
        .R(RST));
  FDRE \slv_reg25_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg25[20]),
        .R(RST));
  FDRE \slv_reg25_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg25[21]),
        .R(RST));
  FDRE \slv_reg25_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg25[22]),
        .R(RST));
  FDRE \slv_reg25_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg25[23]),
        .R(RST));
  FDRE \slv_reg25_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg25[24]),
        .R(RST));
  FDRE \slv_reg25_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg25[25]),
        .R(RST));
  FDRE \slv_reg25_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg25[26]),
        .R(RST));
  FDRE \slv_reg25_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg25[27]),
        .R(RST));
  FDRE \slv_reg25_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg25[28]),
        .R(RST));
  FDRE \slv_reg25_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg25[29]),
        .R(RST));
  FDRE \slv_reg25_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg25[2]),
        .R(RST));
  FDRE \slv_reg25_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg25[30]),
        .R(RST));
  FDRE \slv_reg25_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg25[31]),
        .R(RST));
  FDRE \slv_reg25_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg25[3]),
        .R(RST));
  FDRE \slv_reg25_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg25[4]),
        .R(RST));
  FDRE \slv_reg25_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg25[5]),
        .R(RST));
  FDRE \slv_reg25_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg25[6]),
        .R(RST));
  FDRE \slv_reg25_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg25[7]),
        .R(RST));
  FDRE \slv_reg25_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg25[8]),
        .R(RST));
  FDRE \slv_reg25_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg25[9]),
        .R(RST));
  LUT6 #(
    .INIT(64'h2000000000000000)) 
    \slv_reg26[15]_i_1 
       (.I0(\slv_reg8[31]_i_2_n_0 ),
        .I1(p_0_in[2]),
        .I2(p_0_in[4]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s00_axi_wstrb[1]),
        .O(\slv_reg26[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h2000000000000000)) 
    \slv_reg26[23]_i_1 
       (.I0(\slv_reg8[31]_i_2_n_0 ),
        .I1(p_0_in[2]),
        .I2(p_0_in[4]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s00_axi_wstrb[2]),
        .O(\slv_reg26[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h2000000000000000)) 
    \slv_reg26[31]_i_1 
       (.I0(\slv_reg8[31]_i_2_n_0 ),
        .I1(p_0_in[2]),
        .I2(p_0_in[4]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s00_axi_wstrb[3]),
        .O(\slv_reg26[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h2000000000000000)) 
    \slv_reg26[7]_i_1 
       (.I0(\slv_reg8[31]_i_2_n_0 ),
        .I1(p_0_in[2]),
        .I2(p_0_in[4]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s00_axi_wstrb[0]),
        .O(\slv_reg26[7]_i_1_n_0 ));
  FDRE \slv_reg26_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg26[0]),
        .R(RST));
  FDRE \slv_reg26_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg26[10]),
        .R(RST));
  FDRE \slv_reg26_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg26[11]),
        .R(RST));
  FDRE \slv_reg26_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg26[12]),
        .R(RST));
  FDRE \slv_reg26_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg26[13]),
        .R(RST));
  FDRE \slv_reg26_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg26[14]),
        .R(RST));
  FDRE \slv_reg26_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg26[15]),
        .R(RST));
  FDRE \slv_reg26_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg26[16]),
        .R(RST));
  FDRE \slv_reg26_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg26[17]),
        .R(RST));
  FDRE \slv_reg26_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg26[18]),
        .R(RST));
  FDRE \slv_reg26_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg26[19]),
        .R(RST));
  FDRE \slv_reg26_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg26[1]),
        .R(RST));
  FDRE \slv_reg26_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg26[20]),
        .R(RST));
  FDRE \slv_reg26_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg26[21]),
        .R(RST));
  FDRE \slv_reg26_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg26[22]),
        .R(RST));
  FDRE \slv_reg26_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg26[23]),
        .R(RST));
  FDRE \slv_reg26_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg26[24]),
        .R(RST));
  FDRE \slv_reg26_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg26[25]),
        .R(RST));
  FDRE \slv_reg26_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg26[26]),
        .R(RST));
  FDRE \slv_reg26_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg26[27]),
        .R(RST));
  FDRE \slv_reg26_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg26[28]),
        .R(RST));
  FDRE \slv_reg26_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg26[29]),
        .R(RST));
  FDRE \slv_reg26_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg26[2]),
        .R(RST));
  FDRE \slv_reg26_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg26[30]),
        .R(RST));
  FDRE \slv_reg26_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg26[31]),
        .R(RST));
  FDRE \slv_reg26_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg26[3]),
        .R(RST));
  FDRE \slv_reg26_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg26[4]),
        .R(RST));
  FDRE \slv_reg26_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg26[5]),
        .R(RST));
  FDRE \slv_reg26_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg26[6]),
        .R(RST));
  FDRE \slv_reg26_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg26[7]),
        .R(RST));
  FDRE \slv_reg26_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg26[8]),
        .R(RST));
  FDRE \slv_reg26_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg26[9]),
        .R(RST));
  LUT6 #(
    .INIT(64'h2000000000000000)) 
    \slv_reg27[15]_i_1 
       (.I0(\slv_reg7[31]_i_2_n_0 ),
        .I1(p_0_in[2]),
        .I2(p_0_in[4]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s00_axi_wstrb[1]),
        .O(\slv_reg27[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h2000000000000000)) 
    \slv_reg27[23]_i_1 
       (.I0(\slv_reg7[31]_i_2_n_0 ),
        .I1(p_0_in[2]),
        .I2(p_0_in[4]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s00_axi_wstrb[2]),
        .O(\slv_reg27[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h2000000000000000)) 
    \slv_reg27[31]_i_1 
       (.I0(\slv_reg7[31]_i_2_n_0 ),
        .I1(p_0_in[2]),
        .I2(p_0_in[4]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s00_axi_wstrb[3]),
        .O(\slv_reg27[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h2000000000000000)) 
    \slv_reg27[7]_i_1 
       (.I0(\slv_reg7[31]_i_2_n_0 ),
        .I1(p_0_in[2]),
        .I2(p_0_in[4]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s00_axi_wstrb[0]),
        .O(\slv_reg27[7]_i_1_n_0 ));
  FDRE \slv_reg27_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg27[0]),
        .R(RST));
  FDRE \slv_reg27_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg27[10]),
        .R(RST));
  FDRE \slv_reg27_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg27[11]),
        .R(RST));
  FDRE \slv_reg27_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg27[12]),
        .R(RST));
  FDRE \slv_reg27_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg27[13]),
        .R(RST));
  FDRE \slv_reg27_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg27[14]),
        .R(RST));
  FDRE \slv_reg27_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg27[15]),
        .R(RST));
  FDRE \slv_reg27_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg27[16]),
        .R(RST));
  FDRE \slv_reg27_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg27[17]),
        .R(RST));
  FDRE \slv_reg27_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg27[18]),
        .R(RST));
  FDRE \slv_reg27_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg27[19]),
        .R(RST));
  FDRE \slv_reg27_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg27[1]),
        .R(RST));
  FDRE \slv_reg27_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg27[20]),
        .R(RST));
  FDRE \slv_reg27_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg27[21]),
        .R(RST));
  FDRE \slv_reg27_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg27[22]),
        .R(RST));
  FDRE \slv_reg27_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg27[23]),
        .R(RST));
  FDRE \slv_reg27_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg27[24]),
        .R(RST));
  FDRE \slv_reg27_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg27[25]),
        .R(RST));
  FDRE \slv_reg27_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg27[26]),
        .R(RST));
  FDRE \slv_reg27_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg27[27]),
        .R(RST));
  FDRE \slv_reg27_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg27[28]),
        .R(RST));
  FDRE \slv_reg27_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg27[29]),
        .R(RST));
  FDRE \slv_reg27_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg27[2]),
        .R(RST));
  FDRE \slv_reg27_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg27[30]),
        .R(RST));
  FDRE \slv_reg27_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg27[31]),
        .R(RST));
  FDRE \slv_reg27_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg27[3]),
        .R(RST));
  FDRE \slv_reg27_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg27[4]),
        .R(RST));
  FDRE \slv_reg27_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg27[5]),
        .R(RST));
  FDRE \slv_reg27_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg27[6]),
        .R(RST));
  FDRE \slv_reg27_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg27[7]),
        .R(RST));
  FDRE \slv_reg27_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg27[8]),
        .R(RST));
  FDRE \slv_reg27_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg27[9]),
        .R(RST));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \slv_reg28[15]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg8[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[1]),
        .I4(p_0_in[3]),
        .I5(p_0_in[1]),
        .O(\slv_reg28[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \slv_reg28[23]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg8[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[2]),
        .I4(p_0_in[3]),
        .I5(p_0_in[1]),
        .O(\slv_reg28[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \slv_reg28[31]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg8[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[3]),
        .I4(p_0_in[3]),
        .I5(p_0_in[1]),
        .O(\slv_reg28[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \slv_reg28[7]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg8[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[0]),
        .I4(p_0_in[3]),
        .I5(p_0_in[1]),
        .O(\slv_reg28[7]_i_1_n_0 ));
  FDRE \slv_reg28_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg28[0]),
        .R(RST));
  FDRE \slv_reg28_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg28[10]),
        .R(RST));
  FDRE \slv_reg28_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg28[11]),
        .R(RST));
  FDRE \slv_reg28_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg28[12]),
        .R(RST));
  FDRE \slv_reg28_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg28[13]),
        .R(RST));
  FDRE \slv_reg28_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg28[14]),
        .R(RST));
  FDRE \slv_reg28_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg28[15]),
        .R(RST));
  FDRE \slv_reg28_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg28[16]),
        .R(RST));
  FDRE \slv_reg28_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg28[17]),
        .R(RST));
  FDRE \slv_reg28_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg28[18]),
        .R(RST));
  FDRE \slv_reg28_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg28[19]),
        .R(RST));
  FDRE \slv_reg28_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg28[1]),
        .R(RST));
  FDRE \slv_reg28_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg28[20]),
        .R(RST));
  FDRE \slv_reg28_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg28[21]),
        .R(RST));
  FDRE \slv_reg28_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg28[22]),
        .R(RST));
  FDRE \slv_reg28_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg28[23]),
        .R(RST));
  FDRE \slv_reg28_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg28[24]),
        .R(RST));
  FDRE \slv_reg28_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg28[25]),
        .R(RST));
  FDRE \slv_reg28_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg28[26]),
        .R(RST));
  FDRE \slv_reg28_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg28[27]),
        .R(RST));
  FDRE \slv_reg28_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg28[28]),
        .R(RST));
  FDRE \slv_reg28_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg28[29]),
        .R(RST));
  FDRE \slv_reg28_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg28[2]),
        .R(RST));
  FDRE \slv_reg28_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg28[30]),
        .R(RST));
  FDRE \slv_reg28_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg28[31]),
        .R(RST));
  FDRE \slv_reg28_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg28[3]),
        .R(RST));
  FDRE \slv_reg28_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg28[4]),
        .R(RST));
  FDRE \slv_reg28_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg28[5]),
        .R(RST));
  FDRE \slv_reg28_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg28[6]),
        .R(RST));
  FDRE \slv_reg28_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg28[7]),
        .R(RST));
  FDRE \slv_reg28_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg28[8]),
        .R(RST));
  FDRE \slv_reg28_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg28[9]),
        .R(RST));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \slv_reg29[15]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg7[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[1]),
        .I4(p_0_in[3]),
        .I5(p_0_in[1]),
        .O(\slv_reg29[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \slv_reg29[23]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg7[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[2]),
        .I4(p_0_in[3]),
        .I5(p_0_in[1]),
        .O(\slv_reg29[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \slv_reg29[31]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg7[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[3]),
        .I4(p_0_in[3]),
        .I5(p_0_in[1]),
        .O(\slv_reg29[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \slv_reg29[7]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg7[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[0]),
        .I4(p_0_in[3]),
        .I5(p_0_in[1]),
        .O(\slv_reg29[7]_i_1_n_0 ));
  FDRE \slv_reg29_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg29[0]),
        .R(RST));
  FDRE \slv_reg29_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg29[10]),
        .R(RST));
  FDRE \slv_reg29_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg29[11]),
        .R(RST));
  FDRE \slv_reg29_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg29[12]),
        .R(RST));
  FDRE \slv_reg29_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg29[13]),
        .R(RST));
  FDRE \slv_reg29_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg29[14]),
        .R(RST));
  FDRE \slv_reg29_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg29[15]),
        .R(RST));
  FDRE \slv_reg29_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg29[16]),
        .R(RST));
  FDRE \slv_reg29_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg29[17]),
        .R(RST));
  FDRE \slv_reg29_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg29[18]),
        .R(RST));
  FDRE \slv_reg29_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg29[19]),
        .R(RST));
  FDRE \slv_reg29_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg29[1]),
        .R(RST));
  FDRE \slv_reg29_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg29[20]),
        .R(RST));
  FDRE \slv_reg29_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg29[21]),
        .R(RST));
  FDRE \slv_reg29_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg29[22]),
        .R(RST));
  FDRE \slv_reg29_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg29[23]),
        .R(RST));
  FDRE \slv_reg29_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg29[24]),
        .R(RST));
  FDRE \slv_reg29_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg29[25]),
        .R(RST));
  FDRE \slv_reg29_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg29[26]),
        .R(RST));
  FDRE \slv_reg29_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg29[27]),
        .R(RST));
  FDRE \slv_reg29_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg29[28]),
        .R(RST));
  FDRE \slv_reg29_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg29[29]),
        .R(RST));
  FDRE \slv_reg29_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg29[2]),
        .R(RST));
  FDRE \slv_reg29_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg29[30]),
        .R(RST));
  FDRE \slv_reg29_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg29[31]),
        .R(RST));
  FDRE \slv_reg29_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg29[3]),
        .R(RST));
  FDRE \slv_reg29_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg29[4]),
        .R(RST));
  FDRE \slv_reg29_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg29[5]),
        .R(RST));
  FDRE \slv_reg29_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg29[6]),
        .R(RST));
  FDRE \slv_reg29_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg29[7]),
        .R(RST));
  FDRE \slv_reg29_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg29[8]),
        .R(RST));
  FDRE \slv_reg29_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg29[9]),
        .R(RST));
  LUT6 #(
    .INIT(64'h0000000010000000)) 
    \slv_reg2[15]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg8[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[1]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(\slv_reg2[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000010000000)) 
    \slv_reg2[23]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg8[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(\slv_reg2[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000010000000)) 
    \slv_reg2[31]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg8[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[3]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(\slv_reg2[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000010000000)) 
    \slv_reg2[7]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg8[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[0]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(\slv_reg2[7]_i_1_n_0 ));
  FDRE \slv_reg2_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg2[0]),
        .R(RST));
  FDRE \slv_reg2_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg2[10]),
        .R(RST));
  FDRE \slv_reg2_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg2[11]),
        .R(RST));
  FDRE \slv_reg2_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg2[12]),
        .R(RST));
  FDRE \slv_reg2_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg2[13]),
        .R(RST));
  FDRE \slv_reg2_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg2[14]),
        .R(RST));
  FDRE \slv_reg2_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg2[15]),
        .R(RST));
  FDRE \slv_reg2_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg2[16]),
        .R(RST));
  FDRE \slv_reg2_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg2[17]),
        .R(RST));
  FDRE \slv_reg2_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg2[18]),
        .R(RST));
  FDRE \slv_reg2_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg2[19]),
        .R(RST));
  FDRE \slv_reg2_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg2[1]),
        .R(RST));
  FDRE \slv_reg2_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg2[20]),
        .R(RST));
  FDRE \slv_reg2_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg2[21]),
        .R(RST));
  FDRE \slv_reg2_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg2[22]),
        .R(RST));
  FDRE \slv_reg2_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg2[23]),
        .R(RST));
  FDRE \slv_reg2_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg2[24]),
        .R(RST));
  FDRE \slv_reg2_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg2[25]),
        .R(RST));
  FDRE \slv_reg2_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg2[26]),
        .R(RST));
  FDRE \slv_reg2_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg2[27]),
        .R(RST));
  FDRE \slv_reg2_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg2[28]),
        .R(RST));
  FDRE \slv_reg2_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg2[29]),
        .R(RST));
  FDRE \slv_reg2_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg2[2]),
        .R(RST));
  FDRE \slv_reg2_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg2[30]),
        .R(RST));
  FDRE \slv_reg2_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg2[31]),
        .R(RST));
  FDRE \slv_reg2_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg2[3]),
        .R(RST));
  FDRE \slv_reg2_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg2[4]),
        .R(RST));
  FDRE \slv_reg2_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg2[5]),
        .R(RST));
  FDRE \slv_reg2_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg2[6]),
        .R(RST));
  FDRE \slv_reg2_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg2[7]),
        .R(RST));
  FDRE \slv_reg2_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg2[8]),
        .R(RST));
  FDRE \slv_reg2_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg2[9]),
        .R(RST));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \slv_reg30[15]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg8[31]_i_2_n_0 ),
        .I3(p_0_in[1]),
        .I4(p_0_in[3]),
        .I5(s00_axi_wstrb[1]),
        .O(\slv_reg30[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \slv_reg30[23]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg8[31]_i_2_n_0 ),
        .I3(p_0_in[1]),
        .I4(p_0_in[3]),
        .I5(s00_axi_wstrb[2]),
        .O(\slv_reg30[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \slv_reg30[31]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg8[31]_i_2_n_0 ),
        .I3(p_0_in[1]),
        .I4(p_0_in[3]),
        .I5(s00_axi_wstrb[3]),
        .O(\slv_reg30[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \slv_reg30[7]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg8[31]_i_2_n_0 ),
        .I3(p_0_in[1]),
        .I4(p_0_in[3]),
        .I5(s00_axi_wstrb[0]),
        .O(\slv_reg30[7]_i_1_n_0 ));
  FDRE \slv_reg30_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg30[0]),
        .R(RST));
  FDRE \slv_reg30_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg30[10]),
        .R(RST));
  FDRE \slv_reg30_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg30[11]),
        .R(RST));
  FDRE \slv_reg30_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg30[12]),
        .R(RST));
  FDRE \slv_reg30_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg30[13]),
        .R(RST));
  FDRE \slv_reg30_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg30[14]),
        .R(RST));
  FDRE \slv_reg30_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg30[15]),
        .R(RST));
  FDRE \slv_reg30_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg30[16]),
        .R(RST));
  FDRE \slv_reg30_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg30[17]),
        .R(RST));
  FDRE \slv_reg30_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg30[18]),
        .R(RST));
  FDRE \slv_reg30_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg30[19]),
        .R(RST));
  FDRE \slv_reg30_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg30[1]),
        .R(RST));
  FDRE \slv_reg30_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg30[20]),
        .R(RST));
  FDRE \slv_reg30_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg30[21]),
        .R(RST));
  FDRE \slv_reg30_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg30[22]),
        .R(RST));
  FDRE \slv_reg30_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg30[23]),
        .R(RST));
  FDRE \slv_reg30_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg30[24]),
        .R(RST));
  FDRE \slv_reg30_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg30[25]),
        .R(RST));
  FDRE \slv_reg30_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg30[26]),
        .R(RST));
  FDRE \slv_reg30_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg30[27]),
        .R(RST));
  FDRE \slv_reg30_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg30[28]),
        .R(RST));
  FDRE \slv_reg30_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg30[29]),
        .R(RST));
  FDRE \slv_reg30_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg30[2]),
        .R(RST));
  FDRE \slv_reg30_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg30[30]),
        .R(RST));
  FDRE \slv_reg30_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg30[31]),
        .R(RST));
  FDRE \slv_reg30_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg30[3]),
        .R(RST));
  FDRE \slv_reg30_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg30[4]),
        .R(RST));
  FDRE \slv_reg30_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg30[5]),
        .R(RST));
  FDRE \slv_reg30_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg30[6]),
        .R(RST));
  FDRE \slv_reg30_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg30[7]),
        .R(RST));
  FDRE \slv_reg30_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg30[8]),
        .R(RST));
  FDRE \slv_reg30_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg30[9]),
        .R(RST));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \slv_reg31[15]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg7[31]_i_2_n_0 ),
        .I3(p_0_in[1]),
        .I4(p_0_in[3]),
        .I5(s00_axi_wstrb[1]),
        .O(\slv_reg31[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \slv_reg31[23]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg7[31]_i_2_n_0 ),
        .I3(p_0_in[1]),
        .I4(p_0_in[3]),
        .I5(s00_axi_wstrb[2]),
        .O(\slv_reg31[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \slv_reg31[31]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg7[31]_i_2_n_0 ),
        .I3(p_0_in[1]),
        .I4(p_0_in[3]),
        .I5(s00_axi_wstrb[3]),
        .O(\slv_reg31[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \slv_reg31[7]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg7[31]_i_2_n_0 ),
        .I3(p_0_in[1]),
        .I4(p_0_in[3]),
        .I5(s00_axi_wstrb[0]),
        .O(\slv_reg31[7]_i_1_n_0 ));
  FDRE \slv_reg31_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg31[0]),
        .R(RST));
  FDRE \slv_reg31_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg31[10]),
        .R(RST));
  FDRE \slv_reg31_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg31[11]),
        .R(RST));
  FDRE \slv_reg31_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg31[12]),
        .R(RST));
  FDRE \slv_reg31_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg31[13]),
        .R(RST));
  FDRE \slv_reg31_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg31[14]),
        .R(RST));
  FDRE \slv_reg31_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg31[15]),
        .R(RST));
  FDRE \slv_reg31_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg31[16]),
        .R(RST));
  FDRE \slv_reg31_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg31[17]),
        .R(RST));
  FDRE \slv_reg31_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg31[18]),
        .R(RST));
  FDRE \slv_reg31_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg31[19]),
        .R(RST));
  FDRE \slv_reg31_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg31[1]),
        .R(RST));
  FDRE \slv_reg31_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg31[20]),
        .R(RST));
  FDRE \slv_reg31_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg31[21]),
        .R(RST));
  FDRE \slv_reg31_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg31[22]),
        .R(RST));
  FDRE \slv_reg31_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg31[23]),
        .R(RST));
  FDRE \slv_reg31_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg31[24]),
        .R(RST));
  FDRE \slv_reg31_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg31[25]),
        .R(RST));
  FDRE \slv_reg31_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg31[26]),
        .R(RST));
  FDRE \slv_reg31_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg31[27]),
        .R(RST));
  FDRE \slv_reg31_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg31[28]),
        .R(RST));
  FDRE \slv_reg31_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg31[29]),
        .R(RST));
  FDRE \slv_reg31_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg31[2]),
        .R(RST));
  FDRE \slv_reg31_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg31[30]),
        .R(RST));
  FDRE \slv_reg31_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg31[31]),
        .R(RST));
  FDRE \slv_reg31_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg31[3]),
        .R(RST));
  FDRE \slv_reg31_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg31[4]),
        .R(RST));
  FDRE \slv_reg31_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg31[5]),
        .R(RST));
  FDRE \slv_reg31_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg31[6]),
        .R(RST));
  FDRE \slv_reg31_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg31[7]),
        .R(RST));
  FDRE \slv_reg31_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg31[8]),
        .R(RST));
  FDRE \slv_reg31_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg31[9]),
        .R(RST));
  LUT6 #(
    .INIT(64'h0000000010000000)) 
    \slv_reg3[15]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg7[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[1]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(\slv_reg3[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000010000000)) 
    \slv_reg3[23]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg7[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(\slv_reg3[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000010000000)) 
    \slv_reg3[31]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg7[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[3]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(\slv_reg3[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000010000000)) 
    \slv_reg3[7]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg7[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[0]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(\slv_reg3[7]_i_1_n_0 ));
  FDRE \slv_reg3_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg3[0]),
        .R(RST));
  FDRE \slv_reg3_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg3[10]),
        .R(RST));
  FDRE \slv_reg3_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg3[11]),
        .R(RST));
  FDRE \slv_reg3_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg3[12]),
        .R(RST));
  FDRE \slv_reg3_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg3[13]),
        .R(RST));
  FDRE \slv_reg3_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg3[14]),
        .R(RST));
  FDRE \slv_reg3_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg3[15]),
        .R(RST));
  FDRE \slv_reg3_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg3[16]),
        .R(RST));
  FDRE \slv_reg3_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg3[17]),
        .R(RST));
  FDRE \slv_reg3_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg3[18]),
        .R(RST));
  FDRE \slv_reg3_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg3[19]),
        .R(RST));
  FDRE \slv_reg3_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg3[1]),
        .R(RST));
  FDRE \slv_reg3_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg3[20]),
        .R(RST));
  FDRE \slv_reg3_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg3[21]),
        .R(RST));
  FDRE \slv_reg3_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg3[22]),
        .R(RST));
  FDRE \slv_reg3_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg3[23]),
        .R(RST));
  FDRE \slv_reg3_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg3[24]),
        .R(RST));
  FDRE \slv_reg3_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg3[25]),
        .R(RST));
  FDRE \slv_reg3_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg3[26]),
        .R(RST));
  FDRE \slv_reg3_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg3[27]),
        .R(RST));
  FDRE \slv_reg3_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg3[28]),
        .R(RST));
  FDRE \slv_reg3_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg3[29]),
        .R(RST));
  FDRE \slv_reg3_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg3[2]),
        .R(RST));
  FDRE \slv_reg3_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg3[30]),
        .R(RST));
  FDRE \slv_reg3_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg3[31]),
        .R(RST));
  FDRE \slv_reg3_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg3[3]),
        .R(RST));
  FDRE \slv_reg3_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg3[4]),
        .R(RST));
  FDRE \slv_reg3_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg3[5]),
        .R(RST));
  FDRE \slv_reg3_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg3[6]),
        .R(RST));
  FDRE \slv_reg3_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg3[7]),
        .R(RST));
  FDRE \slv_reg3_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg3[8]),
        .R(RST));
  FDRE \slv_reg3_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg3[9]),
        .R(RST));
  LUT6 #(
    .INIT(64'h0000000000004000)) 
    \slv_reg4[15]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg8[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[1]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(\slv_reg4[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000004000)) 
    \slv_reg4[23]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg8[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(\slv_reg4[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000004000)) 
    \slv_reg4[31]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg8[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[3]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(\slv_reg4[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000004000)) 
    \slv_reg4[7]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg8[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[0]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(\slv_reg4[7]_i_1_n_0 ));
  FDRE \slv_reg4_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg4[0]),
        .R(RST));
  FDRE \slv_reg4_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg4[10]),
        .R(RST));
  FDRE \slv_reg4_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg4[11]),
        .R(RST));
  FDRE \slv_reg4_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg4[12]),
        .R(RST));
  FDRE \slv_reg4_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg4[13]),
        .R(RST));
  FDRE \slv_reg4_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg4[14]),
        .R(RST));
  FDRE \slv_reg4_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg4[15]),
        .R(RST));
  FDRE \slv_reg4_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg4[16]),
        .R(RST));
  FDRE \slv_reg4_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg4[17]),
        .R(RST));
  FDRE \slv_reg4_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg4[18]),
        .R(RST));
  FDRE \slv_reg4_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg4[19]),
        .R(RST));
  FDRE \slv_reg4_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg4[1]),
        .R(RST));
  FDRE \slv_reg4_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg4[20]),
        .R(RST));
  FDRE \slv_reg4_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg4[21]),
        .R(RST));
  FDRE \slv_reg4_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg4[22]),
        .R(RST));
  FDRE \slv_reg4_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg4[23]),
        .R(RST));
  FDRE \slv_reg4_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg4[24]),
        .R(RST));
  FDRE \slv_reg4_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg4[25]),
        .R(RST));
  FDRE \slv_reg4_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg4[26]),
        .R(RST));
  FDRE \slv_reg4_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg4[27]),
        .R(RST));
  FDRE \slv_reg4_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg4[28]),
        .R(RST));
  FDRE \slv_reg4_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg4[29]),
        .R(RST));
  FDRE \slv_reg4_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg4[2]),
        .R(RST));
  FDRE \slv_reg4_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg4[30]),
        .R(RST));
  FDRE \slv_reg4_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg4[31]),
        .R(RST));
  FDRE \slv_reg4_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg4[3]),
        .R(RST));
  FDRE \slv_reg4_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg4[4]),
        .R(RST));
  FDRE \slv_reg4_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg4[5]),
        .R(RST));
  FDRE \slv_reg4_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg4[6]),
        .R(RST));
  FDRE \slv_reg4_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg4[7]),
        .R(RST));
  FDRE \slv_reg4_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg4[8]),
        .R(RST));
  FDRE \slv_reg4_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg4[9]),
        .R(RST));
  LUT6 #(
    .INIT(64'h0000000008000000)) 
    \slv_reg7[15]_i_1 
       (.I0(\slv_reg7[31]_i_2_n_0 ),
        .I1(p_0_in[2]),
        .I2(p_0_in[4]),
        .I3(s00_axi_wstrb[1]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(\slv_reg7[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000008000000)) 
    \slv_reg7[23]_i_1 
       (.I0(\slv_reg7[31]_i_2_n_0 ),
        .I1(p_0_in[2]),
        .I2(p_0_in[4]),
        .I3(s00_axi_wstrb[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(\slv_reg7[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000008000000)) 
    \slv_reg7[31]_i_1 
       (.I0(\slv_reg7[31]_i_2_n_0 ),
        .I1(p_0_in[2]),
        .I2(p_0_in[4]),
        .I3(s00_axi_wstrb[3]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(\slv_reg7[31]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h80000000)) 
    \slv_reg7[31]_i_2 
       (.I0(s00_axi_awvalid),
        .I1(s00_axi_wvalid),
        .I2(s00_axi_awready),
        .I3(s00_axi_wready),
        .I4(p_0_in[0]),
        .O(\slv_reg7[31]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000008000000)) 
    \slv_reg7[7]_i_1 
       (.I0(\slv_reg7[31]_i_2_n_0 ),
        .I1(p_0_in[2]),
        .I2(p_0_in[4]),
        .I3(s00_axi_wstrb[0]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(\slv_reg7[7]_i_1_n_0 ));
  FDRE \slv_reg7_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg7),
        .R(RST));
  FDRE \slv_reg7_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg7__0[10]),
        .R(RST));
  FDRE \slv_reg7_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg7__0[11]),
        .R(RST));
  FDRE \slv_reg7_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg7__0[12]),
        .R(RST));
  FDRE \slv_reg7_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg7__0[13]),
        .R(RST));
  FDRE \slv_reg7_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg7__0[14]),
        .R(RST));
  FDRE \slv_reg7_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg7__0[15]),
        .R(RST));
  FDRE \slv_reg7_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg7__0[16]),
        .R(RST));
  FDRE \slv_reg7_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg7__0[17]),
        .R(RST));
  FDRE \slv_reg7_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg7__0[18]),
        .R(RST));
  FDRE \slv_reg7_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg7__0[19]),
        .R(RST));
  FDRE \slv_reg7_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg7__0[1]),
        .R(RST));
  FDRE \slv_reg7_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg7__0[20]),
        .R(RST));
  FDRE \slv_reg7_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg7__0[21]),
        .R(RST));
  FDRE \slv_reg7_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg7__0[22]),
        .R(RST));
  FDRE \slv_reg7_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg7__0[23]),
        .R(RST));
  FDRE \slv_reg7_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg7__0[24]),
        .R(RST));
  FDRE \slv_reg7_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg7__0[25]),
        .R(RST));
  FDRE \slv_reg7_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg7__0[26]),
        .R(RST));
  FDRE \slv_reg7_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg7__0[27]),
        .R(RST));
  FDRE \slv_reg7_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg7__0[28]),
        .R(RST));
  FDRE \slv_reg7_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg7__0[29]),
        .R(RST));
  FDRE \slv_reg7_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg7__0[2]),
        .R(RST));
  FDRE \slv_reg7_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg7__0[30]),
        .R(RST));
  FDRE \slv_reg7_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg7__0[31]),
        .R(RST));
  FDRE \slv_reg7_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg7__0[3]),
        .R(RST));
  FDRE \slv_reg7_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg7__0[4]),
        .R(RST));
  FDRE \slv_reg7_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg7__0[5]),
        .R(RST));
  FDRE \slv_reg7_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg7__0[6]),
        .R(RST));
  FDRE \slv_reg7_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg7__0[7]),
        .R(RST));
  FDRE \slv_reg7_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg7__0[8]),
        .R(RST));
  FDRE \slv_reg7_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg7__0[9]),
        .R(RST));
  LUT6 #(
    .INIT(64'h0000000010000000)) 
    \slv_reg8[15]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg8[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[1]),
        .I4(p_0_in[3]),
        .I5(p_0_in[1]),
        .O(\slv_reg8[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000010000000)) 
    \slv_reg8[23]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg8[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[2]),
        .I4(p_0_in[3]),
        .I5(p_0_in[1]),
        .O(\slv_reg8[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000010000000)) 
    \slv_reg8[31]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg8[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[3]),
        .I4(p_0_in[3]),
        .I5(p_0_in[1]),
        .O(\slv_reg8[31]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h00008000)) 
    \slv_reg8[31]_i_2 
       (.I0(s00_axi_awvalid),
        .I1(s00_axi_wvalid),
        .I2(s00_axi_awready),
        .I3(s00_axi_wready),
        .I4(p_0_in[0]),
        .O(\slv_reg8[31]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000010000000)) 
    \slv_reg8[7]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg8[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[0]),
        .I4(p_0_in[3]),
        .I5(p_0_in[1]),
        .O(\slv_reg8[7]_i_1_n_0 ));
  FDRE \slv_reg8_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg8[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg8[0]),
        .R(RST));
  FDRE \slv_reg8_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg8[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg8__0[10]),
        .R(RST));
  FDRE \slv_reg8_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg8[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg8__0[11]),
        .R(RST));
  FDRE \slv_reg8_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg8[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg8__0[12]),
        .R(RST));
  FDRE \slv_reg8_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg8[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg8__0[13]),
        .R(RST));
  FDRE \slv_reg8_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg8[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg8__0[14]),
        .R(RST));
  FDRE \slv_reg8_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg8[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg8__0[15]),
        .R(RST));
  FDRE \slv_reg8_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg8[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg8__0[16]),
        .R(RST));
  FDRE \slv_reg8_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg8[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg8__0[17]),
        .R(RST));
  FDRE \slv_reg8_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg8[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg8__0[18]),
        .R(RST));
  FDRE \slv_reg8_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg8[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg8__0[19]),
        .R(RST));
  FDRE \slv_reg8_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg8[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg8[1]),
        .R(RST));
  FDRE \slv_reg8_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg8[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg8__0[20]),
        .R(RST));
  FDRE \slv_reg8_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg8[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg8__0[21]),
        .R(RST));
  FDRE \slv_reg8_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg8[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg8__0[22]),
        .R(RST));
  FDRE \slv_reg8_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg8[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg8__0[23]),
        .R(RST));
  FDRE \slv_reg8_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg8[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg8__0[24]),
        .R(RST));
  FDRE \slv_reg8_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg8[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg8__0[25]),
        .R(RST));
  FDRE \slv_reg8_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg8[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg8__0[26]),
        .R(RST));
  FDRE \slv_reg8_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg8[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg8__0[27]),
        .R(RST));
  FDRE \slv_reg8_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg8[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg8__0[28]),
        .R(RST));
  FDRE \slv_reg8_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg8[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg8__0[29]),
        .R(RST));
  FDRE \slv_reg8_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg8[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg8[2]),
        .R(RST));
  FDRE \slv_reg8_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg8[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg8__0[30]),
        .R(RST));
  FDRE \slv_reg8_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg8[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg8__0[31]),
        .R(RST));
  FDRE \slv_reg8_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg8[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg8[3]),
        .R(RST));
  FDRE \slv_reg8_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg8[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg8[4]),
        .R(RST));
  FDRE \slv_reg8_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg8[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg8[5]),
        .R(RST));
  FDRE \slv_reg8_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg8[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg8[6]),
        .R(RST));
  FDRE \slv_reg8_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg8[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg8[7]),
        .R(RST));
  FDRE \slv_reg8_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg8[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg8[8]),
        .R(RST));
  FDRE \slv_reg8_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg8[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg8[9]),
        .R(RST));
  LUT6 #(
    .INIT(64'h0000000010000000)) 
    \slv_reg9[15]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg7[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[1]),
        .I4(p_0_in[3]),
        .I5(p_0_in[1]),
        .O(\slv_reg9[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000010000000)) 
    \slv_reg9[23]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg7[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[2]),
        .I4(p_0_in[3]),
        .I5(p_0_in[1]),
        .O(\slv_reg9[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000010000000)) 
    \slv_reg9[31]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg7[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[3]),
        .I4(p_0_in[3]),
        .I5(p_0_in[1]),
        .O(\slv_reg9[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000010000000)) 
    \slv_reg9[7]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[2]),
        .I2(\slv_reg7[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[0]),
        .I4(p_0_in[3]),
        .I5(p_0_in[1]),
        .O(\slv_reg9[7]_i_1_n_0 ));
  FDRE \slv_reg9_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg9[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg9[0]),
        .R(RST));
  FDRE \slv_reg9_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg9[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg9[10]),
        .R(RST));
  FDRE \slv_reg9_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg9[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg9[11]),
        .R(RST));
  FDRE \slv_reg9_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg9[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg9[12]),
        .R(RST));
  FDRE \slv_reg9_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg9[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg9[13]),
        .R(RST));
  FDRE \slv_reg9_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg9[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg9[14]),
        .R(RST));
  FDRE \slv_reg9_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg9[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg9[15]),
        .R(RST));
  FDRE \slv_reg9_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg9[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg9__0[16]),
        .R(RST));
  FDRE \slv_reg9_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg9[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg9__0[17]),
        .R(RST));
  FDRE \slv_reg9_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg9[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg9__0[18]),
        .R(RST));
  FDRE \slv_reg9_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg9[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg9__0[19]),
        .R(RST));
  FDRE \slv_reg9_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg9[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg9[1]),
        .R(RST));
  FDRE \slv_reg9_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg9[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg9__0[20]),
        .R(RST));
  FDRE \slv_reg9_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg9[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg9__0[21]),
        .R(RST));
  FDRE \slv_reg9_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg9[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg9__0[22]),
        .R(RST));
  FDRE \slv_reg9_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg9[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg9__0[23]),
        .R(RST));
  FDRE \slv_reg9_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg9[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg9__0[24]),
        .R(RST));
  FDRE \slv_reg9_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg9[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg9__0[25]),
        .R(RST));
  FDRE \slv_reg9_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg9[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg9__0[26]),
        .R(RST));
  FDRE \slv_reg9_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg9[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg9__0[27]),
        .R(RST));
  FDRE \slv_reg9_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg9[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg9__0[28]),
        .R(RST));
  FDRE \slv_reg9_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg9[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg9__0[29]),
        .R(RST));
  FDRE \slv_reg9_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg9[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg9[2]),
        .R(RST));
  FDRE \slv_reg9_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg9[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg9__0[30]),
        .R(RST));
  FDRE \slv_reg9_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg9[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg9__0[31]),
        .R(RST));
  FDRE \slv_reg9_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg9[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg9[3]),
        .R(RST));
  FDRE \slv_reg9_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg9[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg9[4]),
        .R(RST));
  FDRE \slv_reg9_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg9[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg9[5]),
        .R(RST));
  FDRE \slv_reg9_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg9[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg9[6]),
        .R(RST));
  FDRE \slv_reg9_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg9[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg9[7]),
        .R(RST));
  FDRE \slv_reg9_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg9[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg9[8]),
        .R(RST));
  FDRE \slv_reg9_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg9[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg9[9]),
        .R(RST));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_unimacro_BRAM_SDP_MACRO
   (DOADO,
    S,
    \encoded_reg[8] ,
    s00_axi_aclk,
    WREN,
    s00_axi_aresetn,
    Q,
    ADDRBWRADDR,
    DIBDI,
    \processRow_reg[5] );
  output [4:0]DOADO;
  output [0:0]S;
  output \encoded_reg[8] ;
  input s00_axi_aclk;
  input WREN;
  input s00_axi_aresetn;
  input [9:0]Q;
  input [9:0]ADDRBWRADDR;
  input [15:0]DIBDI;
  input [2:0]\processRow_reg[5] ;

  wire [9:0]ADDRBWRADDR;
  wire [15:0]DIBDI;
  wire [4:0]DOADO;
  wire [9:0]Q;
  wire [0:0]S;
  wire WREN;
  wire \encoded[8]_i_16_n_0 ;
  wire \encoded[8]_i_17_n_0 ;
  wire \encoded_reg[8] ;
  wire [2:0]\processRow_reg[5] ;
  wire [13:11]readL;
  wire s00_axi_aclk;
  wire s00_axi_aresetn;
  wire \sdp_bl.ramb18_dp_bl.ram18_bl_n_10 ;
  wire \sdp_bl.ramb18_dp_bl.ram18_bl_n_11 ;
  wire \sdp_bl.ramb18_dp_bl.ram18_bl_n_12 ;
  wire \sdp_bl.ramb18_dp_bl.ram18_bl_n_13 ;
  wire \sdp_bl.ramb18_dp_bl.ram18_bl_n_14 ;
  wire \sdp_bl.ramb18_dp_bl.ram18_bl_n_15 ;
  wire \sdp_bl.ramb18_dp_bl.ram18_bl_n_8 ;
  wire \sdp_bl.ramb18_dp_bl.ram18_bl_n_9 ;
  wire [15:0]\NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOBDO_UNCONNECTED ;
  wire [1:0]\NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOPADOP_UNCONNECTED ;
  wire [1:0]\NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOPBDOP_UNCONNECTED ;

  LUT6 #(
    .INIT(64'h0000000006609006)) 
    \encoded[8]_i_11 
       (.I0(readL[13]),
        .I1(\processRow_reg[5] [2]),
        .I2(\processRow_reg[5] [1]),
        .I3(\encoded[8]_i_16_n_0 ),
        .I4(readL[12]),
        .I5(\encoded[8]_i_17_n_0 ),
        .O(S));
  LUT6 #(
    .INIT(64'hAAAAAAAAAAAAA888)) 
    \encoded[8]_i_15 
       (.I0(readL[13]),
        .I1(readL[12]),
        .I2(DOADO[0]),
        .I3(DOADO[1]),
        .I4(DOADO[2]),
        .I5(readL[11]),
        .O(\encoded_reg[8] ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h0007)) 
    \encoded[8]_i_16 
       (.I0(DOADO[0]),
        .I1(DOADO[1]),
        .I2(DOADO[2]),
        .I3(readL[11]),
        .O(\encoded[8]_i_16_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h5666A999)) 
    \encoded[8]_i_17 
       (.I0(\processRow_reg[5] [0]),
        .I1(DOADO[2]),
        .I2(DOADO[1]),
        .I3(DOADO[0]),
        .I4(readL[11]),
        .O(\encoded[8]_i_17_n_0 ));
  (* box_type = "PRIMITIVE" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h8BC28AFA8A31896988A087D8870F8646857D84B583EC8323825A819180C87FFF),
    .INIT_01(256'hA74FA777A79AA7B7A7CEA7DFA7EAA7EEA7ECA7E4A7D5A7BFA7A3A77FA7547FFF),
    .INIT_02(256'hA269A2D1A337A39AA3FBA458A4B1A507A55AA5A8A5F2A638A67AA6B6A6EE7FFF),
    .INIT_03(256'h9B369BAB9C209C959D0C9D829DF89E6E9EE49F599FCDA041A0B2A123A192A1FE),
    .INIT_04(256'h94FD954B959C95F1964A96A59704976597C9983098999904997199E09A519AC3),
    .INIT_05(256'h92729276927F928E92A192BA92D892FA9321934D937E93B393ED942B946D94B3),
    .INIT_06(256'h950794B6946A942493E293A6936F933E931292EB92C992AD92979286927A9273),
    .INIT_07(256'h9C869BF19B5E9ACF9A4499BC993998B9983E97C7975496E6967D961895B8955D),
    .INIT_08(256'hA719A667A5B4A503A452A3A1A2F2A245A199A0EEA0469FA09EFC9E5A9DBB9D1F),
    .INIT_09(256'hB1B3B11AB07EAFDFAF3DAE99ADF2AD48AC9DABF0AB42AA92A9E2A930A87EA7CC),
    .INIT_0A(256'hB8CFB888B83AB7E6B78BB72BB6C5B659B5E8B571B4F6B476B3F1B367B2DAB248),
    .INIT_0B(256'hB948B97EB9ABB9CFB9EBB9FFBA0ABA0DBA08B9FBB9E7B9CAB9A6B97BB949B90F),
    .INIT_0C(256'hB126B1ECB2A9B35EB409B4ABB544B5D3B65AB6D7B74BB7B7B819B872B8C2B909),
    .INIT_0D(256'hA020A16EA2B4A3F3A529A657A77EA89CA9B1AABEABC3ACBFADB2AE9CAF7EB056),
    .INIT_0E(256'h87C789768B218CC68E6790039199932A94B4963997B7992F9AA19C0B9D6F9ECB),
    .INIT_0F(256'h6B356D0A6EE070B57288745B762C77FB79C87B927D5B7F2080E282A1845C8614),
    .INIT_10(256'h4E6D502751E453A55569573058F95AC65C945E646036620963DD65B36788695E),
    .INIT_11(256'h358236E2384A39B83B2D3CA93E2A3FB3414042D4446D460C47B049584B054CB7),
    .INIT_12(256'h23B7249425792667275F285F29672A782B922CB42DDE2F11304B318E32D83429),
    .INIT_13(256'h1AD21B1C1B6E1BCA1C2F1C9D1D141D941E1E1EB11F4D1FF220A12159221A22E4),
    .INIT_14(256'h1AC71A8C1A581A2C1A0719EB19D619C919C419C819D419E91A061A2C1A5A1A92),
    .INIT_15(256'h21CC213420A020121F871F021E821E071D921D231CB91C561BF81BA21B521B09),
    .INIT_16(256'h2CD82C1A2B5D2AA129E5292C287427BD2709265725A724FB245123AA23072267),
    .INIT_17(256'h386637B9370B365935A634F03439338032C6320A314E30902FD22F142E552D96),
    .INIT_18(256'h414E40DF406B3FF33F763EF53E6F3DE53D573CC63C303B973AFA3A5939B6390F),
    .INIT_19(256'h458A456E454C452544F944C744904454441343CC4380432F42D9427E421D41B8),
    .INIT_1A(256'h44AE44DE450A453245554574458E45A445B545C145C845CB45C845C045B445A1),
    .INIT_1B(256'h3FF8405540B1410B416341B8420C425D42AC42F74340438643C844074442447A),
    .INIT_1C(256'h39FE3A583AB33B103B6F3BCE3C2F3C903CF13D533DB63E173E793EDA3F3B3F9A),
    .INIT_1D(256'h35FE361F3645367136A036D5370D3749378A37CE3815386038AD38FE395139A6),
    .INIT_1E(256'h370A36C7368C3658362A360435E435CB35B935AD35A735A735AD35B935CA35E1),
    .INIT_1F(256'h3F3A3E7C3DC73D193C733BD63B3F3AB13A2B39AC393538C5385E37FE37A53754),
    .INIT_20(256'h4F134DE04CB34B8E4A6F49574846473D463B4540444D4361427D41A140CC3FFF),
    .INIT_21(256'h655263CA624660C65F4A5DD35C615AF3598B582856CA5572541F52D3518D504D),
    .INIT_22(256'h7F1E7D787BD37A2E788976E5754273A0720070616EC56D2B6B9369FE686C66DD),
    .INIT_23(256'h989F971D9597940D928090EF8F5A8DC48C2A8A8E88F0875085AF840C826880C3),
    .INIT_24(256'hADCFACAEAB85AA55A91EA7E0A69BA54FA3FDA2A5A1469FE29E799D0A9B969A1D),
    .INIT_25(256'hBB59BAC5BA28B981B8D0B816B753B687B5B1B4D3B3ECB2FCB203B102AFF9AEE8),
    .INIT_26(256'hBF57BF61BF62BF59BF46BF29BF02BED1BE96BE51BE02BDAABD47BCDABC64BBE3),
    .INIT_27(256'hB9B3BA4FBAE3BB6FBBF2BC6DBCDFBD48BDA7BDFEBE4CBE90BECBBEFCBF24BF42),
    .INIT_28(256'hAC2BAD31AE32AF2EB024B114B1FEB2E2B3C0B498B568B632B6F4B7B0B863B90F),
    .INIT_29(256'h99E09B169C4B9D7F9EB19FE1A10FA23AA363A489A5ABA6CBA7E6A8FEAA11AB21),
    .INIT_2A(256'h869D87C688F28A208B508C828DB58EEA90209157928F93C895009639977198A9),
    .INIT_2B(256'h75F876E077CE78C079B87AB47BB57CBB7DC57ED37FE580FB8215833284538576),
    .INIT_2C(256'h6A846B0D6B9C6C326CCE6D716E196EC86F7D703870F971C0728C735F74377514),
    .INIT_2D(256'h6546656D659965CB66036641668566CF671F677667D36836689F690F69856A01),
    .INIT_2E(256'h6588656465436526650D64F864E764DB64D364CF64D164D764E364F4650A6525),
    .INIT_2F(256'h691A68D768936851680F67CF678F6751671566DB66A3666D6639660965DB65B0),
    .INIT_30(256'h6CEE6CBF6C8D6C586C216BE76BAC6B6F6B306AF06AAF6A6D6A2A69E669A2695E),
    .INIT_31(256'h6DE86DFA6E086E106E146E126E0C6E026DF36DDF6DC86DAD6D8D6D6B6D446D1B),
    .INIT_32(256'h69B06A216A8C6AF16B4F6BA76BF96C456C8B6CCA6D046D386D676D8F6DB26DD0),
    .INIT_33(256'h5F5E602F60FB61C16281633C63F1649F654865EB6687671E67AE683868BC6939),
    .INIT_34(256'h4FBD50D551E952FB540955135619571B581959135A075AF85BE35CCA5DAB5E87),
    .INIT_35(256'h3D343E633F9140C041EE431C444A457646A247CC48F54A1C4B414C634D844EA2),
    .INIT_36(256'h2B452C4C2D582E682F7C309331AE32CB33EC350E3634375B388439AE3ADA3C07),
    .INIT_37(256'h1DBE1E641F121FC720842147221222E323BA2498257C26652754284929432A41),
    .INIT_38(256'h17E217FA181D1849187E18BD1906195719B21A151A821AF71B741BFB1C891D1F),
    .INIT_39(256'h1BA01B191A9C1A2919C01961190C18C118801849181C17FA17E117D217CE17D3),
    .INIT_3A(256'h2922280626F425E924E723EE22FE2217213920651F9A1ED81E201D711CCD1C32),
    .INIT_3B(256'h3EB43D2C3BAA3A2D38B5374435D83473331531BD306C2F222DDF2CA42B712A45),
    .INIT_3C(256'h5928576F55B75401524C50994EE94D3B4B8F49E7484246A04502436841D24041),
    .INIT_3D(256'h748072D8712C6F7E6DCE6C1B6A6768B166F96540638761CC60125E575C9C5AE1),
    .INIT_3E(256'h8CD18B738A0F88A5873685C0844682C681417FB87E2A7C977B01796677C77625),
    .INIT_3F(256'h9F199E2B9D369C399B349A29991597FB96D995B094809349920B90C67FFF8E29),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("NONE"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(0),
    .WRITE_WIDTH_B(18)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl 
       (.ADDRARDADDR({Q,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({ADDRBWRADDR,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(s00_axi_aclk),
        .CLKBWRCLK(s00_axi_aclk),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DIBDI(DIBDI),
        .DIPADIP({1'b0,1'b0}),
        .DIPBDIP({1'b0,1'b0}),
        .DOADO({DOADO[4:3],readL,DOADO[2:0],\sdp_bl.ramb18_dp_bl.ram18_bl_n_8 ,\sdp_bl.ramb18_dp_bl.ram18_bl_n_9 ,\sdp_bl.ramb18_dp_bl.ram18_bl_n_10 ,\sdp_bl.ramb18_dp_bl.ram18_bl_n_11 ,\sdp_bl.ramb18_dp_bl.ram18_bl_n_12 ,\sdp_bl.ramb18_dp_bl.ram18_bl_n_13 ,\sdp_bl.ramb18_dp_bl.ram18_bl_n_14 ,\sdp_bl.ramb18_dp_bl.ram18_bl_n_15 }),
        .DOBDO(\NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOBDO_UNCONNECTED [15:0]),
        .DOPADOP(\NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOPADOP_UNCONNECTED [1:0]),
        .DOPBDOP(\NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOPBDOP_UNCONNECTED [1:0]),
        .ENARDEN(1'b1),
        .ENBWREN(WREN),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(s00_axi_aresetn),
        .RSTRAMB(s00_axi_aresetn),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({1'b0,1'b0}),
        .WEBWE({1'b1,1'b1,1'b1,1'b1}));
endmodule

(* ORIG_REF_NAME = "unimacro_BRAM_SDP_MACRO" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_unimacro_BRAM_SDP_MACRO__parameterized0
   (DOADO,
    ADDRBWRADDR,
    S,
    s00_axi_aclk,
    WREN,
    s00_axi_aresetn,
    Q,
    DIBDI,
    \processRow_reg[5] ,
    \slv_reg8_reg[9] ,
    switch,
    \i_sig_reg[9] ,
    \i_sig_reg[4] ,
    \i_sig_reg[2] );
  output [1:0]DOADO;
  output [9:0]ADDRBWRADDR;
  output [1:0]S;
  input s00_axi_aclk;
  input WREN;
  input s00_axi_aresetn;
  input [9:0]Q;
  input [15:0]DIBDI;
  input [5:0]\processRow_reg[5] ;
  input [9:0]\slv_reg8_reg[9] ;
  input [0:0]switch;
  input [7:0]\i_sig_reg[9] ;
  input \i_sig_reg[4] ;
  input \i_sig_reg[2] ;

  wire [9:0]ADDRBWRADDR;
  wire [15:0]DIBDI;
  wire [1:0]DOADO;
  wire [9:0]Q;
  wire [1:0]S;
  wire WREN;
  wire \i_sig_reg[2] ;
  wire \i_sig_reg[4] ;
  wire [7:0]\i_sig_reg[9] ;
  wire [5:0]\processRow_reg[5] ;
  wire [13:8]readR;
  wire s00_axi_aclk;
  wire s00_axi_aresetn;
  wire \sdp_bl.ramb18_dp_bl.ram18_bl_n_10 ;
  wire \sdp_bl.ramb18_dp_bl.ram18_bl_n_11 ;
  wire \sdp_bl.ramb18_dp_bl.ram18_bl_n_12 ;
  wire \sdp_bl.ramb18_dp_bl.ram18_bl_n_13 ;
  wire \sdp_bl.ramb18_dp_bl.ram18_bl_n_14 ;
  wire \sdp_bl.ramb18_dp_bl.ram18_bl_n_15 ;
  wire \sdp_bl.ramb18_dp_bl.ram18_bl_n_8 ;
  wire \sdp_bl.ramb18_dp_bl.ram18_bl_n_9 ;
  wire [9:0]\slv_reg8_reg[9] ;
  wire [0:0]switch;
  wire [15:0]\NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOBDO_UNCONNECTED ;
  wire [1:0]\NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOPADOP_UNCONNECTED ;
  wire [1:0]\NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOPBDOP_UNCONNECTED ;

  LUT6 #(
    .INIT(64'h9009000000009009)) 
    \encoded[8]_i_7 
       (.I0(readR[12]),
        .I1(\processRow_reg[5] [4]),
        .I2(\processRow_reg[5] [5]),
        .I3(readR[13]),
        .I4(\processRow_reg[5] [3]),
        .I5(readR[11]),
        .O(S[1]));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    \encoded[8]_i_8 
       (.I0(readR[10]),
        .I1(\processRow_reg[5] [2]),
        .I2(\processRow_reg[5] [1]),
        .I3(readR[9]),
        .I4(\processRow_reg[5] [0]),
        .I5(readR[8]),
        .O(S[0]));
  (* box_type = "PRIMITIVE" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h8BC28AF98A31896988A087D8870F8647857E84B583EC8323825A819180C88000),
    .INIT_01(256'h9830976A96A595DF95199452938C92C591FE913790708FA98EE18E198D528C8A),
    .INIT_02(256'hA462A3A2A2E0A21FA15DA09B9FD89F169E529D8F9CCB9C079B439A7F99BA98F5),
    .INIT_03(256'hB03BAF81AEC6AE0BAD4FAC93ABD6AB19AA5CA99EA8E0A821A762A6A3A5E3A523),
    .INIT_04(256'hBB9DBAEBBA39B985B8D2B81DB768B6B3B5FDB547B490B3D8B320B268B1AFB0F5),
    .INIT_05(256'hC66DC5C5C51CC472C3C8C31DC272C1C6C119C06CBFBEBF0FBE60BDB0BD00BC4F),
    .INIT_06(256'hD08FCFF2CF55CEB7CE18CD78CCD8CC37CB95CAF2CA4FC9ABC907C861C7BBC714),
    .INIT_07(256'hD9EAD95BD8CAD839D7A7D714D681D5ECD557D4C1D42AD392D2F9D260D1C5D12A),
    .INIT_08(256'hE268E1E7E165E0E2E05FDFDADF54DECEDE46DDBEDD35DCAADC1FDB93DB06DA79),
    .INIT_09(256'hE9F4E983E910E89DE829E7B3E73DE6C6E64DE5D4E55AE4DFE463E3E5E367E2E8),
    .INIT_0A(256'hF07AF01AEFB8EF56EEF2EE8DEE28EDC1ED59ECF0EC86EC1BEBAFEB41EAD3EA64),
    .INIT_0B(256'hF5ECF59DF54EF4FDF4AAF457F403F3ADF357F2FFF2A6F24CF1F1F195F138F0DA),
    .INIT_0C(256'hFA3BF9FFF9C2F983F943F902F8C0F87DF839F7F3F7ACF765F71CF6D1F686F63A),
    .INIT_0D(256'hFD5DFD34FD0AFCDEFCB2FC84FC54FC24FBF3FBC0FB8CFB57FB20FAE9FAB0FA76),
    .INIT_0E(256'hFF4BFF35FF1EFF06FEEDFED2FEB6FE99FE7BFE5CFE3BFE19FDF6FDD2FDACFD85),
    .INIT_0F(256'hFFFFFFFDFFF9FFF5FFEFFFE8FFE0FFD7FFCCFFC0FFB3FFA5FF95FF85FF73FF5F),
    .INIT_10(256'hFF77FF89FF99FFA9FFB7FFC3FFCFFFD9FFE2FFEAFFF1FFF6FFFAFFFDFFFFFFFF),
    .INIT_11(256'hFDB6FDDBFDFFFE22FE44FE64FE83FEA1FEBEFED9FEF3FF0DFF24FF3BFF50FF64),
    .INIT_12(256'hFABFFAF7FB2FFB65FB99FBCDFC00FC31FC61FC90FCBDFCEAFD15FD3FFD68FD90),
    .INIT_13(256'hF69AF6E5F72FF777F7BFF805F84BF88FF8D2F913F954F993F9D2FA0FFA4BFA86),
    .INIT_14(256'hF150F1ADF209F264F2BDF316F36DF3C4F419F46DF4C0F512F562F5B2F600F64E),
    .INIT_15(256'hEAF0EB5EEBCBEC37ECA1ED0BED74EDDCEE42EEA8EF0CEF70EFD2F033F093F0F2),
    .INIT_16(256'hE388E406E483E4FFE57AE5F4E66DE6E5E75CE7D2E847E8BBE92EE9A0EA11EA81),
    .INIT_17(256'hDB2BDBB8DC43DCCEDD58DDE1DE69DEF1DF77DFFCE081E104E187E209E28AE309),
    .INIT_18(256'hD1EED288D321D3B9D451D4E8D57ED613D6A7D73BD7CDD85FD8F0D980DA0FDA9E),
    .INIT_19(256'hC7E6C88CC931C9D6CA7ACB1DCBBFCC61CD02CDA2CE41CEE0CF7ED01BD0B7D153),
    .INIT_1A(256'hBD2EBDDEBE8EBF3DBFEBC099C146C1F3C29FC34AC3F4C49EC548C5F0C698C740),
    .INIT_1B(256'hB1DFB298B350B408B4BFB576B62CB6E2B797B84CB900B9B4BA67BB1ABBCCBC7D),
    .INIT_1C(256'hA615A6D5A794A853A911A9CFAA8DAB4AAC07ACC4AD80AE3CAEF7AFB1B06CB125),
    .INIT_1D(256'h99ED9AB29B769C3A9CFE9DC29E859F48A00BA0CDA18FA251A313A3D4A494A555),
    .INIT_1E(256'h8D868E4D8F158FDC90A4916B923292F993BF9486954C961296D8979E98639928),
    .INIT_1F(256'h80FD81C6828F8357842084E985B2867B8743880C88D4899D8A658B2D8BF68CBE),
    .INIT_20(256'h7472753B760376CB7794785C792579EE7AB67B7F7C487D117DDA7EA37F6C8034),
    .INIT_21(256'h680368C9698F6A556B1B6BE16CA86D6E6E356EFD6FC4708B7153721A72E273AA),
    .INIT_22(256'h5BD05C915D525E135ED55F98605A611D61E062A46367642C64F065B46679673E),
    .INIT_23(256'h4FF550B0516A522652E2539E545B551855D556935752581058CF598F5A4F5B0F),
    .INIT_24(256'h4491454345F646A9475D481248C7497C4A324AE94BA04C584D104DC84E814F3B),
    .INIT_25(256'h39BF3A673B103BBA3C643D0F3DBB3E673F143FC1406F411E41CD427D432E43DF),
    .INIT_26(256'h2F9A303730D43172321132B1335233F33495353835DB367F372437CA38703917),
    .INIT_27(256'h263B26CB275B27ED287F291229A62A3B2AD02B672BFE2C962D2F2DC82E632EFE),
    .INIT_28(256'h1DB91E3A1EBD1F401FC4204920CF215521DD226622EF237A24052491251E25AC),
    .INIT_29(256'h162A169B170E178117F6186B18E2195919D21A4B1AC61B411BBE1C3B1CBA1D39),
    .INIT_2A(256'h0F9E0FFF106110C41128118D11F3125A12C2132C13961401146E14DB154A15B9),
    .INIT_2B(256'h0A280A770AC70B190B6B0BBF0C130C690CC00D180D710DCB0E260E830EE00F3F),
    .INIT_2C(256'h05D40611064E068D06CD070F0751079507D9081F086608AE08F80942098E09DA),
    .INIT_2D(256'h02AD02D70301032D035A038803B803E9041A044E048204B704EE0526055F0599),
    .INIT_2E(256'h00BB00D100E80100011A01350151016E018D01AD01CE01F002130238025E0285),
    .INIT_2F(256'h000200040008000C0012001A0022002C003700430050005F006F0080009200A6),
    .INIT_30(256'h008400730062005400460039002E0024001B0014000E00090005000200010001),
    .INIT_31(256'h0240021B01F801D501B4019401750157013B0120010600ED00D600BF00AA0097),
    .INIT_32(256'h053204FA04C3048E0459042603F403C3039303640337030B02E002B6028E0266),
    .INIT_33(256'h0953090808BE0876082F07E907A40760071D06DC069B065C061E05E105A6056B),
    .INIT_34(256'h0E970E3B0DDF0D850D2B0CD30C7C0C260BD10B7D0B2B0AD90A890A3A09EB099E),
    .INIT_35(256'h14F31486141913AE134312DA1271120A11A3113E10DA107710150FB40F540EF5),
    .INIT_36(256'h1C571BD91B5D1AE11A6619ED197418FC18851810179B172716B4164315D21562),
    .INIT_37(256'h24B024242398230E228421FB217320EC20661FE11F5D1EDA1E571DD61D551CD6),
    .INIT_38(256'h2DEA2D512CB82C1F2B882AF12A5C29C72933289F280D277B26EB265B25CC253E),
    .INIT_39(256'h37EF374936A435FF355C34B93417337532D53235319630F730592FBC2F202E85),
    .INIT_3A(256'h42A441F4414540963FE83F3A3E8D3DE13D353C8A3BDF3B363A8C39E4393C3895),
    .INIT_3B(256'h4DF14D384C804BC94B114A5B49A448EF483A478546D1461D456A44B844064355),
    .INIT_3C(256'h59B958FA583B577C56BD55FF5542548453C8530B524F519450D9501E4F644EAA),
    .INIT_3D(256'h65E0651B6457639362CF620B614860855FC35F005E3E5D7D5CBB5BFA5B3A5A79),
    .INIT_3E(256'h7247717F70B76FF06F296E616D9A6CD46C0D6B476A8069BA68F5682F676A66A5),
    .INIT_3F(256'h7ECF7E067D3D7C747BAC7AE37A1A7951788977C076F7762F7567749F73D6730E),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("NONE"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(0),
    .WRITE_WIDTH_B(18)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl 
       (.ADDRARDADDR({Q,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({ADDRBWRADDR,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(s00_axi_aclk),
        .CLKBWRCLK(s00_axi_aclk),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DIBDI(DIBDI),
        .DIPADIP({1'b0,1'b0}),
        .DIPBDIP({1'b0,1'b0}),
        .DOADO({DOADO,readR,\sdp_bl.ramb18_dp_bl.ram18_bl_n_8 ,\sdp_bl.ramb18_dp_bl.ram18_bl_n_9 ,\sdp_bl.ramb18_dp_bl.ram18_bl_n_10 ,\sdp_bl.ramb18_dp_bl.ram18_bl_n_11 ,\sdp_bl.ramb18_dp_bl.ram18_bl_n_12 ,\sdp_bl.ramb18_dp_bl.ram18_bl_n_13 ,\sdp_bl.ramb18_dp_bl.ram18_bl_n_14 ,\sdp_bl.ramb18_dp_bl.ram18_bl_n_15 }),
        .DOBDO(\NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOBDO_UNCONNECTED [15:0]),
        .DOPADOP(\NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOPADOP_UNCONNECTED [1:0]),
        .DOPBDOP(\NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOPBDOP_UNCONNECTED [1:0]),
        .ENARDEN(1'b1),
        .ENBWREN(WREN),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(s00_axi_aresetn),
        .RSTRAMB(s00_axi_aresetn),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({1'b0,1'b0}),
        .WEBWE({1'b1,1'b1,1'b1,1'b1}));
  LUT3 #(
    .INIT(8'hB8)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_10 
       (.I0(\slv_reg8_reg[9] [1]),
        .I1(switch),
        .I2(\i_sig_reg[9] [1]),
        .O(ADDRBWRADDR[1]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_11 
       (.I0(\slv_reg8_reg[9] [0]),
        .I1(switch),
        .I2(\i_sig_reg[9] [0]),
        .O(ADDRBWRADDR[0]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_2 
       (.I0(\slv_reg8_reg[9] [9]),
        .I1(switch),
        .I2(\i_sig_reg[9] [7]),
        .O(ADDRBWRADDR[9]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_3 
       (.I0(\slv_reg8_reg[9] [8]),
        .I1(switch),
        .I2(\i_sig_reg[9] [6]),
        .O(ADDRBWRADDR[8]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_4 
       (.I0(\slv_reg8_reg[9] [7]),
        .I1(switch),
        .I2(\i_sig_reg[9] [5]),
        .O(ADDRBWRADDR[7]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_5 
       (.I0(\slv_reg8_reg[9] [6]),
        .I1(switch),
        .I2(\i_sig_reg[9] [4]),
        .O(ADDRBWRADDR[6]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_6 
       (.I0(\slv_reg8_reg[9] [5]),
        .I1(switch),
        .I2(\i_sig_reg[9] [3]),
        .O(ADDRBWRADDR[5]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_7 
       (.I0(\slv_reg8_reg[9] [4]),
        .I1(switch),
        .I2(\i_sig_reg[4] ),
        .O(ADDRBWRADDR[4]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_8 
       (.I0(\slv_reg8_reg[9] [3]),
        .I1(switch),
        .I2(\i_sig_reg[9] [2]),
        .O(ADDRBWRADDR[3]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_9 
       (.I0(\slv_reg8_reg[9] [2]),
        .I1(switch),
        .I2(\i_sig_reg[2] ),
        .O(ADDRBWRADDR[2]));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_vga
   (Q,
    \sdp_bl.ramb18_dp_bl.ram18_bl ,
    \encoded_reg[8] ,
    \encoded_reg[9] ,
    \encoded_reg[8]_0 ,
    \encoded_reg[8]_1 ,
    \encoded_reg[8]_2 ,
    \encoded_reg[0] ,
    \encoded_reg[8]_3 ,
    \encoded_reg[9]_0 ,
    encoded0_in,
    \encoded_reg[2] ,
    \encoded_reg[0]_0 ,
    SR,
    \encoded_reg[9]_1 ,
    \encoded_reg[9]_2 ,
    \encoded_reg[9]_3 ,
    \dc_bias_reg[3] ,
    \dc_bias_reg[1] ,
    \dc_bias_reg[1]_0 ,
    s00_axi_aresetn,
    \dc_bias_reg[3]_0 ,
    \trigger_time_reg[4] ,
    \trigger_time_reg[8] ,
    \trigger_time_reg[3] ,
    \trigger_volt_reg[9] ,
    \trigger_time_reg[9] ,
    \trigger_volt_reg[2] ,
    DOADO,
    \sdp_bl.ramb18_dp_bl.ram18_bl_0 ,
    \sdp_bl.ramb18_dp_bl.ram18_bl_1 ,
    switch,
    \dc_bias_reg[1]_1 ,
    \trigger_time_reg[7] ,
    \trigger_time_reg[5] ,
    \trigger_time_reg[6] ,
    \trigger_time_reg[4]_0 ,
    \trigger_time_reg[6]_0 ,
    \trigger_time_reg[7]_0 ,
    \trigger_time_reg[5]_0 ,
    \trigger_time_reg[1] ,
    \trigger_time_reg[1]_0 ,
    \trigger_time_reg[2] ,
    \trigger_volt_reg[4] ,
    \trigger_volt_reg[5] ,
    \trigger_volt_reg[4]_0 ,
    \trigger_volt_reg[5]_0 ,
    \trigger_volt_reg[1] ,
    \trigger_volt_reg[1]_0 ,
    S,
    \sdp_bl.ramb18_dp_bl.ram18_bl_2 ,
    \trigger_time_reg[6]_1 ,
    \trigger_time_reg[5]_1 ,
    \dc_bias_reg[3]_1 ,
    \dc_bias_reg[3]_2 ,
    s00_axi_aresetn_0,
    CLK);
  output [5:0]Q;
  output [9:0]\sdp_bl.ramb18_dp_bl.ram18_bl ;
  output \encoded_reg[8] ;
  output \encoded_reg[9] ;
  output \encoded_reg[8]_0 ;
  output \encoded_reg[8]_1 ;
  output \encoded_reg[8]_2 ;
  output \encoded_reg[0] ;
  output \encoded_reg[8]_3 ;
  output \encoded_reg[9]_0 ;
  output [0:0]encoded0_in;
  output \encoded_reg[2] ;
  output \encoded_reg[0]_0 ;
  output [0:0]SR;
  output \encoded_reg[9]_1 ;
  output \encoded_reg[9]_2 ;
  output \encoded_reg[9]_3 ;
  input [1:0]\dc_bias_reg[3] ;
  input \dc_bias_reg[1] ;
  input \dc_bias_reg[1]_0 ;
  input s00_axi_aresetn;
  input [0:0]\dc_bias_reg[3]_0 ;
  input \trigger_time_reg[4] ;
  input [3:0]\trigger_time_reg[8] ;
  input \trigger_time_reg[3] ;
  input [9:0]\trigger_volt_reg[9] ;
  input \trigger_time_reg[9] ;
  input \trigger_volt_reg[2] ;
  input [4:0]DOADO;
  input \sdp_bl.ramb18_dp_bl.ram18_bl_0 ;
  input [1:0]\sdp_bl.ramb18_dp_bl.ram18_bl_1 ;
  input [1:0]switch;
  input \dc_bias_reg[1]_1 ;
  input \trigger_time_reg[7] ;
  input \trigger_time_reg[5] ;
  input \trigger_time_reg[6] ;
  input \trigger_time_reg[4]_0 ;
  input \trigger_time_reg[6]_0 ;
  input \trigger_time_reg[7]_0 ;
  input \trigger_time_reg[5]_0 ;
  input \trigger_time_reg[1] ;
  input \trigger_time_reg[1]_0 ;
  input \trigger_time_reg[2] ;
  input \trigger_volt_reg[4] ;
  input \trigger_volt_reg[5] ;
  input \trigger_volt_reg[4]_0 ;
  input \trigger_volt_reg[5]_0 ;
  input \trigger_volt_reg[1] ;
  input \trigger_volt_reg[1]_0 ;
  input [0:0]S;
  input [1:0]\sdp_bl.ramb18_dp_bl.ram18_bl_2 ;
  input \trigger_time_reg[6]_1 ;
  input \trigger_time_reg[5]_1 ;
  input \dc_bias_reg[3]_1 ;
  input [0:0]\dc_bias_reg[3]_2 ;
  input s00_axi_aresetn_0;
  input CLK;

  wire CLK;
  wire [4:0]DOADO;
  wire [5:0]Q;
  wire R10;
  wire R10_carry_i_1_n_0;
  wire R10_carry_i_2_n_0;
  wire R10_carry_i_3_n_0;
  wire R10_carry_i_4_n_0;
  wire R13;
  wire R13_carry_i_1_n_0;
  wire R13_carry_i_2_n_0;
  wire R13_carry_i_3_n_0;
  wire R13_carry_i_4_n_0;
  wire R14;
  wire R14_carry_i_1_n_0;
  wire R14_carry_i_2_n_0;
  wire R14_carry_i_3_n_0;
  wire R14_carry_i_4_n_0;
  wire R14_carry_i_6_n_0;
  wire R15;
  wire R15_carry_i_1_n_0;
  wire R15_carry_i_2_n_0;
  wire R15_carry_i_3_n_0;
  wire R15_carry_i_4_n_0;
  wire R15_carry_i_6_n_0;
  wire R15_carry_i_7_n_0;
  wire R15_carry_i_8_n_0;
  wire R15_carry_i_9_n_0;
  wire R16;
  wire R16_carry_i_10_n_0;
  wire R16_carry_i_11_n_0;
  wire R16_carry_i_1_n_0;
  wire R16_carry_i_2_n_0;
  wire R16_carry_i_3_n_0;
  wire R16_carry_i_4_n_0;
  wire R16_carry_i_7_n_0;
  wire R16_carry_i_8_n_0;
  wire R17;
  wire R17_carry_i_1_n_0;
  wire R17_carry_i_2_n_0;
  wire R17_carry_i_3_n_0;
  wire R17_carry_i_4_n_0;
  wire R22__29_carry_i_4_n_0;
  wire R6;
  wire R6_carry_i_1_n_0;
  wire R6_carry_i_2_n_0;
  wire R6_carry_i_3_n_0;
  wire R6_carry_i_4_n_0;
  wire R7;
  wire R7_carry_i_1_n_0;
  wire R7_carry_i_2_n_0;
  wire R7_carry_i_3_n_0;
  wire R7_carry_i_4_n_0;
  wire R7_carry_i_5_n_0;
  wire R7_carry_i_7_n_0;
  wire R8;
  wire R8_carry_i_1_n_0;
  wire R8_carry_i_2_n_0;
  wire R8_carry_i_3_n_0;
  wire R8_carry_i_4_n_0;
  wire R8_carry_i_6_n_0;
  wire R8_carry_i_7_n_0;
  wire R8_carry_i_8_n_0;
  wire R9;
  wire R9_carry_i_1_n_0;
  wire R9_carry_i_2_n_0;
  wire R9_carry_i_3_n_0;
  wire R9_carry_i_4_n_0;
  wire R9_carry_i_6_n_0;
  wire R9_carry_i_7_n_0;
  wire R9_carry_i_8_n_0;
  wire R9_carry_i_9_n_0;
  wire [0:0]S;
  wire [0:0]SR;
  wire ch13;
  wire ch23;
  wire \dc_bias[3]_i_10_n_0 ;
  wire \dc_bias[3]_i_13_n_0 ;
  wire \dc_bias[3]_i_14_n_0 ;
  wire \dc_bias[3]_i_15_n_0 ;
  wire \dc_bias[3]_i_16_n_0 ;
  wire \dc_bias[3]_i_17_n_0 ;
  wire \dc_bias[3]_i_18_n_0 ;
  wire \dc_bias[3]_i_19_n_0 ;
  wire \dc_bias[3]_i_20_n_0 ;
  wire \dc_bias[3]_i_21_n_0 ;
  wire \dc_bias[3]_i_22_n_0 ;
  wire \dc_bias[3]_i_25_n_0 ;
  wire \dc_bias[3]_i_26_n_0 ;
  wire \dc_bias[3]_i_27_n_0 ;
  wire \dc_bias[3]_i_28_n_0 ;
  wire \dc_bias[3]_i_29_n_0 ;
  wire \dc_bias[3]_i_30_n_0 ;
  wire \dc_bias[3]_i_31_n_0 ;
  wire \dc_bias[3]_i_32_n_0 ;
  wire \dc_bias[3]_i_33_n_0 ;
  wire \dc_bias[3]_i_34_n_0 ;
  wire \dc_bias[3]_i_35_n_0 ;
  wire \dc_bias[3]_i_36_n_0 ;
  wire \dc_bias[3]_i_37_n_0 ;
  wire \dc_bias[3]_i_38_n_0 ;
  wire \dc_bias[3]_i_39_n_0 ;
  wire \dc_bias[3]_i_3__0_n_0 ;
  wire \dc_bias[3]_i_40_n_0 ;
  wire \dc_bias[3]_i_41_n_0 ;
  wire \dc_bias[3]_i_42_n_0 ;
  wire \dc_bias[3]_i_43_n_0 ;
  wire \dc_bias[3]_i_44_n_0 ;
  wire \dc_bias[3]_i_45_n_0 ;
  wire \dc_bias[3]_i_46_n_0 ;
  wire \dc_bias[3]_i_47_n_0 ;
  wire \dc_bias[3]_i_48_n_0 ;
  wire \dc_bias[3]_i_49_n_0 ;
  wire \dc_bias[3]_i_50_n_0 ;
  wire \dc_bias[3]_i_6_n_0 ;
  wire \dc_bias[3]_i_7_n_0 ;
  wire \dc_bias[3]_i_8_n_0 ;
  wire \dc_bias[3]_i_9_n_0 ;
  wire \dc_bias_reg[1] ;
  wire \dc_bias_reg[1]_0 ;
  wire \dc_bias_reg[1]_1 ;
  wire [1:0]\dc_bias_reg[3] ;
  wire [0:0]\dc_bias_reg[3]_0 ;
  wire \dc_bias_reg[3]_1 ;
  wire [0:0]\dc_bias_reg[3]_2 ;
  wire [0:0]encoded0_in;
  wire \encoded[8]_i_10_n_0 ;
  wire \encoded[8]_i_12_n_0 ;
  wire \encoded[8]_i_13_n_0 ;
  wire \encoded[8]_i_14_n_0 ;
  wire \encoded[8]_i_18_n_0 ;
  wire \encoded[8]_i_19_n_0 ;
  wire \encoded[8]_i_20_n_0 ;
  wire \encoded[8]_i_21_n_0 ;
  wire \encoded[8]_i_22_n_0 ;
  wire \encoded[8]_i_23_n_0 ;
  wire \encoded[8]_i_3__0_n_0 ;
  wire \encoded[8]_i_3_n_0 ;
  wire \encoded[8]_i_5__0_n_0 ;
  wire \encoded[8]_i_5_n_0 ;
  wire \encoded[8]_i_6_n_0 ;
  wire \encoded[8]_i_7__0_n_0 ;
  wire \encoded[8]_i_8__0_n_0 ;
  wire \encoded[8]_i_9_n_0 ;
  wire \encoded[9]_i_2_n_0 ;
  wire \encoded[9]_i_4_n_0 ;
  wire \encoded_reg[0] ;
  wire \encoded_reg[0]_0 ;
  wire \encoded_reg[2] ;
  wire \encoded_reg[8] ;
  wire \encoded_reg[8]_0 ;
  wire \encoded_reg[8]_1 ;
  wire \encoded_reg[8]_2 ;
  wire \encoded_reg[8]_3 ;
  wire \encoded_reg[8]_i_3_n_1 ;
  wire \encoded_reg[8]_i_3_n_2 ;
  wire \encoded_reg[8]_i_3_n_3 ;
  wire \encoded_reg[8]_i_4_n_1 ;
  wire \encoded_reg[8]_i_4_n_2 ;
  wire \encoded_reg[8]_i_4_n_3 ;
  wire \encoded_reg[9] ;
  wire \encoded_reg[9]_0 ;
  wire \encoded_reg[9]_1 ;
  wire \encoded_reg[9]_2 ;
  wire \encoded_reg[9]_3 ;
  wire [9:0]p_0_in__2;
  wire [9:0]plusOp;
  wire \processCol[3]_i_2_n_0 ;
  wire \processCol[7]_i_2_n_0 ;
  wire \processCol[7]_i_3_n_0 ;
  wire \processCol[9]_i_1_n_0 ;
  wire \processCol[9]_i_3_n_0 ;
  wire processRow0;
  wire \processRow[6]_i_2_n_0 ;
  wire \processRow[9]_i_1_n_0 ;
  wire \processRow[9]_i_4_n_0 ;
  wire \processRow[9]_i_5_n_0 ;
  wire \processRow[9]_i_6_n_0 ;
  wire \processRow[9]_i_7_n_0 ;
  wire [9:6]row;
  wire s00_axi_aresetn;
  wire s00_axi_aresetn_0;
  wire [9:0]\sdp_bl.ramb18_dp_bl.ram18_bl ;
  wire \sdp_bl.ramb18_dp_bl.ram18_bl_0 ;
  wire [1:0]\sdp_bl.ramb18_dp_bl.ram18_bl_1 ;
  wire [1:0]\sdp_bl.ramb18_dp_bl.ram18_bl_2 ;
  wire [1:0]switch;
  wire \trigger_time_reg[1] ;
  wire \trigger_time_reg[1]_0 ;
  wire \trigger_time_reg[2] ;
  wire \trigger_time_reg[3] ;
  wire \trigger_time_reg[4] ;
  wire \trigger_time_reg[4]_0 ;
  wire \trigger_time_reg[5] ;
  wire \trigger_time_reg[5]_0 ;
  wire \trigger_time_reg[5]_1 ;
  wire \trigger_time_reg[6] ;
  wire \trigger_time_reg[6]_0 ;
  wire \trigger_time_reg[6]_1 ;
  wire \trigger_time_reg[7] ;
  wire \trigger_time_reg[7]_0 ;
  wire [3:0]\trigger_time_reg[8] ;
  wire \trigger_time_reg[9] ;
  wire \trigger_volt_reg[1] ;
  wire \trigger_volt_reg[1]_0 ;
  wire \trigger_volt_reg[2] ;
  wire \trigger_volt_reg[4] ;
  wire \trigger_volt_reg[4]_0 ;
  wire \trigger_volt_reg[5] ;
  wire \trigger_volt_reg[5]_0 ;
  wire [9:0]\trigger_volt_reg[9] ;
  wire [3:0]\NLW_encoded_reg[8]_i_3_O_UNCONNECTED ;
  wire [3:0]\NLW_encoded_reg[8]_i_4_O_UNCONNECTED ;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Scopeface Face
       (.CO(R17),
        .Q({row,Q}),
        .S({R17_carry_i_1_n_0,R17_carry_i_2_n_0,R17_carry_i_3_n_0,R17_carry_i_4_n_0}),
        .\dc_bias_reg[1] (\dc_bias_reg[1] ),
        .\dc_bias_reg[1]_0 (\dc_bias_reg[1]_0 ),
        .\dc_bias_reg[1]_1 (\dc_bias_reg[1]_1 ),
        .\dc_bias_reg[3] (\dc_bias_reg[3] ),
        .\dc_bias_reg[3]_0 (\dc_bias_reg[3]_1 ),
        .\dc_bias_reg[3]_1 (\dc_bias_reg[3]_2 ),
        .\dc_bias_reg[3]_2 (\dc_bias_reg[3]_0 ),
        .encoded0_in(encoded0_in),
        .\encoded_reg[8] (R16),
        .\encoded_reg[8]_0 (R15),
        .\encoded_reg[8]_1 (R14),
        .\encoded_reg[8]_10 (\encoded_reg[8]_1 ),
        .\encoded_reg[8]_11 (\encoded_reg[8]_3 ),
        .\encoded_reg[8]_2 (R13),
        .\encoded_reg[8]_3 (R10),
        .\encoded_reg[8]_4 (R9),
        .\encoded_reg[8]_5 (R8),
        .\encoded_reg[8]_6 (R7),
        .\encoded_reg[8]_7 (R6),
        .\encoded_reg[8]_8 (\encoded_reg[8] ),
        .\encoded_reg[8]_9 (\encoded_reg[8]_0 ),
        .\encoded_reg[9] (\encoded_reg[9]_0 ),
        .\encoded_reg[9]_0 (\encoded_reg[9]_1 ),
        .\encoded_reg[9]_1 (\encoded_reg[9]_2 ),
        .\encoded_reg[9]_2 (\encoded_reg[9]_3 ),
        .\processCol_reg[0] (\encoded_reg[8]_2 ),
        .\processCol_reg[1] (\dc_bias[3]_i_14_n_0 ),
        .\processCol_reg[2] (\dc_bias[3]_i_13_n_0 ),
        .\processCol_reg[6] (\encoded_reg[9] ),
        .\processCol_reg[8] (\encoded[8]_i_3_n_0 ),
        .\processCol_reg[9] (\sdp_bl.ramb18_dp_bl.ram18_bl ),
        .\processCol_reg[9]_0 (\encoded[8]_i_14_n_0 ),
        .\processRow_reg[0] (R22__29_carry_i_4_n_0),
        .\processRow_reg[2] (\dc_bias[3]_i_10_n_0 ),
        .\processRow_reg[4] (\encoded[8]_i_3__0_n_0 ),
        .\processRow_reg[4]_0 (\encoded[9]_i_2_n_0 ),
        .\processRow_reg[7] (\encoded[8]_i_5__0_n_0 ),
        .\processRow_reg[7]_0 (\dc_bias[3]_i_3__0_n_0 ),
        .\processRow_reg[7]_1 (\encoded[8]_i_13_n_0 ),
        .\processRow_reg[9] (ch23),
        .\processRow_reg[9]_0 (ch13),
        .\processRow_reg[9]_1 (\dc_bias[3]_i_25_n_0 ),
        .switch(switch),
        .\trigger_time_reg[0] ({R16_carry_i_1_n_0,R16_carry_i_2_n_0,R16_carry_i_3_n_0,R16_carry_i_4_n_0}),
        .\trigger_time_reg[7] ({R14_carry_i_1_n_0,R14_carry_i_2_n_0,R14_carry_i_3_n_0,R14_carry_i_4_n_0}),
        .\trigger_time_reg[8] ({R15_carry_i_1_n_0,R15_carry_i_2_n_0,R15_carry_i_3_n_0,R15_carry_i_4_n_0}),
        .\trigger_time_reg[8]_0 ({R13_carry_i_1_n_0,R13_carry_i_2_n_0,R13_carry_i_3_n_0,R13_carry_i_4_n_0}),
        .\trigger_volt_reg[7] ({R7_carry_i_1_n_0,R7_carry_i_2_n_0,R7_carry_i_3_n_0,R7_carry_i_4_n_0}),
        .\trigger_volt_reg[7]_0 ({R6_carry_i_1_n_0,R6_carry_i_2_n_0,R6_carry_i_3_n_0,R6_carry_i_4_n_0}),
        .\trigger_volt_reg[8] ({R8_carry_i_1_n_0,R8_carry_i_2_n_0,R8_carry_i_3_n_0,R8_carry_i_4_n_0}),
        .\trigger_volt_reg[9] ({R10_carry_i_1_n_0,R10_carry_i_2_n_0,R10_carry_i_3_n_0,R10_carry_i_4_n_0}),
        .\trigger_volt_reg[9]_0 ({R9_carry_i_1_n_0,R9_carry_i_2_n_0,R9_carry_i_3_n_0,R9_carry_i_4_n_0}));
  LUT2 #(
    .INIT(4'h9)) 
    R10_carry_i_1
       (.I0(\trigger_volt_reg[9] [9]),
        .I1(row[9]),
        .O(R10_carry_i_1_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    R10_carry_i_2
       (.I0(row[6]),
        .I1(\trigger_volt_reg[9] [6]),
        .I2(\trigger_volt_reg[9] [7]),
        .I3(row[7]),
        .I4(\trigger_volt_reg[9] [8]),
        .I5(row[8]),
        .O(R10_carry_i_2_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    R10_carry_i_3
       (.I0(\trigger_volt_reg[9] [4]),
        .I1(Q[4]),
        .I2(\trigger_volt_reg[9] [5]),
        .I3(Q[5]),
        .I4(Q[3]),
        .I5(\trigger_volt_reg[9] [3]),
        .O(R10_carry_i_3_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    R10_carry_i_4
       (.I0(\trigger_volt_reg[9] [1]),
        .I1(Q[1]),
        .I2(\trigger_volt_reg[9] [0]),
        .I3(Q[0]),
        .I4(Q[2]),
        .I5(\trigger_volt_reg[9] [2]),
        .O(R10_carry_i_4_n_0));
  LUT4 #(
    .INIT(16'hD22D)) 
    R13_carry_i_1
       (.I0(\trigger_time_reg[5]_1 ),
        .I1(\trigger_time_reg[8] [3]),
        .I2(\trigger_time_reg[9] ),
        .I3(\sdp_bl.ramb18_dp_bl.ram18_bl [9]),
        .O(R13_carry_i_1_n_0));
  LUT6 #(
    .INIT(64'h8241410800000020)) 
    R13_carry_i_2
       (.I0(\sdp_bl.ramb18_dp_bl.ram18_bl [6]),
        .I1(\trigger_time_reg[7] ),
        .I2(\sdp_bl.ramb18_dp_bl.ram18_bl [7]),
        .I3(\trigger_time_reg[5] ),
        .I4(\trigger_time_reg[6] ),
        .I5(R16_carry_i_7_n_0),
        .O(R13_carry_i_2_n_0));
  LUT6 #(
    .INIT(64'h0882100020000882)) 
    R13_carry_i_3
       (.I0(R14_carry_i_6_n_0),
        .I1(\trigger_time_reg[3] ),
        .I2(\sdp_bl.ramb18_dp_bl.ram18_bl [3]),
        .I3(\trigger_time_reg[1] ),
        .I4(\trigger_time_reg[4] ),
        .I5(\sdp_bl.ramb18_dp_bl.ram18_bl [4]),
        .O(R13_carry_i_3_n_0));
  LUT6 #(
    .INIT(64'h0900000900909000)) 
    R13_carry_i_4
       (.I0(\sdp_bl.ramb18_dp_bl.ram18_bl [0]),
        .I1(\trigger_time_reg[8] [0]),
        .I2(\sdp_bl.ramb18_dp_bl.ram18_bl [1]),
        .I3(\sdp_bl.ramb18_dp_bl.ram18_bl [2]),
        .I4(\trigger_time_reg[2] ),
        .I5(\trigger_time_reg[8] [1]),
        .O(R13_carry_i_4_n_0));
  LUT6 #(
    .INIT(64'h7FFF800080007FFF)) 
    R14_carry_i_1
       (.I0(\trigger_time_reg[7] ),
        .I1(\trigger_time_reg[4]_0 ),
        .I2(\trigger_time_reg[6] ),
        .I3(\trigger_time_reg[8] [3]),
        .I4(\trigger_time_reg[9] ),
        .I5(\sdp_bl.ramb18_dp_bl.ram18_bl [9]),
        .O(R14_carry_i_1_n_0));
  LUT6 #(
    .INIT(64'h0480804010080804)) 
    R14_carry_i_2
       (.I0(\sdp_bl.ramb18_dp_bl.ram18_bl [6]),
        .I1(R16_carry_i_7_n_0),
        .I2(\trigger_time_reg[7] ),
        .I3(\trigger_time_reg[4]_0 ),
        .I4(\trigger_time_reg[6] ),
        .I5(\sdp_bl.ramb18_dp_bl.ram18_bl [7]),
        .O(R14_carry_i_2_n_0));
  LUT6 #(
    .INIT(64'h0090420090000090)) 
    R14_carry_i_3
       (.I0(\trigger_time_reg[4] ),
        .I1(\sdp_bl.ramb18_dp_bl.ram18_bl [4]),
        .I2(R14_carry_i_6_n_0),
        .I3(\trigger_time_reg[1]_0 ),
        .I4(\sdp_bl.ramb18_dp_bl.ram18_bl [3]),
        .I5(\trigger_time_reg[3] ),
        .O(R14_carry_i_3_n_0));
  LUT6 #(
    .INIT(64'h0009090090000090)) 
    R14_carry_i_4
       (.I0(\sdp_bl.ramb18_dp_bl.ram18_bl [0]),
        .I1(\trigger_time_reg[8] [0]),
        .I2(\sdp_bl.ramb18_dp_bl.ram18_bl [1]),
        .I3(\sdp_bl.ramb18_dp_bl.ram18_bl [2]),
        .I4(\trigger_time_reg[2] ),
        .I5(\trigger_time_reg[8] [1]),
        .O(R14_carry_i_4_n_0));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT2 #(
    .INIT(4'h9)) 
    R14_carry_i_6
       (.I0(\sdp_bl.ramb18_dp_bl.ram18_bl [5]),
        .I1(\trigger_time_reg[8] [2]),
        .O(R14_carry_i_6_n_0));
  LUT5 #(
    .INIT(32'hFB0404FB)) 
    R15_carry_i_1
       (.I0(\trigger_time_reg[8] [3]),
        .I1(\trigger_time_reg[5]_1 ),
        .I2(\trigger_time_reg[8] [0]),
        .I3(\trigger_time_reg[9] ),
        .I4(\sdp_bl.ramb18_dp_bl.ram18_bl [9]),
        .O(R15_carry_i_1_n_0));
  LUT4 #(
    .INIT(16'hC8CA)) 
    R15_carry_i_2
       (.I0(R15_carry_i_6_n_0),
        .I1(R15_carry_i_7_n_0),
        .I2(\trigger_time_reg[8] [0]),
        .I3(R15_carry_i_8_n_0),
        .O(R15_carry_i_2_n_0));
  LUT6 #(
    .INIT(64'h00000000FE0101FE)) 
    R15_carry_i_3
       (.I0(\trigger_time_reg[2] ),
        .I1(\trigger_time_reg[8] [1]),
        .I2(\trigger_time_reg[8] [0]),
        .I3(\sdp_bl.ramb18_dp_bl.ram18_bl [3]),
        .I4(\trigger_time_reg[3] ),
        .I5(R15_carry_i_9_n_0),
        .O(R15_carry_i_3_n_0));
  LUT6 #(
    .INIT(64'h2140000800082140)) 
    R15_carry_i_4
       (.I0(\sdp_bl.ramb18_dp_bl.ram18_bl [1]),
        .I1(\sdp_bl.ramb18_dp_bl.ram18_bl [0]),
        .I2(\trigger_time_reg[8] [1]),
        .I3(\trigger_time_reg[8] [0]),
        .I4(\trigger_time_reg[2] ),
        .I5(\sdp_bl.ramb18_dp_bl.ram18_bl [2]),
        .O(R15_carry_i_4_n_0));
  LUT6 #(
    .INIT(64'hCCCC33330130CCCC)) 
    R15_carry_i_6
       (.I0(R16_carry_i_7_n_0),
        .I1(\trigger_time_reg[6] ),
        .I2(\trigger_time_reg[7] ),
        .I3(\sdp_bl.ramb18_dp_bl.ram18_bl [7]),
        .I4(\sdp_bl.ramb18_dp_bl.ram18_bl [6]),
        .I5(\trigger_time_reg[5] ),
        .O(R15_carry_i_6_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    R15_carry_i_7
       (.I0(\trigger_time_reg[8] [3]),
        .I1(\sdp_bl.ramb18_dp_bl.ram18_bl [8]),
        .I2(\sdp_bl.ramb18_dp_bl.ram18_bl [6]),
        .I3(\trigger_time_reg[6] ),
        .I4(\sdp_bl.ramb18_dp_bl.ram18_bl [7]),
        .I5(\trigger_time_reg[7] ),
        .O(R15_carry_i_7_n_0));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT5 #(
    .INIT(32'h666F0F06)) 
    R15_carry_i_8
       (.I0(\sdp_bl.ramb18_dp_bl.ram18_bl [8]),
        .I1(\trigger_time_reg[8] [3]),
        .I2(\trigger_time_reg[6]_0 ),
        .I3(\sdp_bl.ramb18_dp_bl.ram18_bl [7]),
        .I4(\trigger_time_reg[7] ),
        .O(R15_carry_i_8_n_0));
  LUT6 #(
    .INIT(64'h77777E77DDDDD7DD)) 
    R15_carry_i_9
       (.I0(R14_carry_i_6_n_0),
        .I1(\trigger_time_reg[4] ),
        .I2(\trigger_time_reg[3] ),
        .I3(\trigger_time_reg[1] ),
        .I4(\trigger_time_reg[8] [0]),
        .I5(\sdp_bl.ramb18_dp_bl.ram18_bl [4]),
        .O(R15_carry_i_9_n_0));
  LUT5 #(
    .INIT(32'h7F80807F)) 
    R16_carry_i_1
       (.I0(\trigger_time_reg[8] [0]),
        .I1(\trigger_time_reg[6]_1 ),
        .I2(\trigger_time_reg[8] [3]),
        .I3(\trigger_time_reg[9] ),
        .I4(\sdp_bl.ramb18_dp_bl.ram18_bl [9]),
        .O(R16_carry_i_1_n_0));
  LUT6 #(
    .INIT(64'h0090420090000090)) 
    R16_carry_i_10
       (.I0(\trigger_time_reg[4] ),
        .I1(\sdp_bl.ramb18_dp_bl.ram18_bl [4]),
        .I2(R14_carry_i_6_n_0),
        .I3(\trigger_time_reg[1]_0 ),
        .I4(\sdp_bl.ramb18_dp_bl.ram18_bl [3]),
        .I5(\trigger_time_reg[3] ),
        .O(R16_carry_i_10_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    R16_carry_i_11
       (.I0(\sdp_bl.ramb18_dp_bl.ram18_bl [4]),
        .I1(\trigger_time_reg[4] ),
        .I2(\sdp_bl.ramb18_dp_bl.ram18_bl [5]),
        .I3(\trigger_time_reg[8] [2]),
        .I4(\trigger_time_reg[3] ),
        .I5(\sdp_bl.ramb18_dp_bl.ram18_bl [3]),
        .O(R16_carry_i_11_n_0));
  LUT6 #(
    .INIT(64'h0006060060000006)) 
    R16_carry_i_2
       (.I0(\trigger_time_reg[7]_0 ),
        .I1(R16_carry_i_7_n_0),
        .I2(R16_carry_i_8_n_0),
        .I3(\trigger_time_reg[6] ),
        .I4(\trigger_time_reg[5]_0 ),
        .I5(\sdp_bl.ramb18_dp_bl.ram18_bl [6]),
        .O(R16_carry_i_2_n_0));
  LUT3 #(
    .INIT(8'hB8)) 
    R16_carry_i_3
       (.I0(R16_carry_i_10_n_0),
        .I1(\trigger_time_reg[8] [0]),
        .I2(R16_carry_i_11_n_0),
        .O(R16_carry_i_3_n_0));
  LUT6 #(
    .INIT(64'h0810100824000024)) 
    R16_carry_i_4
       (.I0(\sdp_bl.ramb18_dp_bl.ram18_bl [1]),
        .I1(\sdp_bl.ramb18_dp_bl.ram18_bl [0]),
        .I2(\trigger_time_reg[8] [0]),
        .I3(\sdp_bl.ramb18_dp_bl.ram18_bl [2]),
        .I4(\trigger_time_reg[2] ),
        .I5(\trigger_time_reg[8] [1]),
        .O(R16_carry_i_4_n_0));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT2 #(
    .INIT(4'h9)) 
    R16_carry_i_7
       (.I0(\sdp_bl.ramb18_dp_bl.ram18_bl [8]),
        .I1(\trigger_time_reg[8] [3]),
        .O(R16_carry_i_7_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    R16_carry_i_8
       (.I0(\sdp_bl.ramb18_dp_bl.ram18_bl [7]),
        .I1(\trigger_time_reg[7] ),
        .O(R16_carry_i_8_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    R17_carry_i_1
       (.I0(\trigger_time_reg[9] ),
        .I1(\sdp_bl.ramb18_dp_bl.ram18_bl [9]),
        .O(R17_carry_i_1_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    R17_carry_i_2
       (.I0(\trigger_time_reg[8] [3]),
        .I1(\sdp_bl.ramb18_dp_bl.ram18_bl [8]),
        .I2(\sdp_bl.ramb18_dp_bl.ram18_bl [6]),
        .I3(\trigger_time_reg[6] ),
        .I4(\sdp_bl.ramb18_dp_bl.ram18_bl [7]),
        .I5(\trigger_time_reg[7] ),
        .O(R17_carry_i_2_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    R17_carry_i_3
       (.I0(\sdp_bl.ramb18_dp_bl.ram18_bl [4]),
        .I1(\trigger_time_reg[4] ),
        .I2(\sdp_bl.ramb18_dp_bl.ram18_bl [5]),
        .I3(\trigger_time_reg[8] [2]),
        .I4(\trigger_time_reg[3] ),
        .I5(\sdp_bl.ramb18_dp_bl.ram18_bl [3]),
        .O(R17_carry_i_3_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    R17_carry_i_4
       (.I0(\sdp_bl.ramb18_dp_bl.ram18_bl [0]),
        .I1(\trigger_time_reg[8] [0]),
        .I2(\trigger_time_reg[2] ),
        .I3(\sdp_bl.ramb18_dp_bl.ram18_bl [2]),
        .I4(\trigger_time_reg[8] [1]),
        .I5(\sdp_bl.ramb18_dp_bl.ram18_bl [1]),
        .O(R17_carry_i_4_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    R22__29_carry_i_4
       (.I0(Q[0]),
        .O(R22__29_carry_i_4_n_0));
  LUT5 #(
    .INIT(32'hFD0202FD)) 
    R6_carry_i_1
       (.I0(\trigger_volt_reg[5]_0 ),
        .I1(\trigger_volt_reg[9] [7]),
        .I2(\trigger_volt_reg[9] [8]),
        .I3(\trigger_volt_reg[9] [9]),
        .I4(row[9]),
        .O(R6_carry_i_1_n_0));
  LUT6 #(
    .INIT(64'h0041200800820041)) 
    R6_carry_i_2
       (.I0(row[6]),
        .I1(\trigger_volt_reg[9] [7]),
        .I2(row[7]),
        .I3(R7_carry_i_5_n_0),
        .I4(\trigger_volt_reg[9] [6]),
        .I5(\trigger_volt_reg[4] ),
        .O(R6_carry_i_2_n_0));
  LUT6 #(
    .INIT(64'h2802008000402802)) 
    R6_carry_i_3
       (.I0(R7_carry_i_7_n_0),
        .I1(\trigger_volt_reg[1] ),
        .I2(Q[3]),
        .I3(\trigger_volt_reg[9] [3]),
        .I4(Q[4]),
        .I5(\trigger_volt_reg[9] [4]),
        .O(R6_carry_i_3_n_0));
  LUT6 #(
    .INIT(64'h0900000900909000)) 
    R6_carry_i_4
       (.I0(\trigger_volt_reg[9] [0]),
        .I1(Q[0]),
        .I2(Q[1]),
        .I3(\trigger_volt_reg[9] [2]),
        .I4(Q[2]),
        .I5(\trigger_volt_reg[9] [1]),
        .O(R6_carry_i_4_n_0));
  LUT5 #(
    .INIT(32'h7F80807F)) 
    R7_carry_i_1
       (.I0(\trigger_volt_reg[9] [7]),
        .I1(\trigger_volt_reg[5] ),
        .I2(\trigger_volt_reg[9] [8]),
        .I3(\trigger_volt_reg[9] [9]),
        .I4(row[9]),
        .O(R7_carry_i_1_n_0));
  LUT6 #(
    .INIT(64'h0441100020000441)) 
    R7_carry_i_2
       (.I0(R7_carry_i_5_n_0),
        .I1(row[6]),
        .I2(\trigger_volt_reg[4]_0 ),
        .I3(\trigger_volt_reg[9] [6]),
        .I4(\trigger_volt_reg[9] [7]),
        .I5(row[7]),
        .O(R7_carry_i_2_n_0));
  LUT6 #(
    .INIT(64'h0090900024000090)) 
    R7_carry_i_3
       (.I0(Q[4]),
        .I1(\trigger_volt_reg[9] [4]),
        .I2(R7_carry_i_7_n_0),
        .I3(\trigger_volt_reg[1]_0 ),
        .I4(\trigger_volt_reg[9] [3]),
        .I5(Q[3]),
        .O(R7_carry_i_3_n_0));
  LUT6 #(
    .INIT(64'h0009090090000090)) 
    R7_carry_i_4
       (.I0(\trigger_volt_reg[9] [0]),
        .I1(Q[0]),
        .I2(Q[1]),
        .I3(\trigger_volt_reg[9] [2]),
        .I4(Q[2]),
        .I5(\trigger_volt_reg[9] [1]),
        .O(R7_carry_i_4_n_0));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT2 #(
    .INIT(4'h6)) 
    R7_carry_i_5
       (.I0(\trigger_volt_reg[9] [8]),
        .I1(row[8]),
        .O(R7_carry_i_5_n_0));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT2 #(
    .INIT(4'h9)) 
    R7_carry_i_7
       (.I0(\trigger_volt_reg[9] [5]),
        .I1(Q[5]),
        .O(R7_carry_i_7_n_0));
  LUT6 #(
    .INIT(64'hFFEF00100010FFEF)) 
    R8_carry_i_1
       (.I0(\trigger_volt_reg[9] [8]),
        .I1(\trigger_volt_reg[9] [7]),
        .I2(\trigger_volt_reg[5]_0 ),
        .I3(\trigger_volt_reg[9] [0]),
        .I4(\trigger_volt_reg[9] [9]),
        .I5(row[9]),
        .O(R8_carry_i_1_n_0));
  LUT4 #(
    .INIT(16'h00E2)) 
    R8_carry_i_2
       (.I0(R8_carry_i_6_n_0),
        .I1(\trigger_volt_reg[9] [0]),
        .I2(R9_carry_i_7_n_0),
        .I3(R8_carry_i_7_n_0),
        .O(R8_carry_i_2_n_0));
  LUT6 #(
    .INIT(64'h9000009000609000)) 
    R8_carry_i_3
       (.I0(Q[4]),
        .I1(\trigger_volt_reg[9] [4]),
        .I2(R8_carry_i_8_n_0),
        .I3(\trigger_volt_reg[9] [3]),
        .I4(Q[3]),
        .I5(\trigger_volt_reg[2] ),
        .O(R8_carry_i_3_n_0));
  LUT6 #(
    .INIT(64'h1804002000201804)) 
    R8_carry_i_4
       (.I0(Q[1]),
        .I1(\trigger_volt_reg[9] [0]),
        .I2(Q[0]),
        .I3(\trigger_volt_reg[9] [1]),
        .I4(Q[2]),
        .I5(\trigger_volt_reg[9] [2]),
        .O(R8_carry_i_4_n_0));
  LUT6 #(
    .INIT(64'h00FF3800FF0000FF)) 
    R8_carry_i_6
       (.I0(R7_carry_i_5_n_0),
        .I1(row[7]),
        .I2(\trigger_volt_reg[9] [7]),
        .I3(\trigger_volt_reg[4] ),
        .I4(\trigger_volt_reg[9] [6]),
        .I5(row[6]),
        .O(R8_carry_i_6_n_0));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT5 #(
    .INIT(32'h3C7D5514)) 
    R8_carry_i_7
       (.I0(\trigger_volt_reg[5]_0 ),
        .I1(row[8]),
        .I2(\trigger_volt_reg[9] [8]),
        .I3(row[7]),
        .I4(\trigger_volt_reg[9] [7]),
        .O(R8_carry_i_7_n_0));
  LUT6 #(
    .INIT(64'hAAAAAAAAAAAAAAA9)) 
    R8_carry_i_8
       (.I0(R7_carry_i_7_n_0),
        .I1(\trigger_volt_reg[9] [0]),
        .I2(\trigger_volt_reg[9] [3]),
        .I3(\trigger_volt_reg[9] [1]),
        .I4(\trigger_volt_reg[9] [2]),
        .I5(\trigger_volt_reg[9] [4]),
        .O(R8_carry_i_8_n_0));
  LUT6 #(
    .INIT(64'h6AAAAAAA95555555)) 
    R9_carry_i_1
       (.I0(\trigger_volt_reg[9] [9]),
        .I1(\trigger_volt_reg[9] [0]),
        .I2(\trigger_volt_reg[9] [7]),
        .I3(\trigger_volt_reg[5] ),
        .I4(\trigger_volt_reg[9] [8]),
        .I5(row[9]),
        .O(R9_carry_i_1_n_0));
  LUT6 #(
    .INIT(64'hAA82FFC3AA820000)) 
    R9_carry_i_2
       (.I0(R9_carry_i_6_n_0),
        .I1(row[7]),
        .I2(\trigger_volt_reg[9] [7]),
        .I3(\trigger_volt_reg[5] ),
        .I4(\trigger_volt_reg[9] [0]),
        .I5(R9_carry_i_7_n_0),
        .O(R9_carry_i_2_n_0));
  LUT3 #(
    .INIT(8'hB8)) 
    R9_carry_i_3
       (.I0(R9_carry_i_8_n_0),
        .I1(\trigger_volt_reg[9] [0]),
        .I2(R9_carry_i_9_n_0),
        .O(R9_carry_i_3_n_0));
  LUT6 #(
    .INIT(64'h0110800820024004)) 
    R9_carry_i_4
       (.I0(Q[1]),
        .I1(Q[0]),
        .I2(\trigger_volt_reg[9] [2]),
        .I3(Q[2]),
        .I4(\trigger_volt_reg[9] [0]),
        .I5(\trigger_volt_reg[9] [1]),
        .O(R9_carry_i_4_n_0));
  LUT6 #(
    .INIT(64'h100000000AA54AA5)) 
    R9_carry_i_6
       (.I0(row[6]),
        .I1(row[7]),
        .I2(\trigger_volt_reg[9] [6]),
        .I3(\trigger_volt_reg[4]_0 ),
        .I4(\trigger_volt_reg[9] [7]),
        .I5(R7_carry_i_5_n_0),
        .O(R9_carry_i_6_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    R9_carry_i_7
       (.I0(row[6]),
        .I1(\trigger_volt_reg[9] [6]),
        .I2(\trigger_volt_reg[9] [7]),
        .I3(row[7]),
        .I4(\trigger_volt_reg[9] [8]),
        .I5(row[8]),
        .O(R9_carry_i_7_n_0));
  LUT6 #(
    .INIT(64'h0090900024000090)) 
    R9_carry_i_8
       (.I0(Q[4]),
        .I1(\trigger_volt_reg[9] [4]),
        .I2(R7_carry_i_7_n_0),
        .I3(\trigger_volt_reg[1]_0 ),
        .I4(\trigger_volt_reg[9] [3]),
        .I5(Q[3]),
        .O(R9_carry_i_8_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    R9_carry_i_9
       (.I0(\trigger_volt_reg[9] [4]),
        .I1(Q[4]),
        .I2(\trigger_volt_reg[9] [5]),
        .I3(Q[5]),
        .I4(Q[3]),
        .I5(\trigger_volt_reg[9] [3]),
        .O(R9_carry_i_9_n_0));
  LUT6 #(
    .INIT(64'h0404100000100400)) 
    \dc_bias[3]_i_10 
       (.I0(\dc_bias[3]_i_19_n_0 ),
        .I1(Q[2]),
        .I2(\dc_bias[3]_i_20_n_0 ),
        .I3(\dc_bias[3]_i_21_n_0 ),
        .I4(Q[1]),
        .I5(\dc_bias[3]_i_22_n_0 ),
        .O(\dc_bias[3]_i_10_n_0 ));
  LUT5 #(
    .INIT(32'h5454A8FF)) 
    \dc_bias[3]_i_13 
       (.I0(\sdp_bl.ramb18_dp_bl.ram18_bl [2]),
        .I1(\dc_bias[3]_i_26_n_0 ),
        .I2(\dc_bias[3]_i_27_n_0 ),
        .I3(\dc_bias[3]_i_28_n_0 ),
        .I4(\sdp_bl.ramb18_dp_bl.ram18_bl [1]),
        .O(\dc_bias[3]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'h0007000700000007)) 
    \dc_bias[3]_i_14 
       (.I0(\dc_bias[3]_i_28_n_0 ),
        .I1(\sdp_bl.ramb18_dp_bl.ram18_bl [1]),
        .I2(\dc_bias[3]_i_29_n_0 ),
        .I3(\dc_bias[3]_i_30_n_0 ),
        .I4(\dc_bias[3]_i_31_n_0 ),
        .I5(\dc_bias[3]_i_26_n_0 ),
        .O(\dc_bias[3]_i_14_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT5 #(
    .INIT(32'h00000001)) 
    \dc_bias[3]_i_15 
       (.I0(\sdp_bl.ramb18_dp_bl.ram18_bl [5]),
        .I1(\sdp_bl.ramb18_dp_bl.ram18_bl [6]),
        .I2(\sdp_bl.ramb18_dp_bl.ram18_bl [7]),
        .I3(\sdp_bl.ramb18_dp_bl.ram18_bl [8]),
        .I4(\sdp_bl.ramb18_dp_bl.ram18_bl [9]),
        .O(\dc_bias[3]_i_15_n_0 ));
  LUT6 #(
    .INIT(64'hAAFEFEFEFEFEFEFE)) 
    \dc_bias[3]_i_16 
       (.I0(R17),
        .I1(R13),
        .I2(R14),
        .I3(Q[2]),
        .I4(Q[1]),
        .I5(Q[4]),
        .O(\dc_bias[3]_i_16_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \dc_bias[3]_i_17 
       (.I0(Q[0]),
        .I1(Q[2]),
        .I2(Q[1]),
        .O(\dc_bias[3]_i_17_n_0 ));
  LUT6 #(
    .INIT(64'hF7FFF7FFF7FFFFFF)) 
    \dc_bias[3]_i_18 
       (.I0(\dc_bias[3]_i_32_n_0 ),
        .I1(Q[4]),
        .I2(Q[3]),
        .I3(Q[2]),
        .I4(Q[1]),
        .I5(Q[0]),
        .O(\dc_bias[3]_i_18_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT4 #(
    .INIT(16'h1FFF)) 
    \dc_bias[3]_i_19 
       (.I0(\processCol[7]_i_3_n_0 ),
        .I1(\sdp_bl.ramb18_dp_bl.ram18_bl [6]),
        .I2(\dc_bias[3]_i_33_n_0 ),
        .I3(\encoded[8]_i_13_n_0 ),
        .O(\dc_bias[3]_i_19_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \dc_bias[3]_i_1__1 
       (.I0(\encoded_reg[9] ),
        .O(SR));
  LUT6 #(
    .INIT(64'h1442546276EA7EEB)) 
    \dc_bias[3]_i_20 
       (.I0(\dc_bias[3]_i_34_n_0 ),
        .I1(\dc_bias[3]_i_35_n_0 ),
        .I2(Q[3]),
        .I3(Q[4]),
        .I4(Q[2]),
        .I5(\dc_bias[3]_i_36_n_0 ),
        .O(\dc_bias[3]_i_20_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT5 #(
    .INIT(32'h936CC936)) 
    \dc_bias[3]_i_21 
       (.I0(Q[2]),
        .I1(\dc_bias[3]_i_35_n_0 ),
        .I2(Q[3]),
        .I3(Q[4]),
        .I4(\dc_bias[3]_i_36_n_0 ),
        .O(\dc_bias[3]_i_21_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT3 #(
    .INIT(8'h69)) 
    \dc_bias[3]_i_22 
       (.I0(\dc_bias[3]_i_36_n_0 ),
        .I1(Q[3]),
        .I2(Q[2]),
        .O(\dc_bias[3]_i_22_n_0 ));
  LUT6 #(
    .INIT(64'hFEAAAAAAAAAAAAAA)) 
    \dc_bias[3]_i_25 
       (.I0(row[9]),
        .I1(row[6]),
        .I2(Q[5]),
        .I3(row[8]),
        .I4(row[7]),
        .I5(\encoded[8]_i_18_n_0 ),
        .O(\dc_bias[3]_i_25_n_0 ));
  LUT6 #(
    .INIT(64'h00000000FFFFFEFF)) 
    \dc_bias[3]_i_26 
       (.I0(\dc_bias[3]_i_37_n_0 ),
        .I1(\sdp_bl.ramb18_dp_bl.ram18_bl [2]),
        .I2(\dc_bias[3]_i_38_n_0 ),
        .I3(\sdp_bl.ramb18_dp_bl.ram18_bl [3]),
        .I4(\dc_bias[3]_i_39_n_0 ),
        .I5(\dc_bias[3]_i_40_n_0 ),
        .O(\dc_bias[3]_i_26_n_0 ));
  LUT6 #(
    .INIT(64'h0008000000200000)) 
    \dc_bias[3]_i_27 
       (.I0(\sdp_bl.ramb18_dp_bl.ram18_bl [2]),
        .I1(\sdp_bl.ramb18_dp_bl.ram18_bl [4]),
        .I2(\dc_bias[3]_i_41_n_0 ),
        .I3(\sdp_bl.ramb18_dp_bl.ram18_bl [3]),
        .I4(\dc_bias[3]_i_38_n_0 ),
        .I5(\dc_bias[3]_i_42_n_0 ),
        .O(\dc_bias[3]_i_27_n_0 ));
  LUT6 #(
    .INIT(64'hCD32FB04DF20B34C)) 
    \dc_bias[3]_i_28 
       (.I0(\sdp_bl.ramb18_dp_bl.ram18_bl [2]),
        .I1(\sdp_bl.ramb18_dp_bl.ram18_bl [4]),
        .I2(\sdp_bl.ramb18_dp_bl.ram18_bl [3]),
        .I3(\dc_bias[3]_i_42_n_0 ),
        .I4(\dc_bias[3]_i_41_n_0 ),
        .I5(\dc_bias[3]_i_38_n_0 ),
        .O(\dc_bias[3]_i_28_n_0 ));
  LUT6 #(
    .INIT(64'hE81717E817E8E817)) 
    \dc_bias[3]_i_29 
       (.I0(\sdp_bl.ramb18_dp_bl.ram18_bl [2]),
        .I1(\dc_bias[3]_i_38_n_0 ),
        .I2(\sdp_bl.ramb18_dp_bl.ram18_bl [3]),
        .I3(\dc_bias[3]_i_41_n_0 ),
        .I4(\sdp_bl.ramb18_dp_bl.ram18_bl [4]),
        .I5(\sdp_bl.ramb18_dp_bl.ram18_bl [0]),
        .O(\dc_bias[3]_i_29_n_0 ));
  LUT6 #(
    .INIT(64'h00000000FE0FFFFF)) 
    \dc_bias[3]_i_3 
       (.I0(\sdp_bl.ramb18_dp_bl.ram18_bl [6]),
        .I1(\sdp_bl.ramb18_dp_bl.ram18_bl [5]),
        .I2(\sdp_bl.ramb18_dp_bl.ram18_bl [7]),
        .I3(\sdp_bl.ramb18_dp_bl.ram18_bl [8]),
        .I4(\sdp_bl.ramb18_dp_bl.ram18_bl [9]),
        .I5(\dc_bias[3]_i_6_n_0 ),
        .O(\encoded_reg[9] ));
  LUT6 #(
    .INIT(64'h8558FFFFFFFFFFFF)) 
    \dc_bias[3]_i_30 
       (.I0(\sdp_bl.ramb18_dp_bl.ram18_bl [2]),
        .I1(\sdp_bl.ramb18_dp_bl.ram18_bl [1]),
        .I2(\sdp_bl.ramb18_dp_bl.ram18_bl [3]),
        .I3(\dc_bias[3]_i_38_n_0 ),
        .I4(\dc_bias[3]_i_43_n_0 ),
        .I5(\encoded[8]_i_14_n_0 ),
        .O(\dc_bias[3]_i_30_n_0 ));
  LUT6 #(
    .INIT(64'h000F7700FF00000F)) 
    \dc_bias[3]_i_31 
       (.I0(\dc_bias[3]_i_39_n_0 ),
        .I1(\dc_bias[3]_i_37_n_0 ),
        .I2(\sdp_bl.ramb18_dp_bl.ram18_bl [1]),
        .I3(\sdp_bl.ramb18_dp_bl.ram18_bl [2]),
        .I4(\sdp_bl.ramb18_dp_bl.ram18_bl [3]),
        .I5(\dc_bias[3]_i_38_n_0 ),
        .O(\dc_bias[3]_i_31_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT5 #(
    .INIT(32'h00000001)) 
    \dc_bias[3]_i_32 
       (.I0(row[7]),
        .I1(Q[5]),
        .I2(row[6]),
        .I3(row[9]),
        .I4(row[8]),
        .O(\dc_bias[3]_i_32_n_0 ));
  LUT6 #(
    .INIT(64'h0000000B00000000)) 
    \dc_bias[3]_i_33 
       (.I0(\dc_bias[3]_i_44_n_0 ),
        .I1(\sdp_bl.ramb18_dp_bl.ram18_bl [6]),
        .I2(Q[0]),
        .I3(\sdp_bl.ramb18_dp_bl.ram18_bl [9]),
        .I4(\sdp_bl.ramb18_dp_bl.ram18_bl [7]),
        .I5(\sdp_bl.ramb18_dp_bl.ram18_bl [8]),
        .O(\dc_bias[3]_i_33_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT5 #(
    .INIT(32'h793C9EC7)) 
    \dc_bias[3]_i_34 
       (.I0(row[6]),
        .I1(row[7]),
        .I2(row[9]),
        .I3(row[8]),
        .I4(Q[5]),
        .O(\dc_bias[3]_i_34_n_0 ));
  LUT6 #(
    .INIT(64'hC68C3163CE9C73E7)) 
    \dc_bias[3]_i_35 
       (.I0(Q[5]),
        .I1(row[6]),
        .I2(row[7]),
        .I3(row[9]),
        .I4(row[8]),
        .I5(Q[4]),
        .O(\dc_bias[3]_i_35_n_0 ));
  LUT6 #(
    .INIT(64'h7C7F7C5715C101C1)) 
    \dc_bias[3]_i_36 
       (.I0(\dc_bias[3]_i_35_n_0 ),
        .I1(Q[5]),
        .I2(\dc_bias[3]_i_45_n_0 ),
        .I3(Q[4]),
        .I4(Q[3]),
        .I5(\dc_bias[3]_i_46_n_0 ),
        .O(\dc_bias[3]_i_36_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'h96)) 
    \dc_bias[3]_i_37 
       (.I0(\sdp_bl.ramb18_dp_bl.ram18_bl [4]),
        .I1(\dc_bias[3]_i_41_n_0 ),
        .I2(\sdp_bl.ramb18_dp_bl.ram18_bl [3]),
        .O(\dc_bias[3]_i_37_n_0 ));
  LUT6 #(
    .INIT(64'h04000000FFFFFFDF)) 
    \dc_bias[3]_i_38 
       (.I0(\dc_bias[3]_i_42_n_0 ),
        .I1(\sdp_bl.ramb18_dp_bl.ram18_bl [3]),
        .I2(\sdp_bl.ramb18_dp_bl.ram18_bl [4]),
        .I3(\dc_bias[3]_i_47_n_0 ),
        .I4(\dc_bias[3]_i_41_n_0 ),
        .I5(\dc_bias[3]_i_48_n_0 ),
        .O(\dc_bias[3]_i_38_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'hC936936C)) 
    \dc_bias[3]_i_39 
       (.I0(\sdp_bl.ramb18_dp_bl.ram18_bl [3]),
        .I1(\dc_bias[3]_i_49_n_0 ),
        .I2(\sdp_bl.ramb18_dp_bl.ram18_bl [4]),
        .I3(\sdp_bl.ramb18_dp_bl.ram18_bl [5]),
        .I4(\dc_bias[3]_i_41_n_0 ),
        .O(\dc_bias[3]_i_39_n_0 ));
  LUT3 #(
    .INIT(8'h80)) 
    \dc_bias[3]_i_3__0 
       (.I0(\encoded[8]_i_5__0_n_0 ),
        .I1(switch[1]),
        .I2(ch23),
        .O(\dc_bias[3]_i_3__0_n_0 ));
  LUT6 #(
    .INIT(64'hFFF2F2F2F2F2F2F2)) 
    \dc_bias[3]_i_4 
       (.I0(\dc_bias[3]_i_7_n_0 ),
        .I1(\dc_bias[3]_i_8_n_0 ),
        .I2(\dc_bias[3]_i_9_n_0 ),
        .I3(ch13),
        .I4(switch[0]),
        .I5(\encoded[8]_i_5__0_n_0 ),
        .O(\encoded_reg[8]_2 ));
  LUT6 #(
    .INIT(64'h1200EDFF0048FFB7)) 
    \dc_bias[3]_i_40 
       (.I0(\sdp_bl.ramb18_dp_bl.ram18_bl [5]),
        .I1(\sdp_bl.ramb18_dp_bl.ram18_bl [4]),
        .I2(\dc_bias[3]_i_49_n_0 ),
        .I3(\sdp_bl.ramb18_dp_bl.ram18_bl [3]),
        .I4(\dc_bias[3]_i_47_n_0 ),
        .I5(\dc_bias[3]_i_41_n_0 ),
        .O(\dc_bias[3]_i_40_n_0 ));
  LUT6 #(
    .INIT(64'h0EF038F0F0F0F0F0)) 
    \dc_bias[3]_i_41 
       (.I0(\sdp_bl.ramb18_dp_bl.ram18_bl [4]),
        .I1(\sdp_bl.ramb18_dp_bl.ram18_bl [9]),
        .I2(\sdp_bl.ramb18_dp_bl.ram18_bl [8]),
        .I3(\sdp_bl.ramb18_dp_bl.ram18_bl [6]),
        .I4(\sdp_bl.ramb18_dp_bl.ram18_bl [5]),
        .I5(\sdp_bl.ramb18_dp_bl.ram18_bl [7]),
        .O(\dc_bias[3]_i_41_n_0 ));
  LUT6 #(
    .INIT(64'h807F7F807F0080FF)) 
    \dc_bias[3]_i_42 
       (.I0(\sdp_bl.ramb18_dp_bl.ram18_bl [7]),
        .I1(\sdp_bl.ramb18_dp_bl.ram18_bl [6]),
        .I2(\sdp_bl.ramb18_dp_bl.ram18_bl [8]),
        .I3(\sdp_bl.ramb18_dp_bl.ram18_bl [9]),
        .I4(\sdp_bl.ramb18_dp_bl.ram18_bl [4]),
        .I5(\sdp_bl.ramb18_dp_bl.ram18_bl [5]),
        .O(\dc_bias[3]_i_42_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT5 #(
    .INIT(32'h00004000)) 
    \dc_bias[3]_i_43 
       (.I0(row[8]),
        .I1(Q[0]),
        .I2(Q[4]),
        .I3(Q[3]),
        .I4(\dc_bias[3]_i_50_n_0 ),
        .O(\dc_bias[3]_i_43_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000100)) 
    \dc_bias[3]_i_44 
       (.I0(\sdp_bl.ramb18_dp_bl.ram18_bl [3]),
        .I1(\sdp_bl.ramb18_dp_bl.ram18_bl [2]),
        .I2(\sdp_bl.ramb18_dp_bl.ram18_bl [1]),
        .I3(\sdp_bl.ramb18_dp_bl.ram18_bl [0]),
        .I4(\sdp_bl.ramb18_dp_bl.ram18_bl [5]),
        .I5(\sdp_bl.ramb18_dp_bl.ram18_bl [4]),
        .O(\dc_bias[3]_i_44_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT5 #(
    .INIT(32'h0BF0C2BC)) 
    \dc_bias[3]_i_45 
       (.I0(Q[5]),
        .I1(row[8]),
        .I2(row[9]),
        .I3(row[7]),
        .I4(row[6]),
        .O(\dc_bias[3]_i_45_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT5 #(
    .INIT(32'hA6DB5924)) 
    \dc_bias[3]_i_46 
       (.I0(row[8]),
        .I1(row[9]),
        .I2(row[7]),
        .I3(row[6]),
        .I4(Q[5]),
        .O(\dc_bias[3]_i_46_n_0 ));
  LUT6 #(
    .INIT(64'hB33333CC334C4CCC)) 
    \dc_bias[3]_i_47 
       (.I0(\sdp_bl.ramb18_dp_bl.ram18_bl [7]),
        .I1(\sdp_bl.ramb18_dp_bl.ram18_bl [6]),
        .I2(\sdp_bl.ramb18_dp_bl.ram18_bl [8]),
        .I3(\sdp_bl.ramb18_dp_bl.ram18_bl [9]),
        .I4(\sdp_bl.ramb18_dp_bl.ram18_bl [5]),
        .I5(\sdp_bl.ramb18_dp_bl.ram18_bl [4]),
        .O(\dc_bias[3]_i_47_n_0 ));
  LUT6 #(
    .INIT(64'hFE00C80011FF77FF)) 
    \dc_bias[3]_i_48 
       (.I0(\sdp_bl.ramb18_dp_bl.ram18_bl [4]),
        .I1(\sdp_bl.ramb18_dp_bl.ram18_bl [9]),
        .I2(\sdp_bl.ramb18_dp_bl.ram18_bl [8]),
        .I3(\sdp_bl.ramb18_dp_bl.ram18_bl [6]),
        .I4(\sdp_bl.ramb18_dp_bl.ram18_bl [5]),
        .I5(\sdp_bl.ramb18_dp_bl.ram18_bl [7]),
        .O(\dc_bias[3]_i_48_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT5 #(
    .INIT(32'h4AAAAAAA)) 
    \dc_bias[3]_i_49 
       (.I0(\sdp_bl.ramb18_dp_bl.ram18_bl [9]),
        .I1(\sdp_bl.ramb18_dp_bl.ram18_bl [5]),
        .I2(\sdp_bl.ramb18_dp_bl.ram18_bl [8]),
        .I3(\sdp_bl.ramb18_dp_bl.ram18_bl [6]),
        .I4(\sdp_bl.ramb18_dp_bl.ram18_bl [7]),
        .O(\dc_bias[3]_i_49_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFF9FFFFFFFFF)) 
    \dc_bias[3]_i_50 
       (.I0(Q[2]),
        .I1(Q[1]),
        .I2(row[7]),
        .I3(row[9]),
        .I4(Q[5]),
        .I5(row[6]),
        .O(\dc_bias[3]_i_50_n_0 ));
  LUT6 #(
    .INIT(64'h02AAFFFF02AA0000)) 
    \dc_bias[3]_i_6 
       (.I0(\processRow[9]_i_6_n_0 ),
        .I1(Q[0]),
        .I2(Q[1]),
        .I3(\processRow[6]_i_2_n_0 ),
        .I4(row[9]),
        .I5(\encoded[9]_i_4_n_0 ),
        .O(\dc_bias[3]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF7F7F7F00)) 
    \dc_bias[3]_i_7 
       (.I0(\sdp_bl.ramb18_dp_bl.ram18_bl [0]),
        .I1(\sdp_bl.ramb18_dp_bl.ram18_bl [1]),
        .I2(\sdp_bl.ramb18_dp_bl.ram18_bl [2]),
        .I3(R8),
        .I4(R9),
        .I5(\encoded[8]_i_8__0_n_0 ),
        .O(\dc_bias[3]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hDFDFDFFFFFFFFFFF)) 
    \dc_bias[3]_i_8 
       (.I0(\dc_bias[3]_i_15_n_0 ),
        .I1(\sdp_bl.ramb18_dp_bl.ram18_bl [3]),
        .I2(\sdp_bl.ramb18_dp_bl.ram18_bl [4]),
        .I3(\sdp_bl.ramb18_dp_bl.ram18_bl [0]),
        .I4(\sdp_bl.ramb18_dp_bl.ram18_bl [1]),
        .I5(\sdp_bl.ramb18_dp_bl.ram18_bl [2]),
        .O(\dc_bias[3]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'h00000000AAFEFEFE)) 
    \dc_bias[3]_i_9 
       (.I0(\dc_bias[3]_i_16_n_0 ),
        .I1(R16),
        .I2(R15),
        .I3(Q[4]),
        .I4(\dc_bias[3]_i_17_n_0 ),
        .I5(\dc_bias[3]_i_18_n_0 ),
        .O(\dc_bias[3]_i_9_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT3 #(
    .INIT(8'hA3)) 
    \encoded[0]_i_1 
       (.I0(\dc_bias_reg[3] [1]),
        .I1(\encoded[8]_i_3_n_0 ),
        .I2(\encoded_reg[9] ),
        .O(\encoded_reg[0]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \encoded[0]_i_1__1 
       (.I0(\encoded_reg[9] ),
        .I1(\dc_bias_reg[3]_0 ),
        .O(\encoded_reg[0] ));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT3 #(
    .INIT(8'hEA)) 
    \encoded[2]_i_1 
       (.I0(\encoded[8]_i_3_n_0 ),
        .I1(\dc_bias_reg[3] [1]),
        .I2(\encoded_reg[9] ),
        .O(\encoded_reg[2] ));
  LUT6 #(
    .INIT(64'h0002211084400002)) 
    \encoded[8]_i_10 
       (.I0(row[6]),
        .I1(row[8]),
        .I2(DOADO[3]),
        .I3(\sdp_bl.ramb18_dp_bl.ram18_bl_0 ),
        .I4(DOADO[4]),
        .I5(row[7]),
        .O(\encoded[8]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'h1000048204821000)) 
    \encoded[8]_i_12 
       (.I0(Q[0]),
        .I1(Q[1]),
        .I2(DOADO[1]),
        .I3(DOADO[0]),
        .I4(Q[2]),
        .I5(DOADO[2]),
        .O(\encoded[8]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'h0000FF5F0000FCFF)) 
    \encoded[8]_i_13 
       (.I0(\encoded[8]_i_18_n_0 ),
        .I1(\encoded[8]_i_19_n_0 ),
        .I2(row[7]),
        .I3(\encoded[8]_i_20_n_0 ),
        .I4(row[9]),
        .I5(row[8]),
        .O(\encoded[8]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'hB3F3F3F3B3F3F3C3)) 
    \encoded[8]_i_14 
       (.I0(\encoded[8]_i_21_n_0 ),
        .I1(\sdp_bl.ramb18_dp_bl.ram18_bl [9]),
        .I2(\encoded[8]_i_22_n_0 ),
        .I3(\sdp_bl.ramb18_dp_bl.ram18_bl [6]),
        .I4(\sdp_bl.ramb18_dp_bl.ram18_bl [5]),
        .I5(\encoded[8]_i_23_n_0 ),
        .O(\encoded[8]_i_14_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFE0)) 
    \encoded[8]_i_18 
       (.I0(Q[0]),
        .I1(Q[1]),
        .I2(Q[2]),
        .I3(Q[4]),
        .I4(Q[3]),
        .I5(row[6]),
        .O(\encoded[8]_i_18_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'hA8)) 
    \encoded[8]_i_19 
       (.I0(Q[4]),
        .I1(Q[3]),
        .I2(Q[2]),
        .O(\encoded[8]_i_19_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \encoded[8]_i_20 
       (.I0(Q[5]),
        .I1(row[6]),
        .O(\encoded[8]_i_20_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT5 #(
    .INIT(32'h01555555)) 
    \encoded[8]_i_21 
       (.I0(\sdp_bl.ramb18_dp_bl.ram18_bl [4]),
        .I1(\sdp_bl.ramb18_dp_bl.ram18_bl [0]),
        .I2(\sdp_bl.ramb18_dp_bl.ram18_bl [1]),
        .I3(\sdp_bl.ramb18_dp_bl.ram18_bl [2]),
        .I4(\sdp_bl.ramb18_dp_bl.ram18_bl [3]),
        .O(\encoded[8]_i_21_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \encoded[8]_i_22 
       (.I0(\sdp_bl.ramb18_dp_bl.ram18_bl [7]),
        .I1(\sdp_bl.ramb18_dp_bl.ram18_bl [8]),
        .O(\encoded[8]_i_22_n_0 ));
  LUT3 #(
    .INIT(8'hA8)) 
    \encoded[8]_i_23 
       (.I0(\sdp_bl.ramb18_dp_bl.ram18_bl [4]),
        .I1(\sdp_bl.ramb18_dp_bl.ram18_bl [3]),
        .I2(\sdp_bl.ramb18_dp_bl.ram18_bl [2]),
        .O(\encoded[8]_i_23_n_0 ));
  LUT6 #(
    .INIT(64'h1554000000000000)) 
    \encoded[8]_i_3 
       (.I0(\sdp_bl.ramb18_dp_bl.ram18_bl [8]),
        .I1(\sdp_bl.ramb18_dp_bl.ram18_bl [6]),
        .I2(\sdp_bl.ramb18_dp_bl.ram18_bl [5]),
        .I3(\sdp_bl.ramb18_dp_bl.ram18_bl [4]),
        .I4(\sdp_bl.ramb18_dp_bl.ram18_bl [9]),
        .I5(\sdp_bl.ramb18_dp_bl.ram18_bl [7]),
        .O(\encoded[8]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hBBBBBBBBABABABAA)) 
    \encoded[8]_i_3__0 
       (.I0(\dc_bias[3]_i_9_n_0 ),
        .I1(\dc_bias[3]_i_8_n_0 ),
        .I2(\encoded[8]_i_7__0_n_0 ),
        .I3(R8),
        .I4(R9),
        .I5(\encoded[8]_i_8__0_n_0 ),
        .O(\encoded[8]_i_3__0_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \encoded[8]_i_5 
       (.I0(row[9]),
        .O(\encoded[8]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \encoded[8]_i_5__0 
       (.I0(\encoded[8]_i_13_n_0 ),
        .I1(\encoded[8]_i_14_n_0 ),
        .O(\encoded[8]_i_5__0_n_0 ));
  LUT5 #(
    .INIT(32'h00009009)) 
    \encoded[8]_i_6 
       (.I0(\sdp_bl.ramb18_dp_bl.ram18_bl_1 [0]),
        .I1(row[6]),
        .I2(row[7]),
        .I3(\sdp_bl.ramb18_dp_bl.ram18_bl_1 [1]),
        .I4(row[8]),
        .O(\encoded[8]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \encoded[8]_i_7__0 
       (.I0(\sdp_bl.ramb18_dp_bl.ram18_bl [0]),
        .I1(\sdp_bl.ramb18_dp_bl.ram18_bl [1]),
        .I2(\sdp_bl.ramb18_dp_bl.ram18_bl [2]),
        .O(\encoded[8]_i_7__0_n_0 ));
  LUT5 #(
    .INIT(32'hAAFEFEFE)) 
    \encoded[8]_i_8__0 
       (.I0(R10),
        .I1(R6),
        .I2(R7),
        .I3(\sdp_bl.ramb18_dp_bl.ram18_bl [2]),
        .I4(\sdp_bl.ramb18_dp_bl.ram18_bl [1]),
        .O(\encoded[8]_i_8__0_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \encoded[8]_i_9 
       (.I0(row[9]),
        .O(\encoded[8]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'h0000002000000000)) 
    \encoded[9]_i_2 
       (.I0(\encoded[9]_i_4_n_0 ),
        .I1(Q[4]),
        .I2(Q[1]),
        .I3(row[9]),
        .I4(Q[2]),
        .I5(Q[3]),
        .O(\encoded[9]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \encoded[9]_i_4 
       (.I0(row[8]),
        .I1(row[7]),
        .I2(row[6]),
        .I3(Q[5]),
        .O(\encoded[9]_i_4_n_0 ));
  CARRY4 \encoded_reg[8]_i_3 
       (.CI(1'b0),
        .CO({ch23,\encoded_reg[8]_i_3_n_1 ,\encoded_reg[8]_i_3_n_2 ,\encoded_reg[8]_i_3_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_encoded_reg[8]_i_3_O_UNCONNECTED [3:0]),
        .S({\encoded[8]_i_5_n_0 ,\encoded[8]_i_6_n_0 ,\sdp_bl.ramb18_dp_bl.ram18_bl_2 }));
  CARRY4 \encoded_reg[8]_i_4 
       (.CI(1'b0),
        .CO({ch13,\encoded_reg[8]_i_4_n_1 ,\encoded_reg[8]_i_4_n_2 ,\encoded_reg[8]_i_4_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_encoded_reg[8]_i_4_O_UNCONNECTED [3:0]),
        .S({\encoded[8]_i_9_n_0 ,\encoded[8]_i_10_n_0 ,S,\encoded[8]_i_12_n_0 }));
  LUT2 #(
    .INIT(4'h2)) 
    \processCol[0]_i_1 
       (.I0(\processCol[7]_i_2_n_0 ),
        .I1(\sdp_bl.ramb18_dp_bl.ram18_bl [0]),
        .O(p_0_in__2[0]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'h28)) 
    \processCol[1]_i_1 
       (.I0(\processCol[7]_i_2_n_0 ),
        .I1(\sdp_bl.ramb18_dp_bl.ram18_bl [0]),
        .I2(\sdp_bl.ramb18_dp_bl.ram18_bl [1]),
        .O(p_0_in__2[1]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT4 #(
    .INIT(16'h2A80)) 
    \processCol[2]_i_1 
       (.I0(\processCol[7]_i_2_n_0 ),
        .I1(\sdp_bl.ramb18_dp_bl.ram18_bl [1]),
        .I2(\sdp_bl.ramb18_dp_bl.ram18_bl [0]),
        .I3(\sdp_bl.ramb18_dp_bl.ram18_bl [2]),
        .O(p_0_in__2[2]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT5 #(
    .INIT(32'h00007F80)) 
    \processCol[3]_i_1 
       (.I0(\sdp_bl.ramb18_dp_bl.ram18_bl [0]),
        .I1(\sdp_bl.ramb18_dp_bl.ram18_bl [1]),
        .I2(\sdp_bl.ramb18_dp_bl.ram18_bl [2]),
        .I3(\sdp_bl.ramb18_dp_bl.ram18_bl [3]),
        .I4(\processCol[3]_i_2_n_0 ),
        .O(p_0_in__2[3]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT5 #(
    .INIT(32'hFE000000)) 
    \processCol[3]_i_2 
       (.I0(\sdp_bl.ramb18_dp_bl.ram18_bl [6]),
        .I1(\sdp_bl.ramb18_dp_bl.ram18_bl [5]),
        .I2(\sdp_bl.ramb18_dp_bl.ram18_bl [7]),
        .I3(\sdp_bl.ramb18_dp_bl.ram18_bl [8]),
        .I4(\sdp_bl.ramb18_dp_bl.ram18_bl [9]),
        .O(\processCol[3]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h2AAAAAAA80000000)) 
    \processCol[4]_i_1 
       (.I0(\processCol[7]_i_2_n_0 ),
        .I1(\sdp_bl.ramb18_dp_bl.ram18_bl [2]),
        .I2(\sdp_bl.ramb18_dp_bl.ram18_bl [1]),
        .I3(\sdp_bl.ramb18_dp_bl.ram18_bl [0]),
        .I4(\sdp_bl.ramb18_dp_bl.ram18_bl [3]),
        .I5(\sdp_bl.ramb18_dp_bl.ram18_bl [4]),
        .O(p_0_in__2[4]));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT4 #(
    .INIT(16'h0770)) 
    \processCol[5]_i_1 
       (.I0(\sdp_bl.ramb18_dp_bl.ram18_bl [8]),
        .I1(\sdp_bl.ramb18_dp_bl.ram18_bl [9]),
        .I2(\processCol[9]_i_3_n_0 ),
        .I3(\sdp_bl.ramb18_dp_bl.ram18_bl [5]),
        .O(p_0_in__2[5]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'h28)) 
    \processCol[6]_i_1 
       (.I0(\processCol[7]_i_2_n_0 ),
        .I1(\processCol[7]_i_3_n_0 ),
        .I2(\sdp_bl.ramb18_dp_bl.ram18_bl [6]),
        .O(p_0_in__2[6]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT4 #(
    .INIT(16'h2A80)) 
    \processCol[7]_i_1 
       (.I0(\processCol[7]_i_2_n_0 ),
        .I1(\processCol[7]_i_3_n_0 ),
        .I2(\sdp_bl.ramb18_dp_bl.ram18_bl [6]),
        .I3(\sdp_bl.ramb18_dp_bl.ram18_bl [7]),
        .O(p_0_in__2[7]));
  LUT6 #(
    .INIT(64'h00FFFFFF01FFFFFF)) 
    \processCol[7]_i_2 
       (.I0(\sdp_bl.ramb18_dp_bl.ram18_bl [7]),
        .I1(\sdp_bl.ramb18_dp_bl.ram18_bl [5]),
        .I2(\sdp_bl.ramb18_dp_bl.ram18_bl [6]),
        .I3(\sdp_bl.ramb18_dp_bl.ram18_bl [9]),
        .I4(\sdp_bl.ramb18_dp_bl.ram18_bl [8]),
        .I5(\processCol[9]_i_3_n_0 ),
        .O(\processCol[7]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \processCol[7]_i_3 
       (.I0(\sdp_bl.ramb18_dp_bl.ram18_bl [3]),
        .I1(\sdp_bl.ramb18_dp_bl.ram18_bl [0]),
        .I2(\sdp_bl.ramb18_dp_bl.ram18_bl [1]),
        .I3(\sdp_bl.ramb18_dp_bl.ram18_bl [2]),
        .I4(\sdp_bl.ramb18_dp_bl.ram18_bl [4]),
        .I5(\sdp_bl.ramb18_dp_bl.ram18_bl [5]),
        .O(\processCol[7]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h15555557C0000000)) 
    \processCol[8]_i_1 
       (.I0(\sdp_bl.ramb18_dp_bl.ram18_bl [9]),
        .I1(\processCol[9]_i_3_n_0 ),
        .I2(\sdp_bl.ramb18_dp_bl.ram18_bl [6]),
        .I3(\sdp_bl.ramb18_dp_bl.ram18_bl [7]),
        .I4(\sdp_bl.ramb18_dp_bl.ram18_bl [5]),
        .I5(\sdp_bl.ramb18_dp_bl.ram18_bl [8]),
        .O(p_0_in__2[8]));
  LUT5 #(
    .INIT(32'h7777777F)) 
    \processCol[9]_i_1 
       (.I0(\sdp_bl.ramb18_dp_bl.ram18_bl [9]),
        .I1(\sdp_bl.ramb18_dp_bl.ram18_bl [8]),
        .I2(\sdp_bl.ramb18_dp_bl.ram18_bl [7]),
        .I3(\sdp_bl.ramb18_dp_bl.ram18_bl [5]),
        .I4(\sdp_bl.ramb18_dp_bl.ram18_bl [6]),
        .O(\processCol[9]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0001FFFF80000000)) 
    \processCol[9]_i_2 
       (.I0(\sdp_bl.ramb18_dp_bl.ram18_bl [5]),
        .I1(\sdp_bl.ramb18_dp_bl.ram18_bl [7]),
        .I2(\sdp_bl.ramb18_dp_bl.ram18_bl [6]),
        .I3(\processCol[9]_i_3_n_0 ),
        .I4(\sdp_bl.ramb18_dp_bl.ram18_bl [8]),
        .I5(\sdp_bl.ramb18_dp_bl.ram18_bl [9]),
        .O(p_0_in__2[9]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT5 #(
    .INIT(32'h80000000)) 
    \processCol[9]_i_3 
       (.I0(\sdp_bl.ramb18_dp_bl.ram18_bl [3]),
        .I1(\sdp_bl.ramb18_dp_bl.ram18_bl [0]),
        .I2(\sdp_bl.ramb18_dp_bl.ram18_bl [1]),
        .I3(\sdp_bl.ramb18_dp_bl.ram18_bl [2]),
        .I4(\sdp_bl.ramb18_dp_bl.ram18_bl [4]),
        .O(\processCol[9]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \processCol_reg[0] 
       (.C(CLK),
        .CE(\processCol[9]_i_1_n_0 ),
        .D(p_0_in__2[0]),
        .Q(\sdp_bl.ramb18_dp_bl.ram18_bl [0]),
        .R(s00_axi_aresetn_0));
  FDRE #(
    .INIT(1'b0)) 
    \processCol_reg[1] 
       (.C(CLK),
        .CE(\processCol[9]_i_1_n_0 ),
        .D(p_0_in__2[1]),
        .Q(\sdp_bl.ramb18_dp_bl.ram18_bl [1]),
        .R(s00_axi_aresetn_0));
  FDRE #(
    .INIT(1'b0)) 
    \processCol_reg[2] 
       (.C(CLK),
        .CE(\processCol[9]_i_1_n_0 ),
        .D(p_0_in__2[2]),
        .Q(\sdp_bl.ramb18_dp_bl.ram18_bl [2]),
        .R(s00_axi_aresetn_0));
  FDRE #(
    .INIT(1'b0)) 
    \processCol_reg[3] 
       (.C(CLK),
        .CE(\processCol[9]_i_1_n_0 ),
        .D(p_0_in__2[3]),
        .Q(\sdp_bl.ramb18_dp_bl.ram18_bl [3]),
        .R(s00_axi_aresetn_0));
  FDRE #(
    .INIT(1'b0)) 
    \processCol_reg[4] 
       (.C(CLK),
        .CE(\processCol[9]_i_1_n_0 ),
        .D(p_0_in__2[4]),
        .Q(\sdp_bl.ramb18_dp_bl.ram18_bl [4]),
        .R(s00_axi_aresetn_0));
  FDRE #(
    .INIT(1'b0)) 
    \processCol_reg[5] 
       (.C(CLK),
        .CE(\processCol[9]_i_1_n_0 ),
        .D(p_0_in__2[5]),
        .Q(\sdp_bl.ramb18_dp_bl.ram18_bl [5]),
        .R(s00_axi_aresetn_0));
  FDRE #(
    .INIT(1'b0)) 
    \processCol_reg[6] 
       (.C(CLK),
        .CE(\processCol[9]_i_1_n_0 ),
        .D(p_0_in__2[6]),
        .Q(\sdp_bl.ramb18_dp_bl.ram18_bl [6]),
        .R(s00_axi_aresetn_0));
  FDRE #(
    .INIT(1'b0)) 
    \processCol_reg[7] 
       (.C(CLK),
        .CE(\processCol[9]_i_1_n_0 ),
        .D(p_0_in__2[7]),
        .Q(\sdp_bl.ramb18_dp_bl.ram18_bl [7]),
        .R(s00_axi_aresetn_0));
  FDRE #(
    .INIT(1'b0)) 
    \processCol_reg[8] 
       (.C(CLK),
        .CE(\processCol[9]_i_1_n_0 ),
        .D(p_0_in__2[8]),
        .Q(\sdp_bl.ramb18_dp_bl.ram18_bl [8]),
        .R(s00_axi_aresetn_0));
  FDRE #(
    .INIT(1'b0)) 
    \processCol_reg[9] 
       (.C(CLK),
        .CE(\processCol[9]_i_1_n_0 ),
        .D(p_0_in__2[9]),
        .Q(\sdp_bl.ramb18_dp_bl.ram18_bl [9]),
        .R(s00_axi_aresetn_0));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \processRow[0]_i_1 
       (.I0(Q[0]),
        .O(plusOp[0]));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \processRow[1]_i_1 
       (.I0(Q[0]),
        .I1(Q[1]),
        .O(plusOp[1]));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \processRow[2]_i_1 
       (.I0(Q[2]),
        .I1(Q[1]),
        .I2(Q[0]),
        .O(plusOp[2]));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \processRow[3]_i_1 
       (.I0(Q[3]),
        .I1(Q[1]),
        .I2(Q[2]),
        .I3(Q[0]),
        .O(plusOp[3]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \processRow[4]_i_1 
       (.I0(Q[4]),
        .I1(Q[2]),
        .I2(Q[3]),
        .I3(Q[0]),
        .I4(Q[1]),
        .O(plusOp[4]));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \processRow[5]_i_1 
       (.I0(Q[5]),
        .I1(Q[4]),
        .I2(Q[1]),
        .I3(Q[0]),
        .I4(Q[3]),
        .I5(Q[2]),
        .O(plusOp[5]));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \processRow[6]_i_1 
       (.I0(row[6]),
        .I1(Q[5]),
        .I2(\processRow[6]_i_2_n_0 ),
        .I3(Q[0]),
        .I4(Q[1]),
        .I5(Q[4]),
        .O(plusOp[6]));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \processRow[6]_i_2 
       (.I0(Q[2]),
        .I1(Q[3]),
        .O(\processRow[6]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \processRow[7]_i_1 
       (.I0(row[7]),
        .I1(row[6]),
        .I2(\processRow[9]_i_7_n_0 ),
        .I3(Q[5]),
        .O(plusOp[7]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \processRow[8]_i_1 
       (.I0(row[8]),
        .I1(row[7]),
        .I2(Q[5]),
        .I3(\processRow[9]_i_7_n_0 ),
        .I4(row[6]),
        .O(plusOp[8]));
  LUT3 #(
    .INIT(8'h4F)) 
    \processRow[9]_i_1 
       (.I0(\processRow[9]_i_4_n_0 ),
        .I1(\processRow[9]_i_5_n_0 ),
        .I2(s00_axi_aresetn),
        .O(\processRow[9]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h0888AAAA)) 
    \processRow[9]_i_2 
       (.I0(\processRow[9]_i_5_n_0 ),
        .I1(\processRow[9]_i_6_n_0 ),
        .I2(Q[3]),
        .I3(Q[2]),
        .I4(row[9]),
        .O(processRow0));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \processRow[9]_i_3 
       (.I0(row[9]),
        .I1(row[8]),
        .I2(row[7]),
        .I3(row[6]),
        .I4(Q[5]),
        .I5(\processRow[9]_i_7_n_0 ),
        .O(plusOp[9]));
  LUT6 #(
    .INIT(64'hFFDFFFFFFFFFFFFF)) 
    \processRow[9]_i_4 
       (.I0(\processRow[9]_i_6_n_0 ),
        .I1(Q[1]),
        .I2(row[9]),
        .I3(Q[0]),
        .I4(Q[2]),
        .I5(Q[3]),
        .O(\processRow[9]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h0100000000000000)) 
    \processRow[9]_i_5 
       (.I0(\sdp_bl.ramb18_dp_bl.ram18_bl [6]),
        .I1(\sdp_bl.ramb18_dp_bl.ram18_bl [5]),
        .I2(\sdp_bl.ramb18_dp_bl.ram18_bl [7]),
        .I3(\sdp_bl.ramb18_dp_bl.ram18_bl [9]),
        .I4(\sdp_bl.ramb18_dp_bl.ram18_bl [8]),
        .I5(\processCol[9]_i_3_n_0 ),
        .O(\processRow[9]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT5 #(
    .INIT(32'h00000001)) 
    \processRow[9]_i_6 
       (.I0(row[7]),
        .I1(Q[5]),
        .I2(row[6]),
        .I3(row[8]),
        .I4(Q[4]),
        .O(\processRow[9]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT5 #(
    .INIT(32'h80000000)) 
    \processRow[9]_i_7 
       (.I0(Q[2]),
        .I1(Q[3]),
        .I2(Q[0]),
        .I3(Q[1]),
        .I4(Q[4]),
        .O(\processRow[9]_i_7_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \processRow_reg[0] 
       (.C(CLK),
        .CE(processRow0),
        .D(plusOp[0]),
        .Q(Q[0]),
        .R(\processRow[9]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \processRow_reg[1] 
       (.C(CLK),
        .CE(processRow0),
        .D(plusOp[1]),
        .Q(Q[1]),
        .R(\processRow[9]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \processRow_reg[2] 
       (.C(CLK),
        .CE(processRow0),
        .D(plusOp[2]),
        .Q(Q[2]),
        .R(\processRow[9]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \processRow_reg[3] 
       (.C(CLK),
        .CE(processRow0),
        .D(plusOp[3]),
        .Q(Q[3]),
        .R(\processRow[9]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \processRow_reg[4] 
       (.C(CLK),
        .CE(processRow0),
        .D(plusOp[4]),
        .Q(Q[4]),
        .R(\processRow[9]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \processRow_reg[5] 
       (.C(CLK),
        .CE(processRow0),
        .D(plusOp[5]),
        .Q(Q[5]),
        .R(\processRow[9]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \processRow_reg[6] 
       (.C(CLK),
        .CE(processRow0),
        .D(plusOp[6]),
        .Q(row[6]),
        .R(\processRow[9]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \processRow_reg[7] 
       (.C(CLK),
        .CE(processRow0),
        .D(plusOp[7]),
        .Q(row[7]),
        .R(\processRow[9]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \processRow_reg[8] 
       (.C(CLK),
        .CE(processRow0),
        .D(plusOp[8]),
        .Q(row[8]),
        .R(\processRow[9]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \processRow_reg[9] 
       (.C(CLK),
        .CE(processRow0),
        .D(plusOp[9]),
        .Q(row[9]),
        .R(\processRow[9]_i_1_n_0 ));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_video
   (tmds,
    tmdsb,
    Q,
    \sdp_bl.ramb18_dp_bl.ram18_bl ,
    s00_axi_aresetn,
    s00_axi_aclk,
    \trigger_volt_reg[9] ,
    \trigger_time_reg[4] ,
    \trigger_time_reg[8] ,
    \trigger_time_reg[3] ,
    \trigger_time_reg[7] ,
    \trigger_time_reg[6] ,
    \trigger_time_reg[2] ,
    \trigger_time_reg[9] ,
    DOADO,
    \sdp_bl.ramb18_dp_bl.ram18_bl_0 ,
    \sdp_bl.ramb18_dp_bl.ram18_bl_1 ,
    switch,
    S,
    \sdp_bl.ramb18_dp_bl.ram18_bl_2 ,
    s00_axi_aresetn_0,
    lopt);
  output [3:0]tmds;
  output [3:0]tmdsb;
  output [5:0]Q;
  output [9:0]\sdp_bl.ramb18_dp_bl.ram18_bl ;
  input s00_axi_aresetn;
  input s00_axi_aclk;
  input [9:0]\trigger_volt_reg[9] ;
  input \trigger_time_reg[4] ;
  input [3:0]\trigger_time_reg[8] ;
  input \trigger_time_reg[3] ;
  input \trigger_time_reg[7] ;
  input \trigger_time_reg[6] ;
  input \trigger_time_reg[2] ;
  input \trigger_time_reg[9] ;
  input [4:0]DOADO;
  input \sdp_bl.ramb18_dp_bl.ram18_bl_0 ;
  input [1:0]\sdp_bl.ramb18_dp_bl.ram18_bl_1 ;
  input [1:0]switch;
  input [0:0]S;
  input [1:0]\sdp_bl.ramb18_dp_bl.ram18_bl_2 ;
  input s00_axi_aresetn_0;
  output lopt;

  wire [4:0]DOADO;
  wire Inst_vga_n_16;
  wire Inst_vga_n_17;
  wire Inst_vga_n_18;
  wire Inst_vga_n_19;
  wire Inst_vga_n_20;
  wire Inst_vga_n_21;
  wire Inst_vga_n_22;
  wire Inst_vga_n_23;
  wire Inst_vga_n_25;
  wire Inst_vga_n_26;
  wire Inst_vga_n_28;
  wire Inst_vga_n_29;
  wire Inst_vga_n_30;
  wire [5:0]Q;
  wire [0:0]S;
  wire \TDMS_encoder_blue/p_1_in ;
  wire \TDMS_encoder_green/p_1_in ;
  wire \TDMS_encoder_red/p_1_in ;
  wire blank;
  wire blue_s;
  wire clock_s;
  wire [8:8]encoded0_in;
  wire green_s;
  wire inst_dvid_n_10;
  wire inst_dvid_n_11;
  wire inst_dvid_n_12;
  wire inst_dvid_n_13;
  wire inst_dvid_n_14;
  wire inst_dvid_n_15;
  wire inst_dvid_n_17;
  wire inst_dvid_n_18;
  wire inst_dvid_n_19;
  wire inst_dvid_n_20;
  wire inst_dvid_n_21;
  wire inst_dvid_n_22;
  wire inst_dvid_n_23;
  wire inst_dvid_n_24;
  wire inst_dvid_n_25;
  wire inst_dvid_n_26;
  wire inst_dvid_n_27;
  wire inst_dvid_n_4;
  wire inst_dvid_n_7;
  wire inst_dvid_n_8;
  wire inst_dvid_n_9;
  wire lopt;
  wire pixel_clk;
  wire red_s;
  wire s00_axi_aclk;
  wire s00_axi_aresetn;
  wire s00_axi_aresetn_0;
  wire [9:0]\sdp_bl.ramb18_dp_bl.ram18_bl ;
  wire \sdp_bl.ramb18_dp_bl.ram18_bl_0 ;
  wire [1:0]\sdp_bl.ramb18_dp_bl.ram18_bl_1 ;
  wire [1:0]\sdp_bl.ramb18_dp_bl.ram18_bl_2 ;
  wire serialize_clk;
  wire serialize_clk_n;
  wire [1:0]switch;
  wire [3:0]tmds;
  wire [3:0]tmdsb;
  wire \trigger_time_reg[2] ;
  wire \trigger_time_reg[3] ;
  wire \trigger_time_reg[4] ;
  wire \trigger_time_reg[6] ;
  wire \trigger_time_reg[7] ;
  wire [3:0]\trigger_time_reg[8] ;
  wire \trigger_time_reg[9] ;
  wire [9:0]\trigger_volt_reg[9] ;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_vga Inst_vga
       (.CLK(pixel_clk),
        .DOADO(DOADO),
        .Q(Q),
        .S(S),
        .SR(blank),
        .\dc_bias_reg[1] (inst_dvid_n_27),
        .\dc_bias_reg[1]_0 (inst_dvid_n_18),
        .\dc_bias_reg[1]_1 (inst_dvid_n_19),
        .\dc_bias_reg[3] ({\TDMS_encoder_blue/p_1_in ,inst_dvid_n_17}),
        .\dc_bias_reg[3]_0 (\TDMS_encoder_red/p_1_in ),
        .\dc_bias_reg[3]_1 (inst_dvid_n_15),
        .\dc_bias_reg[3]_2 (\TDMS_encoder_green/p_1_in ),
        .encoded0_in(encoded0_in),
        .\encoded_reg[0] (Inst_vga_n_21),
        .\encoded_reg[0]_0 (Inst_vga_n_26),
        .\encoded_reg[2] (Inst_vga_n_25),
        .\encoded_reg[8] (Inst_vga_n_16),
        .\encoded_reg[8]_0 (Inst_vga_n_18),
        .\encoded_reg[8]_1 (Inst_vga_n_19),
        .\encoded_reg[8]_2 (Inst_vga_n_20),
        .\encoded_reg[8]_3 (Inst_vga_n_22),
        .\encoded_reg[9] (Inst_vga_n_17),
        .\encoded_reg[9]_0 (Inst_vga_n_23),
        .\encoded_reg[9]_1 (Inst_vga_n_28),
        .\encoded_reg[9]_2 (Inst_vga_n_29),
        .\encoded_reg[9]_3 (Inst_vga_n_30),
        .s00_axi_aresetn(s00_axi_aresetn),
        .s00_axi_aresetn_0(s00_axi_aresetn_0),
        .\sdp_bl.ramb18_dp_bl.ram18_bl (\sdp_bl.ramb18_dp_bl.ram18_bl ),
        .\sdp_bl.ramb18_dp_bl.ram18_bl_0 (\sdp_bl.ramb18_dp_bl.ram18_bl_0 ),
        .\sdp_bl.ramb18_dp_bl.ram18_bl_1 (\sdp_bl.ramb18_dp_bl.ram18_bl_1 ),
        .\sdp_bl.ramb18_dp_bl.ram18_bl_2 (\sdp_bl.ramb18_dp_bl.ram18_bl_2 ),
        .switch(switch),
        .\trigger_time_reg[1] (inst_dvid_n_22),
        .\trigger_time_reg[1]_0 (inst_dvid_n_20),
        .\trigger_time_reg[2] (\trigger_time_reg[2] ),
        .\trigger_time_reg[3] (\trigger_time_reg[3] ),
        .\trigger_time_reg[4] (\trigger_time_reg[4] ),
        .\trigger_time_reg[4]_0 (inst_dvid_n_8),
        .\trigger_time_reg[5] (inst_dvid_n_12),
        .\trigger_time_reg[5]_0 (inst_dvid_n_10),
        .\trigger_time_reg[5]_1 (inst_dvid_n_21),
        .\trigger_time_reg[6] (\trigger_time_reg[6] ),
        .\trigger_time_reg[6]_0 (inst_dvid_n_11),
        .\trigger_time_reg[6]_1 (inst_dvid_n_9),
        .\trigger_time_reg[7] (\trigger_time_reg[7] ),
        .\trigger_time_reg[7]_0 (inst_dvid_n_7),
        .\trigger_time_reg[8] (\trigger_time_reg[8] ),
        .\trigger_time_reg[9] (\trigger_time_reg[9] ),
        .\trigger_volt_reg[1] (inst_dvid_n_26),
        .\trigger_volt_reg[1]_0 (inst_dvid_n_23),
        .\trigger_volt_reg[2] (inst_dvid_n_4),
        .\trigger_volt_reg[4] (inst_dvid_n_25),
        .\trigger_volt_reg[4]_0 (inst_dvid_n_14),
        .\trigger_volt_reg[5] (inst_dvid_n_13),
        .\trigger_volt_reg[5]_0 (inst_dvid_n_24),
        .\trigger_volt_reg[9] (\trigger_volt_reg[9] ));
  (* CAPACITANCE = "DONT_CARE" *) 
  (* XILINX_LEGACY_PRIM = "OBUFDS" *) 
  (* box_type = "PRIMITIVE" *) 
  OBUFDS #(
    .IOSTANDARD("DEFAULT")) 
    OBUFDS_blue
       (.I(blue_s),
        .O(tmds[0]),
        .OB(tmdsb[0]));
  (* CAPACITANCE = "DONT_CARE" *) 
  (* XILINX_LEGACY_PRIM = "OBUFDS" *) 
  (* box_type = "PRIMITIVE" *) 
  OBUFDS #(
    .IOSTANDARD("DEFAULT")) 
    OBUFDS_clock
       (.I(clock_s),
        .O(tmds[3]),
        .OB(tmdsb[3]));
  (* CAPACITANCE = "DONT_CARE" *) 
  (* XILINX_LEGACY_PRIM = "OBUFDS" *) 
  (* box_type = "PRIMITIVE" *) 
  OBUFDS #(
    .IOSTANDARD("DEFAULT")) 
    OBUFDS_green
       (.I(red_s),
        .O(tmds[2]),
        .OB(tmdsb[2]));
  (* CAPACITANCE = "DONT_CARE" *) 
  (* XILINX_LEGACY_PRIM = "OBUFDS" *) 
  (* box_type = "PRIMITIVE" *) 
  OBUFDS #(
    .IOSTANDARD("DEFAULT")) 
    OBUFDS_red
       (.I(green_s),
        .O(tmds[1]),
        .OB(tmdsb[1]));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_dvid inst_dvid
       (.CLK(pixel_clk),
        .Q(\TDMS_encoder_green/p_1_in ),
        .SR(blank),
        .blue_s(blue_s),
        .clk_out2(serialize_clk),
        .clk_out3(serialize_clk_n),
        .clock_s(clock_s),
        .\dc_bias_reg[0] (\TDMS_encoder_red/p_1_in ),
        .\dc_bias_reg[0]_0 ({\TDMS_encoder_blue/p_1_in ,inst_dvid_n_17}),
        .\dc_bias_reg[1] (Inst_vga_n_22),
        .\dc_bias_reg[2] (Inst_vga_n_16),
        .\dc_bias_reg[3] (Inst_vga_n_21),
        .\dc_bias_reg[3]_0 (Inst_vga_n_30),
        .\dc_bias_reg[3]_1 (Inst_vga_n_29),
        .\dc_bias_reg[3]_2 (Inst_vga_n_25),
        .\dc_bias_reg[3]_3 (Inst_vga_n_26),
        .\dc_bias_reg[3]_4 (Inst_vga_n_28),
        .encoded0_in(encoded0_in),
        .\encoded_reg[8] (inst_dvid_n_4),
        .\encoded_reg[8]_0 (inst_dvid_n_7),
        .\encoded_reg[8]_1 (inst_dvid_n_8),
        .\encoded_reg[8]_10 (inst_dvid_n_20),
        .\encoded_reg[8]_11 (inst_dvid_n_21),
        .\encoded_reg[8]_12 (inst_dvid_n_22),
        .\encoded_reg[8]_13 (inst_dvid_n_23),
        .\encoded_reg[8]_14 (inst_dvid_n_24),
        .\encoded_reg[8]_15 (inst_dvid_n_25),
        .\encoded_reg[8]_16 (inst_dvid_n_26),
        .\encoded_reg[8]_17 (inst_dvid_n_27),
        .\encoded_reg[8]_2 (inst_dvid_n_9),
        .\encoded_reg[8]_3 (inst_dvid_n_10),
        .\encoded_reg[8]_4 (inst_dvid_n_11),
        .\encoded_reg[8]_5 (inst_dvid_n_12),
        .\encoded_reg[8]_6 (inst_dvid_n_13),
        .\encoded_reg[8]_7 (inst_dvid_n_14),
        .\encoded_reg[8]_8 (inst_dvid_n_18),
        .\encoded_reg[8]_9 (inst_dvid_n_19),
        .\encoded_reg[9] (inst_dvid_n_15),
        .green_s(green_s),
        .\processCol_reg[0] (Inst_vga_n_20),
        .\processCol_reg[1] (Inst_vga_n_23),
        .\processCol_reg[6] (Inst_vga_n_19),
        .\processCol_reg[6]_0 (Inst_vga_n_17),
        .\processRow_reg[2] (Inst_vga_n_18),
        .red_s(red_s),
        .\trigger_time_reg[2] (\trigger_time_reg[2] ),
        .\trigger_time_reg[3] (\trigger_time_reg[3] ),
        .\trigger_time_reg[4] (\trigger_time_reg[4] ),
        .\trigger_time_reg[5] (\trigger_time_reg[8] [2:0]),
        .\trigger_time_reg[6] (\trigger_time_reg[6] ),
        .\trigger_time_reg[7] (\trigger_time_reg[7] ),
        .\trigger_volt_reg[6] (\trigger_volt_reg[9] [6:0]));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_0 mmcm_adv_inst_display_clocks
       (.clk_in1(s00_axi_aclk),
        .clk_out1(pixel_clk),
        .clk_out2(serialize_clk),
        .clk_out3(serialize_clk_n),
        .lopt(lopt),
        .resetn(s00_axi_aresetn));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
