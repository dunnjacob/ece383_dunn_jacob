-- Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
-- Date        : Thu Mar 17 21:53:40 2022
-- Host        : DESKTOP-DNF5IPP running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_my_lab3_ip_0_1_sim_netlist.vhdl
-- Design      : design_1_my_lab3_ip_0_1
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a200tsbg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Scopeface is
  port (
    CO : out STD_LOGIC_VECTOR ( 0 to 0 );
    \encoded_reg[8]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \encoded_reg[8]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \encoded_reg[8]_1\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \encoded_reg[8]_2\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \encoded_reg[8]_3\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \encoded_reg[8]_4\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \encoded_reg[8]_5\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \encoded_reg[8]_6\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \encoded_reg[8]_7\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \encoded_reg[8]_8\ : out STD_LOGIC;
    \encoded_reg[8]_9\ : out STD_LOGIC;
    \encoded_reg[8]_10\ : out STD_LOGIC;
    \encoded_reg[8]_11\ : out STD_LOGIC;
    \encoded_reg[9]\ : out STD_LOGIC;
    encoded0_in : out STD_LOGIC_VECTOR ( 0 to 0 );
    \encoded_reg[9]_0\ : out STD_LOGIC;
    \encoded_reg[9]_1\ : out STD_LOGIC;
    \encoded_reg[9]_2\ : out STD_LOGIC;
    S : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \trigger_time_reg[0]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \trigger_time_reg[8]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \trigger_time_reg[7]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \trigger_time_reg[8]_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \trigger_volt_reg[9]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \trigger_volt_reg[9]_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \trigger_volt_reg[8]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \trigger_volt_reg[7]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \trigger_volt_reg[7]_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 9 downto 0 );
    \processRow_reg[0]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \processCol_reg[9]\ : in STD_LOGIC_VECTOR ( 9 downto 0 );
    \processCol_reg[8]\ : in STD_LOGIC;
    \processCol_reg[6]\ : in STD_LOGIC;
    \dc_bias_reg[3]\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \dc_bias_reg[1]\ : in STD_LOGIC;
    \dc_bias_reg[1]_0\ : in STD_LOGIC;
    \processCol_reg[0]\ : in STD_LOGIC;
    \processRow_reg[7]\ : in STD_LOGIC;
    switch : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \processRow_reg[9]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \dc_bias_reg[1]_1\ : in STD_LOGIC;
    \processCol_reg[1]\ : in STD_LOGIC;
    \processCol_reg[2]\ : in STD_LOGIC;
    \processRow_reg[2]\ : in STD_LOGIC;
    \processRow_reg[7]_0\ : in STD_LOGIC;
    \processRow_reg[4]\ : in STD_LOGIC;
    \processRow_reg[9]_0\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \processCol_reg[9]_0\ : in STD_LOGIC;
    \processRow_reg[9]_1\ : in STD_LOGIC;
    \processRow_reg[7]_1\ : in STD_LOGIC;
    \processRow_reg[4]_0\ : in STD_LOGIC;
    \dc_bias_reg[3]_0\ : in STD_LOGIC;
    \dc_bias_reg[3]_1\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \dc_bias_reg[3]_2\ : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Scopeface;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Scopeface is
  signal R10_carry_n_1 : STD_LOGIC;
  signal R10_carry_n_2 : STD_LOGIC;
  signal R10_carry_n_3 : STD_LOGIC;
  signal R13_carry_n_1 : STD_LOGIC;
  signal R13_carry_n_2 : STD_LOGIC;
  signal R13_carry_n_3 : STD_LOGIC;
  signal R14_carry_n_1 : STD_LOGIC;
  signal R14_carry_n_2 : STD_LOGIC;
  signal R14_carry_n_3 : STD_LOGIC;
  signal R15_carry_n_1 : STD_LOGIC;
  signal R15_carry_n_2 : STD_LOGIC;
  signal R15_carry_n_3 : STD_LOGIC;
  signal R16_carry_n_1 : STD_LOGIC;
  signal R16_carry_n_2 : STD_LOGIC;
  signal R16_carry_n_3 : STD_LOGIC;
  signal R17_carry_n_1 : STD_LOGIC;
  signal R17_carry_n_2 : STD_LOGIC;
  signal R17_carry_n_3 : STD_LOGIC;
  signal \R21_inferred__0/i___20_carry__0_n_3\ : STD_LOGIC;
  signal \R21_inferred__0/i___20_carry__0_n_6\ : STD_LOGIC;
  signal \R21_inferred__0/i___20_carry__0_n_7\ : STD_LOGIC;
  signal \R21_inferred__0/i___20_carry_n_0\ : STD_LOGIC;
  signal \R21_inferred__0/i___20_carry_n_1\ : STD_LOGIC;
  signal \R21_inferred__0/i___20_carry_n_2\ : STD_LOGIC;
  signal \R21_inferred__0/i___20_carry_n_3\ : STD_LOGIC;
  signal \R21_inferred__0/i___20_carry_n_4\ : STD_LOGIC;
  signal \R21_inferred__0/i___20_carry_n_5\ : STD_LOGIC;
  signal \R21_inferred__0/i___20_carry_n_6\ : STD_LOGIC;
  signal \R21_inferred__0/i___20_carry_n_7\ : STD_LOGIC;
  signal \R21_inferred__0/i__carry__0_n_0\ : STD_LOGIC;
  signal \R21_inferred__0/i__carry__0_n_1\ : STD_LOGIC;
  signal \R21_inferred__0/i__carry__0_n_2\ : STD_LOGIC;
  signal \R21_inferred__0/i__carry__0_n_3\ : STD_LOGIC;
  signal \R21_inferred__0/i__carry__0_n_4\ : STD_LOGIC;
  signal \R21_inferred__0/i__carry__1_n_0\ : STD_LOGIC;
  signal \R21_inferred__0/i__carry__1_n_2\ : STD_LOGIC;
  signal \R21_inferred__0/i__carry__1_n_3\ : STD_LOGIC;
  signal \R21_inferred__0/i__carry__1_n_5\ : STD_LOGIC;
  signal \R21_inferred__0/i__carry__1_n_6\ : STD_LOGIC;
  signal \R21_inferred__0/i__carry__1_n_7\ : STD_LOGIC;
  signal \R21_inferred__0/i__carry_n_0\ : STD_LOGIC;
  signal \R21_inferred__0/i__carry_n_1\ : STD_LOGIC;
  signal \R21_inferred__0/i__carry_n_2\ : STD_LOGIC;
  signal \R21_inferred__0/i__carry_n_3\ : STD_LOGIC;
  signal \R22__23_carry_i_1_n_0\ : STD_LOGIC;
  signal \R22__23_carry_i_2_n_0\ : STD_LOGIC;
  signal \R22__23_carry_i_3_n_0\ : STD_LOGIC;
  signal \R22__23_carry_i_4_n_0\ : STD_LOGIC;
  signal \R22__23_carry_n_2\ : STD_LOGIC;
  signal \R22__23_carry_n_3\ : STD_LOGIC;
  signal \R22__23_carry_n_5\ : STD_LOGIC;
  signal \R22__23_carry_n_6\ : STD_LOGIC;
  signal \R22__23_carry_n_7\ : STD_LOGIC;
  signal \R22__29_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \R22__29_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \R22__29_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \R22__29_carry__0_n_2\ : STD_LOGIC;
  signal \R22__29_carry__0_n_3\ : STD_LOGIC;
  signal \R22__29_carry__0_n_5\ : STD_LOGIC;
  signal \R22__29_carry__0_n_6\ : STD_LOGIC;
  signal \R22__29_carry__0_n_7\ : STD_LOGIC;
  signal \R22__29_carry_i_1_n_0\ : STD_LOGIC;
  signal \R22__29_carry_i_2_n_0\ : STD_LOGIC;
  signal \R22__29_carry_i_3_n_0\ : STD_LOGIC;
  signal \R22__29_carry_n_0\ : STD_LOGIC;
  signal \R22__29_carry_n_1\ : STD_LOGIC;
  signal \R22__29_carry_n_2\ : STD_LOGIC;
  signal \R22__29_carry_n_3\ : STD_LOGIC;
  signal \R22__29_carry_n_4\ : STD_LOGIC;
  signal \R22__29_carry_n_5\ : STD_LOGIC;
  signal \R22__29_carry_n_6\ : STD_LOGIC;
  signal \R22__29_carry_n_7\ : STD_LOGIC;
  signal \R22_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \R22_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \R22_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \R22_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \R22_carry__0_n_0\ : STD_LOGIC;
  signal \R22_carry__0_n_1\ : STD_LOGIC;
  signal \R22_carry__0_n_2\ : STD_LOGIC;
  signal \R22_carry__0_n_3\ : STD_LOGIC;
  signal \R22_carry__0_n_4\ : STD_LOGIC;
  signal \R22_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \R22_carry__1_n_0\ : STD_LOGIC;
  signal \R22_carry__1_n_2\ : STD_LOGIC;
  signal \R22_carry__1_n_3\ : STD_LOGIC;
  signal \R22_carry__1_n_5\ : STD_LOGIC;
  signal \R22_carry__1_n_6\ : STD_LOGIC;
  signal \R22_carry__1_n_7\ : STD_LOGIC;
  signal R22_carry_i_1_n_0 : STD_LOGIC;
  signal R22_carry_i_2_n_0 : STD_LOGIC;
  signal R22_carry_i_3_n_0 : STD_LOGIC;
  signal R22_carry_n_0 : STD_LOGIC;
  signal R22_carry_n_1 : STD_LOGIC;
  signal R22_carry_n_2 : STD_LOGIC;
  signal R22_carry_n_3 : STD_LOGIC;
  signal R6_carry_n_1 : STD_LOGIC;
  signal R6_carry_n_2 : STD_LOGIC;
  signal R6_carry_n_3 : STD_LOGIC;
  signal R7_carry_n_1 : STD_LOGIC;
  signal R7_carry_n_2 : STD_LOGIC;
  signal R7_carry_n_3 : STD_LOGIC;
  signal R8_carry_n_1 : STD_LOGIC;
  signal R8_carry_n_2 : STD_LOGIC;
  signal R8_carry_n_3 : STD_LOGIC;
  signal R9_carry_n_1 : STD_LOGIC;
  signal R9_carry_n_2 : STD_LOGIC;
  signal R9_carry_n_3 : STD_LOGIC;
  signal \dc_bias[3]_i_11_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_12_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_23_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_24_n_0\ : STD_LOGIC;
  signal \^encoded_reg[8]_9\ : STD_LOGIC;
  signal \^encoded_reg[9]\ : STD_LOGIC;
  signal \i___20_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \i___20_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \i___20_carry_i_1_n_0\ : STD_LOGIC;
  signal \i___20_carry_i_2_n_0\ : STD_LOGIC;
  signal \i___20_carry_i_3_n_0\ : STD_LOGIC;
  signal \i___20_carry_i_4_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_1_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_2_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_3_n_0\ : STD_LOGIC;
  signal \i__carry_i_1__0_n_0\ : STD_LOGIC;
  signal \i__carry_i_2_n_0\ : STD_LOGIC;
  signal \i__carry_i_3_n_0\ : STD_LOGIC;
  signal NLW_R10_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_R13_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_R14_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_R15_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_R16_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_R17_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_R21_inferred__0/i___20_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_R21_inferred__0/i___20_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_R21_inferred__0/i__carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_R21_inferred__0/i__carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_R21_inferred__0/i__carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 to 2 );
  signal \NLW_R21_inferred__0/i__carry__1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_R22__23_carry_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_R22__23_carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_R22__29_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_R22__29_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal NLW_R22_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_R22_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_R22_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 to 2 );
  signal \NLW_R22_carry__1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal NLW_R6_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_R7_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_R8_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_R9_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \TDMS_encoder_red/encoded[9]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \encoded[8]_i_2__1\ : label is "soft_lutpair1";
begin
  \encoded_reg[8]_9\ <= \^encoded_reg[8]_9\;
  \encoded_reg[9]\ <= \^encoded_reg[9]\;
R10_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \encoded_reg[8]_3\(0),
      CO(2) => R10_carry_n_1,
      CO(1) => R10_carry_n_2,
      CO(0) => R10_carry_n_3,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => NLW_R10_carry_O_UNCONNECTED(3 downto 0),
      S(3 downto 0) => \trigger_volt_reg[9]\(3 downto 0)
    );
R13_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \encoded_reg[8]_2\(0),
      CO(2) => R13_carry_n_1,
      CO(1) => R13_carry_n_2,
      CO(0) => R13_carry_n_3,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => NLW_R13_carry_O_UNCONNECTED(3 downto 0),
      S(3 downto 0) => \trigger_time_reg[8]_0\(3 downto 0)
    );
R14_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \encoded_reg[8]_1\(0),
      CO(2) => R14_carry_n_1,
      CO(1) => R14_carry_n_2,
      CO(0) => R14_carry_n_3,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => NLW_R14_carry_O_UNCONNECTED(3 downto 0),
      S(3 downto 0) => \trigger_time_reg[7]\(3 downto 0)
    );
R15_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \encoded_reg[8]_0\(0),
      CO(2) => R15_carry_n_1,
      CO(1) => R15_carry_n_2,
      CO(0) => R15_carry_n_3,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => NLW_R15_carry_O_UNCONNECTED(3 downto 0),
      S(3 downto 0) => \trigger_time_reg[8]\(3 downto 0)
    );
R16_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \encoded_reg[8]\(0),
      CO(2) => R16_carry_n_1,
      CO(1) => R16_carry_n_2,
      CO(0) => R16_carry_n_3,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => NLW_R16_carry_O_UNCONNECTED(3 downto 0),
      S(3 downto 0) => \trigger_time_reg[0]\(3 downto 0)
    );
R17_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => CO(0),
      CO(2) => R17_carry_n_1,
      CO(1) => R17_carry_n_2,
      CO(0) => R17_carry_n_3,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => NLW_R17_carry_O_UNCONNECTED(3 downto 0),
      S(3 downto 0) => S(3 downto 0)
    );
\R21_inferred__0/i___20_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \R21_inferred__0/i___20_carry_n_0\,
      CO(2) => \R21_inferred__0/i___20_carry_n_1\,
      CO(1) => \R21_inferred__0/i___20_carry_n_2\,
      CO(0) => \R21_inferred__0/i___20_carry_n_3\,
      CYINIT => '1',
      DI(3 downto 1) => \processCol_reg[9]\(4 downto 2),
      DI(0) => '1',
      O(3) => \R21_inferred__0/i___20_carry_n_4\,
      O(2) => \R21_inferred__0/i___20_carry_n_5\,
      O(1) => \R21_inferred__0/i___20_carry_n_6\,
      O(0) => \R21_inferred__0/i___20_carry_n_7\,
      S(3) => \i___20_carry_i_1_n_0\,
      S(2) => \i___20_carry_i_2_n_0\,
      S(1) => \i___20_carry_i_3_n_0\,
      S(0) => \i___20_carry_i_4_n_0\
    );
\R21_inferred__0/i___20_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \R21_inferred__0/i___20_carry_n_0\,
      CO(3 downto 1) => \NLW_R21_inferred__0/i___20_carry__0_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \R21_inferred__0/i___20_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \processCol_reg[9]\(5),
      O(3 downto 2) => \NLW_R21_inferred__0/i___20_carry__0_O_UNCONNECTED\(3 downto 2),
      O(1) => \R21_inferred__0/i___20_carry__0_n_6\,
      O(0) => \R21_inferred__0/i___20_carry__0_n_7\,
      S(3 downto 2) => B"00",
      S(1) => \i___20_carry__0_i_1_n_0\,
      S(0) => \i___20_carry__0_i_2_n_0\
    );
\R21_inferred__0/i__carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \R21_inferred__0/i__carry_n_0\,
      CO(2) => \R21_inferred__0/i__carry_n_1\,
      CO(1) => \R21_inferred__0/i__carry_n_2\,
      CO(0) => \R21_inferred__0/i__carry_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => \processCol_reg[9]\(6 downto 4),
      DI(0) => '0',
      O(3 downto 0) => \NLW_R21_inferred__0/i__carry_O_UNCONNECTED\(3 downto 0),
      S(3) => \i__carry_i_1__0_n_0\,
      S(2) => \i__carry_i_2_n_0\,
      S(1) => \i__carry_i_3_n_0\,
      S(0) => \processCol_reg[9]\(3)
    );
\R21_inferred__0/i__carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \R21_inferred__0/i__carry_n_0\,
      CO(3) => \R21_inferred__0/i__carry__0_n_0\,
      CO(2) => \R21_inferred__0/i__carry__0_n_1\,
      CO(1) => \R21_inferred__0/i__carry__0_n_2\,
      CO(0) => \R21_inferred__0/i__carry__0_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2 downto 0) => \processCol_reg[9]\(9 downto 7),
      O(3) => \R21_inferred__0/i__carry__0_n_4\,
      O(2 downto 0) => \NLW_R21_inferred__0/i__carry__0_O_UNCONNECTED\(2 downto 0),
      S(3) => \processCol_reg[9]\(6),
      S(2) => \i__carry__0_i_1_n_0\,
      S(1) => \i__carry__0_i_2_n_0\,
      S(0) => \i__carry__0_i_3_n_0\
    );
\R21_inferred__0/i__carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \R21_inferred__0/i__carry__0_n_0\,
      CO(3) => \R21_inferred__0/i__carry__1_n_0\,
      CO(2) => \NLW_R21_inferred__0/i__carry__1_CO_UNCONNECTED\(2),
      CO(1) => \R21_inferred__0/i__carry__1_n_2\,
      CO(0) => \R21_inferred__0/i__carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \NLW_R21_inferred__0/i__carry__1_O_UNCONNECTED\(3),
      O(2) => \R21_inferred__0/i__carry__1_n_5\,
      O(1) => \R21_inferred__0/i__carry__1_n_6\,
      O(0) => \R21_inferred__0/i__carry__1_n_7\,
      S(3) => '1',
      S(2 downto 0) => \processCol_reg[9]\(9 downto 7)
    );
\R22__23_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3 downto 2) => \NLW_R22__23_carry_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \R22__23_carry_n_2\,
      CO(0) => \R22__23_carry_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => \R22__23_carry_i_1_n_0\,
      DI(0) => '0',
      O(3) => \NLW_R22__23_carry_O_UNCONNECTED\(3),
      O(2) => \R22__23_carry_n_5\,
      O(1) => \R22__23_carry_n_6\,
      O(0) => \R22__23_carry_n_7\,
      S(3) => '0',
      S(2) => \R22__23_carry_i_2_n_0\,
      S(1) => \R22__23_carry_i_3_n_0\,
      S(0) => \R22__23_carry_i_4_n_0\
    );
\R22__23_carry_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \R22_carry__1_n_5\,
      I1 => \R22_carry__0_n_4\,
      O => \R22__23_carry_i_1_n_0\
    );
\R22__23_carry_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"D22D"
    )
        port map (
      I0 => \R22_carry__1_n_0\,
      I1 => \R22_carry__1_n_7\,
      I2 => \R22_carry__1_n_6\,
      I3 => \R22_carry__0_n_4\,
      O => \R22__23_carry_i_2_n_0\
    );
\R22__23_carry_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2DD2"
    )
        port map (
      I0 => \R22_carry__0_n_4\,
      I1 => \R22_carry__1_n_5\,
      I2 => \R22_carry__1_n_0\,
      I3 => \R22_carry__1_n_7\,
      O => \R22__23_carry_i_3_n_0\
    );
\R22__23_carry_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \R22_carry__1_n_5\,
      I1 => \R22_carry__0_n_4\,
      O => \R22__23_carry_i_4_n_0\
    );
\R22__29_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \R22__29_carry_n_0\,
      CO(2) => \R22__29_carry_n_1\,
      CO(1) => \R22__29_carry_n_2\,
      CO(0) => \R22__29_carry_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => Q(3 downto 0),
      O(3) => \R22__29_carry_n_4\,
      O(2) => \R22__29_carry_n_5\,
      O(1) => \R22__29_carry_n_6\,
      O(0) => \R22__29_carry_n_7\,
      S(3) => \R22__29_carry_i_1_n_0\,
      S(2) => \R22__29_carry_i_2_n_0\,
      S(1) => \R22__29_carry_i_3_n_0\,
      S(0) => \processRow_reg[0]\(0)
    );
\R22__29_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \R22__29_carry_n_0\,
      CO(3 downto 2) => \NLW_R22__29_carry__0_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \R22__29_carry__0_n_2\,
      CO(0) => \R22__29_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1 downto 0) => Q(5 downto 4),
      O(3) => \NLW_R22__29_carry__0_O_UNCONNECTED\(3),
      O(2) => \R22__29_carry__0_n_5\,
      O(1) => \R22__29_carry__0_n_6\,
      O(0) => \R22__29_carry__0_n_7\,
      S(3) => '0',
      S(2) => \R22__29_carry__0_i_1_n_0\,
      S(1) => \R22__29_carry__0_i_2_n_0\,
      S(0) => \R22__29_carry__0_i_3_n_0\
    );
\R22__29_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => Q(6),
      I1 => \R22__23_carry_n_5\,
      O => \R22__29_carry__0_i_1_n_0\
    );
\R22__29_carry__0_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => Q(5),
      I1 => \R22__23_carry_n_6\,
      O => \R22__29_carry__0_i_2_n_0\
    );
\R22__29_carry__0_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => Q(4),
      I1 => \R22__23_carry_n_7\,
      O => \R22__29_carry__0_i_3_n_0\
    );
\R22__29_carry_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => Q(3),
      I1 => \R22_carry__1_n_6\,
      O => \R22__29_carry_i_1_n_0\
    );
\R22__29_carry_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => Q(2),
      I1 => \R22_carry__1_n_7\,
      O => \R22__29_carry_i_2_n_0\
    );
\R22__29_carry_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => Q(1),
      I1 => \R22_carry__0_n_4\,
      O => \R22__29_carry_i_3_n_0\
    );
R22_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => R22_carry_n_0,
      CO(2) => R22_carry_n_1,
      CO(1) => R22_carry_n_2,
      CO(0) => R22_carry_n_3,
      CYINIT => '0',
      DI(3 downto 1) => Q(4 downto 2),
      DI(0) => '0',
      O(3 downto 0) => NLW_R22_carry_O_UNCONNECTED(3 downto 0),
      S(3) => R22_carry_i_1_n_0,
      S(2) => R22_carry_i_2_n_0,
      S(1) => R22_carry_i_3_n_0,
      S(0) => Q(1)
    );
\R22_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => R22_carry_n_0,
      CO(3) => \R22_carry__0_n_0\,
      CO(2) => \R22_carry__0_n_1\,
      CO(1) => \R22_carry__0_n_2\,
      CO(0) => \R22_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => Q(8 downto 5),
      O(3) => \R22_carry__0_n_4\,
      O(2 downto 0) => \NLW_R22_carry__0_O_UNCONNECTED\(2 downto 0),
      S(3) => \R22_carry__0_i_1_n_0\,
      S(2) => \R22_carry__0_i_2_n_0\,
      S(1) => \R22_carry__0_i_3_n_0\,
      S(0) => \R22_carry__0_i_4_n_0\
    );
\R22_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Q(8),
      I1 => Q(6),
      O => \R22_carry__0_i_1_n_0\
    );
\R22_carry__0_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Q(7),
      I1 => Q(5),
      O => \R22_carry__0_i_2_n_0\
    );
\R22_carry__0_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Q(6),
      I1 => Q(4),
      O => \R22_carry__0_i_3_n_0\
    );
\R22_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Q(5),
      I1 => Q(3),
      O => \R22_carry__0_i_4_n_0\
    );
\R22_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \R22_carry__0_n_0\,
      CO(3) => \R22_carry__1_n_0\,
      CO(2) => \NLW_R22_carry__1_CO_UNCONNECTED\(2),
      CO(1) => \R22_carry__1_n_2\,
      CO(0) => \R22_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => Q(9),
      O(3) => \NLW_R22_carry__1_O_UNCONNECTED\(3),
      O(2) => \R22_carry__1_n_5\,
      O(1) => \R22_carry__1_n_6\,
      O(0) => \R22_carry__1_n_7\,
      S(3) => '1',
      S(2 downto 1) => Q(9 downto 8),
      S(0) => \R22_carry__1_i_1_n_0\
    );
\R22_carry__1_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Q(9),
      I1 => Q(7),
      O => \R22_carry__1_i_1_n_0\
    );
R22_carry_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Q(4),
      I1 => Q(2),
      O => R22_carry_i_1_n_0
    );
R22_carry_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Q(3),
      I1 => Q(1),
      O => R22_carry_i_2_n_0
    );
R22_carry_i_3: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Q(2),
      I1 => Q(0),
      O => R22_carry_i_3_n_0
    );
R6_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \encoded_reg[8]_7\(0),
      CO(2) => R6_carry_n_1,
      CO(1) => R6_carry_n_2,
      CO(0) => R6_carry_n_3,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => NLW_R6_carry_O_UNCONNECTED(3 downto 0),
      S(3 downto 0) => \trigger_volt_reg[7]_0\(3 downto 0)
    );
R7_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \encoded_reg[8]_6\(0),
      CO(2) => R7_carry_n_1,
      CO(1) => R7_carry_n_2,
      CO(0) => R7_carry_n_3,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => NLW_R7_carry_O_UNCONNECTED(3 downto 0),
      S(3 downto 0) => \trigger_volt_reg[7]\(3 downto 0)
    );
R8_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \encoded_reg[8]_5\(0),
      CO(2) => R8_carry_n_1,
      CO(1) => R8_carry_n_2,
      CO(0) => R8_carry_n_3,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => NLW_R8_carry_O_UNCONNECTED(3 downto 0),
      S(3 downto 0) => \trigger_volt_reg[8]\(3 downto 0)
    );
R9_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \encoded_reg[8]_4\(0),
      CO(2) => R9_carry_n_1,
      CO(1) => R9_carry_n_2,
      CO(0) => R9_carry_n_3,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => NLW_R9_carry_O_UNCONNECTED(3 downto 0),
      S(3 downto 0) => \trigger_volt_reg[9]_0\(3 downto 0)
    );
\TDMS_encoder_blue/encoded[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00C3AAC355C355C3"
    )
        port map (
      I0 => \dc_bias_reg[3]\(1),
      I1 => \processRow_reg[4]_0\,
      I2 => \processCol_reg[8]\,
      I3 => \processCol_reg[6]\,
      I4 => \dc_bias_reg[3]_0\,
      I5 => \^encoded_reg[8]_9\,
      O => \encoded_reg[9]_0\
    );
\TDMS_encoder_green/encoded[9]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D7"
    )
        port map (
      I0 => \processCol_reg[6]\,
      I1 => \dc_bias_reg[3]_1\(0),
      I2 => \^encoded_reg[9]\,
      O => \encoded_reg[9]_1\
    );
\TDMS_encoder_red/encoded[9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7777D077"
    )
        port map (
      I0 => \processCol_reg[6]\,
      I1 => \dc_bias_reg[3]_2\(0),
      I2 => \dc_bias_reg[1]_0\,
      I3 => \^encoded_reg[8]_9\,
      I4 => \processCol_reg[0]\,
      O => \encoded_reg[9]_2\
    );
\dc_bias[3]_i_11\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFD"
    )
        port map (
      I0 => \processRow_reg[7]_1\,
      I1 => \dc_bias[3]_i_23_n_0\,
      I2 => \R21_inferred__0/i___20_carry_n_5\,
      I3 => \R21_inferred__0/i___20_carry__0_n_7\,
      I4 => \R21_inferred__0/i___20_carry_n_7\,
      O => \dc_bias[3]_i_11_n_0\
    );
\dc_bias[3]_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000200200"
    )
        port map (
      I0 => \processCol_reg[9]_0\,
      I1 => \dc_bias[3]_i_24_n_0\,
      I2 => \R22__29_carry_n_6\,
      I3 => \R22__29_carry__0_n_7\,
      I4 => \R22__29_carry__0_n_5\,
      I5 => \processRow_reg[9]_1\,
      O => \dc_bias[3]_i_12_n_0\
    );
\dc_bias[3]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000D00"
    )
        port map (
      I0 => \processCol_reg[1]\,
      I1 => \processCol_reg[2]\,
      I2 => \dc_bias[3]_i_12_n_0\,
      I3 => \dc_bias[3]_i_11_n_0\,
      I4 => \processRow_reg[2]\,
      I5 => \processRow_reg[7]_0\,
      O => \^encoded_reg[9]\
    );
\dc_bias[3]_i_23\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FBBF"
    )
        port map (
      I0 => \processCol_reg[9]\(0),
      I1 => \R21_inferred__0/i___20_carry_n_4\,
      I2 => \R21_inferred__0/i___20_carry__0_n_6\,
      I3 => \R21_inferred__0/i___20_carry_n_6\,
      O => \dc_bias[3]_i_23_n_0\
    );
\dc_bias[3]_i_24\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFD"
    )
        port map (
      I0 => \R22__29_carry_n_5\,
      I1 => \R22__29_carry_n_4\,
      I2 => \R22__29_carry__0_n_6\,
      I3 => \R22__29_carry_n_7\,
      O => \dc_bias[3]_i_24_n_0\
    );
\dc_bias[3]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"04000404"
    )
        port map (
      I0 => \processRow_reg[2]\,
      I1 => \dc_bias[3]_i_11_n_0\,
      I2 => \dc_bias[3]_i_12_n_0\,
      I3 => \processCol_reg[2]\,
      I4 => \processCol_reg[1]\,
      O => \^encoded_reg[8]_9\
    );
\encoded[8]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00007F00"
    )
        port map (
      I0 => \processRow_reg[7]\,
      I1 => switch(1),
      I2 => \processRow_reg[9]\(0),
      I3 => \^encoded_reg[8]_9\,
      I4 => \dc_bias_reg[1]_1\,
      O => \encoded_reg[8]_11\
    );
\encoded[8]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000015550000"
    )
        port map (
      I0 => \processRow_reg[4]\,
      I1 => \processRow_reg[9]_0\(0),
      I2 => switch(0),
      I3 => \processRow_reg[7]\,
      I4 => \^encoded_reg[8]_9\,
      I5 => \dc_bias_reg[1]_0\,
      O => encoded0_in(0)
    );
\encoded[8]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EEEEEAEEAAAAA2AA"
    )
        port map (
      I0 => \processCol_reg[8]\,
      I1 => \processCol_reg[6]\,
      I2 => \dc_bias_reg[3]\(0),
      I3 => \dc_bias_reg[1]\,
      I4 => \dc_bias_reg[3]\(1),
      I5 => \^encoded_reg[8]_9\,
      O => \encoded_reg[8]_8\
    );
\encoded[8]_i_2__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"44C4"
    )
        port map (
      I0 => \processCol_reg[6]\,
      I1 => \dc_bias_reg[1]_0\,
      I2 => \^encoded_reg[8]_9\,
      I3 => \processCol_reg[0]\,
      O => \encoded_reg[8]_10\
    );
\i___20_carry__0_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF0100FE00FEFF01"
    )
        port map (
      I0 => \R21_inferred__0/i__carry__1_n_5\,
      I1 => \R21_inferred__0/i__carry__1_n_7\,
      I2 => \R21_inferred__0/i__carry__1_n_6\,
      I3 => \R21_inferred__0/i__carry__0_n_4\,
      I4 => \R21_inferred__0/i__carry__1_n_0\,
      I5 => \processCol_reg[9]\(6),
      O => \i___20_carry__0_i_1_n_0\
    );
\i___20_carry__0_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"5556AAA9"
    )
        port map (
      I0 => \processCol_reg[9]\(5),
      I1 => \R21_inferred__0/i__carry__1_n_7\,
      I2 => \R21_inferred__0/i__carry__0_n_4\,
      I3 => \R21_inferred__0/i__carry__1_n_6\,
      I4 => \R21_inferred__0/i__carry__1_n_5\,
      O => \i___20_carry__0_i_2_n_0\
    );
\i___20_carry_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"56A9"
    )
        port map (
      I0 => \processCol_reg[9]\(4),
      I1 => \R21_inferred__0/i__carry__0_n_4\,
      I2 => \R21_inferred__0/i__carry__1_n_7\,
      I3 => \R21_inferred__0/i__carry__1_n_6\,
      O => \i___20_carry_i_1_n_0\
    );
\i___20_carry_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \processCol_reg[9]\(3),
      I1 => \R21_inferred__0/i__carry__1_n_7\,
      I2 => \R21_inferred__0/i__carry__0_n_4\,
      O => \i___20_carry_i_2_n_0\
    );
\i___20_carry_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \processCol_reg[9]\(2),
      I1 => \R21_inferred__0/i__carry__0_n_4\,
      O => \i___20_carry_i_3_n_0\
    );
\i___20_carry_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \processCol_reg[9]\(1),
      O => \i___20_carry_i_4_n_0\
    );
\i__carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \processCol_reg[9]\(9),
      I1 => \processCol_reg[9]\(5),
      O => \i__carry__0_i_1_n_0\
    );
\i__carry__0_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \processCol_reg[9]\(8),
      I1 => \processCol_reg[9]\(4),
      O => \i__carry__0_i_2_n_0\
    );
\i__carry__0_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \processCol_reg[9]\(7),
      I1 => \processCol_reg[9]\(3),
      O => \i__carry__0_i_3_n_0\
    );
\i__carry_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \processCol_reg[9]\(6),
      I1 => \processCol_reg[9]\(2),
      O => \i__carry_i_1__0_n_0\
    );
\i__carry_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \processCol_reg[9]\(5),
      I1 => \processCol_reg[9]\(1),
      O => \i__carry_i_2_n_0\
    );
\i__carry_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \processCol_reg[9]\(4),
      I1 => \processCol_reg[9]\(0),
      O => \i__carry_i_3_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_TDMS_encoder is
  port (
    D : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \encoded_reg[9]_0\ : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \encoded_reg[8]_0\ : out STD_LOGIC;
    \dc_bias_reg[2]_0\ : in STD_LOGIC;
    CLK : in STD_LOGIC;
    \dc_bias_reg[3]_0\ : in STD_LOGIC;
    \dc_bias_reg[3]_1\ : in STD_LOGIC;
    \dc_bias_reg[3]_2\ : in STD_LOGIC;
    \processRow_reg[2]\ : in STD_LOGIC;
    \processCol_reg[6]\ : in STD_LOGIC;
    SR : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_TDMS_encoder;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_TDMS_encoder is
  signal \^q\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \dc_bias[0]_i_1__1_n_0\ : STD_LOGIC;
  signal \dc_bias[1]_i_1_n_0\ : STD_LOGIC;
  signal \dc_bias[2]_i_1_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_1_n_0\ : STD_LOGIC;
  signal \dc_bias_reg_n_0_[0]\ : STD_LOGIC;
  signal \dc_bias_reg_n_0_[1]\ : STD_LOGIC;
  signal \encoded[8]_i_1_n_0\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \dc_bias[0]_i_1__1\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \dc_bias[1]_i_1\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \dc_bias[2]_i_1\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \dc_bias[3]_i_1\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \encoded[8]_i_4__0\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \encoded[9]_i_3\ : label is "soft_lutpair27";
begin
  Q(1 downto 0) <= \^q\(1 downto 0);
\dc_bias[0]_i_1__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(1),
      I1 => \dc_bias_reg_n_0_[0]\,
      O => \dc_bias[0]_i_1__1_n_0\
    );
\dc_bias[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"69AA"
    )
        port map (
      I0 => \dc_bias_reg_n_0_[1]\,
      I1 => \dc_bias_reg_n_0_[0]\,
      I2 => \processRow_reg[2]\,
      I3 => \^q\(1),
      O => \dc_bias[1]_i_1_n_0\
    );
\dc_bias[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"807F02FD"
    )
        port map (
      I0 => \^q\(1),
      I1 => \dc_bias_reg_n_0_[1]\,
      I2 => \dc_bias_reg_n_0_[0]\,
      I3 => \^q\(0),
      I4 => \processRow_reg[2]\,
      O => \dc_bias[2]_i_1_n_0\
    );
\dc_bias[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"070F0F2F"
    )
        port map (
      I0 => \^q\(1),
      I1 => \processRow_reg[2]\,
      I2 => \^q\(0),
      I3 => \dc_bias_reg_n_0_[0]\,
      I4 => \dc_bias_reg_n_0_[1]\,
      O => \dc_bias[3]_i_1_n_0\
    );
\dc_bias_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => \dc_bias[0]_i_1__1_n_0\,
      Q => \dc_bias_reg_n_0_[0]\,
      R => SR(0)
    );
\dc_bias_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => \dc_bias[1]_i_1_n_0\,
      Q => \dc_bias_reg_n_0_[1]\,
      R => SR(0)
    );
\dc_bias_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => \dc_bias[2]_i_1_n_0\,
      Q => \^q\(0),
      R => SR(0)
    );
\dc_bias_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => \dc_bias[3]_i_1_n_0\,
      Q => \^q\(1),
      R => SR(0)
    );
\encoded[8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000200000000"
    )
        port map (
      I0 => \processRow_reg[2]\,
      I1 => \^q\(1),
      I2 => \dc_bias_reg_n_0_[1]\,
      I3 => \dc_bias_reg_n_0_[0]\,
      I4 => \^q\(0),
      I5 => \processCol_reg[6]\,
      O => \encoded[8]_i_1_n_0\
    );
\encoded[8]_i_4__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \dc_bias_reg_n_0_[1]\,
      I1 => \dc_bias_reg_n_0_[0]\,
      O => \encoded_reg[8]_0\
    );
\encoded[9]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => \^q\(1),
      I1 => \dc_bias_reg_n_0_[1]\,
      I2 => \dc_bias_reg_n_0_[0]\,
      I3 => \^q\(0),
      O => \encoded_reg[9]_0\
    );
\encoded_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \dc_bias_reg[3]_1\,
      Q => D(0),
      R => '0'
    );
\encoded_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \dc_bias_reg[3]_0\,
      Q => D(1),
      R => '0'
    );
\encoded_reg[8]\: unisim.vcomponents.FDSE
     port map (
      C => CLK,
      CE => '1',
      D => \dc_bias_reg[2]_0\,
      Q => D(2),
      S => \encoded[8]_i_1_n_0\
    );
\encoded_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \dc_bias_reg[3]_2\,
      Q => D(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_TDMS_encoder_0 is
  port (
    D : out STD_LOGIC_VECTOR ( 3 downto 0 );
    Q : out STD_LOGIC_VECTOR ( 0 to 0 );
    \encoded_reg[8]_0\ : out STD_LOGIC;
    \dc_bias_reg[1]_0\ : in STD_LOGIC;
    CLK : in STD_LOGIC;
    \dc_bias_reg[3]_0\ : in STD_LOGIC;
    \processCol_reg[6]\ : in STD_LOGIC;
    \processCol_reg[1]\ : in STD_LOGIC;
    SR : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_TDMS_encoder_0 : entity is "TDMS_encoder";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_TDMS_encoder_0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_TDMS_encoder_0 is
  signal \^q\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \dc_bias[0]_i_1__0_n_0\ : STD_LOGIC;
  signal \dc_bias[1]_i_1__0_n_0\ : STD_LOGIC;
  signal \dc_bias[2]_i_1__0_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_1__0_n_0\ : STD_LOGIC;
  signal \dc_bias_reg_n_0_[0]\ : STD_LOGIC;
  signal \dc_bias_reg_n_0_[1]\ : STD_LOGIC;
  signal \dc_bias_reg_n_0_[2]\ : STD_LOGIC;
  signal \encoded[0]_i_1__0_n_0\ : STD_LOGIC;
  signal \encoded[2]_i_1__0_n_0\ : STD_LOGIC;
  signal \encoded[8]_i_2__0_n_0\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \dc_bias[0]_i_1__0\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \dc_bias[1]_i_1__0\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \dc_bias[2]_i_1__0\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \dc_bias[3]_i_1__0\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \encoded[0]_i_1__0\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \encoded[8]_i_4\ : label is "soft_lutpair30";
begin
  Q(0) <= \^q\(0);
\dc_bias[0]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(0),
      I1 => \dc_bias_reg_n_0_[0]\,
      O => \dc_bias[0]_i_1__0_n_0\
    );
\dc_bias[1]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"69AA"
    )
        port map (
      I0 => \dc_bias_reg_n_0_[1]\,
      I1 => \dc_bias_reg_n_0_[0]\,
      I2 => \processCol_reg[1]\,
      I3 => \^q\(0),
      O => \dc_bias[1]_i_1__0_n_0\
    );
\dc_bias[2]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"870F0F2D"
    )
        port map (
      I0 => \^q\(0),
      I1 => \processCol_reg[1]\,
      I2 => \dc_bias_reg_n_0_[2]\,
      I3 => \dc_bias_reg_n_0_[1]\,
      I4 => \dc_bias_reg_n_0_[0]\,
      O => \dc_bias[2]_i_1__0_n_0\
    );
\dc_bias[3]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00027FFF"
    )
        port map (
      I0 => \^q\(0),
      I1 => \processCol_reg[1]\,
      I2 => \dc_bias_reg_n_0_[1]\,
      I3 => \dc_bias_reg_n_0_[0]\,
      I4 => \dc_bias_reg_n_0_[2]\,
      O => \dc_bias[3]_i_1__0_n_0\
    );
\dc_bias_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => \dc_bias[0]_i_1__0_n_0\,
      Q => \dc_bias_reg_n_0_[0]\,
      R => SR(0)
    );
\dc_bias_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => \dc_bias[1]_i_1__0_n_0\,
      Q => \dc_bias_reg_n_0_[1]\,
      R => SR(0)
    );
\dc_bias_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => \dc_bias[2]_i_1__0_n_0\,
      Q => \dc_bias_reg_n_0_[2]\,
      R => SR(0)
    );
\dc_bias_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => \dc_bias[3]_i_1__0_n_0\,
      Q => \^q\(0),
      R => SR(0)
    );
\encoded[0]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^q\(0),
      I1 => \processCol_reg[6]\,
      O => \encoded[0]_i_1__0_n_0\
    );
\encoded[2]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \^q\(0),
      I1 => \processCol_reg[6]\,
      O => \encoded[2]_i_1__0_n_0\
    );
\encoded[8]_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFEFF00FF00FF"
    )
        port map (
      I0 => \dc_bias_reg_n_0_[1]\,
      I1 => \dc_bias_reg_n_0_[0]\,
      I2 => \dc_bias_reg_n_0_[2]\,
      I3 => \processCol_reg[6]\,
      I4 => \^q\(0),
      I5 => \processCol_reg[1]\,
      O => \encoded[8]_i_2__0_n_0\
    );
\encoded[8]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFEFF"
    )
        port map (
      I0 => \dc_bias_reg_n_0_[1]\,
      I1 => \dc_bias_reg_n_0_[0]\,
      I2 => \dc_bias_reg_n_0_[2]\,
      I3 => \processCol_reg[6]\,
      I4 => \^q\(0),
      O => \encoded_reg[8]_0\
    );
\encoded_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \encoded[0]_i_1__0_n_0\,
      Q => D(0),
      R => '0'
    );
\encoded_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \encoded[2]_i_1__0_n_0\,
      Q => D(1),
      R => '0'
    );
\encoded_reg[8]\: unisim.vcomponents.FDSE
     port map (
      C => CLK,
      CE => '1',
      D => \encoded[8]_i_2__0_n_0\,
      Q => D(2),
      S => \dc_bias_reg[1]_0\
    );
\encoded_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \dc_bias_reg[3]_0\,
      Q => D(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_TDMS_encoder_1 is
  port (
    D : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \encoded_reg[8]_0\ : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 0 to 0 );
    \encoded_reg[8]_1\ : out STD_LOGIC;
    \encoded_reg[8]_2\ : out STD_LOGIC;
    \encoded_reg[8]_3\ : out STD_LOGIC;
    \encoded_reg[8]_4\ : out STD_LOGIC;
    \encoded_reg[8]_5\ : out STD_LOGIC;
    \encoded_reg[8]_6\ : out STD_LOGIC;
    \encoded_reg[8]_7\ : out STD_LOGIC;
    \encoded_reg[8]_8\ : out STD_LOGIC;
    \encoded_reg[8]_9\ : out STD_LOGIC;
    \encoded_reg[8]_10\ : out STD_LOGIC;
    \encoded_reg[8]_11\ : out STD_LOGIC;
    \encoded_reg[8]_12\ : out STD_LOGIC;
    \encoded_reg[8]_13\ : out STD_LOGIC;
    \encoded_reg[8]_14\ : out STD_LOGIC;
    \encoded_reg[8]_15\ : out STD_LOGIC;
    \encoded_reg[8]_16\ : out STD_LOGIC;
    encoded0_in : in STD_LOGIC_VECTOR ( 0 to 0 );
    \processCol_reg[6]\ : in STD_LOGIC;
    CLK : in STD_LOGIC;
    \dc_bias_reg[3]_0\ : in STD_LOGIC;
    \dc_bias_reg[3]_1\ : in STD_LOGIC;
    \trigger_volt_reg[6]\ : in STD_LOGIC_VECTOR ( 6 downto 0 );
    \processCol_reg[6]_0\ : in STD_LOGIC;
    \trigger_time_reg[7]\ : in STD_LOGIC;
    \trigger_time_reg[6]\ : in STD_LOGIC;
    \trigger_time_reg[5]\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \trigger_time_reg[3]\ : in STD_LOGIC;
    \trigger_time_reg[2]\ : in STD_LOGIC;
    \trigger_time_reg[4]\ : in STD_LOGIC;
    \processCol_reg[0]\ : in STD_LOGIC;
    \processRow_reg[2]\ : in STD_LOGIC;
    SR : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_TDMS_encoder_1 : entity is "TDMS_encoder";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_TDMS_encoder_1;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_TDMS_encoder_1 is
  signal \^q\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal R15_carry_i_10_n_0 : STD_LOGIC;
  signal \dc_bias[0]_i_1_n_0\ : STD_LOGIC;
  signal \dc_bias[1]_i_1__1_n_0\ : STD_LOGIC;
  signal \dc_bias[2]_i_1__1_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_2__0_n_0\ : STD_LOGIC;
  signal \dc_bias_reg_n_0_[0]\ : STD_LOGIC;
  signal \dc_bias_reg_n_0_[1]\ : STD_LOGIC;
  signal \dc_bias_reg_n_0_[2]\ : STD_LOGIC;
  signal \encoded[2]_i_1__1_n_0\ : STD_LOGIC;
  signal \^encoded_reg[8]_2\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of R13_carry_i_5 : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of R14_carry_i_5 : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of R14_carry_i_7 : label is "soft_lutpair37";
  attribute SOFT_HLUTNM of R15_carry_i_10 : label is "soft_lutpair37";
  attribute SOFT_HLUTNM of R16_carry_i_5 : label is "soft_lutpair36";
  attribute SOFT_HLUTNM of R16_carry_i_6 : label is "soft_lutpair36";
  attribute SOFT_HLUTNM of R6_carry_i_5 : label is "soft_lutpair35";
  attribute SOFT_HLUTNM of R7_carry_i_6 : label is "soft_lutpair35";
  attribute SOFT_HLUTNM of R7_carry_i_8 : label is "soft_lutpair38";
  attribute SOFT_HLUTNM of R8_carry_i_9 : label is "soft_lutpair38";
  attribute SOFT_HLUTNM of \dc_bias[0]_i_1\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of \dc_bias[1]_i_1__1\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of \encoded[2]_i_1__1\ : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of \encoded[8]_i_6__0\ : label is "soft_lutpair34";
begin
  Q(0) <= \^q\(0);
  \encoded_reg[8]_2\ <= \^encoded_reg[8]_2\;
R13_carry_i_5: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => \trigger_time_reg[5]\(2),
      I1 => \trigger_time_reg[4]\,
      I2 => \trigger_time_reg[2]\,
      I3 => \trigger_time_reg[5]\(1),
      I4 => \trigger_time_reg[3]\,
      O => \encoded_reg[8]_6\
    );
R13_carry_i_6: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \trigger_time_reg[5]\(1),
      I1 => \trigger_time_reg[2]\,
      O => \encoded_reg[8]_12\
    );
R14_carry_i_5: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80000000"
    )
        port map (
      I0 => \trigger_time_reg[4]\,
      I1 => \trigger_time_reg[2]\,
      I2 => \trigger_time_reg[5]\(1),
      I3 => \trigger_time_reg[3]\,
      I4 => \trigger_time_reg[5]\(2),
      O => \^encoded_reg[8]_2\
    );
R14_carry_i_7: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \trigger_time_reg[5]\(1),
      I1 => \trigger_time_reg[2]\,
      O => \encoded_reg[8]_10\
    );
R15_carry_i_10: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => \trigger_time_reg[3]\,
      I1 => \trigger_time_reg[5]\(1),
      I2 => \trigger_time_reg[2]\,
      I3 => \trigger_time_reg[4]\,
      O => R15_carry_i_10_n_0
    );
R15_carry_i_11: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => \trigger_time_reg[6]\,
      I1 => \trigger_time_reg[3]\,
      I2 => \trigger_time_reg[5]\(1),
      I3 => \trigger_time_reg[2]\,
      I4 => \trigger_time_reg[4]\,
      I5 => \trigger_time_reg[5]\(2),
      O => \encoded_reg[8]_5\
    );
R15_carry_i_5: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0004"
    )
        port map (
      I0 => \trigger_time_reg[5]\(2),
      I1 => R15_carry_i_10_n_0,
      I2 => \trigger_time_reg[6]\,
      I3 => \trigger_time_reg[7]\,
      O => \encoded_reg[8]_11\
    );
R16_carry_i_5: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => \trigger_time_reg[6]\,
      I1 => \^encoded_reg[8]_2\,
      I2 => \trigger_time_reg[7]\,
      O => \encoded_reg[8]_3\
    );
R16_carry_i_6: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => \trigger_time_reg[7]\,
      I1 => \^encoded_reg[8]_2\,
      I2 => \trigger_time_reg[6]\,
      I3 => \trigger_time_reg[5]\(0),
      O => \encoded_reg[8]_1\
    );
R16_carry_i_9: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => \trigger_time_reg[5]\(2),
      I1 => \trigger_time_reg[3]\,
      I2 => \trigger_time_reg[5]\(1),
      I3 => \trigger_time_reg[2]\,
      I4 => \trigger_time_reg[4]\,
      I5 => \trigger_time_reg[5]\(0),
      O => \encoded_reg[8]_4\
    );
R6_carry_i_5: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000001"
    )
        port map (
      I0 => \trigger_volt_reg[6]\(4),
      I1 => \trigger_volt_reg[6]\(2),
      I2 => \trigger_volt_reg[6]\(1),
      I3 => \trigger_volt_reg[6]\(3),
      I4 => \trigger_volt_reg[6]\(5),
      O => \encoded_reg[8]_15\
    );
R6_carry_i_6: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \trigger_volt_reg[6]\(1),
      I1 => \trigger_volt_reg[6]\(2),
      O => \encoded_reg[8]_16\
    );
R7_carry_i_6: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80000000"
    )
        port map (
      I0 => \trigger_volt_reg[6]\(4),
      I1 => \trigger_volt_reg[6]\(2),
      I2 => \trigger_volt_reg[6]\(1),
      I3 => \trigger_volt_reg[6]\(3),
      I4 => \trigger_volt_reg[6]\(5),
      O => \encoded_reg[8]_8\
    );
R7_carry_i_8: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \trigger_volt_reg[6]\(1),
      I1 => \trigger_volt_reg[6]\(2),
      O => \encoded_reg[8]_13\
    );
R8_carry_i_5: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => \trigger_volt_reg[6]\(5),
      I1 => \trigger_volt_reg[6]\(3),
      I2 => \trigger_volt_reg[6]\(1),
      I3 => \trigger_volt_reg[6]\(2),
      I4 => \trigger_volt_reg[6]\(4),
      I5 => \trigger_volt_reg[6]\(6),
      O => \encoded_reg[8]_14\
    );
R8_carry_i_9: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => \trigger_volt_reg[6]\(2),
      I1 => \trigger_volt_reg[6]\(1),
      I2 => \trigger_volt_reg[6]\(0),
      O => \encoded_reg[8]_0\
    );
R9_carry_i_5: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => \trigger_volt_reg[6]\(5),
      I1 => \trigger_volt_reg[6]\(3),
      I2 => \trigger_volt_reg[6]\(1),
      I3 => \trigger_volt_reg[6]\(2),
      I4 => \trigger_volt_reg[6]\(4),
      I5 => \trigger_volt_reg[6]\(6),
      O => \encoded_reg[8]_7\
    );
\dc_bias[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(0),
      I1 => \dc_bias_reg_n_0_[0]\,
      O => \dc_bias[0]_i_1_n_0\
    );
\dc_bias[1]_i_1__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"9699AAAA"
    )
        port map (
      I0 => \dc_bias_reg_n_0_[1]\,
      I1 => \dc_bias_reg_n_0_[0]\,
      I2 => \processCol_reg[0]\,
      I3 => \processRow_reg[2]\,
      I4 => \^q\(0),
      O => \dc_bias[1]_i_1__1_n_0\
    );
\dc_bias[2]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5955555565665555"
    )
        port map (
      I0 => \dc_bias_reg_n_0_[2]\,
      I1 => \dc_bias_reg_n_0_[1]\,
      I2 => \processCol_reg[0]\,
      I3 => \processRow_reg[2]\,
      I4 => \^q\(0),
      I5 => \dc_bias_reg_n_0_[0]\,
      O => \dc_bias[2]_i_1__1_n_0\
    );
\dc_bias[3]_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000DFFF008AFFFF"
    )
        port map (
      I0 => \^q\(0),
      I1 => \processCol_reg[0]\,
      I2 => \processRow_reg[2]\,
      I3 => \dc_bias_reg_n_0_[0]\,
      I4 => \dc_bias_reg_n_0_[2]\,
      I5 => \dc_bias_reg_n_0_[1]\,
      O => \dc_bias[3]_i_2__0_n_0\
    );
\dc_bias_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => \dc_bias[0]_i_1_n_0\,
      Q => \dc_bias_reg_n_0_[0]\,
      R => SR(0)
    );
\dc_bias_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => \dc_bias[1]_i_1__1_n_0\,
      Q => \dc_bias_reg_n_0_[1]\,
      R => SR(0)
    );
\dc_bias_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => \dc_bias[2]_i_1__1_n_0\,
      Q => \dc_bias_reg_n_0_[2]\,
      R => SR(0)
    );
\dc_bias_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => \dc_bias[3]_i_2__0_n_0\,
      Q => \^q\(0),
      R => SR(0)
    );
\encoded[2]_i_1__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \^q\(0),
      I1 => \processCol_reg[6]_0\,
      O => \encoded[2]_i_1__1_n_0\
    );
\encoded[8]_i_6__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFEFF"
    )
        port map (
      I0 => \dc_bias_reg_n_0_[1]\,
      I1 => \dc_bias_reg_n_0_[0]\,
      I2 => \dc_bias_reg_n_0_[2]\,
      I3 => \processCol_reg[6]_0\,
      I4 => \^q\(0),
      O => \encoded_reg[8]_9\
    );
\encoded_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \dc_bias_reg[3]_0\,
      Q => D(0),
      R => '0'
    );
\encoded_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \encoded[2]_i_1__1_n_0\,
      Q => D(1),
      R => '0'
    );
\encoded_reg[8]\: unisim.vcomponents.FDSE
     port map (
      C => CLK,
      CE => '1',
      D => \processCol_reg[6]\,
      Q => D(2),
      S => encoded0_in(0)
    );
\encoded_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \dc_bias_reg[3]_1\,
      Q => D(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_TWICtl is
  port (
    D : out STD_LOGIC_VECTOR ( 3 downto 0 );
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    \initA_reg[6]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    initEn_reg : out STD_LOGIC;
    scl : inout STD_LOGIC;
    sda : inout STD_LOGIC;
    clk_out2 : in STD_LOGIC;
    data_i : in STD_LOGIC_VECTOR ( 7 downto 0 );
    s00_axi_aresetn : in STD_LOGIC;
    stb : in STD_LOGIC;
    msg : in STD_LOGIC;
    \initWord_reg[6]\ : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \delaycnt_reg[20]\ : in STD_LOGIC;
    initEn_reg_0 : in STD_LOGIC;
    \initWord_reg[21]\ : in STD_LOGIC;
    \initA_reg[4]\ : in STD_LOGIC;
    initEn : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_TWICtl;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_TWICtl is
  signal \^d\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal DONE_O_i_1_n_0 : STD_LOGIC;
  signal DONE_O_i_2_n_0 : STD_LOGIC;
  signal DONE_O_i_3_n_0 : STD_LOGIC;
  signal DONE_O_i_4_n_0 : STD_LOGIC;
  signal ERR_O_i_1_n_0 : STD_LOGIC;
  signal ERR_O_i_2_n_0 : STD_LOGIC;
  signal \FSM_gray_state[0]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_gray_state[0]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_gray_state[1]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_gray_state[1]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_gray_state[2]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_gray_state[2]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_gray_state[3]_i_10_n_0\ : STD_LOGIC;
  signal \FSM_gray_state[3]_i_11_n_0\ : STD_LOGIC;
  signal \FSM_gray_state[3]_i_12_n_0\ : STD_LOGIC;
  signal \FSM_gray_state[3]_i_13_n_0\ : STD_LOGIC;
  signal \FSM_gray_state[3]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_gray_state[3]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_gray_state[3]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_gray_state[3]_i_4_n_0\ : STD_LOGIC;
  signal \FSM_gray_state[3]_i_5_n_0\ : STD_LOGIC;
  signal \FSM_gray_state[3]_i_6_n_0\ : STD_LOGIC;
  signal \FSM_gray_state[3]_i_7_n_0\ : STD_LOGIC;
  signal \FSM_gray_state[3]_i_8_n_0\ : STD_LOGIC;
  signal \FSM_gray_state[3]_i_9_n_0\ : STD_LOGIC;
  signal addrNData_i_1_n_0 : STD_LOGIC;
  signal addrNData_i_2_n_0 : STD_LOGIC;
  signal addrNData_reg_n_0 : STD_LOGIC;
  signal bitCount : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \bitCount[0]_i_1_n_0\ : STD_LOGIC;
  signal \bitCount[1]_i_1_n_0\ : STD_LOGIC;
  signal \bitCount[2]_i_1_n_0\ : STD_LOGIC;
  signal busFreeCnt0 : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal busFreeCnt0_1 : STD_LOGIC;
  signal \busFreeCnt[1]_i_1_n_0\ : STD_LOGIC;
  signal \busFreeCnt[6]_i_3_n_0\ : STD_LOGIC;
  signal \busFreeCnt_reg__0\ : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal busState0 : STD_LOGIC;
  signal \busState[0]_i_1_n_0\ : STD_LOGIC;
  signal \busState[1]_i_1_n_0\ : STD_LOGIC;
  signal \busState_reg_n_0_[0]\ : STD_LOGIC;
  signal \busState_reg_n_0_[1]\ : STD_LOGIC;
  signal dScl : STD_LOGIC;
  signal dataByte0 : STD_LOGIC;
  signal dataByte1 : STD_LOGIC;
  signal \dataByte[0]_i_1_n_0\ : STD_LOGIC;
  signal \dataByte[1]_i_1_n_0\ : STD_LOGIC;
  signal \dataByte[2]_i_1_n_0\ : STD_LOGIC;
  signal \dataByte[3]_i_1_n_0\ : STD_LOGIC;
  signal \dataByte[4]_i_1_n_0\ : STD_LOGIC;
  signal \dataByte[5]_i_1_n_0\ : STD_LOGIC;
  signal \dataByte[6]_i_1_n_0\ : STD_LOGIC;
  signal \dataByte[7]_i_1_n_0\ : STD_LOGIC;
  signal \dataByte[7]_i_2_n_0\ : STD_LOGIC;
  signal \dataByte[7]_i_5_n_0\ : STD_LOGIC;
  signal \dataByte[7]_i_6_n_0\ : STD_LOGIC;
  signal \dataByte[7]_i_7_n_0\ : STD_LOGIC;
  signal \dataByte_reg_n_0_[0]\ : STD_LOGIC;
  signal \dataByte_reg_n_0_[1]\ : STD_LOGIC;
  signal \dataByte_reg_n_0_[2]\ : STD_LOGIC;
  signal \dataByte_reg_n_0_[3]\ : STD_LOGIC;
  signal \dataByte_reg_n_0_[4]\ : STD_LOGIC;
  signal \dataByte_reg_n_0_[5]\ : STD_LOGIC;
  signal \dataByte_reg_n_0_[6]\ : STD_LOGIC;
  signal \dataByte_reg_n_0_[7]\ : STD_LOGIC;
  signal ddSda : STD_LOGIC;
  signal done : STD_LOGIC;
  signal error : STD_LOGIC;
  signal initEn_i_2_n_0 : STD_LOGIC;
  signal int_Rst : STD_LOGIC;
  signal int_Rst_i_1_n_0 : STD_LOGIC;
  signal p_0_in : STD_LOGIC_VECTOR ( 0 to 0 );
  signal rScl : STD_LOGIC;
  signal rScl_i_1_n_0 : STD_LOGIC;
  signal rSda : STD_LOGIC;
  signal rSda_i_1_n_0 : STD_LOGIC;
  signal rSda_i_2_n_0 : STD_LOGIC;
  signal rSda_i_3_n_0 : STD_LOGIC;
  signal rSda_i_4_n_0 : STD_LOGIC;
  signal rSda_i_5_n_0 : STD_LOGIC;
  signal sclCnt0 : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal sclCnt0_0 : STD_LOGIC;
  signal \sclCnt[1]_i_1_n_0\ : STD_LOGIC;
  signal \sclCnt[6]_i_2_n_0\ : STD_LOGIC;
  signal \sclCnt[6]_i_4_n_0\ : STD_LOGIC;
  signal \sclCnt_reg__0\ : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal scl_INST_0_i_1_n_0 : STD_LOGIC;
  signal sda_INST_0_i_1_n_0 : STD_LOGIC;
  signal state : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute RTL_KEEP : string;
  attribute RTL_KEEP of state : signal is "yes";
  signal \state[1]_i_2_n_0\ : STD_LOGIC;
  signal subState : STD_LOGIC;
  signal \subState[0]_i_1_n_0\ : STD_LOGIC;
  signal \subState[1]_i_1_n_0\ : STD_LOGIC;
  signal \subState[1]_i_2_n_0\ : STD_LOGIC;
  signal \subState[1]_i_3_n_0\ : STD_LOGIC;
  signal \subState_reg_n_0_[0]\ : STD_LOGIC;
  signal \subState_reg_n_0_[1]\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of DONE_O_i_3 : label is "soft_lutpair62";
  attribute SOFT_HLUTNM of \FSM_gray_state[2]_i_2\ : label is "soft_lutpair60";
  attribute SOFT_HLUTNM of \FSM_gray_state[3]_i_13\ : label is "soft_lutpair64";
  attribute SOFT_HLUTNM of \FSM_gray_state[3]_i_3\ : label is "soft_lutpair63";
  attribute SOFT_HLUTNM of \FSM_gray_state[3]_i_7\ : label is "soft_lutpair67";
  attribute SOFT_HLUTNM of \FSM_gray_state[3]_i_8\ : label is "soft_lutpair64";
  attribute SOFT_HLUTNM of \FSM_gray_state[3]_i_9\ : label is "soft_lutpair60";
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_gray_state_reg[0]\ : label is "stwrite:0000,stread:0010,ststart:0101,stidle:0001,stmnackstart:0111,stmack:0110,stsack:0011,stmnackstop:0100,ststop:1111";
  attribute KEEP : string;
  attribute KEEP of \FSM_gray_state_reg[0]\ : label is "yes";
  attribute FSM_ENCODED_STATES of \FSM_gray_state_reg[1]\ : label is "stwrite:0000,stread:0010,ststart:0101,stidle:0001,stmnackstart:0111,stmack:0110,stsack:0011,stmnackstop:0100,ststop:1111";
  attribute KEEP of \FSM_gray_state_reg[1]\ : label is "yes";
  attribute FSM_ENCODED_STATES of \FSM_gray_state_reg[2]\ : label is "stwrite:0000,stread:0010,ststart:0101,stidle:0001,stmnackstart:0111,stmack:0110,stsack:0011,stmnackstop:0100,ststop:1111";
  attribute KEEP of \FSM_gray_state_reg[2]\ : label is "yes";
  attribute FSM_ENCODED_STATES of \FSM_gray_state_reg[3]\ : label is "stwrite:0000,stread:0010,ststart:0101,stidle:0001,stmnackstart:0111,stmack:0110,stsack:0011,stmnackstop:0100,ststop:1111";
  attribute KEEP of \FSM_gray_state_reg[3]\ : label is "yes";
  attribute SOFT_HLUTNM of \bitCount[1]_i_1\ : label is "soft_lutpair58";
  attribute SOFT_HLUTNM of \bitCount[2]_i_1\ : label is "soft_lutpair58";
  attribute SOFT_HLUTNM of \busFreeCnt[0]_i_1\ : label is "soft_lutpair66";
  attribute SOFT_HLUTNM of \busFreeCnt[2]_i_1\ : label is "soft_lutpair66";
  attribute SOFT_HLUTNM of \busFreeCnt[3]_i_1\ : label is "soft_lutpair56";
  attribute SOFT_HLUTNM of \busFreeCnt[4]_i_1\ : label is "soft_lutpair56";
  attribute SOFT_HLUTNM of \dataByte[0]_i_1\ : label is "soft_lutpair62";
  attribute SOFT_HLUTNM of \dataByte[7]_i_7\ : label is "soft_lutpair67";
  attribute SOFT_HLUTNM of initEn_i_2 : label is "soft_lutpair61";
  attribute SOFT_HLUTNM of \sclCnt[1]_i_1\ : label is "soft_lutpair65";
  attribute SOFT_HLUTNM of \sclCnt[2]_i_1\ : label is "soft_lutpair65";
  attribute SOFT_HLUTNM of \sclCnt[3]_i_1\ : label is "soft_lutpair59";
  attribute SOFT_HLUTNM of \sclCnt[4]_i_1\ : label is "soft_lutpair59";
  attribute SOFT_HLUTNM of \sclCnt[6]_i_3\ : label is "soft_lutpair68";
  attribute SOFT_HLUTNM of sda_INST_0_i_1 : label is "soft_lutpair63";
  attribute SOFT_HLUTNM of \state[0]_i_1\ : label is "soft_lutpair57";
  attribute SOFT_HLUTNM of \state[1]_i_2\ : label is "soft_lutpair57";
  attribute SOFT_HLUTNM of \state[2]_i_1\ : label is "soft_lutpair61";
  attribute SOFT_HLUTNM of \subState[1]_i_2\ : label is "soft_lutpair68";
begin
  D(3 downto 0) <= \^d\(3 downto 0);
DONE_O_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCCFCCCCCCDDCCCC"
    )
        port map (
      I0 => \FSM_gray_state[3]_i_8_n_0\,
      I1 => DONE_O_i_2_n_0,
      I2 => DONE_O_i_3_n_0,
      I3 => DONE_O_i_4_n_0,
      I4 => state(1),
      I5 => state(0),
      O => DONE_O_i_1_n_0
    );
DONE_O_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0010000000000000"
    )
        port map (
      I0 => state(1),
      I1 => state(0),
      I2 => \subState[1]_i_3_n_0\,
      I3 => p_0_in(0),
      I4 => dScl,
      I5 => rSda,
      O => DONE_O_i_2_n_0
    );
DONE_O_i_3: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => addrNData_reg_n_0,
      I1 => p_0_in(0),
      O => DONE_O_i_3_n_0
    );
DONE_O_i_4: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFF7"
    )
        port map (
      I0 => \subState[1]_i_2_n_0\,
      I1 => \subState_reg_n_0_[0]\,
      I2 => \subState_reg_n_0_[1]\,
      I3 => state(2),
      I4 => state(3),
      O => DONE_O_i_4_n_0
    );
DONE_O_reg: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => '1',
      D => DONE_O_i_1_n_0,
      Q => done,
      R => '0'
    );
ERR_O_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8888F00088880000"
    )
        port map (
      I0 => ERR_O_i_2_n_0,
      I1 => state(0),
      I2 => rSda,
      I3 => dScl,
      I4 => p_0_in(0),
      I5 => \FSM_gray_state[3]_i_6_n_0\,
      O => ERR_O_i_1_n_0
    );
ERR_O_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0002000000000000"
    )
        port map (
      I0 => state(1),
      I1 => state(3),
      I2 => state(2),
      I3 => \subState_reg_n_0_[1]\,
      I4 => \subState_reg_n_0_[0]\,
      I5 => \subState[1]_i_2_n_0\,
      O => ERR_O_i_2_n_0
    );
ERR_O_reg: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => '1',
      D => ERR_O_i_1_n_0,
      Q => error,
      R => '0'
    );
\FSM_gray_state[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFF44F55FF"
    )
        port map (
      I0 => \FSM_gray_state[0]_i_2_n_0\,
      I1 => \FSM_gray_state[2]_i_2_n_0\,
      I2 => state(2),
      I3 => state(1),
      I4 => state(0),
      I5 => state(3),
      O => \FSM_gray_state[0]_i_1_n_0\
    );
\FSM_gray_state[0]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAAFBFF"
    )
        port map (
      I0 => state(2),
      I1 => msg,
      I2 => int_Rst,
      I3 => stb,
      I4 => state(0),
      O => \FSM_gray_state[0]_i_2_n_0\
    );
\FSM_gray_state[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00015555"
    )
        port map (
      I0 => state(3),
      I1 => state(0),
      I2 => state(1),
      I3 => \FSM_gray_state[3]_i_3_n_0\,
      I4 => \FSM_gray_state[1]_i_2_n_0\,
      O => \FSM_gray_state[1]_i_1_n_0\
    );
\FSM_gray_state[1]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF0FF0BFF0BFF"
    )
        port map (
      I0 => int_Rst,
      I1 => stb,
      I2 => state(2),
      I3 => state(1),
      I4 => \FSM_gray_state[3]_i_9_n_0\,
      I5 => state(0),
      O => \FSM_gray_state[1]_i_2_n_0\
    );
\FSM_gray_state[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"004100FC0041003C"
    )
        port map (
      I0 => \FSM_gray_state[3]_i_3_n_0\,
      I1 => state(0),
      I2 => state(1),
      I3 => state(3),
      I4 => state(2),
      I5 => \FSM_gray_state[2]_i_2_n_0\,
      O => \FSM_gray_state[2]_i_1_n_0\
    );
\FSM_gray_state[2]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EEEEFFEF"
    )
        port map (
      I0 => int_Rst,
      I1 => \dataByte_reg_n_0_[0]\,
      I2 => stb,
      I3 => msg,
      I4 => addrNData_reg_n_0,
      O => \FSM_gray_state[2]_i_2_n_0\
    );
\FSM_gray_state[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FCFCF8F8FFFCF8F8"
    )
        port map (
      I0 => \FSM_gray_state[3]_i_3_n_0\,
      I1 => \FSM_gray_state[3]_i_4_n_0\,
      I2 => \FSM_gray_state[3]_i_5_n_0\,
      I3 => \FSM_gray_state[3]_i_6_n_0\,
      I4 => \FSM_gray_state[3]_i_7_n_0\,
      I5 => \FSM_gray_state[3]_i_8_n_0\,
      O => \FSM_gray_state[3]_i_1_n_0\
    );
\FSM_gray_state[3]_i_10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FCCECFFF"
    )
        port map (
      I0 => \FSM_gray_state[3]_i_13_n_0\,
      I1 => state(3),
      I2 => state(2),
      I3 => state(0),
      I4 => state(1),
      O => \FSM_gray_state[3]_i_10_n_0\
    );
\FSM_gray_state[3]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DFFFFFFFFFFFFFFF"
    )
        port map (
      I0 => \subState_reg_n_0_[1]\,
      I1 => \subState_reg_n_0_[0]\,
      I2 => state(3),
      I3 => state(2),
      I4 => state(1),
      I5 => state(0),
      O => \FSM_gray_state[3]_i_11_n_0\
    );
\FSM_gray_state[3]_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AEAAAAAAAAAAAAAA"
    )
        port map (
      I0 => DONE_O_i_2_n_0,
      I1 => \busState_reg_n_0_[0]\,
      I2 => \busState_reg_n_0_[1]\,
      I3 => stb,
      I4 => subState,
      I5 => s00_axi_aresetn,
      O => \FSM_gray_state[3]_i_12_n_0\
    );
\FSM_gray_state[3]_i_13\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"7F"
    )
        port map (
      I0 => bitCount(0),
      I1 => bitCount(1),
      I2 => bitCount(2),
      O => \FSM_gray_state[3]_i_13_n_0\
    );
\FSM_gray_state[3]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000011000A0000"
    )
        port map (
      I0 => state(0),
      I1 => \FSM_gray_state[3]_i_3_n_0\,
      I2 => \FSM_gray_state[3]_i_9_n_0\,
      I3 => state(3),
      I4 => state(1),
      I5 => state(2),
      O => \FSM_gray_state[3]_i_2_n_0\
    );
\FSM_gray_state[3]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => rSda,
      I1 => dScl,
      I2 => p_0_in(0),
      O => \FSM_gray_state[3]_i_3_n_0\
    );
\FSM_gray_state[3]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4004"
    )
        port map (
      I0 => state(3),
      I1 => state(2),
      I2 => state(1),
      I3 => state(0),
      O => \FSM_gray_state[3]_i_4_n_0\
    );
\FSM_gray_state[3]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF4000FF00"
    )
        port map (
      I0 => \FSM_gray_state[3]_i_10_n_0\,
      I1 => \subState_reg_n_0_[1]\,
      I2 => \subState_reg_n_0_[0]\,
      I3 => \subState[1]_i_2_n_0\,
      I4 => \FSM_gray_state[3]_i_11_n_0\,
      I5 => \FSM_gray_state[3]_i_12_n_0\,
      O => \FSM_gray_state[3]_i_5_n_0\
    );
\FSM_gray_state[3]_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => state(3),
      I1 => state(2),
      I2 => state(0),
      I3 => state(1),
      O => \FSM_gray_state[3]_i_6_n_0\
    );
\FSM_gray_state[3]_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => \subState[1]_i_2_n_0\,
      I1 => \subState_reg_n_0_[1]\,
      I2 => \subState_reg_n_0_[0]\,
      O => \FSM_gray_state[3]_i_7_n_0\
    );
\FSM_gray_state[3]_i_8\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => bitCount(2),
      I1 => bitCount(0),
      I2 => bitCount(1),
      O => \FSM_gray_state[3]_i_8_n_0\
    );
\FSM_gray_state[3]_i_9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"000E"
    )
        port map (
      I0 => addrNData_reg_n_0,
      I1 => stb,
      I2 => \dataByte_reg_n_0_[0]\,
      I3 => int_Rst,
      O => \FSM_gray_state[3]_i_9_n_0\
    );
\FSM_gray_state_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => \FSM_gray_state[3]_i_1_n_0\,
      D => \FSM_gray_state[0]_i_1_n_0\,
      Q => state(0),
      R => '0'
    );
\FSM_gray_state_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => \FSM_gray_state[3]_i_1_n_0\,
      D => \FSM_gray_state[1]_i_1_n_0\,
      Q => state(1),
      R => '0'
    );
\FSM_gray_state_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => \FSM_gray_state[3]_i_1_n_0\,
      D => \FSM_gray_state[2]_i_1_n_0\,
      Q => state(2),
      R => '0'
    );
\FSM_gray_state_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => \FSM_gray_state[3]_i_1_n_0\,
      D => \FSM_gray_state[3]_i_2_n_0\,
      Q => state(3),
      R => '0'
    );
addrNData_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0AEAEAEAEAEAEAEA"
    )
        port map (
      I0 => addrNData_reg_n_0,
      I1 => \dataByte[7]_i_6_n_0\,
      I2 => \subState[1]_i_2_n_0\,
      I3 => \subState_reg_n_0_[1]\,
      I4 => \subState_reg_n_0_[0]\,
      I5 => addrNData_i_2_n_0,
      O => addrNData_i_1_n_0
    );
addrNData_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1000"
    )
        port map (
      I0 => state(3),
      I1 => state(2),
      I2 => state(0),
      I3 => state(1),
      O => addrNData_i_2_n_0
    );
addrNData_reg: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => '1',
      D => addrNData_i_1_n_0,
      Q => addrNData_reg_n_0,
      R => '0'
    );
\bitCount[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"F6"
    )
        port map (
      I0 => bitCount(0),
      I1 => dataByte0,
      I2 => dataByte1,
      O => \bitCount[0]_i_1_n_0\
    );
\bitCount[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFA6"
    )
        port map (
      I0 => bitCount(1),
      I1 => dataByte0,
      I2 => bitCount(0),
      I3 => dataByte1,
      O => \bitCount[1]_i_1_n_0\
    );
\bitCount[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFAAA6"
    )
        port map (
      I0 => bitCount(2),
      I1 => dataByte0,
      I2 => bitCount(1),
      I3 => bitCount(0),
      I4 => dataByte1,
      O => \bitCount[2]_i_1_n_0\
    );
\bitCount_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => \bitCount[0]_i_1_n_0\,
      Q => bitCount(0),
      R => '0'
    );
\bitCount_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => \bitCount[1]_i_1_n_0\,
      Q => bitCount(1),
      R => '0'
    );
\bitCount_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => \bitCount[2]_i_1_n_0\,
      Q => bitCount(2),
      R => '0'
    );
\busFreeCnt[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \busFreeCnt_reg__0\(0),
      O => busFreeCnt0(0)
    );
\busFreeCnt[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \busFreeCnt_reg__0\(1),
      I1 => \busFreeCnt_reg__0\(0),
      O => \busFreeCnt[1]_i_1_n_0\
    );
\busFreeCnt[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"A9"
    )
        port map (
      I0 => \busFreeCnt_reg__0\(2),
      I1 => \busFreeCnt_reg__0\(0),
      I2 => \busFreeCnt_reg__0\(1),
      O => busFreeCnt0(2)
    );
\busFreeCnt[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AAA9"
    )
        port map (
      I0 => \busFreeCnt_reg__0\(3),
      I1 => \busFreeCnt_reg__0\(2),
      I2 => \busFreeCnt_reg__0\(1),
      I3 => \busFreeCnt_reg__0\(0),
      O => busFreeCnt0(3)
    );
\busFreeCnt[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAAAAA9"
    )
        port map (
      I0 => \busFreeCnt_reg__0\(4),
      I1 => \busFreeCnt_reg__0\(3),
      I2 => \busFreeCnt_reg__0\(0),
      I3 => \busFreeCnt_reg__0\(1),
      I4 => \busFreeCnt_reg__0\(2),
      O => busFreeCnt0(4)
    );
\busFreeCnt[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAAAAAAAAA9"
    )
        port map (
      I0 => \busFreeCnt_reg__0\(5),
      I1 => \busFreeCnt_reg__0\(4),
      I2 => \busFreeCnt_reg__0\(2),
      I3 => \busFreeCnt_reg__0\(1),
      I4 => \busFreeCnt_reg__0\(0),
      I5 => \busFreeCnt_reg__0\(3),
      O => busFreeCnt0(5)
    );
\busFreeCnt[6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BF"
    )
        port map (
      I0 => int_Rst,
      I1 => p_0_in(0),
      I2 => dScl,
      O => busFreeCnt0_1
    );
\busFreeCnt[6]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \busFreeCnt_reg__0\(6),
      I1 => \busFreeCnt[6]_i_3_n_0\,
      O => busFreeCnt0(6)
    );
\busFreeCnt[6]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => \busFreeCnt_reg__0\(4),
      I1 => \busFreeCnt_reg__0\(2),
      I2 => \busFreeCnt_reg__0\(1),
      I3 => \busFreeCnt_reg__0\(0),
      I4 => \busFreeCnt_reg__0\(3),
      I5 => \busFreeCnt_reg__0\(5),
      O => \busFreeCnt[6]_i_3_n_0\
    );
\busFreeCnt_reg[0]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => busFreeCnt0(0),
      Q => \busFreeCnt_reg__0\(0),
      S => busFreeCnt0_1
    );
\busFreeCnt_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => \busFreeCnt[1]_i_1_n_0\,
      Q => \busFreeCnt_reg__0\(1),
      R => busFreeCnt0_1
    );
\busFreeCnt_reg[2]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => busFreeCnt0(2),
      Q => \busFreeCnt_reg__0\(2),
      S => busFreeCnt0_1
    );
\busFreeCnt_reg[3]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => busFreeCnt0(3),
      Q => \busFreeCnt_reg__0\(3),
      S => busFreeCnt0_1
    );
\busFreeCnt_reg[4]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => busFreeCnt0(4),
      Q => \busFreeCnt_reg__0\(4),
      S => busFreeCnt0_1
    );
\busFreeCnt_reg[5]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => busFreeCnt0(5),
      Q => \busFreeCnt_reg__0\(5),
      S => busFreeCnt0_1
    );
\busFreeCnt_reg[6]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => busFreeCnt0(6),
      Q => \busFreeCnt_reg__0\(6),
      S => busFreeCnt0_1
    );
\busState[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4555FFFF45550000"
    )
        port map (
      I0 => int_Rst,
      I1 => p_0_in(0),
      I2 => dScl,
      I3 => ddSda,
      I4 => busState0,
      I5 => \busState_reg_n_0_[0]\,
      O => \busState[0]_i_1_n_0\
    );
\busState[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0040FFFF00400000"
    )
        port map (
      I0 => p_0_in(0),
      I1 => dScl,
      I2 => ddSda,
      I3 => int_Rst,
      I4 => busState0,
      I5 => \busState_reg_n_0_[1]\,
      O => \busState[1]_i_1_n_0\
    );
\busState[1]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF4444F444"
    )
        port map (
      I0 => \busFreeCnt_reg__0\(6),
      I1 => \busFreeCnt[6]_i_3_n_0\,
      I2 => ddSda,
      I3 => dScl,
      I4 => p_0_in(0),
      I5 => int_Rst,
      O => busState0
    );
\busState_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => \busState[0]_i_1_n_0\,
      Q => \busState_reg_n_0_[0]\,
      R => '0'
    );
\busState_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => \busState[1]_i_1_n_0\,
      Q => \busState_reg_n_0_[1]\,
      R => '0'
    );
dScl_reg: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => '1',
      D => scl,
      Q => dScl,
      R => '0'
    );
dSda_reg: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => '1',
      D => sda,
      Q => p_0_in(0),
      R => '0'
    );
\dataByte[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"88B8"
    )
        port map (
      I0 => p_0_in(0),
      I1 => \dataByte[7]_i_5_n_0\,
      I2 => data_i(0),
      I3 => \dataByte[7]_i_6_n_0\,
      O => \dataByte[0]_i_1_n_0\
    );
\dataByte[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BBB8"
    )
        port map (
      I0 => \dataByte_reg_n_0_[0]\,
      I1 => \dataByte[7]_i_5_n_0\,
      I2 => data_i(1),
      I3 => \dataByte[7]_i_6_n_0\,
      O => \dataByte[1]_i_1_n_0\
    );
\dataByte[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BBB8"
    )
        port map (
      I0 => \dataByte_reg_n_0_[1]\,
      I1 => \dataByte[7]_i_5_n_0\,
      I2 => data_i(2),
      I3 => \dataByte[7]_i_6_n_0\,
      O => \dataByte[2]_i_1_n_0\
    );
\dataByte[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"88B8"
    )
        port map (
      I0 => \dataByte_reg_n_0_[2]\,
      I1 => \dataByte[7]_i_5_n_0\,
      I2 => data_i(3),
      I3 => \dataByte[7]_i_6_n_0\,
      O => \dataByte[3]_i_1_n_0\
    );
\dataByte[4]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BBB8"
    )
        port map (
      I0 => \dataByte_reg_n_0_[3]\,
      I1 => \dataByte[7]_i_5_n_0\,
      I2 => data_i(4),
      I3 => \dataByte[7]_i_6_n_0\,
      O => \dataByte[4]_i_1_n_0\
    );
\dataByte[5]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BBB8"
    )
        port map (
      I0 => \dataByte_reg_n_0_[4]\,
      I1 => \dataByte[7]_i_5_n_0\,
      I2 => data_i(5),
      I3 => \dataByte[7]_i_6_n_0\,
      O => \dataByte[5]_i_1_n_0\
    );
\dataByte[6]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BBB8"
    )
        port map (
      I0 => \dataByte_reg_n_0_[5]\,
      I1 => \dataByte[7]_i_5_n_0\,
      I2 => data_i(6),
      I3 => \dataByte[7]_i_6_n_0\,
      O => \dataByte[6]_i_1_n_0\
    );
\dataByte[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => dataByte1,
      I1 => dataByte0,
      O => \dataByte[7]_i_1_n_0\
    );
\dataByte[7]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"88B8"
    )
        port map (
      I0 => \dataByte_reg_n_0_[6]\,
      I1 => \dataByte[7]_i_5_n_0\,
      I2 => data_i(7),
      I3 => \dataByte[7]_i_6_n_0\,
      O => \dataByte[7]_i_2_n_0\
    );
\dataByte[7]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0030004000000000"
    )
        port map (
      I0 => \dataByte[7]_i_7_n_0\,
      I1 => state(1),
      I2 => state(0),
      I3 => state(3),
      I4 => state(2),
      I5 => \subState[1]_i_2_n_0\,
      O => dataByte1
    );
\dataByte[7]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00C0000040000000"
    )
        port map (
      I0 => state(0),
      I1 => \subState[1]_i_2_n_0\,
      I2 => \subState_reg_n_0_[0]\,
      I3 => \subState_reg_n_0_[1]\,
      I4 => \subState[1]_i_3_n_0\,
      I5 => state(1),
      O => dataByte0
    );
\dataByte[7]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFDFFFFFFFDF7F"
    )
        port map (
      I0 => \subState[1]_i_2_n_0\,
      I1 => state(1),
      I2 => state(0),
      I3 => state(2),
      I4 => state(3),
      I5 => \dataByte[7]_i_7_n_0\,
      O => \dataByte[7]_i_5_n_0\
    );
\dataByte[7]_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0400"
    )
        port map (
      I0 => state(1),
      I1 => state(0),
      I2 => state(3),
      I3 => state(2),
      O => \dataByte[7]_i_6_n_0\
    );
\dataByte[7]_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \subState_reg_n_0_[0]\,
      I1 => \subState_reg_n_0_[1]\,
      O => \dataByte[7]_i_7_n_0\
    );
\dataByte_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => \dataByte[7]_i_1_n_0\,
      D => \dataByte[0]_i_1_n_0\,
      Q => \dataByte_reg_n_0_[0]\,
      R => '0'
    );
\dataByte_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => \dataByte[7]_i_1_n_0\,
      D => \dataByte[1]_i_1_n_0\,
      Q => \dataByte_reg_n_0_[1]\,
      R => '0'
    );
\dataByte_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => \dataByte[7]_i_1_n_0\,
      D => \dataByte[2]_i_1_n_0\,
      Q => \dataByte_reg_n_0_[2]\,
      R => '0'
    );
\dataByte_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => \dataByte[7]_i_1_n_0\,
      D => \dataByte[3]_i_1_n_0\,
      Q => \dataByte_reg_n_0_[3]\,
      R => '0'
    );
\dataByte_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => \dataByte[7]_i_1_n_0\,
      D => \dataByte[4]_i_1_n_0\,
      Q => \dataByte_reg_n_0_[4]\,
      R => '0'
    );
\dataByte_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => \dataByte[7]_i_1_n_0\,
      D => \dataByte[5]_i_1_n_0\,
      Q => \dataByte_reg_n_0_[5]\,
      R => '0'
    );
\dataByte_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => \dataByte[7]_i_1_n_0\,
      D => \dataByte[6]_i_1_n_0\,
      Q => \dataByte_reg_n_0_[6]\,
      R => '0'
    );
\dataByte_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => \dataByte[7]_i_1_n_0\,
      D => \dataByte[7]_i_2_n_0\,
      Q => \dataByte_reg_n_0_[7]\,
      R => '0'
    );
ddSda_reg: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => '1',
      D => p_0_in(0),
      Q => ddSda,
      R => '0'
    );
\initA[6]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F444"
    )
        port map (
      I0 => Q(3),
      I1 => initEn_reg_0,
      I2 => done,
      I3 => \^d\(3),
      O => \initA_reg[6]\(0)
    );
initEn_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF57FF00005400"
    )
        port map (
      I0 => Q(2),
      I1 => initEn_i_2_n_0,
      I2 => initEn_reg_0,
      I3 => s00_axi_aresetn,
      I4 => Q(3),
      I5 => initEn,
      O => initEn_reg
    );
initEn_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0002"
    )
        port map (
      I0 => done,
      I1 => Q(2),
      I2 => error,
      I3 => \initWord_reg[6]\,
      O => initEn_i_2_n_0
    );
int_Rst_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBBBBBB3BBBBBBBB"
    )
        port map (
      I0 => int_Rst,
      I1 => s00_axi_aresetn,
      I2 => state(3),
      I3 => state(2),
      I4 => state(1),
      I5 => state(0),
      O => int_Rst_i_1_n_0
    );
int_Rst_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => int_Rst_i_1_n_0,
      Q => int_Rst,
      R => '0'
    );
rScl_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFBF003C"
    )
        port map (
      I0 => state(3),
      I1 => \subState_reg_n_0_[1]\,
      I2 => \subState_reg_n_0_[0]\,
      I3 => subState,
      I4 => rScl,
      O => rScl_i_1_n_0
    );
rScl_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0100"
    )
        port map (
      I0 => state(3),
      I1 => state(2),
      I2 => state(1),
      I3 => state(0),
      O => subState
    );
rScl_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => rScl_i_1_n_0,
      Q => rScl,
      R => '0'
    );
rSda_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFAABA0000AA8A"
    )
        port map (
      I0 => rSda_i_2_n_0,
      I1 => state(3),
      I2 => rSda_i_3_n_0,
      I3 => \dataByte[7]_i_6_n_0\,
      I4 => rSda_i_4_n_0,
      I5 => rSda,
      O => rSda_i_1_n_0
    );
rSda_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0088FFC000AA55D1"
    )
        port map (
      I0 => state(3),
      I1 => \FSM_gray_state[3]_i_6_n_0\,
      I2 => \dataByte_reg_n_0_[7]\,
      I3 => \subState_reg_n_0_[0]\,
      I4 => \subState_reg_n_0_[1]\,
      I5 => rSda_i_5_n_0,
      O => rSda_i_2_n_0
    );
rSda_i_3: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \subState_reg_n_0_[0]\,
      I1 => \subState_reg_n_0_[1]\,
      O => rSda_i_3_n_0
    );
rSda_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF00000002020202"
    )
        port map (
      I0 => state(0),
      I1 => state(1),
      I2 => state(2),
      I3 => \subState_reg_n_0_[0]\,
      I4 => \subState_reg_n_0_[1]\,
      I5 => state(3),
      O => rSda_i_4_n_0
    );
rSda_i_5: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0400"
    )
        port map (
      I0 => state(3),
      I1 => state(2),
      I2 => state(0),
      I3 => state(1),
      O => rSda_i_5_n_0
    );
rSda_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => rSda_i_1_n_0,
      Q => rSda,
      R => '0'
    );
\sclCnt[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \sclCnt_reg__0\(0),
      O => sclCnt0(0)
    );
\sclCnt[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \sclCnt_reg__0\(1),
      I1 => \sclCnt_reg__0\(0),
      O => \sclCnt[1]_i_1_n_0\
    );
\sclCnt[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"A9"
    )
        port map (
      I0 => \sclCnt_reg__0\(2),
      I1 => \sclCnt_reg__0\(0),
      I2 => \sclCnt_reg__0\(1),
      O => sclCnt0(2)
    );
\sclCnt[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AAA9"
    )
        port map (
      I0 => \sclCnt_reg__0\(3),
      I1 => \sclCnt_reg__0\(2),
      I2 => \sclCnt_reg__0\(1),
      I3 => \sclCnt_reg__0\(0),
      O => sclCnt0(3)
    );
\sclCnt[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAAAAA9"
    )
        port map (
      I0 => \sclCnt_reg__0\(4),
      I1 => \sclCnt_reg__0\(3),
      I2 => \sclCnt_reg__0\(0),
      I3 => \sclCnt_reg__0\(1),
      I4 => \sclCnt_reg__0\(2),
      O => sclCnt0(4)
    );
\sclCnt[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAAAAAAAAA9"
    )
        port map (
      I0 => \sclCnt_reg__0\(5),
      I1 => \sclCnt_reg__0\(4),
      I2 => \sclCnt_reg__0\(2),
      I3 => \sclCnt_reg__0\(1),
      I4 => \sclCnt_reg__0\(0),
      I5 => \sclCnt_reg__0\(3),
      O => sclCnt0(5)
    );
\sclCnt[6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF0002"
    )
        port map (
      I0 => state(0),
      I1 => state(1),
      I2 => state(2),
      I3 => state(3),
      I4 => \subState[1]_i_2_n_0\,
      O => sclCnt0_0
    );
\sclCnt[6]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => dScl,
      I1 => rScl,
      O => \sclCnt[6]_i_2_n_0\
    );
\sclCnt[6]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \sclCnt_reg__0\(6),
      I1 => \sclCnt[6]_i_4_n_0\,
      O => sclCnt0(6)
    );
\sclCnt[6]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => \sclCnt_reg__0\(4),
      I1 => \sclCnt_reg__0\(2),
      I2 => \sclCnt_reg__0\(1),
      I3 => \sclCnt_reg__0\(0),
      I4 => \sclCnt_reg__0\(3),
      I5 => \sclCnt_reg__0\(5),
      O => \sclCnt[6]_i_4_n_0\
    );
\sclCnt_reg[0]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk_out2,
      CE => \sclCnt[6]_i_2_n_0\,
      D => sclCnt0(0),
      Q => \sclCnt_reg__0\(0),
      S => sclCnt0_0
    );
\sclCnt_reg[1]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => \sclCnt[6]_i_2_n_0\,
      D => \sclCnt[1]_i_1_n_0\,
      Q => \sclCnt_reg__0\(1),
      S => sclCnt0_0
    );
\sclCnt_reg[2]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk_out2,
      CE => \sclCnt[6]_i_2_n_0\,
      D => sclCnt0(2),
      Q => \sclCnt_reg__0\(2),
      S => sclCnt0_0
    );
\sclCnt_reg[3]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk_out2,
      CE => \sclCnt[6]_i_2_n_0\,
      D => sclCnt0(3),
      Q => \sclCnt_reg__0\(3),
      S => sclCnt0_0
    );
\sclCnt_reg[4]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk_out2,
      CE => \sclCnt[6]_i_2_n_0\,
      D => sclCnt0(4),
      Q => \sclCnt_reg__0\(4),
      S => sclCnt0_0
    );
\sclCnt_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk_out2,
      CE => \sclCnt[6]_i_2_n_0\,
      D => sclCnt0(5),
      Q => \sclCnt_reg__0\(5),
      R => sclCnt0_0
    );
\sclCnt_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk_out2,
      CE => \sclCnt[6]_i_2_n_0\,
      D => sclCnt0(6),
      Q => \sclCnt_reg__0\(6),
      R => sclCnt0_0
    );
scl_INST_0: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => '0',
      I1 => scl_INST_0_i_1_n_0,
      I2 => '0',
      I3 => '0',
      I4 => '0',
      I5 => '0',
      O => scl
    );
scl_INST_0_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => rScl,
      O => scl_INST_0_i_1_n_0
    );
sda_INST_0: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => '0',
      I1 => sda_INST_0_i_1_n_0,
      I2 => '0',
      I3 => '0',
      I4 => '0',
      I5 => '0',
      O => sda
    );
sda_INST_0_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => rSda,
      O => sda_INST_0_i_1_n_0
    );
\state[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0000AAFB"
    )
        port map (
      I0 => Q(3),
      I1 => Q(0),
      I2 => Q(1),
      I3 => Q(2),
      I4 => error,
      O => \^d\(0)
    );
\state[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCC0CCCC88808880"
    )
        port map (
      I0 => Q(1),
      I1 => \state[1]_i_2_n_0\,
      I2 => \initWord_reg[6]\,
      I3 => \initA_reg[4]\,
      I4 => \initWord_reg[21]\,
      I5 => Q(0),
      O => \^d\(1)
    );
\state[1]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => error,
      I1 => Q(2),
      I2 => Q(3),
      O => \state[1]_i_2_n_0\
    );
\state[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"C0F1"
    )
        port map (
      I0 => \initWord_reg[6]\,
      I1 => Q(3),
      I2 => error,
      I3 => Q(2),
      O => \^d\(2)
    );
\state[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"80CF00AA800F00AA"
    )
        port map (
      I0 => done,
      I1 => Q(1),
      I2 => Q(0),
      I3 => Q(3),
      I4 => Q(2),
      I5 => \delaycnt_reg[20]\,
      O => E(0)
    );
\state[3]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0001000000000000"
    )
        port map (
      I0 => \initWord_reg[21]\,
      I1 => error,
      I2 => Q(2),
      I3 => Q(3),
      I4 => Q(1),
      I5 => Q(0),
      O => \^d\(3)
    );
\subState[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6666666666666606"
    )
        port map (
      I0 => \subState_reg_n_0_[0]\,
      I1 => \subState[1]_i_2_n_0\,
      I2 => state(0),
      I3 => state(1),
      I4 => state(2),
      I5 => state(3),
      O => \subState[0]_i_1_n_0\
    );
\subState[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6A6A006A6A6A6A6A"
    )
        port map (
      I0 => \subState_reg_n_0_[1]\,
      I1 => \subState[1]_i_2_n_0\,
      I2 => \subState_reg_n_0_[0]\,
      I3 => state(0),
      I4 => state(1),
      I5 => \subState[1]_i_3_n_0\,
      O => \subState[1]_i_1_n_0\
    );
\subState[1]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \sclCnt[6]_i_4_n_0\,
      I1 => \sclCnt_reg__0\(6),
      O => \subState[1]_i_2_n_0\
    );
\subState[1]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => state(2),
      I1 => state(3),
      O => \subState[1]_i_3_n_0\
    );
\subState_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => \subState[0]_i_1_n_0\,
      Q => \subState_reg_n_0_[0]\,
      R => '0'
    );
\subState_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => \subState[1]_i_1_n_0\,
      Q => \subState_reg_n_0_[1]\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_0_clk_wiz is
  port (
    clk_out1 : out STD_LOGIC;
    clk_out2 : out STD_LOGIC;
    clk_out3 : out STD_LOGIC;
    resetn : in STD_LOGIC;
    clk_in1 : in STD_LOGIC;
    lopt : out STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_0_clk_wiz;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_0_clk_wiz is
  signal clk_in1_clk_wiz_0 : STD_LOGIC;
  signal clk_out1_clk_wiz_0 : STD_LOGIC;
  signal clk_out2_clk_wiz_0 : STD_LOGIC;
  signal clk_out3_clk_wiz_0 : STD_LOGIC;
  signal clkfbout_buf_clk_wiz_0 : STD_LOGIC;
  signal clkfbout_clk_wiz_0 : STD_LOGIC;
  signal reset_high : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT2_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT3_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_DRDY_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_LOCKED_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_PSDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_DO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  attribute box_type : string;
  attribute box_type of clkf_buf : label is "PRIMITIVE";
  attribute CAPACITANCE : string;
  attribute CAPACITANCE of clkin1_ibufg : label is "DONT_CARE";
  attribute IBUF_DELAY_VALUE : string;
  attribute IBUF_DELAY_VALUE of clkin1_ibufg : label is "0";
  attribute IFD_DELAY_VALUE : string;
  attribute IFD_DELAY_VALUE of clkin1_ibufg : label is "AUTO";
  attribute box_type of clkin1_ibufg : label is "PRIMITIVE";
  attribute box_type of clkout1_buf : label is "PRIMITIVE";
  attribute box_type of clkout2_buf : label is "PRIMITIVE";
  attribute box_type of clkout3_buf : label is "PRIMITIVE";
  attribute box_type of mmcm_adv_inst : label is "PRIMITIVE";
begin
  lopt <= clk_in1_clk_wiz_0;
clkf_buf: unisim.vcomponents.BUFG
     port map (
      I => clkfbout_clk_wiz_0,
      O => clkfbout_buf_clk_wiz_0
    );
clkin1_ibufg: unisim.vcomponents.IBUF
    generic map(
      IOSTANDARD => "DEFAULT"
    )
        port map (
      I => clk_in1,
      O => clk_in1_clk_wiz_0
    );
clkout1_buf: unisim.vcomponents.BUFG
     port map (
      I => clk_out1_clk_wiz_0,
      O => clk_out1
    );
clkout2_buf: unisim.vcomponents.BUFG
     port map (
      I => clk_out2_clk_wiz_0,
      O => clk_out2
    );
clkout3_buf: unisim.vcomponents.BUFG
     port map (
      I => clk_out3_clk_wiz_0,
      O => clk_out3
    );
mmcm_adv_inst: unisim.vcomponents.MMCME2_ADV
    generic map(
      BANDWIDTH => "OPTIMIZED",
      CLKFBOUT_MULT_F => 10.000000,
      CLKFBOUT_PHASE => 0.000000,
      CLKFBOUT_USE_FINE_PS => false,
      CLKIN1_PERIOD => 10.000000,
      CLKIN2_PERIOD => 0.000000,
      CLKOUT0_DIVIDE_F => 40.000000,
      CLKOUT0_DUTY_CYCLE => 0.500000,
      CLKOUT0_PHASE => 0.000000,
      CLKOUT0_USE_FINE_PS => false,
      CLKOUT1_DIVIDE => 8,
      CLKOUT1_DUTY_CYCLE => 0.500000,
      CLKOUT1_PHASE => 0.000000,
      CLKOUT1_USE_FINE_PS => false,
      CLKOUT2_DIVIDE => 1,
      CLKOUT2_DUTY_CYCLE => 0.500000,
      CLKOUT2_PHASE => 0.000000,
      CLKOUT2_USE_FINE_PS => false,
      CLKOUT3_DIVIDE => 1,
      CLKOUT3_DUTY_CYCLE => 0.500000,
      CLKOUT3_PHASE => 0.000000,
      CLKOUT3_USE_FINE_PS => false,
      CLKOUT4_CASCADE => false,
      CLKOUT4_DIVIDE => 1,
      CLKOUT4_DUTY_CYCLE => 0.500000,
      CLKOUT4_PHASE => 0.000000,
      CLKOUT4_USE_FINE_PS => false,
      CLKOUT5_DIVIDE => 1,
      CLKOUT5_DUTY_CYCLE => 0.500000,
      CLKOUT5_PHASE => 0.000000,
      CLKOUT5_USE_FINE_PS => false,
      CLKOUT6_DIVIDE => 1,
      CLKOUT6_DUTY_CYCLE => 0.500000,
      CLKOUT6_PHASE => 0.000000,
      CLKOUT6_USE_FINE_PS => false,
      COMPENSATION => "ZHOLD",
      DIVCLK_DIVIDE => 1,
      IS_CLKINSEL_INVERTED => '0',
      IS_PSEN_INVERTED => '0',
      IS_PSINCDEC_INVERTED => '0',
      IS_PWRDWN_INVERTED => '0',
      IS_RST_INVERTED => '0',
      REF_JITTER1 => 0.010000,
      REF_JITTER2 => 0.010000,
      SS_EN => "FALSE",
      SS_MODE => "CENTER_HIGH",
      SS_MOD_PERIOD => 10000,
      STARTUP_WAIT => false
    )
        port map (
      CLKFBIN => clkfbout_buf_clk_wiz_0,
      CLKFBOUT => clkfbout_clk_wiz_0,
      CLKFBOUTB => NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED,
      CLKFBSTOPPED => NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED,
      CLKIN1 => clk_in1_clk_wiz_0,
      CLKIN2 => '0',
      CLKINSEL => '1',
      CLKINSTOPPED => NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED,
      CLKOUT0 => clk_out1_clk_wiz_0,
      CLKOUT0B => NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED,
      CLKOUT1 => clk_out2_clk_wiz_0,
      CLKOUT1B => clk_out3_clk_wiz_0,
      CLKOUT2 => NLW_mmcm_adv_inst_CLKOUT2_UNCONNECTED,
      CLKOUT2B => NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED,
      CLKOUT3 => NLW_mmcm_adv_inst_CLKOUT3_UNCONNECTED,
      CLKOUT3B => NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED,
      CLKOUT4 => NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED,
      CLKOUT5 => NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED,
      CLKOUT6 => NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED,
      DADDR(6 downto 0) => B"0000000",
      DCLK => '0',
      DEN => '0',
      DI(15 downto 0) => B"0000000000000000",
      DO(15 downto 0) => NLW_mmcm_adv_inst_DO_UNCONNECTED(15 downto 0),
      DRDY => NLW_mmcm_adv_inst_DRDY_UNCONNECTED,
      DWE => '0',
      LOCKED => NLW_mmcm_adv_inst_LOCKED_UNCONNECTED,
      PSCLK => '0',
      PSDONE => NLW_mmcm_adv_inst_PSDONE_UNCONNECTED,
      PSEN => '0',
      PSINCDEC => '0',
      PWRDWN => '0',
      RST => reset_high
    );
mmcm_adv_inst_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => resetn,
      O => reset_high
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_1_clk_wiz is
  port (
    clk_out1 : out STD_LOGIC;
    clk_out2 : out STD_LOGIC;
    resetn : in STD_LOGIC;
    clk_in1 : in STD_LOGIC;
    lopt : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_1_clk_wiz;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_1_clk_wiz is
  signal clk_out1_clk_wiz_1 : STD_LOGIC;
  signal clk_out2_clk_wiz_1 : STD_LOGIC;
  signal clkfbout_buf_clk_wiz_1 : STD_LOGIC;
  signal clkfbout_clk_wiz_1 : STD_LOGIC;
  signal reset_high : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT1B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT2_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT3_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_DRDY_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_LOCKED_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_PSDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_DO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  attribute box_type : string;
  attribute box_type of clkf_buf : label is "PRIMITIVE";
  attribute box_type of clkout1_buf : label is "PRIMITIVE";
  attribute box_type of clkout2_buf : label is "PRIMITIVE";
  attribute box_type of mmcm_adv_inst : label is "PRIMITIVE";
begin
clkf_buf: unisim.vcomponents.BUFG
     port map (
      I => clkfbout_clk_wiz_1,
      O => clkfbout_buf_clk_wiz_1
    );
clkout1_buf: unisim.vcomponents.BUFG
     port map (
      I => clk_out1_clk_wiz_1,
      O => clk_out1
    );
clkout2_buf: unisim.vcomponents.BUFG
     port map (
      I => clk_out2_clk_wiz_1,
      O => clk_out2
    );
mmcm_adv_inst: unisim.vcomponents.MMCME2_ADV
    generic map(
      BANDWIDTH => "OPTIMIZED",
      CLKFBOUT_MULT_F => 10.000000,
      CLKFBOUT_PHASE => 0.000000,
      CLKFBOUT_USE_FINE_PS => false,
      CLKIN1_PERIOD => 10.000000,
      CLKIN2_PERIOD => 0.000000,
      CLKOUT0_DIVIDE_F => 81.375000,
      CLKOUT0_DUTY_CYCLE => 0.500000,
      CLKOUT0_PHASE => 0.000000,
      CLKOUT0_USE_FINE_PS => false,
      CLKOUT1_DIVIDE => 20,
      CLKOUT1_DUTY_CYCLE => 0.500000,
      CLKOUT1_PHASE => 0.000000,
      CLKOUT1_USE_FINE_PS => false,
      CLKOUT2_DIVIDE => 1,
      CLKOUT2_DUTY_CYCLE => 0.500000,
      CLKOUT2_PHASE => 0.000000,
      CLKOUT2_USE_FINE_PS => false,
      CLKOUT3_DIVIDE => 1,
      CLKOUT3_DUTY_CYCLE => 0.500000,
      CLKOUT3_PHASE => 0.000000,
      CLKOUT3_USE_FINE_PS => false,
      CLKOUT4_CASCADE => false,
      CLKOUT4_DIVIDE => 1,
      CLKOUT4_DUTY_CYCLE => 0.500000,
      CLKOUT4_PHASE => 0.000000,
      CLKOUT4_USE_FINE_PS => false,
      CLKOUT5_DIVIDE => 1,
      CLKOUT5_DUTY_CYCLE => 0.500000,
      CLKOUT5_PHASE => 0.000000,
      CLKOUT5_USE_FINE_PS => false,
      CLKOUT6_DIVIDE => 1,
      CLKOUT6_DUTY_CYCLE => 0.500000,
      CLKOUT6_PHASE => 0.000000,
      CLKOUT6_USE_FINE_PS => false,
      COMPENSATION => "ZHOLD",
      DIVCLK_DIVIDE => 1,
      IS_CLKINSEL_INVERTED => '0',
      IS_PSEN_INVERTED => '0',
      IS_PSINCDEC_INVERTED => '0',
      IS_PWRDWN_INVERTED => '0',
      IS_RST_INVERTED => '0',
      REF_JITTER1 => 0.010000,
      REF_JITTER2 => 0.010000,
      SS_EN => "FALSE",
      SS_MODE => "CENTER_HIGH",
      SS_MOD_PERIOD => 10000,
      STARTUP_WAIT => false
    )
        port map (
      CLKFBIN => clkfbout_buf_clk_wiz_1,
      CLKFBOUT => clkfbout_clk_wiz_1,
      CLKFBOUTB => NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED,
      CLKFBSTOPPED => NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED,
      CLKIN1 => lopt,
      CLKIN2 => '0',
      CLKINSEL => '1',
      CLKINSTOPPED => NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED,
      CLKOUT0 => clk_out1_clk_wiz_1,
      CLKOUT0B => NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED,
      CLKOUT1 => clk_out2_clk_wiz_1,
      CLKOUT1B => NLW_mmcm_adv_inst_CLKOUT1B_UNCONNECTED,
      CLKOUT2 => NLW_mmcm_adv_inst_CLKOUT2_UNCONNECTED,
      CLKOUT2B => NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED,
      CLKOUT3 => NLW_mmcm_adv_inst_CLKOUT3_UNCONNECTED,
      CLKOUT3B => NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED,
      CLKOUT4 => NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED,
      CLKOUT5 => NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED,
      CLKOUT6 => NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED,
      DADDR(6 downto 0) => B"0000000",
      DCLK => '0',
      DEN => '0',
      DI(15 downto 0) => B"0000000000000000",
      DO(15 downto 0) => NLW_mmcm_adv_inst_DO_UNCONNECTED(15 downto 0),
      DRDY => NLW_mmcm_adv_inst_DRDY_UNCONNECTED,
      DWE => '0',
      LOCKED => NLW_mmcm_adv_inst_LOCKED_UNCONNECTED,
      PSCLK => '0',
      PSDONE => NLW_mmcm_adv_inst_PSDONE_UNCONNECTED,
      PSEN => '0',
      PSINCDEC => '0',
      PWRDWN => '0',
      RST => reset_high
    );
mmcm_adv_inst_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => resetn,
      O => reset_high
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_i2s_ctl is
  port (
    ac_bclk : out STD_LOGIC;
    SR : out STD_LOGIC_VECTOR ( 0 to 0 );
    ac_lrclk : out STD_LOGIC;
    ac_lrclk_sig_prev_reg : out STD_LOGIC;
    ready_sig_reg : out STD_LOGIC;
    w_sw : out STD_LOGIC_VECTOR ( 0 to 0 );
    CO : out STD_LOGIC_VECTOR ( 0 to 0 );
    DIBDI : out STD_LOGIC_VECTOR ( 15 downto 0 );
    \L_bus_in_reg[17]\ : out STD_LOGIC_VECTOR ( 17 downto 0 );
    D : out STD_LOGIC_VECTOR ( 0 to 0 );
    \sdp_bl.ramb18_dp_bl.ram18_bl\ : out STD_LOGIC_VECTOR ( 15 downto 0 );
    \R_bus_in_reg[17]\ : out STD_LOGIC_VECTOR ( 17 downto 0 );
    ac_dac_sdata : out STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC;
    ac_adc_sdata : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC;
    ac_lrclk_sig_prev_reg_0 : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 17 downto 0 );
    \R_bus_in_reg[17]_0\ : in STD_LOGIC_VECTOR ( 17 downto 0 );
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    ac_lrclk_count : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \trigger_volt_reg[9]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \slv_reg9_reg[15]\ : in STD_LOGIC_VECTOR ( 15 downto 0 );
    switch : in STD_LOGIC_VECTOR ( 0 to 0 );
    \trigger_volt_reg[8]\ : in STD_LOGIC_VECTOR ( 7 downto 0 );
    \slv_reg10_reg[15]\ : in STD_LOGIC_VECTOR ( 15 downto 0 );
    S : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_i2s_ctl;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_i2s_ctl is
  signal BCLK_Fall_int : STD_LOGIC;
  signal BCLK_int_i_2_n_0 : STD_LOGIC;
  signal \^co\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal Cnt_Bclk0 : STD_LOGIC;
  signal \Cnt_Bclk0_inferred__0/i__carry_n_3\ : STD_LOGIC;
  signal \Cnt_Bclk[4]_i_1_n_0\ : STD_LOGIC;
  signal \Cnt_Bclk_reg__0\ : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal Cnt_Lrclk : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal \Cnt_Lrclk[0]_i_1_n_0\ : STD_LOGIC;
  signal \Cnt_Lrclk[1]_i_1_n_0\ : STD_LOGIC;
  signal \Cnt_Lrclk[2]_i_1_n_0\ : STD_LOGIC;
  signal \Cnt_Lrclk[3]_i_1_n_0\ : STD_LOGIC;
  signal \Cnt_Lrclk[4]_i_2_n_0\ : STD_LOGIC;
  signal D_L_O_int : STD_LOGIC;
  signal \D_R_O_int[23]_i_1_n_0\ : STD_LOGIC;
  signal Data_In_int : STD_LOGIC_VECTOR ( 31 downto 14 );
  signal \Data_In_int[31]_i_1_n_0\ : STD_LOGIC;
  signal \Data_In_int[31]_i_3_n_0\ : STD_LOGIC;
  signal \Data_In_int_reg[12]_srl13___U0_my_lab3_ip_v2_0_S00_AXI_inst_datapath_wrapper_inst_audio_inout_Data_In_int_reg_r_11_n_0\ : STD_LOGIC;
  signal \Data_In_int_reg[13]_U0_my_lab3_ip_v2_0_S00_AXI_inst_datapath_wrapper_inst_audio_inout_Data_In_int_reg_r_12_n_0\ : STD_LOGIC;
  signal Data_In_int_reg_gate_n_0 : STD_LOGIC;
  signal Data_In_int_reg_r_0_n_0 : STD_LOGIC;
  signal Data_In_int_reg_r_10_n_0 : STD_LOGIC;
  signal Data_In_int_reg_r_11_n_0 : STD_LOGIC;
  signal Data_In_int_reg_r_12_n_0 : STD_LOGIC;
  signal Data_In_int_reg_r_1_n_0 : STD_LOGIC;
  signal Data_In_int_reg_r_2_n_0 : STD_LOGIC;
  signal Data_In_int_reg_r_3_n_0 : STD_LOGIC;
  signal Data_In_int_reg_r_4_n_0 : STD_LOGIC;
  signal Data_In_int_reg_r_5_n_0 : STD_LOGIC;
  signal Data_In_int_reg_r_6_n_0 : STD_LOGIC;
  signal Data_In_int_reg_r_7_n_0 : STD_LOGIC;
  signal Data_In_int_reg_r_8_n_0 : STD_LOGIC;
  signal Data_In_int_reg_r_9_n_0 : STD_LOGIC;
  signal Data_In_int_reg_r_n_0 : STD_LOGIC;
  signal \Data_Out_int[13]_i_1_n_0\ : STD_LOGIC;
  signal \Data_Out_int[14]_i_1_n_0\ : STD_LOGIC;
  signal \Data_Out_int[15]_i_1_n_0\ : STD_LOGIC;
  signal \Data_Out_int[16]_i_1_n_0\ : STD_LOGIC;
  signal \Data_Out_int[17]_i_1_n_0\ : STD_LOGIC;
  signal \Data_Out_int[18]_i_1_n_0\ : STD_LOGIC;
  signal \Data_Out_int[19]_i_1_n_0\ : STD_LOGIC;
  signal \Data_Out_int[20]_i_1_n_0\ : STD_LOGIC;
  signal \Data_Out_int[21]_i_1_n_0\ : STD_LOGIC;
  signal \Data_Out_int[22]_i_1_n_0\ : STD_LOGIC;
  signal \Data_Out_int[23]_i_1_n_0\ : STD_LOGIC;
  signal \Data_Out_int[24]_i_1_n_0\ : STD_LOGIC;
  signal \Data_Out_int[25]_i_1_n_0\ : STD_LOGIC;
  signal \Data_Out_int[26]_i_1_n_0\ : STD_LOGIC;
  signal \Data_Out_int[27]_i_1_n_0\ : STD_LOGIC;
  signal \Data_Out_int[28]_i_1_n_0\ : STD_LOGIC;
  signal \Data_Out_int[29]_i_1_n_0\ : STD_LOGIC;
  signal \Data_Out_int[30]_i_1_n_0\ : STD_LOGIC;
  signal \Data_Out_int[31]_i_1_n_0\ : STD_LOGIC;
  signal \Data_Out_int[31]_i_2_n_0\ : STD_LOGIC;
  signal \Data_Out_int[31]_i_3_n_0\ : STD_LOGIC;
  signal \Data_Out_int_reg_n_0_[13]\ : STD_LOGIC;
  signal \Data_Out_int_reg_n_0_[14]\ : STD_LOGIC;
  signal \Data_Out_int_reg_n_0_[15]\ : STD_LOGIC;
  signal \Data_Out_int_reg_n_0_[16]\ : STD_LOGIC;
  signal \Data_Out_int_reg_n_0_[17]\ : STD_LOGIC;
  signal \Data_Out_int_reg_n_0_[18]\ : STD_LOGIC;
  signal \Data_Out_int_reg_n_0_[19]\ : STD_LOGIC;
  signal \Data_Out_int_reg_n_0_[20]\ : STD_LOGIC;
  signal \Data_Out_int_reg_n_0_[21]\ : STD_LOGIC;
  signal \Data_Out_int_reg_n_0_[22]\ : STD_LOGIC;
  signal \Data_Out_int_reg_n_0_[23]\ : STD_LOGIC;
  signal \Data_Out_int_reg_n_0_[24]\ : STD_LOGIC;
  signal \Data_Out_int_reg_n_0_[25]\ : STD_LOGIC;
  signal \Data_Out_int_reg_n_0_[26]\ : STD_LOGIC;
  signal \Data_Out_int_reg_n_0_[27]\ : STD_LOGIC;
  signal \Data_Out_int_reg_n_0_[28]\ : STD_LOGIC;
  signal \Data_Out_int_reg_n_0_[29]\ : STD_LOGIC;
  signal \Data_Out_int_reg_n_0_[30]\ : STD_LOGIC;
  signal \FSM_onehot_state[1]_i_10_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[1]_i_11_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[1]_i_12_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[1]_i_13_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[1]_i_14_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[1]_i_15_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[1]_i_8_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[1]_i_9_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state_reg[1]_i_4_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state_reg[1]_i_4_n_1\ : STD_LOGIC;
  signal \FSM_onehot_state_reg[1]_i_4_n_2\ : STD_LOGIC;
  signal \FSM_onehot_state_reg[1]_i_4_n_3\ : STD_LOGIC;
  signal LRCLK_i_1_n_0 : STD_LOGIC;
  signal LRCLK_i_2_n_0 : STD_LOGIC;
  signal \^l_bus_in_reg[17]\ : STD_LOGIC_VECTOR ( 17 downto 0 );
  signal \^r_bus_in_reg[17]\ : STD_LOGIC_VECTOR ( 17 downto 0 );
  signal \^sr\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \^ac_bclk\ : STD_LOGIC;
  signal \^ac_lrclk\ : STD_LOGIC;
  signal ac_lrclk_count0 : STD_LOGIC;
  signal \i__carry_i_1_n_0\ : STD_LOGIC;
  signal \i__carry_i_2__0_n_0\ : STD_LOGIC;
  signal \p_0_in__0\ : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal p_17_in : STD_LOGIC;
  signal \NLW_Cnt_Bclk0_inferred__0/i__carry_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_Cnt_Bclk0_inferred__0/i__carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_FSM_onehot_state_reg[1]_i_2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_FSM_onehot_state_reg[1]_i_2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_FSM_onehot_state_reg[1]_i_4_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of BCLK_int_i_2 : label is "soft_lutpair50";
  attribute SOFT_HLUTNM of \Cnt_Bclk[1]_i_1\ : label is "soft_lutpair54";
  attribute SOFT_HLUTNM of \Cnt_Bclk[2]_i_1\ : label is "soft_lutpair54";
  attribute SOFT_HLUTNM of \Cnt_Bclk[3]_i_1\ : label is "soft_lutpair49";
  attribute SOFT_HLUTNM of \Cnt_Bclk[4]_i_2\ : label is "soft_lutpair49";
  attribute SOFT_HLUTNM of \Cnt_Lrclk[0]_i_1\ : label is "soft_lutpair55";
  attribute SOFT_HLUTNM of \Cnt_Lrclk[1]_i_1\ : label is "soft_lutpair55";
  attribute SOFT_HLUTNM of \Cnt_Lrclk[2]_i_1\ : label is "soft_lutpair52";
  attribute SOFT_HLUTNM of \Cnt_Lrclk[3]_i_1\ : label is "soft_lutpair52";
  attribute SOFT_HLUTNM of \Cnt_Lrclk[4]_i_2\ : label is "soft_lutpair51";
  attribute srl_bus_name : string;
  attribute srl_bus_name of \Data_In_int_reg[12]_srl13___U0_my_lab3_ip_v2_0_S00_AXI_inst_datapath_wrapper_inst_audio_inout_Data_In_int_reg_r_11\ : label is "\U0/my_lab3_ip_v2_0_S00_AXI_inst/datapath/wrapper_inst/audio_inout/Data_In_int_reg ";
  attribute srl_name : string;
  attribute srl_name of \Data_In_int_reg[12]_srl13___U0_my_lab3_ip_v2_0_S00_AXI_inst_datapath_wrapper_inst_audio_inout_Data_In_int_reg_r_11\ : label is "\U0/my_lab3_ip_v2_0_S00_AXI_inst/datapath/wrapper_inst/audio_inout/Data_In_int_reg[12]_srl13___U0_my_lab3_ip_v2_0_S00_AXI_inst_datapath_wrapper_inst_audio_inout_Data_In_int_reg_r_11 ";
  attribute SOFT_HLUTNM of \Data_Out_int[31]_i_2\ : label is "soft_lutpair53";
  attribute SOFT_HLUTNM of LRCLK_i_1 : label is "soft_lutpair50";
  attribute SOFT_HLUTNM of LRCLK_i_2 : label is "soft_lutpair51";
  attribute SOFT_HLUTNM of ac_lrclk_sig_prev_i_1 : label is "soft_lutpair53";
begin
  CO(0) <= \^co\(0);
  \L_bus_in_reg[17]\(17 downto 0) <= \^l_bus_in_reg[17]\(17 downto 0);
  \R_bus_in_reg[17]\(17 downto 0) <= \^r_bus_in_reg[17]\(17 downto 0);
  SR(0) <= \^sr\(0);
  ac_bclk <= \^ac_bclk\;
  ac_lrclk <= \^ac_lrclk\;
BCLK_int_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => s00_axi_aresetn,
      O => \^sr\(0)
    );
BCLK_int_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Cnt_Bclk0,
      I1 => \^ac_bclk\,
      O => BCLK_int_i_2_n_0
    );
BCLK_int_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => BCLK_int_i_2_n_0,
      Q => \^ac_bclk\,
      R => \^sr\(0)
    );
\Cnt_Bclk0_inferred__0/i__carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3 downto 2) => \NLW_Cnt_Bclk0_inferred__0/i__carry_CO_UNCONNECTED\(3 downto 2),
      CO(1) => Cnt_Bclk0,
      CO(0) => \Cnt_Bclk0_inferred__0/i__carry_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_Cnt_Bclk0_inferred__0/i__carry_O_UNCONNECTED\(3 downto 0),
      S(3 downto 2) => B"00",
      S(1) => \i__carry_i_1_n_0\,
      S(0) => \i__carry_i_2__0_n_0\
    );
\Cnt_Bclk[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \Cnt_Bclk_reg__0\(0),
      O => \p_0_in__0\(0)
    );
\Cnt_Bclk[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \Cnt_Bclk_reg__0\(1),
      I1 => \Cnt_Bclk_reg__0\(0),
      O => \p_0_in__0\(1)
    );
\Cnt_Bclk[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6A"
    )
        port map (
      I0 => \Cnt_Bclk_reg__0\(2),
      I1 => \Cnt_Bclk_reg__0\(0),
      I2 => \Cnt_Bclk_reg__0\(1),
      O => \p_0_in__0\(2)
    );
\Cnt_Bclk[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6AAA"
    )
        port map (
      I0 => \Cnt_Bclk_reg__0\(3),
      I1 => \Cnt_Bclk_reg__0\(2),
      I2 => \Cnt_Bclk_reg__0\(1),
      I3 => \Cnt_Bclk_reg__0\(0),
      O => \p_0_in__0\(3)
    );
\Cnt_Bclk[4]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => Cnt_Bclk0,
      I1 => s00_axi_aresetn,
      O => \Cnt_Bclk[4]_i_1_n_0\
    );
\Cnt_Bclk[4]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6AAAAAAA"
    )
        port map (
      I0 => \Cnt_Bclk_reg__0\(4),
      I1 => \Cnt_Bclk_reg__0\(0),
      I2 => \Cnt_Bclk_reg__0\(1),
      I3 => \Cnt_Bclk_reg__0\(2),
      I4 => \Cnt_Bclk_reg__0\(3),
      O => \p_0_in__0\(4)
    );
\Cnt_Bclk_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \p_0_in__0\(0),
      Q => \Cnt_Bclk_reg__0\(0),
      R => \Cnt_Bclk[4]_i_1_n_0\
    );
\Cnt_Bclk_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \p_0_in__0\(1),
      Q => \Cnt_Bclk_reg__0\(1),
      R => \Cnt_Bclk[4]_i_1_n_0\
    );
\Cnt_Bclk_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \p_0_in__0\(2),
      Q => \Cnt_Bclk_reg__0\(2),
      R => \Cnt_Bclk[4]_i_1_n_0\
    );
\Cnt_Bclk_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \p_0_in__0\(3),
      Q => \Cnt_Bclk_reg__0\(3),
      R => \Cnt_Bclk[4]_i_1_n_0\
    );
\Cnt_Bclk_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \p_0_in__0\(4),
      Q => \Cnt_Bclk_reg__0\(4),
      R => \Cnt_Bclk[4]_i_1_n_0\
    );
\Cnt_Lrclk[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => Cnt_Lrclk(0),
      O => \Cnt_Lrclk[0]_i_1_n_0\
    );
\Cnt_Lrclk[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Cnt_Lrclk(1),
      I1 => Cnt_Lrclk(0),
      O => \Cnt_Lrclk[1]_i_1_n_0\
    );
\Cnt_Lrclk[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6A"
    )
        port map (
      I0 => Cnt_Lrclk(2),
      I1 => Cnt_Lrclk(0),
      I2 => Cnt_Lrclk(1),
      O => \Cnt_Lrclk[2]_i_1_n_0\
    );
\Cnt_Lrclk[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6AAA"
    )
        port map (
      I0 => Cnt_Lrclk(3),
      I1 => Cnt_Lrclk(1),
      I2 => Cnt_Lrclk(0),
      I3 => Cnt_Lrclk(2),
      O => \Cnt_Lrclk[3]_i_1_n_0\
    );
\Cnt_Lrclk[4]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => Cnt_Bclk0,
      I1 => \^ac_bclk\,
      O => BCLK_Fall_int
    );
\Cnt_Lrclk[4]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6AAAAAAA"
    )
        port map (
      I0 => Cnt_Lrclk(4),
      I1 => Cnt_Lrclk(2),
      I2 => Cnt_Lrclk(0),
      I3 => Cnt_Lrclk(1),
      I4 => Cnt_Lrclk(3),
      O => \Cnt_Lrclk[4]_i_2_n_0\
    );
\Cnt_Lrclk_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => BCLK_Fall_int,
      D => \Cnt_Lrclk[0]_i_1_n_0\,
      Q => Cnt_Lrclk(0),
      R => \^sr\(0)
    );
\Cnt_Lrclk_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => BCLK_Fall_int,
      D => \Cnt_Lrclk[1]_i_1_n_0\,
      Q => Cnt_Lrclk(1),
      R => \^sr\(0)
    );
\Cnt_Lrclk_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => BCLK_Fall_int,
      D => \Cnt_Lrclk[2]_i_1_n_0\,
      Q => Cnt_Lrclk(2),
      R => \^sr\(0)
    );
\Cnt_Lrclk_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => BCLK_Fall_int,
      D => \Cnt_Lrclk[3]_i_1_n_0\,
      Q => Cnt_Lrclk(3),
      R => \^sr\(0)
    );
\Cnt_Lrclk_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => BCLK_Fall_int,
      D => \Cnt_Lrclk[4]_i_2_n_0\,
      Q => Cnt_Lrclk(4),
      R => \^sr\(0)
    );
\D_L_O_int[23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^ac_lrclk\,
      I1 => \Data_In_int[31]_i_3_n_0\,
      O => D_L_O_int
    );
\D_L_O_int_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => D_L_O_int,
      D => Data_In_int(18),
      Q => \^l_bus_in_reg[17]\(4),
      R => \^sr\(0)
    );
\D_L_O_int_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => D_L_O_int,
      D => Data_In_int(19),
      Q => \^l_bus_in_reg[17]\(5),
      R => \^sr\(0)
    );
\D_L_O_int_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => D_L_O_int,
      D => Data_In_int(20),
      Q => \^l_bus_in_reg[17]\(6),
      R => \^sr\(0)
    );
\D_L_O_int_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => D_L_O_int,
      D => Data_In_int(21),
      Q => \^l_bus_in_reg[17]\(7),
      R => \^sr\(0)
    );
\D_L_O_int_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => D_L_O_int,
      D => Data_In_int(22),
      Q => \^l_bus_in_reg[17]\(8),
      R => \^sr\(0)
    );
\D_L_O_int_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => D_L_O_int,
      D => Data_In_int(23),
      Q => \^l_bus_in_reg[17]\(9),
      R => \^sr\(0)
    );
\D_L_O_int_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => D_L_O_int,
      D => Data_In_int(24),
      Q => \^l_bus_in_reg[17]\(10),
      R => \^sr\(0)
    );
\D_L_O_int_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => D_L_O_int,
      D => Data_In_int(25),
      Q => \^l_bus_in_reg[17]\(11),
      R => \^sr\(0)
    );
\D_L_O_int_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => D_L_O_int,
      D => Data_In_int(26),
      Q => \^l_bus_in_reg[17]\(12),
      R => \^sr\(0)
    );
\D_L_O_int_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => D_L_O_int,
      D => Data_In_int(27),
      Q => \^l_bus_in_reg[17]\(13),
      R => \^sr\(0)
    );
\D_L_O_int_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => D_L_O_int,
      D => Data_In_int(28),
      Q => \^l_bus_in_reg[17]\(14),
      R => \^sr\(0)
    );
\D_L_O_int_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => D_L_O_int,
      D => Data_In_int(29),
      Q => \^l_bus_in_reg[17]\(15),
      R => \^sr\(0)
    );
\D_L_O_int_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => D_L_O_int,
      D => Data_In_int(30),
      Q => \^l_bus_in_reg[17]\(16),
      R => \^sr\(0)
    );
\D_L_O_int_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => D_L_O_int,
      D => Data_In_int(31),
      Q => \^l_bus_in_reg[17]\(17),
      R => \^sr\(0)
    );
\D_L_O_int_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => D_L_O_int,
      D => Data_In_int(14),
      Q => \^l_bus_in_reg[17]\(0),
      R => \^sr\(0)
    );
\D_L_O_int_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => D_L_O_int,
      D => Data_In_int(15),
      Q => \^l_bus_in_reg[17]\(1),
      R => \^sr\(0)
    );
\D_L_O_int_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => D_L_O_int,
      D => Data_In_int(16),
      Q => \^l_bus_in_reg[17]\(2),
      R => \^sr\(0)
    );
\D_L_O_int_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => D_L_O_int,
      D => Data_In_int(17),
      Q => \^l_bus_in_reg[17]\(3),
      R => \^sr\(0)
    );
\D_R_O_int[23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^ac_lrclk\,
      I1 => \Data_In_int[31]_i_3_n_0\,
      O => \D_R_O_int[23]_i_1_n_0\
    );
\D_R_O_int_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \D_R_O_int[23]_i_1_n_0\,
      D => Data_In_int(18),
      Q => \^r_bus_in_reg[17]\(4),
      R => \^sr\(0)
    );
\D_R_O_int_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \D_R_O_int[23]_i_1_n_0\,
      D => Data_In_int(19),
      Q => \^r_bus_in_reg[17]\(5),
      R => \^sr\(0)
    );
\D_R_O_int_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \D_R_O_int[23]_i_1_n_0\,
      D => Data_In_int(20),
      Q => \^r_bus_in_reg[17]\(6),
      R => \^sr\(0)
    );
\D_R_O_int_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \D_R_O_int[23]_i_1_n_0\,
      D => Data_In_int(21),
      Q => \^r_bus_in_reg[17]\(7),
      R => \^sr\(0)
    );
\D_R_O_int_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \D_R_O_int[23]_i_1_n_0\,
      D => Data_In_int(22),
      Q => \^r_bus_in_reg[17]\(8),
      R => \^sr\(0)
    );
\D_R_O_int_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \D_R_O_int[23]_i_1_n_0\,
      D => Data_In_int(23),
      Q => \^r_bus_in_reg[17]\(9),
      R => \^sr\(0)
    );
\D_R_O_int_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \D_R_O_int[23]_i_1_n_0\,
      D => Data_In_int(24),
      Q => \^r_bus_in_reg[17]\(10),
      R => \^sr\(0)
    );
\D_R_O_int_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \D_R_O_int[23]_i_1_n_0\,
      D => Data_In_int(25),
      Q => \^r_bus_in_reg[17]\(11),
      R => \^sr\(0)
    );
\D_R_O_int_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \D_R_O_int[23]_i_1_n_0\,
      D => Data_In_int(26),
      Q => \^r_bus_in_reg[17]\(12),
      R => \^sr\(0)
    );
\D_R_O_int_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \D_R_O_int[23]_i_1_n_0\,
      D => Data_In_int(27),
      Q => \^r_bus_in_reg[17]\(13),
      R => \^sr\(0)
    );
\D_R_O_int_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \D_R_O_int[23]_i_1_n_0\,
      D => Data_In_int(28),
      Q => \^r_bus_in_reg[17]\(14),
      R => \^sr\(0)
    );
\D_R_O_int_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \D_R_O_int[23]_i_1_n_0\,
      D => Data_In_int(29),
      Q => \^r_bus_in_reg[17]\(15),
      R => \^sr\(0)
    );
\D_R_O_int_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \D_R_O_int[23]_i_1_n_0\,
      D => Data_In_int(30),
      Q => \^r_bus_in_reg[17]\(16),
      R => \^sr\(0)
    );
\D_R_O_int_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \D_R_O_int[23]_i_1_n_0\,
      D => Data_In_int(31),
      Q => \^r_bus_in_reg[17]\(17),
      R => \^sr\(0)
    );
\D_R_O_int_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \D_R_O_int[23]_i_1_n_0\,
      D => Data_In_int(14),
      Q => \^r_bus_in_reg[17]\(0),
      R => \^sr\(0)
    );
\D_R_O_int_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \D_R_O_int[23]_i_1_n_0\,
      D => Data_In_int(15),
      Q => \^r_bus_in_reg[17]\(1),
      R => \^sr\(0)
    );
\D_R_O_int_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \D_R_O_int[23]_i_1_n_0\,
      D => Data_In_int(16),
      Q => \^r_bus_in_reg[17]\(2),
      R => \^sr\(0)
    );
\D_R_O_int_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \D_R_O_int[23]_i_1_n_0\,
      D => Data_In_int(17),
      Q => \^r_bus_in_reg[17]\(3),
      R => \^sr\(0)
    );
\Data_In_int[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => s00_axi_aresetn,
      I1 => \Data_In_int[31]_i_3_n_0\,
      O => \Data_In_int[31]_i_1_n_0\
    );
\Data_In_int[31]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => Cnt_Bclk0,
      I1 => \^ac_bclk\,
      O => p_17_in
    );
\Data_In_int[31]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFEFFFFFFFF"
    )
        port map (
      I0 => Cnt_Lrclk(3),
      I1 => Cnt_Lrclk(4),
      I2 => Cnt_Lrclk(2),
      I3 => Cnt_Lrclk(1),
      I4 => Cnt_Lrclk(0),
      I5 => BCLK_Fall_int,
      O => \Data_In_int[31]_i_3_n_0\
    );
\Data_In_int_reg[12]_srl13___U0_my_lab3_ip_v2_0_S00_AXI_inst_datapath_wrapper_inst_audio_inout_Data_In_int_reg_r_11\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '0',
      A2 => '1',
      A3 => '1',
      CE => p_17_in,
      CLK => s00_axi_aclk,
      D => ac_adc_sdata,
      Q => \Data_In_int_reg[12]_srl13___U0_my_lab3_ip_v2_0_S00_AXI_inst_datapath_wrapper_inst_audio_inout_Data_In_int_reg_r_11_n_0\
    );
\Data_In_int_reg[13]_U0_my_lab3_ip_v2_0_S00_AXI_inst_datapath_wrapper_inst_audio_inout_Data_In_int_reg_r_12\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_17_in,
      D => \Data_In_int_reg[12]_srl13___U0_my_lab3_ip_v2_0_S00_AXI_inst_datapath_wrapper_inst_audio_inout_Data_In_int_reg_r_11_n_0\,
      Q => \Data_In_int_reg[13]_U0_my_lab3_ip_v2_0_S00_AXI_inst_datapath_wrapper_inst_audio_inout_Data_In_int_reg_r_12_n_0\,
      R => '0'
    );
\Data_In_int_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_17_in,
      D => Data_In_int_reg_gate_n_0,
      Q => Data_In_int(14),
      R => \Data_In_int[31]_i_1_n_0\
    );
\Data_In_int_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_17_in,
      D => Data_In_int(14),
      Q => Data_In_int(15),
      R => \Data_In_int[31]_i_1_n_0\
    );
\Data_In_int_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_17_in,
      D => Data_In_int(15),
      Q => Data_In_int(16),
      R => \Data_In_int[31]_i_1_n_0\
    );
\Data_In_int_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_17_in,
      D => Data_In_int(16),
      Q => Data_In_int(17),
      R => \Data_In_int[31]_i_1_n_0\
    );
\Data_In_int_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_17_in,
      D => Data_In_int(17),
      Q => Data_In_int(18),
      R => \Data_In_int[31]_i_1_n_0\
    );
\Data_In_int_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_17_in,
      D => Data_In_int(18),
      Q => Data_In_int(19),
      R => \Data_In_int[31]_i_1_n_0\
    );
\Data_In_int_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_17_in,
      D => Data_In_int(19),
      Q => Data_In_int(20),
      R => \Data_In_int[31]_i_1_n_0\
    );
\Data_In_int_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_17_in,
      D => Data_In_int(20),
      Q => Data_In_int(21),
      R => \Data_In_int[31]_i_1_n_0\
    );
\Data_In_int_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_17_in,
      D => Data_In_int(21),
      Q => Data_In_int(22),
      R => \Data_In_int[31]_i_1_n_0\
    );
\Data_In_int_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_17_in,
      D => Data_In_int(22),
      Q => Data_In_int(23),
      R => \Data_In_int[31]_i_1_n_0\
    );
\Data_In_int_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_17_in,
      D => Data_In_int(23),
      Q => Data_In_int(24),
      R => \Data_In_int[31]_i_1_n_0\
    );
\Data_In_int_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_17_in,
      D => Data_In_int(24),
      Q => Data_In_int(25),
      R => \Data_In_int[31]_i_1_n_0\
    );
\Data_In_int_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_17_in,
      D => Data_In_int(25),
      Q => Data_In_int(26),
      R => \Data_In_int[31]_i_1_n_0\
    );
\Data_In_int_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_17_in,
      D => Data_In_int(26),
      Q => Data_In_int(27),
      R => \Data_In_int[31]_i_1_n_0\
    );
\Data_In_int_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_17_in,
      D => Data_In_int(27),
      Q => Data_In_int(28),
      R => \Data_In_int[31]_i_1_n_0\
    );
\Data_In_int_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_17_in,
      D => Data_In_int(28),
      Q => Data_In_int(29),
      R => \Data_In_int[31]_i_1_n_0\
    );
\Data_In_int_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_17_in,
      D => Data_In_int(29),
      Q => Data_In_int(30),
      R => \Data_In_int[31]_i_1_n_0\
    );
\Data_In_int_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_17_in,
      D => Data_In_int(30),
      Q => Data_In_int(31),
      R => \Data_In_int[31]_i_1_n_0\
    );
Data_In_int_reg_gate: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \Data_In_int_reg[13]_U0_my_lab3_ip_v2_0_S00_AXI_inst_datapath_wrapper_inst_audio_inout_Data_In_int_reg_r_12_n_0\,
      I1 => Data_In_int_reg_r_12_n_0,
      O => Data_In_int_reg_gate_n_0
    );
Data_In_int_reg_r: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_17_in,
      D => '1',
      Q => Data_In_int_reg_r_n_0,
      R => \Data_In_int[31]_i_1_n_0\
    );
Data_In_int_reg_r_0: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_17_in,
      D => Data_In_int_reg_r_n_0,
      Q => Data_In_int_reg_r_0_n_0,
      R => \Data_In_int[31]_i_1_n_0\
    );
Data_In_int_reg_r_1: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_17_in,
      D => Data_In_int_reg_r_0_n_0,
      Q => Data_In_int_reg_r_1_n_0,
      R => \Data_In_int[31]_i_1_n_0\
    );
Data_In_int_reg_r_10: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_17_in,
      D => Data_In_int_reg_r_9_n_0,
      Q => Data_In_int_reg_r_10_n_0,
      R => \Data_In_int[31]_i_1_n_0\
    );
Data_In_int_reg_r_11: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_17_in,
      D => Data_In_int_reg_r_10_n_0,
      Q => Data_In_int_reg_r_11_n_0,
      R => \Data_In_int[31]_i_1_n_0\
    );
Data_In_int_reg_r_12: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_17_in,
      D => Data_In_int_reg_r_11_n_0,
      Q => Data_In_int_reg_r_12_n_0,
      R => \Data_In_int[31]_i_1_n_0\
    );
Data_In_int_reg_r_2: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_17_in,
      D => Data_In_int_reg_r_1_n_0,
      Q => Data_In_int_reg_r_2_n_0,
      R => \Data_In_int[31]_i_1_n_0\
    );
Data_In_int_reg_r_3: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_17_in,
      D => Data_In_int_reg_r_2_n_0,
      Q => Data_In_int_reg_r_3_n_0,
      R => \Data_In_int[31]_i_1_n_0\
    );
Data_In_int_reg_r_4: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_17_in,
      D => Data_In_int_reg_r_3_n_0,
      Q => Data_In_int_reg_r_4_n_0,
      R => \Data_In_int[31]_i_1_n_0\
    );
Data_In_int_reg_r_5: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_17_in,
      D => Data_In_int_reg_r_4_n_0,
      Q => Data_In_int_reg_r_5_n_0,
      R => \Data_In_int[31]_i_1_n_0\
    );
Data_In_int_reg_r_6: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_17_in,
      D => Data_In_int_reg_r_5_n_0,
      Q => Data_In_int_reg_r_6_n_0,
      R => \Data_In_int[31]_i_1_n_0\
    );
Data_In_int_reg_r_7: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_17_in,
      D => Data_In_int_reg_r_6_n_0,
      Q => Data_In_int_reg_r_7_n_0,
      R => \Data_In_int[31]_i_1_n_0\
    );
Data_In_int_reg_r_8: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_17_in,
      D => Data_In_int_reg_r_7_n_0,
      Q => Data_In_int_reg_r_8_n_0,
      R => \Data_In_int[31]_i_1_n_0\
    );
Data_In_int_reg_r_9: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_17_in,
      D => Data_In_int_reg_r_8_n_0,
      Q => Data_In_int_reg_r_9_n_0,
      R => \Data_In_int[31]_i_1_n_0\
    );
\Data_Out_int[13]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A8CC08CC"
    )
        port map (
      I0 => \Data_Out_int[31]_i_3_n_0\,
      I1 => Q(0),
      I2 => \^ac_lrclk\,
      I3 => s00_axi_aresetn,
      I4 => \R_bus_in_reg[17]_0\(0),
      O => \Data_Out_int[13]_i_1_n_0\
    );
\Data_Out_int[14]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFB0FFFF8F800000"
    )
        port map (
      I0 => \R_bus_in_reg[17]_0\(1),
      I1 => \^ac_lrclk\,
      I2 => \Data_Out_int[31]_i_3_n_0\,
      I3 => \Data_Out_int_reg_n_0_[13]\,
      I4 => s00_axi_aresetn,
      I5 => Q(1),
      O => \Data_Out_int[14]_i_1_n_0\
    );
\Data_Out_int[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFB0FFFF8F800000"
    )
        port map (
      I0 => \R_bus_in_reg[17]_0\(2),
      I1 => \^ac_lrclk\,
      I2 => \Data_Out_int[31]_i_3_n_0\,
      I3 => \Data_Out_int_reg_n_0_[14]\,
      I4 => s00_axi_aresetn,
      I5 => Q(2),
      O => \Data_Out_int[15]_i_1_n_0\
    );
\Data_Out_int[16]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFB0FFFF8F800000"
    )
        port map (
      I0 => \R_bus_in_reg[17]_0\(3),
      I1 => \^ac_lrclk\,
      I2 => \Data_Out_int[31]_i_3_n_0\,
      I3 => \Data_Out_int_reg_n_0_[15]\,
      I4 => s00_axi_aresetn,
      I5 => Q(3),
      O => \Data_Out_int[16]_i_1_n_0\
    );
\Data_Out_int[17]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFB0FFFF8F800000"
    )
        port map (
      I0 => \R_bus_in_reg[17]_0\(4),
      I1 => \^ac_lrclk\,
      I2 => \Data_Out_int[31]_i_3_n_0\,
      I3 => \Data_Out_int_reg_n_0_[16]\,
      I4 => s00_axi_aresetn,
      I5 => Q(4),
      O => \Data_Out_int[17]_i_1_n_0\
    );
\Data_Out_int[18]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFB0FFFF8F800000"
    )
        port map (
      I0 => \R_bus_in_reg[17]_0\(5),
      I1 => \^ac_lrclk\,
      I2 => \Data_Out_int[31]_i_3_n_0\,
      I3 => \Data_Out_int_reg_n_0_[17]\,
      I4 => s00_axi_aresetn,
      I5 => Q(5),
      O => \Data_Out_int[18]_i_1_n_0\
    );
\Data_Out_int[19]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFB0FFFF8F800000"
    )
        port map (
      I0 => \R_bus_in_reg[17]_0\(6),
      I1 => \^ac_lrclk\,
      I2 => \Data_Out_int[31]_i_3_n_0\,
      I3 => \Data_Out_int_reg_n_0_[18]\,
      I4 => s00_axi_aresetn,
      I5 => Q(6),
      O => \Data_Out_int[19]_i_1_n_0\
    );
\Data_Out_int[20]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFB0FFFF8F800000"
    )
        port map (
      I0 => \R_bus_in_reg[17]_0\(7),
      I1 => \^ac_lrclk\,
      I2 => \Data_Out_int[31]_i_3_n_0\,
      I3 => \Data_Out_int_reg_n_0_[19]\,
      I4 => s00_axi_aresetn,
      I5 => Q(7),
      O => \Data_Out_int[20]_i_1_n_0\
    );
\Data_Out_int[21]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFB0FFFF8F800000"
    )
        port map (
      I0 => \R_bus_in_reg[17]_0\(8),
      I1 => \^ac_lrclk\,
      I2 => \Data_Out_int[31]_i_3_n_0\,
      I3 => \Data_Out_int_reg_n_0_[20]\,
      I4 => s00_axi_aresetn,
      I5 => Q(8),
      O => \Data_Out_int[21]_i_1_n_0\
    );
\Data_Out_int[22]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFB0FFFF8F800000"
    )
        port map (
      I0 => \R_bus_in_reg[17]_0\(9),
      I1 => \^ac_lrclk\,
      I2 => \Data_Out_int[31]_i_3_n_0\,
      I3 => \Data_Out_int_reg_n_0_[21]\,
      I4 => s00_axi_aresetn,
      I5 => Q(9),
      O => \Data_Out_int[22]_i_1_n_0\
    );
\Data_Out_int[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFB0FFFF8F800000"
    )
        port map (
      I0 => \R_bus_in_reg[17]_0\(10),
      I1 => \^ac_lrclk\,
      I2 => \Data_Out_int[31]_i_3_n_0\,
      I3 => \Data_Out_int_reg_n_0_[22]\,
      I4 => s00_axi_aresetn,
      I5 => Q(10),
      O => \Data_Out_int[23]_i_1_n_0\
    );
\Data_Out_int[24]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFB0FFFF8F800000"
    )
        port map (
      I0 => \R_bus_in_reg[17]_0\(11),
      I1 => \^ac_lrclk\,
      I2 => \Data_Out_int[31]_i_3_n_0\,
      I3 => \Data_Out_int_reg_n_0_[23]\,
      I4 => s00_axi_aresetn,
      I5 => Q(11),
      O => \Data_Out_int[24]_i_1_n_0\
    );
\Data_Out_int[25]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFB0FFFF8F800000"
    )
        port map (
      I0 => \R_bus_in_reg[17]_0\(12),
      I1 => \^ac_lrclk\,
      I2 => \Data_Out_int[31]_i_3_n_0\,
      I3 => \Data_Out_int_reg_n_0_[24]\,
      I4 => s00_axi_aresetn,
      I5 => Q(12),
      O => \Data_Out_int[25]_i_1_n_0\
    );
\Data_Out_int[26]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFB0FFFF8F800000"
    )
        port map (
      I0 => \R_bus_in_reg[17]_0\(13),
      I1 => \^ac_lrclk\,
      I2 => \Data_Out_int[31]_i_3_n_0\,
      I3 => \Data_Out_int_reg_n_0_[25]\,
      I4 => s00_axi_aresetn,
      I5 => Q(13),
      O => \Data_Out_int[26]_i_1_n_0\
    );
\Data_Out_int[27]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFB0FFFF8F800000"
    )
        port map (
      I0 => \R_bus_in_reg[17]_0\(14),
      I1 => \^ac_lrclk\,
      I2 => \Data_Out_int[31]_i_3_n_0\,
      I3 => \Data_Out_int_reg_n_0_[26]\,
      I4 => s00_axi_aresetn,
      I5 => Q(14),
      O => \Data_Out_int[27]_i_1_n_0\
    );
\Data_Out_int[28]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFB0FFFF8F800000"
    )
        port map (
      I0 => \R_bus_in_reg[17]_0\(15),
      I1 => \^ac_lrclk\,
      I2 => \Data_Out_int[31]_i_3_n_0\,
      I3 => \Data_Out_int_reg_n_0_[27]\,
      I4 => s00_axi_aresetn,
      I5 => Q(15),
      O => \Data_Out_int[28]_i_1_n_0\
    );
\Data_Out_int[29]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFB0FFFF8F800000"
    )
        port map (
      I0 => \R_bus_in_reg[17]_0\(16),
      I1 => \^ac_lrclk\,
      I2 => \Data_Out_int[31]_i_3_n_0\,
      I3 => \Data_Out_int_reg_n_0_[28]\,
      I4 => s00_axi_aresetn,
      I5 => Q(16),
      O => \Data_Out_int[29]_i_1_n_0\
    );
\Data_Out_int[30]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFB0FFFF8F800000"
    )
        port map (
      I0 => \R_bus_in_reg[17]_0\(17),
      I1 => \^ac_lrclk\,
      I2 => \Data_Out_int[31]_i_3_n_0\,
      I3 => \Data_Out_int_reg_n_0_[29]\,
      I4 => s00_axi_aresetn,
      I5 => Q(17),
      O => \Data_Out_int[30]_i_1_n_0\
    );
\Data_Out_int[31]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF8F"
    )
        port map (
      I0 => \^ac_bclk\,
      I1 => Cnt_Bclk0,
      I2 => s00_axi_aresetn,
      I3 => \Data_Out_int[31]_i_3_n_0\,
      O => \Data_Out_int[31]_i_1_n_0\
    );
\Data_Out_int[31]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \Data_Out_int_reg_n_0_[30]\,
      I1 => s00_axi_aresetn,
      I2 => \Data_Out_int[31]_i_3_n_0\,
      O => \Data_Out_int[31]_i_2_n_0\
    );
\Data_Out_int[31]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000002"
    )
        port map (
      I0 => p_17_in,
      I1 => Cnt_Lrclk(3),
      I2 => Cnt_Lrclk(4),
      I3 => Cnt_Lrclk(2),
      I4 => Cnt_Lrclk(1),
      I5 => Cnt_Lrclk(0),
      O => \Data_Out_int[31]_i_3_n_0\
    );
\Data_Out_int_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \Data_Out_int[31]_i_1_n_0\,
      D => \Data_Out_int[13]_i_1_n_0\,
      Q => \Data_Out_int_reg_n_0_[13]\,
      R => '0'
    );
\Data_Out_int_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \Data_Out_int[31]_i_1_n_0\,
      D => \Data_Out_int[14]_i_1_n_0\,
      Q => \Data_Out_int_reg_n_0_[14]\,
      R => '0'
    );
\Data_Out_int_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \Data_Out_int[31]_i_1_n_0\,
      D => \Data_Out_int[15]_i_1_n_0\,
      Q => \Data_Out_int_reg_n_0_[15]\,
      R => '0'
    );
\Data_Out_int_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \Data_Out_int[31]_i_1_n_0\,
      D => \Data_Out_int[16]_i_1_n_0\,
      Q => \Data_Out_int_reg_n_0_[16]\,
      R => '0'
    );
\Data_Out_int_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \Data_Out_int[31]_i_1_n_0\,
      D => \Data_Out_int[17]_i_1_n_0\,
      Q => \Data_Out_int_reg_n_0_[17]\,
      R => '0'
    );
\Data_Out_int_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \Data_Out_int[31]_i_1_n_0\,
      D => \Data_Out_int[18]_i_1_n_0\,
      Q => \Data_Out_int_reg_n_0_[18]\,
      R => '0'
    );
\Data_Out_int_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \Data_Out_int[31]_i_1_n_0\,
      D => \Data_Out_int[19]_i_1_n_0\,
      Q => \Data_Out_int_reg_n_0_[19]\,
      R => '0'
    );
\Data_Out_int_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \Data_Out_int[31]_i_1_n_0\,
      D => \Data_Out_int[20]_i_1_n_0\,
      Q => \Data_Out_int_reg_n_0_[20]\,
      R => '0'
    );
\Data_Out_int_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \Data_Out_int[31]_i_1_n_0\,
      D => \Data_Out_int[21]_i_1_n_0\,
      Q => \Data_Out_int_reg_n_0_[21]\,
      R => '0'
    );
\Data_Out_int_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \Data_Out_int[31]_i_1_n_0\,
      D => \Data_Out_int[22]_i_1_n_0\,
      Q => \Data_Out_int_reg_n_0_[22]\,
      R => '0'
    );
\Data_Out_int_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \Data_Out_int[31]_i_1_n_0\,
      D => \Data_Out_int[23]_i_1_n_0\,
      Q => \Data_Out_int_reg_n_0_[23]\,
      R => '0'
    );
\Data_Out_int_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \Data_Out_int[31]_i_1_n_0\,
      D => \Data_Out_int[24]_i_1_n_0\,
      Q => \Data_Out_int_reg_n_0_[24]\,
      R => '0'
    );
\Data_Out_int_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \Data_Out_int[31]_i_1_n_0\,
      D => \Data_Out_int[25]_i_1_n_0\,
      Q => \Data_Out_int_reg_n_0_[25]\,
      R => '0'
    );
\Data_Out_int_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \Data_Out_int[31]_i_1_n_0\,
      D => \Data_Out_int[26]_i_1_n_0\,
      Q => \Data_Out_int_reg_n_0_[26]\,
      R => '0'
    );
\Data_Out_int_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \Data_Out_int[31]_i_1_n_0\,
      D => \Data_Out_int[27]_i_1_n_0\,
      Q => \Data_Out_int_reg_n_0_[27]\,
      R => '0'
    );
\Data_Out_int_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \Data_Out_int[31]_i_1_n_0\,
      D => \Data_Out_int[28]_i_1_n_0\,
      Q => \Data_Out_int_reg_n_0_[28]\,
      R => '0'
    );
\Data_Out_int_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \Data_Out_int[31]_i_1_n_0\,
      D => \Data_Out_int[29]_i_1_n_0\,
      Q => \Data_Out_int_reg_n_0_[29]\,
      R => '0'
    );
\Data_Out_int_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \Data_Out_int[31]_i_1_n_0\,
      D => \Data_Out_int[30]_i_1_n_0\,
      Q => \Data_Out_int_reg_n_0_[30]\,
      R => '0'
    );
\Data_Out_int_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \Data_Out_int[31]_i_1_n_0\,
      D => \Data_Out_int[31]_i_2_n_0\,
      Q => ac_dac_sdata,
      R => '0'
    );
\FSM_onehot_state[1]_i_10\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"44D4"
    )
        port map (
      I0 => \trigger_volt_reg[8]\(3),
      I1 => \^l_bus_in_reg[17]\(13),
      I2 => \^l_bus_in_reg[17]\(12),
      I3 => \trigger_volt_reg[8]\(2),
      O => \FSM_onehot_state[1]_i_10_n_0\
    );
\FSM_onehot_state[1]_i_11\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"44D4"
    )
        port map (
      I0 => \trigger_volt_reg[8]\(1),
      I1 => \^l_bus_in_reg[17]\(11),
      I2 => \^l_bus_in_reg[17]\(10),
      I3 => \trigger_volt_reg[8]\(0),
      O => \FSM_onehot_state[1]_i_11_n_0\
    );
\FSM_onehot_state[1]_i_12\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6006"
    )
        port map (
      I0 => \^l_bus_in_reg[17]\(17),
      I1 => \trigger_volt_reg[8]\(7),
      I2 => \^l_bus_in_reg[17]\(16),
      I3 => \trigger_volt_reg[8]\(6),
      O => \FSM_onehot_state[1]_i_12_n_0\
    );
\FSM_onehot_state[1]_i_13\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^l_bus_in_reg[17]\(15),
      I1 => \trigger_volt_reg[8]\(5),
      I2 => \^l_bus_in_reg[17]\(14),
      I3 => \trigger_volt_reg[8]\(4),
      O => \FSM_onehot_state[1]_i_13_n_0\
    );
\FSM_onehot_state[1]_i_14\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^l_bus_in_reg[17]\(13),
      I1 => \trigger_volt_reg[8]\(3),
      I2 => \^l_bus_in_reg[17]\(12),
      I3 => \trigger_volt_reg[8]\(2),
      O => \FSM_onehot_state[1]_i_14_n_0\
    );
\FSM_onehot_state[1]_i_15\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^l_bus_in_reg[17]\(11),
      I1 => \trigger_volt_reg[8]\(1),
      I2 => \^l_bus_in_reg[17]\(10),
      I3 => \trigger_volt_reg[8]\(0),
      O => \FSM_onehot_state[1]_i_15_n_0\
    );
\FSM_onehot_state[1]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1171"
    )
        port map (
      I0 => \trigger_volt_reg[8]\(7),
      I1 => \^l_bus_in_reg[17]\(17),
      I2 => \^l_bus_in_reg[17]\(16),
      I3 => \trigger_volt_reg[8]\(6),
      O => \FSM_onehot_state[1]_i_8_n_0\
    );
\FSM_onehot_state[1]_i_9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"44D4"
    )
        port map (
      I0 => \trigger_volt_reg[8]\(5),
      I1 => \^l_bus_in_reg[17]\(15),
      I2 => \^l_bus_in_reg[17]\(14),
      I3 => \trigger_volt_reg[8]\(4),
      O => \FSM_onehot_state[1]_i_9_n_0\
    );
\FSM_onehot_state[2]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^co\(0),
      I1 => \trigger_volt_reg[9]\(0),
      O => w_sw(0)
    );
\FSM_onehot_state_reg[1]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \FSM_onehot_state_reg[1]_i_4_n_0\,
      CO(3 downto 1) => \NLW_FSM_onehot_state_reg[1]_i_2_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \^co\(0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_FSM_onehot_state_reg[1]_i_2_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => B"000",
      S(0) => S(0)
    );
\FSM_onehot_state_reg[1]_i_4\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \FSM_onehot_state_reg[1]_i_4_n_0\,
      CO(2) => \FSM_onehot_state_reg[1]_i_4_n_1\,
      CO(1) => \FSM_onehot_state_reg[1]_i_4_n_2\,
      CO(0) => \FSM_onehot_state_reg[1]_i_4_n_3\,
      CYINIT => '1',
      DI(3) => \FSM_onehot_state[1]_i_8_n_0\,
      DI(2) => \FSM_onehot_state[1]_i_9_n_0\,
      DI(1) => \FSM_onehot_state[1]_i_10_n_0\,
      DI(0) => \FSM_onehot_state[1]_i_11_n_0\,
      O(3 downto 0) => \NLW_FSM_onehot_state_reg[1]_i_4_O_UNCONNECTED\(3 downto 0),
      S(3) => \FSM_onehot_state[1]_i_12_n_0\,
      S(2) => \FSM_onehot_state[1]_i_13_n_0\,
      S(1) => \FSM_onehot_state[1]_i_14_n_0\,
      S(0) => \FSM_onehot_state[1]_i_15_n_0\
    );
LRCLK_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => Cnt_Lrclk(4),
      I1 => LRCLK_i_2_n_0,
      I2 => Cnt_Bclk0,
      I3 => \^ac_bclk\,
      I4 => \^ac_lrclk\,
      O => LRCLK_i_1_n_0
    );
LRCLK_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => Cnt_Lrclk(3),
      I1 => Cnt_Lrclk(1),
      I2 => Cnt_Lrclk(0),
      I3 => Cnt_Lrclk(2),
      O => LRCLK_i_2_n_0
    );
LRCLK_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => LRCLK_i_1_n_0,
      Q => \^ac_lrclk\,
      R => \^sr\(0)
    );
\UnL_bus_out2[17]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^l_bus_in_reg[17]\(17),
      O => D(0)
    );
ac_lrclk_sig_prev_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D8"
    )
        port map (
      I0 => s00_axi_aresetn,
      I1 => \^ac_lrclk\,
      I2 => ac_lrclk_sig_prev_reg_0,
      O => ac_lrclk_sig_prev_reg
    );
\i__carry_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \Cnt_Bclk_reg__0\(3),
      I1 => \Cnt_Bclk_reg__0\(4),
      O => \i__carry_i_1_n_0\
    );
\i__carry_i_2__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => \Cnt_Bclk_reg__0\(2),
      I1 => \Cnt_Bclk_reg__0\(0),
      I2 => \Cnt_Bclk_reg__0\(1),
      O => \i__carry_i_2__0_n_0\
    );
ready_sig_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"E2A2A2A2A2A2A2A2"
    )
        port map (
      I0 => E(0),
      I1 => s00_axi_aresetn,
      I2 => ac_lrclk_count0,
      I3 => ac_lrclk_count(1),
      I4 => ac_lrclk_count(0),
      I5 => ac_lrclk_count(2),
      O => ready_sig_reg
    );
ready_sig_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^ac_lrclk\,
      I1 => ac_lrclk_sig_prev_reg_0,
      O => ac_lrclk_count0
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_10__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg10_reg[15]\(6),
      I1 => switch(0),
      I2 => \^r_bus_in_reg[17]\(8),
      O => \sdp_bl.ramb18_dp_bl.ram18_bl\(6)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_11__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg10_reg[15]\(5),
      I1 => switch(0),
      I2 => \^r_bus_in_reg[17]\(7),
      O => \sdp_bl.ramb18_dp_bl.ram18_bl\(5)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_12\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8B"
    )
        port map (
      I0 => \slv_reg9_reg[15]\(15),
      I1 => switch(0),
      I2 => \^l_bus_in_reg[17]\(17),
      O => DIBDI(15)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_12__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg10_reg[15]\(4),
      I1 => switch(0),
      I2 => \^r_bus_in_reg[17]\(6),
      O => \sdp_bl.ramb18_dp_bl.ram18_bl\(4)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_13\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg9_reg[15]\(14),
      I1 => switch(0),
      I2 => \^l_bus_in_reg[17]\(16),
      O => DIBDI(14)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_13__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg10_reg[15]\(3),
      I1 => switch(0),
      I2 => \^r_bus_in_reg[17]\(5),
      O => \sdp_bl.ramb18_dp_bl.ram18_bl\(3)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_14\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg9_reg[15]\(13),
      I1 => switch(0),
      I2 => \^l_bus_in_reg[17]\(15),
      O => DIBDI(13)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_14__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg10_reg[15]\(2),
      I1 => switch(0),
      I2 => \^r_bus_in_reg[17]\(4),
      O => \sdp_bl.ramb18_dp_bl.ram18_bl\(2)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_15\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg9_reg[15]\(12),
      I1 => switch(0),
      I2 => \^l_bus_in_reg[17]\(14),
      O => DIBDI(12)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_15__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg10_reg[15]\(1),
      I1 => switch(0),
      I2 => \^r_bus_in_reg[17]\(3),
      O => \sdp_bl.ramb18_dp_bl.ram18_bl\(1)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_16\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg9_reg[15]\(11),
      I1 => switch(0),
      I2 => \^l_bus_in_reg[17]\(13),
      O => DIBDI(11)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_16__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg10_reg[15]\(0),
      I1 => switch(0),
      I2 => \^r_bus_in_reg[17]\(2),
      O => \sdp_bl.ramb18_dp_bl.ram18_bl\(0)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_17\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg9_reg[15]\(10),
      I1 => switch(0),
      I2 => \^l_bus_in_reg[17]\(12),
      O => DIBDI(10)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_18\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg9_reg[15]\(9),
      I1 => switch(0),
      I2 => \^l_bus_in_reg[17]\(11),
      O => DIBDI(9)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_19\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg9_reg[15]\(8),
      I1 => switch(0),
      I2 => \^l_bus_in_reg[17]\(10),
      O => DIBDI(8)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"A3"
    )
        port map (
      I0 => \slv_reg10_reg[15]\(15),
      I1 => \^r_bus_in_reg[17]\(17),
      I2 => switch(0),
      O => \sdp_bl.ramb18_dp_bl.ram18_bl\(15)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_20\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg9_reg[15]\(7),
      I1 => switch(0),
      I2 => \^l_bus_in_reg[17]\(9),
      O => DIBDI(7)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_21\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg9_reg[15]\(6),
      I1 => switch(0),
      I2 => \^l_bus_in_reg[17]\(8),
      O => DIBDI(6)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_22\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg9_reg[15]\(5),
      I1 => switch(0),
      I2 => \^l_bus_in_reg[17]\(7),
      O => DIBDI(5)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_23\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg9_reg[15]\(4),
      I1 => switch(0),
      I2 => \^l_bus_in_reg[17]\(6),
      O => DIBDI(4)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_24\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg9_reg[15]\(3),
      I1 => switch(0),
      I2 => \^l_bus_in_reg[17]\(5),
      O => DIBDI(3)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_25\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg9_reg[15]\(2),
      I1 => switch(0),
      I2 => \^l_bus_in_reg[17]\(4),
      O => DIBDI(2)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_26\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg9_reg[15]\(1),
      I1 => switch(0),
      I2 => \^l_bus_in_reg[17]\(3),
      O => DIBDI(1)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_27\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg9_reg[15]\(0),
      I1 => switch(0),
      I2 => \^l_bus_in_reg[17]\(2),
      O => DIBDI(0)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_2__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg10_reg[15]\(14),
      I1 => switch(0),
      I2 => \^r_bus_in_reg[17]\(16),
      O => \sdp_bl.ramb18_dp_bl.ram18_bl\(14)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_3__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg10_reg[15]\(13),
      I1 => switch(0),
      I2 => \^r_bus_in_reg[17]\(15),
      O => \sdp_bl.ramb18_dp_bl.ram18_bl\(13)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_4__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg10_reg[15]\(12),
      I1 => switch(0),
      I2 => \^r_bus_in_reg[17]\(14),
      O => \sdp_bl.ramb18_dp_bl.ram18_bl\(12)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_5__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg10_reg[15]\(11),
      I1 => switch(0),
      I2 => \^r_bus_in_reg[17]\(13),
      O => \sdp_bl.ramb18_dp_bl.ram18_bl\(11)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_6__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg10_reg[15]\(10),
      I1 => switch(0),
      I2 => \^r_bus_in_reg[17]\(12),
      O => \sdp_bl.ramb18_dp_bl.ram18_bl\(10)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_7__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg10_reg[15]\(9),
      I1 => switch(0),
      I2 => \^r_bus_in_reg[17]\(11),
      O => \sdp_bl.ramb18_dp_bl.ram18_bl\(9)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_8__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg10_reg[15]\(8),
      I1 => switch(0),
      I2 => \^r_bus_in_reg[17]\(10),
      O => \sdp_bl.ramb18_dp_bl.ram18_bl\(8)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_9__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg10_reg[15]\(7),
      I1 => switch(0),
      I2 => \^r_bus_in_reg[17]\(9),
      O => \sdp_bl.ramb18_dp_bl.ram18_bl\(7)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_lab2_fsm is
  port (
    SR : out STD_LOGIC_VECTOR ( 0 to 0 );
    Q : out STD_LOGIC_VECTOR ( 1 downto 0 );
    WREN : out STD_LOGIC;
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    s00_axi_aresetn : in STD_LOGIC;
    \slv_reg7_reg[0]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    switch : in STD_LOGIC_VECTOR ( 0 to 0 );
    SS : in STD_LOGIC_VECTOR ( 0 to 0 );
    s00_axi_aclk : in STD_LOGIC;
    \i_sig_reg[9]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \i_sig_reg[5]\ : in STD_LOGIC;
    w_sw : in STD_LOGIC_VECTOR ( 2 downto 0 );
    CO : in STD_LOGIC_VECTOR ( 0 to 0 );
    \trigger_volt_reg[9]\ : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_lab2_fsm;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_lab2_fsm is
  signal \FSM_onehot_state[0]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[1]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[2]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[3]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state_reg_n_0_[0]\ : STD_LOGIC;
  attribute RTL_KEEP : string;
  attribute RTL_KEEP of \FSM_onehot_state_reg_n_0_[0]\ : signal is "yes";
  signal \FSM_onehot_state_reg_n_0_[1]\ : STD_LOGIC;
  attribute RTL_KEEP of \FSM_onehot_state_reg_n_0_[1]\ : signal is "yes";
  signal \FSM_onehot_state_reg_n_0_[2]\ : STD_LOGIC;
  attribute RTL_KEEP of \FSM_onehot_state_reg_n_0_[2]\ : signal is "yes";
  signal \FSM_onehot_state_reg_n_0_[3]\ : STD_LOGIC;
  attribute RTL_KEEP of \FSM_onehot_state_reg_n_0_[3]\ : signal is "yes";
  signal \FSM_onehot_state_reg_n_0_[4]\ : STD_LOGIC;
  attribute RTL_KEEP of \FSM_onehot_state_reg_n_0_[4]\ : signal is "yes";
  signal \FSM_onehot_state_reg_n_0_[5]\ : STD_LOGIC;
  attribute RTL_KEEP of \FSM_onehot_state_reg_n_0_[5]\ : signal is "yes";
  signal \^q\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \cw_reg[0]_i_1_n_0\ : STD_LOGIC;
  signal \cw_reg[1]_i_1_n_0\ : STD_LOGIC;
  signal \cw_reg[2]_i_1_n_0\ : STD_LOGIC;
  signal \cw_reg[2]_i_2_n_0\ : STD_LOGIC;
  signal w_cw : STD_LOGIC_VECTOR ( 0 to 0 );
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg[0]\ : label is "countinc:010000,countcmp:100000,waittrigger:000010,write:001000,waitready:000100,init:000001";
  attribute KEEP : string;
  attribute KEEP of \FSM_onehot_state_reg[0]\ : label is "yes";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg[1]\ : label is "countinc:010000,countcmp:100000,waittrigger:000010,write:001000,waitready:000100,init:000001";
  attribute KEEP of \FSM_onehot_state_reg[1]\ : label is "yes";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg[2]\ : label is "countinc:010000,countcmp:100000,waittrigger:000010,write:001000,waitready:000100,init:000001";
  attribute KEEP of \FSM_onehot_state_reg[2]\ : label is "yes";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg[3]\ : label is "countinc:010000,countcmp:100000,waittrigger:000010,write:001000,waitready:000100,init:000001";
  attribute KEEP of \FSM_onehot_state_reg[3]\ : label is "yes";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg[4]\ : label is "countinc:010000,countcmp:100000,waittrigger:000010,write:001000,waitready:000100,init:000001";
  attribute KEEP of \FSM_onehot_state_reg[4]\ : label is "yes";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg[5]\ : label is "countinc:010000,countcmp:100000,waittrigger:000010,write:001000,waitready:000100,init:000001";
  attribute KEEP of \FSM_onehot_state_reg[5]\ : label is "yes";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of \cw_reg[0]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \cw_reg[1]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \cw_reg[2]\ : label is "LD";
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \i_sig[9]_i_1\ : label is "soft_lutpair93";
  attribute SOFT_HLUTNM of \i_sig[9]_i_2\ : label is "soft_lutpair93";
begin
  Q(1 downto 0) <= \^q\(1 downto 0);
\FSM_onehot_state[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[5]\,
      I1 => \i_sig_reg[9]\(2),
      I2 => \i_sig_reg[9]\(0),
      I3 => \i_sig_reg[5]\,
      I4 => \i_sig_reg[9]\(1),
      I5 => \i_sig_reg[9]\(3),
      O => \FSM_onehot_state[0]_i_1_n_0\
    );
\FSM_onehot_state[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BFAA"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[0]\,
      I1 => CO(0),
      I2 => \trigger_volt_reg[9]\(0),
      I3 => \FSM_onehot_state_reg_n_0_[1]\,
      O => \FSM_onehot_state[1]_i_1_n_0\
    );
\FSM_onehot_state[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF444F444F444"
    )
        port map (
      I0 => w_sw(1),
      I1 => \FSM_onehot_state_reg_n_0_[2]\,
      I2 => \FSM_onehot_state_reg_n_0_[1]\,
      I3 => w_sw(0),
      I4 => \FSM_onehot_state_reg_n_0_[5]\,
      I5 => w_sw(2),
      O => \FSM_onehot_state[2]_i_1_n_0\
    );
\FSM_onehot_state[3]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[2]\,
      I1 => w_sw(1),
      O => \FSM_onehot_state[3]_i_1_n_0\
    );
\FSM_onehot_state_reg[0]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \FSM_onehot_state[0]_i_1_n_0\,
      Q => \FSM_onehot_state_reg_n_0_[0]\,
      S => SS(0)
    );
\FSM_onehot_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \FSM_onehot_state[1]_i_1_n_0\,
      Q => \FSM_onehot_state_reg_n_0_[1]\,
      R => SS(0)
    );
\FSM_onehot_state_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \FSM_onehot_state[2]_i_1_n_0\,
      Q => \FSM_onehot_state_reg_n_0_[2]\,
      R => SS(0)
    );
\FSM_onehot_state_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \FSM_onehot_state[3]_i_1_n_0\,
      Q => \FSM_onehot_state_reg_n_0_[3]\,
      R => SS(0)
    );
\FSM_onehot_state_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \FSM_onehot_state_reg_n_0_[3]\,
      Q => \FSM_onehot_state_reg_n_0_[4]\,
      R => SS(0)
    );
\FSM_onehot_state_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \FSM_onehot_state_reg_n_0_[4]\,
      Q => \FSM_onehot_state_reg_n_0_[5]\,
      R => SS(0)
    );
\cw_reg[0]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \cw_reg[0]_i_1_n_0\,
      G => \cw_reg[2]_i_2_n_0\,
      GE => '1',
      Q => w_cw(0)
    );
\cw_reg[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0002"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[3]\,
      I1 => \FSM_onehot_state_reg_n_0_[5]\,
      I2 => \FSM_onehot_state_reg_n_0_[2]\,
      I3 => \FSM_onehot_state_reg_n_0_[1]\,
      O => \cw_reg[0]_i_1_n_0\
    );
\cw_reg[1]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \cw_reg[1]_i_1_n_0\,
      G => \cw_reg[2]_i_2_n_0\,
      GE => '1',
      Q => \^q\(0)
    );
\cw_reg[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[5]\,
      I1 => \FSM_onehot_state_reg_n_0_[2]\,
      I2 => \FSM_onehot_state_reg_n_0_[1]\,
      I3 => \FSM_onehot_state_reg_n_0_[3]\,
      O => \cw_reg[1]_i_1_n_0\
    );
\cw_reg[2]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \cw_reg[2]_i_1_n_0\,
      G => \cw_reg[2]_i_2_n_0\,
      GE => '1',
      Q => \^q\(1)
    );
\cw_reg[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000001"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[4]\,
      I1 => \FSM_onehot_state_reg_n_0_[3]\,
      I2 => \FSM_onehot_state_reg_n_0_[5]\,
      I3 => \FSM_onehot_state_reg_n_0_[2]\,
      I4 => \FSM_onehot_state_reg_n_0_[1]\,
      O => \cw_reg[2]_i_1_n_0\
    );
\cw_reg[2]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[5]\,
      I1 => \FSM_onehot_state_reg_n_0_[2]\,
      I2 => \FSM_onehot_state_reg_n_0_[1]\,
      I3 => \FSM_onehot_state_reg_n_0_[4]\,
      I4 => \FSM_onehot_state_reg_n_0_[0]\,
      I5 => \FSM_onehot_state_reg_n_0_[3]\,
      O => \cw_reg[2]_i_2_n_0\
    );
\i_sig[9]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      I2 => s00_axi_aresetn,
      O => SR(0)
    );
\i_sig[9]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      O => E(0)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg7_reg[0]\(0),
      I1 => switch(0),
      I2 => w_cw(0),
      O => WREN
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_unimacro_BRAM_SDP_MACRO is
  port (
    DOADO : out STD_LOGIC_VECTOR ( 4 downto 0 );
    S : out STD_LOGIC_VECTOR ( 0 to 0 );
    \encoded_reg[8]\ : out STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC;
    WREN : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 9 downto 0 );
    ADDRBWRADDR : in STD_LOGIC_VECTOR ( 9 downto 0 );
    DIBDI : in STD_LOGIC_VECTOR ( 15 downto 0 );
    \processRow_reg[5]\ : in STD_LOGIC_VECTOR ( 2 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_unimacro_BRAM_SDP_MACRO;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_unimacro_BRAM_SDP_MACRO is
  signal \^doado\ : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal \encoded[8]_i_16_n_0\ : STD_LOGIC;
  signal \encoded[8]_i_17_n_0\ : STD_LOGIC;
  signal readL : STD_LOGIC_VECTOR ( 13 downto 11 );
  signal \sdp_bl.ramb18_dp_bl.ram18_bl_n_10\ : STD_LOGIC;
  signal \sdp_bl.ramb18_dp_bl.ram18_bl_n_11\ : STD_LOGIC;
  signal \sdp_bl.ramb18_dp_bl.ram18_bl_n_12\ : STD_LOGIC;
  signal \sdp_bl.ramb18_dp_bl.ram18_bl_n_13\ : STD_LOGIC;
  signal \sdp_bl.ramb18_dp_bl.ram18_bl_n_14\ : STD_LOGIC;
  signal \sdp_bl.ramb18_dp_bl.ram18_bl_n_15\ : STD_LOGIC;
  signal \sdp_bl.ramb18_dp_bl.ram18_bl_n_8\ : STD_LOGIC;
  signal \sdp_bl.ramb18_dp_bl.ram18_bl_n_9\ : STD_LOGIC;
  signal \NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOBDO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOPADOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOPBDOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \encoded[8]_i_16\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \encoded[8]_i_17\ : label is "soft_lutpair0";
  attribute box_type : string;
  attribute box_type of \sdp_bl.ramb18_dp_bl.ram18_bl\ : label is "PRIMITIVE";
begin
  DOADO(4 downto 0) <= \^doado\(4 downto 0);
\encoded[8]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000006609006"
    )
        port map (
      I0 => readL(13),
      I1 => \processRow_reg[5]\(2),
      I2 => \processRow_reg[5]\(1),
      I3 => \encoded[8]_i_16_n_0\,
      I4 => readL(12),
      I5 => \encoded[8]_i_17_n_0\,
      O => S(0)
    );
\encoded[8]_i_15\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAAAAAAA888"
    )
        port map (
      I0 => readL(13),
      I1 => readL(12),
      I2 => \^doado\(0),
      I3 => \^doado\(1),
      I4 => \^doado\(2),
      I5 => readL(11),
      O => \encoded_reg[8]\
    );
\encoded[8]_i_16\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0007"
    )
        port map (
      I0 => \^doado\(0),
      I1 => \^doado\(1),
      I2 => \^doado\(2),
      I3 => readL(11),
      O => \encoded[8]_i_16_n_0\
    );
\encoded[8]_i_17\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"5666A999"
    )
        port map (
      I0 => \processRow_reg[5]\(0),
      I1 => \^doado\(2),
      I2 => \^doado\(1),
      I3 => \^doado\(0),
      I4 => readL(11),
      O => \encoded[8]_i_17_n_0\
    );
\sdp_bl.ramb18_dp_bl.ram18_bl\: unisim.vcomponents.RAMB18E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      INITP_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_00 => X"8BC28AFA8A31896988A087D8870F8646857D84B583EC8323825A819180C87FFF",
      INIT_01 => X"A74FA777A79AA7B7A7CEA7DFA7EAA7EEA7ECA7E4A7D5A7BFA7A3A77FA7547FFF",
      INIT_02 => X"A269A2D1A337A39AA3FBA458A4B1A507A55AA5A8A5F2A638A67AA6B6A6EE7FFF",
      INIT_03 => X"9B369BAB9C209C959D0C9D829DF89E6E9EE49F599FCDA041A0B2A123A192A1FE",
      INIT_04 => X"94FD954B959C95F1964A96A59704976597C9983098999904997199E09A519AC3",
      INIT_05 => X"92729276927F928E92A192BA92D892FA9321934D937E93B393ED942B946D94B3",
      INIT_06 => X"950794B6946A942493E293A6936F933E931292EB92C992AD92979286927A9273",
      INIT_07 => X"9C869BF19B5E9ACF9A4499BC993998B9983E97C7975496E6967D961895B8955D",
      INIT_08 => X"A719A667A5B4A503A452A3A1A2F2A245A199A0EEA0469FA09EFC9E5A9DBB9D1F",
      INIT_09 => X"B1B3B11AB07EAFDFAF3DAE99ADF2AD48AC9DABF0AB42AA92A9E2A930A87EA7CC",
      INIT_0A => X"B8CFB888B83AB7E6B78BB72BB6C5B659B5E8B571B4F6B476B3F1B367B2DAB248",
      INIT_0B => X"B948B97EB9ABB9CFB9EBB9FFBA0ABA0DBA08B9FBB9E7B9CAB9A6B97BB949B90F",
      INIT_0C => X"B126B1ECB2A9B35EB409B4ABB544B5D3B65AB6D7B74BB7B7B819B872B8C2B909",
      INIT_0D => X"A020A16EA2B4A3F3A529A657A77EA89CA9B1AABEABC3ACBFADB2AE9CAF7EB056",
      INIT_0E => X"87C789768B218CC68E6790039199932A94B4963997B7992F9AA19C0B9D6F9ECB",
      INIT_0F => X"6B356D0A6EE070B57288745B762C77FB79C87B927D5B7F2080E282A1845C8614",
      INIT_10 => X"4E6D502751E453A55569573058F95AC65C945E646036620963DD65B36788695E",
      INIT_11 => X"358236E2384A39B83B2D3CA93E2A3FB3414042D4446D460C47B049584B054CB7",
      INIT_12 => X"23B7249425792667275F285F29672A782B922CB42DDE2F11304B318E32D83429",
      INIT_13 => X"1AD21B1C1B6E1BCA1C2F1C9D1D141D941E1E1EB11F4D1FF220A12159221A22E4",
      INIT_14 => X"1AC71A8C1A581A2C1A0719EB19D619C919C419C819D419E91A061A2C1A5A1A92",
      INIT_15 => X"21CC213420A020121F871F021E821E071D921D231CB91C561BF81BA21B521B09",
      INIT_16 => X"2CD82C1A2B5D2AA129E5292C287427BD2709265725A724FB245123AA23072267",
      INIT_17 => X"386637B9370B365935A634F03439338032C6320A314E30902FD22F142E552D96",
      INIT_18 => X"414E40DF406B3FF33F763EF53E6F3DE53D573CC63C303B973AFA3A5939B6390F",
      INIT_19 => X"458A456E454C452544F944C744904454441343CC4380432F42D9427E421D41B8",
      INIT_1A => X"44AE44DE450A453245554574458E45A445B545C145C845CB45C845C045B445A1",
      INIT_1B => X"3FF8405540B1410B416341B8420C425D42AC42F74340438643C844074442447A",
      INIT_1C => X"39FE3A583AB33B103B6F3BCE3C2F3C903CF13D533DB63E173E793EDA3F3B3F9A",
      INIT_1D => X"35FE361F3645367136A036D5370D3749378A37CE3815386038AD38FE395139A6",
      INIT_1E => X"370A36C7368C3658362A360435E435CB35B935AD35A735A735AD35B935CA35E1",
      INIT_1F => X"3F3A3E7C3DC73D193C733BD63B3F3AB13A2B39AC393538C5385E37FE37A53754",
      INIT_20 => X"4F134DE04CB34B8E4A6F49574846473D463B4540444D4361427D41A140CC3FFF",
      INIT_21 => X"655263CA624660C65F4A5DD35C615AF3598B582856CA5572541F52D3518D504D",
      INIT_22 => X"7F1E7D787BD37A2E788976E5754273A0720070616EC56D2B6B9369FE686C66DD",
      INIT_23 => X"989F971D9597940D928090EF8F5A8DC48C2A8A8E88F0875085AF840C826880C3",
      INIT_24 => X"ADCFACAEAB85AA55A91EA7E0A69BA54FA3FDA2A5A1469FE29E799D0A9B969A1D",
      INIT_25 => X"BB59BAC5BA28B981B8D0B816B753B687B5B1B4D3B3ECB2FCB203B102AFF9AEE8",
      INIT_26 => X"BF57BF61BF62BF59BF46BF29BF02BED1BE96BE51BE02BDAABD47BCDABC64BBE3",
      INIT_27 => X"B9B3BA4FBAE3BB6FBBF2BC6DBCDFBD48BDA7BDFEBE4CBE90BECBBEFCBF24BF42",
      INIT_28 => X"AC2BAD31AE32AF2EB024B114B1FEB2E2B3C0B498B568B632B6F4B7B0B863B90F",
      INIT_29 => X"99E09B169C4B9D7F9EB19FE1A10FA23AA363A489A5ABA6CBA7E6A8FEAA11AB21",
      INIT_2A => X"869D87C688F28A208B508C828DB58EEA90209157928F93C895009639977198A9",
      INIT_2B => X"75F876E077CE78C079B87AB47BB57CBB7DC57ED37FE580FB8215833284538576",
      INIT_2C => X"6A846B0D6B9C6C326CCE6D716E196EC86F7D703870F971C0728C735F74377514",
      INIT_2D => X"6546656D659965CB66036641668566CF671F677667D36836689F690F69856A01",
      INIT_2E => X"6588656465436526650D64F864E764DB64D364CF64D164D764E364F4650A6525",
      INIT_2F => X"691A68D768936851680F67CF678F6751671566DB66A3666D6639660965DB65B0",
      INIT_30 => X"6CEE6CBF6C8D6C586C216BE76BAC6B6F6B306AF06AAF6A6D6A2A69E669A2695E",
      INIT_31 => X"6DE86DFA6E086E106E146E126E0C6E026DF36DDF6DC86DAD6D8D6D6B6D446D1B",
      INIT_32 => X"69B06A216A8C6AF16B4F6BA76BF96C456C8B6CCA6D046D386D676D8F6DB26DD0",
      INIT_33 => X"5F5E602F60FB61C16281633C63F1649F654865EB6687671E67AE683868BC6939",
      INIT_34 => X"4FBD50D551E952FB540955135619571B581959135A075AF85BE35CCA5DAB5E87",
      INIT_35 => X"3D343E633F9140C041EE431C444A457646A247CC48F54A1C4B414C634D844EA2",
      INIT_36 => X"2B452C4C2D582E682F7C309331AE32CB33EC350E3634375B388439AE3ADA3C07",
      INIT_37 => X"1DBE1E641F121FC720842147221222E323BA2498257C26652754284929432A41",
      INIT_38 => X"17E217FA181D1849187E18BD1906195719B21A151A821AF71B741BFB1C891D1F",
      INIT_39 => X"1BA01B191A9C1A2919C01961190C18C118801849181C17FA17E117D217CE17D3",
      INIT_3A => X"2922280626F425E924E723EE22FE2217213920651F9A1ED81E201D711CCD1C32",
      INIT_3B => X"3EB43D2C3BAA3A2D38B5374435D83473331531BD306C2F222DDF2CA42B712A45",
      INIT_3C => X"5928576F55B75401524C50994EE94D3B4B8F49E7484246A04502436841D24041",
      INIT_3D => X"748072D8712C6F7E6DCE6C1B6A6768B166F96540638761CC60125E575C9C5AE1",
      INIT_3E => X"8CD18B738A0F88A5873685C0844682C681417FB87E2A7C977B01796677C77625",
      INIT_3F => X"9F199E2B9D369C399B349A29991597FB96D995B094809349920B90C67FFF8E29",
      INIT_A => X"00000",
      INIT_B => X"00000",
      INIT_FILE => "NONE",
      IS_CLKARDCLK_INVERTED => '0',
      IS_CLKBWRCLK_INVERTED => '0',
      IS_ENARDEN_INVERTED => '0',
      IS_ENBWREN_INVERTED => '0',
      IS_RSTRAMARSTRAM_INVERTED => '0',
      IS_RSTRAMB_INVERTED => '0',
      IS_RSTREGARSTREG_INVERTED => '0',
      IS_RSTREGB_INVERTED => '0',
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "NONE",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"00000",
      SRVAL_B => X"00000",
      WRITE_MODE_A => "WRITE_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 0,
      WRITE_WIDTH_B => 18
    )
        port map (
      ADDRARDADDR(13 downto 4) => Q(9 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(13 downto 4) => ADDRBWRADDR(9 downto 0),
      ADDRBWRADDR(3 downto 0) => B"1111",
      CLKARDCLK => s00_axi_aclk,
      CLKBWRCLK => s00_axi_aclk,
      DIADI(15 downto 0) => B"0000000000000000",
      DIBDI(15 downto 0) => DIBDI(15 downto 0),
      DIPADIP(1 downto 0) => B"00",
      DIPBDIP(1 downto 0) => B"00",
      DOADO(15 downto 14) => \^doado\(4 downto 3),
      DOADO(13 downto 11) => readL(13 downto 11),
      DOADO(10 downto 8) => \^doado\(2 downto 0),
      DOADO(7) => \sdp_bl.ramb18_dp_bl.ram18_bl_n_8\,
      DOADO(6) => \sdp_bl.ramb18_dp_bl.ram18_bl_n_9\,
      DOADO(5) => \sdp_bl.ramb18_dp_bl.ram18_bl_n_10\,
      DOADO(4) => \sdp_bl.ramb18_dp_bl.ram18_bl_n_11\,
      DOADO(3) => \sdp_bl.ramb18_dp_bl.ram18_bl_n_12\,
      DOADO(2) => \sdp_bl.ramb18_dp_bl.ram18_bl_n_13\,
      DOADO(1) => \sdp_bl.ramb18_dp_bl.ram18_bl_n_14\,
      DOADO(0) => \sdp_bl.ramb18_dp_bl.ram18_bl_n_15\,
      DOBDO(15 downto 0) => \NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOBDO_UNCONNECTED\(15 downto 0),
      DOPADOP(1 downto 0) => \NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOPADOP_UNCONNECTED\(1 downto 0),
      DOPBDOP(1 downto 0) => \NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOPBDOP_UNCONNECTED\(1 downto 0),
      ENARDEN => '1',
      ENBWREN => WREN,
      REGCEAREGCE => '1',
      REGCEB => '0',
      RSTRAMARSTRAM => s00_axi_aresetn,
      RSTRAMB => s00_axi_aresetn,
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      WEA(1 downto 0) => B"00",
      WEBWE(3 downto 0) => B"1111"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_unimacro_BRAM_SDP_MACRO__parameterized0\ is
  port (
    DOADO : out STD_LOGIC_VECTOR ( 1 downto 0 );
    ADDRBWRADDR : out STD_LOGIC_VECTOR ( 9 downto 0 );
    S : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_aclk : in STD_LOGIC;
    WREN : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 9 downto 0 );
    DIBDI : in STD_LOGIC_VECTOR ( 15 downto 0 );
    \processRow_reg[5]\ : in STD_LOGIC_VECTOR ( 5 downto 0 );
    \slv_reg8_reg[9]\ : in STD_LOGIC_VECTOR ( 9 downto 0 );
    switch : in STD_LOGIC_VECTOR ( 0 to 0 );
    \i_sig_reg[9]\ : in STD_LOGIC_VECTOR ( 7 downto 0 );
    \i_sig_reg[4]\ : in STD_LOGIC;
    \i_sig_reg[2]\ : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_unimacro_BRAM_SDP_MACRO__parameterized0\ : entity is "unimacro_BRAM_SDP_MACRO";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_unimacro_BRAM_SDP_MACRO__parameterized0\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_unimacro_BRAM_SDP_MACRO__parameterized0\ is
  signal \^addrbwraddr\ : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal readR : STD_LOGIC_VECTOR ( 13 downto 8 );
  signal \sdp_bl.ramb18_dp_bl.ram18_bl_n_10\ : STD_LOGIC;
  signal \sdp_bl.ramb18_dp_bl.ram18_bl_n_11\ : STD_LOGIC;
  signal \sdp_bl.ramb18_dp_bl.ram18_bl_n_12\ : STD_LOGIC;
  signal \sdp_bl.ramb18_dp_bl.ram18_bl_n_13\ : STD_LOGIC;
  signal \sdp_bl.ramb18_dp_bl.ram18_bl_n_14\ : STD_LOGIC;
  signal \sdp_bl.ramb18_dp_bl.ram18_bl_n_15\ : STD_LOGIC;
  signal \sdp_bl.ramb18_dp_bl.ram18_bl_n_8\ : STD_LOGIC;
  signal \sdp_bl.ramb18_dp_bl.ram18_bl_n_9\ : STD_LOGIC;
  signal \NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOBDO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOPADOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOPBDOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute box_type : string;
  attribute box_type of \sdp_bl.ramb18_dp_bl.ram18_bl\ : label is "PRIMITIVE";
begin
  ADDRBWRADDR(9 downto 0) <= \^addrbwraddr\(9 downto 0);
\encoded[8]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => readR(12),
      I1 => \processRow_reg[5]\(4),
      I2 => \processRow_reg[5]\(5),
      I3 => readR(13),
      I4 => \processRow_reg[5]\(3),
      I5 => readR(11),
      O => S(1)
    );
\encoded[8]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => readR(10),
      I1 => \processRow_reg[5]\(2),
      I2 => \processRow_reg[5]\(1),
      I3 => readR(9),
      I4 => \processRow_reg[5]\(0),
      I5 => readR(8),
      O => S(0)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl\: unisim.vcomponents.RAMB18E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      INITP_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_00 => X"8BC28AF98A31896988A087D8870F8647857E84B583EC8323825A819180C88000",
      INIT_01 => X"9830976A96A595DF95199452938C92C591FE913790708FA98EE18E198D528C8A",
      INIT_02 => X"A462A3A2A2E0A21FA15DA09B9FD89F169E529D8F9CCB9C079B439A7F99BA98F5",
      INIT_03 => X"B03BAF81AEC6AE0BAD4FAC93ABD6AB19AA5CA99EA8E0A821A762A6A3A5E3A523",
      INIT_04 => X"BB9DBAEBBA39B985B8D2B81DB768B6B3B5FDB547B490B3D8B320B268B1AFB0F5",
      INIT_05 => X"C66DC5C5C51CC472C3C8C31DC272C1C6C119C06CBFBEBF0FBE60BDB0BD00BC4F",
      INIT_06 => X"D08FCFF2CF55CEB7CE18CD78CCD8CC37CB95CAF2CA4FC9ABC907C861C7BBC714",
      INIT_07 => X"D9EAD95BD8CAD839D7A7D714D681D5ECD557D4C1D42AD392D2F9D260D1C5D12A",
      INIT_08 => X"E268E1E7E165E0E2E05FDFDADF54DECEDE46DDBEDD35DCAADC1FDB93DB06DA79",
      INIT_09 => X"E9F4E983E910E89DE829E7B3E73DE6C6E64DE5D4E55AE4DFE463E3E5E367E2E8",
      INIT_0A => X"F07AF01AEFB8EF56EEF2EE8DEE28EDC1ED59ECF0EC86EC1BEBAFEB41EAD3EA64",
      INIT_0B => X"F5ECF59DF54EF4FDF4AAF457F403F3ADF357F2FFF2A6F24CF1F1F195F138F0DA",
      INIT_0C => X"FA3BF9FFF9C2F983F943F902F8C0F87DF839F7F3F7ACF765F71CF6D1F686F63A",
      INIT_0D => X"FD5DFD34FD0AFCDEFCB2FC84FC54FC24FBF3FBC0FB8CFB57FB20FAE9FAB0FA76",
      INIT_0E => X"FF4BFF35FF1EFF06FEEDFED2FEB6FE99FE7BFE5CFE3BFE19FDF6FDD2FDACFD85",
      INIT_0F => X"FFFFFFFDFFF9FFF5FFEFFFE8FFE0FFD7FFCCFFC0FFB3FFA5FF95FF85FF73FF5F",
      INIT_10 => X"FF77FF89FF99FFA9FFB7FFC3FFCFFFD9FFE2FFEAFFF1FFF6FFFAFFFDFFFFFFFF",
      INIT_11 => X"FDB6FDDBFDFFFE22FE44FE64FE83FEA1FEBEFED9FEF3FF0DFF24FF3BFF50FF64",
      INIT_12 => X"FABFFAF7FB2FFB65FB99FBCDFC00FC31FC61FC90FCBDFCEAFD15FD3FFD68FD90",
      INIT_13 => X"F69AF6E5F72FF777F7BFF805F84BF88FF8D2F913F954F993F9D2FA0FFA4BFA86",
      INIT_14 => X"F150F1ADF209F264F2BDF316F36DF3C4F419F46DF4C0F512F562F5B2F600F64E",
      INIT_15 => X"EAF0EB5EEBCBEC37ECA1ED0BED74EDDCEE42EEA8EF0CEF70EFD2F033F093F0F2",
      INIT_16 => X"E388E406E483E4FFE57AE5F4E66DE6E5E75CE7D2E847E8BBE92EE9A0EA11EA81",
      INIT_17 => X"DB2BDBB8DC43DCCEDD58DDE1DE69DEF1DF77DFFCE081E104E187E209E28AE309",
      INIT_18 => X"D1EED288D321D3B9D451D4E8D57ED613D6A7D73BD7CDD85FD8F0D980DA0FDA9E",
      INIT_19 => X"C7E6C88CC931C9D6CA7ACB1DCBBFCC61CD02CDA2CE41CEE0CF7ED01BD0B7D153",
      INIT_1A => X"BD2EBDDEBE8EBF3DBFEBC099C146C1F3C29FC34AC3F4C49EC548C5F0C698C740",
      INIT_1B => X"B1DFB298B350B408B4BFB576B62CB6E2B797B84CB900B9B4BA67BB1ABBCCBC7D",
      INIT_1C => X"A615A6D5A794A853A911A9CFAA8DAB4AAC07ACC4AD80AE3CAEF7AFB1B06CB125",
      INIT_1D => X"99ED9AB29B769C3A9CFE9DC29E859F48A00BA0CDA18FA251A313A3D4A494A555",
      INIT_1E => X"8D868E4D8F158FDC90A4916B923292F993BF9486954C961296D8979E98639928",
      INIT_1F => X"80FD81C6828F8357842084E985B2867B8743880C88D4899D8A658B2D8BF68CBE",
      INIT_20 => X"7472753B760376CB7794785C792579EE7AB67B7F7C487D117DDA7EA37F6C8034",
      INIT_21 => X"680368C9698F6A556B1B6BE16CA86D6E6E356EFD6FC4708B7153721A72E273AA",
      INIT_22 => X"5BD05C915D525E135ED55F98605A611D61E062A46367642C64F065B46679673E",
      INIT_23 => X"4FF550B0516A522652E2539E545B551855D556935752581058CF598F5A4F5B0F",
      INIT_24 => X"4491454345F646A9475D481248C7497C4A324AE94BA04C584D104DC84E814F3B",
      INIT_25 => X"39BF3A673B103BBA3C643D0F3DBB3E673F143FC1406F411E41CD427D432E43DF",
      INIT_26 => X"2F9A303730D43172321132B1335233F33495353835DB367F372437CA38703917",
      INIT_27 => X"263B26CB275B27ED287F291229A62A3B2AD02B672BFE2C962D2F2DC82E632EFE",
      INIT_28 => X"1DB91E3A1EBD1F401FC4204920CF215521DD226622EF237A24052491251E25AC",
      INIT_29 => X"162A169B170E178117F6186B18E2195919D21A4B1AC61B411BBE1C3B1CBA1D39",
      INIT_2A => X"0F9E0FFF106110C41128118D11F3125A12C2132C13961401146E14DB154A15B9",
      INIT_2B => X"0A280A770AC70B190B6B0BBF0C130C690CC00D180D710DCB0E260E830EE00F3F",
      INIT_2C => X"05D40611064E068D06CD070F0751079507D9081F086608AE08F80942098E09DA",
      INIT_2D => X"02AD02D70301032D035A038803B803E9041A044E048204B704EE0526055F0599",
      INIT_2E => X"00BB00D100E80100011A01350151016E018D01AD01CE01F002130238025E0285",
      INIT_2F => X"000200040008000C0012001A0022002C003700430050005F006F0080009200A6",
      INIT_30 => X"008400730062005400460039002E0024001B0014000E00090005000200010001",
      INIT_31 => X"0240021B01F801D501B4019401750157013B0120010600ED00D600BF00AA0097",
      INIT_32 => X"053204FA04C3048E0459042603F403C3039303640337030B02E002B6028E0266",
      INIT_33 => X"0953090808BE0876082F07E907A40760071D06DC069B065C061E05E105A6056B",
      INIT_34 => X"0E970E3B0DDF0D850D2B0CD30C7C0C260BD10B7D0B2B0AD90A890A3A09EB099E",
      INIT_35 => X"14F31486141913AE134312DA1271120A11A3113E10DA107710150FB40F540EF5",
      INIT_36 => X"1C571BD91B5D1AE11A6619ED197418FC18851810179B172716B4164315D21562",
      INIT_37 => X"24B024242398230E228421FB217320EC20661FE11F5D1EDA1E571DD61D551CD6",
      INIT_38 => X"2DEA2D512CB82C1F2B882AF12A5C29C72933289F280D277B26EB265B25CC253E",
      INIT_39 => X"37EF374936A435FF355C34B93417337532D53235319630F730592FBC2F202E85",
      INIT_3A => X"42A441F4414540963FE83F3A3E8D3DE13D353C8A3BDF3B363A8C39E4393C3895",
      INIT_3B => X"4DF14D384C804BC94B114A5B49A448EF483A478546D1461D456A44B844064355",
      INIT_3C => X"59B958FA583B577C56BD55FF5542548453C8530B524F519450D9501E4F644EAA",
      INIT_3D => X"65E0651B6457639362CF620B614860855FC35F005E3E5D7D5CBB5BFA5B3A5A79",
      INIT_3E => X"7247717F70B76FF06F296E616D9A6CD46C0D6B476A8069BA68F5682F676A66A5",
      INIT_3F => X"7ECF7E067D3D7C747BAC7AE37A1A7951788977C076F7762F7567749F73D6730E",
      INIT_A => X"00000",
      INIT_B => X"00000",
      INIT_FILE => "NONE",
      IS_CLKARDCLK_INVERTED => '0',
      IS_CLKBWRCLK_INVERTED => '0',
      IS_ENARDEN_INVERTED => '0',
      IS_ENBWREN_INVERTED => '0',
      IS_RSTRAMARSTRAM_INVERTED => '0',
      IS_RSTRAMB_INVERTED => '0',
      IS_RSTREGARSTREG_INVERTED => '0',
      IS_RSTREGB_INVERTED => '0',
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "NONE",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"00000",
      SRVAL_B => X"00000",
      WRITE_MODE_A => "WRITE_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 0,
      WRITE_WIDTH_B => 18
    )
        port map (
      ADDRARDADDR(13 downto 4) => Q(9 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(13 downto 4) => \^addrbwraddr\(9 downto 0),
      ADDRBWRADDR(3 downto 0) => B"1111",
      CLKARDCLK => s00_axi_aclk,
      CLKBWRCLK => s00_axi_aclk,
      DIADI(15 downto 0) => B"0000000000000000",
      DIBDI(15 downto 0) => DIBDI(15 downto 0),
      DIPADIP(1 downto 0) => B"00",
      DIPBDIP(1 downto 0) => B"00",
      DOADO(15 downto 14) => DOADO(1 downto 0),
      DOADO(13 downto 8) => readR(13 downto 8),
      DOADO(7) => \sdp_bl.ramb18_dp_bl.ram18_bl_n_8\,
      DOADO(6) => \sdp_bl.ramb18_dp_bl.ram18_bl_n_9\,
      DOADO(5) => \sdp_bl.ramb18_dp_bl.ram18_bl_n_10\,
      DOADO(4) => \sdp_bl.ramb18_dp_bl.ram18_bl_n_11\,
      DOADO(3) => \sdp_bl.ramb18_dp_bl.ram18_bl_n_12\,
      DOADO(2) => \sdp_bl.ramb18_dp_bl.ram18_bl_n_13\,
      DOADO(1) => \sdp_bl.ramb18_dp_bl.ram18_bl_n_14\,
      DOADO(0) => \sdp_bl.ramb18_dp_bl.ram18_bl_n_15\,
      DOBDO(15 downto 0) => \NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOBDO_UNCONNECTED\(15 downto 0),
      DOPADOP(1 downto 0) => \NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOPADOP_UNCONNECTED\(1 downto 0),
      DOPBDOP(1 downto 0) => \NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOPBDOP_UNCONNECTED\(1 downto 0),
      ENARDEN => '1',
      ENBWREN => WREN,
      REGCEAREGCE => '1',
      REGCEB => '0',
      RSTRAMARSTRAM => s00_axi_aresetn,
      RSTRAMB => s00_axi_aresetn,
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      WEA(1 downto 0) => B"00",
      WEBWE(3 downto 0) => B"1111"
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_10\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg8_reg[9]\(1),
      I1 => switch(0),
      I2 => \i_sig_reg[9]\(1),
      O => \^addrbwraddr\(1)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_11\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg8_reg[9]\(0),
      I1 => switch(0),
      I2 => \i_sig_reg[9]\(0),
      O => \^addrbwraddr\(0)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg8_reg[9]\(9),
      I1 => switch(0),
      I2 => \i_sig_reg[9]\(7),
      O => \^addrbwraddr\(9)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg8_reg[9]\(8),
      I1 => switch(0),
      I2 => \i_sig_reg[9]\(6),
      O => \^addrbwraddr\(8)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg8_reg[9]\(7),
      I1 => switch(0),
      I2 => \i_sig_reg[9]\(5),
      O => \^addrbwraddr\(7)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg8_reg[9]\(6),
      I1 => switch(0),
      I2 => \i_sig_reg[9]\(4),
      O => \^addrbwraddr\(6)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg8_reg[9]\(5),
      I1 => switch(0),
      I2 => \i_sig_reg[9]\(3),
      O => \^addrbwraddr\(5)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg8_reg[9]\(4),
      I1 => switch(0),
      I2 => \i_sig_reg[4]\,
      O => \^addrbwraddr\(4)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_8\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg8_reg[9]\(3),
      I1 => switch(0),
      I2 => \i_sig_reg[9]\(2),
      O => \^addrbwraddr\(3)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg8_reg[9]\(2),
      I1 => switch(0),
      I2 => \i_sig_reg[2]\,
      O => \^addrbwraddr\(2)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_audio_init is
  port (
    scl : inout STD_LOGIC;
    sda : inout STD_LOGIC;
    clk_out2 : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC;
    SR : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_audio_init;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_audio_init is
  signal data0 : STD_LOGIC_VECTOR ( 6 to 6 );
  signal data1 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal data2 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal data_i : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \data_i[0]_i_1_n_0\ : STD_LOGIC;
  signal \data_i[1]_i_1_n_0\ : STD_LOGIC;
  signal \data_i[2]_i_1_n_0\ : STD_LOGIC;
  signal \data_i[3]_i_1_n_0\ : STD_LOGIC;
  signal \data_i[4]_i_1_n_0\ : STD_LOGIC;
  signal \data_i[4]_i_2_n_0\ : STD_LOGIC;
  signal \data_i[5]_i_1_n_0\ : STD_LOGIC;
  signal \data_i[5]_i_2_n_0\ : STD_LOGIC;
  signal \data_i[6]_i_1_n_0\ : STD_LOGIC;
  signal \data_i[6]_i_2_n_0\ : STD_LOGIC;
  signal \data_i[7]_i_1_n_0\ : STD_LOGIC;
  signal delayEn : STD_LOGIC;
  signal delayEn_i_1_n_0 : STD_LOGIC;
  signal delayEn_i_2_n_0 : STD_LOGIC;
  signal delaycnt : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal delaycnt0 : STD_LOGIC;
  signal \delaycnt0_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \delaycnt0_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \delaycnt0_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \delaycnt0_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \delaycnt0_carry__0_n_0\ : STD_LOGIC;
  signal \delaycnt0_carry__0_n_1\ : STD_LOGIC;
  signal \delaycnt0_carry__0_n_2\ : STD_LOGIC;
  signal \delaycnt0_carry__0_n_3\ : STD_LOGIC;
  signal \delaycnt0_carry__0_n_4\ : STD_LOGIC;
  signal \delaycnt0_carry__0_n_5\ : STD_LOGIC;
  signal \delaycnt0_carry__0_n_6\ : STD_LOGIC;
  signal \delaycnt0_carry__0_n_7\ : STD_LOGIC;
  signal \delaycnt0_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \delaycnt0_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \delaycnt0_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \delaycnt0_carry__1_i_4_n_0\ : STD_LOGIC;
  signal \delaycnt0_carry__1_n_0\ : STD_LOGIC;
  signal \delaycnt0_carry__1_n_1\ : STD_LOGIC;
  signal \delaycnt0_carry__1_n_2\ : STD_LOGIC;
  signal \delaycnt0_carry__1_n_3\ : STD_LOGIC;
  signal \delaycnt0_carry__1_n_4\ : STD_LOGIC;
  signal \delaycnt0_carry__1_n_5\ : STD_LOGIC;
  signal \delaycnt0_carry__1_n_6\ : STD_LOGIC;
  signal \delaycnt0_carry__1_n_7\ : STD_LOGIC;
  signal \delaycnt0_carry__2_i_1_n_0\ : STD_LOGIC;
  signal \delaycnt0_carry__2_i_2_n_0\ : STD_LOGIC;
  signal \delaycnt0_carry__2_i_3_n_0\ : STD_LOGIC;
  signal \delaycnt0_carry__2_i_4_n_0\ : STD_LOGIC;
  signal \delaycnt0_carry__2_n_0\ : STD_LOGIC;
  signal \delaycnt0_carry__2_n_1\ : STD_LOGIC;
  signal \delaycnt0_carry__2_n_2\ : STD_LOGIC;
  signal \delaycnt0_carry__2_n_3\ : STD_LOGIC;
  signal \delaycnt0_carry__2_n_4\ : STD_LOGIC;
  signal \delaycnt0_carry__2_n_5\ : STD_LOGIC;
  signal \delaycnt0_carry__2_n_6\ : STD_LOGIC;
  signal \delaycnt0_carry__2_n_7\ : STD_LOGIC;
  signal \delaycnt0_carry__3_i_1_n_0\ : STD_LOGIC;
  signal \delaycnt0_carry__3_i_2_n_0\ : STD_LOGIC;
  signal \delaycnt0_carry__3_i_3_n_0\ : STD_LOGIC;
  signal \delaycnt0_carry__3_i_4_n_0\ : STD_LOGIC;
  signal \delaycnt0_carry__3_n_0\ : STD_LOGIC;
  signal \delaycnt0_carry__3_n_1\ : STD_LOGIC;
  signal \delaycnt0_carry__3_n_2\ : STD_LOGIC;
  signal \delaycnt0_carry__3_n_3\ : STD_LOGIC;
  signal \delaycnt0_carry__3_n_4\ : STD_LOGIC;
  signal \delaycnt0_carry__3_n_5\ : STD_LOGIC;
  signal \delaycnt0_carry__3_n_6\ : STD_LOGIC;
  signal \delaycnt0_carry__3_n_7\ : STD_LOGIC;
  signal \delaycnt0_carry__4_i_1_n_0\ : STD_LOGIC;
  signal \delaycnt0_carry__4_i_2_n_0\ : STD_LOGIC;
  signal \delaycnt0_carry__4_i_3_n_0\ : STD_LOGIC;
  signal \delaycnt0_carry__4_i_4_n_0\ : STD_LOGIC;
  signal \delaycnt0_carry__4_n_0\ : STD_LOGIC;
  signal \delaycnt0_carry__4_n_1\ : STD_LOGIC;
  signal \delaycnt0_carry__4_n_2\ : STD_LOGIC;
  signal \delaycnt0_carry__4_n_3\ : STD_LOGIC;
  signal \delaycnt0_carry__4_n_4\ : STD_LOGIC;
  signal \delaycnt0_carry__4_n_5\ : STD_LOGIC;
  signal \delaycnt0_carry__4_n_6\ : STD_LOGIC;
  signal \delaycnt0_carry__4_n_7\ : STD_LOGIC;
  signal \delaycnt0_carry__5_i_1_n_0\ : STD_LOGIC;
  signal \delaycnt0_carry__5_i_2_n_0\ : STD_LOGIC;
  signal \delaycnt0_carry__5_i_3_n_0\ : STD_LOGIC;
  signal \delaycnt0_carry__5_i_4_n_0\ : STD_LOGIC;
  signal \delaycnt0_carry__5_n_0\ : STD_LOGIC;
  signal \delaycnt0_carry__5_n_1\ : STD_LOGIC;
  signal \delaycnt0_carry__5_n_2\ : STD_LOGIC;
  signal \delaycnt0_carry__5_n_3\ : STD_LOGIC;
  signal \delaycnt0_carry__5_n_4\ : STD_LOGIC;
  signal \delaycnt0_carry__5_n_5\ : STD_LOGIC;
  signal \delaycnt0_carry__5_n_6\ : STD_LOGIC;
  signal \delaycnt0_carry__5_n_7\ : STD_LOGIC;
  signal \delaycnt0_carry__6_i_1_n_0\ : STD_LOGIC;
  signal \delaycnt0_carry__6_i_2_n_0\ : STD_LOGIC;
  signal \delaycnt0_carry__6_i_3_n_0\ : STD_LOGIC;
  signal \delaycnt0_carry__6_n_2\ : STD_LOGIC;
  signal \delaycnt0_carry__6_n_3\ : STD_LOGIC;
  signal \delaycnt0_carry__6_n_5\ : STD_LOGIC;
  signal \delaycnt0_carry__6_n_6\ : STD_LOGIC;
  signal \delaycnt0_carry__6_n_7\ : STD_LOGIC;
  signal delaycnt0_carry_i_1_n_0 : STD_LOGIC;
  signal delaycnt0_carry_i_2_n_0 : STD_LOGIC;
  signal delaycnt0_carry_i_3_n_0 : STD_LOGIC;
  signal delaycnt0_carry_i_4_n_0 : STD_LOGIC;
  signal delaycnt0_carry_n_0 : STD_LOGIC;
  signal delaycnt0_carry_n_1 : STD_LOGIC;
  signal delaycnt0_carry_n_2 : STD_LOGIC;
  signal delaycnt0_carry_n_3 : STD_LOGIC;
  signal delaycnt0_carry_n_4 : STD_LOGIC;
  signal delaycnt0_carry_n_5 : STD_LOGIC;
  signal delaycnt0_carry_n_6 : STD_LOGIC;
  signal delaycnt0_carry_n_7 : STD_LOGIC;
  signal \delaycnt[0]_i_1_n_0\ : STD_LOGIC;
  signal \initA[0]_i_1_n_0\ : STD_LOGIC;
  signal \initA[6]_i_3_n_0\ : STD_LOGIC;
  signal \initA_reg_n_0_[0]\ : STD_LOGIC;
  signal \initA_reg_n_0_[1]\ : STD_LOGIC;
  signal \initA_reg_n_0_[2]\ : STD_LOGIC;
  signal \initA_reg_n_0_[3]\ : STD_LOGIC;
  signal \initA_reg_n_0_[4]\ : STD_LOGIC;
  signal \initA_reg_n_0_[5]\ : STD_LOGIC;
  signal \initA_reg_n_0_[6]\ : STD_LOGIC;
  signal initEn : STD_LOGIC;
  signal \initWord[0]_i_1_n_0\ : STD_LOGIC;
  signal \initWord[10]_i_1_n_0\ : STD_LOGIC;
  signal \initWord[11]_i_1_n_0\ : STD_LOGIC;
  signal \initWord[12]_i_1_n_0\ : STD_LOGIC;
  signal \initWord[13]_i_1_n_0\ : STD_LOGIC;
  signal \initWord[14]_i_1_n_0\ : STD_LOGIC;
  signal \initWord[15]_i_1_n_0\ : STD_LOGIC;
  signal \initWord[16]_i_1_n_0\ : STD_LOGIC;
  signal \initWord[17]_i_1_n_0\ : STD_LOGIC;
  signal \initWord[18]_i_1_n_0\ : STD_LOGIC;
  signal \initWord[19]_i_1_n_0\ : STD_LOGIC;
  signal \initWord[20]_i_1_n_0\ : STD_LOGIC;
  signal \initWord[21]_i_1_n_0\ : STD_LOGIC;
  signal \initWord[23]_i_1_n_0\ : STD_LOGIC;
  signal \initWord[30]_i_1_n_0\ : STD_LOGIC;
  signal \initWord[30]_i_2_n_0\ : STD_LOGIC;
  signal \initWord[30]_i_3_n_0\ : STD_LOGIC;
  signal \initWord[6]_i_1_n_0\ : STD_LOGIC;
  signal \initWord[8]_i_1_n_0\ : STD_LOGIC;
  signal \initWord[9]_i_1_n_0\ : STD_LOGIC;
  signal \initWord_reg_n_0_[0]\ : STD_LOGIC;
  signal \initWord_reg_n_0_[6]\ : STD_LOGIC;
  signal msg : STD_LOGIC;
  signal msg0 : STD_LOGIC;
  signal \p_1_in__0\ : STD_LOGIC_VECTOR ( 6 downto 1 );
  signal \state[1]_i_3_n_0\ : STD_LOGIC;
  signal \state[1]_i_4_n_0\ : STD_LOGIC;
  signal \state[2]_i_2_n_0\ : STD_LOGIC;
  signal \state[3]_i_10_n_0\ : STD_LOGIC;
  signal \state[3]_i_11_n_0\ : STD_LOGIC;
  signal \state[3]_i_12_n_0\ : STD_LOGIC;
  signal \state[3]_i_13_n_0\ : STD_LOGIC;
  signal \state[3]_i_3_n_0\ : STD_LOGIC;
  signal \state[3]_i_4_n_0\ : STD_LOGIC;
  signal \state[3]_i_5_n_0\ : STD_LOGIC;
  signal \state[3]_i_6_n_0\ : STD_LOGIC;
  signal \state[3]_i_7_n_0\ : STD_LOGIC;
  signal \state[3]_i_8_n_0\ : STD_LOGIC;
  signal \state[3]_i_9_n_0\ : STD_LOGIC;
  signal \state_reg_n_0_[0]\ : STD_LOGIC;
  signal \state_reg_n_0_[1]\ : STD_LOGIC;
  signal \state_reg_n_0_[2]\ : STD_LOGIC;
  signal \state_reg_n_0_[3]\ : STD_LOGIC;
  signal stb : STD_LOGIC;
  signal stb_i_1_n_0 : STD_LOGIC;
  signal twi_controller_n_0 : STD_LOGIC;
  signal twi_controller_n_1 : STD_LOGIC;
  signal twi_controller_n_2 : STD_LOGIC;
  signal twi_controller_n_3 : STD_LOGIC;
  signal twi_controller_n_4 : STD_LOGIC;
  signal twi_controller_n_5 : STD_LOGIC;
  signal twi_controller_n_6 : STD_LOGIC;
  signal \NLW_delaycnt0_carry__6_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_delaycnt0_carry__6_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \data_i[6]_i_2\ : label is "soft_lutpair69";
  attribute SOFT_HLUTNM of delayEn_i_2 : label is "soft_lutpair69";
  attribute SOFT_HLUTNM of \delaycnt[0]_i_1\ : label is "soft_lutpair73";
  attribute SOFT_HLUTNM of \initA[0]_i_1\ : label is "soft_lutpair76";
  attribute SOFT_HLUTNM of \initA[1]_i_1\ : label is "soft_lutpair76";
  attribute SOFT_HLUTNM of \initA[2]_i_1\ : label is "soft_lutpair74";
  attribute SOFT_HLUTNM of \initA[3]_i_1\ : label is "soft_lutpair71";
  attribute SOFT_HLUTNM of \initA[4]_i_1\ : label is "soft_lutpair71";
  attribute SOFT_HLUTNM of \initWord[0]_i_1\ : label is "soft_lutpair75";
  attribute SOFT_HLUTNM of \initWord[11]_i_1\ : label is "soft_lutpair70";
  attribute SOFT_HLUTNM of \initWord[30]_i_2\ : label is "soft_lutpair75";
  attribute SOFT_HLUTNM of \initWord[30]_i_3\ : label is "soft_lutpair74";
  attribute SOFT_HLUTNM of \initWord[6]_i_1\ : label is "soft_lutpair70";
  attribute SOFT_HLUTNM of msg_i_1 : label is "soft_lutpair72";
  attribute SOFT_HLUTNM of \state[3]_i_12\ : label is "soft_lutpair73";
  attribute SOFT_HLUTNM of stb_i_1 : label is "soft_lutpair72";
begin
\data_i[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0AACC00FFFFFFFF"
    )
        port map (
      I0 => data2(0),
      I1 => data1(0),
      I2 => \initWord_reg_n_0_[0]\,
      I3 => \state_reg_n_0_[0]\,
      I4 => \state_reg_n_0_[1]\,
      I5 => \data_i[4]_i_2_n_0\,
      O => \data_i[0]_i_1_n_0\
    );
\data_i[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0010110000100000"
    )
        port map (
      I0 => \state_reg_n_0_[3]\,
      I1 => \state_reg_n_0_[2]\,
      I2 => data1(1),
      I3 => \state_reg_n_0_[1]\,
      I4 => \state_reg_n_0_[0]\,
      I5 => data2(1),
      O => \data_i[1]_i_1_n_0\
    );
\data_i[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0CCAA00FFFFFFFF"
    )
        port map (
      I0 => data1(2),
      I1 => data2(2),
      I2 => \initWord_reg_n_0_[6]\,
      I3 => \state_reg_n_0_[0]\,
      I4 => \state_reg_n_0_[1]\,
      I5 => \data_i[4]_i_2_n_0\,
      O => \data_i[2]_i_1_n_0\
    );
\data_i[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0AACC00FFFFFFFF"
    )
        port map (
      I0 => data2(3),
      I1 => data1(3),
      I2 => \initWord_reg_n_0_[6]\,
      I3 => \state_reg_n_0_[0]\,
      I4 => \state_reg_n_0_[1]\,
      I5 => \data_i[4]_i_2_n_0\,
      O => \data_i[3]_i_1_n_0\
    );
\data_i[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8AA08A0080A08000"
    )
        port map (
      I0 => \data_i[4]_i_2_n_0\,
      I1 => \initWord_reg_n_0_[6]\,
      I2 => \state_reg_n_0_[0]\,
      I3 => \state_reg_n_0_[1]\,
      I4 => data1(4),
      I5 => data2(4),
      O => \data_i[4]_i_1_n_0\
    );
\data_i[4]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \state_reg_n_0_[2]\,
      I1 => \state_reg_n_0_[3]\,
      O => \data_i[4]_i_2_n_0\
    );
\data_i[5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80000000"
    )
        port map (
      I0 => \state_reg_n_0_[2]\,
      I1 => \state_reg_n_0_[0]\,
      I2 => \state_reg_n_0_[1]\,
      I3 => \state_reg_n_0_[3]\,
      I4 => s00_axi_aresetn,
      O => \data_i[5]_i_1_n_0\
    );
\data_i[5]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0AACC00FFFFFFFF"
    )
        port map (
      I0 => data2(5),
      I1 => data1(5),
      I2 => \initWord_reg_n_0_[6]\,
      I3 => \state_reg_n_0_[0]\,
      I4 => \state_reg_n_0_[1]\,
      I5 => \data_i[4]_i_2_n_0\,
      O => \data_i[5]_i_2_n_0\
    );
\data_i[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"80800000000010FF"
    )
        port map (
      I0 => \state_reg_n_0_[1]\,
      I1 => \state_reg_n_0_[0]\,
      I2 => data0(6),
      I3 => \data_i[6]_i_2_n_0\,
      I4 => \state_reg_n_0_[2]\,
      I5 => \state_reg_n_0_[3]\,
      O => \data_i[6]_i_1_n_0\
    );
\data_i[6]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"053FF53F"
    )
        port map (
      I0 => data1(7),
      I1 => data2(6),
      I2 => \state_reg_n_0_[1]\,
      I3 => \state_reg_n_0_[0]\,
      I4 => \initWord_reg_n_0_[6]\,
      O => \data_i[6]_i_2_n_0\
    );
\data_i[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000A000C0000"
    )
        port map (
      I0 => data1(7),
      I1 => data2(7),
      I2 => \state_reg_n_0_[2]\,
      I3 => \state_reg_n_0_[3]\,
      I4 => \state_reg_n_0_[1]\,
      I5 => \state_reg_n_0_[0]\,
      O => \data_i[7]_i_1_n_0\
    );
\data_i_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => s00_axi_aresetn,
      D => \data_i[0]_i_1_n_0\,
      Q => data_i(0),
      R => \data_i[5]_i_1_n_0\
    );
\data_i_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => s00_axi_aresetn,
      D => \data_i[1]_i_1_n_0\,
      Q => data_i(1),
      R => '0'
    );
\data_i_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => s00_axi_aresetn,
      D => \data_i[2]_i_1_n_0\,
      Q => data_i(2),
      R => \data_i[5]_i_1_n_0\
    );
\data_i_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => s00_axi_aresetn,
      D => \data_i[3]_i_1_n_0\,
      Q => data_i(3),
      R => \data_i[5]_i_1_n_0\
    );
\data_i_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => s00_axi_aresetn,
      D => \data_i[4]_i_1_n_0\,
      Q => data_i(4),
      R => '0'
    );
\data_i_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => s00_axi_aresetn,
      D => \data_i[5]_i_2_n_0\,
      Q => data_i(5),
      R => \data_i[5]_i_1_n_0\
    );
\data_i_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => s00_axi_aresetn,
      D => \data_i[6]_i_1_n_0\,
      Q => data_i(6),
      R => '0'
    );
\data_i_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => s00_axi_aresetn,
      D => \data_i[7]_i_1_n_0\,
      Q => data_i(7),
      R => '0'
    );
delayEn_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A2AAAEAA00000000"
    )
        port map (
      I0 => delayEn,
      I1 => \state_reg_n_0_[2]\,
      I2 => \state_reg_n_0_[3]\,
      I3 => delayEn_i_2_n_0,
      I4 => \state[3]_i_3_n_0\,
      I5 => s00_axi_aresetn,
      O => delayEn_i_1_n_0
    );
delayEn_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \state_reg_n_0_[1]\,
      I1 => \state_reg_n_0_[0]\,
      O => delayEn_i_2_n_0
    );
delayEn_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => delayEn_i_1_n_0,
      Q => delayEn,
      R => '0'
    );
delaycnt0_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => delaycnt0_carry_n_0,
      CO(2) => delaycnt0_carry_n_1,
      CO(1) => delaycnt0_carry_n_2,
      CO(0) => delaycnt0_carry_n_3,
      CYINIT => delaycnt(0),
      DI(3 downto 0) => delaycnt(4 downto 1),
      O(3) => delaycnt0_carry_n_4,
      O(2) => delaycnt0_carry_n_5,
      O(1) => delaycnt0_carry_n_6,
      O(0) => delaycnt0_carry_n_7,
      S(3) => delaycnt0_carry_i_1_n_0,
      S(2) => delaycnt0_carry_i_2_n_0,
      S(1) => delaycnt0_carry_i_3_n_0,
      S(0) => delaycnt0_carry_i_4_n_0
    );
\delaycnt0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => delaycnt0_carry_n_0,
      CO(3) => \delaycnt0_carry__0_n_0\,
      CO(2) => \delaycnt0_carry__0_n_1\,
      CO(1) => \delaycnt0_carry__0_n_2\,
      CO(0) => \delaycnt0_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => delaycnt(8 downto 5),
      O(3) => \delaycnt0_carry__0_n_4\,
      O(2) => \delaycnt0_carry__0_n_5\,
      O(1) => \delaycnt0_carry__0_n_6\,
      O(0) => \delaycnt0_carry__0_n_7\,
      S(3) => \delaycnt0_carry__0_i_1_n_0\,
      S(2) => \delaycnt0_carry__0_i_2_n_0\,
      S(1) => \delaycnt0_carry__0_i_3_n_0\,
      S(0) => \delaycnt0_carry__0_i_4_n_0\
    );
\delaycnt0_carry__0_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => delaycnt(8),
      O => \delaycnt0_carry__0_i_1_n_0\
    );
\delaycnt0_carry__0_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => delaycnt(7),
      O => \delaycnt0_carry__0_i_2_n_0\
    );
\delaycnt0_carry__0_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => delaycnt(6),
      O => \delaycnt0_carry__0_i_3_n_0\
    );
\delaycnt0_carry__0_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => delaycnt(5),
      O => \delaycnt0_carry__0_i_4_n_0\
    );
\delaycnt0_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \delaycnt0_carry__0_n_0\,
      CO(3) => \delaycnt0_carry__1_n_0\,
      CO(2) => \delaycnt0_carry__1_n_1\,
      CO(1) => \delaycnt0_carry__1_n_2\,
      CO(0) => \delaycnt0_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => delaycnt(12 downto 9),
      O(3) => \delaycnt0_carry__1_n_4\,
      O(2) => \delaycnt0_carry__1_n_5\,
      O(1) => \delaycnt0_carry__1_n_6\,
      O(0) => \delaycnt0_carry__1_n_7\,
      S(3) => \delaycnt0_carry__1_i_1_n_0\,
      S(2) => \delaycnt0_carry__1_i_2_n_0\,
      S(1) => \delaycnt0_carry__1_i_3_n_0\,
      S(0) => \delaycnt0_carry__1_i_4_n_0\
    );
\delaycnt0_carry__1_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => delaycnt(12),
      O => \delaycnt0_carry__1_i_1_n_0\
    );
\delaycnt0_carry__1_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => delaycnt(11),
      O => \delaycnt0_carry__1_i_2_n_0\
    );
\delaycnt0_carry__1_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => delaycnt(10),
      O => \delaycnt0_carry__1_i_3_n_0\
    );
\delaycnt0_carry__1_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => delaycnt(9),
      O => \delaycnt0_carry__1_i_4_n_0\
    );
\delaycnt0_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \delaycnt0_carry__1_n_0\,
      CO(3) => \delaycnt0_carry__2_n_0\,
      CO(2) => \delaycnt0_carry__2_n_1\,
      CO(1) => \delaycnt0_carry__2_n_2\,
      CO(0) => \delaycnt0_carry__2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => delaycnt(16 downto 13),
      O(3) => \delaycnt0_carry__2_n_4\,
      O(2) => \delaycnt0_carry__2_n_5\,
      O(1) => \delaycnt0_carry__2_n_6\,
      O(0) => \delaycnt0_carry__2_n_7\,
      S(3) => \delaycnt0_carry__2_i_1_n_0\,
      S(2) => \delaycnt0_carry__2_i_2_n_0\,
      S(1) => \delaycnt0_carry__2_i_3_n_0\,
      S(0) => \delaycnt0_carry__2_i_4_n_0\
    );
\delaycnt0_carry__2_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => delaycnt(16),
      O => \delaycnt0_carry__2_i_1_n_0\
    );
\delaycnt0_carry__2_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => delaycnt(15),
      O => \delaycnt0_carry__2_i_2_n_0\
    );
\delaycnt0_carry__2_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => delaycnt(14),
      O => \delaycnt0_carry__2_i_3_n_0\
    );
\delaycnt0_carry__2_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => delaycnt(13),
      O => \delaycnt0_carry__2_i_4_n_0\
    );
\delaycnt0_carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \delaycnt0_carry__2_n_0\,
      CO(3) => \delaycnt0_carry__3_n_0\,
      CO(2) => \delaycnt0_carry__3_n_1\,
      CO(1) => \delaycnt0_carry__3_n_2\,
      CO(0) => \delaycnt0_carry__3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => delaycnt(20 downto 17),
      O(3) => \delaycnt0_carry__3_n_4\,
      O(2) => \delaycnt0_carry__3_n_5\,
      O(1) => \delaycnt0_carry__3_n_6\,
      O(0) => \delaycnt0_carry__3_n_7\,
      S(3) => \delaycnt0_carry__3_i_1_n_0\,
      S(2) => \delaycnt0_carry__3_i_2_n_0\,
      S(1) => \delaycnt0_carry__3_i_3_n_0\,
      S(0) => \delaycnt0_carry__3_i_4_n_0\
    );
\delaycnt0_carry__3_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => delaycnt(20),
      O => \delaycnt0_carry__3_i_1_n_0\
    );
\delaycnt0_carry__3_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => delaycnt(19),
      O => \delaycnt0_carry__3_i_2_n_0\
    );
\delaycnt0_carry__3_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => delaycnt(18),
      O => \delaycnt0_carry__3_i_3_n_0\
    );
\delaycnt0_carry__3_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => delaycnt(17),
      O => \delaycnt0_carry__3_i_4_n_0\
    );
\delaycnt0_carry__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \delaycnt0_carry__3_n_0\,
      CO(3) => \delaycnt0_carry__4_n_0\,
      CO(2) => \delaycnt0_carry__4_n_1\,
      CO(1) => \delaycnt0_carry__4_n_2\,
      CO(0) => \delaycnt0_carry__4_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => delaycnt(24 downto 21),
      O(3) => \delaycnt0_carry__4_n_4\,
      O(2) => \delaycnt0_carry__4_n_5\,
      O(1) => \delaycnt0_carry__4_n_6\,
      O(0) => \delaycnt0_carry__4_n_7\,
      S(3) => \delaycnt0_carry__4_i_1_n_0\,
      S(2) => \delaycnt0_carry__4_i_2_n_0\,
      S(1) => \delaycnt0_carry__4_i_3_n_0\,
      S(0) => \delaycnt0_carry__4_i_4_n_0\
    );
\delaycnt0_carry__4_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => delaycnt(24),
      O => \delaycnt0_carry__4_i_1_n_0\
    );
\delaycnt0_carry__4_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => delaycnt(23),
      O => \delaycnt0_carry__4_i_2_n_0\
    );
\delaycnt0_carry__4_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => delaycnt(22),
      O => \delaycnt0_carry__4_i_3_n_0\
    );
\delaycnt0_carry__4_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => delaycnt(21),
      O => \delaycnt0_carry__4_i_4_n_0\
    );
\delaycnt0_carry__5\: unisim.vcomponents.CARRY4
     port map (
      CI => \delaycnt0_carry__4_n_0\,
      CO(3) => \delaycnt0_carry__5_n_0\,
      CO(2) => \delaycnt0_carry__5_n_1\,
      CO(1) => \delaycnt0_carry__5_n_2\,
      CO(0) => \delaycnt0_carry__5_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => delaycnt(28 downto 25),
      O(3) => \delaycnt0_carry__5_n_4\,
      O(2) => \delaycnt0_carry__5_n_5\,
      O(1) => \delaycnt0_carry__5_n_6\,
      O(0) => \delaycnt0_carry__5_n_7\,
      S(3) => \delaycnt0_carry__5_i_1_n_0\,
      S(2) => \delaycnt0_carry__5_i_2_n_0\,
      S(1) => \delaycnt0_carry__5_i_3_n_0\,
      S(0) => \delaycnt0_carry__5_i_4_n_0\
    );
\delaycnt0_carry__5_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => delaycnt(28),
      O => \delaycnt0_carry__5_i_1_n_0\
    );
\delaycnt0_carry__5_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => delaycnt(27),
      O => \delaycnt0_carry__5_i_2_n_0\
    );
\delaycnt0_carry__5_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => delaycnt(26),
      O => \delaycnt0_carry__5_i_3_n_0\
    );
\delaycnt0_carry__5_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => delaycnt(25),
      O => \delaycnt0_carry__5_i_4_n_0\
    );
\delaycnt0_carry__6\: unisim.vcomponents.CARRY4
     port map (
      CI => \delaycnt0_carry__5_n_0\,
      CO(3 downto 2) => \NLW_delaycnt0_carry__6_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \delaycnt0_carry__6_n_2\,
      CO(0) => \delaycnt0_carry__6_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1 downto 0) => delaycnt(30 downto 29),
      O(3) => \NLW_delaycnt0_carry__6_O_UNCONNECTED\(3),
      O(2) => \delaycnt0_carry__6_n_5\,
      O(1) => \delaycnt0_carry__6_n_6\,
      O(0) => \delaycnt0_carry__6_n_7\,
      S(3) => '0',
      S(2) => \delaycnt0_carry__6_i_1_n_0\,
      S(1) => \delaycnt0_carry__6_i_2_n_0\,
      S(0) => \delaycnt0_carry__6_i_3_n_0\
    );
\delaycnt0_carry__6_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => delaycnt(31),
      O => \delaycnt0_carry__6_i_1_n_0\
    );
\delaycnt0_carry__6_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => delaycnt(30),
      O => \delaycnt0_carry__6_i_2_n_0\
    );
\delaycnt0_carry__6_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => delaycnt(29),
      O => \delaycnt0_carry__6_i_3_n_0\
    );
delaycnt0_carry_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => delaycnt(4),
      O => delaycnt0_carry_i_1_n_0
    );
delaycnt0_carry_i_2: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => delaycnt(3),
      O => delaycnt0_carry_i_2_n_0
    );
delaycnt0_carry_i_3: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => delaycnt(2),
      O => delaycnt0_carry_i_3_n_0
    );
delaycnt0_carry_i_4: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => delaycnt(1),
      O => delaycnt0_carry_i_4_n_0
    );
\delaycnt[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => delaycnt(0),
      O => \delaycnt[0]_i_1_n_0\
    );
\delaycnt[31]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => delayEn,
      O => delaycnt0
    );
\delaycnt_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => '1',
      D => \delaycnt[0]_i_1_n_0\,
      Q => delaycnt(0),
      R => delaycnt0
    );
\delaycnt_reg[10]\: unisim.vcomponents.FDSE
     port map (
      C => clk_out2,
      CE => '1',
      D => \delaycnt0_carry__1_n_6\,
      Q => delaycnt(10),
      S => delaycnt0
    );
\delaycnt_reg[11]\: unisim.vcomponents.FDSE
     port map (
      C => clk_out2,
      CE => '1',
      D => \delaycnt0_carry__1_n_5\,
      Q => delaycnt(11),
      S => delaycnt0
    );
\delaycnt_reg[12]\: unisim.vcomponents.FDSE
     port map (
      C => clk_out2,
      CE => '1',
      D => \delaycnt0_carry__1_n_4\,
      Q => delaycnt(12),
      S => delaycnt0
    );
\delaycnt_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => '1',
      D => \delaycnt0_carry__2_n_7\,
      Q => delaycnt(13),
      R => delaycnt0
    );
\delaycnt_reg[14]\: unisim.vcomponents.FDSE
     port map (
      C => clk_out2,
      CE => '1',
      D => \delaycnt0_carry__2_n_6\,
      Q => delaycnt(14),
      S => delaycnt0
    );
\delaycnt_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => '1',
      D => \delaycnt0_carry__2_n_5\,
      Q => delaycnt(15),
      R => delaycnt0
    );
\delaycnt_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => '1',
      D => \delaycnt0_carry__2_n_4\,
      Q => delaycnt(16),
      R => delaycnt0
    );
\delaycnt_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => '1',
      D => \delaycnt0_carry__3_n_7\,
      Q => delaycnt(17),
      R => delaycnt0
    );
\delaycnt_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => '1',
      D => \delaycnt0_carry__3_n_6\,
      Q => delaycnt(18),
      R => delaycnt0
    );
\delaycnt_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => '1',
      D => \delaycnt0_carry__3_n_5\,
      Q => delaycnt(19),
      R => delaycnt0
    );
\delaycnt_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => '1',
      D => delaycnt0_carry_n_7,
      Q => delaycnt(1),
      R => delaycnt0
    );
\delaycnt_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => '1',
      D => \delaycnt0_carry__3_n_4\,
      Q => delaycnt(20),
      R => delaycnt0
    );
\delaycnt_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => '1',
      D => \delaycnt0_carry__4_n_7\,
      Q => delaycnt(21),
      R => delaycnt0
    );
\delaycnt_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => '1',
      D => \delaycnt0_carry__4_n_6\,
      Q => delaycnt(22),
      R => delaycnt0
    );
\delaycnt_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => '1',
      D => \delaycnt0_carry__4_n_5\,
      Q => delaycnt(23),
      R => delaycnt0
    );
\delaycnt_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => '1',
      D => \delaycnt0_carry__4_n_4\,
      Q => delaycnt(24),
      R => delaycnt0
    );
\delaycnt_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => '1',
      D => \delaycnt0_carry__5_n_7\,
      Q => delaycnt(25),
      R => delaycnt0
    );
\delaycnt_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => '1',
      D => \delaycnt0_carry__5_n_6\,
      Q => delaycnt(26),
      R => delaycnt0
    );
\delaycnt_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => '1',
      D => \delaycnt0_carry__5_n_5\,
      Q => delaycnt(27),
      R => delaycnt0
    );
\delaycnt_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => '1',
      D => \delaycnt0_carry__5_n_4\,
      Q => delaycnt(28),
      R => delaycnt0
    );
\delaycnt_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => '1',
      D => \delaycnt0_carry__6_n_7\,
      Q => delaycnt(29),
      R => delaycnt0
    );
\delaycnt_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => '1',
      D => delaycnt0_carry_n_6,
      Q => delaycnt(2),
      R => delaycnt0
    );
\delaycnt_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => '1',
      D => \delaycnt0_carry__6_n_6\,
      Q => delaycnt(30),
      R => delaycnt0
    );
\delaycnt_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => '1',
      D => \delaycnt0_carry__6_n_5\,
      Q => delaycnt(31),
      R => delaycnt0
    );
\delaycnt_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => '1',
      D => delaycnt0_carry_n_5,
      Q => delaycnt(3),
      R => delaycnt0
    );
\delaycnt_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => '1',
      D => delaycnt0_carry_n_4,
      Q => delaycnt(4),
      R => delaycnt0
    );
\delaycnt_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => '1',
      D => \delaycnt0_carry__0_n_7\,
      Q => delaycnt(5),
      R => delaycnt0
    );
\delaycnt_reg[6]\: unisim.vcomponents.FDSE
     port map (
      C => clk_out2,
      CE => '1',
      D => \delaycnt0_carry__0_n_6\,
      Q => delaycnt(6),
      S => delaycnt0
    );
\delaycnt_reg[7]\: unisim.vcomponents.FDSE
     port map (
      C => clk_out2,
      CE => '1',
      D => \delaycnt0_carry__0_n_5\,
      Q => delaycnt(7),
      S => delaycnt0
    );
\delaycnt_reg[8]\: unisim.vcomponents.FDSE
     port map (
      C => clk_out2,
      CE => '1',
      D => \delaycnt0_carry__0_n_4\,
      Q => delaycnt(8),
      S => delaycnt0
    );
\delaycnt_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => '1',
      D => \delaycnt0_carry__1_n_7\,
      Q => delaycnt(9),
      R => delaycnt0
    );
\initA[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \initA_reg_n_0_[0]\,
      O => \initA[0]_i_1_n_0\
    );
\initA[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \initA_reg_n_0_[0]\,
      I1 => \initA_reg_n_0_[1]\,
      O => \p_1_in__0\(1)
    );
\initA[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6A"
    )
        port map (
      I0 => \initA_reg_n_0_[2]\,
      I1 => \initA_reg_n_0_[1]\,
      I2 => \initA_reg_n_0_[0]\,
      O => \p_1_in__0\(2)
    );
\initA[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => \initA_reg_n_0_[2]\,
      I1 => \initA_reg_n_0_[0]\,
      I2 => \initA_reg_n_0_[1]\,
      I3 => \initA_reg_n_0_[3]\,
      O => \p_1_in__0\(3)
    );
\initA[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6AAAAAAA"
    )
        port map (
      I0 => \initA_reg_n_0_[4]\,
      I1 => \initA_reg_n_0_[0]\,
      I2 => \initA_reg_n_0_[1]\,
      I3 => \initA_reg_n_0_[3]\,
      I4 => \initA_reg_n_0_[2]\,
      O => \p_1_in__0\(4)
    );
\initA[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AAAAAAAAAAAAAAA"
    )
        port map (
      I0 => \initA_reg_n_0_[5]\,
      I1 => \initA_reg_n_0_[2]\,
      I2 => \initA_reg_n_0_[3]\,
      I3 => \initA_reg_n_0_[1]\,
      I4 => \initA_reg_n_0_[0]\,
      I5 => \initA_reg_n_0_[4]\,
      O => \p_1_in__0\(5)
    );
\initA[6]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AAAAAAAAAAAAAAA"
    )
        port map (
      I0 => \initA_reg_n_0_[6]\,
      I1 => \initA_reg_n_0_[5]\,
      I2 => \initA_reg_n_0_[4]\,
      I3 => \initWord[30]_i_3_n_0\,
      I4 => \initA_reg_n_0_[3]\,
      I5 => \initA_reg_n_0_[2]\,
      O => \p_1_in__0\(6)
    );
\initA[6]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80000000"
    )
        port map (
      I0 => \state[3]_i_3_n_0\,
      I1 => initEn,
      I2 => \state_reg_n_0_[2]\,
      I3 => \state_reg_n_0_[0]\,
      I4 => \state_reg_n_0_[1]\,
      O => \initA[6]_i_3_n_0\
    );
\initA_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => twi_controller_n_5,
      D => \initA[0]_i_1_n_0\,
      Q => \initA_reg_n_0_[0]\,
      R => SR(0)
    );
\initA_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => twi_controller_n_5,
      D => \p_1_in__0\(1),
      Q => \initA_reg_n_0_[1]\,
      R => SR(0)
    );
\initA_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => twi_controller_n_5,
      D => \p_1_in__0\(2),
      Q => \initA_reg_n_0_[2]\,
      R => SR(0)
    );
\initA_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => twi_controller_n_5,
      D => \p_1_in__0\(3),
      Q => \initA_reg_n_0_[3]\,
      R => SR(0)
    );
\initA_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => twi_controller_n_5,
      D => \p_1_in__0\(4),
      Q => \initA_reg_n_0_[4]\,
      R => SR(0)
    );
\initA_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => twi_controller_n_5,
      D => \p_1_in__0\(5),
      Q => \initA_reg_n_0_[5]\,
      R => SR(0)
    );
\initA_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => twi_controller_n_5,
      D => \p_1_in__0\(6),
      Q => \initA_reg_n_0_[6]\,
      R => SR(0)
    );
initEn_reg: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => '1',
      D => twi_controller_n_6,
      Q => initEn,
      R => '0'
    );
\initWord[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"A8"
    )
        port map (
      I0 => \initA_reg_n_0_[5]\,
      I1 => \initA_reg_n_0_[1]\,
      I2 => \initA_reg_n_0_[0]\,
      O => \initWord[0]_i_1_n_0\
    );
\initWord[10]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000600010C000A00"
    )
        port map (
      I0 => \initA_reg_n_0_[1]\,
      I1 => \initA_reg_n_0_[0]\,
      I2 => \initA_reg_n_0_[5]\,
      I3 => \initA_reg_n_0_[4]\,
      I4 => \initA_reg_n_0_[2]\,
      I5 => \initA_reg_n_0_[3]\,
      O => \initWord[10]_i_1_n_0\
    );
\initWord[11]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"11110010"
    )
        port map (
      I0 => \initA_reg_n_0_[1]\,
      I1 => \initA_reg_n_0_[0]\,
      I2 => \initA_reg_n_0_[3]\,
      I3 => \initA_reg_n_0_[4]\,
      I4 => \initA_reg_n_0_[5]\,
      O => \initWord[11]_i_1_n_0\
    );
\initWord[12]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000006"
    )
        port map (
      I0 => \initA_reg_n_0_[2]\,
      I1 => \initA_reg_n_0_[3]\,
      I2 => \initA_reg_n_0_[0]\,
      I3 => \initA_reg_n_0_[1]\,
      I4 => \initA_reg_n_0_[5]\,
      I5 => \initA_reg_n_0_[4]\,
      O => \initWord[12]_i_1_n_0\
    );
\initWord[13]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AABAAEEE10000010"
    )
        port map (
      I0 => \initA_reg_n_0_[5]\,
      I1 => \initA_reg_n_0_[4]\,
      I2 => \initA_reg_n_0_[3]\,
      I3 => \initA_reg_n_0_[0]\,
      I4 => \initA_reg_n_0_[2]\,
      I5 => \initA_reg_n_0_[1]\,
      O => \initWord[13]_i_1_n_0\
    );
\initWord[14]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000010180884"
    )
        port map (
      I0 => \initA_reg_n_0_[2]\,
      I1 => \initA_reg_n_0_[3]\,
      I2 => \initA_reg_n_0_[1]\,
      I3 => \initA_reg_n_0_[0]\,
      I4 => \initA_reg_n_0_[4]\,
      I5 => \initA_reg_n_0_[5]\,
      O => \initWord[14]_i_1_n_0\
    );
\initWord[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0010001800090000"
    )
        port map (
      I0 => \initA_reg_n_0_[2]\,
      I1 => \initA_reg_n_0_[3]\,
      I2 => \initA_reg_n_0_[4]\,
      I3 => \initA_reg_n_0_[5]\,
      I4 => \initA_reg_n_0_[0]\,
      I5 => \initA_reg_n_0_[1]\,
      O => \initWord[15]_i_1_n_0\
    );
\initWord[16]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000078A70000A407"
    )
        port map (
      I0 => \initA_reg_n_0_[3]\,
      I1 => \initA_reg_n_0_[1]\,
      I2 => \initA_reg_n_0_[0]\,
      I3 => \initA_reg_n_0_[4]\,
      I4 => \initA_reg_n_0_[5]\,
      I5 => \initA_reg_n_0_[2]\,
      O => \initWord[16]_i_1_n_0\
    );
\initWord[17]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3654371037102344"
    )
        port map (
      I0 => \initA_reg_n_0_[5]\,
      I1 => \initA_reg_n_0_[1]\,
      I2 => \initA_reg_n_0_[4]\,
      I3 => \initA_reg_n_0_[0]\,
      I4 => \initA_reg_n_0_[3]\,
      I5 => \initA_reg_n_0_[2]\,
      O => \initWord[17]_i_1_n_0\
    );
\initWord[18]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DD9CC8C8D8D8C98D"
    )
        port map (
      I0 => \initA_reg_n_0_[5]\,
      I1 => \initA_reg_n_0_[1]\,
      I2 => \initA_reg_n_0_[4]\,
      I3 => \initA_reg_n_0_[0]\,
      I4 => \initA_reg_n_0_[3]\,
      I5 => \initA_reg_n_0_[2]\,
      O => \initWord[18]_i_1_n_0\
    );
\initWord[19]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"98B9B99A98B9BB88"
    )
        port map (
      I0 => \initA_reg_n_0_[1]\,
      I1 => \initA_reg_n_0_[5]\,
      I2 => \initA_reg_n_0_[4]\,
      I3 => \initA_reg_n_0_[2]\,
      I4 => \initA_reg_n_0_[3]\,
      I5 => \initA_reg_n_0_[0]\,
      O => \initWord[19]_i_1_n_0\
    );
\initWord[20]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2221110122210113"
    )
        port map (
      I0 => \initA_reg_n_0_[4]\,
      I1 => \initA_reg_n_0_[5]\,
      I2 => \initA_reg_n_0_[2]\,
      I3 => \initA_reg_n_0_[1]\,
      I4 => \initA_reg_n_0_[3]\,
      I5 => \initA_reg_n_0_[0]\,
      O => \initWord[20]_i_1_n_0\
    );
\initWord[21]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000C00EFEF8"
    )
        port map (
      I0 => \initA_reg_n_0_[0]\,
      I1 => \initA_reg_n_0_[1]\,
      I2 => \initA_reg_n_0_[3]\,
      I3 => \initA_reg_n_0_[2]\,
      I4 => \initA_reg_n_0_[4]\,
      I5 => \initA_reg_n_0_[5]\,
      O => \initWord[21]_i_1_n_0\
    );
\initWord[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0C00020200030000"
    )
        port map (
      I0 => \initA_reg_n_0_[0]\,
      I1 => \initA_reg_n_0_[4]\,
      I2 => \initA_reg_n_0_[5]\,
      I3 => \initA_reg_n_0_[2]\,
      I4 => \initA_reg_n_0_[3]\,
      I5 => \initA_reg_n_0_[1]\,
      O => \initWord[23]_i_1_n_0\
    );
\initWord[30]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0055005500550057"
    )
        port map (
      I0 => \initA_reg_n_0_[5]\,
      I1 => \initA_reg_n_0_[2]\,
      I2 => \initA_reg_n_0_[3]\,
      I3 => \initA_reg_n_0_[6]\,
      I4 => \initA_reg_n_0_[4]\,
      I5 => \initWord[30]_i_3_n_0\,
      O => \initWord[30]_i_1_n_0\
    );
\initWord[30]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \initA_reg_n_0_[1]\,
      I1 => \initA_reg_n_0_[5]\,
      O => \initWord[30]_i_2_n_0\
    );
\initWord[30]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \initA_reg_n_0_[0]\,
      I1 => \initA_reg_n_0_[1]\,
      O => \initWord[30]_i_3_n_0\
    );
\initWord[6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \initA_reg_n_0_[1]\,
      I1 => \initA_reg_n_0_[5]\,
      I2 => \initA_reg_n_0_[0]\,
      O => \initWord[6]_i_1_n_0\
    );
\initWord[8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF74BC00FF3CFF"
    )
        port map (
      I0 => \initA_reg_n_0_[4]\,
      I1 => \initA_reg_n_0_[3]\,
      I2 => \initA_reg_n_0_[2]\,
      I3 => \initA_reg_n_0_[0]\,
      I4 => \initA_reg_n_0_[5]\,
      I5 => \initA_reg_n_0_[1]\,
      O => \initWord[8]_i_1_n_0\
    );
\initWord[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000003D4000030BC"
    )
        port map (
      I0 => \initA_reg_n_0_[0]\,
      I1 => \initA_reg_n_0_[2]\,
      I2 => \initA_reg_n_0_[3]\,
      I3 => \initA_reg_n_0_[4]\,
      I4 => \initA_reg_n_0_[5]\,
      I5 => \initA_reg_n_0_[1]\,
      O => \initWord[9]_i_1_n_0\
    );
\initWord_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => \initWord[30]_i_1_n_0\,
      D => \initWord[0]_i_1_n_0\,
      Q => \initWord_reg_n_0_[0]\,
      R => '0'
    );
\initWord_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => \initWord[30]_i_1_n_0\,
      D => \initWord[10]_i_1_n_0\,
      Q => data2(2),
      R => '0'
    );
\initWord_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => \initWord[30]_i_1_n_0\,
      D => \initWord[11]_i_1_n_0\,
      Q => data2(3),
      R => '0'
    );
\initWord_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => \initWord[30]_i_1_n_0\,
      D => \initWord[12]_i_1_n_0\,
      Q => data2(4),
      R => '0'
    );
\initWord_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => \initWord[30]_i_1_n_0\,
      D => \initWord[13]_i_1_n_0\,
      Q => data2(5),
      R => '0'
    );
\initWord_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => \initWord[30]_i_1_n_0\,
      D => \initWord[14]_i_1_n_0\,
      Q => data2(6),
      R => '0'
    );
\initWord_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => \initWord[30]_i_1_n_0\,
      D => \initWord[15]_i_1_n_0\,
      Q => data2(7),
      R => '0'
    );
\initWord_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => \initWord[30]_i_1_n_0\,
      D => \initWord[16]_i_1_n_0\,
      Q => data1(0),
      R => '0'
    );
\initWord_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => \initWord[30]_i_1_n_0\,
      D => \initWord[17]_i_1_n_0\,
      Q => data1(1),
      R => '0'
    );
\initWord_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => \initWord[30]_i_1_n_0\,
      D => \initWord[18]_i_1_n_0\,
      Q => data1(2),
      R => '0'
    );
\initWord_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => \initWord[30]_i_1_n_0\,
      D => \initWord[19]_i_1_n_0\,
      Q => data1(3),
      R => '0'
    );
\initWord_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => \initWord[30]_i_1_n_0\,
      D => \initWord[20]_i_1_n_0\,
      Q => data1(4),
      R => '0'
    );
\initWord_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => \initWord[30]_i_1_n_0\,
      D => \initWord[21]_i_1_n_0\,
      Q => data1(5),
      R => '0'
    );
\initWord_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => \initWord[30]_i_1_n_0\,
      D => \initWord[23]_i_1_n_0\,
      Q => data1(7),
      R => '0'
    );
\initWord_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => \initWord[30]_i_1_n_0\,
      D => \initWord[30]_i_2_n_0\,
      Q => data0(6),
      R => '0'
    );
\initWord_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => \initWord[30]_i_1_n_0\,
      D => \initWord[6]_i_1_n_0\,
      Q => \initWord_reg_n_0_[6]\,
      R => '0'
    );
\initWord_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => \initWord[30]_i_1_n_0\,
      D => \initWord[8]_i_1_n_0\,
      Q => data2(0),
      R => '0'
    );
\initWord_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => \initWord[30]_i_1_n_0\,
      D => \initWord[9]_i_1_n_0\,
      Q => data2(1),
      R => '0'
    );
msg_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => \state_reg_n_0_[3]\,
      I1 => \state_reg_n_0_[2]\,
      I2 => \state_reg_n_0_[0]\,
      I3 => \state_reg_n_0_[1]\,
      O => msg0
    );
msg_reg: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => s00_axi_aresetn,
      D => msg0,
      Q => msg,
      R => '0'
    );
\state[1]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFEFFFFFFFFFFF"
    )
        port map (
      I0 => \initA_reg_n_0_[4]\,
      I1 => \initA_reg_n_0_[6]\,
      I2 => \state[1]_i_4_n_0\,
      I3 => \initA_reg_n_0_[5]\,
      I4 => \initA_reg_n_0_[0]\,
      I5 => \initA_reg_n_0_[1]\,
      O => \state[1]_i_3_n_0\
    );
\state[1]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \initA_reg_n_0_[3]\,
      I1 => \initA_reg_n_0_[2]\,
      O => \state[1]_i_4_n_0\
    );
\state[2]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0EFF"
    )
        port map (
      I0 => \initWord_reg_n_0_[6]\,
      I1 => \initWord_reg_n_0_[0]\,
      I2 => \state_reg_n_0_[0]\,
      I3 => \state_reg_n_0_[1]\,
      O => \state[2]_i_2_n_0\
    );
\state[3]_i_10\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => delaycnt(19),
      I1 => delaycnt(18),
      I2 => delaycnt(16),
      I3 => delaycnt(17),
      O => \state[3]_i_10_n_0\
    );
\state[3]_i_11\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => delaycnt(27),
      I1 => delaycnt(26),
      I2 => delaycnt(25),
      I3 => delaycnt(24),
      O => \state[3]_i_11_n_0\
    );
\state[3]_i_12\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => delaycnt(3),
      I1 => delaycnt(2),
      I2 => delaycnt(1),
      I3 => delaycnt(0),
      O => \state[3]_i_12_n_0\
    );
\state[3]_i_13\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => delaycnt(9),
      I1 => delaycnt(8),
      I2 => delaycnt(10),
      I3 => delaycnt(11),
      O => \state[3]_i_13_n_0\
    );
\state[3]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0004"
    )
        port map (
      I0 => \state[3]_i_5_n_0\,
      I1 => \state[3]_i_6_n_0\,
      I2 => \state[3]_i_7_n_0\,
      I3 => \state[3]_i_8_n_0\,
      O => \state[3]_i_3_n_0\
    );
\state[3]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => data1(5),
      I1 => data1(4),
      I2 => data1(3),
      I3 => \state[3]_i_9_n_0\,
      O => \state[3]_i_4_n_0\
    );
\state[3]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => delaycnt(20),
      I1 => delaycnt(22),
      I2 => delaycnt(21),
      I3 => delaycnt(23),
      I4 => \state[3]_i_10_n_0\,
      O => \state[3]_i_5_n_0\
    );
\state[3]_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000001"
    )
        port map (
      I0 => delaycnt(29),
      I1 => delaycnt(28),
      I2 => delaycnt(30),
      I3 => delaycnt(31),
      I4 => \state[3]_i_11_n_0\,
      O => \state[3]_i_6_n_0\
    );
\state[3]_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => delaycnt(5),
      I1 => delaycnt(6),
      I2 => delaycnt(4),
      I3 => delaycnt(7),
      I4 => \state[3]_i_12_n_0\,
      O => \state[3]_i_7_n_0\
    );
\state[3]_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => delaycnt(14),
      I1 => delaycnt(15),
      I2 => delaycnt(12),
      I3 => delaycnt(13),
      I4 => \state[3]_i_13_n_0\,
      O => \state[3]_i_8_n_0\
    );
\state[3]_i_9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFD"
    )
        port map (
      I0 => data1(1),
      I1 => data1(0),
      I2 => data1(7),
      I3 => data1(2),
      O => \state[3]_i_9_n_0\
    );
\state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => twi_controller_n_4,
      D => twi_controller_n_3,
      Q => \state_reg_n_0_[0]\,
      R => SR(0)
    );
\state_reg[1]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk_out2,
      CE => twi_controller_n_4,
      D => twi_controller_n_2,
      Q => \state_reg_n_0_[1]\,
      S => SR(0)
    );
\state_reg[2]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk_out2,
      CE => twi_controller_n_4,
      D => twi_controller_n_1,
      Q => \state_reg_n_0_[2]\,
      S => SR(0)
    );
\state_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => twi_controller_n_4,
      D => twi_controller_n_0,
      Q => \state_reg_n_0_[3]\,
      R => SR(0)
    );
stb_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8055"
    )
        port map (
      I0 => \state_reg_n_0_[2]\,
      I1 => \state_reg_n_0_[0]\,
      I2 => \state_reg_n_0_[1]\,
      I3 => \state_reg_n_0_[3]\,
      O => stb_i_1_n_0
    );
stb_reg: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => s00_axi_aresetn,
      D => stb_i_1_n_0,
      Q => stb,
      R => '0'
    );
twi_controller: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_TWICtl
     port map (
      D(3) => twi_controller_n_0,
      D(2) => twi_controller_n_1,
      D(1) => twi_controller_n_2,
      D(0) => twi_controller_n_3,
      E(0) => twi_controller_n_4,
      Q(3) => \state_reg_n_0_[3]\,
      Q(2) => \state_reg_n_0_[2]\,
      Q(1) => \state_reg_n_0_[1]\,
      Q(0) => \state_reg_n_0_[0]\,
      clk_out2 => clk_out2,
      data_i(7 downto 0) => data_i(7 downto 0),
      \delaycnt_reg[20]\ => \state[3]_i_3_n_0\,
      \initA_reg[4]\ => \state[1]_i_3_n_0\,
      \initA_reg[6]\(0) => twi_controller_n_5,
      initEn => initEn,
      initEn_reg => twi_controller_n_6,
      initEn_reg_0 => \initA[6]_i_3_n_0\,
      \initWord_reg[21]\ => \state[3]_i_4_n_0\,
      \initWord_reg[6]\ => \state[2]_i_2_n_0\,
      msg => msg,
      s00_axi_aresetn => s00_axi_aresetn,
      scl => scl,
      sda => sda,
      stb => stb
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_0 is
  port (
    clk_out1 : out STD_LOGIC;
    clk_out2 : out STD_LOGIC;
    clk_out3 : out STD_LOGIC;
    resetn : in STD_LOGIC;
    clk_in1 : in STD_LOGIC;
    lopt : out STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_0 is
begin
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_0_clk_wiz
     port map (
      clk_in1 => clk_in1,
      clk_out1 => clk_out1,
      clk_out2 => clk_out2,
      clk_out3 => clk_out3,
      lopt => lopt,
      resetn => resetn
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_1 is
  port (
    clk_out1 : out STD_LOGIC;
    clk_out2 : out STD_LOGIC;
    resetn : in STD_LOGIC;
    clk_in1 : in STD_LOGIC;
    lopt : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_1;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_1 is
begin
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_1_clk_wiz
     port map (
      clk_in1 => clk_in1,
      clk_out1 => clk_out1,
      clk_out2 => clk_out2,
      lopt => lopt,
      resetn => resetn
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_dvid is
  port (
    red_s : out STD_LOGIC;
    green_s : out STD_LOGIC;
    blue_s : out STD_LOGIC;
    clock_s : out STD_LOGIC;
    \encoded_reg[8]\ : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 0 to 0 );
    \dc_bias_reg[0]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \encoded_reg[8]_0\ : out STD_LOGIC;
    \encoded_reg[8]_1\ : out STD_LOGIC;
    \encoded_reg[8]_2\ : out STD_LOGIC;
    \encoded_reg[8]_3\ : out STD_LOGIC;
    \encoded_reg[8]_4\ : out STD_LOGIC;
    \encoded_reg[8]_5\ : out STD_LOGIC;
    \encoded_reg[8]_6\ : out STD_LOGIC;
    \encoded_reg[8]_7\ : out STD_LOGIC;
    \encoded_reg[9]\ : out STD_LOGIC;
    \dc_bias_reg[0]_0\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \encoded_reg[8]_8\ : out STD_LOGIC;
    \encoded_reg[8]_9\ : out STD_LOGIC;
    \encoded_reg[8]_10\ : out STD_LOGIC;
    \encoded_reg[8]_11\ : out STD_LOGIC;
    \encoded_reg[8]_12\ : out STD_LOGIC;
    \encoded_reg[8]_13\ : out STD_LOGIC;
    \encoded_reg[8]_14\ : out STD_LOGIC;
    \encoded_reg[8]_15\ : out STD_LOGIC;
    \encoded_reg[8]_16\ : out STD_LOGIC;
    \encoded_reg[8]_17\ : out STD_LOGIC;
    clk_out2 : in STD_LOGIC;
    clk_out3 : in STD_LOGIC;
    encoded0_in : in STD_LOGIC_VECTOR ( 0 to 0 );
    \processCol_reg[6]\ : in STD_LOGIC;
    CLK : in STD_LOGIC;
    \dc_bias_reg[3]\ : in STD_LOGIC;
    \dc_bias_reg[3]_0\ : in STD_LOGIC;
    \dc_bias_reg[1]\ : in STD_LOGIC;
    \dc_bias_reg[3]_1\ : in STD_LOGIC;
    \dc_bias_reg[2]\ : in STD_LOGIC;
    \dc_bias_reg[3]_2\ : in STD_LOGIC;
    \dc_bias_reg[3]_3\ : in STD_LOGIC;
    \dc_bias_reg[3]_4\ : in STD_LOGIC;
    \trigger_volt_reg[6]\ : in STD_LOGIC_VECTOR ( 6 downto 0 );
    \processCol_reg[6]_0\ : in STD_LOGIC;
    \processCol_reg[1]\ : in STD_LOGIC;
    \trigger_time_reg[7]\ : in STD_LOGIC;
    \trigger_time_reg[6]\ : in STD_LOGIC;
    \trigger_time_reg[5]\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \trigger_time_reg[3]\ : in STD_LOGIC;
    \trigger_time_reg[2]\ : in STD_LOGIC;
    \trigger_time_reg[4]\ : in STD_LOGIC;
    \processRow_reg[2]\ : in STD_LOGIC;
    \processCol_reg[0]\ : in STD_LOGIC;
    SR : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_dvid;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_dvid is
  signal D0 : STD_LOGIC;
  signal D1 : STD_LOGIC;
  signal TDMS_encoder_blue_n_0 : STD_LOGIC;
  signal TDMS_encoder_blue_n_1 : STD_LOGIC;
  signal TDMS_encoder_blue_n_2 : STD_LOGIC;
  signal TDMS_encoder_blue_n_3 : STD_LOGIC;
  signal TDMS_encoder_green_n_0 : STD_LOGIC;
  signal TDMS_encoder_green_n_1 : STD_LOGIC;
  signal TDMS_encoder_green_n_2 : STD_LOGIC;
  signal TDMS_encoder_green_n_3 : STD_LOGIC;
  signal TDMS_encoder_red_n_0 : STD_LOGIC;
  signal TDMS_encoder_red_n_1 : STD_LOGIC;
  signal TDMS_encoder_red_n_2 : STD_LOGIC;
  signal TDMS_encoder_red_n_3 : STD_LOGIC;
  signal data1 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal latched_blue : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal latched_green : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal latched_red : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal shift_blue : STD_LOGIC_VECTOR ( 6 downto 2 );
  signal \shift_blue[0]_i_1_n_0\ : STD_LOGIC;
  signal \shift_blue[1]_i_1_n_0\ : STD_LOGIC;
  signal \shift_blue[3]_i_1_n_0\ : STD_LOGIC;
  signal \shift_blue[5]_i_1_n_0\ : STD_LOGIC;
  signal \shift_blue[7]_i_1_n_0\ : STD_LOGIC;
  signal \shift_blue[7]_i_2_n_0\ : STD_LOGIC;
  signal \shift_blue_reg_n_0_[0]\ : STD_LOGIC;
  signal \shift_blue_reg_n_0_[1]\ : STD_LOGIC;
  signal \shift_blue_reg_n_0_[2]\ : STD_LOGIC;
  signal \shift_blue_reg_n_0_[3]\ : STD_LOGIC;
  signal \shift_blue_reg_n_0_[4]\ : STD_LOGIC;
  signal \shift_blue_reg_n_0_[5]\ : STD_LOGIC;
  signal \shift_blue_reg_n_0_[6]\ : STD_LOGIC;
  signal \shift_blue_reg_n_0_[7]\ : STD_LOGIC;
  signal \shift_blue_reg_n_0_[8]\ : STD_LOGIC;
  signal \shift_blue_reg_n_0_[9]\ : STD_LOGIC;
  signal shift_clock : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \shift_clock__0\ : STD_LOGIC_VECTOR ( 9 downto 2 );
  signal shift_green : STD_LOGIC_VECTOR ( 6 downto 2 );
  signal \shift_green[0]_i_1_n_0\ : STD_LOGIC;
  signal \shift_green[1]_i_1_n_0\ : STD_LOGIC;
  signal \shift_green[3]_i_1_n_0\ : STD_LOGIC;
  signal \shift_green[5]_i_1_n_0\ : STD_LOGIC;
  signal \shift_green[7]_i_1_n_0\ : STD_LOGIC;
  signal \shift_green[7]_i_2_n_0\ : STD_LOGIC;
  signal \shift_green_reg_n_0_[0]\ : STD_LOGIC;
  signal \shift_green_reg_n_0_[1]\ : STD_LOGIC;
  signal \shift_green_reg_n_0_[2]\ : STD_LOGIC;
  signal \shift_green_reg_n_0_[3]\ : STD_LOGIC;
  signal \shift_green_reg_n_0_[4]\ : STD_LOGIC;
  signal \shift_green_reg_n_0_[5]\ : STD_LOGIC;
  signal \shift_green_reg_n_0_[6]\ : STD_LOGIC;
  signal \shift_green_reg_n_0_[7]\ : STD_LOGIC;
  signal \shift_green_reg_n_0_[8]\ : STD_LOGIC;
  signal \shift_green_reg_n_0_[9]\ : STD_LOGIC;
  signal shift_red : STD_LOGIC_VECTOR ( 6 downto 2 );
  signal \shift_red[0]_i_1_n_0\ : STD_LOGIC;
  signal \shift_red[1]_i_1_n_0\ : STD_LOGIC;
  signal \shift_red[3]_i_1_n_0\ : STD_LOGIC;
  signal \shift_red[5]_i_1_n_0\ : STD_LOGIC;
  signal \shift_red[7]_i_1_n_0\ : STD_LOGIC;
  signal \shift_red[7]_i_2_n_0\ : STD_LOGIC;
  signal \shift_red[9]_i_1_n_0\ : STD_LOGIC;
  signal \shift_red[9]_i_2_n_0\ : STD_LOGIC;
  signal NLW_ODDR2_blue_R_UNCONNECTED : STD_LOGIC;
  signal NLW_ODDR2_blue_S_UNCONNECTED : STD_LOGIC;
  signal NLW_ODDR2_clock_R_UNCONNECTED : STD_LOGIC;
  signal NLW_ODDR2_clock_S_UNCONNECTED : STD_LOGIC;
  signal NLW_ODDR2_green_R_UNCONNECTED : STD_LOGIC;
  signal NLW_ODDR2_green_S_UNCONNECTED : STD_LOGIC;
  signal NLW_ODDR2_red_R_UNCONNECTED : STD_LOGIC;
  signal NLW_ODDR2_red_S_UNCONNECTED : STD_LOGIC;
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of ODDR2_blue : label is "ODDR2";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of ODDR2_blue : label is "D0:D1 D1:D2 C0:C";
  attribute \__SRVAL\ : string;
  attribute \__SRVAL\ of ODDR2_blue : label is "TRUE";
  attribute box_type : string;
  attribute box_type of ODDR2_blue : label is "PRIMITIVE";
  attribute XILINX_LEGACY_PRIM of ODDR2_clock : label is "ODDR2";
  attribute XILINX_TRANSFORM_PINMAP of ODDR2_clock : label is "D0:D1 D1:D2 C0:C";
  attribute \__SRVAL\ of ODDR2_clock : label is "TRUE";
  attribute box_type of ODDR2_clock : label is "PRIMITIVE";
  attribute XILINX_LEGACY_PRIM of ODDR2_green : label is "ODDR2";
  attribute XILINX_TRANSFORM_PINMAP of ODDR2_green : label is "D0:D1 D1:D2 C0:C";
  attribute \__SRVAL\ of ODDR2_green : label is "TRUE";
  attribute box_type of ODDR2_green : label is "PRIMITIVE";
  attribute XILINX_LEGACY_PRIM of ODDR2_red : label is "ODDR2";
  attribute XILINX_TRANSFORM_PINMAP of ODDR2_red : label is "D0:D1 D1:D2 C0:C";
  attribute \__SRVAL\ of ODDR2_red : label is "TRUE";
  attribute box_type of ODDR2_red : label is "PRIMITIVE";
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \shift_blue[0]_i_1\ : label is "soft_lutpair43";
  attribute SOFT_HLUTNM of \shift_blue[1]_i_1\ : label is "soft_lutpair45";
  attribute SOFT_HLUTNM of \shift_blue[2]_i_1\ : label is "soft_lutpair41";
  attribute SOFT_HLUTNM of \shift_blue[3]_i_1\ : label is "soft_lutpair43";
  attribute SOFT_HLUTNM of \shift_blue[4]_i_1\ : label is "soft_lutpair41";
  attribute SOFT_HLUTNM of \shift_blue[5]_i_1\ : label is "soft_lutpair45";
  attribute SOFT_HLUTNM of \shift_blue[6]_i_1\ : label is "soft_lutpair42";
  attribute SOFT_HLUTNM of \shift_green[0]_i_1\ : label is "soft_lutpair44";
  attribute SOFT_HLUTNM of \shift_green[1]_i_1\ : label is "soft_lutpair44";
  attribute SOFT_HLUTNM of \shift_green[2]_i_1\ : label is "soft_lutpair39";
  attribute SOFT_HLUTNM of \shift_green[3]_i_1\ : label is "soft_lutpair46";
  attribute SOFT_HLUTNM of \shift_green[4]_i_1\ : label is "soft_lutpair39";
  attribute SOFT_HLUTNM of \shift_green[5]_i_1\ : label is "soft_lutpair46";
  attribute SOFT_HLUTNM of \shift_green[6]_i_1\ : label is "soft_lutpair42";
  attribute SOFT_HLUTNM of \shift_red[0]_i_1\ : label is "soft_lutpair47";
  attribute SOFT_HLUTNM of \shift_red[1]_i_1\ : label is "soft_lutpair47";
  attribute SOFT_HLUTNM of \shift_red[2]_i_1\ : label is "soft_lutpair40";
  attribute SOFT_HLUTNM of \shift_red[3]_i_1\ : label is "soft_lutpair48";
  attribute SOFT_HLUTNM of \shift_red[5]_i_1\ : label is "soft_lutpair48";
  attribute SOFT_HLUTNM of \shift_red[6]_i_1\ : label is "soft_lutpair40";
begin
ODDR2_blue: unisim.vcomponents.ODDR
    generic map(
      DDR_CLK_EDGE => "SAME_EDGE",
      INIT => '0',
      SRTYPE => "ASYNC"
    )
        port map (
      C => clk_out2,
      CE => '1',
      D1 => \shift_blue_reg_n_0_[0]\,
      D2 => \shift_blue_reg_n_0_[1]\,
      Q => blue_s,
      R => NLW_ODDR2_blue_R_UNCONNECTED,
      S => NLW_ODDR2_blue_S_UNCONNECTED
    );
ODDR2_clock: unisim.vcomponents.ODDR
    generic map(
      DDR_CLK_EDGE => "SAME_EDGE",
      INIT => '0',
      SRTYPE => "ASYNC"
    )
        port map (
      C => clk_out2,
      CE => '1',
      D1 => shift_clock(0),
      D2 => shift_clock(1),
      Q => clock_s,
      R => NLW_ODDR2_clock_R_UNCONNECTED,
      S => NLW_ODDR2_clock_S_UNCONNECTED
    );
ODDR2_green: unisim.vcomponents.ODDR
    generic map(
      DDR_CLK_EDGE => "SAME_EDGE",
      INIT => '0',
      SRTYPE => "ASYNC"
    )
        port map (
      C => clk_out2,
      CE => '1',
      D1 => \shift_green_reg_n_0_[0]\,
      D2 => \shift_green_reg_n_0_[1]\,
      Q => green_s,
      R => NLW_ODDR2_green_R_UNCONNECTED,
      S => NLW_ODDR2_green_S_UNCONNECTED
    );
ODDR2_red: unisim.vcomponents.ODDR
    generic map(
      DDR_CLK_EDGE => "SAME_EDGE",
      INIT => '0',
      SRTYPE => "ASYNC"
    )
        port map (
      C => clk_out2,
      CE => '1',
      D1 => D0,
      D2 => D1,
      Q => red_s,
      R => NLW_ODDR2_red_R_UNCONNECTED,
      S => NLW_ODDR2_red_S_UNCONNECTED
    );
TDMS_encoder_blue: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_TDMS_encoder
     port map (
      CLK => CLK,
      D(3) => TDMS_encoder_blue_n_0,
      D(2) => TDMS_encoder_blue_n_1,
      D(1) => TDMS_encoder_blue_n_2,
      D(0) => TDMS_encoder_blue_n_3,
      Q(1 downto 0) => \dc_bias_reg[0]_0\(1 downto 0),
      SR(0) => SR(0),
      \dc_bias_reg[2]_0\ => \dc_bias_reg[2]\,
      \dc_bias_reg[3]_0\ => \dc_bias_reg[3]_2\,
      \dc_bias_reg[3]_1\ => \dc_bias_reg[3]_3\,
      \dc_bias_reg[3]_2\ => \dc_bias_reg[3]_4\,
      \encoded_reg[8]_0\ => \encoded_reg[8]_17\,
      \encoded_reg[9]_0\ => \encoded_reg[9]\,
      \processCol_reg[6]\ => \processCol_reg[6]_0\,
      \processRow_reg[2]\ => \processRow_reg[2]\
    );
TDMS_encoder_green: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_TDMS_encoder_0
     port map (
      CLK => CLK,
      D(3) => TDMS_encoder_green_n_0,
      D(2) => TDMS_encoder_green_n_1,
      D(1) => TDMS_encoder_green_n_2,
      D(0) => TDMS_encoder_green_n_3,
      Q(0) => Q(0),
      SR(0) => SR(0),
      \dc_bias_reg[1]_0\ => \dc_bias_reg[1]\,
      \dc_bias_reg[3]_0\ => \dc_bias_reg[3]_1\,
      \encoded_reg[8]_0\ => \encoded_reg[8]_9\,
      \processCol_reg[1]\ => \processCol_reg[1]\,
      \processCol_reg[6]\ => \processCol_reg[6]_0\
    );
TDMS_encoder_red: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_TDMS_encoder_1
     port map (
      CLK => CLK,
      D(3) => TDMS_encoder_red_n_0,
      D(2) => TDMS_encoder_red_n_1,
      D(1) => TDMS_encoder_red_n_2,
      D(0) => TDMS_encoder_red_n_3,
      Q(0) => \dc_bias_reg[0]\(0),
      SR(0) => SR(0),
      \dc_bias_reg[3]_0\ => \dc_bias_reg[3]\,
      \dc_bias_reg[3]_1\ => \dc_bias_reg[3]_0\,
      encoded0_in(0) => encoded0_in(0),
      \encoded_reg[8]_0\ => \encoded_reg[8]\,
      \encoded_reg[8]_1\ => \encoded_reg[8]_0\,
      \encoded_reg[8]_10\ => \encoded_reg[8]_10\,
      \encoded_reg[8]_11\ => \encoded_reg[8]_11\,
      \encoded_reg[8]_12\ => \encoded_reg[8]_12\,
      \encoded_reg[8]_13\ => \encoded_reg[8]_13\,
      \encoded_reg[8]_14\ => \encoded_reg[8]_14\,
      \encoded_reg[8]_15\ => \encoded_reg[8]_15\,
      \encoded_reg[8]_16\ => \encoded_reg[8]_16\,
      \encoded_reg[8]_2\ => \encoded_reg[8]_1\,
      \encoded_reg[8]_3\ => \encoded_reg[8]_2\,
      \encoded_reg[8]_4\ => \encoded_reg[8]_3\,
      \encoded_reg[8]_5\ => \encoded_reg[8]_4\,
      \encoded_reg[8]_6\ => \encoded_reg[8]_5\,
      \encoded_reg[8]_7\ => \encoded_reg[8]_6\,
      \encoded_reg[8]_8\ => \encoded_reg[8]_7\,
      \encoded_reg[8]_9\ => \encoded_reg[8]_8\,
      \processCol_reg[0]\ => \processCol_reg[0]\,
      \processCol_reg[6]\ => \processCol_reg[6]\,
      \processCol_reg[6]_0\ => \processCol_reg[6]_0\,
      \processRow_reg[2]\ => \processRow_reg[2]\,
      \trigger_time_reg[2]\ => \trigger_time_reg[2]\,
      \trigger_time_reg[3]\ => \trigger_time_reg[3]\,
      \trigger_time_reg[4]\ => \trigger_time_reg[4]\,
      \trigger_time_reg[5]\(2 downto 0) => \trigger_time_reg[5]\(2 downto 0),
      \trigger_time_reg[6]\ => \trigger_time_reg[6]\,
      \trigger_time_reg[7]\ => \trigger_time_reg[7]\,
      \trigger_volt_reg[6]\(6 downto 0) => \trigger_volt_reg[6]\(6 downto 0)
    );
\latched_blue_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => TDMS_encoder_blue_n_3,
      Q => latched_blue(0),
      R => '0'
    );
\latched_blue_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => TDMS_encoder_blue_n_2,
      Q => latched_blue(2),
      R => '0'
    );
\latched_blue_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => TDMS_encoder_blue_n_1,
      Q => latched_blue(8),
      R => '0'
    );
\latched_blue_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => TDMS_encoder_blue_n_0,
      Q => latched_blue(9),
      R => '0'
    );
\latched_green_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => TDMS_encoder_green_n_3,
      Q => latched_green(0),
      R => '0'
    );
\latched_green_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => TDMS_encoder_green_n_2,
      Q => latched_green(2),
      R => '0'
    );
\latched_green_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => TDMS_encoder_green_n_1,
      Q => latched_green(8),
      R => '0'
    );
\latched_green_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => TDMS_encoder_green_n_0,
      Q => latched_green(9),
      R => '0'
    );
\latched_red_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => TDMS_encoder_red_n_3,
      Q => latched_red(0),
      R => '0'
    );
\latched_red_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => TDMS_encoder_red_n_2,
      Q => latched_red(2),
      R => '0'
    );
\latched_red_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => TDMS_encoder_red_n_1,
      Q => latched_red(8),
      R => '0'
    );
\latched_red_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => TDMS_encoder_red_n_0,
      Q => latched_red(9),
      R => '0'
    );
\shift_blue[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \shift_red[9]_i_1_n_0\,
      I1 => \shift_blue_reg_n_0_[2]\,
      O => \shift_blue[0]_i_1_n_0\
    );
\shift_blue[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \shift_red[9]_i_1_n_0\,
      I1 => \shift_blue_reg_n_0_[3]\,
      O => \shift_blue[1]_i_1_n_0\
    );
\shift_blue[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \shift_blue_reg_n_0_[4]\,
      I1 => \shift_red[9]_i_1_n_0\,
      I2 => latched_blue(2),
      O => shift_blue(2)
    );
\shift_blue[3]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \shift_red[9]_i_1_n_0\,
      I1 => \shift_blue_reg_n_0_[5]\,
      O => \shift_blue[3]_i_1_n_0\
    );
\shift_blue[4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \shift_blue_reg_n_0_[6]\,
      I1 => \shift_red[9]_i_1_n_0\,
      I2 => latched_blue(2),
      O => shift_blue(4)
    );
\shift_blue[5]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \shift_red[9]_i_1_n_0\,
      I1 => \shift_blue_reg_n_0_[7]\,
      O => \shift_blue[5]_i_1_n_0\
    );
\shift_blue[6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \shift_blue_reg_n_0_[8]\,
      I1 => \shift_red[9]_i_1_n_0\,
      I2 => latched_blue(2),
      O => shift_blue(6)
    );
\shift_blue[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => latched_blue(0),
      I1 => \shift_red[9]_i_1_n_0\,
      O => \shift_blue[7]_i_1_n_0\
    );
\shift_blue[7]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \shift_red[9]_i_1_n_0\,
      I1 => \shift_blue_reg_n_0_[9]\,
      O => \shift_blue[7]_i_2_n_0\
    );
\shift_blue_reg[0]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => \shift_blue[0]_i_1_n_0\,
      Q => \shift_blue_reg_n_0_[0]\,
      S => \shift_blue[7]_i_1_n_0\
    );
\shift_blue_reg[1]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => \shift_blue[1]_i_1_n_0\,
      Q => \shift_blue_reg_n_0_[1]\,
      S => \shift_blue[7]_i_1_n_0\
    );
\shift_blue_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => shift_blue(2),
      Q => \shift_blue_reg_n_0_[2]\,
      R => '0'
    );
\shift_blue_reg[3]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => \shift_blue[3]_i_1_n_0\,
      Q => \shift_blue_reg_n_0_[3]\,
      S => \shift_blue[7]_i_1_n_0\
    );
\shift_blue_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => shift_blue(4),
      Q => \shift_blue_reg_n_0_[4]\,
      R => '0'
    );
\shift_blue_reg[5]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => \shift_blue[5]_i_1_n_0\,
      Q => \shift_blue_reg_n_0_[5]\,
      S => \shift_blue[7]_i_1_n_0\
    );
\shift_blue_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => shift_blue(6),
      Q => \shift_blue_reg_n_0_[6]\,
      R => '0'
    );
\shift_blue_reg[7]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => \shift_blue[7]_i_2_n_0\,
      Q => \shift_blue_reg_n_0_[7]\,
      S => \shift_blue[7]_i_1_n_0\
    );
\shift_blue_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => latched_blue(8),
      Q => \shift_blue_reg_n_0_[8]\,
      R => \shift_red[9]_i_1_n_0\
    );
\shift_blue_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => latched_blue(9),
      Q => \shift_blue_reg_n_0_[9]\,
      R => \shift_red[9]_i_1_n_0\
    );
\shift_clock_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => \shift_clock__0\(2),
      Q => shift_clock(0),
      R => '0'
    );
\shift_clock_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => \shift_clock__0\(3),
      Q => shift_clock(1),
      R => '0'
    );
\shift_clock_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => \shift_clock__0\(4),
      Q => \shift_clock__0\(2),
      R => '0'
    );
\shift_clock_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => \shift_clock__0\(5),
      Q => \shift_clock__0\(3),
      R => '0'
    );
\shift_clock_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => \shift_clock__0\(6),
      Q => \shift_clock__0\(4),
      R => '0'
    );
\shift_clock_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => \shift_clock__0\(7),
      Q => \shift_clock__0\(5),
      R => '0'
    );
\shift_clock_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => \shift_clock__0\(8),
      Q => \shift_clock__0\(6),
      R => '0'
    );
\shift_clock_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => \shift_clock__0\(9),
      Q => \shift_clock__0\(7),
      R => '0'
    );
\shift_clock_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => shift_clock(0),
      Q => \shift_clock__0\(8),
      R => '0'
    );
\shift_clock_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => shift_clock(1),
      Q => \shift_clock__0\(9),
      R => '0'
    );
\shift_green[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \shift_red[9]_i_1_n_0\,
      I1 => \shift_green_reg_n_0_[2]\,
      O => \shift_green[0]_i_1_n_0\
    );
\shift_green[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \shift_red[9]_i_1_n_0\,
      I1 => \shift_green_reg_n_0_[3]\,
      O => \shift_green[1]_i_1_n_0\
    );
\shift_green[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \shift_green_reg_n_0_[4]\,
      I1 => \shift_red[9]_i_1_n_0\,
      I2 => latched_green(2),
      O => shift_green(2)
    );
\shift_green[3]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \shift_red[9]_i_1_n_0\,
      I1 => \shift_green_reg_n_0_[5]\,
      O => \shift_green[3]_i_1_n_0\
    );
\shift_green[4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \shift_green_reg_n_0_[6]\,
      I1 => \shift_red[9]_i_1_n_0\,
      I2 => latched_green(2),
      O => shift_green(4)
    );
\shift_green[5]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \shift_red[9]_i_1_n_0\,
      I1 => \shift_green_reg_n_0_[7]\,
      O => \shift_green[5]_i_1_n_0\
    );
\shift_green[6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \shift_green_reg_n_0_[8]\,
      I1 => \shift_red[9]_i_1_n_0\,
      I2 => latched_green(2),
      O => shift_green(6)
    );
\shift_green[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => latched_green(0),
      I1 => \shift_red[9]_i_1_n_0\,
      O => \shift_green[7]_i_1_n_0\
    );
\shift_green[7]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \shift_red[9]_i_1_n_0\,
      I1 => \shift_green_reg_n_0_[9]\,
      O => \shift_green[7]_i_2_n_0\
    );
\shift_green_reg[0]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => \shift_green[0]_i_1_n_0\,
      Q => \shift_green_reg_n_0_[0]\,
      S => \shift_green[7]_i_1_n_0\
    );
\shift_green_reg[1]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => \shift_green[1]_i_1_n_0\,
      Q => \shift_green_reg_n_0_[1]\,
      S => \shift_green[7]_i_1_n_0\
    );
\shift_green_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => shift_green(2),
      Q => \shift_green_reg_n_0_[2]\,
      R => '0'
    );
\shift_green_reg[3]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => \shift_green[3]_i_1_n_0\,
      Q => \shift_green_reg_n_0_[3]\,
      S => \shift_green[7]_i_1_n_0\
    );
\shift_green_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => shift_green(4),
      Q => \shift_green_reg_n_0_[4]\,
      R => '0'
    );
\shift_green_reg[5]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => \shift_green[5]_i_1_n_0\,
      Q => \shift_green_reg_n_0_[5]\,
      S => \shift_green[7]_i_1_n_0\
    );
\shift_green_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => shift_green(6),
      Q => \shift_green_reg_n_0_[6]\,
      R => '0'
    );
\shift_green_reg[7]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => \shift_green[7]_i_2_n_0\,
      Q => \shift_green_reg_n_0_[7]\,
      S => \shift_green[7]_i_1_n_0\
    );
\shift_green_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => latched_green(8),
      Q => \shift_green_reg_n_0_[8]\,
      R => \shift_red[9]_i_1_n_0\
    );
\shift_green_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => latched_green(9),
      Q => \shift_green_reg_n_0_[9]\,
      R => \shift_red[9]_i_1_n_0\
    );
\shift_red[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \shift_red[9]_i_1_n_0\,
      I1 => data1(0),
      O => \shift_red[0]_i_1_n_0\
    );
\shift_red[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \shift_red[9]_i_1_n_0\,
      I1 => data1(1),
      O => \shift_red[1]_i_1_n_0\
    );
\shift_red[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => data1(2),
      I1 => \shift_red[9]_i_1_n_0\,
      I2 => latched_red(2),
      O => shift_red(2)
    );
\shift_red[3]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \shift_red[9]_i_1_n_0\,
      I1 => data1(3),
      O => \shift_red[3]_i_1_n_0\
    );
\shift_red[4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => data1(4),
      I1 => \shift_red[9]_i_1_n_0\,
      I2 => latched_red(2),
      O => shift_red(4)
    );
\shift_red[5]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \shift_red[9]_i_1_n_0\,
      I1 => data1(5),
      O => \shift_red[5]_i_1_n_0\
    );
\shift_red[6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => data1(6),
      I1 => \shift_red[9]_i_1_n_0\,
      I2 => latched_red(2),
      O => shift_red(6)
    );
\shift_red[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => latched_red(0),
      I1 => \shift_red[9]_i_1_n_0\,
      O => \shift_red[7]_i_1_n_0\
    );
\shift_red[7]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \shift_red[9]_i_1_n_0\,
      I1 => data1(7),
      O => \shift_red[7]_i_2_n_0\
    );
\shift_red[9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EFFFFFFF"
    )
        port map (
      I0 => \shift_red[9]_i_2_n_0\,
      I1 => \shift_clock__0\(5),
      I2 => \shift_clock__0\(4),
      I3 => \shift_clock__0\(2),
      I4 => \shift_clock__0\(3),
      O => \shift_red[9]_i_1_n_0\
    );
\shift_red[9]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFFFFFFFFFFFF"
    )
        port map (
      I0 => \shift_clock__0\(8),
      I1 => \shift_clock__0\(9),
      I2 => \shift_clock__0\(6),
      I3 => \shift_clock__0\(7),
      I4 => shift_clock(1),
      I5 => shift_clock(0),
      O => \shift_red[9]_i_2_n_0\
    );
\shift_red_reg[0]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => \shift_red[0]_i_1_n_0\,
      Q => D0,
      S => \shift_red[7]_i_1_n_0\
    );
\shift_red_reg[1]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => \shift_red[1]_i_1_n_0\,
      Q => D1,
      S => \shift_red[7]_i_1_n_0\
    );
\shift_red_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => shift_red(2),
      Q => data1(0),
      R => '0'
    );
\shift_red_reg[3]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => \shift_red[3]_i_1_n_0\,
      Q => data1(1),
      S => \shift_red[7]_i_1_n_0\
    );
\shift_red_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => shift_red(4),
      Q => data1(2),
      R => '0'
    );
\shift_red_reg[5]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => \shift_red[5]_i_1_n_0\,
      Q => data1(3),
      S => \shift_red[7]_i_1_n_0\
    );
\shift_red_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => shift_red(6),
      Q => data1(4),
      R => '0'
    );
\shift_red_reg[7]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => \shift_red[7]_i_2_n_0\,
      Q => data1(5),
      S => \shift_red[7]_i_1_n_0\
    );
\shift_red_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => latched_red(8),
      Q => data1(6),
      R => \shift_red[9]_i_1_n_0\
    );
\shift_red_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => latched_red(9),
      Q => data1(7),
      R => \shift_red[9]_i_1_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_vga is
  port (
    Q : out STD_LOGIC_VECTOR ( 5 downto 0 );
    \sdp_bl.ramb18_dp_bl.ram18_bl\ : out STD_LOGIC_VECTOR ( 9 downto 0 );
    \encoded_reg[8]\ : out STD_LOGIC;
    \encoded_reg[9]\ : out STD_LOGIC;
    \encoded_reg[8]_0\ : out STD_LOGIC;
    \encoded_reg[8]_1\ : out STD_LOGIC;
    \encoded_reg[8]_2\ : out STD_LOGIC;
    \encoded_reg[0]\ : out STD_LOGIC;
    \encoded_reg[8]_3\ : out STD_LOGIC;
    \encoded_reg[9]_0\ : out STD_LOGIC;
    encoded0_in : out STD_LOGIC_VECTOR ( 0 to 0 );
    \encoded_reg[2]\ : out STD_LOGIC;
    \encoded_reg[0]_0\ : out STD_LOGIC;
    SR : out STD_LOGIC_VECTOR ( 0 to 0 );
    \encoded_reg[9]_1\ : out STD_LOGIC;
    \encoded_reg[9]_2\ : out STD_LOGIC;
    \encoded_reg[9]_3\ : out STD_LOGIC;
    \dc_bias_reg[3]\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \dc_bias_reg[1]\ : in STD_LOGIC;
    \dc_bias_reg[1]_0\ : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC;
    \dc_bias_reg[3]_0\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \trigger_time_reg[4]\ : in STD_LOGIC;
    \trigger_time_reg[8]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \trigger_time_reg[3]\ : in STD_LOGIC;
    \trigger_volt_reg[9]\ : in STD_LOGIC_VECTOR ( 9 downto 0 );
    \trigger_time_reg[9]\ : in STD_LOGIC;
    \trigger_volt_reg[2]\ : in STD_LOGIC;
    DOADO : in STD_LOGIC_VECTOR ( 4 downto 0 );
    \sdp_bl.ramb18_dp_bl.ram18_bl_0\ : in STD_LOGIC;
    \sdp_bl.ramb18_dp_bl.ram18_bl_1\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    switch : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \dc_bias_reg[1]_1\ : in STD_LOGIC;
    \trigger_time_reg[7]\ : in STD_LOGIC;
    \trigger_time_reg[5]\ : in STD_LOGIC;
    \trigger_time_reg[6]\ : in STD_LOGIC;
    \trigger_time_reg[4]_0\ : in STD_LOGIC;
    \trigger_time_reg[6]_0\ : in STD_LOGIC;
    \trigger_time_reg[7]_0\ : in STD_LOGIC;
    \trigger_time_reg[5]_0\ : in STD_LOGIC;
    \trigger_time_reg[1]\ : in STD_LOGIC;
    \trigger_time_reg[1]_0\ : in STD_LOGIC;
    \trigger_time_reg[2]\ : in STD_LOGIC;
    \trigger_volt_reg[4]\ : in STD_LOGIC;
    \trigger_volt_reg[5]\ : in STD_LOGIC;
    \trigger_volt_reg[4]_0\ : in STD_LOGIC;
    \trigger_volt_reg[5]_0\ : in STD_LOGIC;
    \trigger_volt_reg[1]\ : in STD_LOGIC;
    \trigger_volt_reg[1]_0\ : in STD_LOGIC;
    S : in STD_LOGIC_VECTOR ( 0 to 0 );
    \sdp_bl.ramb18_dp_bl.ram18_bl_2\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \trigger_time_reg[6]_1\ : in STD_LOGIC;
    \trigger_time_reg[5]_1\ : in STD_LOGIC;
    \dc_bias_reg[3]_1\ : in STD_LOGIC;
    \dc_bias_reg[3]_2\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    s00_axi_aresetn_0 : in STD_LOGIC;
    CLK : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_vga;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_vga is
  signal \^q\ : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal R10 : STD_LOGIC;
  signal R10_carry_i_1_n_0 : STD_LOGIC;
  signal R10_carry_i_2_n_0 : STD_LOGIC;
  signal R10_carry_i_3_n_0 : STD_LOGIC;
  signal R10_carry_i_4_n_0 : STD_LOGIC;
  signal R13 : STD_LOGIC;
  signal R13_carry_i_1_n_0 : STD_LOGIC;
  signal R13_carry_i_2_n_0 : STD_LOGIC;
  signal R13_carry_i_3_n_0 : STD_LOGIC;
  signal R13_carry_i_4_n_0 : STD_LOGIC;
  signal R14 : STD_LOGIC;
  signal R14_carry_i_1_n_0 : STD_LOGIC;
  signal R14_carry_i_2_n_0 : STD_LOGIC;
  signal R14_carry_i_3_n_0 : STD_LOGIC;
  signal R14_carry_i_4_n_0 : STD_LOGIC;
  signal R14_carry_i_6_n_0 : STD_LOGIC;
  signal R15 : STD_LOGIC;
  signal R15_carry_i_1_n_0 : STD_LOGIC;
  signal R15_carry_i_2_n_0 : STD_LOGIC;
  signal R15_carry_i_3_n_0 : STD_LOGIC;
  signal R15_carry_i_4_n_0 : STD_LOGIC;
  signal R15_carry_i_6_n_0 : STD_LOGIC;
  signal R15_carry_i_7_n_0 : STD_LOGIC;
  signal R15_carry_i_8_n_0 : STD_LOGIC;
  signal R15_carry_i_9_n_0 : STD_LOGIC;
  signal R16 : STD_LOGIC;
  signal R16_carry_i_10_n_0 : STD_LOGIC;
  signal R16_carry_i_11_n_0 : STD_LOGIC;
  signal R16_carry_i_1_n_0 : STD_LOGIC;
  signal R16_carry_i_2_n_0 : STD_LOGIC;
  signal R16_carry_i_3_n_0 : STD_LOGIC;
  signal R16_carry_i_4_n_0 : STD_LOGIC;
  signal R16_carry_i_7_n_0 : STD_LOGIC;
  signal R16_carry_i_8_n_0 : STD_LOGIC;
  signal R17 : STD_LOGIC;
  signal R17_carry_i_1_n_0 : STD_LOGIC;
  signal R17_carry_i_2_n_0 : STD_LOGIC;
  signal R17_carry_i_3_n_0 : STD_LOGIC;
  signal R17_carry_i_4_n_0 : STD_LOGIC;
  signal \R22__29_carry_i_4_n_0\ : STD_LOGIC;
  signal R6 : STD_LOGIC;
  signal R6_carry_i_1_n_0 : STD_LOGIC;
  signal R6_carry_i_2_n_0 : STD_LOGIC;
  signal R6_carry_i_3_n_0 : STD_LOGIC;
  signal R6_carry_i_4_n_0 : STD_LOGIC;
  signal R7 : STD_LOGIC;
  signal R7_carry_i_1_n_0 : STD_LOGIC;
  signal R7_carry_i_2_n_0 : STD_LOGIC;
  signal R7_carry_i_3_n_0 : STD_LOGIC;
  signal R7_carry_i_4_n_0 : STD_LOGIC;
  signal R7_carry_i_5_n_0 : STD_LOGIC;
  signal R7_carry_i_7_n_0 : STD_LOGIC;
  signal R8 : STD_LOGIC;
  signal R8_carry_i_1_n_0 : STD_LOGIC;
  signal R8_carry_i_2_n_0 : STD_LOGIC;
  signal R8_carry_i_3_n_0 : STD_LOGIC;
  signal R8_carry_i_4_n_0 : STD_LOGIC;
  signal R8_carry_i_6_n_0 : STD_LOGIC;
  signal R8_carry_i_7_n_0 : STD_LOGIC;
  signal R8_carry_i_8_n_0 : STD_LOGIC;
  signal R9 : STD_LOGIC;
  signal R9_carry_i_1_n_0 : STD_LOGIC;
  signal R9_carry_i_2_n_0 : STD_LOGIC;
  signal R9_carry_i_3_n_0 : STD_LOGIC;
  signal R9_carry_i_4_n_0 : STD_LOGIC;
  signal R9_carry_i_6_n_0 : STD_LOGIC;
  signal R9_carry_i_7_n_0 : STD_LOGIC;
  signal R9_carry_i_8_n_0 : STD_LOGIC;
  signal R9_carry_i_9_n_0 : STD_LOGIC;
  signal ch13 : STD_LOGIC;
  signal ch23 : STD_LOGIC;
  signal \dc_bias[3]_i_10_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_13_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_14_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_15_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_16_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_17_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_18_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_19_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_20_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_21_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_22_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_25_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_26_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_27_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_28_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_29_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_30_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_31_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_32_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_33_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_34_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_35_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_36_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_37_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_38_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_39_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_3__0_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_40_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_41_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_42_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_43_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_44_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_45_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_46_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_47_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_48_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_49_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_50_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_6_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_7_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_8_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_9_n_0\ : STD_LOGIC;
  signal \encoded[8]_i_10_n_0\ : STD_LOGIC;
  signal \encoded[8]_i_12_n_0\ : STD_LOGIC;
  signal \encoded[8]_i_13_n_0\ : STD_LOGIC;
  signal \encoded[8]_i_14_n_0\ : STD_LOGIC;
  signal \encoded[8]_i_18_n_0\ : STD_LOGIC;
  signal \encoded[8]_i_19_n_0\ : STD_LOGIC;
  signal \encoded[8]_i_20_n_0\ : STD_LOGIC;
  signal \encoded[8]_i_21_n_0\ : STD_LOGIC;
  signal \encoded[8]_i_22_n_0\ : STD_LOGIC;
  signal \encoded[8]_i_23_n_0\ : STD_LOGIC;
  signal \encoded[8]_i_3__0_n_0\ : STD_LOGIC;
  signal \encoded[8]_i_3_n_0\ : STD_LOGIC;
  signal \encoded[8]_i_5__0_n_0\ : STD_LOGIC;
  signal \encoded[8]_i_5_n_0\ : STD_LOGIC;
  signal \encoded[8]_i_6_n_0\ : STD_LOGIC;
  signal \encoded[8]_i_7__0_n_0\ : STD_LOGIC;
  signal \encoded[8]_i_8__0_n_0\ : STD_LOGIC;
  signal \encoded[8]_i_9_n_0\ : STD_LOGIC;
  signal \encoded[9]_i_2_n_0\ : STD_LOGIC;
  signal \encoded[9]_i_4_n_0\ : STD_LOGIC;
  signal \^encoded_reg[8]_2\ : STD_LOGIC;
  signal \encoded_reg[8]_i_3_n_1\ : STD_LOGIC;
  signal \encoded_reg[8]_i_3_n_2\ : STD_LOGIC;
  signal \encoded_reg[8]_i_3_n_3\ : STD_LOGIC;
  signal \encoded_reg[8]_i_4_n_1\ : STD_LOGIC;
  signal \encoded_reg[8]_i_4_n_2\ : STD_LOGIC;
  signal \encoded_reg[8]_i_4_n_3\ : STD_LOGIC;
  signal \^encoded_reg[9]\ : STD_LOGIC;
  signal \p_0_in__2\ : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal plusOp : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal \processCol[3]_i_2_n_0\ : STD_LOGIC;
  signal \processCol[7]_i_2_n_0\ : STD_LOGIC;
  signal \processCol[7]_i_3_n_0\ : STD_LOGIC;
  signal \processCol[9]_i_1_n_0\ : STD_LOGIC;
  signal \processCol[9]_i_3_n_0\ : STD_LOGIC;
  signal processRow0 : STD_LOGIC;
  signal \processRow[6]_i_2_n_0\ : STD_LOGIC;
  signal \processRow[9]_i_1_n_0\ : STD_LOGIC;
  signal \processRow[9]_i_4_n_0\ : STD_LOGIC;
  signal \processRow[9]_i_5_n_0\ : STD_LOGIC;
  signal \processRow[9]_i_6_n_0\ : STD_LOGIC;
  signal \processRow[9]_i_7_n_0\ : STD_LOGIC;
  signal row : STD_LOGIC_VECTOR ( 9 downto 6 );
  signal \^sdp_bl.ramb18_dp_bl.ram18_bl\ : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal \NLW_encoded_reg[8]_i_3_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_encoded_reg[8]_i_4_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of R14_carry_i_6 : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of R15_carry_i_8 : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of R16_carry_i_7 : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of R7_carry_i_5 : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of R7_carry_i_7 : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of R8_carry_i_7 : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \dc_bias[3]_i_15\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \dc_bias[3]_i_17\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \dc_bias[3]_i_19\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \dc_bias[3]_i_1__1\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \dc_bias[3]_i_21\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \dc_bias[3]_i_22\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \dc_bias[3]_i_32\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \dc_bias[3]_i_34\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \dc_bias[3]_i_37\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \dc_bias[3]_i_39\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \dc_bias[3]_i_43\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \dc_bias[3]_i_45\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \dc_bias[3]_i_46\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \dc_bias[3]_i_49\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \encoded[0]_i_1\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \encoded[0]_i_1__1\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \encoded[2]_i_1\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \encoded[8]_i_19\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \encoded[8]_i_20\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \encoded[8]_i_21\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \encoded[8]_i_22\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \encoded[8]_i_5__0\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \encoded[8]_i_7__0\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \encoded[9]_i_4\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \processCol[1]_i_1\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \processCol[2]_i_1\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \processCol[3]_i_1\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \processCol[3]_i_2\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \processCol[5]_i_1\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \processCol[6]_i_1\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \processCol[7]_i_1\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \processCol[9]_i_3\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \processRow[0]_i_1\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \processRow[1]_i_1\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \processRow[2]_i_1\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \processRow[3]_i_1\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \processRow[4]_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \processRow[6]_i_2\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \processRow[7]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \processRow[8]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \processRow[9]_i_6\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \processRow[9]_i_7\ : label is "soft_lutpair11";
begin
  Q(5 downto 0) <= \^q\(5 downto 0);
  \encoded_reg[8]_2\ <= \^encoded_reg[8]_2\;
  \encoded_reg[9]\ <= \^encoded_reg[9]\;
  \sdp_bl.ramb18_dp_bl.ram18_bl\(9 downto 0) <= \^sdp_bl.ramb18_dp_bl.ram18_bl\(9 downto 0);
Face: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Scopeface
     port map (
      CO(0) => R17,
      Q(9 downto 6) => row(9 downto 6),
      Q(5 downto 0) => \^q\(5 downto 0),
      S(3) => R17_carry_i_1_n_0,
      S(2) => R17_carry_i_2_n_0,
      S(1) => R17_carry_i_3_n_0,
      S(0) => R17_carry_i_4_n_0,
      \dc_bias_reg[1]\ => \dc_bias_reg[1]\,
      \dc_bias_reg[1]_0\ => \dc_bias_reg[1]_0\,
      \dc_bias_reg[1]_1\ => \dc_bias_reg[1]_1\,
      \dc_bias_reg[3]\(1 downto 0) => \dc_bias_reg[3]\(1 downto 0),
      \dc_bias_reg[3]_0\ => \dc_bias_reg[3]_1\,
      \dc_bias_reg[3]_1\(0) => \dc_bias_reg[3]_2\(0),
      \dc_bias_reg[3]_2\(0) => \dc_bias_reg[3]_0\(0),
      encoded0_in(0) => encoded0_in(0),
      \encoded_reg[8]\(0) => R16,
      \encoded_reg[8]_0\(0) => R15,
      \encoded_reg[8]_1\(0) => R14,
      \encoded_reg[8]_10\ => \encoded_reg[8]_1\,
      \encoded_reg[8]_11\ => \encoded_reg[8]_3\,
      \encoded_reg[8]_2\(0) => R13,
      \encoded_reg[8]_3\(0) => R10,
      \encoded_reg[8]_4\(0) => R9,
      \encoded_reg[8]_5\(0) => R8,
      \encoded_reg[8]_6\(0) => R7,
      \encoded_reg[8]_7\(0) => R6,
      \encoded_reg[8]_8\ => \encoded_reg[8]\,
      \encoded_reg[8]_9\ => \encoded_reg[8]_0\,
      \encoded_reg[9]\ => \encoded_reg[9]_0\,
      \encoded_reg[9]_0\ => \encoded_reg[9]_1\,
      \encoded_reg[9]_1\ => \encoded_reg[9]_2\,
      \encoded_reg[9]_2\ => \encoded_reg[9]_3\,
      \processCol_reg[0]\ => \^encoded_reg[8]_2\,
      \processCol_reg[1]\ => \dc_bias[3]_i_14_n_0\,
      \processCol_reg[2]\ => \dc_bias[3]_i_13_n_0\,
      \processCol_reg[6]\ => \^encoded_reg[9]\,
      \processCol_reg[8]\ => \encoded[8]_i_3_n_0\,
      \processCol_reg[9]\(9 downto 0) => \^sdp_bl.ramb18_dp_bl.ram18_bl\(9 downto 0),
      \processCol_reg[9]_0\ => \encoded[8]_i_14_n_0\,
      \processRow_reg[0]\(0) => \R22__29_carry_i_4_n_0\,
      \processRow_reg[2]\ => \dc_bias[3]_i_10_n_0\,
      \processRow_reg[4]\ => \encoded[8]_i_3__0_n_0\,
      \processRow_reg[4]_0\ => \encoded[9]_i_2_n_0\,
      \processRow_reg[7]\ => \encoded[8]_i_5__0_n_0\,
      \processRow_reg[7]_0\ => \dc_bias[3]_i_3__0_n_0\,
      \processRow_reg[7]_1\ => \encoded[8]_i_13_n_0\,
      \processRow_reg[9]\(0) => ch23,
      \processRow_reg[9]_0\(0) => ch13,
      \processRow_reg[9]_1\ => \dc_bias[3]_i_25_n_0\,
      switch(1 downto 0) => switch(1 downto 0),
      \trigger_time_reg[0]\(3) => R16_carry_i_1_n_0,
      \trigger_time_reg[0]\(2) => R16_carry_i_2_n_0,
      \trigger_time_reg[0]\(1) => R16_carry_i_3_n_0,
      \trigger_time_reg[0]\(0) => R16_carry_i_4_n_0,
      \trigger_time_reg[7]\(3) => R14_carry_i_1_n_0,
      \trigger_time_reg[7]\(2) => R14_carry_i_2_n_0,
      \trigger_time_reg[7]\(1) => R14_carry_i_3_n_0,
      \trigger_time_reg[7]\(0) => R14_carry_i_4_n_0,
      \trigger_time_reg[8]\(3) => R15_carry_i_1_n_0,
      \trigger_time_reg[8]\(2) => R15_carry_i_2_n_0,
      \trigger_time_reg[8]\(1) => R15_carry_i_3_n_0,
      \trigger_time_reg[8]\(0) => R15_carry_i_4_n_0,
      \trigger_time_reg[8]_0\(3) => R13_carry_i_1_n_0,
      \trigger_time_reg[8]_0\(2) => R13_carry_i_2_n_0,
      \trigger_time_reg[8]_0\(1) => R13_carry_i_3_n_0,
      \trigger_time_reg[8]_0\(0) => R13_carry_i_4_n_0,
      \trigger_volt_reg[7]\(3) => R7_carry_i_1_n_0,
      \trigger_volt_reg[7]\(2) => R7_carry_i_2_n_0,
      \trigger_volt_reg[7]\(1) => R7_carry_i_3_n_0,
      \trigger_volt_reg[7]\(0) => R7_carry_i_4_n_0,
      \trigger_volt_reg[7]_0\(3) => R6_carry_i_1_n_0,
      \trigger_volt_reg[7]_0\(2) => R6_carry_i_2_n_0,
      \trigger_volt_reg[7]_0\(1) => R6_carry_i_3_n_0,
      \trigger_volt_reg[7]_0\(0) => R6_carry_i_4_n_0,
      \trigger_volt_reg[8]\(3) => R8_carry_i_1_n_0,
      \trigger_volt_reg[8]\(2) => R8_carry_i_2_n_0,
      \trigger_volt_reg[8]\(1) => R8_carry_i_3_n_0,
      \trigger_volt_reg[8]\(0) => R8_carry_i_4_n_0,
      \trigger_volt_reg[9]\(3) => R10_carry_i_1_n_0,
      \trigger_volt_reg[9]\(2) => R10_carry_i_2_n_0,
      \trigger_volt_reg[9]\(1) => R10_carry_i_3_n_0,
      \trigger_volt_reg[9]\(0) => R10_carry_i_4_n_0,
      \trigger_volt_reg[9]_0\(3) => R9_carry_i_1_n_0,
      \trigger_volt_reg[9]_0\(2) => R9_carry_i_2_n_0,
      \trigger_volt_reg[9]_0\(1) => R9_carry_i_3_n_0,
      \trigger_volt_reg[9]_0\(0) => R9_carry_i_4_n_0
    );
R10_carry_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \trigger_volt_reg[9]\(9),
      I1 => row(9),
      O => R10_carry_i_1_n_0
    );
R10_carry_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => row(6),
      I1 => \trigger_volt_reg[9]\(6),
      I2 => \trigger_volt_reg[9]\(7),
      I3 => row(7),
      I4 => \trigger_volt_reg[9]\(8),
      I5 => row(8),
      O => R10_carry_i_2_n_0
    );
R10_carry_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => \trigger_volt_reg[9]\(4),
      I1 => \^q\(4),
      I2 => \trigger_volt_reg[9]\(5),
      I3 => \^q\(5),
      I4 => \^q\(3),
      I5 => \trigger_volt_reg[9]\(3),
      O => R10_carry_i_3_n_0
    );
R10_carry_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => \trigger_volt_reg[9]\(1),
      I1 => \^q\(1),
      I2 => \trigger_volt_reg[9]\(0),
      I3 => \^q\(0),
      I4 => \^q\(2),
      I5 => \trigger_volt_reg[9]\(2),
      O => R10_carry_i_4_n_0
    );
R13_carry_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"D22D"
    )
        port map (
      I0 => \trigger_time_reg[5]_1\,
      I1 => \trigger_time_reg[8]\(3),
      I2 => \trigger_time_reg[9]\,
      I3 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(9),
      O => R13_carry_i_1_n_0
    );
R13_carry_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8241410800000020"
    )
        port map (
      I0 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(6),
      I1 => \trigger_time_reg[7]\,
      I2 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(7),
      I3 => \trigger_time_reg[5]\,
      I4 => \trigger_time_reg[6]\,
      I5 => R16_carry_i_7_n_0,
      O => R13_carry_i_2_n_0
    );
R13_carry_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0882100020000882"
    )
        port map (
      I0 => R14_carry_i_6_n_0,
      I1 => \trigger_time_reg[3]\,
      I2 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(3),
      I3 => \trigger_time_reg[1]\,
      I4 => \trigger_time_reg[4]\,
      I5 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(4),
      O => R13_carry_i_3_n_0
    );
R13_carry_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0900000900909000"
    )
        port map (
      I0 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(0),
      I1 => \trigger_time_reg[8]\(0),
      I2 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(1),
      I3 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(2),
      I4 => \trigger_time_reg[2]\,
      I5 => \trigger_time_reg[8]\(1),
      O => R13_carry_i_4_n_0
    );
R14_carry_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFF800080007FFF"
    )
        port map (
      I0 => \trigger_time_reg[7]\,
      I1 => \trigger_time_reg[4]_0\,
      I2 => \trigger_time_reg[6]\,
      I3 => \trigger_time_reg[8]\(3),
      I4 => \trigger_time_reg[9]\,
      I5 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(9),
      O => R14_carry_i_1_n_0
    );
R14_carry_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0480804010080804"
    )
        port map (
      I0 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(6),
      I1 => R16_carry_i_7_n_0,
      I2 => \trigger_time_reg[7]\,
      I3 => \trigger_time_reg[4]_0\,
      I4 => \trigger_time_reg[6]\,
      I5 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(7),
      O => R14_carry_i_2_n_0
    );
R14_carry_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0090420090000090"
    )
        port map (
      I0 => \trigger_time_reg[4]\,
      I1 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(4),
      I2 => R14_carry_i_6_n_0,
      I3 => \trigger_time_reg[1]_0\,
      I4 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(3),
      I5 => \trigger_time_reg[3]\,
      O => R14_carry_i_3_n_0
    );
R14_carry_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0009090090000090"
    )
        port map (
      I0 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(0),
      I1 => \trigger_time_reg[8]\(0),
      I2 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(1),
      I3 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(2),
      I4 => \trigger_time_reg[2]\,
      I5 => \trigger_time_reg[8]\(1),
      O => R14_carry_i_4_n_0
    );
R14_carry_i_6: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(5),
      I1 => \trigger_time_reg[8]\(2),
      O => R14_carry_i_6_n_0
    );
R15_carry_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FB0404FB"
    )
        port map (
      I0 => \trigger_time_reg[8]\(3),
      I1 => \trigger_time_reg[5]_1\,
      I2 => \trigger_time_reg[8]\(0),
      I3 => \trigger_time_reg[9]\,
      I4 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(9),
      O => R15_carry_i_1_n_0
    );
R15_carry_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"C8CA"
    )
        port map (
      I0 => R15_carry_i_6_n_0,
      I1 => R15_carry_i_7_n_0,
      I2 => \trigger_time_reg[8]\(0),
      I3 => R15_carry_i_8_n_0,
      O => R15_carry_i_2_n_0
    );
R15_carry_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000FE0101FE"
    )
        port map (
      I0 => \trigger_time_reg[2]\,
      I1 => \trigger_time_reg[8]\(1),
      I2 => \trigger_time_reg[8]\(0),
      I3 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(3),
      I4 => \trigger_time_reg[3]\,
      I5 => R15_carry_i_9_n_0,
      O => R15_carry_i_3_n_0
    );
R15_carry_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2140000800082140"
    )
        port map (
      I0 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(1),
      I1 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(0),
      I2 => \trigger_time_reg[8]\(1),
      I3 => \trigger_time_reg[8]\(0),
      I4 => \trigger_time_reg[2]\,
      I5 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(2),
      O => R15_carry_i_4_n_0
    );
R15_carry_i_6: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCCC33330130CCCC"
    )
        port map (
      I0 => R16_carry_i_7_n_0,
      I1 => \trigger_time_reg[6]\,
      I2 => \trigger_time_reg[7]\,
      I3 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(7),
      I4 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(6),
      I5 => \trigger_time_reg[5]\,
      O => R15_carry_i_6_n_0
    );
R15_carry_i_7: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => \trigger_time_reg[8]\(3),
      I1 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(8),
      I2 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(6),
      I3 => \trigger_time_reg[6]\,
      I4 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(7),
      I5 => \trigger_time_reg[7]\,
      O => R15_carry_i_7_n_0
    );
R15_carry_i_8: unisim.vcomponents.LUT5
    generic map(
      INIT => X"666F0F06"
    )
        port map (
      I0 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(8),
      I1 => \trigger_time_reg[8]\(3),
      I2 => \trigger_time_reg[6]_0\,
      I3 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(7),
      I4 => \trigger_time_reg[7]\,
      O => R15_carry_i_8_n_0
    );
R15_carry_i_9: unisim.vcomponents.LUT6
    generic map(
      INIT => X"77777E77DDDDD7DD"
    )
        port map (
      I0 => R14_carry_i_6_n_0,
      I1 => \trigger_time_reg[4]\,
      I2 => \trigger_time_reg[3]\,
      I3 => \trigger_time_reg[1]\,
      I4 => \trigger_time_reg[8]\(0),
      I5 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(4),
      O => R15_carry_i_9_n_0
    );
R16_carry_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7F80807F"
    )
        port map (
      I0 => \trigger_time_reg[8]\(0),
      I1 => \trigger_time_reg[6]_1\,
      I2 => \trigger_time_reg[8]\(3),
      I3 => \trigger_time_reg[9]\,
      I4 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(9),
      O => R16_carry_i_1_n_0
    );
R16_carry_i_10: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0090420090000090"
    )
        port map (
      I0 => \trigger_time_reg[4]\,
      I1 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(4),
      I2 => R14_carry_i_6_n_0,
      I3 => \trigger_time_reg[1]_0\,
      I4 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(3),
      I5 => \trigger_time_reg[3]\,
      O => R16_carry_i_10_n_0
    );
R16_carry_i_11: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(4),
      I1 => \trigger_time_reg[4]\,
      I2 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(5),
      I3 => \trigger_time_reg[8]\(2),
      I4 => \trigger_time_reg[3]\,
      I5 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(3),
      O => R16_carry_i_11_n_0
    );
R16_carry_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0006060060000006"
    )
        port map (
      I0 => \trigger_time_reg[7]_0\,
      I1 => R16_carry_i_7_n_0,
      I2 => R16_carry_i_8_n_0,
      I3 => \trigger_time_reg[6]\,
      I4 => \trigger_time_reg[5]_0\,
      I5 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(6),
      O => R16_carry_i_2_n_0
    );
R16_carry_i_3: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => R16_carry_i_10_n_0,
      I1 => \trigger_time_reg[8]\(0),
      I2 => R16_carry_i_11_n_0,
      O => R16_carry_i_3_n_0
    );
R16_carry_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0810100824000024"
    )
        port map (
      I0 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(1),
      I1 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(0),
      I2 => \trigger_time_reg[8]\(0),
      I3 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(2),
      I4 => \trigger_time_reg[2]\,
      I5 => \trigger_time_reg[8]\(1),
      O => R16_carry_i_4_n_0
    );
R16_carry_i_7: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(8),
      I1 => \trigger_time_reg[8]\(3),
      O => R16_carry_i_7_n_0
    );
R16_carry_i_8: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(7),
      I1 => \trigger_time_reg[7]\,
      O => R16_carry_i_8_n_0
    );
R17_carry_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \trigger_time_reg[9]\,
      I1 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(9),
      O => R17_carry_i_1_n_0
    );
R17_carry_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => \trigger_time_reg[8]\(3),
      I1 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(8),
      I2 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(6),
      I3 => \trigger_time_reg[6]\,
      I4 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(7),
      I5 => \trigger_time_reg[7]\,
      O => R17_carry_i_2_n_0
    );
R17_carry_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(4),
      I1 => \trigger_time_reg[4]\,
      I2 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(5),
      I3 => \trigger_time_reg[8]\(2),
      I4 => \trigger_time_reg[3]\,
      I5 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(3),
      O => R17_carry_i_3_n_0
    );
R17_carry_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(0),
      I1 => \trigger_time_reg[8]\(0),
      I2 => \trigger_time_reg[2]\,
      I3 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(2),
      I4 => \trigger_time_reg[8]\(1),
      I5 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(1),
      O => R17_carry_i_4_n_0
    );
\R22__29_carry_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(0),
      O => \R22__29_carry_i_4_n_0\
    );
R6_carry_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FD0202FD"
    )
        port map (
      I0 => \trigger_volt_reg[5]_0\,
      I1 => \trigger_volt_reg[9]\(7),
      I2 => \trigger_volt_reg[9]\(8),
      I3 => \trigger_volt_reg[9]\(9),
      I4 => row(9),
      O => R6_carry_i_1_n_0
    );
R6_carry_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0041200800820041"
    )
        port map (
      I0 => row(6),
      I1 => \trigger_volt_reg[9]\(7),
      I2 => row(7),
      I3 => R7_carry_i_5_n_0,
      I4 => \trigger_volt_reg[9]\(6),
      I5 => \trigger_volt_reg[4]\,
      O => R6_carry_i_2_n_0
    );
R6_carry_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2802008000402802"
    )
        port map (
      I0 => R7_carry_i_7_n_0,
      I1 => \trigger_volt_reg[1]\,
      I2 => \^q\(3),
      I3 => \trigger_volt_reg[9]\(3),
      I4 => \^q\(4),
      I5 => \trigger_volt_reg[9]\(4),
      O => R6_carry_i_3_n_0
    );
R6_carry_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0900000900909000"
    )
        port map (
      I0 => \trigger_volt_reg[9]\(0),
      I1 => \^q\(0),
      I2 => \^q\(1),
      I3 => \trigger_volt_reg[9]\(2),
      I4 => \^q\(2),
      I5 => \trigger_volt_reg[9]\(1),
      O => R6_carry_i_4_n_0
    );
R7_carry_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7F80807F"
    )
        port map (
      I0 => \trigger_volt_reg[9]\(7),
      I1 => \trigger_volt_reg[5]\,
      I2 => \trigger_volt_reg[9]\(8),
      I3 => \trigger_volt_reg[9]\(9),
      I4 => row(9),
      O => R7_carry_i_1_n_0
    );
R7_carry_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0441100020000441"
    )
        port map (
      I0 => R7_carry_i_5_n_0,
      I1 => row(6),
      I2 => \trigger_volt_reg[4]_0\,
      I3 => \trigger_volt_reg[9]\(6),
      I4 => \trigger_volt_reg[9]\(7),
      I5 => row(7),
      O => R7_carry_i_2_n_0
    );
R7_carry_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0090900024000090"
    )
        port map (
      I0 => \^q\(4),
      I1 => \trigger_volt_reg[9]\(4),
      I2 => R7_carry_i_7_n_0,
      I3 => \trigger_volt_reg[1]_0\,
      I4 => \trigger_volt_reg[9]\(3),
      I5 => \^q\(3),
      O => R7_carry_i_3_n_0
    );
R7_carry_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0009090090000090"
    )
        port map (
      I0 => \trigger_volt_reg[9]\(0),
      I1 => \^q\(0),
      I2 => \^q\(1),
      I3 => \trigger_volt_reg[9]\(2),
      I4 => \^q\(2),
      I5 => \trigger_volt_reg[9]\(1),
      O => R7_carry_i_4_n_0
    );
R7_carry_i_5: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \trigger_volt_reg[9]\(8),
      I1 => row(8),
      O => R7_carry_i_5_n_0
    );
R7_carry_i_7: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \trigger_volt_reg[9]\(5),
      I1 => \^q\(5),
      O => R7_carry_i_7_n_0
    );
R8_carry_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFEF00100010FFEF"
    )
        port map (
      I0 => \trigger_volt_reg[9]\(8),
      I1 => \trigger_volt_reg[9]\(7),
      I2 => \trigger_volt_reg[5]_0\,
      I3 => \trigger_volt_reg[9]\(0),
      I4 => \trigger_volt_reg[9]\(9),
      I5 => row(9),
      O => R8_carry_i_1_n_0
    );
R8_carry_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00E2"
    )
        port map (
      I0 => R8_carry_i_6_n_0,
      I1 => \trigger_volt_reg[9]\(0),
      I2 => R9_carry_i_7_n_0,
      I3 => R8_carry_i_7_n_0,
      O => R8_carry_i_2_n_0
    );
R8_carry_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9000009000609000"
    )
        port map (
      I0 => \^q\(4),
      I1 => \trigger_volt_reg[9]\(4),
      I2 => R8_carry_i_8_n_0,
      I3 => \trigger_volt_reg[9]\(3),
      I4 => \^q\(3),
      I5 => \trigger_volt_reg[2]\,
      O => R8_carry_i_3_n_0
    );
R8_carry_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1804002000201804"
    )
        port map (
      I0 => \^q\(1),
      I1 => \trigger_volt_reg[9]\(0),
      I2 => \^q\(0),
      I3 => \trigger_volt_reg[9]\(1),
      I4 => \^q\(2),
      I5 => \trigger_volt_reg[9]\(2),
      O => R8_carry_i_4_n_0
    );
R8_carry_i_6: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00FF3800FF0000FF"
    )
        port map (
      I0 => R7_carry_i_5_n_0,
      I1 => row(7),
      I2 => \trigger_volt_reg[9]\(7),
      I3 => \trigger_volt_reg[4]\,
      I4 => \trigger_volt_reg[9]\(6),
      I5 => row(6),
      O => R8_carry_i_6_n_0
    );
R8_carry_i_7: unisim.vcomponents.LUT5
    generic map(
      INIT => X"3C7D5514"
    )
        port map (
      I0 => \trigger_volt_reg[5]_0\,
      I1 => row(8),
      I2 => \trigger_volt_reg[9]\(8),
      I3 => row(7),
      I4 => \trigger_volt_reg[9]\(7),
      O => R8_carry_i_7_n_0
    );
R8_carry_i_8: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAAAAAAAAA9"
    )
        port map (
      I0 => R7_carry_i_7_n_0,
      I1 => \trigger_volt_reg[9]\(0),
      I2 => \trigger_volt_reg[9]\(3),
      I3 => \trigger_volt_reg[9]\(1),
      I4 => \trigger_volt_reg[9]\(2),
      I5 => \trigger_volt_reg[9]\(4),
      O => R8_carry_i_8_n_0
    );
R9_carry_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AAAAAAA95555555"
    )
        port map (
      I0 => \trigger_volt_reg[9]\(9),
      I1 => \trigger_volt_reg[9]\(0),
      I2 => \trigger_volt_reg[9]\(7),
      I3 => \trigger_volt_reg[5]\,
      I4 => \trigger_volt_reg[9]\(8),
      I5 => row(9),
      O => R9_carry_i_1_n_0
    );
R9_carry_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AA82FFC3AA820000"
    )
        port map (
      I0 => R9_carry_i_6_n_0,
      I1 => row(7),
      I2 => \trigger_volt_reg[9]\(7),
      I3 => \trigger_volt_reg[5]\,
      I4 => \trigger_volt_reg[9]\(0),
      I5 => R9_carry_i_7_n_0,
      O => R9_carry_i_2_n_0
    );
R9_carry_i_3: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => R9_carry_i_8_n_0,
      I1 => \trigger_volt_reg[9]\(0),
      I2 => R9_carry_i_9_n_0,
      O => R9_carry_i_3_n_0
    );
R9_carry_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0110800820024004"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(0),
      I2 => \trigger_volt_reg[9]\(2),
      I3 => \^q\(2),
      I4 => \trigger_volt_reg[9]\(0),
      I5 => \trigger_volt_reg[9]\(1),
      O => R9_carry_i_4_n_0
    );
R9_carry_i_6: unisim.vcomponents.LUT6
    generic map(
      INIT => X"100000000AA54AA5"
    )
        port map (
      I0 => row(6),
      I1 => row(7),
      I2 => \trigger_volt_reg[9]\(6),
      I3 => \trigger_volt_reg[4]_0\,
      I4 => \trigger_volt_reg[9]\(7),
      I5 => R7_carry_i_5_n_0,
      O => R9_carry_i_6_n_0
    );
R9_carry_i_7: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => row(6),
      I1 => \trigger_volt_reg[9]\(6),
      I2 => \trigger_volt_reg[9]\(7),
      I3 => row(7),
      I4 => \trigger_volt_reg[9]\(8),
      I5 => row(8),
      O => R9_carry_i_7_n_0
    );
R9_carry_i_8: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0090900024000090"
    )
        port map (
      I0 => \^q\(4),
      I1 => \trigger_volt_reg[9]\(4),
      I2 => R7_carry_i_7_n_0,
      I3 => \trigger_volt_reg[1]_0\,
      I4 => \trigger_volt_reg[9]\(3),
      I5 => \^q\(3),
      O => R9_carry_i_8_n_0
    );
R9_carry_i_9: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => \trigger_volt_reg[9]\(4),
      I1 => \^q\(4),
      I2 => \trigger_volt_reg[9]\(5),
      I3 => \^q\(5),
      I4 => \^q\(3),
      I5 => \trigger_volt_reg[9]\(3),
      O => R9_carry_i_9_n_0
    );
\dc_bias[3]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0404100000100400"
    )
        port map (
      I0 => \dc_bias[3]_i_19_n_0\,
      I1 => \^q\(2),
      I2 => \dc_bias[3]_i_20_n_0\,
      I3 => \dc_bias[3]_i_21_n_0\,
      I4 => \^q\(1),
      I5 => \dc_bias[3]_i_22_n_0\,
      O => \dc_bias[3]_i_10_n_0\
    );
\dc_bias[3]_i_13\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"5454A8FF"
    )
        port map (
      I0 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(2),
      I1 => \dc_bias[3]_i_26_n_0\,
      I2 => \dc_bias[3]_i_27_n_0\,
      I3 => \dc_bias[3]_i_28_n_0\,
      I4 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(1),
      O => \dc_bias[3]_i_13_n_0\
    );
\dc_bias[3]_i_14\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0007000700000007"
    )
        port map (
      I0 => \dc_bias[3]_i_28_n_0\,
      I1 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(1),
      I2 => \dc_bias[3]_i_29_n_0\,
      I3 => \dc_bias[3]_i_30_n_0\,
      I4 => \dc_bias[3]_i_31_n_0\,
      I5 => \dc_bias[3]_i_26_n_0\,
      O => \dc_bias[3]_i_14_n_0\
    );
\dc_bias[3]_i_15\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000001"
    )
        port map (
      I0 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(5),
      I1 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(6),
      I2 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(7),
      I3 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(8),
      I4 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(9),
      O => \dc_bias[3]_i_15_n_0\
    );
\dc_bias[3]_i_16\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAFEFEFEFEFEFEFE"
    )
        port map (
      I0 => R17,
      I1 => R13,
      I2 => R14,
      I3 => \^q\(2),
      I4 => \^q\(1),
      I5 => \^q\(4),
      O => \dc_bias[3]_i_16_n_0\
    );
\dc_bias[3]_i_17\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(2),
      I2 => \^q\(1),
      O => \dc_bias[3]_i_17_n_0\
    );
\dc_bias[3]_i_18\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F7FFF7FFF7FFFFFF"
    )
        port map (
      I0 => \dc_bias[3]_i_32_n_0\,
      I1 => \^q\(4),
      I2 => \^q\(3),
      I3 => \^q\(2),
      I4 => \^q\(1),
      I5 => \^q\(0),
      O => \dc_bias[3]_i_18_n_0\
    );
\dc_bias[3]_i_19\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1FFF"
    )
        port map (
      I0 => \processCol[7]_i_3_n_0\,
      I1 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(6),
      I2 => \dc_bias[3]_i_33_n_0\,
      I3 => \encoded[8]_i_13_n_0\,
      O => \dc_bias[3]_i_19_n_0\
    );
\dc_bias[3]_i_1__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^encoded_reg[9]\,
      O => SR(0)
    );
\dc_bias[3]_i_20\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1442546276EA7EEB"
    )
        port map (
      I0 => \dc_bias[3]_i_34_n_0\,
      I1 => \dc_bias[3]_i_35_n_0\,
      I2 => \^q\(3),
      I3 => \^q\(4),
      I4 => \^q\(2),
      I5 => \dc_bias[3]_i_36_n_0\,
      O => \dc_bias[3]_i_20_n_0\
    );
\dc_bias[3]_i_21\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"936CC936"
    )
        port map (
      I0 => \^q\(2),
      I1 => \dc_bias[3]_i_35_n_0\,
      I2 => \^q\(3),
      I3 => \^q\(4),
      I4 => \dc_bias[3]_i_36_n_0\,
      O => \dc_bias[3]_i_21_n_0\
    );
\dc_bias[3]_i_22\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \dc_bias[3]_i_36_n_0\,
      I1 => \^q\(3),
      I2 => \^q\(2),
      O => \dc_bias[3]_i_22_n_0\
    );
\dc_bias[3]_i_25\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FEAAAAAAAAAAAAAA"
    )
        port map (
      I0 => row(9),
      I1 => row(6),
      I2 => \^q\(5),
      I3 => row(8),
      I4 => row(7),
      I5 => \encoded[8]_i_18_n_0\,
      O => \dc_bias[3]_i_25_n_0\
    );
\dc_bias[3]_i_26\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000FFFFFEFF"
    )
        port map (
      I0 => \dc_bias[3]_i_37_n_0\,
      I1 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(2),
      I2 => \dc_bias[3]_i_38_n_0\,
      I3 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(3),
      I4 => \dc_bias[3]_i_39_n_0\,
      I5 => \dc_bias[3]_i_40_n_0\,
      O => \dc_bias[3]_i_26_n_0\
    );
\dc_bias[3]_i_27\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0008000000200000"
    )
        port map (
      I0 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(2),
      I1 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(4),
      I2 => \dc_bias[3]_i_41_n_0\,
      I3 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(3),
      I4 => \dc_bias[3]_i_38_n_0\,
      I5 => \dc_bias[3]_i_42_n_0\,
      O => \dc_bias[3]_i_27_n_0\
    );
\dc_bias[3]_i_28\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CD32FB04DF20B34C"
    )
        port map (
      I0 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(2),
      I1 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(4),
      I2 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(3),
      I3 => \dc_bias[3]_i_42_n_0\,
      I4 => \dc_bias[3]_i_41_n_0\,
      I5 => \dc_bias[3]_i_38_n_0\,
      O => \dc_bias[3]_i_28_n_0\
    );
\dc_bias[3]_i_29\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"E81717E817E8E817"
    )
        port map (
      I0 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(2),
      I1 => \dc_bias[3]_i_38_n_0\,
      I2 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(3),
      I3 => \dc_bias[3]_i_41_n_0\,
      I4 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(4),
      I5 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(0),
      O => \dc_bias[3]_i_29_n_0\
    );
\dc_bias[3]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000FE0FFFFF"
    )
        port map (
      I0 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(6),
      I1 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(5),
      I2 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(7),
      I3 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(8),
      I4 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(9),
      I5 => \dc_bias[3]_i_6_n_0\,
      O => \^encoded_reg[9]\
    );
\dc_bias[3]_i_30\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8558FFFFFFFFFFFF"
    )
        port map (
      I0 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(2),
      I1 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(1),
      I2 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(3),
      I3 => \dc_bias[3]_i_38_n_0\,
      I4 => \dc_bias[3]_i_43_n_0\,
      I5 => \encoded[8]_i_14_n_0\,
      O => \dc_bias[3]_i_30_n_0\
    );
\dc_bias[3]_i_31\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000F7700FF00000F"
    )
        port map (
      I0 => \dc_bias[3]_i_39_n_0\,
      I1 => \dc_bias[3]_i_37_n_0\,
      I2 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(1),
      I3 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(2),
      I4 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(3),
      I5 => \dc_bias[3]_i_38_n_0\,
      O => \dc_bias[3]_i_31_n_0\
    );
\dc_bias[3]_i_32\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000001"
    )
        port map (
      I0 => row(7),
      I1 => \^q\(5),
      I2 => row(6),
      I3 => row(9),
      I4 => row(8),
      O => \dc_bias[3]_i_32_n_0\
    );
\dc_bias[3]_i_33\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000B00000000"
    )
        port map (
      I0 => \dc_bias[3]_i_44_n_0\,
      I1 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(6),
      I2 => \^q\(0),
      I3 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(9),
      I4 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(7),
      I5 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(8),
      O => \dc_bias[3]_i_33_n_0\
    );
\dc_bias[3]_i_34\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"793C9EC7"
    )
        port map (
      I0 => row(6),
      I1 => row(7),
      I2 => row(9),
      I3 => row(8),
      I4 => \^q\(5),
      O => \dc_bias[3]_i_34_n_0\
    );
\dc_bias[3]_i_35\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"C68C3163CE9C73E7"
    )
        port map (
      I0 => \^q\(5),
      I1 => row(6),
      I2 => row(7),
      I3 => row(9),
      I4 => row(8),
      I5 => \^q\(4),
      O => \dc_bias[3]_i_35_n_0\
    );
\dc_bias[3]_i_36\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7C7F7C5715C101C1"
    )
        port map (
      I0 => \dc_bias[3]_i_35_n_0\,
      I1 => \^q\(5),
      I2 => \dc_bias[3]_i_45_n_0\,
      I3 => \^q\(4),
      I4 => \^q\(3),
      I5 => \dc_bias[3]_i_46_n_0\,
      O => \dc_bias[3]_i_36_n_0\
    );
\dc_bias[3]_i_37\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(4),
      I1 => \dc_bias[3]_i_41_n_0\,
      I2 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(3),
      O => \dc_bias[3]_i_37_n_0\
    );
\dc_bias[3]_i_38\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"04000000FFFFFFDF"
    )
        port map (
      I0 => \dc_bias[3]_i_42_n_0\,
      I1 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(3),
      I2 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(4),
      I3 => \dc_bias[3]_i_47_n_0\,
      I4 => \dc_bias[3]_i_41_n_0\,
      I5 => \dc_bias[3]_i_48_n_0\,
      O => \dc_bias[3]_i_38_n_0\
    );
\dc_bias[3]_i_39\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"C936936C"
    )
        port map (
      I0 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(3),
      I1 => \dc_bias[3]_i_49_n_0\,
      I2 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(4),
      I3 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(5),
      I4 => \dc_bias[3]_i_41_n_0\,
      O => \dc_bias[3]_i_39_n_0\
    );
\dc_bias[3]_i_3__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => \encoded[8]_i_5__0_n_0\,
      I1 => switch(1),
      I2 => ch23,
      O => \dc_bias[3]_i_3__0_n_0\
    );
\dc_bias[3]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFF2F2F2F2F2F2F2"
    )
        port map (
      I0 => \dc_bias[3]_i_7_n_0\,
      I1 => \dc_bias[3]_i_8_n_0\,
      I2 => \dc_bias[3]_i_9_n_0\,
      I3 => ch13,
      I4 => switch(0),
      I5 => \encoded[8]_i_5__0_n_0\,
      O => \^encoded_reg[8]_2\
    );
\dc_bias[3]_i_40\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1200EDFF0048FFB7"
    )
        port map (
      I0 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(5),
      I1 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(4),
      I2 => \dc_bias[3]_i_49_n_0\,
      I3 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(3),
      I4 => \dc_bias[3]_i_47_n_0\,
      I5 => \dc_bias[3]_i_41_n_0\,
      O => \dc_bias[3]_i_40_n_0\
    );
\dc_bias[3]_i_41\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0EF038F0F0F0F0F0"
    )
        port map (
      I0 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(4),
      I1 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(9),
      I2 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(8),
      I3 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(6),
      I4 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(5),
      I5 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(7),
      O => \dc_bias[3]_i_41_n_0\
    );
\dc_bias[3]_i_42\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"807F7F807F0080FF"
    )
        port map (
      I0 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(7),
      I1 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(6),
      I2 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(8),
      I3 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(9),
      I4 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(4),
      I5 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(5),
      O => \dc_bias[3]_i_42_n_0\
    );
\dc_bias[3]_i_43\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00004000"
    )
        port map (
      I0 => row(8),
      I1 => \^q\(0),
      I2 => \^q\(4),
      I3 => \^q\(3),
      I4 => \dc_bias[3]_i_50_n_0\,
      O => \dc_bias[3]_i_43_n_0\
    );
\dc_bias[3]_i_44\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000100"
    )
        port map (
      I0 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(3),
      I1 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(2),
      I2 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(1),
      I3 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(0),
      I4 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(5),
      I5 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(4),
      O => \dc_bias[3]_i_44_n_0\
    );
\dc_bias[3]_i_45\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0BF0C2BC"
    )
        port map (
      I0 => \^q\(5),
      I1 => row(8),
      I2 => row(9),
      I3 => row(7),
      I4 => row(6),
      O => \dc_bias[3]_i_45_n_0\
    );
\dc_bias[3]_i_46\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A6DB5924"
    )
        port map (
      I0 => row(8),
      I1 => row(9),
      I2 => row(7),
      I3 => row(6),
      I4 => \^q\(5),
      O => \dc_bias[3]_i_46_n_0\
    );
\dc_bias[3]_i_47\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B33333CC334C4CCC"
    )
        port map (
      I0 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(7),
      I1 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(6),
      I2 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(8),
      I3 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(9),
      I4 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(5),
      I5 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(4),
      O => \dc_bias[3]_i_47_n_0\
    );
\dc_bias[3]_i_48\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FE00C80011FF77FF"
    )
        port map (
      I0 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(4),
      I1 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(9),
      I2 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(8),
      I3 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(6),
      I4 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(5),
      I5 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(7),
      O => \dc_bias[3]_i_48_n_0\
    );
\dc_bias[3]_i_49\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"4AAAAAAA"
    )
        port map (
      I0 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(9),
      I1 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(5),
      I2 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(8),
      I3 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(6),
      I4 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(7),
      O => \dc_bias[3]_i_49_n_0\
    );
\dc_bias[3]_i_50\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF9FFFFFFFFF"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(1),
      I2 => row(7),
      I3 => row(9),
      I4 => \^q\(5),
      I5 => row(6),
      O => \dc_bias[3]_i_50_n_0\
    );
\dc_bias[3]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"02AAFFFF02AA0000"
    )
        port map (
      I0 => \processRow[9]_i_6_n_0\,
      I1 => \^q\(0),
      I2 => \^q\(1),
      I3 => \processRow[6]_i_2_n_0\,
      I4 => row(9),
      I5 => \encoded[9]_i_4_n_0\,
      O => \dc_bias[3]_i_6_n_0\
    );
\dc_bias[3]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF7F7F7F00"
    )
        port map (
      I0 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(0),
      I1 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(1),
      I2 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(2),
      I3 => R8,
      I4 => R9,
      I5 => \encoded[8]_i_8__0_n_0\,
      O => \dc_bias[3]_i_7_n_0\
    );
\dc_bias[3]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DFDFDFFFFFFFFFFF"
    )
        port map (
      I0 => \dc_bias[3]_i_15_n_0\,
      I1 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(3),
      I2 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(4),
      I3 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(0),
      I4 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(1),
      I5 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(2),
      O => \dc_bias[3]_i_8_n_0\
    );
\dc_bias[3]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000AAFEFEFE"
    )
        port map (
      I0 => \dc_bias[3]_i_16_n_0\,
      I1 => R16,
      I2 => R15,
      I3 => \^q\(4),
      I4 => \dc_bias[3]_i_17_n_0\,
      I5 => \dc_bias[3]_i_18_n_0\,
      O => \dc_bias[3]_i_9_n_0\
    );
\encoded[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"A3"
    )
        port map (
      I0 => \dc_bias_reg[3]\(1),
      I1 => \encoded[8]_i_3_n_0\,
      I2 => \^encoded_reg[9]\,
      O => \encoded_reg[0]_0\
    );
\encoded[0]_i_1__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^encoded_reg[9]\,
      I1 => \dc_bias_reg[3]_0\(0),
      O => \encoded_reg[0]\
    );
\encoded[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"EA"
    )
        port map (
      I0 => \encoded[8]_i_3_n_0\,
      I1 => \dc_bias_reg[3]\(1),
      I2 => \^encoded_reg[9]\,
      O => \encoded_reg[2]\
    );
\encoded[8]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0002211084400002"
    )
        port map (
      I0 => row(6),
      I1 => row(8),
      I2 => DOADO(3),
      I3 => \sdp_bl.ramb18_dp_bl.ram18_bl_0\,
      I4 => DOADO(4),
      I5 => row(7),
      O => \encoded[8]_i_10_n_0\
    );
\encoded[8]_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1000048204821000"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      I2 => DOADO(1),
      I3 => DOADO(0),
      I4 => \^q\(2),
      I5 => DOADO(2),
      O => \encoded[8]_i_12_n_0\
    );
\encoded[8]_i_13\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000FF5F0000FCFF"
    )
        port map (
      I0 => \encoded[8]_i_18_n_0\,
      I1 => \encoded[8]_i_19_n_0\,
      I2 => row(7),
      I3 => \encoded[8]_i_20_n_0\,
      I4 => row(9),
      I5 => row(8),
      O => \encoded[8]_i_13_n_0\
    );
\encoded[8]_i_14\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B3F3F3F3B3F3F3C3"
    )
        port map (
      I0 => \encoded[8]_i_21_n_0\,
      I1 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(9),
      I2 => \encoded[8]_i_22_n_0\,
      I3 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(6),
      I4 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(5),
      I5 => \encoded[8]_i_23_n_0\,
      O => \encoded[8]_i_14_n_0\
    );
\encoded[8]_i_18\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFE0"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      I2 => \^q\(2),
      I3 => \^q\(4),
      I4 => \^q\(3),
      I5 => row(6),
      O => \encoded[8]_i_18_n_0\
    );
\encoded[8]_i_19\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"A8"
    )
        port map (
      I0 => \^q\(4),
      I1 => \^q\(3),
      I2 => \^q\(2),
      O => \encoded[8]_i_19_n_0\
    );
\encoded[8]_i_20\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(5),
      I1 => row(6),
      O => \encoded[8]_i_20_n_0\
    );
\encoded[8]_i_21\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"01555555"
    )
        port map (
      I0 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(4),
      I1 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(0),
      I2 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(1),
      I3 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(2),
      I4 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(3),
      O => \encoded[8]_i_21_n_0\
    );
\encoded[8]_i_22\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(7),
      I1 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(8),
      O => \encoded[8]_i_22_n_0\
    );
\encoded[8]_i_23\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"A8"
    )
        port map (
      I0 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(4),
      I1 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(3),
      I2 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(2),
      O => \encoded[8]_i_23_n_0\
    );
\encoded[8]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1554000000000000"
    )
        port map (
      I0 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(8),
      I1 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(6),
      I2 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(5),
      I3 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(4),
      I4 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(9),
      I5 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(7),
      O => \encoded[8]_i_3_n_0\
    );
\encoded[8]_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBBBBBBBABABABAA"
    )
        port map (
      I0 => \dc_bias[3]_i_9_n_0\,
      I1 => \dc_bias[3]_i_8_n_0\,
      I2 => \encoded[8]_i_7__0_n_0\,
      I3 => R8,
      I4 => R9,
      I5 => \encoded[8]_i_8__0_n_0\,
      O => \encoded[8]_i_3__0_n_0\
    );
\encoded[8]_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => row(9),
      O => \encoded[8]_i_5_n_0\
    );
\encoded[8]_i_5__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \encoded[8]_i_13_n_0\,
      I1 => \encoded[8]_i_14_n_0\,
      O => \encoded[8]_i_5__0_n_0\
    );
\encoded[8]_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00009009"
    )
        port map (
      I0 => \sdp_bl.ramb18_dp_bl.ram18_bl_1\(0),
      I1 => row(6),
      I2 => row(7),
      I3 => \sdp_bl.ramb18_dp_bl.ram18_bl_1\(1),
      I4 => row(8),
      O => \encoded[8]_i_6_n_0\
    );
\encoded[8]_i_7__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(0),
      I1 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(1),
      I2 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(2),
      O => \encoded[8]_i_7__0_n_0\
    );
\encoded[8]_i_8__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAFEFEFE"
    )
        port map (
      I0 => R10,
      I1 => R6,
      I2 => R7,
      I3 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(2),
      I4 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(1),
      O => \encoded[8]_i_8__0_n_0\
    );
\encoded[8]_i_9\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => row(9),
      O => \encoded[8]_i_9_n_0\
    );
\encoded[9]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000002000000000"
    )
        port map (
      I0 => \encoded[9]_i_4_n_0\,
      I1 => \^q\(4),
      I2 => \^q\(1),
      I3 => row(9),
      I4 => \^q\(2),
      I5 => \^q\(3),
      O => \encoded[9]_i_2_n_0\
    );
\encoded[9]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => row(8),
      I1 => row(7),
      I2 => row(6),
      I3 => \^q\(5),
      O => \encoded[9]_i_4_n_0\
    );
\encoded_reg[8]_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => ch23,
      CO(2) => \encoded_reg[8]_i_3_n_1\,
      CO(1) => \encoded_reg[8]_i_3_n_2\,
      CO(0) => \encoded_reg[8]_i_3_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_encoded_reg[8]_i_3_O_UNCONNECTED\(3 downto 0),
      S(3) => \encoded[8]_i_5_n_0\,
      S(2) => \encoded[8]_i_6_n_0\,
      S(1 downto 0) => \sdp_bl.ramb18_dp_bl.ram18_bl_2\(1 downto 0)
    );
\encoded_reg[8]_i_4\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => ch13,
      CO(2) => \encoded_reg[8]_i_4_n_1\,
      CO(1) => \encoded_reg[8]_i_4_n_2\,
      CO(0) => \encoded_reg[8]_i_4_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_encoded_reg[8]_i_4_O_UNCONNECTED\(3 downto 0),
      S(3) => \encoded[8]_i_9_n_0\,
      S(2) => \encoded[8]_i_10_n_0\,
      S(1) => S(0),
      S(0) => \encoded[8]_i_12_n_0\
    );
\processCol[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \processCol[7]_i_2_n_0\,
      I1 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(0),
      O => \p_0_in__2\(0)
    );
\processCol[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"28"
    )
        port map (
      I0 => \processCol[7]_i_2_n_0\,
      I1 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(0),
      I2 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(1),
      O => \p_0_in__2\(1)
    );
\processCol[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2A80"
    )
        port map (
      I0 => \processCol[7]_i_2_n_0\,
      I1 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(1),
      I2 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(0),
      I3 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(2),
      O => \p_0_in__2\(2)
    );
\processCol[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00007F80"
    )
        port map (
      I0 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(0),
      I1 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(1),
      I2 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(2),
      I3 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(3),
      I4 => \processCol[3]_i_2_n_0\,
      O => \p_0_in__2\(3)
    );
\processCol[3]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FE000000"
    )
        port map (
      I0 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(6),
      I1 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(5),
      I2 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(7),
      I3 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(8),
      I4 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(9),
      O => \processCol[3]_i_2_n_0\
    );
\processCol[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2AAAAAAA80000000"
    )
        port map (
      I0 => \processCol[7]_i_2_n_0\,
      I1 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(2),
      I2 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(1),
      I3 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(0),
      I4 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(3),
      I5 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(4),
      O => \p_0_in__2\(4)
    );
\processCol[5]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0770"
    )
        port map (
      I0 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(8),
      I1 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(9),
      I2 => \processCol[9]_i_3_n_0\,
      I3 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(5),
      O => \p_0_in__2\(5)
    );
\processCol[6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"28"
    )
        port map (
      I0 => \processCol[7]_i_2_n_0\,
      I1 => \processCol[7]_i_3_n_0\,
      I2 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(6),
      O => \p_0_in__2\(6)
    );
\processCol[7]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2A80"
    )
        port map (
      I0 => \processCol[7]_i_2_n_0\,
      I1 => \processCol[7]_i_3_n_0\,
      I2 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(6),
      I3 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(7),
      O => \p_0_in__2\(7)
    );
\processCol[7]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00FFFFFF01FFFFFF"
    )
        port map (
      I0 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(7),
      I1 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(5),
      I2 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(6),
      I3 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(9),
      I4 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(8),
      I5 => \processCol[9]_i_3_n_0\,
      O => \processCol[7]_i_2_n_0\
    );
\processCol[7]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(3),
      I1 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(0),
      I2 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(1),
      I3 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(2),
      I4 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(4),
      I5 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(5),
      O => \processCol[7]_i_3_n_0\
    );
\processCol[8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"15555557C0000000"
    )
        port map (
      I0 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(9),
      I1 => \processCol[9]_i_3_n_0\,
      I2 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(6),
      I3 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(7),
      I4 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(5),
      I5 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(8),
      O => \p_0_in__2\(8)
    );
\processCol[9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7777777F"
    )
        port map (
      I0 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(9),
      I1 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(8),
      I2 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(7),
      I3 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(5),
      I4 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(6),
      O => \processCol[9]_i_1_n_0\
    );
\processCol[9]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0001FFFF80000000"
    )
        port map (
      I0 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(5),
      I1 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(7),
      I2 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(6),
      I3 => \processCol[9]_i_3_n_0\,
      I4 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(8),
      I5 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(9),
      O => \p_0_in__2\(9)
    );
\processCol[9]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80000000"
    )
        port map (
      I0 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(3),
      I1 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(0),
      I2 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(1),
      I3 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(2),
      I4 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(4),
      O => \processCol[9]_i_3_n_0\
    );
\processCol_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => \processCol[9]_i_1_n_0\,
      D => \p_0_in__2\(0),
      Q => \^sdp_bl.ramb18_dp_bl.ram18_bl\(0),
      R => s00_axi_aresetn_0
    );
\processCol_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => \processCol[9]_i_1_n_0\,
      D => \p_0_in__2\(1),
      Q => \^sdp_bl.ramb18_dp_bl.ram18_bl\(1),
      R => s00_axi_aresetn_0
    );
\processCol_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => \processCol[9]_i_1_n_0\,
      D => \p_0_in__2\(2),
      Q => \^sdp_bl.ramb18_dp_bl.ram18_bl\(2),
      R => s00_axi_aresetn_0
    );
\processCol_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => \processCol[9]_i_1_n_0\,
      D => \p_0_in__2\(3),
      Q => \^sdp_bl.ramb18_dp_bl.ram18_bl\(3),
      R => s00_axi_aresetn_0
    );
\processCol_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => \processCol[9]_i_1_n_0\,
      D => \p_0_in__2\(4),
      Q => \^sdp_bl.ramb18_dp_bl.ram18_bl\(4),
      R => s00_axi_aresetn_0
    );
\processCol_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => \processCol[9]_i_1_n_0\,
      D => \p_0_in__2\(5),
      Q => \^sdp_bl.ramb18_dp_bl.ram18_bl\(5),
      R => s00_axi_aresetn_0
    );
\processCol_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => \processCol[9]_i_1_n_0\,
      D => \p_0_in__2\(6),
      Q => \^sdp_bl.ramb18_dp_bl.ram18_bl\(6),
      R => s00_axi_aresetn_0
    );
\processCol_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => \processCol[9]_i_1_n_0\,
      D => \p_0_in__2\(7),
      Q => \^sdp_bl.ramb18_dp_bl.ram18_bl\(7),
      R => s00_axi_aresetn_0
    );
\processCol_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => \processCol[9]_i_1_n_0\,
      D => \p_0_in__2\(8),
      Q => \^sdp_bl.ramb18_dp_bl.ram18_bl\(8),
      R => s00_axi_aresetn_0
    );
\processCol_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => \processCol[9]_i_1_n_0\,
      D => \p_0_in__2\(9),
      Q => \^sdp_bl.ramb18_dp_bl.ram18_bl\(9),
      R => s00_axi_aresetn_0
    );
\processRow[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(0),
      O => plusOp(0)
    );
\processRow[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      O => plusOp(1)
    );
\processRow[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6A"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(1),
      I2 => \^q\(0),
      O => plusOp(2)
    );
\processRow[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6AAA"
    )
        port map (
      I0 => \^q\(3),
      I1 => \^q\(1),
      I2 => \^q\(2),
      I3 => \^q\(0),
      O => plusOp(3)
    );
\processRow[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6AAAAAAA"
    )
        port map (
      I0 => \^q\(4),
      I1 => \^q\(2),
      I2 => \^q\(3),
      I3 => \^q\(0),
      I4 => \^q\(1),
      O => plusOp(4)
    );
\processRow[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AAAAAAAAAAAAAAA"
    )
        port map (
      I0 => \^q\(5),
      I1 => \^q\(4),
      I2 => \^q\(1),
      I3 => \^q\(0),
      I4 => \^q\(3),
      I5 => \^q\(2),
      O => plusOp(5)
    );
\processRow[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AAAAAAAAAAAAAAA"
    )
        port map (
      I0 => row(6),
      I1 => \^q\(5),
      I2 => \processRow[6]_i_2_n_0\,
      I3 => \^q\(0),
      I4 => \^q\(1),
      I5 => \^q\(4),
      O => plusOp(6)
    );
\processRow[6]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(3),
      O => \processRow[6]_i_2_n_0\
    );
\processRow[7]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6AAA"
    )
        port map (
      I0 => row(7),
      I1 => row(6),
      I2 => \processRow[9]_i_7_n_0\,
      I3 => \^q\(5),
      O => plusOp(7)
    );
\processRow[8]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6AAAAAAA"
    )
        port map (
      I0 => row(8),
      I1 => row(7),
      I2 => \^q\(5),
      I3 => \processRow[9]_i_7_n_0\,
      I4 => row(6),
      O => plusOp(8)
    );
\processRow[9]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4F"
    )
        port map (
      I0 => \processRow[9]_i_4_n_0\,
      I1 => \processRow[9]_i_5_n_0\,
      I2 => s00_axi_aresetn,
      O => \processRow[9]_i_1_n_0\
    );
\processRow[9]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0888AAAA"
    )
        port map (
      I0 => \processRow[9]_i_5_n_0\,
      I1 => \processRow[9]_i_6_n_0\,
      I2 => \^q\(3),
      I3 => \^q\(2),
      I4 => row(9),
      O => processRow0
    );
\processRow[9]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AAAAAAAAAAAAAAA"
    )
        port map (
      I0 => row(9),
      I1 => row(8),
      I2 => row(7),
      I3 => row(6),
      I4 => \^q\(5),
      I5 => \processRow[9]_i_7_n_0\,
      O => plusOp(9)
    );
\processRow[9]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFDFFFFFFFFFFFFF"
    )
        port map (
      I0 => \processRow[9]_i_6_n_0\,
      I1 => \^q\(1),
      I2 => row(9),
      I3 => \^q\(0),
      I4 => \^q\(2),
      I5 => \^q\(3),
      O => \processRow[9]_i_4_n_0\
    );
\processRow[9]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0100000000000000"
    )
        port map (
      I0 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(6),
      I1 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(5),
      I2 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(7),
      I3 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(9),
      I4 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(8),
      I5 => \processCol[9]_i_3_n_0\,
      O => \processRow[9]_i_5_n_0\
    );
\processRow[9]_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000001"
    )
        port map (
      I0 => row(7),
      I1 => \^q\(5),
      I2 => row(6),
      I3 => row(8),
      I4 => \^q\(4),
      O => \processRow[9]_i_6_n_0\
    );
\processRow[9]_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80000000"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(3),
      I2 => \^q\(0),
      I3 => \^q\(1),
      I4 => \^q\(4),
      O => \processRow[9]_i_7_n_0\
    );
\processRow_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => processRow0,
      D => plusOp(0),
      Q => \^q\(0),
      R => \processRow[9]_i_1_n_0\
    );
\processRow_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => processRow0,
      D => plusOp(1),
      Q => \^q\(1),
      R => \processRow[9]_i_1_n_0\
    );
\processRow_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => processRow0,
      D => plusOp(2),
      Q => \^q\(2),
      R => \processRow[9]_i_1_n_0\
    );
\processRow_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => processRow0,
      D => plusOp(3),
      Q => \^q\(3),
      R => \processRow[9]_i_1_n_0\
    );
\processRow_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => processRow0,
      D => plusOp(4),
      Q => \^q\(4),
      R => \processRow[9]_i_1_n_0\
    );
\processRow_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => processRow0,
      D => plusOp(5),
      Q => \^q\(5),
      R => \processRow[9]_i_1_n_0\
    );
\processRow_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => processRow0,
      D => plusOp(6),
      Q => row(6),
      R => \processRow[9]_i_1_n_0\
    );
\processRow_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => processRow0,
      D => plusOp(7),
      Q => row(7),
      R => \processRow[9]_i_1_n_0\
    );
\processRow_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => processRow0,
      D => plusOp(8),
      Q => row(8),
      R => \processRow[9]_i_1_n_0\
    );
\processRow_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => processRow0,
      D => plusOp(9),
      Q => row(9),
      R => \processRow[9]_i_1_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Audio_Codec_Wrapper is
  port (
    ac_mclk : out STD_LOGIC;
    ac_bclk : out STD_LOGIC;
    BCLK_int_reg : out STD_LOGIC;
    ac_lrclk : out STD_LOGIC;
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    \UnL_bus_out2_reg[10]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    w_sw : out STD_LOGIC_VECTOR ( 0 to 0 );
    CO : out STD_LOGIC_VECTOR ( 0 to 0 );
    DIBDI : out STD_LOGIC_VECTOR ( 15 downto 0 );
    \L_bus_in_reg[17]\ : out STD_LOGIC_VECTOR ( 17 downto 0 );
    D : out STD_LOGIC_VECTOR ( 0 to 0 );
    \sdp_bl.ramb18_dp_bl.ram18_bl\ : out STD_LOGIC_VECTOR ( 15 downto 0 );
    \R_bus_in_reg[17]\ : out STD_LOGIC_VECTOR ( 17 downto 0 );
    ac_dac_sdata : out STD_LOGIC;
    scl : inout STD_LOGIC;
    sda : inout STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC;
    ac_adc_sdata : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 17 downto 0 );
    \R_bus_in_reg[17]_0\ : in STD_LOGIC_VECTOR ( 17 downto 0 );
    \trigger_volt_reg[9]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \slv_reg9_reg[15]\ : in STD_LOGIC_VECTOR ( 15 downto 0 );
    switch : in STD_LOGIC_VECTOR ( 0 to 0 );
    \trigger_volt_reg[8]\ : in STD_LOGIC_VECTOR ( 7 downto 0 );
    \slv_reg10_reg[15]\ : in STD_LOGIC_VECTOR ( 15 downto 0 );
    S : in STD_LOGIC_VECTOR ( 0 to 0 );
    lopt : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Audio_Codec_Wrapper;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Audio_Codec_Wrapper is
  signal \^bclk_int_reg\ : STD_LOGIC;
  signal \^e\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \^ac_lrclk\ : STD_LOGIC;
  signal ac_lrclk_count : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \ac_lrclk_count[0]_i_1_n_0\ : STD_LOGIC;
  signal \ac_lrclk_count[1]_i_1_n_0\ : STD_LOGIC;
  signal \ac_lrclk_count[2]_i_1_n_0\ : STD_LOGIC;
  signal ac_lrclk_sig_prev_reg_n_0 : STD_LOGIC;
  signal audio_inout_n_3 : STD_LOGIC;
  signal audio_inout_n_4 : STD_LOGIC;
  signal clk_50 : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \ac_lrclk_count[0]_i_1\ : label is "soft_lutpair77";
  attribute SOFT_HLUTNM of \ac_lrclk_count[1]_i_1\ : label is "soft_lutpair77";
begin
  BCLK_int_reg <= \^bclk_int_reg\;
  E(0) <= \^e\(0);
  ac_lrclk <= \^ac_lrclk\;
\UnL_bus_out2[17]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => s00_axi_aresetn,
      I1 => \^e\(0),
      O => \UnL_bus_out2_reg[10]\(0)
    );
\ac_lrclk_count[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8A20"
    )
        port map (
      I0 => s00_axi_aresetn,
      I1 => ac_lrclk_sig_prev_reg_n_0,
      I2 => \^ac_lrclk\,
      I3 => ac_lrclk_count(0),
      O => \ac_lrclk_count[0]_i_1_n_0\
    );
\ac_lrclk_count[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"8A20AA00"
    )
        port map (
      I0 => s00_axi_aresetn,
      I1 => ac_lrclk_sig_prev_reg_n_0,
      I2 => \^ac_lrclk\,
      I3 => ac_lrclk_count(1),
      I4 => ac_lrclk_count(0),
      O => \ac_lrclk_count[1]_i_1_n_0\
    );
\ac_lrclk_count[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8AAAAAAA20000000"
    )
        port map (
      I0 => s00_axi_aresetn,
      I1 => ac_lrclk_sig_prev_reg_n_0,
      I2 => \^ac_lrclk\,
      I3 => ac_lrclk_count(1),
      I4 => ac_lrclk_count(0),
      I5 => ac_lrclk_count(2),
      O => \ac_lrclk_count[2]_i_1_n_0\
    );
\ac_lrclk_count_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ac_lrclk_count[0]_i_1_n_0\,
      Q => ac_lrclk_count(0),
      R => '0'
    );
\ac_lrclk_count_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ac_lrclk_count[1]_i_1_n_0\,
      Q => ac_lrclk_count(1),
      R => '0'
    );
\ac_lrclk_count_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ac_lrclk_count[2]_i_1_n_0\,
      Q => ac_lrclk_count(2),
      R => '0'
    );
ac_lrclk_sig_prev_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => audio_inout_n_3,
      Q => ac_lrclk_sig_prev_reg_n_0,
      R => '0'
    );
audio_inout: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_i2s_ctl
     port map (
      CO(0) => CO(0),
      D(0) => D(0),
      DIBDI(15 downto 0) => DIBDI(15 downto 0),
      E(0) => \^e\(0),
      \L_bus_in_reg[17]\(17 downto 0) => \L_bus_in_reg[17]\(17 downto 0),
      Q(17 downto 0) => Q(17 downto 0),
      \R_bus_in_reg[17]\(17 downto 0) => \R_bus_in_reg[17]\(17 downto 0),
      \R_bus_in_reg[17]_0\(17 downto 0) => \R_bus_in_reg[17]_0\(17 downto 0),
      S(0) => S(0),
      SR(0) => \^bclk_int_reg\,
      ac_adc_sdata => ac_adc_sdata,
      ac_bclk => ac_bclk,
      ac_dac_sdata => ac_dac_sdata,
      ac_lrclk => \^ac_lrclk\,
      ac_lrclk_count(2 downto 0) => ac_lrclk_count(2 downto 0),
      ac_lrclk_sig_prev_reg => audio_inout_n_3,
      ac_lrclk_sig_prev_reg_0 => ac_lrclk_sig_prev_reg_n_0,
      ready_sig_reg => audio_inout_n_4,
      s00_axi_aclk => s00_axi_aclk,
      s00_axi_aresetn => s00_axi_aresetn,
      \sdp_bl.ramb18_dp_bl.ram18_bl\(15 downto 0) => \sdp_bl.ramb18_dp_bl.ram18_bl\(15 downto 0),
      \slv_reg10_reg[15]\(15 downto 0) => \slv_reg10_reg[15]\(15 downto 0),
      \slv_reg9_reg[15]\(15 downto 0) => \slv_reg9_reg[15]\(15 downto 0),
      switch(0) => switch(0),
      \trigger_volt_reg[8]\(7 downto 0) => \trigger_volt_reg[8]\(7 downto 0),
      \trigger_volt_reg[9]\(0) => \trigger_volt_reg[9]\(0),
      w_sw(0) => w_sw(0)
    );
audiocodec_master_clock: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_1
     port map (
      clk_in1 => s00_axi_aclk,
      clk_out1 => ac_mclk,
      clk_out2 => clk_50,
      lopt => lopt,
      resetn => s00_axi_aresetn
    );
initialize_audio: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_audio_init
     port map (
      SR(0) => \^bclk_int_reg\,
      clk_out2 => clk_50,
      s00_axi_aresetn => s00_axi_aresetn,
      scl => scl,
      sda => sda
    );
ready_sig_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => audio_inout_n_4,
      Q => \^e\(0),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_video is
  port (
    tmds : out STD_LOGIC_VECTOR ( 3 downto 0 );
    tmdsb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    Q : out STD_LOGIC_VECTOR ( 5 downto 0 );
    \sdp_bl.ramb18_dp_bl.ram18_bl\ : out STD_LOGIC_VECTOR ( 9 downto 0 );
    s00_axi_aresetn : in STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC;
    \trigger_volt_reg[9]\ : in STD_LOGIC_VECTOR ( 9 downto 0 );
    \trigger_time_reg[4]\ : in STD_LOGIC;
    \trigger_time_reg[8]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \trigger_time_reg[3]\ : in STD_LOGIC;
    \trigger_time_reg[7]\ : in STD_LOGIC;
    \trigger_time_reg[6]\ : in STD_LOGIC;
    \trigger_time_reg[2]\ : in STD_LOGIC;
    \trigger_time_reg[9]\ : in STD_LOGIC;
    DOADO : in STD_LOGIC_VECTOR ( 4 downto 0 );
    \sdp_bl.ramb18_dp_bl.ram18_bl_0\ : in STD_LOGIC;
    \sdp_bl.ramb18_dp_bl.ram18_bl_1\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    switch : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S : in STD_LOGIC_VECTOR ( 0 to 0 );
    \sdp_bl.ramb18_dp_bl.ram18_bl_2\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_aresetn_0 : in STD_LOGIC;
    lopt : out STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_video;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_video is
  signal Inst_vga_n_16 : STD_LOGIC;
  signal Inst_vga_n_17 : STD_LOGIC;
  signal Inst_vga_n_18 : STD_LOGIC;
  signal Inst_vga_n_19 : STD_LOGIC;
  signal Inst_vga_n_20 : STD_LOGIC;
  signal Inst_vga_n_21 : STD_LOGIC;
  signal Inst_vga_n_22 : STD_LOGIC;
  signal Inst_vga_n_23 : STD_LOGIC;
  signal Inst_vga_n_25 : STD_LOGIC;
  signal Inst_vga_n_26 : STD_LOGIC;
  signal Inst_vga_n_28 : STD_LOGIC;
  signal Inst_vga_n_29 : STD_LOGIC;
  signal Inst_vga_n_30 : STD_LOGIC;
  signal \TDMS_encoder_blue/p_1_in\ : STD_LOGIC;
  signal \TDMS_encoder_green/p_1_in\ : STD_LOGIC;
  signal \TDMS_encoder_red/p_1_in\ : STD_LOGIC;
  signal blank : STD_LOGIC;
  signal blue_s : STD_LOGIC;
  signal clock_s : STD_LOGIC;
  signal encoded0_in : STD_LOGIC_VECTOR ( 8 to 8 );
  signal green_s : STD_LOGIC;
  signal inst_dvid_n_10 : STD_LOGIC;
  signal inst_dvid_n_11 : STD_LOGIC;
  signal inst_dvid_n_12 : STD_LOGIC;
  signal inst_dvid_n_13 : STD_LOGIC;
  signal inst_dvid_n_14 : STD_LOGIC;
  signal inst_dvid_n_15 : STD_LOGIC;
  signal inst_dvid_n_17 : STD_LOGIC;
  signal inst_dvid_n_18 : STD_LOGIC;
  signal inst_dvid_n_19 : STD_LOGIC;
  signal inst_dvid_n_20 : STD_LOGIC;
  signal inst_dvid_n_21 : STD_LOGIC;
  signal inst_dvid_n_22 : STD_LOGIC;
  signal inst_dvid_n_23 : STD_LOGIC;
  signal inst_dvid_n_24 : STD_LOGIC;
  signal inst_dvid_n_25 : STD_LOGIC;
  signal inst_dvid_n_26 : STD_LOGIC;
  signal inst_dvid_n_27 : STD_LOGIC;
  signal inst_dvid_n_4 : STD_LOGIC;
  signal inst_dvid_n_7 : STD_LOGIC;
  signal inst_dvid_n_8 : STD_LOGIC;
  signal inst_dvid_n_9 : STD_LOGIC;
  signal pixel_clk : STD_LOGIC;
  signal red_s : STD_LOGIC;
  signal serialize_clk : STD_LOGIC;
  signal serialize_clk_n : STD_LOGIC;
  attribute CAPACITANCE : string;
  attribute CAPACITANCE of OBUFDS_blue : label is "DONT_CARE";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of OBUFDS_blue : label is "OBUFDS";
  attribute box_type : string;
  attribute box_type of OBUFDS_blue : label is "PRIMITIVE";
  attribute CAPACITANCE of OBUFDS_clock : label is "DONT_CARE";
  attribute XILINX_LEGACY_PRIM of OBUFDS_clock : label is "OBUFDS";
  attribute box_type of OBUFDS_clock : label is "PRIMITIVE";
  attribute CAPACITANCE of OBUFDS_green : label is "DONT_CARE";
  attribute XILINX_LEGACY_PRIM of OBUFDS_green : label is "OBUFDS";
  attribute box_type of OBUFDS_green : label is "PRIMITIVE";
  attribute CAPACITANCE of OBUFDS_red : label is "DONT_CARE";
  attribute XILINX_LEGACY_PRIM of OBUFDS_red : label is "OBUFDS";
  attribute box_type of OBUFDS_red : label is "PRIMITIVE";
begin
Inst_vga: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_vga
     port map (
      CLK => pixel_clk,
      DOADO(4 downto 0) => DOADO(4 downto 0),
      Q(5 downto 0) => Q(5 downto 0),
      S(0) => S(0),
      SR(0) => blank,
      \dc_bias_reg[1]\ => inst_dvid_n_27,
      \dc_bias_reg[1]_0\ => inst_dvid_n_18,
      \dc_bias_reg[1]_1\ => inst_dvid_n_19,
      \dc_bias_reg[3]\(1) => \TDMS_encoder_blue/p_1_in\,
      \dc_bias_reg[3]\(0) => inst_dvid_n_17,
      \dc_bias_reg[3]_0\(0) => \TDMS_encoder_red/p_1_in\,
      \dc_bias_reg[3]_1\ => inst_dvid_n_15,
      \dc_bias_reg[3]_2\(0) => \TDMS_encoder_green/p_1_in\,
      encoded0_in(0) => encoded0_in(8),
      \encoded_reg[0]\ => Inst_vga_n_21,
      \encoded_reg[0]_0\ => Inst_vga_n_26,
      \encoded_reg[2]\ => Inst_vga_n_25,
      \encoded_reg[8]\ => Inst_vga_n_16,
      \encoded_reg[8]_0\ => Inst_vga_n_18,
      \encoded_reg[8]_1\ => Inst_vga_n_19,
      \encoded_reg[8]_2\ => Inst_vga_n_20,
      \encoded_reg[8]_3\ => Inst_vga_n_22,
      \encoded_reg[9]\ => Inst_vga_n_17,
      \encoded_reg[9]_0\ => Inst_vga_n_23,
      \encoded_reg[9]_1\ => Inst_vga_n_28,
      \encoded_reg[9]_2\ => Inst_vga_n_29,
      \encoded_reg[9]_3\ => Inst_vga_n_30,
      s00_axi_aresetn => s00_axi_aresetn,
      s00_axi_aresetn_0 => s00_axi_aresetn_0,
      \sdp_bl.ramb18_dp_bl.ram18_bl\(9 downto 0) => \sdp_bl.ramb18_dp_bl.ram18_bl\(9 downto 0),
      \sdp_bl.ramb18_dp_bl.ram18_bl_0\ => \sdp_bl.ramb18_dp_bl.ram18_bl_0\,
      \sdp_bl.ramb18_dp_bl.ram18_bl_1\(1 downto 0) => \sdp_bl.ramb18_dp_bl.ram18_bl_1\(1 downto 0),
      \sdp_bl.ramb18_dp_bl.ram18_bl_2\(1 downto 0) => \sdp_bl.ramb18_dp_bl.ram18_bl_2\(1 downto 0),
      switch(1 downto 0) => switch(1 downto 0),
      \trigger_time_reg[1]\ => inst_dvid_n_22,
      \trigger_time_reg[1]_0\ => inst_dvid_n_20,
      \trigger_time_reg[2]\ => \trigger_time_reg[2]\,
      \trigger_time_reg[3]\ => \trigger_time_reg[3]\,
      \trigger_time_reg[4]\ => \trigger_time_reg[4]\,
      \trigger_time_reg[4]_0\ => inst_dvid_n_8,
      \trigger_time_reg[5]\ => inst_dvid_n_12,
      \trigger_time_reg[5]_0\ => inst_dvid_n_10,
      \trigger_time_reg[5]_1\ => inst_dvid_n_21,
      \trigger_time_reg[6]\ => \trigger_time_reg[6]\,
      \trigger_time_reg[6]_0\ => inst_dvid_n_11,
      \trigger_time_reg[6]_1\ => inst_dvid_n_9,
      \trigger_time_reg[7]\ => \trigger_time_reg[7]\,
      \trigger_time_reg[7]_0\ => inst_dvid_n_7,
      \trigger_time_reg[8]\(3 downto 0) => \trigger_time_reg[8]\(3 downto 0),
      \trigger_time_reg[9]\ => \trigger_time_reg[9]\,
      \trigger_volt_reg[1]\ => inst_dvid_n_26,
      \trigger_volt_reg[1]_0\ => inst_dvid_n_23,
      \trigger_volt_reg[2]\ => inst_dvid_n_4,
      \trigger_volt_reg[4]\ => inst_dvid_n_25,
      \trigger_volt_reg[4]_0\ => inst_dvid_n_14,
      \trigger_volt_reg[5]\ => inst_dvid_n_13,
      \trigger_volt_reg[5]_0\ => inst_dvid_n_24,
      \trigger_volt_reg[9]\(9 downto 0) => \trigger_volt_reg[9]\(9 downto 0)
    );
OBUFDS_blue: unisim.vcomponents.OBUFDS
    generic map(
      IOSTANDARD => "DEFAULT"
    )
        port map (
      I => blue_s,
      O => tmds(0),
      OB => tmdsb(0)
    );
OBUFDS_clock: unisim.vcomponents.OBUFDS
    generic map(
      IOSTANDARD => "DEFAULT"
    )
        port map (
      I => clock_s,
      O => tmds(3),
      OB => tmdsb(3)
    );
OBUFDS_green: unisim.vcomponents.OBUFDS
    generic map(
      IOSTANDARD => "DEFAULT"
    )
        port map (
      I => red_s,
      O => tmds(2),
      OB => tmdsb(2)
    );
OBUFDS_red: unisim.vcomponents.OBUFDS
    generic map(
      IOSTANDARD => "DEFAULT"
    )
        port map (
      I => green_s,
      O => tmds(1),
      OB => tmdsb(1)
    );
inst_dvid: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_dvid
     port map (
      CLK => pixel_clk,
      Q(0) => \TDMS_encoder_green/p_1_in\,
      SR(0) => blank,
      blue_s => blue_s,
      clk_out2 => serialize_clk,
      clk_out3 => serialize_clk_n,
      clock_s => clock_s,
      \dc_bias_reg[0]\(0) => \TDMS_encoder_red/p_1_in\,
      \dc_bias_reg[0]_0\(1) => \TDMS_encoder_blue/p_1_in\,
      \dc_bias_reg[0]_0\(0) => inst_dvid_n_17,
      \dc_bias_reg[1]\ => Inst_vga_n_22,
      \dc_bias_reg[2]\ => Inst_vga_n_16,
      \dc_bias_reg[3]\ => Inst_vga_n_21,
      \dc_bias_reg[3]_0\ => Inst_vga_n_30,
      \dc_bias_reg[3]_1\ => Inst_vga_n_29,
      \dc_bias_reg[3]_2\ => Inst_vga_n_25,
      \dc_bias_reg[3]_3\ => Inst_vga_n_26,
      \dc_bias_reg[3]_4\ => Inst_vga_n_28,
      encoded0_in(0) => encoded0_in(8),
      \encoded_reg[8]\ => inst_dvid_n_4,
      \encoded_reg[8]_0\ => inst_dvid_n_7,
      \encoded_reg[8]_1\ => inst_dvid_n_8,
      \encoded_reg[8]_10\ => inst_dvid_n_20,
      \encoded_reg[8]_11\ => inst_dvid_n_21,
      \encoded_reg[8]_12\ => inst_dvid_n_22,
      \encoded_reg[8]_13\ => inst_dvid_n_23,
      \encoded_reg[8]_14\ => inst_dvid_n_24,
      \encoded_reg[8]_15\ => inst_dvid_n_25,
      \encoded_reg[8]_16\ => inst_dvid_n_26,
      \encoded_reg[8]_17\ => inst_dvid_n_27,
      \encoded_reg[8]_2\ => inst_dvid_n_9,
      \encoded_reg[8]_3\ => inst_dvid_n_10,
      \encoded_reg[8]_4\ => inst_dvid_n_11,
      \encoded_reg[8]_5\ => inst_dvid_n_12,
      \encoded_reg[8]_6\ => inst_dvid_n_13,
      \encoded_reg[8]_7\ => inst_dvid_n_14,
      \encoded_reg[8]_8\ => inst_dvid_n_18,
      \encoded_reg[8]_9\ => inst_dvid_n_19,
      \encoded_reg[9]\ => inst_dvid_n_15,
      green_s => green_s,
      \processCol_reg[0]\ => Inst_vga_n_20,
      \processCol_reg[1]\ => Inst_vga_n_23,
      \processCol_reg[6]\ => Inst_vga_n_19,
      \processCol_reg[6]_0\ => Inst_vga_n_17,
      \processRow_reg[2]\ => Inst_vga_n_18,
      red_s => red_s,
      \trigger_time_reg[2]\ => \trigger_time_reg[2]\,
      \trigger_time_reg[3]\ => \trigger_time_reg[3]\,
      \trigger_time_reg[4]\ => \trigger_time_reg[4]\,
      \trigger_time_reg[5]\(2 downto 0) => \trigger_time_reg[8]\(2 downto 0),
      \trigger_time_reg[6]\ => \trigger_time_reg[6]\,
      \trigger_time_reg[7]\ => \trigger_time_reg[7]\,
      \trigger_volt_reg[6]\(6 downto 0) => \trigger_volt_reg[9]\(6 downto 0)
    );
mmcm_adv_inst_display_clocks: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_0
     port map (
      clk_in1 => s00_axi_aclk,
      clk_out1 => pixel_clk,
      clk_out2 => serialize_clk,
      clk_out3 => serialize_clk_n,
      lopt => lopt,
      resetn => s00_axi_aresetn
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_lab2_datapath is
  port (
    SS : out STD_LOGIC_VECTOR ( 0 to 0 );
    tmds : out STD_LOGIC_VECTOR ( 3 downto 0 );
    tmdsb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    ac_mclk : out STD_LOGIC;
    ac_bclk : out STD_LOGIC;
    ac_lrclk : out STD_LOGIC;
    w_sw : out STD_LOGIC_VECTOR ( 2 downto 0 );
    Q : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \i_sig_reg[6]_0\ : out STD_LOGIC;
    CO : out STD_LOGIC_VECTOR ( 0 to 0 );
    \FSM_onehot_state_reg[1]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    ac_dac_sdata : out STD_LOGIC;
    scl : inout STD_LOGIC;
    sda : inout STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC;
    ac_adc_sdata : in STD_LOGIC;
    WREN : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC;
    btn : in STD_LOGIC_VECTOR ( 4 downto 0 );
    \slv_reg9_reg[15]\ : in STD_LOGIC_VECTOR ( 15 downto 0 );
    switch : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \slv_reg8_reg[9]\ : in STD_LOGIC_VECTOR ( 9 downto 0 );
    \slv_reg10_reg[15]\ : in STD_LOGIC_VECTOR ( 15 downto 0 );
    SR : in STD_LOGIC_VECTOR ( 0 to 0 );
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    \FSM_onehot_state_reg[4]\ : in STD_LOGIC_VECTOR ( 1 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_lab2_datapath;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_lab2_datapath is
  signal AddrRead : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal DI : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal D_L_O : STD_LOGIC_VECTOR ( 17 downto 0 );
  signal D_R_O : STD_LOGIC_VECTOR ( 17 downto 0 );
  signal \FSM_onehot_state[1]_i_16_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[1]_i_17_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[1]_i_18_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[1]_i_19_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[1]_i_20_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[1]_i_21_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[1]_i_22_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[1]_i_23_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[1]_i_5_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[1]_i_7_n_0\ : STD_LOGIC;
  signal \^fsm_onehot_state_reg[1]\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \FSM_onehot_state_reg[1]_i_6_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state_reg[1]_i_6_n_1\ : STD_LOGIC;
  signal \FSM_onehot_state_reg[1]_i_6_n_2\ : STD_LOGIC;
  signal \FSM_onehot_state_reg[1]_i_6_n_3\ : STD_LOGIC;
  signal L_bus_in : STD_LOGIC_VECTOR ( 17 downto 0 );
  signal \^q\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal R_bus_in : STD_LOGIC_VECTOR ( 17 downto 0 );
  signal \^ss\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal UnL_bus_out : STD_LOGIC_VECTOR ( 17 to 17 );
  signal UnL_bus_out2 : STD_LOGIC;
  signal WRADDR : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal \i_sig[0]_i_1_n_0\ : STD_LOGIC;
  signal \i_sig[1]_i_1_n_0\ : STD_LOGIC;
  signal \i_sig[2]_i_1_n_0\ : STD_LOGIC;
  signal \i_sig[3]_i_1_n_0\ : STD_LOGIC;
  signal \i_sig[4]_i_1_n_0\ : STD_LOGIC;
  signal \i_sig[4]_i_2_n_0\ : STD_LOGIC;
  signal \i_sig[5]_i_1_n_0\ : STD_LOGIC;
  signal \i_sig[6]_i_1_n_0\ : STD_LOGIC;
  signal \i_sig[7]_i_1_n_0\ : STD_LOGIC;
  signal \i_sig[8]_i_1_n_0\ : STD_LOGIC;
  signal \i_sig[9]_i_3_n_0\ : STD_LOGIC;
  signal \^i_sig_reg[6]_0\ : STD_LOGIC;
  signal \i_sig_reg_n_0_[0]\ : STD_LOGIC;
  signal \i_sig_reg_n_0_[1]\ : STD_LOGIC;
  signal \i_sig_reg_n_0_[2]\ : STD_LOGIC;
  signal \i_sig_reg_n_0_[3]\ : STD_LOGIC;
  signal \i_sig_reg_n_0_[4]\ : STD_LOGIC;
  signal \i_sig_reg_n_0_[5]\ : STD_LOGIC;
  signal leftChannelMemory_n_5 : STD_LOGIC;
  signal leftChannelMemory_n_6 : STD_LOGIC;
  signal lopt : STD_LOGIC;
  signal old_button : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal p_0_in : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal \p_0_in__1\ : STD_LOGIC_VECTOR ( 9 downto 1 );
  signal p_1_in : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal readL : STD_LOGIC_VECTOR ( 15 downto 8 );
  signal readR : STD_LOGIC_VECTOR ( 15 downto 14 );
  signal rightChannelMemory_n_12 : STD_LOGIC;
  signal rightChannelMemory_n_13 : STD_LOGIC;
  signal row : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal trigger_time0_in : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal \trigger_time[2]_i_1_n_0\ : STD_LOGIC;
  signal \trigger_time[3]_i_1_n_0\ : STD_LOGIC;
  signal \trigger_time[4]_i_1_n_0\ : STD_LOGIC;
  signal \trigger_time[4]_i_2_n_0\ : STD_LOGIC;
  signal \trigger_time[4]_i_3_n_0\ : STD_LOGIC;
  signal \trigger_time[5]_i_2_n_0\ : STD_LOGIC;
  signal \trigger_time[5]_i_3_n_0\ : STD_LOGIC;
  signal \trigger_time[6]_i_1_n_0\ : STD_LOGIC;
  signal \trigger_time[7]_i_1_n_0\ : STD_LOGIC;
  signal \trigger_time[7]_i_2_n_0\ : STD_LOGIC;
  signal \trigger_time[7]_i_3_n_0\ : STD_LOGIC;
  signal \trigger_time[8]_i_1_n_0\ : STD_LOGIC;
  signal \trigger_time[8]_i_3_n_0\ : STD_LOGIC;
  signal \trigger_time[8]_i_4_n_0\ : STD_LOGIC;
  signal \trigger_time[8]_i_5_n_0\ : STD_LOGIC;
  signal \trigger_time[8]_i_6_n_0\ : STD_LOGIC;
  signal \trigger_time[8]_i_7_n_0\ : STD_LOGIC;
  signal \trigger_time[8]_i_8_n_0\ : STD_LOGIC;
  signal \trigger_time[8]_i_9_n_0\ : STD_LOGIC;
  signal \trigger_time[9]_i_1_n_0\ : STD_LOGIC;
  signal \trigger_time[9]_i_2_n_0\ : STD_LOGIC;
  signal \trigger_time_reg_n_0_[0]\ : STD_LOGIC;
  signal \trigger_time_reg_n_0_[1]\ : STD_LOGIC;
  signal \trigger_time_reg_n_0_[2]\ : STD_LOGIC;
  signal \trigger_time_reg_n_0_[3]\ : STD_LOGIC;
  signal \trigger_time_reg_n_0_[4]\ : STD_LOGIC;
  signal \trigger_time_reg_n_0_[5]\ : STD_LOGIC;
  signal \trigger_time_reg_n_0_[6]\ : STD_LOGIC;
  signal \trigger_time_reg_n_0_[7]\ : STD_LOGIC;
  signal \trigger_time_reg_n_0_[8]\ : STD_LOGIC;
  signal \trigger_time_reg_n_0_[9]\ : STD_LOGIC;
  signal \trigger_volt[0]_i_1_n_0\ : STD_LOGIC;
  signal \trigger_volt[3]_i_2_n_0\ : STD_LOGIC;
  signal \trigger_volt[4]_i_2_n_0\ : STD_LOGIC;
  signal \trigger_volt[4]_i_3_n_0\ : STD_LOGIC;
  signal \trigger_volt[5]_i_2_n_0\ : STD_LOGIC;
  signal \trigger_volt[5]_i_3_n_0\ : STD_LOGIC;
  signal \trigger_volt[7]_i_2_n_0\ : STD_LOGIC;
  signal \trigger_volt[7]_i_3_n_0\ : STD_LOGIC;
  signal \trigger_volt[8]_i_2_n_0\ : STD_LOGIC;
  signal \trigger_volt[8]_i_3_n_0\ : STD_LOGIC;
  signal \trigger_volt[8]_i_4_n_0\ : STD_LOGIC;
  signal \trigger_volt[8]_i_5_n_0\ : STD_LOGIC;
  signal \trigger_volt[9]_i_1_n_0\ : STD_LOGIC;
  signal \trigger_volt[9]_i_3_n_0\ : STD_LOGIC;
  signal \trigger_volt[9]_i_4_n_0\ : STD_LOGIC;
  signal \trigger_volt[9]_i_5_n_0\ : STD_LOGIC;
  signal \trigger_volt_reg_n_0_[0]\ : STD_LOGIC;
  signal \^w_sw\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal wrapper_inst_n_43 : STD_LOGIC;
  signal wrapper_inst_n_44 : STD_LOGIC;
  signal wrapper_inst_n_45 : STD_LOGIC;
  signal wrapper_inst_n_46 : STD_LOGIC;
  signal wrapper_inst_n_47 : STD_LOGIC;
  signal wrapper_inst_n_48 : STD_LOGIC;
  signal wrapper_inst_n_49 : STD_LOGIC;
  signal wrapper_inst_n_50 : STD_LOGIC;
  signal wrapper_inst_n_51 : STD_LOGIC;
  signal wrapper_inst_n_52 : STD_LOGIC;
  signal wrapper_inst_n_53 : STD_LOGIC;
  signal wrapper_inst_n_54 : STD_LOGIC;
  signal wrapper_inst_n_55 : STD_LOGIC;
  signal wrapper_inst_n_56 : STD_LOGIC;
  signal wrapper_inst_n_57 : STD_LOGIC;
  signal wrapper_inst_n_58 : STD_LOGIC;
  signal \NLW_FSM_onehot_state_reg[1]_i_3_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_FSM_onehot_state_reg[1]_i_3_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_FSM_onehot_state_reg[1]_i_6_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \FSM_onehot_state[2]_i_3\ : label is "soft_lutpair82";
  attribute SOFT_HLUTNM of \i_sig[0]_i_1\ : label is "soft_lutpair92";
  attribute SOFT_HLUTNM of \i_sig[1]_i_1\ : label is "soft_lutpair92";
  attribute SOFT_HLUTNM of \i_sig[3]_i_1\ : label is "soft_lutpair83";
  attribute SOFT_HLUTNM of \i_sig[4]_i_2\ : label is "soft_lutpair83";
  attribute SOFT_HLUTNM of \i_sig[7]_i_1\ : label is "soft_lutpair86";
  attribute SOFT_HLUTNM of \i_sig[8]_i_1\ : label is "soft_lutpair86";
  attribute SOFT_HLUTNM of \i_sig[9]_i_3\ : label is "soft_lutpair82";
  attribute SOFT_HLUTNM of \trigger_time[4]_i_2\ : label is "soft_lutpair87";
  attribute SOFT_HLUTNM of \trigger_time[4]_i_3\ : label is "soft_lutpair87";
  attribute SOFT_HLUTNM of \trigger_time[5]_i_2\ : label is "soft_lutpair80";
  attribute SOFT_HLUTNM of \trigger_time[5]_i_3\ : label is "soft_lutpair80";
  attribute SOFT_HLUTNM of \trigger_time[8]_i_4\ : label is "soft_lutpair91";
  attribute SOFT_HLUTNM of \trigger_time[8]_i_5\ : label is "soft_lutpair91";
  attribute SOFT_HLUTNM of \trigger_time[8]_i_6\ : label is "soft_lutpair88";
  attribute SOFT_HLUTNM of \trigger_time[8]_i_7\ : label is "soft_lutpair88";
  attribute SOFT_HLUTNM of \trigger_time[8]_i_8\ : label is "soft_lutpair84";
  attribute SOFT_HLUTNM of \trigger_volt[0]_i_1\ : label is "soft_lutpair79";
  attribute SOFT_HLUTNM of \trigger_volt[1]_i_1\ : label is "soft_lutpair78";
  attribute SOFT_HLUTNM of \trigger_volt[2]_i_1\ : label is "soft_lutpair78";
  attribute SOFT_HLUTNM of \trigger_volt[3]_i_2\ : label is "soft_lutpair84";
  attribute SOFT_HLUTNM of \trigger_volt[4]_i_2\ : label is "soft_lutpair85";
  attribute SOFT_HLUTNM of \trigger_volt[4]_i_3\ : label is "soft_lutpair85";
  attribute SOFT_HLUTNM of \trigger_volt[5]_i_2\ : label is "soft_lutpair81";
  attribute SOFT_HLUTNM of \trigger_volt[5]_i_3\ : label is "soft_lutpair81";
  attribute SOFT_HLUTNM of \trigger_volt[8]_i_4\ : label is "soft_lutpair90";
  attribute SOFT_HLUTNM of \trigger_volt[8]_i_5\ : label is "soft_lutpair90";
  attribute SOFT_HLUTNM of \trigger_volt[9]_i_3\ : label is "soft_lutpair89";
  attribute SOFT_HLUTNM of \trigger_volt[9]_i_4\ : label is "soft_lutpair79";
  attribute SOFT_HLUTNM of \trigger_volt[9]_i_5\ : label is "soft_lutpair89";
begin
  \FSM_onehot_state_reg[1]\(0) <= \^fsm_onehot_state_reg[1]\(0);
  Q(3 downto 0) <= \^q\(3 downto 0);
  SS(0) <= \^ss\(0);
  \i_sig_reg[6]_0\ <= \^i_sig_reg[6]_0\;
  w_sw(2 downto 0) <= \^w_sw\(2 downto 0);
\FSM_onehot_state[1]_i_16\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => p_0_in(7),
      I1 => p_1_in(7),
      I2 => p_0_in(6),
      I3 => p_1_in(6),
      O => \FSM_onehot_state[1]_i_16_n_0\
    );
\FSM_onehot_state[1]_i_17\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => p_0_in(5),
      I1 => p_1_in(5),
      I2 => p_0_in(4),
      I3 => p_1_in(4),
      O => \FSM_onehot_state[1]_i_17_n_0\
    );
\FSM_onehot_state[1]_i_18\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => p_0_in(3),
      I1 => p_1_in(3),
      I2 => p_0_in(2),
      I3 => p_1_in(2),
      O => \FSM_onehot_state[1]_i_18_n_0\
    );
\FSM_onehot_state[1]_i_19\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => p_0_in(1),
      I1 => p_1_in(1),
      I2 => p_0_in(0),
      I3 => p_1_in(0),
      O => \FSM_onehot_state[1]_i_19_n_0\
    );
\FSM_onehot_state[1]_i_20\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => p_1_in(7),
      I1 => p_0_in(7),
      I2 => p_1_in(6),
      I3 => p_0_in(6),
      O => \FSM_onehot_state[1]_i_20_n_0\
    );
\FSM_onehot_state[1]_i_21\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => p_1_in(5),
      I1 => p_0_in(5),
      I2 => p_1_in(4),
      I3 => p_0_in(4),
      O => \FSM_onehot_state[1]_i_21_n_0\
    );
\FSM_onehot_state[1]_i_22\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => p_1_in(3),
      I1 => p_0_in(3),
      I2 => p_1_in(2),
      I3 => p_0_in(2),
      O => \FSM_onehot_state[1]_i_22_n_0\
    );
\FSM_onehot_state[1]_i_23\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => p_1_in(1),
      I1 => p_0_in(1),
      I2 => p_1_in(0),
      I3 => p_0_in(0),
      O => \FSM_onehot_state[1]_i_23_n_0\
    );
\FSM_onehot_state[1]_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => p_0_in(8),
      O => \FSM_onehot_state[1]_i_5_n_0\
    );
\FSM_onehot_state[1]_i_7\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => p_0_in(8),
      O => \FSM_onehot_state[1]_i_7_n_0\
    );
\FSM_onehot_state[2]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFFFFFF"
    )
        port map (
      I0 => \^q\(3),
      I1 => \^q\(1),
      I2 => \^i_sig_reg[6]_0\,
      I3 => \^q\(0),
      I4 => \^q\(2),
      O => \^w_sw\(2)
    );
\FSM_onehot_state_reg[1]_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => \FSM_onehot_state_reg[1]_i_6_n_0\,
      CO(3 downto 1) => \NLW_FSM_onehot_state_reg[1]_i_3_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \^fsm_onehot_state_reg[1]\(0),
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => p_0_in(8),
      O(3 downto 0) => \NLW_FSM_onehot_state_reg[1]_i_3_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => B"000",
      S(0) => \FSM_onehot_state[1]_i_7_n_0\
    );
\FSM_onehot_state_reg[1]_i_6\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \FSM_onehot_state_reg[1]_i_6_n_0\,
      CO(2) => \FSM_onehot_state_reg[1]_i_6_n_1\,
      CO(1) => \FSM_onehot_state_reg[1]_i_6_n_2\,
      CO(0) => \FSM_onehot_state_reg[1]_i_6_n_3\,
      CYINIT => '1',
      DI(3) => \FSM_onehot_state[1]_i_16_n_0\,
      DI(2) => \FSM_onehot_state[1]_i_17_n_0\,
      DI(1) => \FSM_onehot_state[1]_i_18_n_0\,
      DI(0) => \FSM_onehot_state[1]_i_19_n_0\,
      O(3 downto 0) => \NLW_FSM_onehot_state_reg[1]_i_6_O_UNCONNECTED\(3 downto 0),
      S(3) => \FSM_onehot_state[1]_i_20_n_0\,
      S(2) => \FSM_onehot_state[1]_i_21_n_0\,
      S(1) => \FSM_onehot_state[1]_i_22_n_0\,
      S(0) => \FSM_onehot_state[1]_i_23_n_0\
    );
\L_bus_in_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \^w_sw\(1),
      D => D_L_O(0),
      Q => L_bus_in(0),
      R => \^ss\(0)
    );
\L_bus_in_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \^w_sw\(1),
      D => D_L_O(10),
      Q => L_bus_in(10),
      R => \^ss\(0)
    );
\L_bus_in_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \^w_sw\(1),
      D => D_L_O(11),
      Q => L_bus_in(11),
      R => \^ss\(0)
    );
\L_bus_in_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \^w_sw\(1),
      D => D_L_O(12),
      Q => L_bus_in(12),
      R => \^ss\(0)
    );
\L_bus_in_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \^w_sw\(1),
      D => D_L_O(13),
      Q => L_bus_in(13),
      R => \^ss\(0)
    );
\L_bus_in_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \^w_sw\(1),
      D => D_L_O(14),
      Q => L_bus_in(14),
      R => \^ss\(0)
    );
\L_bus_in_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \^w_sw\(1),
      D => D_L_O(15),
      Q => L_bus_in(15),
      R => \^ss\(0)
    );
\L_bus_in_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \^w_sw\(1),
      D => D_L_O(16),
      Q => L_bus_in(16),
      R => \^ss\(0)
    );
\L_bus_in_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \^w_sw\(1),
      D => D_L_O(17),
      Q => L_bus_in(17),
      R => \^ss\(0)
    );
\L_bus_in_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \^w_sw\(1),
      D => D_L_O(1),
      Q => L_bus_in(1),
      R => \^ss\(0)
    );
\L_bus_in_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \^w_sw\(1),
      D => D_L_O(2),
      Q => L_bus_in(2),
      R => \^ss\(0)
    );
\L_bus_in_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \^w_sw\(1),
      D => D_L_O(3),
      Q => L_bus_in(3),
      R => \^ss\(0)
    );
\L_bus_in_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \^w_sw\(1),
      D => D_L_O(4),
      Q => L_bus_in(4),
      R => \^ss\(0)
    );
\L_bus_in_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \^w_sw\(1),
      D => D_L_O(5),
      Q => L_bus_in(5),
      R => \^ss\(0)
    );
\L_bus_in_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \^w_sw\(1),
      D => D_L_O(6),
      Q => L_bus_in(6),
      R => \^ss\(0)
    );
\L_bus_in_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \^w_sw\(1),
      D => D_L_O(7),
      Q => L_bus_in(7),
      R => \^ss\(0)
    );
\L_bus_in_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \^w_sw\(1),
      D => D_L_O(8),
      Q => L_bus_in(8),
      R => \^ss\(0)
    );
\L_bus_in_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \^w_sw\(1),
      D => D_L_O(9),
      Q => L_bus_in(9),
      R => \^ss\(0)
    );
\R_bus_in_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \^w_sw\(1),
      D => D_R_O(0),
      Q => R_bus_in(0),
      R => \^ss\(0)
    );
\R_bus_in_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \^w_sw\(1),
      D => D_R_O(10),
      Q => R_bus_in(10),
      R => \^ss\(0)
    );
\R_bus_in_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \^w_sw\(1),
      D => D_R_O(11),
      Q => R_bus_in(11),
      R => \^ss\(0)
    );
\R_bus_in_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \^w_sw\(1),
      D => D_R_O(12),
      Q => R_bus_in(12),
      R => \^ss\(0)
    );
\R_bus_in_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \^w_sw\(1),
      D => D_R_O(13),
      Q => R_bus_in(13),
      R => \^ss\(0)
    );
\R_bus_in_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \^w_sw\(1),
      D => D_R_O(14),
      Q => R_bus_in(14),
      R => \^ss\(0)
    );
\R_bus_in_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \^w_sw\(1),
      D => D_R_O(15),
      Q => R_bus_in(15),
      R => \^ss\(0)
    );
\R_bus_in_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \^w_sw\(1),
      D => D_R_O(16),
      Q => R_bus_in(16),
      R => \^ss\(0)
    );
\R_bus_in_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \^w_sw\(1),
      D => D_R_O(17),
      Q => R_bus_in(17),
      R => \^ss\(0)
    );
\R_bus_in_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \^w_sw\(1),
      D => D_R_O(1),
      Q => R_bus_in(1),
      R => \^ss\(0)
    );
\R_bus_in_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \^w_sw\(1),
      D => D_R_O(2),
      Q => R_bus_in(2),
      R => \^ss\(0)
    );
\R_bus_in_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \^w_sw\(1),
      D => D_R_O(3),
      Q => R_bus_in(3),
      R => \^ss\(0)
    );
\R_bus_in_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \^w_sw\(1),
      D => D_R_O(4),
      Q => R_bus_in(4),
      R => \^ss\(0)
    );
\R_bus_in_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \^w_sw\(1),
      D => D_R_O(5),
      Q => R_bus_in(5),
      R => \^ss\(0)
    );
\R_bus_in_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \^w_sw\(1),
      D => D_R_O(6),
      Q => R_bus_in(6),
      R => \^ss\(0)
    );
\R_bus_in_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \^w_sw\(1),
      D => D_R_O(7),
      Q => R_bus_in(7),
      R => \^ss\(0)
    );
\R_bus_in_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \^w_sw\(1),
      D => D_R_O(8),
      Q => R_bus_in(8),
      R => \^ss\(0)
    );
\R_bus_in_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \^w_sw\(1),
      D => D_R_O(9),
      Q => R_bus_in(9),
      R => \^ss\(0)
    );
\UnL_bus_out2_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => UnL_bus_out2,
      D => D_L_O(10),
      Q => p_1_in(0),
      R => '0'
    );
\UnL_bus_out2_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => UnL_bus_out2,
      D => D_L_O(11),
      Q => p_1_in(1),
      R => '0'
    );
\UnL_bus_out2_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => UnL_bus_out2,
      D => D_L_O(12),
      Q => p_1_in(2),
      R => '0'
    );
\UnL_bus_out2_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => UnL_bus_out2,
      D => D_L_O(13),
      Q => p_1_in(3),
      R => '0'
    );
\UnL_bus_out2_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => UnL_bus_out2,
      D => D_L_O(14),
      Q => p_1_in(4),
      R => '0'
    );
\UnL_bus_out2_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => UnL_bus_out2,
      D => D_L_O(15),
      Q => p_1_in(5),
      R => '0'
    );
\UnL_bus_out2_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => UnL_bus_out2,
      D => D_L_O(16),
      Q => p_1_in(6),
      R => '0'
    );
\UnL_bus_out2_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => UnL_bus_out2,
      D => UnL_bus_out(17),
      Q => p_1_in(7),
      R => '0'
    );
\i_sig[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \i_sig_reg_n_0_[0]\,
      O => \i_sig[0]_i_1_n_0\
    );
\i_sig[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \i_sig_reg_n_0_[1]\,
      I1 => \i_sig_reg_n_0_[0]\,
      O => \i_sig[1]_i_1_n_0\
    );
\i_sig[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0F7F0080"
    )
        port map (
      I0 => \i_sig_reg_n_0_[1]\,
      I1 => \i_sig_reg_n_0_[0]\,
      I2 => \FSM_onehot_state_reg[4]\(0),
      I3 => \FSM_onehot_state_reg[4]\(1),
      I4 => \i_sig_reg_n_0_[2]\,
      O => \i_sig[2]_i_1_n_0\
    );
\i_sig[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6AAA"
    )
        port map (
      I0 => \i_sig_reg_n_0_[3]\,
      I1 => \i_sig_reg_n_0_[1]\,
      I2 => \i_sig_reg_n_0_[0]\,
      I3 => \i_sig_reg_n_0_[2]\,
      O => \i_sig[3]_i_1_n_0\
    );
\i_sig[4]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3708"
    )
        port map (
      I0 => \i_sig[4]_i_2_n_0\,
      I1 => \FSM_onehot_state_reg[4]\(0),
      I2 => \FSM_onehot_state_reg[4]\(1),
      I3 => \i_sig_reg_n_0_[4]\,
      O => \i_sig[4]_i_1_n_0\
    );
\i_sig[4]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => \i_sig_reg_n_0_[3]\,
      I1 => \i_sig_reg_n_0_[1]\,
      I2 => \i_sig_reg_n_0_[0]\,
      I3 => \i_sig_reg_n_0_[2]\,
      O => \i_sig[4]_i_2_n_0\
    );
\i_sig[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AAAAAAAAAAAAAAA"
    )
        port map (
      I0 => \i_sig_reg_n_0_[5]\,
      I1 => \i_sig_reg_n_0_[3]\,
      I2 => \i_sig_reg_n_0_[1]\,
      I3 => \i_sig_reg_n_0_[0]\,
      I4 => \i_sig_reg_n_0_[2]\,
      I5 => \i_sig_reg_n_0_[4]\,
      O => \i_sig[5]_i_1_n_0\
    );
\i_sig[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^i_sig_reg[6]_0\,
      O => \i_sig[6]_i_1_n_0\
    );
\i_sig[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6A"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^i_sig_reg[6]_0\,
      I2 => \^q\(0),
      O => \i_sig[7]_i_1_n_0\
    );
\i_sig[8]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6AAA"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(0),
      I2 => \^i_sig_reg[6]_0\,
      I3 => \^q\(1),
      O => \i_sig[8]_i_1_n_0\
    );
\i_sig[9]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6AAAAAAA"
    )
        port map (
      I0 => \^q\(3),
      I1 => \^q\(1),
      I2 => \^i_sig_reg[6]_0\,
      I3 => \^q\(0),
      I4 => \^q\(2),
      O => \i_sig[9]_i_3_n_0\
    );
\i_sig[9]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => \i_sig_reg_n_0_[5]\,
      I1 => \i_sig_reg_n_0_[3]\,
      I2 => \i_sig_reg_n_0_[1]\,
      I3 => \i_sig_reg_n_0_[0]\,
      I4 => \i_sig_reg_n_0_[2]\,
      I5 => \i_sig_reg_n_0_[4]\,
      O => \^i_sig_reg[6]_0\
    );
\i_sig_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => E(0),
      D => \i_sig[0]_i_1_n_0\,
      Q => \i_sig_reg_n_0_[0]\,
      R => SR(0)
    );
\i_sig_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => E(0),
      D => \i_sig[1]_i_1_n_0\,
      Q => \i_sig_reg_n_0_[1]\,
      R => SR(0)
    );
\i_sig_reg[2]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \i_sig[2]_i_1_n_0\,
      Q => \i_sig_reg_n_0_[2]\,
      S => \^ss\(0)
    );
\i_sig_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => E(0),
      D => \i_sig[3]_i_1_n_0\,
      Q => \i_sig_reg_n_0_[3]\,
      R => SR(0)
    );
\i_sig_reg[4]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \i_sig[4]_i_1_n_0\,
      Q => \i_sig_reg_n_0_[4]\,
      S => \^ss\(0)
    );
\i_sig_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => E(0),
      D => \i_sig[5]_i_1_n_0\,
      Q => \i_sig_reg_n_0_[5]\,
      R => SR(0)
    );
\i_sig_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => E(0),
      D => \i_sig[6]_i_1_n_0\,
      Q => \^q\(0),
      R => SR(0)
    );
\i_sig_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => E(0),
      D => \i_sig[7]_i_1_n_0\,
      Q => \^q\(1),
      R => SR(0)
    );
\i_sig_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => E(0),
      D => \i_sig[8]_i_1_n_0\,
      Q => \^q\(2),
      R => SR(0)
    );
\i_sig_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => E(0),
      D => \i_sig[9]_i_3_n_0\,
      Q => \^q\(3),
      R => SR(0)
    );
leftChannelMemory: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_unimacro_BRAM_SDP_MACRO
     port map (
      ADDRBWRADDR(9 downto 0) => WRADDR(9 downto 0),
      DIBDI(15 downto 0) => DI(15 downto 0),
      DOADO(4 downto 3) => readL(15 downto 14),
      DOADO(2 downto 0) => readL(10 downto 8),
      Q(9 downto 0) => AddrRead(9 downto 0),
      S(0) => leftChannelMemory_n_5,
      WREN => WREN,
      \encoded_reg[8]\ => leftChannelMemory_n_6,
      \processRow_reg[5]\(2 downto 0) => row(5 downto 3),
      s00_axi_aclk => s00_axi_aclk,
      s00_axi_aresetn => \^ss\(0)
    );
\old_button_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => btn(0),
      Q => old_button(0),
      R => '0'
    );
\old_button_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => btn(1),
      Q => old_button(1),
      R => '0'
    );
\old_button_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => btn(2),
      Q => old_button(2),
      R => '0'
    );
\old_button_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => btn(3),
      Q => old_button(3),
      R => '0'
    );
\old_button_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => btn(4),
      Q => old_button(4),
      R => '0'
    );
rightChannelMemory: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_unimacro_BRAM_SDP_MACRO__parameterized0\
     port map (
      ADDRBWRADDR(9 downto 0) => WRADDR(9 downto 0),
      DIBDI(15) => wrapper_inst_n_43,
      DIBDI(14) => wrapper_inst_n_44,
      DIBDI(13) => wrapper_inst_n_45,
      DIBDI(12) => wrapper_inst_n_46,
      DIBDI(11) => wrapper_inst_n_47,
      DIBDI(10) => wrapper_inst_n_48,
      DIBDI(9) => wrapper_inst_n_49,
      DIBDI(8) => wrapper_inst_n_50,
      DIBDI(7) => wrapper_inst_n_51,
      DIBDI(6) => wrapper_inst_n_52,
      DIBDI(5) => wrapper_inst_n_53,
      DIBDI(4) => wrapper_inst_n_54,
      DIBDI(3) => wrapper_inst_n_55,
      DIBDI(2) => wrapper_inst_n_56,
      DIBDI(1) => wrapper_inst_n_57,
      DIBDI(0) => wrapper_inst_n_58,
      DOADO(1 downto 0) => readR(15 downto 14),
      Q(9 downto 0) => AddrRead(9 downto 0),
      S(1) => rightChannelMemory_n_12,
      S(0) => rightChannelMemory_n_13,
      WREN => WREN,
      \i_sig_reg[2]\ => \i_sig_reg_n_0_[2]\,
      \i_sig_reg[4]\ => \i_sig_reg_n_0_[4]\,
      \i_sig_reg[9]\(7 downto 4) => \^q\(3 downto 0),
      \i_sig_reg[9]\(3) => \i_sig_reg_n_0_[5]\,
      \i_sig_reg[9]\(2) => \i_sig_reg_n_0_[3]\,
      \i_sig_reg[9]\(1) => \i_sig_reg_n_0_[1]\,
      \i_sig_reg[9]\(0) => \i_sig_reg_n_0_[0]\,
      \processRow_reg[5]\(5 downto 0) => row(5 downto 0),
      s00_axi_aclk => s00_axi_aclk,
      s00_axi_aresetn => \^ss\(0),
      \slv_reg8_reg[9]\(9 downto 0) => \slv_reg8_reg[9]\(9 downto 0),
      switch(0) => switch(2)
    );
\trigger_time[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000F04"
    )
        port map (
      I0 => btn(2),
      I1 => btn(3),
      I2 => \trigger_time[8]_i_3_n_0\,
      I3 => btn(1),
      I4 => btn(0),
      I5 => \trigger_time_reg_n_0_[0]\,
      O => trigger_time0_in(0)
    );
\trigger_time[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF417D"
    )
        port map (
      I0 => \trigger_time[8]_i_8_n_0\,
      I1 => \trigger_time_reg_n_0_[0]\,
      I2 => \trigger_time_reg_n_0_[1]\,
      I3 => \trigger_time[8]_i_5_n_0\,
      I4 => \trigger_time[8]_i_4_n_0\,
      O => trigger_time0_in(1)
    );
\trigger_time[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CA0E0EAC"
    )
        port map (
      I0 => \trigger_time[8]_i_5_n_0\,
      I1 => \trigger_time[8]_i_8_n_0\,
      I2 => \trigger_time_reg_n_0_[2]\,
      I3 => \trigger_time_reg_n_0_[0]\,
      I4 => \trigger_time_reg_n_0_[1]\,
      O => \trigger_time[2]_i_1_n_0\
    );
\trigger_time[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EEE33BBB22222888"
    )
        port map (
      I0 => \trigger_time[8]_i_8_n_0\,
      I1 => \trigger_time_reg_n_0_[3]\,
      I2 => \trigger_time_reg_n_0_[1]\,
      I3 => \trigger_time_reg_n_0_[0]\,
      I4 => \trigger_time_reg_n_0_[2]\,
      I5 => \trigger_time[8]_i_5_n_0\,
      O => \trigger_time[3]_i_1_n_0\
    );
\trigger_time[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"4FF84848"
    )
        port map (
      I0 => \trigger_time[4]_i_2_n_0\,
      I1 => \trigger_time[8]_i_8_n_0\,
      I2 => \trigger_time_reg_n_0_[4]\,
      I3 => \trigger_time[4]_i_3_n_0\,
      I4 => \trigger_time[8]_i_5_n_0\,
      O => \trigger_time[4]_i_1_n_0\
    );
\trigger_time[4]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AA80"
    )
        port map (
      I0 => \trigger_time_reg_n_0_[3]\,
      I1 => \trigger_time_reg_n_0_[1]\,
      I2 => \trigger_time_reg_n_0_[0]\,
      I3 => \trigger_time_reg_n_0_[2]\,
      O => \trigger_time[4]_i_2_n_0\
    );
\trigger_time[4]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0155"
    )
        port map (
      I0 => \trigger_time_reg_n_0_[3]\,
      I1 => \trigger_time_reg_n_0_[1]\,
      I2 => \trigger_time_reg_n_0_[0]\,
      I3 => \trigger_time_reg_n_0_[2]\,
      O => \trigger_time[4]_i_3_n_0\
    );
\trigger_time[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAFBBFFBBFAABFFB"
    )
        port map (
      I0 => \trigger_time[8]_i_4_n_0\,
      I1 => \trigger_time[8]_i_5_n_0\,
      I2 => \trigger_time[5]_i_2_n_0\,
      I3 => \trigger_time_reg_n_0_[5]\,
      I4 => \trigger_time[8]_i_8_n_0\,
      I5 => \trigger_time[5]_i_3_n_0\,
      O => trigger_time0_in(5)
    );
\trigger_time[5]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000057"
    )
        port map (
      I0 => \trigger_time_reg_n_0_[2]\,
      I1 => \trigger_time_reg_n_0_[0]\,
      I2 => \trigger_time_reg_n_0_[1]\,
      I3 => \trigger_time_reg_n_0_[3]\,
      I4 => \trigger_time_reg_n_0_[4]\,
      O => \trigger_time[5]_i_2_n_0\
    );
\trigger_time[5]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EA000000"
    )
        port map (
      I0 => \trigger_time_reg_n_0_[2]\,
      I1 => \trigger_time_reg_n_0_[0]\,
      I2 => \trigger_time_reg_n_0_[1]\,
      I3 => \trigger_time_reg_n_0_[3]\,
      I4 => \trigger_time_reg_n_0_[4]\,
      O => \trigger_time[5]_i_3_n_0\
    );
\trigger_time[6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"4FF84848"
    )
        port map (
      I0 => \trigger_time[7]_i_2_n_0\,
      I1 => \trigger_time[8]_i_8_n_0\,
      I2 => \trigger_time_reg_n_0_[6]\,
      I3 => \trigger_time[7]_i_3_n_0\,
      I4 => \trigger_time[8]_i_5_n_0\,
      O => \trigger_time[6]_i_1_n_0\
    );
\trigger_time[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F8CFF8F048C048C0"
    )
        port map (
      I0 => \trigger_time[7]_i_2_n_0\,
      I1 => \trigger_time[8]_i_8_n_0\,
      I2 => \trigger_time_reg_n_0_[7]\,
      I3 => \trigger_time_reg_n_0_[6]\,
      I4 => \trigger_time[7]_i_3_n_0\,
      I5 => \trigger_time[8]_i_5_n_0\,
      O => \trigger_time[7]_i_1_n_0\
    );
\trigger_time[7]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8888800000000000"
    )
        port map (
      I0 => \trigger_time_reg_n_0_[4]\,
      I1 => \trigger_time_reg_n_0_[3]\,
      I2 => \trigger_time_reg_n_0_[1]\,
      I3 => \trigger_time_reg_n_0_[0]\,
      I4 => \trigger_time_reg_n_0_[2]\,
      I5 => \trigger_time_reg_n_0_[5]\,
      O => \trigger_time[7]_i_2_n_0\
    );
\trigger_time[7]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000011111"
    )
        port map (
      I0 => \trigger_time_reg_n_0_[4]\,
      I1 => \trigger_time_reg_n_0_[3]\,
      I2 => \trigger_time_reg_n_0_[1]\,
      I3 => \trigger_time_reg_n_0_[0]\,
      I4 => \trigger_time_reg_n_0_[2]\,
      I5 => \trigger_time_reg_n_0_[5]\,
      O => \trigger_time[7]_i_3_n_0\
    );
\trigger_time[8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000F04FFFFFFFF"
    )
        port map (
      I0 => btn(2),
      I1 => btn(3),
      I2 => \trigger_time[8]_i_3_n_0\,
      I3 => btn(1),
      I4 => btn(0),
      I5 => s00_axi_aresetn,
      O => \trigger_time[8]_i_1_n_0\
    );
\trigger_time[8]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAFBBFAABFFBBFFB"
    )
        port map (
      I0 => \trigger_time[8]_i_4_n_0\,
      I1 => \trigger_time[8]_i_5_n_0\,
      I2 => \trigger_time[8]_i_6_n_0\,
      I3 => \trigger_time_reg_n_0_[8]\,
      I4 => \trigger_time[8]_i_7_n_0\,
      I5 => \trigger_time[8]_i_8_n_0\,
      O => trigger_time0_in(8)
    );
\trigger_time[8]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"41000041"
    )
        port map (
      I0 => \trigger_time[8]_i_9_n_0\,
      I1 => old_button(4),
      I2 => btn(4),
      I3 => btn(3),
      I4 => old_button(3),
      O => \trigger_time[8]_i_3_n_0\
    );
\trigger_time[8]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => btn(0),
      I1 => \trigger_time[8]_i_3_n_0\,
      O => \trigger_time[8]_i_4_n_0\
    );
\trigger_time[8]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => btn(1),
      I1 => \trigger_time[8]_i_3_n_0\,
      O => \trigger_time[8]_i_5_n_0\
    );
\trigger_time[8]_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => \trigger_time_reg_n_0_[6]\,
      I1 => \trigger_time[7]_i_3_n_0\,
      I2 => \trigger_time_reg_n_0_[7]\,
      O => \trigger_time[8]_i_6_n_0\
    );
\trigger_time[8]_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => \trigger_time_reg_n_0_[6]\,
      I1 => \trigger_time[7]_i_2_n_0\,
      I2 => \trigger_time_reg_n_0_[7]\,
      O => \trigger_time[8]_i_7_n_0\
    );
\trigger_time[8]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0010"
    )
        port map (
      I0 => btn(1),
      I1 => btn(2),
      I2 => btn(3),
      I3 => \trigger_time[8]_i_3_n_0\,
      O => \trigger_time[8]_i_8_n_0\
    );
\trigger_time[8]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6FF6FFFFFFFF6FF6"
    )
        port map (
      I0 => btn(0),
      I1 => old_button(0),
      I2 => btn(2),
      I3 => old_button(2),
      I4 => btn(1),
      I5 => old_button(1),
      O => \trigger_time[8]_i_9_n_0\
    );
\trigger_time[9]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \trigger_time[8]_i_4_n_0\,
      I1 => \trigger_time[8]_i_1_n_0\,
      O => \trigger_time[9]_i_1_n_0\
    );
\trigger_time[9]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFFFF088CC440088"
    )
        port map (
      I0 => \trigger_time[8]_i_6_n_0\,
      I1 => \trigger_time[8]_i_5_n_0\,
      I2 => \trigger_time[8]_i_7_n_0\,
      I3 => \trigger_time_reg_n_0_[8]\,
      I4 => \trigger_time_reg_n_0_[9]\,
      I5 => \trigger_time[8]_i_8_n_0\,
      O => \trigger_time[9]_i_2_n_0\
    );
\trigger_time_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \trigger_time[8]_i_1_n_0\,
      D => trigger_time0_in(0),
      Q => \trigger_time_reg_n_0_[0]\,
      R => '0'
    );
\trigger_time_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \trigger_time[8]_i_1_n_0\,
      D => trigger_time0_in(1),
      Q => \trigger_time_reg_n_0_[1]\,
      R => '0'
    );
\trigger_time_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \trigger_time[8]_i_1_n_0\,
      D => \trigger_time[2]_i_1_n_0\,
      Q => \trigger_time_reg_n_0_[2]\,
      R => \trigger_time[9]_i_1_n_0\
    );
\trigger_time_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \trigger_time[8]_i_1_n_0\,
      D => \trigger_time[3]_i_1_n_0\,
      Q => \trigger_time_reg_n_0_[3]\,
      R => \trigger_time[9]_i_1_n_0\
    );
\trigger_time_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \trigger_time[8]_i_1_n_0\,
      D => \trigger_time[4]_i_1_n_0\,
      Q => \trigger_time_reg_n_0_[4]\,
      R => \trigger_time[9]_i_1_n_0\
    );
\trigger_time_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \trigger_time[8]_i_1_n_0\,
      D => trigger_time0_in(5),
      Q => \trigger_time_reg_n_0_[5]\,
      R => '0'
    );
\trigger_time_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \trigger_time[8]_i_1_n_0\,
      D => \trigger_time[6]_i_1_n_0\,
      Q => \trigger_time_reg_n_0_[6]\,
      R => \trigger_time[9]_i_1_n_0\
    );
\trigger_time_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \trigger_time[8]_i_1_n_0\,
      D => \trigger_time[7]_i_1_n_0\,
      Q => \trigger_time_reg_n_0_[7]\,
      R => \trigger_time[9]_i_1_n_0\
    );
\trigger_time_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \trigger_time[8]_i_1_n_0\,
      D => trigger_time0_in(8),
      Q => \trigger_time_reg_n_0_[8]\,
      R => '0'
    );
\trigger_time_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \trigger_time[8]_i_1_n_0\,
      D => \trigger_time[9]_i_2_n_0\,
      Q => \trigger_time_reg_n_0_[9]\,
      R => \trigger_time[9]_i_1_n_0\
    );
\trigger_volt[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DDCDFFFF"
    )
        port map (
      I0 => btn(0),
      I1 => \trigger_time[8]_i_3_n_0\,
      I2 => btn(2),
      I3 => btn(1),
      I4 => \trigger_volt_reg_n_0_[0]\,
      O => \trigger_volt[0]_i_1_n_0\
    );
\trigger_volt[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EB28"
    )
        port map (
      I0 => \trigger_volt[9]_i_4_n_0\,
      I1 => \trigger_volt_reg_n_0_[0]\,
      I2 => p_0_in(0),
      I3 => \trigger_time[8]_i_4_n_0\,
      O => \p_0_in__1\(1)
    );
\trigger_volt[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B33E8222"
    )
        port map (
      I0 => \trigger_volt[9]_i_4_n_0\,
      I1 => p_0_in(1),
      I2 => p_0_in(0),
      I3 => \trigger_volt_reg_n_0_[0]\,
      I4 => \trigger_time[8]_i_4_n_0\,
      O => \p_0_in__1\(2)
    );
\trigger_volt[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"E3E3E32F2F3E3E3E"
    )
        port map (
      I0 => \trigger_volt[3]_i_2_n_0\,
      I1 => \trigger_time[8]_i_4_n_0\,
      I2 => p_0_in(2),
      I3 => \trigger_volt_reg_n_0_[0]\,
      I4 => p_0_in(0),
      I5 => p_0_in(1),
      O => \p_0_in__1\(3)
    );
\trigger_volt[3]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => \trigger_time[8]_i_3_n_0\,
      I1 => btn(2),
      I2 => btn(1),
      O => \trigger_volt[3]_i_2_n_0\
    );
\trigger_volt[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0D707D7D"
    )
        port map (
      I0 => \trigger_volt[9]_i_4_n_0\,
      I1 => \trigger_volt[4]_i_2_n_0\,
      I2 => p_0_in(3),
      I3 => \trigger_volt[4]_i_3_n_0\,
      I4 => \trigger_time[8]_i_4_n_0\,
      O => \p_0_in__1\(4)
    );
\trigger_volt[4]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AA80"
    )
        port map (
      I0 => p_0_in(2),
      I1 => \trigger_volt_reg_n_0_[0]\,
      I2 => p_0_in(0),
      I3 => p_0_in(1),
      O => \trigger_volt[4]_i_2_n_0\
    );
\trigger_volt[4]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0155"
    )
        port map (
      I0 => p_0_in(2),
      I1 => \trigger_volt_reg_n_0_[0]\,
      I2 => p_0_in(0),
      I3 => p_0_in(1),
      O => \trigger_volt[4]_i_3_n_0\
    );
\trigger_volt[5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0D707D7D"
    )
        port map (
      I0 => \trigger_volt[9]_i_4_n_0\,
      I1 => \trigger_volt[5]_i_2_n_0\,
      I2 => p_0_in(4),
      I3 => \trigger_volt[5]_i_3_n_0\,
      I4 => \trigger_time[8]_i_4_n_0\,
      O => \p_0_in__1\(5)
    );
\trigger_volt[5]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EA000000"
    )
        port map (
      I0 => p_0_in(1),
      I1 => p_0_in(0),
      I2 => \trigger_volt_reg_n_0_[0]\,
      I3 => p_0_in(2),
      I4 => p_0_in(3),
      O => \trigger_volt[5]_i_2_n_0\
    );
\trigger_volt[5]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000057"
    )
        port map (
      I0 => p_0_in(1),
      I1 => p_0_in(0),
      I2 => \trigger_volt_reg_n_0_[0]\,
      I3 => p_0_in(2),
      I4 => p_0_in(3),
      O => \trigger_volt[5]_i_3_n_0\
    );
\trigger_volt[6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"4FF84848"
    )
        port map (
      I0 => \trigger_volt[7]_i_3_n_0\,
      I1 => \trigger_time[8]_i_4_n_0\,
      I2 => p_0_in(5),
      I3 => \trigger_volt[7]_i_2_n_0\,
      I4 => \trigger_volt[9]_i_4_n_0\,
      O => \p_0_in__1\(6)
    );
\trigger_volt[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"700570F07DF57DF5"
    )
        port map (
      I0 => \trigger_volt[9]_i_4_n_0\,
      I1 => \trigger_volt[7]_i_2_n_0\,
      I2 => p_0_in(6),
      I3 => p_0_in(5),
      I4 => \trigger_volt[7]_i_3_n_0\,
      I5 => \trigger_time[8]_i_4_n_0\,
      O => \p_0_in__1\(7)
    );
\trigger_volt[7]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8888800000000000"
    )
        port map (
      I0 => p_0_in(3),
      I1 => p_0_in(2),
      I2 => \trigger_volt_reg_n_0_[0]\,
      I3 => p_0_in(0),
      I4 => p_0_in(1),
      I5 => p_0_in(4),
      O => \trigger_volt[7]_i_2_n_0\
    );
\trigger_volt[7]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000011111"
    )
        port map (
      I0 => p_0_in(3),
      I1 => p_0_in(2),
      I2 => \trigger_volt_reg_n_0_[0]\,
      I3 => p_0_in(0),
      I4 => p_0_in(1),
      I5 => p_0_in(4),
      O => \trigger_volt[7]_i_3_n_0\
    );
\trigger_volt[8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFF8F0F8C048C048"
    )
        port map (
      I0 => \trigger_volt[8]_i_2_n_0\,
      I1 => \trigger_time[8]_i_4_n_0\,
      I2 => p_0_in(7),
      I3 => p_0_in(6),
      I4 => \trigger_volt[8]_i_3_n_0\,
      I5 => \trigger_volt[9]_i_4_n_0\,
      O => \p_0_in__1\(8)
    );
\trigger_volt[8]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000051"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(1),
      I2 => \trigger_volt[8]_i_4_n_0\,
      I3 => p_0_in(2),
      I4 => p_0_in(3),
      I5 => p_0_in(5),
      O => \trigger_volt[8]_i_2_n_0\
    );
\trigger_volt[8]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A800000000000000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(1),
      I2 => \trigger_volt[8]_i_5_n_0\,
      I3 => p_0_in(2),
      I4 => p_0_in(3),
      I5 => p_0_in(5),
      O => \trigger_volt[8]_i_3_n_0\
    );
\trigger_volt[8]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \trigger_volt_reg_n_0_[0]\,
      I1 => p_0_in(0),
      O => \trigger_volt[8]_i_4_n_0\
    );
\trigger_volt[8]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \trigger_volt_reg_n_0_[0]\,
      I1 => p_0_in(0),
      O => \trigger_volt[8]_i_5_n_0\
    );
\trigger_volt[9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"5D5D5F5D"
    )
        port map (
      I0 => s00_axi_aresetn,
      I1 => btn(0),
      I2 => \trigger_time[8]_i_3_n_0\,
      I3 => btn(2),
      I4 => btn(1),
      O => \trigger_volt[9]_i_1_n_0\
    );
\trigger_volt[9]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F8CFF8F048C048C0"
    )
        port map (
      I0 => \trigger_volt[9]_i_3_n_0\,
      I1 => \trigger_volt[9]_i_4_n_0\,
      I2 => p_0_in(8),
      I3 => p_0_in(7),
      I4 => \trigger_volt[9]_i_5_n_0\,
      I5 => \trigger_time[8]_i_4_n_0\,
      O => \p_0_in__1\(9)
    );
\trigger_volt[9]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => p_0_in(5),
      I1 => \trigger_volt[7]_i_2_n_0\,
      I2 => p_0_in(6),
      O => \trigger_volt[9]_i_3_n_0\
    );
\trigger_volt[9]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0010"
    )
        port map (
      I0 => btn(0),
      I1 => btn(1),
      I2 => btn(2),
      I3 => \trigger_time[8]_i_3_n_0\,
      O => \trigger_volt[9]_i_4_n_0\
    );
\trigger_volt[9]_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => p_0_in(5),
      I1 => \trigger_volt[7]_i_3_n_0\,
      I2 => p_0_in(6),
      O => \trigger_volt[9]_i_5_n_0\
    );
\trigger_volt_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \trigger_volt[9]_i_1_n_0\,
      D => \trigger_volt[0]_i_1_n_0\,
      Q => \trigger_volt_reg_n_0_[0]\,
      R => '0'
    );
\trigger_volt_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \trigger_volt[9]_i_1_n_0\,
      D => \p_0_in__1\(1),
      Q => p_0_in(0),
      R => '0'
    );
\trigger_volt_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \trigger_volt[9]_i_1_n_0\,
      D => \p_0_in__1\(2),
      Q => p_0_in(1),
      R => '0'
    );
\trigger_volt_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \trigger_volt[9]_i_1_n_0\,
      D => \p_0_in__1\(3),
      Q => p_0_in(2),
      R => '0'
    );
\trigger_volt_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \trigger_volt[9]_i_1_n_0\,
      D => \p_0_in__1\(4),
      Q => p_0_in(3),
      R => '0'
    );
\trigger_volt_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \trigger_volt[9]_i_1_n_0\,
      D => \p_0_in__1\(5),
      Q => p_0_in(4),
      R => '0'
    );
\trigger_volt_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \trigger_volt[9]_i_1_n_0\,
      D => \p_0_in__1\(6),
      Q => p_0_in(5),
      R => '0'
    );
\trigger_volt_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \trigger_volt[9]_i_1_n_0\,
      D => \p_0_in__1\(7),
      Q => p_0_in(6),
      R => '0'
    );
\trigger_volt_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \trigger_volt[9]_i_1_n_0\,
      D => \p_0_in__1\(8),
      Q => p_0_in(7),
      R => '0'
    );
\trigger_volt_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \trigger_volt[9]_i_1_n_0\,
      D => \p_0_in__1\(9),
      Q => p_0_in(8),
      R => '0'
    );
video_inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_video
     port map (
      DOADO(4 downto 3) => readL(15 downto 14),
      DOADO(2 downto 0) => readL(10 downto 8),
      Q(5 downto 0) => row(5 downto 0),
      S(0) => leftChannelMemory_n_5,
      lopt => lopt,
      s00_axi_aclk => s00_axi_aclk,
      s00_axi_aresetn => s00_axi_aresetn,
      s00_axi_aresetn_0 => \^ss\(0),
      \sdp_bl.ramb18_dp_bl.ram18_bl\(9 downto 0) => AddrRead(9 downto 0),
      \sdp_bl.ramb18_dp_bl.ram18_bl_0\ => leftChannelMemory_n_6,
      \sdp_bl.ramb18_dp_bl.ram18_bl_1\(1 downto 0) => readR(15 downto 14),
      \sdp_bl.ramb18_dp_bl.ram18_bl_2\(1) => rightChannelMemory_n_12,
      \sdp_bl.ramb18_dp_bl.ram18_bl_2\(0) => rightChannelMemory_n_13,
      switch(1 downto 0) => switch(1 downto 0),
      tmds(3 downto 0) => tmds(3 downto 0),
      tmdsb(3 downto 0) => tmdsb(3 downto 0),
      \trigger_time_reg[2]\ => \trigger_time_reg_n_0_[2]\,
      \trigger_time_reg[3]\ => \trigger_time_reg_n_0_[3]\,
      \trigger_time_reg[4]\ => \trigger_time_reg_n_0_[4]\,
      \trigger_time_reg[6]\ => \trigger_time_reg_n_0_[6]\,
      \trigger_time_reg[7]\ => \trigger_time_reg_n_0_[7]\,
      \trigger_time_reg[8]\(3) => \trigger_time_reg_n_0_[8]\,
      \trigger_time_reg[8]\(2) => \trigger_time_reg_n_0_[5]\,
      \trigger_time_reg[8]\(1) => \trigger_time_reg_n_0_[1]\,
      \trigger_time_reg[8]\(0) => \trigger_time_reg_n_0_[0]\,
      \trigger_time_reg[9]\ => \trigger_time_reg_n_0_[9]\,
      \trigger_volt_reg[9]\(9 downto 1) => p_0_in(8 downto 0),
      \trigger_volt_reg[9]\(0) => \trigger_volt_reg_n_0_[0]\
    );
wrapper_inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Audio_Codec_Wrapper
     port map (
      BCLK_int_reg => \^ss\(0),
      CO(0) => CO(0),
      D(0) => UnL_bus_out(17),
      DIBDI(15 downto 0) => DI(15 downto 0),
      E(0) => \^w_sw\(1),
      \L_bus_in_reg[17]\(17 downto 0) => D_L_O(17 downto 0),
      Q(17 downto 0) => L_bus_in(17 downto 0),
      \R_bus_in_reg[17]\(17 downto 0) => D_R_O(17 downto 0),
      \R_bus_in_reg[17]_0\(17 downto 0) => R_bus_in(17 downto 0),
      S(0) => \FSM_onehot_state[1]_i_5_n_0\,
      \UnL_bus_out2_reg[10]\(0) => UnL_bus_out2,
      ac_adc_sdata => ac_adc_sdata,
      ac_bclk => ac_bclk,
      ac_dac_sdata => ac_dac_sdata,
      ac_lrclk => ac_lrclk,
      ac_mclk => ac_mclk,
      lopt => lopt,
      s00_axi_aclk => s00_axi_aclk,
      s00_axi_aresetn => s00_axi_aresetn,
      scl => scl,
      sda => sda,
      \sdp_bl.ramb18_dp_bl.ram18_bl\(15) => wrapper_inst_n_43,
      \sdp_bl.ramb18_dp_bl.ram18_bl\(14) => wrapper_inst_n_44,
      \sdp_bl.ramb18_dp_bl.ram18_bl\(13) => wrapper_inst_n_45,
      \sdp_bl.ramb18_dp_bl.ram18_bl\(12) => wrapper_inst_n_46,
      \sdp_bl.ramb18_dp_bl.ram18_bl\(11) => wrapper_inst_n_47,
      \sdp_bl.ramb18_dp_bl.ram18_bl\(10) => wrapper_inst_n_48,
      \sdp_bl.ramb18_dp_bl.ram18_bl\(9) => wrapper_inst_n_49,
      \sdp_bl.ramb18_dp_bl.ram18_bl\(8) => wrapper_inst_n_50,
      \sdp_bl.ramb18_dp_bl.ram18_bl\(7) => wrapper_inst_n_51,
      \sdp_bl.ramb18_dp_bl.ram18_bl\(6) => wrapper_inst_n_52,
      \sdp_bl.ramb18_dp_bl.ram18_bl\(5) => wrapper_inst_n_53,
      \sdp_bl.ramb18_dp_bl.ram18_bl\(4) => wrapper_inst_n_54,
      \sdp_bl.ramb18_dp_bl.ram18_bl\(3) => wrapper_inst_n_55,
      \sdp_bl.ramb18_dp_bl.ram18_bl\(2) => wrapper_inst_n_56,
      \sdp_bl.ramb18_dp_bl.ram18_bl\(1) => wrapper_inst_n_57,
      \sdp_bl.ramb18_dp_bl.ram18_bl\(0) => wrapper_inst_n_58,
      \slv_reg10_reg[15]\(15 downto 0) => \slv_reg10_reg[15]\(15 downto 0),
      \slv_reg9_reg[15]\(15 downto 0) => \slv_reg9_reg[15]\(15 downto 0),
      switch(0) => switch(2),
      \trigger_volt_reg[8]\(7 downto 0) => p_0_in(7 downto 0),
      \trigger_volt_reg[9]\(0) => \^fsm_onehot_state_reg[1]\(0),
      w_sw(0) => \^w_sw\(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_my_lab3_ip_v3_0_S00_AXI is
  port (
    s00_axi_wready : out STD_LOGIC;
    s00_axi_awready : out STD_LOGIC;
    s00_axi_arready : out STD_LOGIC;
    s00_axi_bvalid : out STD_LOGIC;
    s00_axi_rvalid : out STD_LOGIC;
    s00_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    tmds : out STD_LOGIC_VECTOR ( 3 downto 0 );
    tmdsb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    ac_mclk : out STD_LOGIC;
    ac_dac_sdata : out STD_LOGIC;
    ac_lrclk : out STD_LOGIC;
    ac_bclk : out STD_LOGIC;
    scl : inout STD_LOGIC;
    sda : inout STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC;
    ac_adc_sdata : in STD_LOGIC;
    axi_bvalid_reg_0 : in STD_LOGIC;
    axi_arready_reg_0 : in STD_LOGIC;
    btn : in STD_LOGIC_VECTOR ( 4 downto 0 );
    s00_axi_aresetn : in STD_LOGIC;
    s00_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_awaddr : in STD_LOGIC_VECTOR ( 4 downto 0 );
    s00_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_araddr : in STD_LOGIC_VECTOR ( 4 downto 0 );
    s00_axi_wvalid : in STD_LOGIC;
    s00_axi_awvalid : in STD_LOGIC;
    s00_axi_arvalid : in STD_LOGIC;
    switch : in STD_LOGIC_VECTOR ( 2 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_my_lab3_ip_v3_0_S00_AXI;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_my_lab3_ip_v3_0_S00_AXI is
  signal RST : STD_LOGIC;
  signal WREN : STD_LOGIC;
  signal \axi_araddr_reg[2]_rep_n_0\ : STD_LOGIC;
  signal \axi_araddr_reg[3]_rep_n_0\ : STD_LOGIC;
  signal axi_arready0 : STD_LOGIC;
  signal axi_awready0 : STD_LOGIC;
  signal \axi_rdata[0]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[0]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[0]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[0]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[0]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[0]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[0]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[10]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[10]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[10]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[10]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[10]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[10]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[10]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[12]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[12]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[12]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[12]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[12]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[12]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[12]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[13]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[13]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[13]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[13]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[13]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[13]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[13]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[14]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[14]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[14]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[14]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[14]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[14]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[14]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[16]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[16]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[16]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[16]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[16]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[16]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[16]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[17]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[17]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[17]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[17]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[17]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[17]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[17]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[18]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[18]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[18]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[18]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[18]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[18]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[18]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[19]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[19]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[19]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[19]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[19]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[19]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[19]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[1]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[1]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[1]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[1]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[1]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[1]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[1]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[20]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[20]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[20]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[20]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[20]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[20]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[20]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[21]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[21]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[21]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[21]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[21]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[21]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[21]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[22]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[22]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[22]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[22]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[22]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[22]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[22]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[23]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[23]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[23]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[23]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[23]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[23]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[23]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[24]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[24]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[24]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[24]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[24]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[24]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[24]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[25]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[25]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[25]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[25]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[25]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[25]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[25]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[26]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[26]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[26]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[26]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[26]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[26]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[26]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[27]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[27]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[27]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[27]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[27]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[27]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[27]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[28]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[28]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[28]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[28]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[28]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[28]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[28]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[29]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[29]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[29]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[29]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[29]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[29]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[29]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[2]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[2]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[2]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[2]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[2]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[2]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[2]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[30]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[30]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[30]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[30]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[30]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[30]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[30]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_11_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[5]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[5]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[5]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[5]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[5]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[5]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[5]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[6]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[6]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[6]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[6]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[6]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[6]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[6]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[8]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[8]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[8]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[8]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[8]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[8]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[8]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[9]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[9]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[9]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[9]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[9]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[9]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[9]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[0]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[10]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[10]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[11]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[11]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[12]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[12]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[13]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[13]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[14]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[14]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[15]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[15]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[16]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[16]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[17]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[17]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[18]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[18]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[19]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[19]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[1]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[1]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[20]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[20]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[21]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[21]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[22]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[22]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[23]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[23]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[24]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[24]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[25]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[25]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[26]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[26]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[27]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[27]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[28]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[28]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[29]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[29]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[2]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[2]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[30]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[30]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[31]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[31]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[3]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[3]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[4]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[4]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[5]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[5]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[6]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[6]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[7]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[7]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[8]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[8]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[9]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[9]_i_4_n_0\ : STD_LOGIC;
  signal axi_wready0 : STD_LOGIC;
  signal datapath_n_15 : STD_LOGIC;
  signal datapath_n_16 : STD_LOGIC;
  signal datapath_n_17 : STD_LOGIC;
  signal datapath_n_18 : STD_LOGIC;
  signal datapath_n_19 : STD_LOGIC;
  signal fsm_n_0 : STD_LOGIC;
  signal fsm_n_4 : STD_LOGIC;
  signal p_0_in : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal p_1_in : STD_LOGIC_VECTOR ( 31 downto 7 );
  signal \reg_data_out__0\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \^s00_axi_arready\ : STD_LOGIC;
  signal \^s00_axi_awready\ : STD_LOGIC;
  signal \^s00_axi_rvalid\ : STD_LOGIC;
  signal \^s00_axi_wready\ : STD_LOGIC;
  signal sel0 : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal slv_reg0 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal slv_reg1 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal slv_reg10 : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \slv_reg10[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg10[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg10[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg10[7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg10__0\ : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal slv_reg12 : STD_LOGIC_VECTOR ( 31 downto 1 );
  signal \slv_reg12[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg12[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg12[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg12[7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg12__0\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \slv_reg1[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg1[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg1[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg1[7]_i_1_n_0\ : STD_LOGIC;
  signal slv_reg2 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal slv_reg20 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg20[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg20[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg20[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg20[7]_i_1_n_0\ : STD_LOGIC;
  signal slv_reg21 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg21[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg21[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg21[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg21[7]_i_1_n_0\ : STD_LOGIC;
  signal slv_reg22 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg22[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg22[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg22[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg22[7]_i_1_n_0\ : STD_LOGIC;
  signal slv_reg23 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg23[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg23[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg23[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg23[7]_i_1_n_0\ : STD_LOGIC;
  signal slv_reg24 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg24[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg24[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg24[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg24[7]_i_1_n_0\ : STD_LOGIC;
  signal slv_reg25 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg25[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg25[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg25[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg25[7]_i_1_n_0\ : STD_LOGIC;
  signal slv_reg26 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg26[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg26[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg26[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg26[7]_i_1_n_0\ : STD_LOGIC;
  signal slv_reg27 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg27[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg27[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg27[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg27[7]_i_1_n_0\ : STD_LOGIC;
  signal slv_reg28 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg28[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg28[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg28[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg28[7]_i_1_n_0\ : STD_LOGIC;
  signal slv_reg29 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg29[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg29[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg29[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg29[7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg2[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg2[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg2[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg2[7]_i_1_n_0\ : STD_LOGIC;
  signal slv_reg3 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal slv_reg30 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg30[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg30[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg30[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg30[7]_i_1_n_0\ : STD_LOGIC;
  signal slv_reg31 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg31[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg31[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg31[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg31[7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg3[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg3[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg3[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg3[7]_i_1_n_0\ : STD_LOGIC;
  signal slv_reg4 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg4[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg4[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg4[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg4[7]_i_1_n_0\ : STD_LOGIC;
  signal slv_reg7 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \slv_reg7[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg7[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg7[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg7[31]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg7[7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg7__0\ : STD_LOGIC_VECTOR ( 31 downto 1 );
  signal slv_reg8 : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal \slv_reg8[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg8[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg8[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg8[31]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg8[7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg8__0\ : STD_LOGIC_VECTOR ( 31 downto 10 );
  signal slv_reg9 : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \slv_reg9[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg9[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg9[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg9[7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg9__0\ : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal slv_reg_rden : STD_LOGIC;
  signal sw1 : STD_LOGIC;
  signal sw14_in : STD_LOGIC;
  signal w_cw : STD_LOGIC_VECTOR ( 2 downto 1 );
  signal w_sw : STD_LOGIC_VECTOR ( 2 downto 0 );
  attribute ORIG_CELL_NAME : string;
  attribute ORIG_CELL_NAME of \axi_araddr_reg[2]\ : label is "axi_araddr_reg[2]";
  attribute ORIG_CELL_NAME of \axi_araddr_reg[2]_rep\ : label is "axi_araddr_reg[2]";
  attribute ORIG_CELL_NAME of \axi_araddr_reg[3]\ : label is "axi_araddr_reg[3]";
  attribute ORIG_CELL_NAME of \axi_araddr_reg[3]_rep\ : label is "axi_araddr_reg[3]";
begin
  s00_axi_arready <= \^s00_axi_arready\;
  s00_axi_awready <= \^s00_axi_awready\;
  s00_axi_rvalid <= \^s00_axi_rvalid\;
  s00_axi_wready <= \^s00_axi_wready\;
\axi_araddr_reg[2]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => axi_arready0,
      D => s00_axi_araddr(0),
      Q => sel0(0),
      S => RST
    );
\axi_araddr_reg[2]_rep\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => axi_arready0,
      D => s00_axi_araddr(0),
      Q => \axi_araddr_reg[2]_rep_n_0\,
      S => RST
    );
\axi_araddr_reg[3]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => axi_arready0,
      D => s00_axi_araddr(1),
      Q => sel0(1),
      S => RST
    );
\axi_araddr_reg[3]_rep\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => axi_arready0,
      D => s00_axi_araddr(1),
      Q => \axi_araddr_reg[3]_rep_n_0\,
      S => RST
    );
\axi_araddr_reg[4]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => axi_arready0,
      D => s00_axi_araddr(2),
      Q => sel0(2),
      S => RST
    );
\axi_araddr_reg[5]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => axi_arready0,
      D => s00_axi_araddr(3),
      Q => sel0(3),
      S => RST
    );
\axi_araddr_reg[6]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => axi_arready0,
      D => s00_axi_araddr(4),
      Q => sel0(4),
      S => RST
    );
axi_arready_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => s00_axi_arvalid,
      I1 => \^s00_axi_arready\,
      O => axi_arready0
    );
axi_arready_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_arready0,
      Q => \^s00_axi_arready\,
      R => RST
    );
\axi_awaddr_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => axi_awready0,
      D => s00_axi_awaddr(0),
      Q => p_0_in(0),
      R => RST
    );
\axi_awaddr_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => axi_awready0,
      D => s00_axi_awaddr(1),
      Q => p_0_in(1),
      R => RST
    );
\axi_awaddr_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => axi_awready0,
      D => s00_axi_awaddr(2),
      Q => p_0_in(2),
      R => RST
    );
\axi_awaddr_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => axi_awready0,
      D => s00_axi_awaddr(3),
      Q => p_0_in(3),
      R => RST
    );
\axi_awaddr_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => axi_awready0,
      D => s00_axi_awaddr(4),
      Q => p_0_in(4),
      R => RST
    );
axi_awready_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => s00_axi_wvalid,
      I1 => s00_axi_awvalid,
      I2 => \^s00_axi_awready\,
      O => axi_awready0
    );
axi_awready_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_awready0,
      Q => \^s00_axi_awready\,
      R => RST
    );
axi_bvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_bvalid_reg_0,
      Q => s00_axi_bvalid,
      R => RST
    );
\axi_rdata[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B888FFFFB8880000"
    )
        port map (
      I0 => \axi_rdata_reg[0]_i_2_n_0\,
      I1 => sel0(3),
      I2 => sel0(2),
      I3 => \axi_rdata[0]_i_3_n_0\,
      I4 => sel0(4),
      I5 => \axi_rdata_reg[0]_i_4_n_0\,
      O => \reg_data_out__0\(0)
    );
\axi_rdata[0]_i_10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => slv_reg10(0),
      I1 => \axi_araddr_reg[3]_rep_n_0\,
      I2 => slv_reg9(0),
      I3 => \axi_araddr_reg[2]_rep_n_0\,
      I4 => slv_reg8(0),
      O => \axi_rdata[0]_i_10_n_0\
    );
\axi_rdata[0]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg23(0),
      I1 => slv_reg22(0),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg21(0),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg20(0),
      O => \axi_rdata[0]_i_3_n_0\
    );
\axi_rdata[0]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg27(0),
      I1 => slv_reg26(0),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg25(0),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg24(0),
      O => \axi_rdata[0]_i_5_n_0\
    );
\axi_rdata[0]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg31(0),
      I1 => slv_reg30(0),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg29(0),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg28(0),
      O => \axi_rdata[0]_i_6_n_0\
    );
\axi_rdata[0]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8830FFFF88300000"
    )
        port map (
      I0 => slv_reg7(0),
      I1 => \axi_araddr_reg[3]_rep_n_0\,
      I2 => slv_reg4(0),
      I3 => \axi_araddr_reg[2]_rep_n_0\,
      I4 => sel0(2),
      I5 => \axi_rdata[0]_i_9_n_0\,
      O => \axi_rdata[0]_i_7_n_0\
    );
\axi_rdata[0]_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"04FF0400"
    )
        port map (
      I0 => \axi_araddr_reg[2]_rep_n_0\,
      I1 => \slv_reg12__0\(0),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => sel0(2),
      I4 => \axi_rdata[0]_i_10_n_0\,
      O => \axi_rdata[0]_i_8_n_0\
    );
\axi_rdata[0]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(0),
      I1 => slv_reg2(0),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg1(0),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg0(0),
      O => \axi_rdata[0]_i_9_n_0\
    );
\axi_rdata[10]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B888FFFFB8880000"
    )
        port map (
      I0 => \axi_rdata_reg[10]_i_2_n_0\,
      I1 => sel0(3),
      I2 => sel0(2),
      I3 => \axi_rdata[10]_i_3_n_0\,
      I4 => sel0(4),
      I5 => \axi_rdata_reg[10]_i_4_n_0\,
      O => \reg_data_out__0\(10)
    );
\axi_rdata[10]_i_10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => slv_reg10(10),
      I1 => \axi_araddr_reg[3]_rep_n_0\,
      I2 => slv_reg9(10),
      I3 => \axi_araddr_reg[2]_rep_n_0\,
      I4 => \slv_reg8__0\(10),
      O => \axi_rdata[10]_i_10_n_0\
    );
\axi_rdata[10]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg23(10),
      I1 => slv_reg22(10),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg21(10),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg20(10),
      O => \axi_rdata[10]_i_3_n_0\
    );
\axi_rdata[10]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg27(10),
      I1 => slv_reg26(10),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg25(10),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg24(10),
      O => \axi_rdata[10]_i_5_n_0\
    );
\axi_rdata[10]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg31(10),
      I1 => slv_reg30(10),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg29(10),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg28(10),
      O => \axi_rdata[10]_i_6_n_0\
    );
\axi_rdata[10]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8830FFFF88300000"
    )
        port map (
      I0 => \slv_reg7__0\(10),
      I1 => \axi_araddr_reg[3]_rep_n_0\,
      I2 => slv_reg4(10),
      I3 => \axi_araddr_reg[2]_rep_n_0\,
      I4 => sel0(2),
      I5 => \axi_rdata[10]_i_9_n_0\,
      O => \axi_rdata[10]_i_7_n_0\
    );
\axi_rdata[10]_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"04FF0400"
    )
        port map (
      I0 => \axi_araddr_reg[2]_rep_n_0\,
      I1 => slv_reg12(10),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => sel0(2),
      I4 => \axi_rdata[10]_i_10_n_0\,
      O => \axi_rdata[10]_i_8_n_0\
    );
\axi_rdata[10]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(10),
      I1 => slv_reg2(10),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg1(10),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg0(10),
      O => \axi_rdata[10]_i_9_n_0\
    );
\axi_rdata[11]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B888FFFFB8880000"
    )
        port map (
      I0 => \axi_rdata_reg[11]_i_2_n_0\,
      I1 => sel0(3),
      I2 => sel0(2),
      I3 => \axi_rdata[11]_i_3_n_0\,
      I4 => sel0(4),
      I5 => \axi_rdata_reg[11]_i_4_n_0\,
      O => \reg_data_out__0\(11)
    );
\axi_rdata[11]_i_10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => slv_reg10(11),
      I1 => \axi_araddr_reg[3]_rep_n_0\,
      I2 => slv_reg9(11),
      I3 => \axi_araddr_reg[2]_rep_n_0\,
      I4 => \slv_reg8__0\(11),
      O => \axi_rdata[11]_i_10_n_0\
    );
\axi_rdata[11]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg23(11),
      I1 => slv_reg22(11),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg21(11),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg20(11),
      O => \axi_rdata[11]_i_3_n_0\
    );
\axi_rdata[11]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg27(11),
      I1 => slv_reg26(11),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg25(11),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg24(11),
      O => \axi_rdata[11]_i_5_n_0\
    );
\axi_rdata[11]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg31(11),
      I1 => slv_reg30(11),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg29(11),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg28(11),
      O => \axi_rdata[11]_i_6_n_0\
    );
\axi_rdata[11]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8830FFFF88300000"
    )
        port map (
      I0 => \slv_reg7__0\(11),
      I1 => \axi_araddr_reg[3]_rep_n_0\,
      I2 => slv_reg4(11),
      I3 => \axi_araddr_reg[2]_rep_n_0\,
      I4 => sel0(2),
      I5 => \axi_rdata[11]_i_9_n_0\,
      O => \axi_rdata[11]_i_7_n_0\
    );
\axi_rdata[11]_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"04FF0400"
    )
        port map (
      I0 => \axi_araddr_reg[2]_rep_n_0\,
      I1 => slv_reg12(11),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => sel0(2),
      I4 => \axi_rdata[11]_i_10_n_0\,
      O => \axi_rdata[11]_i_8_n_0\
    );
\axi_rdata[11]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(11),
      I1 => slv_reg2(11),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg1(11),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg0(11),
      O => \axi_rdata[11]_i_9_n_0\
    );
\axi_rdata[12]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B888FFFFB8880000"
    )
        port map (
      I0 => \axi_rdata_reg[12]_i_2_n_0\,
      I1 => sel0(3),
      I2 => sel0(2),
      I3 => \axi_rdata[12]_i_3_n_0\,
      I4 => sel0(4),
      I5 => \axi_rdata_reg[12]_i_4_n_0\,
      O => \reg_data_out__0\(12)
    );
\axi_rdata[12]_i_10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => slv_reg10(12),
      I1 => \axi_araddr_reg[3]_rep_n_0\,
      I2 => slv_reg9(12),
      I3 => \axi_araddr_reg[2]_rep_n_0\,
      I4 => \slv_reg8__0\(12),
      O => \axi_rdata[12]_i_10_n_0\
    );
\axi_rdata[12]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg23(12),
      I1 => slv_reg22(12),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg21(12),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg20(12),
      O => \axi_rdata[12]_i_3_n_0\
    );
\axi_rdata[12]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg27(12),
      I1 => slv_reg26(12),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg25(12),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg24(12),
      O => \axi_rdata[12]_i_5_n_0\
    );
\axi_rdata[12]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg31(12),
      I1 => slv_reg30(12),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg29(12),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg28(12),
      O => \axi_rdata[12]_i_6_n_0\
    );
\axi_rdata[12]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8830FFFF88300000"
    )
        port map (
      I0 => \slv_reg7__0\(12),
      I1 => \axi_araddr_reg[3]_rep_n_0\,
      I2 => slv_reg4(12),
      I3 => \axi_araddr_reg[2]_rep_n_0\,
      I4 => sel0(2),
      I5 => \axi_rdata[12]_i_9_n_0\,
      O => \axi_rdata[12]_i_7_n_0\
    );
\axi_rdata[12]_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"04FF0400"
    )
        port map (
      I0 => \axi_araddr_reg[2]_rep_n_0\,
      I1 => slv_reg12(12),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => sel0(2),
      I4 => \axi_rdata[12]_i_10_n_0\,
      O => \axi_rdata[12]_i_8_n_0\
    );
\axi_rdata[12]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(12),
      I1 => slv_reg2(12),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg1(12),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg0(12),
      O => \axi_rdata[12]_i_9_n_0\
    );
\axi_rdata[13]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B888FFFFB8880000"
    )
        port map (
      I0 => \axi_rdata_reg[13]_i_2_n_0\,
      I1 => sel0(3),
      I2 => sel0(2),
      I3 => \axi_rdata[13]_i_3_n_0\,
      I4 => sel0(4),
      I5 => \axi_rdata_reg[13]_i_4_n_0\,
      O => \reg_data_out__0\(13)
    );
\axi_rdata[13]_i_10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => slv_reg10(13),
      I1 => sel0(1),
      I2 => slv_reg9(13),
      I3 => sel0(0),
      I4 => \slv_reg8__0\(13),
      O => \axi_rdata[13]_i_10_n_0\
    );
\axi_rdata[13]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg23(13),
      I1 => slv_reg22(13),
      I2 => sel0(1),
      I3 => slv_reg21(13),
      I4 => sel0(0),
      I5 => slv_reg20(13),
      O => \axi_rdata[13]_i_3_n_0\
    );
\axi_rdata[13]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg27(13),
      I1 => slv_reg26(13),
      I2 => sel0(1),
      I3 => slv_reg25(13),
      I4 => sel0(0),
      I5 => slv_reg24(13),
      O => \axi_rdata[13]_i_5_n_0\
    );
\axi_rdata[13]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg31(13),
      I1 => slv_reg30(13),
      I2 => sel0(1),
      I3 => slv_reg29(13),
      I4 => sel0(0),
      I5 => slv_reg28(13),
      O => \axi_rdata[13]_i_6_n_0\
    );
\axi_rdata[13]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8830FFFF88300000"
    )
        port map (
      I0 => \slv_reg7__0\(13),
      I1 => sel0(1),
      I2 => slv_reg4(13),
      I3 => sel0(0),
      I4 => sel0(2),
      I5 => \axi_rdata[13]_i_9_n_0\,
      O => \axi_rdata[13]_i_7_n_0\
    );
\axi_rdata[13]_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"04FF0400"
    )
        port map (
      I0 => sel0(0),
      I1 => slv_reg12(13),
      I2 => sel0(1),
      I3 => sel0(2),
      I4 => \axi_rdata[13]_i_10_n_0\,
      O => \axi_rdata[13]_i_8_n_0\
    );
\axi_rdata[13]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(13),
      I1 => slv_reg2(13),
      I2 => sel0(1),
      I3 => slv_reg1(13),
      I4 => sel0(0),
      I5 => slv_reg0(13),
      O => \axi_rdata[13]_i_9_n_0\
    );
\axi_rdata[14]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B888FFFFB8880000"
    )
        port map (
      I0 => \axi_rdata_reg[14]_i_2_n_0\,
      I1 => sel0(3),
      I2 => sel0(2),
      I3 => \axi_rdata[14]_i_3_n_0\,
      I4 => sel0(4),
      I5 => \axi_rdata_reg[14]_i_4_n_0\,
      O => \reg_data_out__0\(14)
    );
\axi_rdata[14]_i_10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => slv_reg10(14),
      I1 => sel0(1),
      I2 => slv_reg9(14),
      I3 => sel0(0),
      I4 => \slv_reg8__0\(14),
      O => \axi_rdata[14]_i_10_n_0\
    );
\axi_rdata[14]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg23(14),
      I1 => slv_reg22(14),
      I2 => sel0(1),
      I3 => slv_reg21(14),
      I4 => sel0(0),
      I5 => slv_reg20(14),
      O => \axi_rdata[14]_i_3_n_0\
    );
\axi_rdata[14]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg27(14),
      I1 => slv_reg26(14),
      I2 => sel0(1),
      I3 => slv_reg25(14),
      I4 => sel0(0),
      I5 => slv_reg24(14),
      O => \axi_rdata[14]_i_5_n_0\
    );
\axi_rdata[14]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg31(14),
      I1 => slv_reg30(14),
      I2 => sel0(1),
      I3 => slv_reg29(14),
      I4 => sel0(0),
      I5 => slv_reg28(14),
      O => \axi_rdata[14]_i_6_n_0\
    );
\axi_rdata[14]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8830FFFF88300000"
    )
        port map (
      I0 => \slv_reg7__0\(14),
      I1 => sel0(1),
      I2 => slv_reg4(14),
      I3 => sel0(0),
      I4 => sel0(2),
      I5 => \axi_rdata[14]_i_9_n_0\,
      O => \axi_rdata[14]_i_7_n_0\
    );
\axi_rdata[14]_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"04FF0400"
    )
        port map (
      I0 => sel0(0),
      I1 => slv_reg12(14),
      I2 => sel0(1),
      I3 => sel0(2),
      I4 => \axi_rdata[14]_i_10_n_0\,
      O => \axi_rdata[14]_i_8_n_0\
    );
\axi_rdata[14]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(14),
      I1 => slv_reg2(14),
      I2 => sel0(1),
      I3 => slv_reg1(14),
      I4 => sel0(0),
      I5 => slv_reg0(14),
      O => \axi_rdata[14]_i_9_n_0\
    );
\axi_rdata[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B888FFFFB8880000"
    )
        port map (
      I0 => \axi_rdata_reg[15]_i_2_n_0\,
      I1 => sel0(3),
      I2 => sel0(2),
      I3 => \axi_rdata[15]_i_3_n_0\,
      I4 => sel0(4),
      I5 => \axi_rdata_reg[15]_i_4_n_0\,
      O => \reg_data_out__0\(15)
    );
\axi_rdata[15]_i_10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => slv_reg10(15),
      I1 => sel0(1),
      I2 => slv_reg9(15),
      I3 => sel0(0),
      I4 => \slv_reg8__0\(15),
      O => \axi_rdata[15]_i_10_n_0\
    );
\axi_rdata[15]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg23(15),
      I1 => slv_reg22(15),
      I2 => sel0(1),
      I3 => slv_reg21(15),
      I4 => sel0(0),
      I5 => slv_reg20(15),
      O => \axi_rdata[15]_i_3_n_0\
    );
\axi_rdata[15]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg27(15),
      I1 => slv_reg26(15),
      I2 => sel0(1),
      I3 => slv_reg25(15),
      I4 => sel0(0),
      I5 => slv_reg24(15),
      O => \axi_rdata[15]_i_5_n_0\
    );
\axi_rdata[15]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg31(15),
      I1 => slv_reg30(15),
      I2 => sel0(1),
      I3 => slv_reg29(15),
      I4 => sel0(0),
      I5 => slv_reg28(15),
      O => \axi_rdata[15]_i_6_n_0\
    );
\axi_rdata[15]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8830FFFF88300000"
    )
        port map (
      I0 => \slv_reg7__0\(15),
      I1 => sel0(1),
      I2 => slv_reg4(15),
      I3 => sel0(0),
      I4 => sel0(2),
      I5 => \axi_rdata[15]_i_9_n_0\,
      O => \axi_rdata[15]_i_7_n_0\
    );
\axi_rdata[15]_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"04FF0400"
    )
        port map (
      I0 => sel0(0),
      I1 => slv_reg12(15),
      I2 => sel0(1),
      I3 => sel0(2),
      I4 => \axi_rdata[15]_i_10_n_0\,
      O => \axi_rdata[15]_i_8_n_0\
    );
\axi_rdata[15]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(15),
      I1 => slv_reg2(15),
      I2 => sel0(1),
      I3 => slv_reg1(15),
      I4 => sel0(0),
      I5 => slv_reg0(15),
      O => \axi_rdata[15]_i_9_n_0\
    );
\axi_rdata[16]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B888FFFFB8880000"
    )
        port map (
      I0 => \axi_rdata_reg[16]_i_2_n_0\,
      I1 => sel0(3),
      I2 => sel0(2),
      I3 => \axi_rdata[16]_i_3_n_0\,
      I4 => sel0(4),
      I5 => \axi_rdata_reg[16]_i_4_n_0\,
      O => \reg_data_out__0\(16)
    );
\axi_rdata[16]_i_10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \slv_reg10__0\(16),
      I1 => sel0(1),
      I2 => \slv_reg9__0\(16),
      I3 => sel0(0),
      I4 => \slv_reg8__0\(16),
      O => \axi_rdata[16]_i_10_n_0\
    );
\axi_rdata[16]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg23(16),
      I1 => slv_reg22(16),
      I2 => sel0(1),
      I3 => slv_reg21(16),
      I4 => sel0(0),
      I5 => slv_reg20(16),
      O => \axi_rdata[16]_i_3_n_0\
    );
\axi_rdata[16]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg27(16),
      I1 => slv_reg26(16),
      I2 => sel0(1),
      I3 => slv_reg25(16),
      I4 => sel0(0),
      I5 => slv_reg24(16),
      O => \axi_rdata[16]_i_5_n_0\
    );
\axi_rdata[16]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg31(16),
      I1 => slv_reg30(16),
      I2 => sel0(1),
      I3 => slv_reg29(16),
      I4 => sel0(0),
      I5 => slv_reg28(16),
      O => \axi_rdata[16]_i_6_n_0\
    );
\axi_rdata[16]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8830FFFF88300000"
    )
        port map (
      I0 => \slv_reg7__0\(16),
      I1 => sel0(1),
      I2 => slv_reg4(16),
      I3 => sel0(0),
      I4 => sel0(2),
      I5 => \axi_rdata[16]_i_9_n_0\,
      O => \axi_rdata[16]_i_7_n_0\
    );
\axi_rdata[16]_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"04FF0400"
    )
        port map (
      I0 => sel0(0),
      I1 => slv_reg12(16),
      I2 => sel0(1),
      I3 => sel0(2),
      I4 => \axi_rdata[16]_i_10_n_0\,
      O => \axi_rdata[16]_i_8_n_0\
    );
\axi_rdata[16]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(16),
      I1 => slv_reg2(16),
      I2 => sel0(1),
      I3 => slv_reg1(16),
      I4 => sel0(0),
      I5 => slv_reg0(16),
      O => \axi_rdata[16]_i_9_n_0\
    );
\axi_rdata[17]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B888FFFFB8880000"
    )
        port map (
      I0 => \axi_rdata_reg[17]_i_2_n_0\,
      I1 => sel0(3),
      I2 => sel0(2),
      I3 => \axi_rdata[17]_i_3_n_0\,
      I4 => sel0(4),
      I5 => \axi_rdata_reg[17]_i_4_n_0\,
      O => \reg_data_out__0\(17)
    );
\axi_rdata[17]_i_10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \slv_reg10__0\(17),
      I1 => sel0(1),
      I2 => \slv_reg9__0\(17),
      I3 => sel0(0),
      I4 => \slv_reg8__0\(17),
      O => \axi_rdata[17]_i_10_n_0\
    );
\axi_rdata[17]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg23(17),
      I1 => slv_reg22(17),
      I2 => sel0(1),
      I3 => slv_reg21(17),
      I4 => sel0(0),
      I5 => slv_reg20(17),
      O => \axi_rdata[17]_i_3_n_0\
    );
\axi_rdata[17]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg27(17),
      I1 => slv_reg26(17),
      I2 => sel0(1),
      I3 => slv_reg25(17),
      I4 => sel0(0),
      I5 => slv_reg24(17),
      O => \axi_rdata[17]_i_5_n_0\
    );
\axi_rdata[17]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg31(17),
      I1 => slv_reg30(17),
      I2 => sel0(1),
      I3 => slv_reg29(17),
      I4 => sel0(0),
      I5 => slv_reg28(17),
      O => \axi_rdata[17]_i_6_n_0\
    );
\axi_rdata[17]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8830FFFF88300000"
    )
        port map (
      I0 => \slv_reg7__0\(17),
      I1 => sel0(1),
      I2 => slv_reg4(17),
      I3 => sel0(0),
      I4 => sel0(2),
      I5 => \axi_rdata[17]_i_9_n_0\,
      O => \axi_rdata[17]_i_7_n_0\
    );
\axi_rdata[17]_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"04FF0400"
    )
        port map (
      I0 => sel0(0),
      I1 => slv_reg12(17),
      I2 => sel0(1),
      I3 => sel0(2),
      I4 => \axi_rdata[17]_i_10_n_0\,
      O => \axi_rdata[17]_i_8_n_0\
    );
\axi_rdata[17]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(17),
      I1 => slv_reg2(17),
      I2 => sel0(1),
      I3 => slv_reg1(17),
      I4 => sel0(0),
      I5 => slv_reg0(17),
      O => \axi_rdata[17]_i_9_n_0\
    );
\axi_rdata[18]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B888FFFFB8880000"
    )
        port map (
      I0 => \axi_rdata_reg[18]_i_2_n_0\,
      I1 => sel0(3),
      I2 => sel0(2),
      I3 => \axi_rdata[18]_i_3_n_0\,
      I4 => sel0(4),
      I5 => \axi_rdata_reg[18]_i_4_n_0\,
      O => \reg_data_out__0\(18)
    );
\axi_rdata[18]_i_10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \slv_reg10__0\(18),
      I1 => sel0(1),
      I2 => \slv_reg9__0\(18),
      I3 => sel0(0),
      I4 => \slv_reg8__0\(18),
      O => \axi_rdata[18]_i_10_n_0\
    );
\axi_rdata[18]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg23(18),
      I1 => slv_reg22(18),
      I2 => sel0(1),
      I3 => slv_reg21(18),
      I4 => sel0(0),
      I5 => slv_reg20(18),
      O => \axi_rdata[18]_i_3_n_0\
    );
\axi_rdata[18]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg27(18),
      I1 => slv_reg26(18),
      I2 => sel0(1),
      I3 => slv_reg25(18),
      I4 => sel0(0),
      I5 => slv_reg24(18),
      O => \axi_rdata[18]_i_5_n_0\
    );
\axi_rdata[18]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg31(18),
      I1 => slv_reg30(18),
      I2 => sel0(1),
      I3 => slv_reg29(18),
      I4 => sel0(0),
      I5 => slv_reg28(18),
      O => \axi_rdata[18]_i_6_n_0\
    );
\axi_rdata[18]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8830FFFF88300000"
    )
        port map (
      I0 => \slv_reg7__0\(18),
      I1 => sel0(1),
      I2 => slv_reg4(18),
      I3 => sel0(0),
      I4 => sel0(2),
      I5 => \axi_rdata[18]_i_9_n_0\,
      O => \axi_rdata[18]_i_7_n_0\
    );
\axi_rdata[18]_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"04FF0400"
    )
        port map (
      I0 => sel0(0),
      I1 => slv_reg12(18),
      I2 => sel0(1),
      I3 => sel0(2),
      I4 => \axi_rdata[18]_i_10_n_0\,
      O => \axi_rdata[18]_i_8_n_0\
    );
\axi_rdata[18]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(18),
      I1 => slv_reg2(18),
      I2 => sel0(1),
      I3 => slv_reg1(18),
      I4 => sel0(0),
      I5 => slv_reg0(18),
      O => \axi_rdata[18]_i_9_n_0\
    );
\axi_rdata[19]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B888FFFFB8880000"
    )
        port map (
      I0 => \axi_rdata_reg[19]_i_2_n_0\,
      I1 => sel0(3),
      I2 => sel0(2),
      I3 => \axi_rdata[19]_i_3_n_0\,
      I4 => sel0(4),
      I5 => \axi_rdata_reg[19]_i_4_n_0\,
      O => \reg_data_out__0\(19)
    );
\axi_rdata[19]_i_10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \slv_reg10__0\(19),
      I1 => sel0(1),
      I2 => \slv_reg9__0\(19),
      I3 => sel0(0),
      I4 => \slv_reg8__0\(19),
      O => \axi_rdata[19]_i_10_n_0\
    );
\axi_rdata[19]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg23(19),
      I1 => slv_reg22(19),
      I2 => sel0(1),
      I3 => slv_reg21(19),
      I4 => sel0(0),
      I5 => slv_reg20(19),
      O => \axi_rdata[19]_i_3_n_0\
    );
\axi_rdata[19]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg27(19),
      I1 => slv_reg26(19),
      I2 => sel0(1),
      I3 => slv_reg25(19),
      I4 => sel0(0),
      I5 => slv_reg24(19),
      O => \axi_rdata[19]_i_5_n_0\
    );
\axi_rdata[19]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg31(19),
      I1 => slv_reg30(19),
      I2 => sel0(1),
      I3 => slv_reg29(19),
      I4 => sel0(0),
      I5 => slv_reg28(19),
      O => \axi_rdata[19]_i_6_n_0\
    );
\axi_rdata[19]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8830FFFF88300000"
    )
        port map (
      I0 => \slv_reg7__0\(19),
      I1 => sel0(1),
      I2 => slv_reg4(19),
      I3 => sel0(0),
      I4 => sel0(2),
      I5 => \axi_rdata[19]_i_9_n_0\,
      O => \axi_rdata[19]_i_7_n_0\
    );
\axi_rdata[19]_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"04FF0400"
    )
        port map (
      I0 => sel0(0),
      I1 => slv_reg12(19),
      I2 => sel0(1),
      I3 => sel0(2),
      I4 => \axi_rdata[19]_i_10_n_0\,
      O => \axi_rdata[19]_i_8_n_0\
    );
\axi_rdata[19]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(19),
      I1 => slv_reg2(19),
      I2 => sel0(1),
      I3 => slv_reg1(19),
      I4 => sel0(0),
      I5 => slv_reg0(19),
      O => \axi_rdata[19]_i_9_n_0\
    );
\axi_rdata[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B888FFFFB8880000"
    )
        port map (
      I0 => \axi_rdata_reg[1]_i_2_n_0\,
      I1 => sel0(3),
      I2 => sel0(2),
      I3 => \axi_rdata[1]_i_3_n_0\,
      I4 => sel0(4),
      I5 => \axi_rdata_reg[1]_i_4_n_0\,
      O => \reg_data_out__0\(1)
    );
\axi_rdata[1]_i_10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => slv_reg10(1),
      I1 => \axi_araddr_reg[3]_rep_n_0\,
      I2 => slv_reg9(1),
      I3 => \axi_araddr_reg[2]_rep_n_0\,
      I4 => slv_reg8(1),
      O => \axi_rdata[1]_i_10_n_0\
    );
\axi_rdata[1]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg23(1),
      I1 => slv_reg22(1),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg21(1),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg20(1),
      O => \axi_rdata[1]_i_3_n_0\
    );
\axi_rdata[1]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg27(1),
      I1 => slv_reg26(1),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg25(1),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg24(1),
      O => \axi_rdata[1]_i_5_n_0\
    );
\axi_rdata[1]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg31(1),
      I1 => slv_reg30(1),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg29(1),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg28(1),
      O => \axi_rdata[1]_i_6_n_0\
    );
\axi_rdata[1]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8830FFFF88300000"
    )
        port map (
      I0 => \slv_reg7__0\(1),
      I1 => \axi_araddr_reg[3]_rep_n_0\,
      I2 => slv_reg4(1),
      I3 => \axi_araddr_reg[2]_rep_n_0\,
      I4 => sel0(2),
      I5 => \axi_rdata[1]_i_9_n_0\,
      O => \axi_rdata[1]_i_7_n_0\
    );
\axi_rdata[1]_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"04FF0400"
    )
        port map (
      I0 => \axi_araddr_reg[2]_rep_n_0\,
      I1 => slv_reg12(1),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => sel0(2),
      I4 => \axi_rdata[1]_i_10_n_0\,
      O => \axi_rdata[1]_i_8_n_0\
    );
\axi_rdata[1]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(1),
      I1 => slv_reg2(1),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg1(1),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg0(1),
      O => \axi_rdata[1]_i_9_n_0\
    );
\axi_rdata[20]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B888FFFFB8880000"
    )
        port map (
      I0 => \axi_rdata_reg[20]_i_2_n_0\,
      I1 => sel0(3),
      I2 => sel0(2),
      I3 => \axi_rdata[20]_i_3_n_0\,
      I4 => sel0(4),
      I5 => \axi_rdata_reg[20]_i_4_n_0\,
      O => \reg_data_out__0\(20)
    );
\axi_rdata[20]_i_10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \slv_reg10__0\(20),
      I1 => sel0(1),
      I2 => \slv_reg9__0\(20),
      I3 => sel0(0),
      I4 => \slv_reg8__0\(20),
      O => \axi_rdata[20]_i_10_n_0\
    );
\axi_rdata[20]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg23(20),
      I1 => slv_reg22(20),
      I2 => sel0(1),
      I3 => slv_reg21(20),
      I4 => sel0(0),
      I5 => slv_reg20(20),
      O => \axi_rdata[20]_i_3_n_0\
    );
\axi_rdata[20]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg27(20),
      I1 => slv_reg26(20),
      I2 => sel0(1),
      I3 => slv_reg25(20),
      I4 => sel0(0),
      I5 => slv_reg24(20),
      O => \axi_rdata[20]_i_5_n_0\
    );
\axi_rdata[20]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg31(20),
      I1 => slv_reg30(20),
      I2 => sel0(1),
      I3 => slv_reg29(20),
      I4 => sel0(0),
      I5 => slv_reg28(20),
      O => \axi_rdata[20]_i_6_n_0\
    );
\axi_rdata[20]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8830FFFF88300000"
    )
        port map (
      I0 => \slv_reg7__0\(20),
      I1 => sel0(1),
      I2 => slv_reg4(20),
      I3 => sel0(0),
      I4 => sel0(2),
      I5 => \axi_rdata[20]_i_9_n_0\,
      O => \axi_rdata[20]_i_7_n_0\
    );
\axi_rdata[20]_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"04FF0400"
    )
        port map (
      I0 => sel0(0),
      I1 => slv_reg12(20),
      I2 => sel0(1),
      I3 => sel0(2),
      I4 => \axi_rdata[20]_i_10_n_0\,
      O => \axi_rdata[20]_i_8_n_0\
    );
\axi_rdata[20]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(20),
      I1 => slv_reg2(20),
      I2 => sel0(1),
      I3 => slv_reg1(20),
      I4 => sel0(0),
      I5 => slv_reg0(20),
      O => \axi_rdata[20]_i_9_n_0\
    );
\axi_rdata[21]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B888FFFFB8880000"
    )
        port map (
      I0 => \axi_rdata_reg[21]_i_2_n_0\,
      I1 => sel0(3),
      I2 => sel0(2),
      I3 => \axi_rdata[21]_i_3_n_0\,
      I4 => sel0(4),
      I5 => \axi_rdata_reg[21]_i_4_n_0\,
      O => \reg_data_out__0\(21)
    );
\axi_rdata[21]_i_10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \slv_reg10__0\(21),
      I1 => sel0(1),
      I2 => \slv_reg9__0\(21),
      I3 => sel0(0),
      I4 => \slv_reg8__0\(21),
      O => \axi_rdata[21]_i_10_n_0\
    );
\axi_rdata[21]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg23(21),
      I1 => slv_reg22(21),
      I2 => sel0(1),
      I3 => slv_reg21(21),
      I4 => sel0(0),
      I5 => slv_reg20(21),
      O => \axi_rdata[21]_i_3_n_0\
    );
\axi_rdata[21]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg27(21),
      I1 => slv_reg26(21),
      I2 => sel0(1),
      I3 => slv_reg25(21),
      I4 => sel0(0),
      I5 => slv_reg24(21),
      O => \axi_rdata[21]_i_5_n_0\
    );
\axi_rdata[21]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg31(21),
      I1 => slv_reg30(21),
      I2 => sel0(1),
      I3 => slv_reg29(21),
      I4 => sel0(0),
      I5 => slv_reg28(21),
      O => \axi_rdata[21]_i_6_n_0\
    );
\axi_rdata[21]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8830FFFF88300000"
    )
        port map (
      I0 => \slv_reg7__0\(21),
      I1 => sel0(1),
      I2 => slv_reg4(21),
      I3 => sel0(0),
      I4 => sel0(2),
      I5 => \axi_rdata[21]_i_9_n_0\,
      O => \axi_rdata[21]_i_7_n_0\
    );
\axi_rdata[21]_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"04FF0400"
    )
        port map (
      I0 => sel0(0),
      I1 => slv_reg12(21),
      I2 => sel0(1),
      I3 => sel0(2),
      I4 => \axi_rdata[21]_i_10_n_0\,
      O => \axi_rdata[21]_i_8_n_0\
    );
\axi_rdata[21]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(21),
      I1 => slv_reg2(21),
      I2 => sel0(1),
      I3 => slv_reg1(21),
      I4 => sel0(0),
      I5 => slv_reg0(21),
      O => \axi_rdata[21]_i_9_n_0\
    );
\axi_rdata[22]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B888FFFFB8880000"
    )
        port map (
      I0 => \axi_rdata_reg[22]_i_2_n_0\,
      I1 => sel0(3),
      I2 => sel0(2),
      I3 => \axi_rdata[22]_i_3_n_0\,
      I4 => sel0(4),
      I5 => \axi_rdata_reg[22]_i_4_n_0\,
      O => \reg_data_out__0\(22)
    );
\axi_rdata[22]_i_10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \slv_reg10__0\(22),
      I1 => sel0(1),
      I2 => \slv_reg9__0\(22),
      I3 => sel0(0),
      I4 => \slv_reg8__0\(22),
      O => \axi_rdata[22]_i_10_n_0\
    );
\axi_rdata[22]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg23(22),
      I1 => slv_reg22(22),
      I2 => sel0(1),
      I3 => slv_reg21(22),
      I4 => sel0(0),
      I5 => slv_reg20(22),
      O => \axi_rdata[22]_i_3_n_0\
    );
\axi_rdata[22]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg27(22),
      I1 => slv_reg26(22),
      I2 => sel0(1),
      I3 => slv_reg25(22),
      I4 => sel0(0),
      I5 => slv_reg24(22),
      O => \axi_rdata[22]_i_5_n_0\
    );
\axi_rdata[22]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg31(22),
      I1 => slv_reg30(22),
      I2 => sel0(1),
      I3 => slv_reg29(22),
      I4 => sel0(0),
      I5 => slv_reg28(22),
      O => \axi_rdata[22]_i_6_n_0\
    );
\axi_rdata[22]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8830FFFF88300000"
    )
        port map (
      I0 => \slv_reg7__0\(22),
      I1 => sel0(1),
      I2 => slv_reg4(22),
      I3 => sel0(0),
      I4 => sel0(2),
      I5 => \axi_rdata[22]_i_9_n_0\,
      O => \axi_rdata[22]_i_7_n_0\
    );
\axi_rdata[22]_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"04FF0400"
    )
        port map (
      I0 => sel0(0),
      I1 => slv_reg12(22),
      I2 => sel0(1),
      I3 => sel0(2),
      I4 => \axi_rdata[22]_i_10_n_0\,
      O => \axi_rdata[22]_i_8_n_0\
    );
\axi_rdata[22]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(22),
      I1 => slv_reg2(22),
      I2 => sel0(1),
      I3 => slv_reg1(22),
      I4 => sel0(0),
      I5 => slv_reg0(22),
      O => \axi_rdata[22]_i_9_n_0\
    );
\axi_rdata[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B888FFFFB8880000"
    )
        port map (
      I0 => \axi_rdata_reg[23]_i_2_n_0\,
      I1 => sel0(3),
      I2 => sel0(2),
      I3 => \axi_rdata[23]_i_3_n_0\,
      I4 => sel0(4),
      I5 => \axi_rdata_reg[23]_i_4_n_0\,
      O => \reg_data_out__0\(23)
    );
\axi_rdata[23]_i_10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \slv_reg10__0\(23),
      I1 => sel0(1),
      I2 => \slv_reg9__0\(23),
      I3 => sel0(0),
      I4 => \slv_reg8__0\(23),
      O => \axi_rdata[23]_i_10_n_0\
    );
\axi_rdata[23]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg23(23),
      I1 => slv_reg22(23),
      I2 => sel0(1),
      I3 => slv_reg21(23),
      I4 => sel0(0),
      I5 => slv_reg20(23),
      O => \axi_rdata[23]_i_3_n_0\
    );
\axi_rdata[23]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg27(23),
      I1 => slv_reg26(23),
      I2 => sel0(1),
      I3 => slv_reg25(23),
      I4 => sel0(0),
      I5 => slv_reg24(23),
      O => \axi_rdata[23]_i_5_n_0\
    );
\axi_rdata[23]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg31(23),
      I1 => slv_reg30(23),
      I2 => sel0(1),
      I3 => slv_reg29(23),
      I4 => sel0(0),
      I5 => slv_reg28(23),
      O => \axi_rdata[23]_i_6_n_0\
    );
\axi_rdata[23]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8830FFFF88300000"
    )
        port map (
      I0 => \slv_reg7__0\(23),
      I1 => sel0(1),
      I2 => slv_reg4(23),
      I3 => sel0(0),
      I4 => sel0(2),
      I5 => \axi_rdata[23]_i_9_n_0\,
      O => \axi_rdata[23]_i_7_n_0\
    );
\axi_rdata[23]_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"04FF0400"
    )
        port map (
      I0 => sel0(0),
      I1 => slv_reg12(23),
      I2 => sel0(1),
      I3 => sel0(2),
      I4 => \axi_rdata[23]_i_10_n_0\,
      O => \axi_rdata[23]_i_8_n_0\
    );
\axi_rdata[23]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(23),
      I1 => slv_reg2(23),
      I2 => sel0(1),
      I3 => slv_reg1(23),
      I4 => sel0(0),
      I5 => slv_reg0(23),
      O => \axi_rdata[23]_i_9_n_0\
    );
\axi_rdata[24]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B888FFFFB8880000"
    )
        port map (
      I0 => \axi_rdata_reg[24]_i_2_n_0\,
      I1 => sel0(3),
      I2 => sel0(2),
      I3 => \axi_rdata[24]_i_3_n_0\,
      I4 => sel0(4),
      I5 => \axi_rdata_reg[24]_i_4_n_0\,
      O => \reg_data_out__0\(24)
    );
\axi_rdata[24]_i_10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \slv_reg10__0\(24),
      I1 => sel0(1),
      I2 => \slv_reg9__0\(24),
      I3 => sel0(0),
      I4 => \slv_reg8__0\(24),
      O => \axi_rdata[24]_i_10_n_0\
    );
\axi_rdata[24]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg23(24),
      I1 => slv_reg22(24),
      I2 => sel0(1),
      I3 => slv_reg21(24),
      I4 => sel0(0),
      I5 => slv_reg20(24),
      O => \axi_rdata[24]_i_3_n_0\
    );
\axi_rdata[24]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg27(24),
      I1 => slv_reg26(24),
      I2 => sel0(1),
      I3 => slv_reg25(24),
      I4 => sel0(0),
      I5 => slv_reg24(24),
      O => \axi_rdata[24]_i_5_n_0\
    );
\axi_rdata[24]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg31(24),
      I1 => slv_reg30(24),
      I2 => sel0(1),
      I3 => slv_reg29(24),
      I4 => sel0(0),
      I5 => slv_reg28(24),
      O => \axi_rdata[24]_i_6_n_0\
    );
\axi_rdata[24]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8830FFFF88300000"
    )
        port map (
      I0 => \slv_reg7__0\(24),
      I1 => sel0(1),
      I2 => slv_reg4(24),
      I3 => sel0(0),
      I4 => sel0(2),
      I5 => \axi_rdata[24]_i_9_n_0\,
      O => \axi_rdata[24]_i_7_n_0\
    );
\axi_rdata[24]_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"04FF0400"
    )
        port map (
      I0 => sel0(0),
      I1 => slv_reg12(24),
      I2 => sel0(1),
      I3 => sel0(2),
      I4 => \axi_rdata[24]_i_10_n_0\,
      O => \axi_rdata[24]_i_8_n_0\
    );
\axi_rdata[24]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(24),
      I1 => slv_reg2(24),
      I2 => sel0(1),
      I3 => slv_reg1(24),
      I4 => sel0(0),
      I5 => slv_reg0(24),
      O => \axi_rdata[24]_i_9_n_0\
    );
\axi_rdata[25]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B888FFFFB8880000"
    )
        port map (
      I0 => \axi_rdata_reg[25]_i_2_n_0\,
      I1 => sel0(3),
      I2 => sel0(2),
      I3 => \axi_rdata[25]_i_3_n_0\,
      I4 => sel0(4),
      I5 => \axi_rdata_reg[25]_i_4_n_0\,
      O => \reg_data_out__0\(25)
    );
\axi_rdata[25]_i_10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \slv_reg10__0\(25),
      I1 => sel0(1),
      I2 => \slv_reg9__0\(25),
      I3 => sel0(0),
      I4 => \slv_reg8__0\(25),
      O => \axi_rdata[25]_i_10_n_0\
    );
\axi_rdata[25]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg23(25),
      I1 => slv_reg22(25),
      I2 => sel0(1),
      I3 => slv_reg21(25),
      I4 => sel0(0),
      I5 => slv_reg20(25),
      O => \axi_rdata[25]_i_3_n_0\
    );
\axi_rdata[25]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg27(25),
      I1 => slv_reg26(25),
      I2 => sel0(1),
      I3 => slv_reg25(25),
      I4 => sel0(0),
      I5 => slv_reg24(25),
      O => \axi_rdata[25]_i_5_n_0\
    );
\axi_rdata[25]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg31(25),
      I1 => slv_reg30(25),
      I2 => sel0(1),
      I3 => slv_reg29(25),
      I4 => sel0(0),
      I5 => slv_reg28(25),
      O => \axi_rdata[25]_i_6_n_0\
    );
\axi_rdata[25]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8830FFFF88300000"
    )
        port map (
      I0 => \slv_reg7__0\(25),
      I1 => sel0(1),
      I2 => slv_reg4(25),
      I3 => sel0(0),
      I4 => sel0(2),
      I5 => \axi_rdata[25]_i_9_n_0\,
      O => \axi_rdata[25]_i_7_n_0\
    );
\axi_rdata[25]_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"04FF0400"
    )
        port map (
      I0 => sel0(0),
      I1 => slv_reg12(25),
      I2 => sel0(1),
      I3 => sel0(2),
      I4 => \axi_rdata[25]_i_10_n_0\,
      O => \axi_rdata[25]_i_8_n_0\
    );
\axi_rdata[25]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(25),
      I1 => slv_reg2(25),
      I2 => sel0(1),
      I3 => slv_reg1(25),
      I4 => sel0(0),
      I5 => slv_reg0(25),
      O => \axi_rdata[25]_i_9_n_0\
    );
\axi_rdata[26]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B888FFFFB8880000"
    )
        port map (
      I0 => \axi_rdata_reg[26]_i_2_n_0\,
      I1 => sel0(3),
      I2 => sel0(2),
      I3 => \axi_rdata[26]_i_3_n_0\,
      I4 => sel0(4),
      I5 => \axi_rdata_reg[26]_i_4_n_0\,
      O => \reg_data_out__0\(26)
    );
\axi_rdata[26]_i_10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \slv_reg10__0\(26),
      I1 => sel0(1),
      I2 => \slv_reg9__0\(26),
      I3 => sel0(0),
      I4 => \slv_reg8__0\(26),
      O => \axi_rdata[26]_i_10_n_0\
    );
\axi_rdata[26]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg23(26),
      I1 => slv_reg22(26),
      I2 => sel0(1),
      I3 => slv_reg21(26),
      I4 => sel0(0),
      I5 => slv_reg20(26),
      O => \axi_rdata[26]_i_3_n_0\
    );
\axi_rdata[26]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg27(26),
      I1 => slv_reg26(26),
      I2 => sel0(1),
      I3 => slv_reg25(26),
      I4 => sel0(0),
      I5 => slv_reg24(26),
      O => \axi_rdata[26]_i_5_n_0\
    );
\axi_rdata[26]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg31(26),
      I1 => slv_reg30(26),
      I2 => sel0(1),
      I3 => slv_reg29(26),
      I4 => sel0(0),
      I5 => slv_reg28(26),
      O => \axi_rdata[26]_i_6_n_0\
    );
\axi_rdata[26]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8830FFFF88300000"
    )
        port map (
      I0 => \slv_reg7__0\(26),
      I1 => sel0(1),
      I2 => slv_reg4(26),
      I3 => sel0(0),
      I4 => sel0(2),
      I5 => \axi_rdata[26]_i_9_n_0\,
      O => \axi_rdata[26]_i_7_n_0\
    );
\axi_rdata[26]_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"04FF0400"
    )
        port map (
      I0 => sel0(0),
      I1 => slv_reg12(26),
      I2 => sel0(1),
      I3 => sel0(2),
      I4 => \axi_rdata[26]_i_10_n_0\,
      O => \axi_rdata[26]_i_8_n_0\
    );
\axi_rdata[26]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(26),
      I1 => slv_reg2(26),
      I2 => sel0(1),
      I3 => slv_reg1(26),
      I4 => sel0(0),
      I5 => slv_reg0(26),
      O => \axi_rdata[26]_i_9_n_0\
    );
\axi_rdata[27]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B888FFFFB8880000"
    )
        port map (
      I0 => \axi_rdata_reg[27]_i_2_n_0\,
      I1 => sel0(3),
      I2 => sel0(2),
      I3 => \axi_rdata[27]_i_3_n_0\,
      I4 => sel0(4),
      I5 => \axi_rdata_reg[27]_i_4_n_0\,
      O => \reg_data_out__0\(27)
    );
\axi_rdata[27]_i_10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \slv_reg10__0\(27),
      I1 => sel0(1),
      I2 => \slv_reg9__0\(27),
      I3 => sel0(0),
      I4 => \slv_reg8__0\(27),
      O => \axi_rdata[27]_i_10_n_0\
    );
\axi_rdata[27]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg23(27),
      I1 => slv_reg22(27),
      I2 => sel0(1),
      I3 => slv_reg21(27),
      I4 => sel0(0),
      I5 => slv_reg20(27),
      O => \axi_rdata[27]_i_3_n_0\
    );
\axi_rdata[27]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg27(27),
      I1 => slv_reg26(27),
      I2 => sel0(1),
      I3 => slv_reg25(27),
      I4 => sel0(0),
      I5 => slv_reg24(27),
      O => \axi_rdata[27]_i_5_n_0\
    );
\axi_rdata[27]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg31(27),
      I1 => slv_reg30(27),
      I2 => sel0(1),
      I3 => slv_reg29(27),
      I4 => sel0(0),
      I5 => slv_reg28(27),
      O => \axi_rdata[27]_i_6_n_0\
    );
\axi_rdata[27]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8830FFFF88300000"
    )
        port map (
      I0 => \slv_reg7__0\(27),
      I1 => sel0(1),
      I2 => slv_reg4(27),
      I3 => sel0(0),
      I4 => sel0(2),
      I5 => \axi_rdata[27]_i_9_n_0\,
      O => \axi_rdata[27]_i_7_n_0\
    );
\axi_rdata[27]_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"04FF0400"
    )
        port map (
      I0 => sel0(0),
      I1 => slv_reg12(27),
      I2 => sel0(1),
      I3 => sel0(2),
      I4 => \axi_rdata[27]_i_10_n_0\,
      O => \axi_rdata[27]_i_8_n_0\
    );
\axi_rdata[27]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(27),
      I1 => slv_reg2(27),
      I2 => sel0(1),
      I3 => slv_reg1(27),
      I4 => sel0(0),
      I5 => slv_reg0(27),
      O => \axi_rdata[27]_i_9_n_0\
    );
\axi_rdata[28]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B888FFFFB8880000"
    )
        port map (
      I0 => \axi_rdata_reg[28]_i_2_n_0\,
      I1 => sel0(3),
      I2 => sel0(2),
      I3 => \axi_rdata[28]_i_3_n_0\,
      I4 => sel0(4),
      I5 => \axi_rdata_reg[28]_i_4_n_0\,
      O => \reg_data_out__0\(28)
    );
\axi_rdata[28]_i_10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \slv_reg10__0\(28),
      I1 => sel0(1),
      I2 => \slv_reg9__0\(28),
      I3 => sel0(0),
      I4 => \slv_reg8__0\(28),
      O => \axi_rdata[28]_i_10_n_0\
    );
\axi_rdata[28]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg23(28),
      I1 => slv_reg22(28),
      I2 => sel0(1),
      I3 => slv_reg21(28),
      I4 => sel0(0),
      I5 => slv_reg20(28),
      O => \axi_rdata[28]_i_3_n_0\
    );
\axi_rdata[28]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg27(28),
      I1 => slv_reg26(28),
      I2 => sel0(1),
      I3 => slv_reg25(28),
      I4 => sel0(0),
      I5 => slv_reg24(28),
      O => \axi_rdata[28]_i_5_n_0\
    );
\axi_rdata[28]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg31(28),
      I1 => slv_reg30(28),
      I2 => sel0(1),
      I3 => slv_reg29(28),
      I4 => sel0(0),
      I5 => slv_reg28(28),
      O => \axi_rdata[28]_i_6_n_0\
    );
\axi_rdata[28]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8830FFFF88300000"
    )
        port map (
      I0 => \slv_reg7__0\(28),
      I1 => sel0(1),
      I2 => slv_reg4(28),
      I3 => sel0(0),
      I4 => sel0(2),
      I5 => \axi_rdata[28]_i_9_n_0\,
      O => \axi_rdata[28]_i_7_n_0\
    );
\axi_rdata[28]_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"04FF0400"
    )
        port map (
      I0 => sel0(0),
      I1 => slv_reg12(28),
      I2 => sel0(1),
      I3 => sel0(2),
      I4 => \axi_rdata[28]_i_10_n_0\,
      O => \axi_rdata[28]_i_8_n_0\
    );
\axi_rdata[28]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(28),
      I1 => slv_reg2(28),
      I2 => sel0(1),
      I3 => slv_reg1(28),
      I4 => sel0(0),
      I5 => slv_reg0(28),
      O => \axi_rdata[28]_i_9_n_0\
    );
\axi_rdata[29]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B888FFFFB8880000"
    )
        port map (
      I0 => \axi_rdata_reg[29]_i_2_n_0\,
      I1 => sel0(3),
      I2 => sel0(2),
      I3 => \axi_rdata[29]_i_3_n_0\,
      I4 => sel0(4),
      I5 => \axi_rdata_reg[29]_i_4_n_0\,
      O => \reg_data_out__0\(29)
    );
\axi_rdata[29]_i_10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \slv_reg10__0\(29),
      I1 => sel0(1),
      I2 => \slv_reg9__0\(29),
      I3 => sel0(0),
      I4 => \slv_reg8__0\(29),
      O => \axi_rdata[29]_i_10_n_0\
    );
\axi_rdata[29]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg23(29),
      I1 => slv_reg22(29),
      I2 => sel0(1),
      I3 => slv_reg21(29),
      I4 => sel0(0),
      I5 => slv_reg20(29),
      O => \axi_rdata[29]_i_3_n_0\
    );
\axi_rdata[29]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg27(29),
      I1 => slv_reg26(29),
      I2 => sel0(1),
      I3 => slv_reg25(29),
      I4 => sel0(0),
      I5 => slv_reg24(29),
      O => \axi_rdata[29]_i_5_n_0\
    );
\axi_rdata[29]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg31(29),
      I1 => slv_reg30(29),
      I2 => sel0(1),
      I3 => slv_reg29(29),
      I4 => sel0(0),
      I5 => slv_reg28(29),
      O => \axi_rdata[29]_i_6_n_0\
    );
\axi_rdata[29]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8830FFFF88300000"
    )
        port map (
      I0 => \slv_reg7__0\(29),
      I1 => sel0(1),
      I2 => slv_reg4(29),
      I3 => sel0(0),
      I4 => sel0(2),
      I5 => \axi_rdata[29]_i_9_n_0\,
      O => \axi_rdata[29]_i_7_n_0\
    );
\axi_rdata[29]_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"04FF0400"
    )
        port map (
      I0 => sel0(0),
      I1 => slv_reg12(29),
      I2 => sel0(1),
      I3 => sel0(2),
      I4 => \axi_rdata[29]_i_10_n_0\,
      O => \axi_rdata[29]_i_8_n_0\
    );
\axi_rdata[29]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(29),
      I1 => slv_reg2(29),
      I2 => sel0(1),
      I3 => slv_reg1(29),
      I4 => sel0(0),
      I5 => slv_reg0(29),
      O => \axi_rdata[29]_i_9_n_0\
    );
\axi_rdata[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B888FFFFB8880000"
    )
        port map (
      I0 => \axi_rdata_reg[2]_i_2_n_0\,
      I1 => sel0(3),
      I2 => sel0(2),
      I3 => \axi_rdata[2]_i_3_n_0\,
      I4 => sel0(4),
      I5 => \axi_rdata_reg[2]_i_4_n_0\,
      O => \reg_data_out__0\(2)
    );
\axi_rdata[2]_i_10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => slv_reg10(2),
      I1 => \axi_araddr_reg[3]_rep_n_0\,
      I2 => slv_reg9(2),
      I3 => \axi_araddr_reg[2]_rep_n_0\,
      I4 => slv_reg8(2),
      O => \axi_rdata[2]_i_10_n_0\
    );
\axi_rdata[2]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg23(2),
      I1 => slv_reg22(2),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg21(2),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg20(2),
      O => \axi_rdata[2]_i_3_n_0\
    );
\axi_rdata[2]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg27(2),
      I1 => slv_reg26(2),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg25(2),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg24(2),
      O => \axi_rdata[2]_i_5_n_0\
    );
\axi_rdata[2]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg31(2),
      I1 => slv_reg30(2),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg29(2),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg28(2),
      O => \axi_rdata[2]_i_6_n_0\
    );
\axi_rdata[2]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8830FFFF88300000"
    )
        port map (
      I0 => \slv_reg7__0\(2),
      I1 => \axi_araddr_reg[3]_rep_n_0\,
      I2 => slv_reg4(2),
      I3 => \axi_araddr_reg[2]_rep_n_0\,
      I4 => sel0(2),
      I5 => \axi_rdata[2]_i_9_n_0\,
      O => \axi_rdata[2]_i_7_n_0\
    );
\axi_rdata[2]_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"04FF0400"
    )
        port map (
      I0 => \axi_araddr_reg[2]_rep_n_0\,
      I1 => slv_reg12(2),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => sel0(2),
      I4 => \axi_rdata[2]_i_10_n_0\,
      O => \axi_rdata[2]_i_8_n_0\
    );
\axi_rdata[2]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(2),
      I1 => slv_reg2(2),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg1(2),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg0(2),
      O => \axi_rdata[2]_i_9_n_0\
    );
\axi_rdata[30]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B888FFFFB8880000"
    )
        port map (
      I0 => \axi_rdata_reg[30]_i_2_n_0\,
      I1 => sel0(3),
      I2 => sel0(2),
      I3 => \axi_rdata[30]_i_3_n_0\,
      I4 => sel0(4),
      I5 => \axi_rdata_reg[30]_i_4_n_0\,
      O => \reg_data_out__0\(30)
    );
\axi_rdata[30]_i_10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \slv_reg10__0\(30),
      I1 => sel0(1),
      I2 => \slv_reg9__0\(30),
      I3 => sel0(0),
      I4 => \slv_reg8__0\(30),
      O => \axi_rdata[30]_i_10_n_0\
    );
\axi_rdata[30]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg23(30),
      I1 => slv_reg22(30),
      I2 => sel0(1),
      I3 => slv_reg21(30),
      I4 => sel0(0),
      I5 => slv_reg20(30),
      O => \axi_rdata[30]_i_3_n_0\
    );
\axi_rdata[30]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg27(30),
      I1 => slv_reg26(30),
      I2 => sel0(1),
      I3 => slv_reg25(30),
      I4 => sel0(0),
      I5 => slv_reg24(30),
      O => \axi_rdata[30]_i_5_n_0\
    );
\axi_rdata[30]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg31(30),
      I1 => slv_reg30(30),
      I2 => sel0(1),
      I3 => slv_reg29(30),
      I4 => sel0(0),
      I5 => slv_reg28(30),
      O => \axi_rdata[30]_i_6_n_0\
    );
\axi_rdata[30]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8830FFFF88300000"
    )
        port map (
      I0 => \slv_reg7__0\(30),
      I1 => sel0(1),
      I2 => slv_reg4(30),
      I3 => sel0(0),
      I4 => sel0(2),
      I5 => \axi_rdata[30]_i_9_n_0\,
      O => \axi_rdata[30]_i_7_n_0\
    );
\axi_rdata[30]_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"04FF0400"
    )
        port map (
      I0 => sel0(0),
      I1 => slv_reg12(30),
      I2 => sel0(1),
      I3 => sel0(2),
      I4 => \axi_rdata[30]_i_10_n_0\,
      O => \axi_rdata[30]_i_8_n_0\
    );
\axi_rdata[30]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(30),
      I1 => slv_reg2(30),
      I2 => sel0(1),
      I3 => slv_reg1(30),
      I4 => sel0(0),
      I5 => slv_reg0(30),
      O => \axi_rdata[30]_i_9_n_0\
    );
\axi_rdata[31]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \^s00_axi_arready\,
      I1 => s00_axi_arvalid,
      I2 => \^s00_axi_rvalid\,
      O => slv_reg_rden
    );
\axi_rdata[31]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(31),
      I1 => slv_reg2(31),
      I2 => sel0(1),
      I3 => slv_reg1(31),
      I4 => sel0(0),
      I5 => slv_reg0(31),
      O => \axi_rdata[31]_i_10_n_0\
    );
\axi_rdata[31]_i_11\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \slv_reg10__0\(31),
      I1 => sel0(1),
      I2 => \slv_reg9__0\(31),
      I3 => sel0(0),
      I4 => \slv_reg8__0\(31),
      O => \axi_rdata[31]_i_11_n_0\
    );
\axi_rdata[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B888FFFFB8880000"
    )
        port map (
      I0 => \axi_rdata_reg[31]_i_3_n_0\,
      I1 => sel0(3),
      I2 => sel0(2),
      I3 => \axi_rdata[31]_i_4_n_0\,
      I4 => sel0(4),
      I5 => \axi_rdata_reg[31]_i_5_n_0\,
      O => \reg_data_out__0\(31)
    );
\axi_rdata[31]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg23(31),
      I1 => slv_reg22(31),
      I2 => sel0(1),
      I3 => slv_reg21(31),
      I4 => sel0(0),
      I5 => slv_reg20(31),
      O => \axi_rdata[31]_i_4_n_0\
    );
\axi_rdata[31]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg27(31),
      I1 => slv_reg26(31),
      I2 => sel0(1),
      I3 => slv_reg25(31),
      I4 => sel0(0),
      I5 => slv_reg24(31),
      O => \axi_rdata[31]_i_6_n_0\
    );
\axi_rdata[31]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg31(31),
      I1 => slv_reg30(31),
      I2 => sel0(1),
      I3 => slv_reg29(31),
      I4 => sel0(0),
      I5 => slv_reg28(31),
      O => \axi_rdata[31]_i_7_n_0\
    );
\axi_rdata[31]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8830FFFF88300000"
    )
        port map (
      I0 => \slv_reg7__0\(31),
      I1 => sel0(1),
      I2 => slv_reg4(31),
      I3 => sel0(0),
      I4 => sel0(2),
      I5 => \axi_rdata[31]_i_10_n_0\,
      O => \axi_rdata[31]_i_8_n_0\
    );
\axi_rdata[31]_i_9\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"04FF0400"
    )
        port map (
      I0 => sel0(0),
      I1 => slv_reg12(31),
      I2 => sel0(1),
      I3 => sel0(2),
      I4 => \axi_rdata[31]_i_11_n_0\,
      O => \axi_rdata[31]_i_9_n_0\
    );
\axi_rdata[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B888FFFFB8880000"
    )
        port map (
      I0 => \axi_rdata_reg[3]_i_2_n_0\,
      I1 => sel0(3),
      I2 => sel0(2),
      I3 => \axi_rdata[3]_i_3_n_0\,
      I4 => sel0(4),
      I5 => \axi_rdata_reg[3]_i_4_n_0\,
      O => \reg_data_out__0\(3)
    );
\axi_rdata[3]_i_10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => slv_reg10(3),
      I1 => \axi_araddr_reg[3]_rep_n_0\,
      I2 => slv_reg9(3),
      I3 => \axi_araddr_reg[2]_rep_n_0\,
      I4 => slv_reg8(3),
      O => \axi_rdata[3]_i_10_n_0\
    );
\axi_rdata[3]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg23(3),
      I1 => slv_reg22(3),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg21(3),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg20(3),
      O => \axi_rdata[3]_i_3_n_0\
    );
\axi_rdata[3]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg27(3),
      I1 => slv_reg26(3),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg25(3),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg24(3),
      O => \axi_rdata[3]_i_5_n_0\
    );
\axi_rdata[3]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg31(3),
      I1 => slv_reg30(3),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg29(3),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg28(3),
      O => \axi_rdata[3]_i_6_n_0\
    );
\axi_rdata[3]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8830FFFF88300000"
    )
        port map (
      I0 => \slv_reg7__0\(3),
      I1 => \axi_araddr_reg[3]_rep_n_0\,
      I2 => slv_reg4(3),
      I3 => \axi_araddr_reg[2]_rep_n_0\,
      I4 => sel0(2),
      I5 => \axi_rdata[3]_i_9_n_0\,
      O => \axi_rdata[3]_i_7_n_0\
    );
\axi_rdata[3]_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"04FF0400"
    )
        port map (
      I0 => \axi_araddr_reg[2]_rep_n_0\,
      I1 => slv_reg12(3),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => sel0(2),
      I4 => \axi_rdata[3]_i_10_n_0\,
      O => \axi_rdata[3]_i_8_n_0\
    );
\axi_rdata[3]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(3),
      I1 => slv_reg2(3),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg1(3),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg0(3),
      O => \axi_rdata[3]_i_9_n_0\
    );
\axi_rdata[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B888FFFFB8880000"
    )
        port map (
      I0 => \axi_rdata_reg[4]_i_2_n_0\,
      I1 => sel0(3),
      I2 => sel0(2),
      I3 => \axi_rdata[4]_i_3_n_0\,
      I4 => sel0(4),
      I5 => \axi_rdata_reg[4]_i_4_n_0\,
      O => \reg_data_out__0\(4)
    );
\axi_rdata[4]_i_10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => slv_reg10(4),
      I1 => \axi_araddr_reg[3]_rep_n_0\,
      I2 => slv_reg9(4),
      I3 => \axi_araddr_reg[2]_rep_n_0\,
      I4 => slv_reg8(4),
      O => \axi_rdata[4]_i_10_n_0\
    );
\axi_rdata[4]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg23(4),
      I1 => slv_reg22(4),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg21(4),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg20(4),
      O => \axi_rdata[4]_i_3_n_0\
    );
\axi_rdata[4]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg27(4),
      I1 => slv_reg26(4),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg25(4),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg24(4),
      O => \axi_rdata[4]_i_5_n_0\
    );
\axi_rdata[4]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg31(4),
      I1 => slv_reg30(4),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg29(4),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg28(4),
      O => \axi_rdata[4]_i_6_n_0\
    );
\axi_rdata[4]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8830FFFF88300000"
    )
        port map (
      I0 => \slv_reg7__0\(4),
      I1 => \axi_araddr_reg[3]_rep_n_0\,
      I2 => slv_reg4(4),
      I3 => \axi_araddr_reg[2]_rep_n_0\,
      I4 => sel0(2),
      I5 => \axi_rdata[4]_i_9_n_0\,
      O => \axi_rdata[4]_i_7_n_0\
    );
\axi_rdata[4]_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"04FF0400"
    )
        port map (
      I0 => \axi_araddr_reg[2]_rep_n_0\,
      I1 => slv_reg12(4),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => sel0(2),
      I4 => \axi_rdata[4]_i_10_n_0\,
      O => \axi_rdata[4]_i_8_n_0\
    );
\axi_rdata[4]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(4),
      I1 => slv_reg2(4),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg1(4),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg0(4),
      O => \axi_rdata[4]_i_9_n_0\
    );
\axi_rdata[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B888FFFFB8880000"
    )
        port map (
      I0 => \axi_rdata_reg[5]_i_2_n_0\,
      I1 => sel0(3),
      I2 => sel0(2),
      I3 => \axi_rdata[5]_i_3_n_0\,
      I4 => sel0(4),
      I5 => \axi_rdata_reg[5]_i_4_n_0\,
      O => \reg_data_out__0\(5)
    );
\axi_rdata[5]_i_10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => slv_reg10(5),
      I1 => \axi_araddr_reg[3]_rep_n_0\,
      I2 => slv_reg9(5),
      I3 => \axi_araddr_reg[2]_rep_n_0\,
      I4 => slv_reg8(5),
      O => \axi_rdata[5]_i_10_n_0\
    );
\axi_rdata[5]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg23(5),
      I1 => slv_reg22(5),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg21(5),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg20(5),
      O => \axi_rdata[5]_i_3_n_0\
    );
\axi_rdata[5]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg27(5),
      I1 => slv_reg26(5),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg25(5),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg24(5),
      O => \axi_rdata[5]_i_5_n_0\
    );
\axi_rdata[5]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg31(5),
      I1 => slv_reg30(5),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg29(5),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg28(5),
      O => \axi_rdata[5]_i_6_n_0\
    );
\axi_rdata[5]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8830FFFF88300000"
    )
        port map (
      I0 => \slv_reg7__0\(5),
      I1 => \axi_araddr_reg[3]_rep_n_0\,
      I2 => slv_reg4(5),
      I3 => \axi_araddr_reg[2]_rep_n_0\,
      I4 => sel0(2),
      I5 => \axi_rdata[5]_i_9_n_0\,
      O => \axi_rdata[5]_i_7_n_0\
    );
\axi_rdata[5]_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"04FF0400"
    )
        port map (
      I0 => \axi_araddr_reg[2]_rep_n_0\,
      I1 => slv_reg12(5),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => sel0(2),
      I4 => \axi_rdata[5]_i_10_n_0\,
      O => \axi_rdata[5]_i_8_n_0\
    );
\axi_rdata[5]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(5),
      I1 => slv_reg2(5),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg1(5),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg0(5),
      O => \axi_rdata[5]_i_9_n_0\
    );
\axi_rdata[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B888FFFFB8880000"
    )
        port map (
      I0 => \axi_rdata_reg[6]_i_2_n_0\,
      I1 => sel0(3),
      I2 => sel0(2),
      I3 => \axi_rdata[6]_i_3_n_0\,
      I4 => sel0(4),
      I5 => \axi_rdata_reg[6]_i_4_n_0\,
      O => \reg_data_out__0\(6)
    );
\axi_rdata[6]_i_10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => slv_reg10(6),
      I1 => \axi_araddr_reg[3]_rep_n_0\,
      I2 => slv_reg9(6),
      I3 => \axi_araddr_reg[2]_rep_n_0\,
      I4 => slv_reg8(6),
      O => \axi_rdata[6]_i_10_n_0\
    );
\axi_rdata[6]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg23(6),
      I1 => slv_reg22(6),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg21(6),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg20(6),
      O => \axi_rdata[6]_i_3_n_0\
    );
\axi_rdata[6]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg27(6),
      I1 => slv_reg26(6),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg25(6),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg24(6),
      O => \axi_rdata[6]_i_5_n_0\
    );
\axi_rdata[6]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg31(6),
      I1 => slv_reg30(6),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg29(6),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg28(6),
      O => \axi_rdata[6]_i_6_n_0\
    );
\axi_rdata[6]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8830FFFF88300000"
    )
        port map (
      I0 => \slv_reg7__0\(6),
      I1 => \axi_araddr_reg[3]_rep_n_0\,
      I2 => slv_reg4(6),
      I3 => \axi_araddr_reg[2]_rep_n_0\,
      I4 => sel0(2),
      I5 => \axi_rdata[6]_i_9_n_0\,
      O => \axi_rdata[6]_i_7_n_0\
    );
\axi_rdata[6]_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"04FF0400"
    )
        port map (
      I0 => \axi_araddr_reg[2]_rep_n_0\,
      I1 => slv_reg12(6),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => sel0(2),
      I4 => \axi_rdata[6]_i_10_n_0\,
      O => \axi_rdata[6]_i_8_n_0\
    );
\axi_rdata[6]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(6),
      I1 => slv_reg2(6),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg1(6),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg0(6),
      O => \axi_rdata[6]_i_9_n_0\
    );
\axi_rdata[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B888FFFFB8880000"
    )
        port map (
      I0 => \axi_rdata_reg[7]_i_2_n_0\,
      I1 => sel0(3),
      I2 => sel0(2),
      I3 => \axi_rdata[7]_i_3_n_0\,
      I4 => sel0(4),
      I5 => \axi_rdata_reg[7]_i_4_n_0\,
      O => \reg_data_out__0\(7)
    );
\axi_rdata[7]_i_10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => slv_reg10(7),
      I1 => \axi_araddr_reg[3]_rep_n_0\,
      I2 => slv_reg9(7),
      I3 => \axi_araddr_reg[2]_rep_n_0\,
      I4 => slv_reg8(7),
      O => \axi_rdata[7]_i_10_n_0\
    );
\axi_rdata[7]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg23(7),
      I1 => slv_reg22(7),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg21(7),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg20(7),
      O => \axi_rdata[7]_i_3_n_0\
    );
\axi_rdata[7]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg27(7),
      I1 => slv_reg26(7),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg25(7),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg24(7),
      O => \axi_rdata[7]_i_5_n_0\
    );
\axi_rdata[7]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg31(7),
      I1 => slv_reg30(7),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg29(7),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg28(7),
      O => \axi_rdata[7]_i_6_n_0\
    );
\axi_rdata[7]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8830FFFF88300000"
    )
        port map (
      I0 => \slv_reg7__0\(7),
      I1 => \axi_araddr_reg[3]_rep_n_0\,
      I2 => slv_reg4(7),
      I3 => \axi_araddr_reg[2]_rep_n_0\,
      I4 => sel0(2),
      I5 => \axi_rdata[7]_i_9_n_0\,
      O => \axi_rdata[7]_i_7_n_0\
    );
\axi_rdata[7]_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"04FF0400"
    )
        port map (
      I0 => \axi_araddr_reg[2]_rep_n_0\,
      I1 => slv_reg12(7),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => sel0(2),
      I4 => \axi_rdata[7]_i_10_n_0\,
      O => \axi_rdata[7]_i_8_n_0\
    );
\axi_rdata[7]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(7),
      I1 => slv_reg2(7),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg1(7),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg0(7),
      O => \axi_rdata[7]_i_9_n_0\
    );
\axi_rdata[8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B888FFFFB8880000"
    )
        port map (
      I0 => \axi_rdata_reg[8]_i_2_n_0\,
      I1 => sel0(3),
      I2 => sel0(2),
      I3 => \axi_rdata[8]_i_3_n_0\,
      I4 => sel0(4),
      I5 => \axi_rdata_reg[8]_i_4_n_0\,
      O => \reg_data_out__0\(8)
    );
\axi_rdata[8]_i_10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => slv_reg10(8),
      I1 => \axi_araddr_reg[3]_rep_n_0\,
      I2 => slv_reg9(8),
      I3 => \axi_araddr_reg[2]_rep_n_0\,
      I4 => slv_reg8(8),
      O => \axi_rdata[8]_i_10_n_0\
    );
\axi_rdata[8]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg23(8),
      I1 => slv_reg22(8),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg21(8),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg20(8),
      O => \axi_rdata[8]_i_3_n_0\
    );
\axi_rdata[8]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg27(8),
      I1 => slv_reg26(8),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg25(8),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg24(8),
      O => \axi_rdata[8]_i_5_n_0\
    );
\axi_rdata[8]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg31(8),
      I1 => slv_reg30(8),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg29(8),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg28(8),
      O => \axi_rdata[8]_i_6_n_0\
    );
\axi_rdata[8]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8830FFFF88300000"
    )
        port map (
      I0 => \slv_reg7__0\(8),
      I1 => \axi_araddr_reg[3]_rep_n_0\,
      I2 => slv_reg4(8),
      I3 => \axi_araddr_reg[2]_rep_n_0\,
      I4 => sel0(2),
      I5 => \axi_rdata[8]_i_9_n_0\,
      O => \axi_rdata[8]_i_7_n_0\
    );
\axi_rdata[8]_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"04FF0400"
    )
        port map (
      I0 => \axi_araddr_reg[2]_rep_n_0\,
      I1 => slv_reg12(8),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => sel0(2),
      I4 => \axi_rdata[8]_i_10_n_0\,
      O => \axi_rdata[8]_i_8_n_0\
    );
\axi_rdata[8]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(8),
      I1 => slv_reg2(8),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg1(8),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg0(8),
      O => \axi_rdata[8]_i_9_n_0\
    );
\axi_rdata[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B888FFFFB8880000"
    )
        port map (
      I0 => \axi_rdata_reg[9]_i_2_n_0\,
      I1 => sel0(3),
      I2 => sel0(2),
      I3 => \axi_rdata[9]_i_3_n_0\,
      I4 => sel0(4),
      I5 => \axi_rdata_reg[9]_i_4_n_0\,
      O => \reg_data_out__0\(9)
    );
\axi_rdata[9]_i_10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => slv_reg10(9),
      I1 => \axi_araddr_reg[3]_rep_n_0\,
      I2 => slv_reg9(9),
      I3 => \axi_araddr_reg[2]_rep_n_0\,
      I4 => slv_reg8(9),
      O => \axi_rdata[9]_i_10_n_0\
    );
\axi_rdata[9]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg23(9),
      I1 => slv_reg22(9),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg21(9),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg20(9),
      O => \axi_rdata[9]_i_3_n_0\
    );
\axi_rdata[9]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg27(9),
      I1 => slv_reg26(9),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg25(9),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg24(9),
      O => \axi_rdata[9]_i_5_n_0\
    );
\axi_rdata[9]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg31(9),
      I1 => slv_reg30(9),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg29(9),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg28(9),
      O => \axi_rdata[9]_i_6_n_0\
    );
\axi_rdata[9]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8830FFFF88300000"
    )
        port map (
      I0 => \slv_reg7__0\(9),
      I1 => \axi_araddr_reg[3]_rep_n_0\,
      I2 => slv_reg4(9),
      I3 => \axi_araddr_reg[2]_rep_n_0\,
      I4 => sel0(2),
      I5 => \axi_rdata[9]_i_9_n_0\,
      O => \axi_rdata[9]_i_7_n_0\
    );
\axi_rdata[9]_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"04FF0400"
    )
        port map (
      I0 => \axi_araddr_reg[2]_rep_n_0\,
      I1 => slv_reg12(9),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => sel0(2),
      I4 => \axi_rdata[9]_i_10_n_0\,
      O => \axi_rdata[9]_i_8_n_0\
    );
\axi_rdata[9]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(9),
      I1 => slv_reg2(9),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg1(9),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg0(9),
      O => \axi_rdata[9]_i_9_n_0\
    );
\axi_rdata_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(0),
      Q => s00_axi_rdata(0),
      R => RST
    );
\axi_rdata_reg[0]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[0]_i_5_n_0\,
      I1 => \axi_rdata[0]_i_6_n_0\,
      O => \axi_rdata_reg[0]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[0]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[0]_i_7_n_0\,
      I1 => \axi_rdata[0]_i_8_n_0\,
      O => \axi_rdata_reg[0]_i_4_n_0\,
      S => sel0(3)
    );
\axi_rdata_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(10),
      Q => s00_axi_rdata(10),
      R => RST
    );
\axi_rdata_reg[10]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[10]_i_5_n_0\,
      I1 => \axi_rdata[10]_i_6_n_0\,
      O => \axi_rdata_reg[10]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[10]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[10]_i_7_n_0\,
      I1 => \axi_rdata[10]_i_8_n_0\,
      O => \axi_rdata_reg[10]_i_4_n_0\,
      S => sel0(3)
    );
\axi_rdata_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(11),
      Q => s00_axi_rdata(11),
      R => RST
    );
\axi_rdata_reg[11]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[11]_i_5_n_0\,
      I1 => \axi_rdata[11]_i_6_n_0\,
      O => \axi_rdata_reg[11]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[11]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[11]_i_7_n_0\,
      I1 => \axi_rdata[11]_i_8_n_0\,
      O => \axi_rdata_reg[11]_i_4_n_0\,
      S => sel0(3)
    );
\axi_rdata_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(12),
      Q => s00_axi_rdata(12),
      R => RST
    );
\axi_rdata_reg[12]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[12]_i_5_n_0\,
      I1 => \axi_rdata[12]_i_6_n_0\,
      O => \axi_rdata_reg[12]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[12]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[12]_i_7_n_0\,
      I1 => \axi_rdata[12]_i_8_n_0\,
      O => \axi_rdata_reg[12]_i_4_n_0\,
      S => sel0(3)
    );
\axi_rdata_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(13),
      Q => s00_axi_rdata(13),
      R => RST
    );
\axi_rdata_reg[13]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[13]_i_5_n_0\,
      I1 => \axi_rdata[13]_i_6_n_0\,
      O => \axi_rdata_reg[13]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[13]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[13]_i_7_n_0\,
      I1 => \axi_rdata[13]_i_8_n_0\,
      O => \axi_rdata_reg[13]_i_4_n_0\,
      S => sel0(3)
    );
\axi_rdata_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(14),
      Q => s00_axi_rdata(14),
      R => RST
    );
\axi_rdata_reg[14]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[14]_i_5_n_0\,
      I1 => \axi_rdata[14]_i_6_n_0\,
      O => \axi_rdata_reg[14]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[14]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[14]_i_7_n_0\,
      I1 => \axi_rdata[14]_i_8_n_0\,
      O => \axi_rdata_reg[14]_i_4_n_0\,
      S => sel0(3)
    );
\axi_rdata_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(15),
      Q => s00_axi_rdata(15),
      R => RST
    );
\axi_rdata_reg[15]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[15]_i_5_n_0\,
      I1 => \axi_rdata[15]_i_6_n_0\,
      O => \axi_rdata_reg[15]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[15]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[15]_i_7_n_0\,
      I1 => \axi_rdata[15]_i_8_n_0\,
      O => \axi_rdata_reg[15]_i_4_n_0\,
      S => sel0(3)
    );
\axi_rdata_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(16),
      Q => s00_axi_rdata(16),
      R => RST
    );
\axi_rdata_reg[16]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[16]_i_5_n_0\,
      I1 => \axi_rdata[16]_i_6_n_0\,
      O => \axi_rdata_reg[16]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[16]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[16]_i_7_n_0\,
      I1 => \axi_rdata[16]_i_8_n_0\,
      O => \axi_rdata_reg[16]_i_4_n_0\,
      S => sel0(3)
    );
\axi_rdata_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(17),
      Q => s00_axi_rdata(17),
      R => RST
    );
\axi_rdata_reg[17]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[17]_i_5_n_0\,
      I1 => \axi_rdata[17]_i_6_n_0\,
      O => \axi_rdata_reg[17]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[17]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[17]_i_7_n_0\,
      I1 => \axi_rdata[17]_i_8_n_0\,
      O => \axi_rdata_reg[17]_i_4_n_0\,
      S => sel0(3)
    );
\axi_rdata_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(18),
      Q => s00_axi_rdata(18),
      R => RST
    );
\axi_rdata_reg[18]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[18]_i_5_n_0\,
      I1 => \axi_rdata[18]_i_6_n_0\,
      O => \axi_rdata_reg[18]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[18]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[18]_i_7_n_0\,
      I1 => \axi_rdata[18]_i_8_n_0\,
      O => \axi_rdata_reg[18]_i_4_n_0\,
      S => sel0(3)
    );
\axi_rdata_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(19),
      Q => s00_axi_rdata(19),
      R => RST
    );
\axi_rdata_reg[19]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[19]_i_5_n_0\,
      I1 => \axi_rdata[19]_i_6_n_0\,
      O => \axi_rdata_reg[19]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[19]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[19]_i_7_n_0\,
      I1 => \axi_rdata[19]_i_8_n_0\,
      O => \axi_rdata_reg[19]_i_4_n_0\,
      S => sel0(3)
    );
\axi_rdata_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(1),
      Q => s00_axi_rdata(1),
      R => RST
    );
\axi_rdata_reg[1]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[1]_i_5_n_0\,
      I1 => \axi_rdata[1]_i_6_n_0\,
      O => \axi_rdata_reg[1]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[1]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[1]_i_7_n_0\,
      I1 => \axi_rdata[1]_i_8_n_0\,
      O => \axi_rdata_reg[1]_i_4_n_0\,
      S => sel0(3)
    );
\axi_rdata_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(20),
      Q => s00_axi_rdata(20),
      R => RST
    );
\axi_rdata_reg[20]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[20]_i_5_n_0\,
      I1 => \axi_rdata[20]_i_6_n_0\,
      O => \axi_rdata_reg[20]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[20]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[20]_i_7_n_0\,
      I1 => \axi_rdata[20]_i_8_n_0\,
      O => \axi_rdata_reg[20]_i_4_n_0\,
      S => sel0(3)
    );
\axi_rdata_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(21),
      Q => s00_axi_rdata(21),
      R => RST
    );
\axi_rdata_reg[21]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[21]_i_5_n_0\,
      I1 => \axi_rdata[21]_i_6_n_0\,
      O => \axi_rdata_reg[21]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[21]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[21]_i_7_n_0\,
      I1 => \axi_rdata[21]_i_8_n_0\,
      O => \axi_rdata_reg[21]_i_4_n_0\,
      S => sel0(3)
    );
\axi_rdata_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(22),
      Q => s00_axi_rdata(22),
      R => RST
    );
\axi_rdata_reg[22]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[22]_i_5_n_0\,
      I1 => \axi_rdata[22]_i_6_n_0\,
      O => \axi_rdata_reg[22]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[22]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[22]_i_7_n_0\,
      I1 => \axi_rdata[22]_i_8_n_0\,
      O => \axi_rdata_reg[22]_i_4_n_0\,
      S => sel0(3)
    );
\axi_rdata_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(23),
      Q => s00_axi_rdata(23),
      R => RST
    );
\axi_rdata_reg[23]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[23]_i_5_n_0\,
      I1 => \axi_rdata[23]_i_6_n_0\,
      O => \axi_rdata_reg[23]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[23]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[23]_i_7_n_0\,
      I1 => \axi_rdata[23]_i_8_n_0\,
      O => \axi_rdata_reg[23]_i_4_n_0\,
      S => sel0(3)
    );
\axi_rdata_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(24),
      Q => s00_axi_rdata(24),
      R => RST
    );
\axi_rdata_reg[24]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[24]_i_5_n_0\,
      I1 => \axi_rdata[24]_i_6_n_0\,
      O => \axi_rdata_reg[24]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[24]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[24]_i_7_n_0\,
      I1 => \axi_rdata[24]_i_8_n_0\,
      O => \axi_rdata_reg[24]_i_4_n_0\,
      S => sel0(3)
    );
\axi_rdata_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(25),
      Q => s00_axi_rdata(25),
      R => RST
    );
\axi_rdata_reg[25]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[25]_i_5_n_0\,
      I1 => \axi_rdata[25]_i_6_n_0\,
      O => \axi_rdata_reg[25]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[25]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[25]_i_7_n_0\,
      I1 => \axi_rdata[25]_i_8_n_0\,
      O => \axi_rdata_reg[25]_i_4_n_0\,
      S => sel0(3)
    );
\axi_rdata_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(26),
      Q => s00_axi_rdata(26),
      R => RST
    );
\axi_rdata_reg[26]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[26]_i_5_n_0\,
      I1 => \axi_rdata[26]_i_6_n_0\,
      O => \axi_rdata_reg[26]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[26]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[26]_i_7_n_0\,
      I1 => \axi_rdata[26]_i_8_n_0\,
      O => \axi_rdata_reg[26]_i_4_n_0\,
      S => sel0(3)
    );
\axi_rdata_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(27),
      Q => s00_axi_rdata(27),
      R => RST
    );
\axi_rdata_reg[27]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[27]_i_5_n_0\,
      I1 => \axi_rdata[27]_i_6_n_0\,
      O => \axi_rdata_reg[27]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[27]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[27]_i_7_n_0\,
      I1 => \axi_rdata[27]_i_8_n_0\,
      O => \axi_rdata_reg[27]_i_4_n_0\,
      S => sel0(3)
    );
\axi_rdata_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(28),
      Q => s00_axi_rdata(28),
      R => RST
    );
\axi_rdata_reg[28]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[28]_i_5_n_0\,
      I1 => \axi_rdata[28]_i_6_n_0\,
      O => \axi_rdata_reg[28]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[28]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[28]_i_7_n_0\,
      I1 => \axi_rdata[28]_i_8_n_0\,
      O => \axi_rdata_reg[28]_i_4_n_0\,
      S => sel0(3)
    );
\axi_rdata_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(29),
      Q => s00_axi_rdata(29),
      R => RST
    );
\axi_rdata_reg[29]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[29]_i_5_n_0\,
      I1 => \axi_rdata[29]_i_6_n_0\,
      O => \axi_rdata_reg[29]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[29]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[29]_i_7_n_0\,
      I1 => \axi_rdata[29]_i_8_n_0\,
      O => \axi_rdata_reg[29]_i_4_n_0\,
      S => sel0(3)
    );
\axi_rdata_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(2),
      Q => s00_axi_rdata(2),
      R => RST
    );
\axi_rdata_reg[2]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[2]_i_5_n_0\,
      I1 => \axi_rdata[2]_i_6_n_0\,
      O => \axi_rdata_reg[2]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[2]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[2]_i_7_n_0\,
      I1 => \axi_rdata[2]_i_8_n_0\,
      O => \axi_rdata_reg[2]_i_4_n_0\,
      S => sel0(3)
    );
\axi_rdata_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(30),
      Q => s00_axi_rdata(30),
      R => RST
    );
\axi_rdata_reg[30]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[30]_i_5_n_0\,
      I1 => \axi_rdata[30]_i_6_n_0\,
      O => \axi_rdata_reg[30]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[30]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[30]_i_7_n_0\,
      I1 => \axi_rdata[30]_i_8_n_0\,
      O => \axi_rdata_reg[30]_i_4_n_0\,
      S => sel0(3)
    );
\axi_rdata_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(31),
      Q => s00_axi_rdata(31),
      R => RST
    );
\axi_rdata_reg[31]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[31]_i_6_n_0\,
      I1 => \axi_rdata[31]_i_7_n_0\,
      O => \axi_rdata_reg[31]_i_3_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[31]_i_5\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[31]_i_8_n_0\,
      I1 => \axi_rdata[31]_i_9_n_0\,
      O => \axi_rdata_reg[31]_i_5_n_0\,
      S => sel0(3)
    );
\axi_rdata_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(3),
      Q => s00_axi_rdata(3),
      R => RST
    );
\axi_rdata_reg[3]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[3]_i_5_n_0\,
      I1 => \axi_rdata[3]_i_6_n_0\,
      O => \axi_rdata_reg[3]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[3]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[3]_i_7_n_0\,
      I1 => \axi_rdata[3]_i_8_n_0\,
      O => \axi_rdata_reg[3]_i_4_n_0\,
      S => sel0(3)
    );
\axi_rdata_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(4),
      Q => s00_axi_rdata(4),
      R => RST
    );
\axi_rdata_reg[4]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[4]_i_5_n_0\,
      I1 => \axi_rdata[4]_i_6_n_0\,
      O => \axi_rdata_reg[4]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[4]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[4]_i_7_n_0\,
      I1 => \axi_rdata[4]_i_8_n_0\,
      O => \axi_rdata_reg[4]_i_4_n_0\,
      S => sel0(3)
    );
\axi_rdata_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(5),
      Q => s00_axi_rdata(5),
      R => RST
    );
\axi_rdata_reg[5]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[5]_i_5_n_0\,
      I1 => \axi_rdata[5]_i_6_n_0\,
      O => \axi_rdata_reg[5]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[5]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[5]_i_7_n_0\,
      I1 => \axi_rdata[5]_i_8_n_0\,
      O => \axi_rdata_reg[5]_i_4_n_0\,
      S => sel0(3)
    );
\axi_rdata_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(6),
      Q => s00_axi_rdata(6),
      R => RST
    );
\axi_rdata_reg[6]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[6]_i_5_n_0\,
      I1 => \axi_rdata[6]_i_6_n_0\,
      O => \axi_rdata_reg[6]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[6]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[6]_i_7_n_0\,
      I1 => \axi_rdata[6]_i_8_n_0\,
      O => \axi_rdata_reg[6]_i_4_n_0\,
      S => sel0(3)
    );
\axi_rdata_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(7),
      Q => s00_axi_rdata(7),
      R => RST
    );
\axi_rdata_reg[7]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[7]_i_5_n_0\,
      I1 => \axi_rdata[7]_i_6_n_0\,
      O => \axi_rdata_reg[7]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[7]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[7]_i_7_n_0\,
      I1 => \axi_rdata[7]_i_8_n_0\,
      O => \axi_rdata_reg[7]_i_4_n_0\,
      S => sel0(3)
    );
\axi_rdata_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(8),
      Q => s00_axi_rdata(8),
      R => RST
    );
\axi_rdata_reg[8]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[8]_i_5_n_0\,
      I1 => \axi_rdata[8]_i_6_n_0\,
      O => \axi_rdata_reg[8]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[8]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[8]_i_7_n_0\,
      I1 => \axi_rdata[8]_i_8_n_0\,
      O => \axi_rdata_reg[8]_i_4_n_0\,
      S => sel0(3)
    );
\axi_rdata_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(9),
      Q => s00_axi_rdata(9),
      R => RST
    );
\axi_rdata_reg[9]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[9]_i_5_n_0\,
      I1 => \axi_rdata[9]_i_6_n_0\,
      O => \axi_rdata_reg[9]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[9]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[9]_i_7_n_0\,
      I1 => \axi_rdata[9]_i_8_n_0\,
      O => \axi_rdata_reg[9]_i_4_n_0\,
      S => sel0(3)
    );
axi_rvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_arready_reg_0,
      Q => \^s00_axi_rvalid\,
      R => RST
    );
axi_wready_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => s00_axi_wvalid,
      I1 => s00_axi_awvalid,
      I2 => \^s00_axi_wready\,
      O => axi_wready0
    );
axi_wready_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_wready0,
      Q => \^s00_axi_wready\,
      R => RST
    );
datapath: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_lab2_datapath
     port map (
      CO(0) => sw14_in,
      E(0) => fsm_n_4,
      \FSM_onehot_state_reg[1]\(0) => sw1,
      \FSM_onehot_state_reg[4]\(1 downto 0) => w_cw(2 downto 1),
      Q(3) => datapath_n_15,
      Q(2) => datapath_n_16,
      Q(1) => datapath_n_17,
      Q(0) => datapath_n_18,
      SR(0) => fsm_n_0,
      SS(0) => RST,
      WREN => WREN,
      ac_adc_sdata => ac_adc_sdata,
      ac_bclk => ac_bclk,
      ac_dac_sdata => ac_dac_sdata,
      ac_lrclk => ac_lrclk,
      ac_mclk => ac_mclk,
      btn(4 downto 0) => btn(4 downto 0),
      \i_sig_reg[6]_0\ => datapath_n_19,
      s00_axi_aclk => s00_axi_aclk,
      s00_axi_aresetn => s00_axi_aresetn,
      scl => scl,
      sda => sda,
      \slv_reg10_reg[15]\(15 downto 0) => slv_reg10(15 downto 0),
      \slv_reg8_reg[9]\(9 downto 0) => slv_reg8(9 downto 0),
      \slv_reg9_reg[15]\(15 downto 0) => slv_reg9(15 downto 0),
      switch(2 downto 0) => switch(2 downto 0),
      tmds(3 downto 0) => tmds(3 downto 0),
      tmdsb(3 downto 0) => tmdsb(3 downto 0),
      w_sw(2 downto 0) => w_sw(2 downto 0)
    );
fsm: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_lab2_fsm
     port map (
      CO(0) => sw14_in,
      E(0) => fsm_n_4,
      Q(1 downto 0) => w_cw(2 downto 1),
      SR(0) => fsm_n_0,
      SS(0) => RST,
      WREN => WREN,
      \i_sig_reg[5]\ => datapath_n_19,
      \i_sig_reg[9]\(3) => datapath_n_15,
      \i_sig_reg[9]\(2) => datapath_n_16,
      \i_sig_reg[9]\(1) => datapath_n_17,
      \i_sig_reg[9]\(0) => datapath_n_18,
      s00_axi_aclk => s00_axi_aclk,
      s00_axi_aresetn => s00_axi_aresetn,
      \slv_reg7_reg[0]\(0) => slv_reg7(0),
      switch(0) => switch(2),
      \trigger_volt_reg[9]\(0) => sw1,
      w_sw(2 downto 0) => w_sw(2 downto 0)
    );
\slv_reg0[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000001000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg8[31]_i_2_n_0\,
      I3 => s00_axi_wstrb(1),
      I4 => p_0_in(1),
      I5 => p_0_in(3),
      O => p_1_in(15)
    );
\slv_reg0[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000001000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg8[31]_i_2_n_0\,
      I3 => s00_axi_wstrb(2),
      I4 => p_0_in(1),
      I5 => p_0_in(3),
      O => p_1_in(23)
    );
\slv_reg0[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000001000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg8[31]_i_2_n_0\,
      I3 => s00_axi_wstrb(3),
      I4 => p_0_in(1),
      I5 => p_0_in(3),
      O => p_1_in(31)
    );
\slv_reg0[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000001000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg8[31]_i_2_n_0\,
      I3 => s00_axi_wstrb(0),
      I4 => p_0_in(1),
      I5 => p_0_in(3),
      O => p_1_in(7)
    );
\slv_reg0_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(0),
      Q => slv_reg0(0),
      R => RST
    );
\slv_reg0_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(10),
      Q => slv_reg0(10),
      R => RST
    );
\slv_reg0_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(11),
      Q => slv_reg0(11),
      R => RST
    );
\slv_reg0_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(12),
      Q => slv_reg0(12),
      R => RST
    );
\slv_reg0_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(13),
      Q => slv_reg0(13),
      R => RST
    );
\slv_reg0_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(14),
      Q => slv_reg0(14),
      R => RST
    );
\slv_reg0_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(15),
      Q => slv_reg0(15),
      R => RST
    );
\slv_reg0_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(16),
      Q => slv_reg0(16),
      R => RST
    );
\slv_reg0_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(17),
      Q => slv_reg0(17),
      R => RST
    );
\slv_reg0_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(18),
      Q => slv_reg0(18),
      R => RST
    );
\slv_reg0_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(19),
      Q => slv_reg0(19),
      R => RST
    );
\slv_reg0_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(1),
      Q => slv_reg0(1),
      R => RST
    );
\slv_reg0_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(20),
      Q => slv_reg0(20),
      R => RST
    );
\slv_reg0_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(21),
      Q => slv_reg0(21),
      R => RST
    );
\slv_reg0_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(22),
      Q => slv_reg0(22),
      R => RST
    );
\slv_reg0_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(23),
      Q => slv_reg0(23),
      R => RST
    );
\slv_reg0_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(24),
      Q => slv_reg0(24),
      R => RST
    );
\slv_reg0_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(25),
      Q => slv_reg0(25),
      R => RST
    );
\slv_reg0_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(26),
      Q => slv_reg0(26),
      R => RST
    );
\slv_reg0_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(27),
      Q => slv_reg0(27),
      R => RST
    );
\slv_reg0_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(28),
      Q => slv_reg0(28),
      R => RST
    );
\slv_reg0_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(29),
      Q => slv_reg0(29),
      R => RST
    );
\slv_reg0_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(2),
      Q => slv_reg0(2),
      R => RST
    );
\slv_reg0_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(30),
      Q => slv_reg0(30),
      R => RST
    );
\slv_reg0_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(31),
      Q => slv_reg0(31),
      R => RST
    );
\slv_reg0_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(3),
      Q => slv_reg0(3),
      R => RST
    );
\slv_reg0_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(4),
      Q => slv_reg0(4),
      R => RST
    );
\slv_reg0_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(5),
      Q => slv_reg0(5),
      R => RST
    );
\slv_reg0_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(6),
      Q => slv_reg0(6),
      R => RST
    );
\slv_reg0_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(7),
      Q => slv_reg0(7),
      R => RST
    );
\slv_reg0_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(8),
      Q => slv_reg0(8),
      R => RST
    );
\slv_reg0_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(9),
      Q => slv_reg0(9),
      R => RST
    );
\slv_reg10[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1000000000000000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg8[31]_i_2_n_0\,
      I3 => p_0_in(1),
      I4 => p_0_in(3),
      I5 => s00_axi_wstrb(1),
      O => \slv_reg10[15]_i_1_n_0\
    );
\slv_reg10[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1000000000000000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg8[31]_i_2_n_0\,
      I3 => p_0_in(1),
      I4 => p_0_in(3),
      I5 => s00_axi_wstrb(2),
      O => \slv_reg10[23]_i_1_n_0\
    );
\slv_reg10[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1000000000000000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg8[31]_i_2_n_0\,
      I3 => p_0_in(1),
      I4 => p_0_in(3),
      I5 => s00_axi_wstrb(3),
      O => \slv_reg10[31]_i_1_n_0\
    );
\slv_reg10[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1000000000000000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg8[31]_i_2_n_0\,
      I3 => p_0_in(1),
      I4 => p_0_in(3),
      I5 => s00_axi_wstrb(0),
      O => \slv_reg10[7]_i_1_n_0\
    );
\slv_reg10_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg10[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg10(0),
      R => RST
    );
\slv_reg10_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg10[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => slv_reg10(10),
      R => RST
    );
\slv_reg10_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg10[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => slv_reg10(11),
      R => RST
    );
\slv_reg10_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg10[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => slv_reg10(12),
      R => RST
    );
\slv_reg10_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg10[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => slv_reg10(13),
      R => RST
    );
\slv_reg10_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg10[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => slv_reg10(14),
      R => RST
    );
\slv_reg10_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg10[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => slv_reg10(15),
      R => RST
    );
\slv_reg10_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg10[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => \slv_reg10__0\(16),
      R => RST
    );
\slv_reg10_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg10[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => \slv_reg10__0\(17),
      R => RST
    );
\slv_reg10_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg10[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => \slv_reg10__0\(18),
      R => RST
    );
\slv_reg10_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg10[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => \slv_reg10__0\(19),
      R => RST
    );
\slv_reg10_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg10[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => slv_reg10(1),
      R => RST
    );
\slv_reg10_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg10[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => \slv_reg10__0\(20),
      R => RST
    );
\slv_reg10_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg10[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => \slv_reg10__0\(21),
      R => RST
    );
\slv_reg10_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg10[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => \slv_reg10__0\(22),
      R => RST
    );
\slv_reg10_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg10[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => \slv_reg10__0\(23),
      R => RST
    );
\slv_reg10_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg10[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => \slv_reg10__0\(24),
      R => RST
    );
\slv_reg10_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg10[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => \slv_reg10__0\(25),
      R => RST
    );
\slv_reg10_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg10[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => \slv_reg10__0\(26),
      R => RST
    );
\slv_reg10_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg10[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => \slv_reg10__0\(27),
      R => RST
    );
\slv_reg10_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg10[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => \slv_reg10__0\(28),
      R => RST
    );
\slv_reg10_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg10[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => \slv_reg10__0\(29),
      R => RST
    );
\slv_reg10_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg10[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => slv_reg10(2),
      R => RST
    );
\slv_reg10_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg10[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => \slv_reg10__0\(30),
      R => RST
    );
\slv_reg10_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg10[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => \slv_reg10__0\(31),
      R => RST
    );
\slv_reg10_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg10[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg10(3),
      R => RST
    );
\slv_reg10_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg10[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg10(4),
      R => RST
    );
\slv_reg10_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg10[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg10(5),
      R => RST
    );
\slv_reg10_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg10[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg10(6),
      R => RST
    );
\slv_reg10_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg10[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg10(7),
      R => RST
    );
\slv_reg10_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg10[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg10(8),
      R => RST
    );
\slv_reg10_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg10[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg10(9),
      R => RST
    );
\slv_reg12[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000040000000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg8[31]_i_2_n_0\,
      I3 => s00_axi_wstrb(1),
      I4 => p_0_in(3),
      I5 => p_0_in(1),
      O => \slv_reg12[15]_i_1_n_0\
    );
\slv_reg12[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000040000000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg8[31]_i_2_n_0\,
      I3 => s00_axi_wstrb(2),
      I4 => p_0_in(3),
      I5 => p_0_in(1),
      O => \slv_reg12[23]_i_1_n_0\
    );
\slv_reg12[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000040000000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg8[31]_i_2_n_0\,
      I3 => s00_axi_wstrb(3),
      I4 => p_0_in(3),
      I5 => p_0_in(1),
      O => \slv_reg12[31]_i_1_n_0\
    );
\slv_reg12[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000040000000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg8[31]_i_2_n_0\,
      I3 => s00_axi_wstrb(0),
      I4 => p_0_in(3),
      I5 => p_0_in(1),
      O => \slv_reg12[7]_i_1_n_0\
    );
\slv_reg12_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg12[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => \slv_reg12__0\(0),
      R => RST
    );
\slv_reg12_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg12[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => slv_reg12(10),
      R => RST
    );
\slv_reg12_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg12[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => slv_reg12(11),
      R => RST
    );
\slv_reg12_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg12[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => slv_reg12(12),
      R => RST
    );
\slv_reg12_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg12[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => slv_reg12(13),
      R => RST
    );
\slv_reg12_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg12[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => slv_reg12(14),
      R => RST
    );
\slv_reg12_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg12[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => slv_reg12(15),
      R => RST
    );
\slv_reg12_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg12[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => slv_reg12(16),
      R => RST
    );
\slv_reg12_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg12[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => slv_reg12(17),
      R => RST
    );
\slv_reg12_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg12[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => slv_reg12(18),
      R => RST
    );
\slv_reg12_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg12[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => slv_reg12(19),
      R => RST
    );
\slv_reg12_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg12[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => slv_reg12(1),
      R => RST
    );
\slv_reg12_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg12[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => slv_reg12(20),
      R => RST
    );
\slv_reg12_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg12[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => slv_reg12(21),
      R => RST
    );
\slv_reg12_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg12[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => slv_reg12(22),
      R => RST
    );
\slv_reg12_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg12[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => slv_reg12(23),
      R => RST
    );
\slv_reg12_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg12[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => slv_reg12(24),
      R => RST
    );
\slv_reg12_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg12[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => slv_reg12(25),
      R => RST
    );
\slv_reg12_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg12[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => slv_reg12(26),
      R => RST
    );
\slv_reg12_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg12[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => slv_reg12(27),
      R => RST
    );
\slv_reg12_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg12[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => slv_reg12(28),
      R => RST
    );
\slv_reg12_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg12[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => slv_reg12(29),
      R => RST
    );
\slv_reg12_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg12[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => slv_reg12(2),
      R => RST
    );
\slv_reg12_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg12[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => slv_reg12(30),
      R => RST
    );
\slv_reg12_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg12[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => slv_reg12(31),
      R => RST
    );
\slv_reg12_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg12[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg12(3),
      R => RST
    );
\slv_reg12_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg12[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg12(4),
      R => RST
    );
\slv_reg12_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg12[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg12(5),
      R => RST
    );
\slv_reg12_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg12[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg12(6),
      R => RST
    );
\slv_reg12_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg12[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg12(7),
      R => RST
    );
\slv_reg12_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg12[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg12(8),
      R => RST
    );
\slv_reg12_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg12[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg12(9),
      R => RST
    );
\slv_reg1[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000001000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg7[31]_i_2_n_0\,
      I3 => s00_axi_wstrb(1),
      I4 => p_0_in(1),
      I5 => p_0_in(3),
      O => \slv_reg1[15]_i_1_n_0\
    );
\slv_reg1[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000001000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg7[31]_i_2_n_0\,
      I3 => s00_axi_wstrb(2),
      I4 => p_0_in(1),
      I5 => p_0_in(3),
      O => \slv_reg1[23]_i_1_n_0\
    );
\slv_reg1[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000001000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg7[31]_i_2_n_0\,
      I3 => s00_axi_wstrb(3),
      I4 => p_0_in(1),
      I5 => p_0_in(3),
      O => \slv_reg1[31]_i_1_n_0\
    );
\slv_reg1[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000001000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg7[31]_i_2_n_0\,
      I3 => s00_axi_wstrb(0),
      I4 => p_0_in(1),
      I5 => p_0_in(3),
      O => \slv_reg1[7]_i_1_n_0\
    );
\slv_reg1_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg1(0),
      R => RST
    );
\slv_reg1_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => slv_reg1(10),
      R => RST
    );
\slv_reg1_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => slv_reg1(11),
      R => RST
    );
\slv_reg1_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => slv_reg1(12),
      R => RST
    );
\slv_reg1_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => slv_reg1(13),
      R => RST
    );
\slv_reg1_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => slv_reg1(14),
      R => RST
    );
\slv_reg1_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => slv_reg1(15),
      R => RST
    );
\slv_reg1_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => slv_reg1(16),
      R => RST
    );
\slv_reg1_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => slv_reg1(17),
      R => RST
    );
\slv_reg1_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => slv_reg1(18),
      R => RST
    );
\slv_reg1_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => slv_reg1(19),
      R => RST
    );
\slv_reg1_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => slv_reg1(1),
      R => RST
    );
\slv_reg1_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => slv_reg1(20),
      R => RST
    );
\slv_reg1_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => slv_reg1(21),
      R => RST
    );
\slv_reg1_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => slv_reg1(22),
      R => RST
    );
\slv_reg1_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => slv_reg1(23),
      R => RST
    );
\slv_reg1_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => slv_reg1(24),
      R => RST
    );
\slv_reg1_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => slv_reg1(25),
      R => RST
    );
\slv_reg1_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => slv_reg1(26),
      R => RST
    );
\slv_reg1_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => slv_reg1(27),
      R => RST
    );
\slv_reg1_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => slv_reg1(28),
      R => RST
    );
\slv_reg1_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => slv_reg1(29),
      R => RST
    );
\slv_reg1_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => slv_reg1(2),
      R => RST
    );
\slv_reg1_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => slv_reg1(30),
      R => RST
    );
\slv_reg1_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => slv_reg1(31),
      R => RST
    );
\slv_reg1_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg1(3),
      R => RST
    );
\slv_reg1_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg1(4),
      R => RST
    );
\slv_reg1_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg1(5),
      R => RST
    );
\slv_reg1_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg1(6),
      R => RST
    );
\slv_reg1_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg1(7),
      R => RST
    );
\slv_reg1_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg1(8),
      R => RST
    );
\slv_reg1_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg1(9),
      R => RST
    );
\slv_reg20[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg8[31]_i_2_n_0\,
      I3 => s00_axi_wstrb(1),
      I4 => p_0_in(1),
      I5 => p_0_in(3),
      O => \slv_reg20[15]_i_1_n_0\
    );
\slv_reg20[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg8[31]_i_2_n_0\,
      I3 => s00_axi_wstrb(2),
      I4 => p_0_in(1),
      I5 => p_0_in(3),
      O => \slv_reg20[23]_i_1_n_0\
    );
\slv_reg20[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg8[31]_i_2_n_0\,
      I3 => s00_axi_wstrb(3),
      I4 => p_0_in(1),
      I5 => p_0_in(3),
      O => \slv_reg20[31]_i_1_n_0\
    );
\slv_reg20[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg8[31]_i_2_n_0\,
      I3 => s00_axi_wstrb(0),
      I4 => p_0_in(1),
      I5 => p_0_in(3),
      O => \slv_reg20[7]_i_1_n_0\
    );
\slv_reg20_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg20[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg20(0),
      R => RST
    );
\slv_reg20_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg20[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => slv_reg20(10),
      R => RST
    );
\slv_reg20_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg20[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => slv_reg20(11),
      R => RST
    );
\slv_reg20_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg20[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => slv_reg20(12),
      R => RST
    );
\slv_reg20_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg20[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => slv_reg20(13),
      R => RST
    );
\slv_reg20_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg20[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => slv_reg20(14),
      R => RST
    );
\slv_reg20_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg20[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => slv_reg20(15),
      R => RST
    );
\slv_reg20_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg20[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => slv_reg20(16),
      R => RST
    );
\slv_reg20_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg20[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => slv_reg20(17),
      R => RST
    );
\slv_reg20_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg20[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => slv_reg20(18),
      R => RST
    );
\slv_reg20_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg20[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => slv_reg20(19),
      R => RST
    );
\slv_reg20_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg20[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => slv_reg20(1),
      R => RST
    );
\slv_reg20_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg20[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => slv_reg20(20),
      R => RST
    );
\slv_reg20_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg20[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => slv_reg20(21),
      R => RST
    );
\slv_reg20_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg20[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => slv_reg20(22),
      R => RST
    );
\slv_reg20_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg20[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => slv_reg20(23),
      R => RST
    );
\slv_reg20_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg20[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => slv_reg20(24),
      R => RST
    );
\slv_reg20_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg20[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => slv_reg20(25),
      R => RST
    );
\slv_reg20_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg20[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => slv_reg20(26),
      R => RST
    );
\slv_reg20_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg20[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => slv_reg20(27),
      R => RST
    );
\slv_reg20_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg20[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => slv_reg20(28),
      R => RST
    );
\slv_reg20_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg20[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => slv_reg20(29),
      R => RST
    );
\slv_reg20_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg20[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => slv_reg20(2),
      R => RST
    );
\slv_reg20_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg20[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => slv_reg20(30),
      R => RST
    );
\slv_reg20_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg20[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => slv_reg20(31),
      R => RST
    );
\slv_reg20_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg20[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg20(3),
      R => RST
    );
\slv_reg20_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg20[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg20(4),
      R => RST
    );
\slv_reg20_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg20[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg20(5),
      R => RST
    );
\slv_reg20_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg20[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg20(6),
      R => RST
    );
\slv_reg20_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg20[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg20(7),
      R => RST
    );
\slv_reg20_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg20[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg20(8),
      R => RST
    );
\slv_reg20_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg20[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg20(9),
      R => RST
    );
\slv_reg21[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg7[31]_i_2_n_0\,
      I3 => s00_axi_wstrb(1),
      I4 => p_0_in(1),
      I5 => p_0_in(3),
      O => \slv_reg21[15]_i_1_n_0\
    );
\slv_reg21[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg7[31]_i_2_n_0\,
      I3 => s00_axi_wstrb(2),
      I4 => p_0_in(1),
      I5 => p_0_in(3),
      O => \slv_reg21[23]_i_1_n_0\
    );
\slv_reg21[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg7[31]_i_2_n_0\,
      I3 => s00_axi_wstrb(3),
      I4 => p_0_in(1),
      I5 => p_0_in(3),
      O => \slv_reg21[31]_i_1_n_0\
    );
\slv_reg21[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg7[31]_i_2_n_0\,
      I3 => s00_axi_wstrb(0),
      I4 => p_0_in(1),
      I5 => p_0_in(3),
      O => \slv_reg21[7]_i_1_n_0\
    );
\slv_reg21_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg21[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg21(0),
      R => RST
    );
\slv_reg21_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg21[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => slv_reg21(10),
      R => RST
    );
\slv_reg21_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg21[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => slv_reg21(11),
      R => RST
    );
\slv_reg21_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg21[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => slv_reg21(12),
      R => RST
    );
\slv_reg21_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg21[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => slv_reg21(13),
      R => RST
    );
\slv_reg21_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg21[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => slv_reg21(14),
      R => RST
    );
\slv_reg21_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg21[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => slv_reg21(15),
      R => RST
    );
\slv_reg21_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg21[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => slv_reg21(16),
      R => RST
    );
\slv_reg21_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg21[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => slv_reg21(17),
      R => RST
    );
\slv_reg21_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg21[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => slv_reg21(18),
      R => RST
    );
\slv_reg21_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg21[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => slv_reg21(19),
      R => RST
    );
\slv_reg21_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg21[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => slv_reg21(1),
      R => RST
    );
\slv_reg21_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg21[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => slv_reg21(20),
      R => RST
    );
\slv_reg21_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg21[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => slv_reg21(21),
      R => RST
    );
\slv_reg21_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg21[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => slv_reg21(22),
      R => RST
    );
\slv_reg21_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg21[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => slv_reg21(23),
      R => RST
    );
\slv_reg21_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg21[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => slv_reg21(24),
      R => RST
    );
\slv_reg21_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg21[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => slv_reg21(25),
      R => RST
    );
\slv_reg21_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg21[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => slv_reg21(26),
      R => RST
    );
\slv_reg21_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg21[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => slv_reg21(27),
      R => RST
    );
\slv_reg21_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg21[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => slv_reg21(28),
      R => RST
    );
\slv_reg21_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg21[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => slv_reg21(29),
      R => RST
    );
\slv_reg21_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg21[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => slv_reg21(2),
      R => RST
    );
\slv_reg21_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg21[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => slv_reg21(30),
      R => RST
    );
\slv_reg21_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg21[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => slv_reg21(31),
      R => RST
    );
\slv_reg21_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg21[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg21(3),
      R => RST
    );
\slv_reg21_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg21[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg21(4),
      R => RST
    );
\slv_reg21_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg21[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg21(5),
      R => RST
    );
\slv_reg21_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg21[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg21(6),
      R => RST
    );
\slv_reg21_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg21[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg21(7),
      R => RST
    );
\slv_reg21_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg21[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg21(8),
      R => RST
    );
\slv_reg21_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg21[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg21(9),
      R => RST
    );
\slv_reg22[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000080000000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg8[31]_i_2_n_0\,
      I3 => s00_axi_wstrb(1),
      I4 => p_0_in(1),
      I5 => p_0_in(3),
      O => \slv_reg22[15]_i_1_n_0\
    );
\slv_reg22[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000080000000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg8[31]_i_2_n_0\,
      I3 => s00_axi_wstrb(2),
      I4 => p_0_in(1),
      I5 => p_0_in(3),
      O => \slv_reg22[23]_i_1_n_0\
    );
\slv_reg22[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000080000000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg8[31]_i_2_n_0\,
      I3 => s00_axi_wstrb(3),
      I4 => p_0_in(1),
      I5 => p_0_in(3),
      O => \slv_reg22[31]_i_1_n_0\
    );
\slv_reg22[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000080000000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg8[31]_i_2_n_0\,
      I3 => s00_axi_wstrb(0),
      I4 => p_0_in(1),
      I5 => p_0_in(3),
      O => \slv_reg22[7]_i_1_n_0\
    );
\slv_reg22_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg22[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg22(0),
      R => RST
    );
\slv_reg22_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg22[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => slv_reg22(10),
      R => RST
    );
\slv_reg22_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg22[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => slv_reg22(11),
      R => RST
    );
\slv_reg22_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg22[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => slv_reg22(12),
      R => RST
    );
\slv_reg22_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg22[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => slv_reg22(13),
      R => RST
    );
\slv_reg22_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg22[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => slv_reg22(14),
      R => RST
    );
\slv_reg22_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg22[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => slv_reg22(15),
      R => RST
    );
\slv_reg22_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg22[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => slv_reg22(16),
      R => RST
    );
\slv_reg22_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg22[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => slv_reg22(17),
      R => RST
    );
\slv_reg22_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg22[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => slv_reg22(18),
      R => RST
    );
\slv_reg22_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg22[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => slv_reg22(19),
      R => RST
    );
\slv_reg22_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg22[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => slv_reg22(1),
      R => RST
    );
\slv_reg22_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg22[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => slv_reg22(20),
      R => RST
    );
\slv_reg22_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg22[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => slv_reg22(21),
      R => RST
    );
\slv_reg22_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg22[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => slv_reg22(22),
      R => RST
    );
\slv_reg22_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg22[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => slv_reg22(23),
      R => RST
    );
\slv_reg22_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg22[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => slv_reg22(24),
      R => RST
    );
\slv_reg22_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg22[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => slv_reg22(25),
      R => RST
    );
\slv_reg22_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg22[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => slv_reg22(26),
      R => RST
    );
\slv_reg22_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg22[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => slv_reg22(27),
      R => RST
    );
\slv_reg22_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg22[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => slv_reg22(28),
      R => RST
    );
\slv_reg22_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg22[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => slv_reg22(29),
      R => RST
    );
\slv_reg22_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg22[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => slv_reg22(2),
      R => RST
    );
\slv_reg22_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg22[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => slv_reg22(30),
      R => RST
    );
\slv_reg22_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg22[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => slv_reg22(31),
      R => RST
    );
\slv_reg22_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg22[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg22(3),
      R => RST
    );
\slv_reg22_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg22[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg22(4),
      R => RST
    );
\slv_reg22_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg22[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg22(5),
      R => RST
    );
\slv_reg22_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg22[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg22(6),
      R => RST
    );
\slv_reg22_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg22[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg22(7),
      R => RST
    );
\slv_reg22_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg22[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg22(8),
      R => RST
    );
\slv_reg22_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg22[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg22(9),
      R => RST
    );
\slv_reg23[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000080000000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg7[31]_i_2_n_0\,
      I3 => s00_axi_wstrb(1),
      I4 => p_0_in(1),
      I5 => p_0_in(3),
      O => \slv_reg23[15]_i_1_n_0\
    );
\slv_reg23[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000080000000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg7[31]_i_2_n_0\,
      I3 => s00_axi_wstrb(2),
      I4 => p_0_in(1),
      I5 => p_0_in(3),
      O => \slv_reg23[23]_i_1_n_0\
    );
\slv_reg23[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000080000000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg7[31]_i_2_n_0\,
      I3 => s00_axi_wstrb(3),
      I4 => p_0_in(1),
      I5 => p_0_in(3),
      O => \slv_reg23[31]_i_1_n_0\
    );
\slv_reg23[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000080000000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg7[31]_i_2_n_0\,
      I3 => s00_axi_wstrb(0),
      I4 => p_0_in(1),
      I5 => p_0_in(3),
      O => \slv_reg23[7]_i_1_n_0\
    );
\slv_reg23_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg23[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg23(0),
      R => RST
    );
\slv_reg23_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg23[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => slv_reg23(10),
      R => RST
    );
\slv_reg23_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg23[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => slv_reg23(11),
      R => RST
    );
\slv_reg23_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg23[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => slv_reg23(12),
      R => RST
    );
\slv_reg23_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg23[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => slv_reg23(13),
      R => RST
    );
\slv_reg23_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg23[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => slv_reg23(14),
      R => RST
    );
\slv_reg23_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg23[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => slv_reg23(15),
      R => RST
    );
\slv_reg23_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg23[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => slv_reg23(16),
      R => RST
    );
\slv_reg23_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg23[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => slv_reg23(17),
      R => RST
    );
\slv_reg23_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg23[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => slv_reg23(18),
      R => RST
    );
\slv_reg23_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg23[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => slv_reg23(19),
      R => RST
    );
\slv_reg23_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg23[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => slv_reg23(1),
      R => RST
    );
\slv_reg23_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg23[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => slv_reg23(20),
      R => RST
    );
\slv_reg23_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg23[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => slv_reg23(21),
      R => RST
    );
\slv_reg23_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg23[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => slv_reg23(22),
      R => RST
    );
\slv_reg23_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg23[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => slv_reg23(23),
      R => RST
    );
\slv_reg23_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg23[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => slv_reg23(24),
      R => RST
    );
\slv_reg23_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg23[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => slv_reg23(25),
      R => RST
    );
\slv_reg23_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg23[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => slv_reg23(26),
      R => RST
    );
\slv_reg23_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg23[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => slv_reg23(27),
      R => RST
    );
\slv_reg23_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg23[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => slv_reg23(28),
      R => RST
    );
\slv_reg23_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg23[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => slv_reg23(29),
      R => RST
    );
\slv_reg23_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg23[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => slv_reg23(2),
      R => RST
    );
\slv_reg23_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg23[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => slv_reg23(30),
      R => RST
    );
\slv_reg23_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg23[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => slv_reg23(31),
      R => RST
    );
\slv_reg23_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg23[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg23(3),
      R => RST
    );
\slv_reg23_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg23[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg23(4),
      R => RST
    );
\slv_reg23_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg23[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg23(5),
      R => RST
    );
\slv_reg23_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg23[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg23(6),
      R => RST
    );
\slv_reg23_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg23[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg23(7),
      R => RST
    );
\slv_reg23_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg23[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg23(8),
      R => RST
    );
\slv_reg23_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg23[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg23(9),
      R => RST
    );
\slv_reg24[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0020000000000000"
    )
        port map (
      I0 => \slv_reg8[31]_i_2_n_0\,
      I1 => p_0_in(2),
      I2 => p_0_in(4),
      I3 => p_0_in(1),
      I4 => p_0_in(3),
      I5 => s00_axi_wstrb(1),
      O => \slv_reg24[15]_i_1_n_0\
    );
\slv_reg24[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0020000000000000"
    )
        port map (
      I0 => \slv_reg8[31]_i_2_n_0\,
      I1 => p_0_in(2),
      I2 => p_0_in(4),
      I3 => p_0_in(1),
      I4 => p_0_in(3),
      I5 => s00_axi_wstrb(2),
      O => \slv_reg24[23]_i_1_n_0\
    );
\slv_reg24[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0020000000000000"
    )
        port map (
      I0 => \slv_reg8[31]_i_2_n_0\,
      I1 => p_0_in(2),
      I2 => p_0_in(4),
      I3 => p_0_in(1),
      I4 => p_0_in(3),
      I5 => s00_axi_wstrb(3),
      O => \slv_reg24[31]_i_1_n_0\
    );
\slv_reg24[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0020000000000000"
    )
        port map (
      I0 => \slv_reg8[31]_i_2_n_0\,
      I1 => p_0_in(2),
      I2 => p_0_in(4),
      I3 => p_0_in(1),
      I4 => p_0_in(3),
      I5 => s00_axi_wstrb(0),
      O => \slv_reg24[7]_i_1_n_0\
    );
\slv_reg24_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg24[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg24(0),
      R => RST
    );
\slv_reg24_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg24[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => slv_reg24(10),
      R => RST
    );
\slv_reg24_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg24[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => slv_reg24(11),
      R => RST
    );
\slv_reg24_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg24[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => slv_reg24(12),
      R => RST
    );
\slv_reg24_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg24[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => slv_reg24(13),
      R => RST
    );
\slv_reg24_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg24[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => slv_reg24(14),
      R => RST
    );
\slv_reg24_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg24[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => slv_reg24(15),
      R => RST
    );
\slv_reg24_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg24[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => slv_reg24(16),
      R => RST
    );
\slv_reg24_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg24[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => slv_reg24(17),
      R => RST
    );
\slv_reg24_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg24[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => slv_reg24(18),
      R => RST
    );
\slv_reg24_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg24[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => slv_reg24(19),
      R => RST
    );
\slv_reg24_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg24[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => slv_reg24(1),
      R => RST
    );
\slv_reg24_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg24[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => slv_reg24(20),
      R => RST
    );
\slv_reg24_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg24[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => slv_reg24(21),
      R => RST
    );
\slv_reg24_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg24[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => slv_reg24(22),
      R => RST
    );
\slv_reg24_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg24[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => slv_reg24(23),
      R => RST
    );
\slv_reg24_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg24[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => slv_reg24(24),
      R => RST
    );
\slv_reg24_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg24[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => slv_reg24(25),
      R => RST
    );
\slv_reg24_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg24[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => slv_reg24(26),
      R => RST
    );
\slv_reg24_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg24[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => slv_reg24(27),
      R => RST
    );
\slv_reg24_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg24[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => slv_reg24(28),
      R => RST
    );
\slv_reg24_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg24[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => slv_reg24(29),
      R => RST
    );
\slv_reg24_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg24[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => slv_reg24(2),
      R => RST
    );
\slv_reg24_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg24[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => slv_reg24(30),
      R => RST
    );
\slv_reg24_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg24[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => slv_reg24(31),
      R => RST
    );
\slv_reg24_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg24[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg24(3),
      R => RST
    );
\slv_reg24_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg24[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg24(4),
      R => RST
    );
\slv_reg24_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg24[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg24(5),
      R => RST
    );
\slv_reg24_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg24[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg24(6),
      R => RST
    );
\slv_reg24_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg24[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg24(7),
      R => RST
    );
\slv_reg24_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg24[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg24(8),
      R => RST
    );
\slv_reg24_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg24[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg24(9),
      R => RST
    );
\slv_reg25[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0020000000000000"
    )
        port map (
      I0 => \slv_reg7[31]_i_2_n_0\,
      I1 => p_0_in(2),
      I2 => p_0_in(4),
      I3 => p_0_in(1),
      I4 => p_0_in(3),
      I5 => s00_axi_wstrb(1),
      O => \slv_reg25[15]_i_1_n_0\
    );
\slv_reg25[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0020000000000000"
    )
        port map (
      I0 => \slv_reg7[31]_i_2_n_0\,
      I1 => p_0_in(2),
      I2 => p_0_in(4),
      I3 => p_0_in(1),
      I4 => p_0_in(3),
      I5 => s00_axi_wstrb(2),
      O => \slv_reg25[23]_i_1_n_0\
    );
\slv_reg25[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0020000000000000"
    )
        port map (
      I0 => \slv_reg7[31]_i_2_n_0\,
      I1 => p_0_in(2),
      I2 => p_0_in(4),
      I3 => p_0_in(1),
      I4 => p_0_in(3),
      I5 => s00_axi_wstrb(3),
      O => \slv_reg25[31]_i_1_n_0\
    );
\slv_reg25[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0020000000000000"
    )
        port map (
      I0 => \slv_reg7[31]_i_2_n_0\,
      I1 => p_0_in(2),
      I2 => p_0_in(4),
      I3 => p_0_in(1),
      I4 => p_0_in(3),
      I5 => s00_axi_wstrb(0),
      O => \slv_reg25[7]_i_1_n_0\
    );
\slv_reg25_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg25[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg25(0),
      R => RST
    );
\slv_reg25_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg25[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => slv_reg25(10),
      R => RST
    );
\slv_reg25_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg25[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => slv_reg25(11),
      R => RST
    );
\slv_reg25_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg25[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => slv_reg25(12),
      R => RST
    );
\slv_reg25_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg25[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => slv_reg25(13),
      R => RST
    );
\slv_reg25_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg25[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => slv_reg25(14),
      R => RST
    );
\slv_reg25_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg25[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => slv_reg25(15),
      R => RST
    );
\slv_reg25_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg25[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => slv_reg25(16),
      R => RST
    );
\slv_reg25_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg25[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => slv_reg25(17),
      R => RST
    );
\slv_reg25_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg25[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => slv_reg25(18),
      R => RST
    );
\slv_reg25_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg25[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => slv_reg25(19),
      R => RST
    );
\slv_reg25_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg25[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => slv_reg25(1),
      R => RST
    );
\slv_reg25_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg25[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => slv_reg25(20),
      R => RST
    );
\slv_reg25_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg25[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => slv_reg25(21),
      R => RST
    );
\slv_reg25_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg25[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => slv_reg25(22),
      R => RST
    );
\slv_reg25_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg25[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => slv_reg25(23),
      R => RST
    );
\slv_reg25_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg25[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => slv_reg25(24),
      R => RST
    );
\slv_reg25_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg25[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => slv_reg25(25),
      R => RST
    );
\slv_reg25_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg25[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => slv_reg25(26),
      R => RST
    );
\slv_reg25_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg25[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => slv_reg25(27),
      R => RST
    );
\slv_reg25_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg25[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => slv_reg25(28),
      R => RST
    );
\slv_reg25_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg25[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => slv_reg25(29),
      R => RST
    );
\slv_reg25_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg25[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => slv_reg25(2),
      R => RST
    );
\slv_reg25_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg25[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => slv_reg25(30),
      R => RST
    );
\slv_reg25_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg25[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => slv_reg25(31),
      R => RST
    );
\slv_reg25_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg25[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg25(3),
      R => RST
    );
\slv_reg25_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg25[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg25(4),
      R => RST
    );
\slv_reg25_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg25[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg25(5),
      R => RST
    );
\slv_reg25_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg25[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg25(6),
      R => RST
    );
\slv_reg25_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg25[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg25(7),
      R => RST
    );
\slv_reg25_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg25[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg25(8),
      R => RST
    );
\slv_reg25_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg25[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg25(9),
      R => RST
    );
\slv_reg26[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2000000000000000"
    )
        port map (
      I0 => \slv_reg8[31]_i_2_n_0\,
      I1 => p_0_in(2),
      I2 => p_0_in(4),
      I3 => p_0_in(3),
      I4 => p_0_in(1),
      I5 => s00_axi_wstrb(1),
      O => \slv_reg26[15]_i_1_n_0\
    );
\slv_reg26[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2000000000000000"
    )
        port map (
      I0 => \slv_reg8[31]_i_2_n_0\,
      I1 => p_0_in(2),
      I2 => p_0_in(4),
      I3 => p_0_in(3),
      I4 => p_0_in(1),
      I5 => s00_axi_wstrb(2),
      O => \slv_reg26[23]_i_1_n_0\
    );
\slv_reg26[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2000000000000000"
    )
        port map (
      I0 => \slv_reg8[31]_i_2_n_0\,
      I1 => p_0_in(2),
      I2 => p_0_in(4),
      I3 => p_0_in(3),
      I4 => p_0_in(1),
      I5 => s00_axi_wstrb(3),
      O => \slv_reg26[31]_i_1_n_0\
    );
\slv_reg26[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2000000000000000"
    )
        port map (
      I0 => \slv_reg8[31]_i_2_n_0\,
      I1 => p_0_in(2),
      I2 => p_0_in(4),
      I3 => p_0_in(3),
      I4 => p_0_in(1),
      I5 => s00_axi_wstrb(0),
      O => \slv_reg26[7]_i_1_n_0\
    );
\slv_reg26_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg26[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg26(0),
      R => RST
    );
\slv_reg26_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg26[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => slv_reg26(10),
      R => RST
    );
\slv_reg26_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg26[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => slv_reg26(11),
      R => RST
    );
\slv_reg26_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg26[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => slv_reg26(12),
      R => RST
    );
\slv_reg26_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg26[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => slv_reg26(13),
      R => RST
    );
\slv_reg26_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg26[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => slv_reg26(14),
      R => RST
    );
\slv_reg26_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg26[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => slv_reg26(15),
      R => RST
    );
\slv_reg26_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg26[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => slv_reg26(16),
      R => RST
    );
\slv_reg26_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg26[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => slv_reg26(17),
      R => RST
    );
\slv_reg26_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg26[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => slv_reg26(18),
      R => RST
    );
\slv_reg26_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg26[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => slv_reg26(19),
      R => RST
    );
\slv_reg26_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg26[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => slv_reg26(1),
      R => RST
    );
\slv_reg26_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg26[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => slv_reg26(20),
      R => RST
    );
\slv_reg26_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg26[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => slv_reg26(21),
      R => RST
    );
\slv_reg26_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg26[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => slv_reg26(22),
      R => RST
    );
\slv_reg26_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg26[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => slv_reg26(23),
      R => RST
    );
\slv_reg26_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg26[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => slv_reg26(24),
      R => RST
    );
\slv_reg26_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg26[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => slv_reg26(25),
      R => RST
    );
\slv_reg26_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg26[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => slv_reg26(26),
      R => RST
    );
\slv_reg26_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg26[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => slv_reg26(27),
      R => RST
    );
\slv_reg26_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg26[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => slv_reg26(28),
      R => RST
    );
\slv_reg26_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg26[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => slv_reg26(29),
      R => RST
    );
\slv_reg26_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg26[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => slv_reg26(2),
      R => RST
    );
\slv_reg26_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg26[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => slv_reg26(30),
      R => RST
    );
\slv_reg26_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg26[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => slv_reg26(31),
      R => RST
    );
\slv_reg26_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg26[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg26(3),
      R => RST
    );
\slv_reg26_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg26[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg26(4),
      R => RST
    );
\slv_reg26_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg26[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg26(5),
      R => RST
    );
\slv_reg26_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg26[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg26(6),
      R => RST
    );
\slv_reg26_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg26[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg26(7),
      R => RST
    );
\slv_reg26_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg26[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg26(8),
      R => RST
    );
\slv_reg26_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg26[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg26(9),
      R => RST
    );
\slv_reg27[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2000000000000000"
    )
        port map (
      I0 => \slv_reg7[31]_i_2_n_0\,
      I1 => p_0_in(2),
      I2 => p_0_in(4),
      I3 => p_0_in(3),
      I4 => p_0_in(1),
      I5 => s00_axi_wstrb(1),
      O => \slv_reg27[15]_i_1_n_0\
    );
\slv_reg27[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2000000000000000"
    )
        port map (
      I0 => \slv_reg7[31]_i_2_n_0\,
      I1 => p_0_in(2),
      I2 => p_0_in(4),
      I3 => p_0_in(3),
      I4 => p_0_in(1),
      I5 => s00_axi_wstrb(2),
      O => \slv_reg27[23]_i_1_n_0\
    );
\slv_reg27[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2000000000000000"
    )
        port map (
      I0 => \slv_reg7[31]_i_2_n_0\,
      I1 => p_0_in(2),
      I2 => p_0_in(4),
      I3 => p_0_in(3),
      I4 => p_0_in(1),
      I5 => s00_axi_wstrb(3),
      O => \slv_reg27[31]_i_1_n_0\
    );
\slv_reg27[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2000000000000000"
    )
        port map (
      I0 => \slv_reg7[31]_i_2_n_0\,
      I1 => p_0_in(2),
      I2 => p_0_in(4),
      I3 => p_0_in(3),
      I4 => p_0_in(1),
      I5 => s00_axi_wstrb(0),
      O => \slv_reg27[7]_i_1_n_0\
    );
\slv_reg27_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg27[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg27(0),
      R => RST
    );
\slv_reg27_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg27[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => slv_reg27(10),
      R => RST
    );
\slv_reg27_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg27[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => slv_reg27(11),
      R => RST
    );
\slv_reg27_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg27[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => slv_reg27(12),
      R => RST
    );
\slv_reg27_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg27[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => slv_reg27(13),
      R => RST
    );
\slv_reg27_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg27[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => slv_reg27(14),
      R => RST
    );
\slv_reg27_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg27[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => slv_reg27(15),
      R => RST
    );
\slv_reg27_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg27[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => slv_reg27(16),
      R => RST
    );
\slv_reg27_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg27[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => slv_reg27(17),
      R => RST
    );
\slv_reg27_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg27[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => slv_reg27(18),
      R => RST
    );
\slv_reg27_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg27[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => slv_reg27(19),
      R => RST
    );
\slv_reg27_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg27[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => slv_reg27(1),
      R => RST
    );
\slv_reg27_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg27[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => slv_reg27(20),
      R => RST
    );
\slv_reg27_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg27[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => slv_reg27(21),
      R => RST
    );
\slv_reg27_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg27[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => slv_reg27(22),
      R => RST
    );
\slv_reg27_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg27[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => slv_reg27(23),
      R => RST
    );
\slv_reg27_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg27[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => slv_reg27(24),
      R => RST
    );
\slv_reg27_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg27[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => slv_reg27(25),
      R => RST
    );
\slv_reg27_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg27[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => slv_reg27(26),
      R => RST
    );
\slv_reg27_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg27[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => slv_reg27(27),
      R => RST
    );
\slv_reg27_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg27[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => slv_reg27(28),
      R => RST
    );
\slv_reg27_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg27[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => slv_reg27(29),
      R => RST
    );
\slv_reg27_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg27[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => slv_reg27(2),
      R => RST
    );
\slv_reg27_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg27[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => slv_reg27(30),
      R => RST
    );
\slv_reg27_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg27[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => slv_reg27(31),
      R => RST
    );
\slv_reg27_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg27[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg27(3),
      R => RST
    );
\slv_reg27_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg27[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg27(4),
      R => RST
    );
\slv_reg27_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg27[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg27(5),
      R => RST
    );
\slv_reg27_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg27[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg27(6),
      R => RST
    );
\slv_reg27_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg27[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg27(7),
      R => RST
    );
\slv_reg27_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg27[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg27(8),
      R => RST
    );
\slv_reg27_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg27[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg27(9),
      R => RST
    );
\slv_reg28[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000080000000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg8[31]_i_2_n_0\,
      I3 => s00_axi_wstrb(1),
      I4 => p_0_in(3),
      I5 => p_0_in(1),
      O => \slv_reg28[15]_i_1_n_0\
    );
\slv_reg28[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000080000000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg8[31]_i_2_n_0\,
      I3 => s00_axi_wstrb(2),
      I4 => p_0_in(3),
      I5 => p_0_in(1),
      O => \slv_reg28[23]_i_1_n_0\
    );
\slv_reg28[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000080000000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg8[31]_i_2_n_0\,
      I3 => s00_axi_wstrb(3),
      I4 => p_0_in(3),
      I5 => p_0_in(1),
      O => \slv_reg28[31]_i_1_n_0\
    );
\slv_reg28[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000080000000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg8[31]_i_2_n_0\,
      I3 => s00_axi_wstrb(0),
      I4 => p_0_in(3),
      I5 => p_0_in(1),
      O => \slv_reg28[7]_i_1_n_0\
    );
\slv_reg28_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg28[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg28(0),
      R => RST
    );
\slv_reg28_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg28[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => slv_reg28(10),
      R => RST
    );
\slv_reg28_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg28[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => slv_reg28(11),
      R => RST
    );
\slv_reg28_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg28[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => slv_reg28(12),
      R => RST
    );
\slv_reg28_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg28[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => slv_reg28(13),
      R => RST
    );
\slv_reg28_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg28[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => slv_reg28(14),
      R => RST
    );
\slv_reg28_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg28[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => slv_reg28(15),
      R => RST
    );
\slv_reg28_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg28[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => slv_reg28(16),
      R => RST
    );
\slv_reg28_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg28[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => slv_reg28(17),
      R => RST
    );
\slv_reg28_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg28[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => slv_reg28(18),
      R => RST
    );
\slv_reg28_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg28[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => slv_reg28(19),
      R => RST
    );
\slv_reg28_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg28[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => slv_reg28(1),
      R => RST
    );
\slv_reg28_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg28[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => slv_reg28(20),
      R => RST
    );
\slv_reg28_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg28[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => slv_reg28(21),
      R => RST
    );
\slv_reg28_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg28[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => slv_reg28(22),
      R => RST
    );
\slv_reg28_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg28[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => slv_reg28(23),
      R => RST
    );
\slv_reg28_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg28[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => slv_reg28(24),
      R => RST
    );
\slv_reg28_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg28[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => slv_reg28(25),
      R => RST
    );
\slv_reg28_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg28[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => slv_reg28(26),
      R => RST
    );
\slv_reg28_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg28[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => slv_reg28(27),
      R => RST
    );
\slv_reg28_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg28[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => slv_reg28(28),
      R => RST
    );
\slv_reg28_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg28[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => slv_reg28(29),
      R => RST
    );
\slv_reg28_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg28[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => slv_reg28(2),
      R => RST
    );
\slv_reg28_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg28[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => slv_reg28(30),
      R => RST
    );
\slv_reg28_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg28[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => slv_reg28(31),
      R => RST
    );
\slv_reg28_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg28[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg28(3),
      R => RST
    );
\slv_reg28_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg28[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg28(4),
      R => RST
    );
\slv_reg28_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg28[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg28(5),
      R => RST
    );
\slv_reg28_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg28[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg28(6),
      R => RST
    );
\slv_reg28_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg28[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg28(7),
      R => RST
    );
\slv_reg28_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg28[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg28(8),
      R => RST
    );
\slv_reg28_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg28[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg28(9),
      R => RST
    );
\slv_reg29[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000080000000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg7[31]_i_2_n_0\,
      I3 => s00_axi_wstrb(1),
      I4 => p_0_in(3),
      I5 => p_0_in(1),
      O => \slv_reg29[15]_i_1_n_0\
    );
\slv_reg29[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000080000000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg7[31]_i_2_n_0\,
      I3 => s00_axi_wstrb(2),
      I4 => p_0_in(3),
      I5 => p_0_in(1),
      O => \slv_reg29[23]_i_1_n_0\
    );
\slv_reg29[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000080000000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg7[31]_i_2_n_0\,
      I3 => s00_axi_wstrb(3),
      I4 => p_0_in(3),
      I5 => p_0_in(1),
      O => \slv_reg29[31]_i_1_n_0\
    );
\slv_reg29[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000080000000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg7[31]_i_2_n_0\,
      I3 => s00_axi_wstrb(0),
      I4 => p_0_in(3),
      I5 => p_0_in(1),
      O => \slv_reg29[7]_i_1_n_0\
    );
\slv_reg29_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg29[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg29(0),
      R => RST
    );
\slv_reg29_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg29[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => slv_reg29(10),
      R => RST
    );
\slv_reg29_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg29[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => slv_reg29(11),
      R => RST
    );
\slv_reg29_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg29[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => slv_reg29(12),
      R => RST
    );
\slv_reg29_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg29[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => slv_reg29(13),
      R => RST
    );
\slv_reg29_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg29[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => slv_reg29(14),
      R => RST
    );
\slv_reg29_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg29[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => slv_reg29(15),
      R => RST
    );
\slv_reg29_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg29[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => slv_reg29(16),
      R => RST
    );
\slv_reg29_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg29[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => slv_reg29(17),
      R => RST
    );
\slv_reg29_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg29[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => slv_reg29(18),
      R => RST
    );
\slv_reg29_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg29[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => slv_reg29(19),
      R => RST
    );
\slv_reg29_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg29[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => slv_reg29(1),
      R => RST
    );
\slv_reg29_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg29[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => slv_reg29(20),
      R => RST
    );
\slv_reg29_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg29[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => slv_reg29(21),
      R => RST
    );
\slv_reg29_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg29[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => slv_reg29(22),
      R => RST
    );
\slv_reg29_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg29[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => slv_reg29(23),
      R => RST
    );
\slv_reg29_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg29[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => slv_reg29(24),
      R => RST
    );
\slv_reg29_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg29[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => slv_reg29(25),
      R => RST
    );
\slv_reg29_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg29[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => slv_reg29(26),
      R => RST
    );
\slv_reg29_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg29[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => slv_reg29(27),
      R => RST
    );
\slv_reg29_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg29[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => slv_reg29(28),
      R => RST
    );
\slv_reg29_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg29[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => slv_reg29(29),
      R => RST
    );
\slv_reg29_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg29[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => slv_reg29(2),
      R => RST
    );
\slv_reg29_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg29[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => slv_reg29(30),
      R => RST
    );
\slv_reg29_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg29[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => slv_reg29(31),
      R => RST
    );
\slv_reg29_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg29[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg29(3),
      R => RST
    );
\slv_reg29_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg29[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg29(4),
      R => RST
    );
\slv_reg29_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg29[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg29(5),
      R => RST
    );
\slv_reg29_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg29[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg29(6),
      R => RST
    );
\slv_reg29_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg29[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg29(7),
      R => RST
    );
\slv_reg29_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg29[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg29(8),
      R => RST
    );
\slv_reg29_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg29[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg29(9),
      R => RST
    );
\slv_reg2[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000010000000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg8[31]_i_2_n_0\,
      I3 => s00_axi_wstrb(1),
      I4 => p_0_in(1),
      I5 => p_0_in(3),
      O => \slv_reg2[15]_i_1_n_0\
    );
\slv_reg2[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000010000000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg8[31]_i_2_n_0\,
      I3 => s00_axi_wstrb(2),
      I4 => p_0_in(1),
      I5 => p_0_in(3),
      O => \slv_reg2[23]_i_1_n_0\
    );
\slv_reg2[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000010000000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg8[31]_i_2_n_0\,
      I3 => s00_axi_wstrb(3),
      I4 => p_0_in(1),
      I5 => p_0_in(3),
      O => \slv_reg2[31]_i_1_n_0\
    );
\slv_reg2[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000010000000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg8[31]_i_2_n_0\,
      I3 => s00_axi_wstrb(0),
      I4 => p_0_in(1),
      I5 => p_0_in(3),
      O => \slv_reg2[7]_i_1_n_0\
    );
\slv_reg2_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg2(0),
      R => RST
    );
\slv_reg2_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => slv_reg2(10),
      R => RST
    );
\slv_reg2_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => slv_reg2(11),
      R => RST
    );
\slv_reg2_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => slv_reg2(12),
      R => RST
    );
\slv_reg2_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => slv_reg2(13),
      R => RST
    );
\slv_reg2_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => slv_reg2(14),
      R => RST
    );
\slv_reg2_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => slv_reg2(15),
      R => RST
    );
\slv_reg2_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => slv_reg2(16),
      R => RST
    );
\slv_reg2_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => slv_reg2(17),
      R => RST
    );
\slv_reg2_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => slv_reg2(18),
      R => RST
    );
\slv_reg2_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => slv_reg2(19),
      R => RST
    );
\slv_reg2_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => slv_reg2(1),
      R => RST
    );
\slv_reg2_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => slv_reg2(20),
      R => RST
    );
\slv_reg2_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => slv_reg2(21),
      R => RST
    );
\slv_reg2_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => slv_reg2(22),
      R => RST
    );
\slv_reg2_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => slv_reg2(23),
      R => RST
    );
\slv_reg2_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => slv_reg2(24),
      R => RST
    );
\slv_reg2_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => slv_reg2(25),
      R => RST
    );
\slv_reg2_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => slv_reg2(26),
      R => RST
    );
\slv_reg2_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => slv_reg2(27),
      R => RST
    );
\slv_reg2_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => slv_reg2(28),
      R => RST
    );
\slv_reg2_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => slv_reg2(29),
      R => RST
    );
\slv_reg2_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => slv_reg2(2),
      R => RST
    );
\slv_reg2_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => slv_reg2(30),
      R => RST
    );
\slv_reg2_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => slv_reg2(31),
      R => RST
    );
\slv_reg2_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg2(3),
      R => RST
    );
\slv_reg2_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg2(4),
      R => RST
    );
\slv_reg2_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg2(5),
      R => RST
    );
\slv_reg2_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg2(6),
      R => RST
    );
\slv_reg2_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg2(7),
      R => RST
    );
\slv_reg2_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg2(8),
      R => RST
    );
\slv_reg2_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg2(9),
      R => RST
    );
\slv_reg30[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg8[31]_i_2_n_0\,
      I3 => p_0_in(1),
      I4 => p_0_in(3),
      I5 => s00_axi_wstrb(1),
      O => \slv_reg30[15]_i_1_n_0\
    );
\slv_reg30[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg8[31]_i_2_n_0\,
      I3 => p_0_in(1),
      I4 => p_0_in(3),
      I5 => s00_axi_wstrb(2),
      O => \slv_reg30[23]_i_1_n_0\
    );
\slv_reg30[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg8[31]_i_2_n_0\,
      I3 => p_0_in(1),
      I4 => p_0_in(3),
      I5 => s00_axi_wstrb(3),
      O => \slv_reg30[31]_i_1_n_0\
    );
\slv_reg30[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg8[31]_i_2_n_0\,
      I3 => p_0_in(1),
      I4 => p_0_in(3),
      I5 => s00_axi_wstrb(0),
      O => \slv_reg30[7]_i_1_n_0\
    );
\slv_reg30_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg30[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg30(0),
      R => RST
    );
\slv_reg30_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg30[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => slv_reg30(10),
      R => RST
    );
\slv_reg30_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg30[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => slv_reg30(11),
      R => RST
    );
\slv_reg30_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg30[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => slv_reg30(12),
      R => RST
    );
\slv_reg30_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg30[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => slv_reg30(13),
      R => RST
    );
\slv_reg30_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg30[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => slv_reg30(14),
      R => RST
    );
\slv_reg30_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg30[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => slv_reg30(15),
      R => RST
    );
\slv_reg30_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg30[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => slv_reg30(16),
      R => RST
    );
\slv_reg30_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg30[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => slv_reg30(17),
      R => RST
    );
\slv_reg30_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg30[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => slv_reg30(18),
      R => RST
    );
\slv_reg30_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg30[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => slv_reg30(19),
      R => RST
    );
\slv_reg30_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg30[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => slv_reg30(1),
      R => RST
    );
\slv_reg30_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg30[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => slv_reg30(20),
      R => RST
    );
\slv_reg30_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg30[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => slv_reg30(21),
      R => RST
    );
\slv_reg30_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg30[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => slv_reg30(22),
      R => RST
    );
\slv_reg30_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg30[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => slv_reg30(23),
      R => RST
    );
\slv_reg30_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg30[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => slv_reg30(24),
      R => RST
    );
\slv_reg30_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg30[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => slv_reg30(25),
      R => RST
    );
\slv_reg30_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg30[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => slv_reg30(26),
      R => RST
    );
\slv_reg30_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg30[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => slv_reg30(27),
      R => RST
    );
\slv_reg30_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg30[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => slv_reg30(28),
      R => RST
    );
\slv_reg30_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg30[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => slv_reg30(29),
      R => RST
    );
\slv_reg30_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg30[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => slv_reg30(2),
      R => RST
    );
\slv_reg30_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg30[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => slv_reg30(30),
      R => RST
    );
\slv_reg30_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg30[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => slv_reg30(31),
      R => RST
    );
\slv_reg30_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg30[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg30(3),
      R => RST
    );
\slv_reg30_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg30[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg30(4),
      R => RST
    );
\slv_reg30_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg30[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg30(5),
      R => RST
    );
\slv_reg30_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg30[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg30(6),
      R => RST
    );
\slv_reg30_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg30[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg30(7),
      R => RST
    );
\slv_reg30_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg30[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg30(8),
      R => RST
    );
\slv_reg30_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg30[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg30(9),
      R => RST
    );
\slv_reg31[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg7[31]_i_2_n_0\,
      I3 => p_0_in(1),
      I4 => p_0_in(3),
      I5 => s00_axi_wstrb(1),
      O => \slv_reg31[15]_i_1_n_0\
    );
\slv_reg31[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg7[31]_i_2_n_0\,
      I3 => p_0_in(1),
      I4 => p_0_in(3),
      I5 => s00_axi_wstrb(2),
      O => \slv_reg31[23]_i_1_n_0\
    );
\slv_reg31[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg7[31]_i_2_n_0\,
      I3 => p_0_in(1),
      I4 => p_0_in(3),
      I5 => s00_axi_wstrb(3),
      O => \slv_reg31[31]_i_1_n_0\
    );
\slv_reg31[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg7[31]_i_2_n_0\,
      I3 => p_0_in(1),
      I4 => p_0_in(3),
      I5 => s00_axi_wstrb(0),
      O => \slv_reg31[7]_i_1_n_0\
    );
\slv_reg31_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg31[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg31(0),
      R => RST
    );
\slv_reg31_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg31[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => slv_reg31(10),
      R => RST
    );
\slv_reg31_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg31[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => slv_reg31(11),
      R => RST
    );
\slv_reg31_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg31[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => slv_reg31(12),
      R => RST
    );
\slv_reg31_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg31[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => slv_reg31(13),
      R => RST
    );
\slv_reg31_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg31[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => slv_reg31(14),
      R => RST
    );
\slv_reg31_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg31[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => slv_reg31(15),
      R => RST
    );
\slv_reg31_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg31[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => slv_reg31(16),
      R => RST
    );
\slv_reg31_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg31[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => slv_reg31(17),
      R => RST
    );
\slv_reg31_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg31[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => slv_reg31(18),
      R => RST
    );
\slv_reg31_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg31[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => slv_reg31(19),
      R => RST
    );
\slv_reg31_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg31[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => slv_reg31(1),
      R => RST
    );
\slv_reg31_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg31[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => slv_reg31(20),
      R => RST
    );
\slv_reg31_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg31[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => slv_reg31(21),
      R => RST
    );
\slv_reg31_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg31[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => slv_reg31(22),
      R => RST
    );
\slv_reg31_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg31[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => slv_reg31(23),
      R => RST
    );
\slv_reg31_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg31[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => slv_reg31(24),
      R => RST
    );
\slv_reg31_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg31[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => slv_reg31(25),
      R => RST
    );
\slv_reg31_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg31[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => slv_reg31(26),
      R => RST
    );
\slv_reg31_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg31[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => slv_reg31(27),
      R => RST
    );
\slv_reg31_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg31[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => slv_reg31(28),
      R => RST
    );
\slv_reg31_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg31[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => slv_reg31(29),
      R => RST
    );
\slv_reg31_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg31[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => slv_reg31(2),
      R => RST
    );
\slv_reg31_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg31[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => slv_reg31(30),
      R => RST
    );
\slv_reg31_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg31[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => slv_reg31(31),
      R => RST
    );
\slv_reg31_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg31[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg31(3),
      R => RST
    );
\slv_reg31_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg31[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg31(4),
      R => RST
    );
\slv_reg31_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg31[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg31(5),
      R => RST
    );
\slv_reg31_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg31[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg31(6),
      R => RST
    );
\slv_reg31_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg31[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg31(7),
      R => RST
    );
\slv_reg31_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg31[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg31(8),
      R => RST
    );
\slv_reg31_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg31[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg31(9),
      R => RST
    );
\slv_reg3[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000010000000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg7[31]_i_2_n_0\,
      I3 => s00_axi_wstrb(1),
      I4 => p_0_in(1),
      I5 => p_0_in(3),
      O => \slv_reg3[15]_i_1_n_0\
    );
\slv_reg3[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000010000000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg7[31]_i_2_n_0\,
      I3 => s00_axi_wstrb(2),
      I4 => p_0_in(1),
      I5 => p_0_in(3),
      O => \slv_reg3[23]_i_1_n_0\
    );
\slv_reg3[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000010000000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg7[31]_i_2_n_0\,
      I3 => s00_axi_wstrb(3),
      I4 => p_0_in(1),
      I5 => p_0_in(3),
      O => \slv_reg3[31]_i_1_n_0\
    );
\slv_reg3[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000010000000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg7[31]_i_2_n_0\,
      I3 => s00_axi_wstrb(0),
      I4 => p_0_in(1),
      I5 => p_0_in(3),
      O => \slv_reg3[7]_i_1_n_0\
    );
\slv_reg3_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg3(0),
      R => RST
    );
\slv_reg3_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => slv_reg3(10),
      R => RST
    );
\slv_reg3_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => slv_reg3(11),
      R => RST
    );
\slv_reg3_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => slv_reg3(12),
      R => RST
    );
\slv_reg3_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => slv_reg3(13),
      R => RST
    );
\slv_reg3_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => slv_reg3(14),
      R => RST
    );
\slv_reg3_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => slv_reg3(15),
      R => RST
    );
\slv_reg3_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => slv_reg3(16),
      R => RST
    );
\slv_reg3_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => slv_reg3(17),
      R => RST
    );
\slv_reg3_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => slv_reg3(18),
      R => RST
    );
\slv_reg3_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => slv_reg3(19),
      R => RST
    );
\slv_reg3_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => slv_reg3(1),
      R => RST
    );
\slv_reg3_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => slv_reg3(20),
      R => RST
    );
\slv_reg3_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => slv_reg3(21),
      R => RST
    );
\slv_reg3_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => slv_reg3(22),
      R => RST
    );
\slv_reg3_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => slv_reg3(23),
      R => RST
    );
\slv_reg3_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => slv_reg3(24),
      R => RST
    );
\slv_reg3_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => slv_reg3(25),
      R => RST
    );
\slv_reg3_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => slv_reg3(26),
      R => RST
    );
\slv_reg3_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => slv_reg3(27),
      R => RST
    );
\slv_reg3_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => slv_reg3(28),
      R => RST
    );
\slv_reg3_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => slv_reg3(29),
      R => RST
    );
\slv_reg3_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => slv_reg3(2),
      R => RST
    );
\slv_reg3_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => slv_reg3(30),
      R => RST
    );
\slv_reg3_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => slv_reg3(31),
      R => RST
    );
\slv_reg3_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg3(3),
      R => RST
    );
\slv_reg3_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg3(4),
      R => RST
    );
\slv_reg3_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg3(5),
      R => RST
    );
\slv_reg3_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg3(6),
      R => RST
    );
\slv_reg3_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg3(7),
      R => RST
    );
\slv_reg3_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg3(8),
      R => RST
    );
\slv_reg3_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg3(9),
      R => RST
    );
\slv_reg4[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000004000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg8[31]_i_2_n_0\,
      I3 => s00_axi_wstrb(1),
      I4 => p_0_in(1),
      I5 => p_0_in(3),
      O => \slv_reg4[15]_i_1_n_0\
    );
\slv_reg4[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000004000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg8[31]_i_2_n_0\,
      I3 => s00_axi_wstrb(2),
      I4 => p_0_in(1),
      I5 => p_0_in(3),
      O => \slv_reg4[23]_i_1_n_0\
    );
\slv_reg4[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000004000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg8[31]_i_2_n_0\,
      I3 => s00_axi_wstrb(3),
      I4 => p_0_in(1),
      I5 => p_0_in(3),
      O => \slv_reg4[31]_i_1_n_0\
    );
\slv_reg4[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000004000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg8[31]_i_2_n_0\,
      I3 => s00_axi_wstrb(0),
      I4 => p_0_in(1),
      I5 => p_0_in(3),
      O => \slv_reg4[7]_i_1_n_0\
    );
\slv_reg4_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg4(0),
      R => RST
    );
\slv_reg4_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => slv_reg4(10),
      R => RST
    );
\slv_reg4_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => slv_reg4(11),
      R => RST
    );
\slv_reg4_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => slv_reg4(12),
      R => RST
    );
\slv_reg4_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => slv_reg4(13),
      R => RST
    );
\slv_reg4_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => slv_reg4(14),
      R => RST
    );
\slv_reg4_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => slv_reg4(15),
      R => RST
    );
\slv_reg4_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => slv_reg4(16),
      R => RST
    );
\slv_reg4_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => slv_reg4(17),
      R => RST
    );
\slv_reg4_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => slv_reg4(18),
      R => RST
    );
\slv_reg4_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => slv_reg4(19),
      R => RST
    );
\slv_reg4_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => slv_reg4(1),
      R => RST
    );
\slv_reg4_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => slv_reg4(20),
      R => RST
    );
\slv_reg4_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => slv_reg4(21),
      R => RST
    );
\slv_reg4_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => slv_reg4(22),
      R => RST
    );
\slv_reg4_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => slv_reg4(23),
      R => RST
    );
\slv_reg4_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => slv_reg4(24),
      R => RST
    );
\slv_reg4_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => slv_reg4(25),
      R => RST
    );
\slv_reg4_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => slv_reg4(26),
      R => RST
    );
\slv_reg4_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => slv_reg4(27),
      R => RST
    );
\slv_reg4_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => slv_reg4(28),
      R => RST
    );
\slv_reg4_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => slv_reg4(29),
      R => RST
    );
\slv_reg4_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => slv_reg4(2),
      R => RST
    );
\slv_reg4_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => slv_reg4(30),
      R => RST
    );
\slv_reg4_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => slv_reg4(31),
      R => RST
    );
\slv_reg4_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg4(3),
      R => RST
    );
\slv_reg4_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg4(4),
      R => RST
    );
\slv_reg4_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg4(5),
      R => RST
    );
\slv_reg4_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg4(6),
      R => RST
    );
\slv_reg4_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg4(7),
      R => RST
    );
\slv_reg4_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg4(8),
      R => RST
    );
\slv_reg4_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg4(9),
      R => RST
    );
\slv_reg7[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000008000000"
    )
        port map (
      I0 => \slv_reg7[31]_i_2_n_0\,
      I1 => p_0_in(2),
      I2 => p_0_in(4),
      I3 => s00_axi_wstrb(1),
      I4 => p_0_in(1),
      I5 => p_0_in(3),
      O => \slv_reg7[15]_i_1_n_0\
    );
\slv_reg7[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000008000000"
    )
        port map (
      I0 => \slv_reg7[31]_i_2_n_0\,
      I1 => p_0_in(2),
      I2 => p_0_in(4),
      I3 => s00_axi_wstrb(2),
      I4 => p_0_in(1),
      I5 => p_0_in(3),
      O => \slv_reg7[23]_i_1_n_0\
    );
\slv_reg7[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000008000000"
    )
        port map (
      I0 => \slv_reg7[31]_i_2_n_0\,
      I1 => p_0_in(2),
      I2 => p_0_in(4),
      I3 => s00_axi_wstrb(3),
      I4 => p_0_in(1),
      I5 => p_0_in(3),
      O => \slv_reg7[31]_i_1_n_0\
    );
\slv_reg7[31]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80000000"
    )
        port map (
      I0 => s00_axi_awvalid,
      I1 => s00_axi_wvalid,
      I2 => \^s00_axi_awready\,
      I3 => \^s00_axi_wready\,
      I4 => p_0_in(0),
      O => \slv_reg7[31]_i_2_n_0\
    );
\slv_reg7[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000008000000"
    )
        port map (
      I0 => \slv_reg7[31]_i_2_n_0\,
      I1 => p_0_in(2),
      I2 => p_0_in(4),
      I3 => s00_axi_wstrb(0),
      I4 => p_0_in(1),
      I5 => p_0_in(3),
      O => \slv_reg7[7]_i_1_n_0\
    );
\slv_reg7_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg7(0),
      R => RST
    );
\slv_reg7_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => \slv_reg7__0\(10),
      R => RST
    );
\slv_reg7_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => \slv_reg7__0\(11),
      R => RST
    );
\slv_reg7_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => \slv_reg7__0\(12),
      R => RST
    );
\slv_reg7_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => \slv_reg7__0\(13),
      R => RST
    );
\slv_reg7_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => \slv_reg7__0\(14),
      R => RST
    );
\slv_reg7_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => \slv_reg7__0\(15),
      R => RST
    );
\slv_reg7_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => \slv_reg7__0\(16),
      R => RST
    );
\slv_reg7_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => \slv_reg7__0\(17),
      R => RST
    );
\slv_reg7_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => \slv_reg7__0\(18),
      R => RST
    );
\slv_reg7_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => \slv_reg7__0\(19),
      R => RST
    );
\slv_reg7_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => \slv_reg7__0\(1),
      R => RST
    );
\slv_reg7_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => \slv_reg7__0\(20),
      R => RST
    );
\slv_reg7_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => \slv_reg7__0\(21),
      R => RST
    );
\slv_reg7_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => \slv_reg7__0\(22),
      R => RST
    );
\slv_reg7_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => \slv_reg7__0\(23),
      R => RST
    );
\slv_reg7_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => \slv_reg7__0\(24),
      R => RST
    );
\slv_reg7_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => \slv_reg7__0\(25),
      R => RST
    );
\slv_reg7_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => \slv_reg7__0\(26),
      R => RST
    );
\slv_reg7_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => \slv_reg7__0\(27),
      R => RST
    );
\slv_reg7_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => \slv_reg7__0\(28),
      R => RST
    );
\slv_reg7_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => \slv_reg7__0\(29),
      R => RST
    );
\slv_reg7_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => \slv_reg7__0\(2),
      R => RST
    );
\slv_reg7_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => \slv_reg7__0\(30),
      R => RST
    );
\slv_reg7_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => \slv_reg7__0\(31),
      R => RST
    );
\slv_reg7_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => \slv_reg7__0\(3),
      R => RST
    );
\slv_reg7_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => \slv_reg7__0\(4),
      R => RST
    );
\slv_reg7_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => \slv_reg7__0\(5),
      R => RST
    );
\slv_reg7_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => \slv_reg7__0\(6),
      R => RST
    );
\slv_reg7_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => \slv_reg7__0\(7),
      R => RST
    );
\slv_reg7_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => \slv_reg7__0\(8),
      R => RST
    );
\slv_reg7_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => \slv_reg7__0\(9),
      R => RST
    );
\slv_reg8[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000010000000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg8[31]_i_2_n_0\,
      I3 => s00_axi_wstrb(1),
      I4 => p_0_in(3),
      I5 => p_0_in(1),
      O => \slv_reg8[15]_i_1_n_0\
    );
\slv_reg8[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000010000000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg8[31]_i_2_n_0\,
      I3 => s00_axi_wstrb(2),
      I4 => p_0_in(3),
      I5 => p_0_in(1),
      O => \slv_reg8[23]_i_1_n_0\
    );
\slv_reg8[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000010000000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg8[31]_i_2_n_0\,
      I3 => s00_axi_wstrb(3),
      I4 => p_0_in(3),
      I5 => p_0_in(1),
      O => \slv_reg8[31]_i_1_n_0\
    );
\slv_reg8[31]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00008000"
    )
        port map (
      I0 => s00_axi_awvalid,
      I1 => s00_axi_wvalid,
      I2 => \^s00_axi_awready\,
      I3 => \^s00_axi_wready\,
      I4 => p_0_in(0),
      O => \slv_reg8[31]_i_2_n_0\
    );
\slv_reg8[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000010000000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg8[31]_i_2_n_0\,
      I3 => s00_axi_wstrb(0),
      I4 => p_0_in(3),
      I5 => p_0_in(1),
      O => \slv_reg8[7]_i_1_n_0\
    );
\slv_reg8_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg8[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg8(0),
      R => RST
    );
\slv_reg8_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg8[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => \slv_reg8__0\(10),
      R => RST
    );
\slv_reg8_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg8[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => \slv_reg8__0\(11),
      R => RST
    );
\slv_reg8_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg8[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => \slv_reg8__0\(12),
      R => RST
    );
\slv_reg8_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg8[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => \slv_reg8__0\(13),
      R => RST
    );
\slv_reg8_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg8[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => \slv_reg8__0\(14),
      R => RST
    );
\slv_reg8_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg8[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => \slv_reg8__0\(15),
      R => RST
    );
\slv_reg8_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg8[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => \slv_reg8__0\(16),
      R => RST
    );
\slv_reg8_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg8[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => \slv_reg8__0\(17),
      R => RST
    );
\slv_reg8_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg8[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => \slv_reg8__0\(18),
      R => RST
    );
\slv_reg8_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg8[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => \slv_reg8__0\(19),
      R => RST
    );
\slv_reg8_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg8[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => slv_reg8(1),
      R => RST
    );
\slv_reg8_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg8[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => \slv_reg8__0\(20),
      R => RST
    );
\slv_reg8_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg8[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => \slv_reg8__0\(21),
      R => RST
    );
\slv_reg8_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg8[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => \slv_reg8__0\(22),
      R => RST
    );
\slv_reg8_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg8[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => \slv_reg8__0\(23),
      R => RST
    );
\slv_reg8_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg8[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => \slv_reg8__0\(24),
      R => RST
    );
\slv_reg8_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg8[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => \slv_reg8__0\(25),
      R => RST
    );
\slv_reg8_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg8[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => \slv_reg8__0\(26),
      R => RST
    );
\slv_reg8_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg8[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => \slv_reg8__0\(27),
      R => RST
    );
\slv_reg8_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg8[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => \slv_reg8__0\(28),
      R => RST
    );
\slv_reg8_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg8[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => \slv_reg8__0\(29),
      R => RST
    );
\slv_reg8_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg8[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => slv_reg8(2),
      R => RST
    );
\slv_reg8_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg8[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => \slv_reg8__0\(30),
      R => RST
    );
\slv_reg8_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg8[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => \slv_reg8__0\(31),
      R => RST
    );
\slv_reg8_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg8[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg8(3),
      R => RST
    );
\slv_reg8_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg8[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg8(4),
      R => RST
    );
\slv_reg8_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg8[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg8(5),
      R => RST
    );
\slv_reg8_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg8[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg8(6),
      R => RST
    );
\slv_reg8_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg8[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg8(7),
      R => RST
    );
\slv_reg8_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg8[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg8(8),
      R => RST
    );
\slv_reg8_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg8[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg8(9),
      R => RST
    );
\slv_reg9[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000010000000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg7[31]_i_2_n_0\,
      I3 => s00_axi_wstrb(1),
      I4 => p_0_in(3),
      I5 => p_0_in(1),
      O => \slv_reg9[15]_i_1_n_0\
    );
\slv_reg9[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000010000000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg7[31]_i_2_n_0\,
      I3 => s00_axi_wstrb(2),
      I4 => p_0_in(3),
      I5 => p_0_in(1),
      O => \slv_reg9[23]_i_1_n_0\
    );
\slv_reg9[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000010000000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg7[31]_i_2_n_0\,
      I3 => s00_axi_wstrb(3),
      I4 => p_0_in(3),
      I5 => p_0_in(1),
      O => \slv_reg9[31]_i_1_n_0\
    );
\slv_reg9[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000010000000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(2),
      I2 => \slv_reg7[31]_i_2_n_0\,
      I3 => s00_axi_wstrb(0),
      I4 => p_0_in(3),
      I5 => p_0_in(1),
      O => \slv_reg9[7]_i_1_n_0\
    );
\slv_reg9_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg9[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg9(0),
      R => RST
    );
\slv_reg9_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg9[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => slv_reg9(10),
      R => RST
    );
\slv_reg9_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg9[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => slv_reg9(11),
      R => RST
    );
\slv_reg9_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg9[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => slv_reg9(12),
      R => RST
    );
\slv_reg9_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg9[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => slv_reg9(13),
      R => RST
    );
\slv_reg9_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg9[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => slv_reg9(14),
      R => RST
    );
\slv_reg9_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg9[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => slv_reg9(15),
      R => RST
    );
\slv_reg9_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg9[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => \slv_reg9__0\(16),
      R => RST
    );
\slv_reg9_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg9[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => \slv_reg9__0\(17),
      R => RST
    );
\slv_reg9_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg9[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => \slv_reg9__0\(18),
      R => RST
    );
\slv_reg9_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg9[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => \slv_reg9__0\(19),
      R => RST
    );
\slv_reg9_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg9[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => slv_reg9(1),
      R => RST
    );
\slv_reg9_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg9[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => \slv_reg9__0\(20),
      R => RST
    );
\slv_reg9_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg9[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => \slv_reg9__0\(21),
      R => RST
    );
\slv_reg9_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg9[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => \slv_reg9__0\(22),
      R => RST
    );
\slv_reg9_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg9[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => \slv_reg9__0\(23),
      R => RST
    );
\slv_reg9_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg9[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => \slv_reg9__0\(24),
      R => RST
    );
\slv_reg9_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg9[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => \slv_reg9__0\(25),
      R => RST
    );
\slv_reg9_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg9[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => \slv_reg9__0\(26),
      R => RST
    );
\slv_reg9_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg9[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => \slv_reg9__0\(27),
      R => RST
    );
\slv_reg9_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg9[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => \slv_reg9__0\(28),
      R => RST
    );
\slv_reg9_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg9[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => \slv_reg9__0\(29),
      R => RST
    );
\slv_reg9_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg9[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => slv_reg9(2),
      R => RST
    );
\slv_reg9_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg9[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => \slv_reg9__0\(30),
      R => RST
    );
\slv_reg9_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg9[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => \slv_reg9__0\(31),
      R => RST
    );
\slv_reg9_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg9[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg9(3),
      R => RST
    );
\slv_reg9_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg9[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg9(4),
      R => RST
    );
\slv_reg9_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg9[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg9(5),
      R => RST
    );
\slv_reg9_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg9[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg9(6),
      R => RST
    );
\slv_reg9_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg9[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg9(7),
      R => RST
    );
\slv_reg9_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg9[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg9(8),
      R => RST
    );
\slv_reg9_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg9[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg9(9),
      R => RST
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_my_lab3_ip_v3_0 is
  port (
    tmds : out STD_LOGIC_VECTOR ( 3 downto 0 );
    tmdsb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    ac_mclk : out STD_LOGIC;
    ac_dac_sdata : out STD_LOGIC;
    ac_lrclk : out STD_LOGIC;
    ac_bclk : out STD_LOGIC;
    S_AXI_WREADY : out STD_LOGIC;
    S_AXI_AWREADY : out STD_LOGIC;
    S_AXI_ARREADY : out STD_LOGIC;
    s00_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_rvalid : out STD_LOGIC;
    s00_axi_bvalid : out STD_LOGIC;
    scl : inout STD_LOGIC;
    sda : inout STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC;
    ac_adc_sdata : in STD_LOGIC;
    btn : in STD_LOGIC_VECTOR ( 4 downto 0 );
    s00_axi_aresetn : in STD_LOGIC;
    s00_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    switch : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_awaddr : in STD_LOGIC_VECTOR ( 4 downto 0 );
    s00_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_araddr : in STD_LOGIC_VECTOR ( 4 downto 0 );
    s00_axi_wvalid : in STD_LOGIC;
    s00_axi_awvalid : in STD_LOGIC;
    s00_axi_arvalid : in STD_LOGIC;
    s00_axi_bready : in STD_LOGIC;
    s00_axi_rready : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_my_lab3_ip_v3_0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_my_lab3_ip_v3_0 is
  signal \^s_axi_arready\ : STD_LOGIC;
  signal \^s_axi_awready\ : STD_LOGIC;
  signal \^s_axi_wready\ : STD_LOGIC;
  signal axi_bvalid_i_1_n_0 : STD_LOGIC;
  signal axi_rvalid_i_1_n_0 : STD_LOGIC;
  signal \^s00_axi_bvalid\ : STD_LOGIC;
  signal \^s00_axi_rvalid\ : STD_LOGIC;
begin
  S_AXI_ARREADY <= \^s_axi_arready\;
  S_AXI_AWREADY <= \^s_axi_awready\;
  S_AXI_WREADY <= \^s_axi_wready\;
  s00_axi_bvalid <= \^s00_axi_bvalid\;
  s00_axi_rvalid <= \^s00_axi_rvalid\;
axi_bvalid_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7444444444444444"
    )
        port map (
      I0 => s00_axi_bready,
      I1 => \^s00_axi_bvalid\,
      I2 => \^s_axi_wready\,
      I3 => \^s_axi_awready\,
      I4 => s00_axi_wvalid,
      I5 => s00_axi_awvalid,
      O => axi_bvalid_i_1_n_0
    );
axi_rvalid_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"08F8"
    )
        port map (
      I0 => s00_axi_arvalid,
      I1 => \^s_axi_arready\,
      I2 => \^s00_axi_rvalid\,
      I3 => s00_axi_rready,
      O => axi_rvalid_i_1_n_0
    );
my_lab3_ip_v2_0_S00_AXI_inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_my_lab3_ip_v3_0_S00_AXI
     port map (
      ac_adc_sdata => ac_adc_sdata,
      ac_bclk => ac_bclk,
      ac_dac_sdata => ac_dac_sdata,
      ac_lrclk => ac_lrclk,
      ac_mclk => ac_mclk,
      axi_arready_reg_0 => axi_rvalid_i_1_n_0,
      axi_bvalid_reg_0 => axi_bvalid_i_1_n_0,
      btn(4 downto 0) => btn(4 downto 0),
      s00_axi_aclk => s00_axi_aclk,
      s00_axi_araddr(4 downto 0) => s00_axi_araddr(4 downto 0),
      s00_axi_aresetn => s00_axi_aresetn,
      s00_axi_arready => \^s_axi_arready\,
      s00_axi_arvalid => s00_axi_arvalid,
      s00_axi_awaddr(4 downto 0) => s00_axi_awaddr(4 downto 0),
      s00_axi_awready => \^s_axi_awready\,
      s00_axi_awvalid => s00_axi_awvalid,
      s00_axi_bvalid => \^s00_axi_bvalid\,
      s00_axi_rdata(31 downto 0) => s00_axi_rdata(31 downto 0),
      s00_axi_rvalid => \^s00_axi_rvalid\,
      s00_axi_wdata(31 downto 0) => s00_axi_wdata(31 downto 0),
      s00_axi_wready => \^s_axi_wready\,
      s00_axi_wstrb(3 downto 0) => s00_axi_wstrb(3 downto 0),
      s00_axi_wvalid => s00_axi_wvalid,
      scl => scl,
      sda => sda,
      switch(2 downto 0) => switch(2 downto 0),
      tmds(3 downto 0) => tmds(3 downto 0),
      tmdsb(3 downto 0) => tmdsb(3 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    ac_mclk : out STD_LOGIC;
    ac_adc_sdata : in STD_LOGIC;
    ac_dac_sdata : out STD_LOGIC;
    ac_bclk : out STD_LOGIC;
    ac_lrclk : out STD_LOGIC;
    scl : inout STD_LOGIC;
    sda : inout STD_LOGIC;
    tmds : out STD_LOGIC_VECTOR ( 3 downto 0 );
    tmdsb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    switch : in STD_LOGIC_VECTOR ( 4 downto 0 );
    btn : in STD_LOGIC_VECTOR ( 4 downto 0 );
    ready : out STD_LOGIC;
    trig_volt : out STD_LOGIC_VECTOR ( 9 downto 0 );
    trig_time : out STD_LOGIC_VECTOR ( 9 downto 0 );
    s00_axi_awaddr : in STD_LOGIC_VECTOR ( 6 downto 0 );
    s00_axi_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_awvalid : in STD_LOGIC;
    s00_axi_awready : out STD_LOGIC;
    s00_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_wvalid : in STD_LOGIC;
    s00_axi_wready : out STD_LOGIC;
    s00_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_bvalid : out STD_LOGIC;
    s00_axi_bready : in STD_LOGIC;
    s00_axi_araddr : in STD_LOGIC_VECTOR ( 6 downto 0 );
    s00_axi_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_arvalid : in STD_LOGIC;
    s00_axi_arready : out STD_LOGIC;
    s00_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_rvalid : out STD_LOGIC;
    s00_axi_rready : in STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_1_my_lab3_ip_0_1,my_lab3_ip_v3_0,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "my_lab3_ip_v3_0,Vivado 2018.2";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  signal \<const0>\ : STD_LOGIC;
  attribute x_interface_info : string;
  attribute x_interface_info of s00_axi_aclk : signal is "xilinx.com:signal:clock:1.0 S00_AXI_CLK CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of s00_axi_aclk : signal is "XIL_INTERFACENAME S00_AXI_CLK, ASSOCIATED_BUSIF S00_AXI, ASSOCIATED_RESET s00_axi_aresetn, FREQ_HZ 100000000, PHASE 0.0, CLK_DOMAIN /clk_wiz_1_clk_out1";
  attribute x_interface_info of s00_axi_aresetn : signal is "xilinx.com:signal:reset:1.0 S00_AXI_RST RST";
  attribute x_interface_parameter of s00_axi_aresetn : signal is "XIL_INTERFACENAME S00_AXI_RST, POLARITY ACTIVE_LOW";
  attribute x_interface_info of s00_axi_arready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARREADY";
  attribute x_interface_info of s00_axi_arvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARVALID";
  attribute x_interface_info of s00_axi_awready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWREADY";
  attribute x_interface_info of s00_axi_awvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWVALID";
  attribute x_interface_info of s00_axi_bready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI BREADY";
  attribute x_interface_info of s00_axi_bvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI BVALID";
  attribute x_interface_info of s00_axi_rready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RREADY";
  attribute x_interface_info of s00_axi_rvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RVALID";
  attribute x_interface_info of s00_axi_wready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WREADY";
  attribute x_interface_info of s00_axi_wvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WVALID";
  attribute x_interface_info of s00_axi_araddr : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARADDR";
  attribute x_interface_info of s00_axi_arprot : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARPROT";
  attribute x_interface_info of s00_axi_awaddr : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWADDR";
  attribute x_interface_parameter of s00_axi_awaddr : signal is "XIL_INTERFACENAME S00_AXI, WIZ_DATA_WIDTH 32, WIZ_NUM_REG 32, SUPPORTS_NARROW_BURST 0, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 100000000, ID_WIDTH 0, ADDR_WIDTH 7, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, NUM_READ_OUTSTANDING 2, NUM_WRITE_OUTSTANDING 2, MAX_BURST_LENGTH 1, PHASE 0.0, CLK_DOMAIN /clk_wiz_1_clk_out1, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0";
  attribute x_interface_info of s00_axi_awprot : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWPROT";
  attribute x_interface_info of s00_axi_bresp : signal is "xilinx.com:interface:aximm:1.0 S00_AXI BRESP";
  attribute x_interface_info of s00_axi_rdata : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RDATA";
  attribute x_interface_info of s00_axi_rresp : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RRESP";
  attribute x_interface_info of s00_axi_wdata : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WDATA";
  attribute x_interface_info of s00_axi_wstrb : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WSTRB";
begin
  s00_axi_bresp(1) <= \<const0>\;
  s00_axi_bresp(0) <= \<const0>\;
  s00_axi_rresp(1) <= \<const0>\;
  s00_axi_rresp(0) <= \<const0>\;
  ready <= 'Z';
  trig_time(0) <= 'Z';
  trig_time(1) <= 'Z';
  trig_time(2) <= 'Z';
  trig_time(3) <= 'Z';
  trig_time(4) <= 'Z';
  trig_time(5) <= 'Z';
  trig_time(6) <= 'Z';
  trig_time(7) <= 'Z';
  trig_time(8) <= 'Z';
  trig_time(9) <= 'Z';
  trig_volt(0) <= 'Z';
  trig_volt(1) <= 'Z';
  trig_volt(2) <= 'Z';
  trig_volt(3) <= 'Z';
  trig_volt(4) <= 'Z';
  trig_volt(5) <= 'Z';
  trig_volt(6) <= 'Z';
  trig_volt(7) <= 'Z';
  trig_volt(8) <= 'Z';
  trig_volt(9) <= 'Z';
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
U0: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_my_lab3_ip_v3_0
     port map (
      S_AXI_ARREADY => s00_axi_arready,
      S_AXI_AWREADY => s00_axi_awready,
      S_AXI_WREADY => s00_axi_wready,
      ac_adc_sdata => ac_adc_sdata,
      ac_bclk => ac_bclk,
      ac_dac_sdata => ac_dac_sdata,
      ac_lrclk => ac_lrclk,
      ac_mclk => ac_mclk,
      btn(4 downto 0) => btn(4 downto 0),
      s00_axi_aclk => s00_axi_aclk,
      s00_axi_araddr(4 downto 0) => s00_axi_araddr(6 downto 2),
      s00_axi_aresetn => s00_axi_aresetn,
      s00_axi_arvalid => s00_axi_arvalid,
      s00_axi_awaddr(4 downto 0) => s00_axi_awaddr(6 downto 2),
      s00_axi_awvalid => s00_axi_awvalid,
      s00_axi_bready => s00_axi_bready,
      s00_axi_bvalid => s00_axi_bvalid,
      s00_axi_rdata(31 downto 0) => s00_axi_rdata(31 downto 0),
      s00_axi_rready => s00_axi_rready,
      s00_axi_rvalid => s00_axi_rvalid,
      s00_axi_wdata(31 downto 0) => s00_axi_wdata(31 downto 0),
      s00_axi_wstrb(3 downto 0) => s00_axi_wstrb(3 downto 0),
      s00_axi_wvalid => s00_axi_wvalid,
      scl => scl,
      sda => sda,
      switch(2 downto 0) => switch(2 downto 0),
      tmds(3 downto 0) => tmds(3 downto 0),
      tmdsb(3 downto 0) => tmdsb(3 downto 0)
    );
end STRUCTURE;
