--------------------------------------------------------------------
-- Name:	Jacob Dunn
-- Date:	6 January 2022
-- Course:	ECE383
-- File:	HW1
-- HW:		HW1 Jacob Dunn
--
-- Purp: VHDL code for problem 6 on HW1 
--
-- Doc:	C2C Kevin Carrig
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity HW1 is
  Port (I0, I1, I2, I3: in std_logic;
        O0, O1: out std_logic);
end HW1;

architecture Behavioral of HW1 is

begin 
	O1 <= I3 or I2;
	O0 <= I3 or (I1 and not I3 and not I2);

end Behavioral;
