--------------------------------------------------------------------------------
-- Name:	George York
-- Purp:	Sandbox testbench to test simple VHDL code
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
 
ENTITY sandbox_tb IS
END sandbox_tb;
 
ARCHITECTURE behavior OF sandbox_tb IS 
 
   --signals for the counter
   signal clk : std_logic := '0';
   signal reset : std_logic := '0';
	signal ctrl : std_logic_vector(1 downto 0) := (others => '0');
   signal D : unsigned(3 downto 0) := (others => '0');
   signal processQ: unsigned (3 downto 0);
 	--Outputs
   signal Q : unsigned(3 downto 0);

   -- Clock period definitions
   constant clk_period : time := 1 us;
 
   -- signals to play with VHDL math and syntax
    signal readL_stdLogicVector: std_logic_vector(15 downto 0);
    signal readL_16bit : unsigned(15 downto 0);
    signal readL_10bit : unsigned(9 downto 0);
    signal readL_9bit : unsigned(8 downto 0);  
    signal readL_8bit : unsigned(7 downto 0);  
    signal row, offset, ReadLPlusOffset, ReadLMinusOffset: unsigned(9 downto 0);
    signal ch1: std_logic;
    
BEGIN
    -- make a counter if you need one
        -----------------------------------------------------------------------------
    --        ctrl
    --        00            hold
    --        01            count up mod 10
    --        10            load D
    --        11            synch reset
    -----------------------------------------------------------------------------
    process(clk)
    begin
        if (rising_edge(clk)) then
            if (reset = '0') then
                processQ <= (others => '0');
            elsif ((processQ < 9) and (ctrl = "01")) then
                processQ <= processQ + 1;
            elsif ((processQ = 9) and (ctrl = "01")) then
                processQ <= (others => '0');
            elsif (ctrl = "10") then
                processQ <= unsigned(D);
            elsif (ctrl = "11") then
                processQ <= (others => '0');
            end if;
        end if;
    end process;
    
    Q <= processQ;

   -- make a clock if you need one
   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 
 	-----------------------------------------------------------------------------
	--		ctrl  --- some simple tests for the clock
	--		00			hold
	--		01			count up mod 10
	--		10			load D
	--		11			synch reset
	-----------------------------------------------------------------------------
	ctrl <= "01", "10" after 15us, "01" after 16us, "11" after 17us, "01" after 18us, "00" after 19us, "01" after 20us;
	D <= "1110";
	reset <= '0', '1' after 1us;
	
	--------------
	-- some math tests
	--------------
	-- make some test inputs
	readL_stdLogicVector <= "1000000000000000"; -- the middle value or DC value
	       -- the most significant bits of the 16-bit number in BRAM grabbed into a 10-bit number
	       -- test cases of 512, 256, 128 for the 10-bit, 9-bit, 8-bit DC middle case
	offset <= to_unsigned(36, 10);  -- decimal 36 made into a 10-bit unsigned signal:  is this a good offset guess for one of the bit sizes?
	row <= to_unsigned(220, 10);    -- decimal 220 made into a 10-bit unsigned signal
	-- put test code here, and see results in simulation plot
	readL_16bit <= unsigned(readL_stdLogicVector);
	readL_10bit <= readL_16bit(15 downto 6);
	readL_9bit <= readL_16bit(15 downto 7);
	readL_8bit <= readL_16bit(15 downto 8);
	-- test each size
	ReadLPlusOffset <= readL_10bit+offset after 10 us, readL_9bit+offset after 20 us, readL_8bit+offset after 30 us;
	ReadLMinusOffset <= readL_10bit-offset after 10 us, readL_9bit-offset after 20 us, readL_8bit-offset after 30 us;
	-- if I guess write, ch1 will fire when readL +/- offset = row = 220;
	ch1 <= '1' when (ReadLMinusOffset = row) else '0';		-- should I add or subtract offset?

	       
	

END;
