--
--	Package File Template
--
--	Purpose: This package defines supplemental types, subtypes, 
--		 constants, and functions 
--
--   To use any of the example code shown below, uncomment the lines and modify as necessary
--


library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.ALL;

package graphicsParts is

component graphics
    Port ( clk : in  STD_LOGIC;
           reset_n : in  STD_LOGIC;
           ac_mclk : out STD_LOGIC;
           ac_adc_sdata : in STD_LOGIC;
           ac_dac_sdata : out STD_LOGIC;
           ac_bclk : out STD_LOGIC;
           ac_lrclk : out STD_LOGIC;
           scl : inout STD_LOGIC;
           sda : inout STD_LOGIC;
           tmds : out  STD_LOGIC_VECTOR (3 downto 0);
           tmdsb : out  STD_LOGIC_VECTOR (3 downto 0);
           btn: in	STD_LOGIC_VECTOR(4 downto 0);
           sw : in std_logic_vector(7 downto 0) );
end component;

component graphics_fsm
    Port ( clk : in  STD_LOGIC;
           reset_n : in  STD_LOGIC;
		   fsmCol: out std_logic_vector(5 downto 0);
           fsmRow: out std_logic_vector(4 downto 0);
           fsmWen : out std_logic;
           fsmData: out std_logic_vector(15 downto 0);
           cursorRow, cursorCol : in unsigned(2 downto 0);
           btn : in std_logic_vector(4 downto 0);
           sw : in std_logic_vector(7 downto 0);
           ch1, ch2 : in std_logic_vector(15 downto 0) );
end component;

component graphics_datapath
    Port ( clk : in  STD_LOGIC;
           reset_n : in  STD_LOGIC;  
           ac_mclk : out STD_LOGIC;
           ac_adc_sdata : in STD_LOGIC;
           ac_dac_sdata : out STD_LOGIC;
           ac_bclk : out STD_LOGIC;
           ac_lrclk : out STD_LOGIC;
           scl : inout STD_LOGIC;
           sda : inout STD_LOGIC;   
           Lbus_out, Rbus_out: out std_logic_vector(17 downto 0);
           exLbus, exRbus: in signed(15 downto 0);
		   tmds : out  STD_LOGIC_VECTOR (3 downto 0);
           tmdsb : out  STD_LOGIC_VECTOR (3 downto 0);
		   fsmCol: in std_logic_vector(5 downto 0);
           fsmRow: in std_logic_vector(4 downto 0);
           fsmWen : in std_logic;
           fsmData: in std_logic_vector(15 downto 0);
			  exCol: in std_logic_vector(5 downto 0);
			  exRow: in std_logic_vector(4 downto 0);
			  exWen, exSel: in std_logic;
			  exData: in std_logic_vector(15 downto 0);
			  btn : in std_logic_vector(4 downto 0);
			  sw : in std_logic_vector(7 downto 0);
			  cursorRow, cursorCol : out unsigned(2 downto 0);
			  ch1, ch2 : out std_logic_vector(15 downto 0);
			  base, interp : in signed(15 downto 0) );
end component;

component lab4_datapath is
    Port(  clk, reset_n : in std_logic;
           cw : in std_logic_vector(4 downto 0);
           btn : in std_logic_vector(4 downto 0);
           switch : in std_logic_vector(7 downto 0);
           interp, base : out signed(15 downto 0) ); 
end component;

	component video is
    Port ( clk : in  STD_LOGIC;
           reset_n : in  STD_LOGIC;
           tmds : out  STD_LOGIC_VECTOR (3 downto 0);
           tmdsb : out  STD_LOGIC_VECTOR (3 downto 0);
			  row: out unsigned(9 downto 0);
			  column: out unsigned(9 downto 0);
			  ch1 : in  std_logic_vector(15 downto 0);
			  ch1_enb: in std_logic;
			  ch2: in std_logic_vector(15 downto 0);
			  ch2_enb: in std_logic;
			  v_synch: out std_logic;
			  cursorRow : in unsigned(2 downto 0);
			  cursorCol : in unsigned(2 downto 0) );
	end component;
	
	component Audio_Codec_Wrapper is
    Port ( clk : in STD_LOGIC;
            reset_n : in STD_LOGIC;
            ac_mclk : out STD_LOGIC;
            ac_adc_sdata : in STD_LOGIC;
            ac_dac_sdata : out STD_LOGIC;
            ac_bclk : out STD_LOGIC;
            ac_lrclk : out STD_LOGIC;
            ready : out STD_LOGIC;
            L_bus_in : in std_logic_vector(17 downto 0); -- left channel input to DAC
            R_bus_in : in std_logic_vector(17 downto 0); -- right channel input to DAC
            L_bus_out : out  std_logic_vector(17 downto 0); -- left channel output from ADC
            R_bus_out : out  std_logic_vector(17 downto 0); -- right channel output from ADC
            scl : inout STD_LOGIC;
            sda : inout STD_LOGIC;
            sim_live : in STD_LOGIC);
    end component;
    
--    component nes_fsm is
--        port (
--          clk: in std_logic;
--          latch: in std_logic;
--          pulse: in std_logic;
--          data: in std_logic;
--          button: out std_logic_vector(7 downto 0) );
--    end component;

    component lab4_fsm is
        Port ( clk, reset_n : in std_logic;
               sw : in std_logic; 
               cw : out std_logic_vector(4 downto 0) );
    end component;
	
end graphicsParts;
