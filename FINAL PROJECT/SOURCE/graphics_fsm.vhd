----------------------------------------------------------------------------------
-- Name:	Jacob Dunn
-- Date:	Spring 2022
-- File:    graphics_fsm.vhd
-- HW:	    Final Project
-- Pupr:	Checkers 
--
-- Doc:	C2C Carrig and C2C Bean assisted with various parts of the project
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity graphics_fsm is
    Port ( clk : in  STD_LOGIC;
           reset_n : in  STD_LOGIC;
		   fsmCol: out std_logic_vector(5 downto 0);
           fsmRow: out std_logic_vector(4 downto 0);
           fsmWen : out std_logic;
           fsmData: out std_logic_vector(15 downto 0);
           cursorRow, cursorCol : in unsigned(2 downto 0);
           btn : in std_logic_vector(4 downto 0);
           sw : in std_logic_vector(7 downto 0);
           ch1, ch2 : in std_logic_vector(15 downto 0) );
end graphics_fsm;

architecture Behavioral of graphics_fsm is

	type state_type is (INIT, DELAY, CHANGE_DATA, SET, CLEAR);
	signal state: state_type;
	signal writeCntr: unsigned(17 downto 0);
	signal col : unsigned(5 downto 0);
    signal row : unsigned(4 downto 0);
	signal Data, Old_Data: std_logic_vector(15 downto 0);
	signal Wen: std_logic;
	signal old_button : std_logic_vector(4 downto 0);
	signal piece : std_logic_vector(15 downto 0);
    signal oldRow, oldCol, jumpRow, jumpCol : unsigned(2 downto 0);

begin
    fsmCol <= std_logic_vector(col);
    fsmRow <= std_logic_vector(row);
    fsmData <= Data;
    fsmWen <= Wen;
	-------------------------------------------------------------------------------
	-- Long delay counter
	-------------------------------------------------------------------------------	
	writeCounter: process(clk)
	begin
		if (rising_edge(clk)) then
			if (reset_n = '0') then
				writeCntr <= "000000000000000000";  -- change for graphics memory test
			else 
			    writeCntr <= writeCntr+1;
			end if;
		end if;
	end process;
		
	state_proces: process(clk)  
	begin
		if (rising_edge(clk)) then
			if (reset_n = '0') then 
				state <= INIT;
				row <= "00000";
				col <= "000000";   
				jumpRow <= "001";
                jumpCol <= "000";                         
				Data <= "0000000000000000";
				Wen <= '0';
			else 
				case state is
					when INIT =>
					   row(2 downto 0) <= cursorRow;
                       col(2 downto 0) <= cursorCol;
                       Data <= ch2;
					   state <= DELAY;    		
					when DELAY => 
						if (writeCntr = "000000000000000000") then state <= CHANGE_DATA; end if;
                    when CHANGE_DATA => --switches control the game functions
                         if ( sw(0) = '1' ) then --save data
                             piece <= ch2;
                             oldRow <= cursorRow;
                             oldCol <= cursorCol;
                         elsif( sw(1) = '1') then --write data
                             row(2 downto 0) <= cursorRow;
                             col(2 downto 0) <= cursorCol;
                             Data <= piece;
                         elsif( sw(2) = '1') then --delete data
                             row(2 downto 0) <= oldRow;
                             col(2 downto 0) <= oldCol;
                             Data <= "0000000000000000";
                         elsif( sw(3) = '1') then --delete jumped piece
                             if( cursorRow = (oldRow + 2)) then
                                jumpRow <= cursorRow - 1;
                             elsif( cursorRow = (oldRow - 2)) then
                                jumpRow <= cursorRow + 1;
                             end if;
                             
                             if( cursorCol = (oldCol + 2)) then
                                jumpCol <= cursorCol - 1;
                             elsif( cursorCol = (oldCol - 2)) then
                                jumpCol <= cursorCol + 1;
                             end if;
                             
                             row(2 downto 0) <= jumpRow;
                             col(2 downto 0) <= jumpCol;
                             Data <= "0000000000000000";
                         end if;
                         
                         if ( sw(4) = '1' ) then --King
                            if( piece = "0000000000000001") then
                                Data <= "0000000000000011";
                            end if;
                            
                         if( piece = "0000000000000010") then
                                Data <= "0000000000000100";
                            end if;
                         end if;
                         state <= SET;
					when SET =>
						state <= CLEAR;
						Wen <= '1';
					when CLEAR =>
						state <= INIT;
                        Wen <= '0';
				end case;
			end if;
		end if;
	end process;

    Old_Data <= Data;


end Behavioral;
