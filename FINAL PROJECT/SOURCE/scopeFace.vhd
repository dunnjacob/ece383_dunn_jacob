----------------------------------------------------------------------------------
-- Name:	George York
-- Date:	Spring 2020
-- File: scopeFace.vhd
-- HW:	Lab 1 
-- Pupr:	Scope Face component entity description for Lab 1.  This component sweeps
--			acrossthe display from left to right, and then return to the left side of 
--			the next lower row. The VGA interface determines the color of each pixel
--			on this journey with the help of the scopeFace component.
-- Doc:	Adapted from Dr Coulston's Lab exercise
-- 	


--		Total scope display is 640x480

--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity scopeFace is
    Port ( row : in  unsigned (9 downto 0);
           column : in  unsigned (9 downto 0);
           r : out  std_logic_vector(7 downto 0);
           g : out  std_logic_vector(7 downto 0);
           b : out  std_logic_vector(7 downto 0);
           ch1 : in  std_logic_vector(15 downto 0);
           ch1_enb : in  STD_LOGIC;
           ch2 : in  STD_LOGIC_vector(15 downto 0);
           ch2_enb : in  STD_LOGIC;
           cursorRow : in unsigned(2 downto 0);
           cursorCol : in unsigned(2 downto 0) );
end scopeFace;

architecture Behavioral of scopeFace is
    signal graphics_on, obj1_pixel, obj2_pixel, obj3_pixel, obj4_pixel, obj5_pixel, obj6_pixel : std_logic;
    signal r_obj1, g_obj1, b_obj1, r_obj2, g_obj2, b_obj2, r_obj3, g_obj3, b_obj3, r_obj4, g_obj4, b_obj4, r_obj5, g_obj5, b_obj5, r_obj6, g_obj6, b_obj6 : std_logic_vector(7 downto 0);

begin


-- color map for each object
r_obj1 <= "00000000";
g_obj1 <= "00000000";
b_obj1 <= "00000000";
r_obj2 <= "11111111";
g_obj2 <= "00000000";
b_obj2 <= "11111111"; 
r_obj3 <= "00000000";
g_obj3 <= "11111111";
b_obj3 <= "11111111";
r_obj4 <= "11111111";
g_obj4 <= "11111111";
b_obj4 <= "00000000";
r_obj5 <= "11111111";
g_obj5 <= "00000000";
b_obj5 <= "11111111";
r_obj6 <= "00000000";
g_obj6 <= "11111111";
b_obj6 <= "11111111";
 
graphics_on <= '1' when (column < 257) and (row < 257) else '0';

r <= "11111111" when (graphics_on = '1') and (((column mod 32 = 0) and (column < 257) and (row < 257) ) or 
                     ((row mod 32 = 0) and (column < 257) and (row < 257))) else
     r_obj1 when ((graphics_on = '1') and (obj1_pixel = '1')) else
     r_obj2 when ((graphics_on = '1') and (obj2_pixel = '1')) else
     r_obj3 when ((graphics_on = '1') and (obj3_pixel = '1')) else
     r_obj4 when ((graphics_on = '1') and (obj4_pixel = '1')) else
     r_obj5 when ((graphics_on = '1') and (obj5_pixel = '1')) else
     r_obj6 when ((graphics_on = '1') and (obj6_pixel = '1')) else
     "00000000";
     
g <= "11111111" when (graphics_on = '1') and (((column mod 32 = 0) and (column < 257) and (row < 257) ) or 
                     ((row mod 32 = 0) and (column < 257) and (row < 257))) else
     g_obj1 when ((graphics_on = '1') and (obj1_pixel = '1')) else
     g_obj2 when ((graphics_on = '1') and (obj2_pixel = '1')) else
     g_obj3 when ((graphics_on = '1') and (obj3_pixel = '1')) else
     g_obj4 when ((graphics_on = '1') and (obj4_pixel = '1')) else
     g_obj5 when ((graphics_on = '1') and (obj5_pixel = '1')) else
     g_obj6 when ((graphics_on = '1') and (obj6_pixel = '1')) else
     "00000000";
     
b <= "11111111" when (graphics_on = '1') and (((column mod 32 = 0) and (column < 257) and (row < 257) ) or 
                     ((row mod 32 = 0) and (column < 257) and (row < 257))) else
     b_obj1 when ((graphics_on = '1') and (obj1_pixel = '1')) else
     b_obj2 when ((graphics_on = '1') and (obj2_pixel = '1')) else
     b_obj3 when ((graphics_on = '1') and (obj3_pixel = '1')) else
     b_obj4 when ((graphics_on = '1') and (obj4_pixel = '1')) else
     b_obj5 when ((graphics_on = '1') and (obj5_pixel = '1')) else
     b_obj6 when ((graphics_on = '1') and (obj6_pixel = '1')) else
     "00000000";

-- draw object shapes 
-- draw nothing
obj1_pixel <= '1' when (ch1(2 downto 0) = "000") and not ((cursorRow = row(7 downto 5)) and (cursorCol = column(7 downto 5))) else '0';

-- make object2 a +
obj2_pixel <= '1' when (ch1(2 downto 0) = "001") and ((row(4 downto 0) = "10000") or (column(4 downto 0) = "10000")) else '0';

-- make object3 a #
obj3_pixel <= '1' when (ch1(2 downto 0) = "010") and ((row(4 downto 0) = "01011") or (row(4 downto 0) = "10110") or 
                                                      (column(4 downto 0) = "01011") or (column(4 downto 0) = "10110")) 
                                                      else '0'; 
-- make object1 the boundary of the box                                                      
obj4_pixel <= '1' when (((cursorRow = row(7 downto 5)) and (cursorCol = column(7 downto 5))) and ((row(4 downto 0) = "00001") or (row(4 downto 0) = "11111") or 
                                                      (column(4 downto 0) = "00001") or (column(4 downto 0) = "11111"))) else '0';
                                                      
-- make object2 a +
obj5_pixel <= '1' when (ch1(2 downto 0) = "011") and (((row(4 downto 0) = "10000") or (column(4 downto 0) = "10000"))
                                                     or ((row(4 downto 0) = "01011") or (row(4 downto 0) = "10110") or 
                                                     (column(4 downto 0) = "01011") or (column(4 downto 0) = "10110")))
                                                     else '0';

-- make object2 a +
obj6_pixel <= '1' when (ch1(2 downto 0) = "100") and (((row(4 downto 0) = "10000") or (column(4 downto 0) = "10000"))
                                                     or ((row(4 downto 0) = "01011") or (row(4 downto 0) = "10110") or 
                                                     (column(4 downto 0) = "01011") or (column(4 downto 0) = "10110")))
                                                     else '0';
end Behavioral;

