--------------------------------------------------------------------
-- Name:	Jacob Dunn
-- Date:	April 9, 2022
-- File:	lab4_datapath.vhdl
-- HW:	    Lab4
-- Crs:	    ECE 383
--
-- Purp:	This is the datapath for lab4
--
-- Documentation: None.
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
------------------------------------------------------------------------- 
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.MATH_REAL.ALL;
library UNIMACRO;		-- This contains links to the Xilinx block RAM
use UNIMACRO.vcomponents.all;

entity lab4_datapath is
    Port(  clk, reset_n : in std_logic;
           cw : in std_logic_vector(4 downto 0);
           btn : in std_logic_vector(4 downto 0);
           switch : in std_logic_vector(7 downto 0);
           interp, base : out signed(15 downto 0) );
            
end lab4_datapath;

architecture Behavioral of lab4_datapath is

signal Din_L, Din_R : std_logic_vector(15 downto 0);
signal delta_offset : signed(24 downto 0);
signal ampint : signed(23 downto 0);
signal phaseInc, baseValue, nextValue, bRAM, delta, interpolated, output : signed(15 downto 0);
signal index_offset1, index_offset2 : signed(15 downto 0) := "0000000000000000";
signal Addr2 : signed(9 downto 0);
signal index, offset, indexBRAM, addr1 : signed(7 downto 0);
signal reset, ready : std_logic;
signal data : std_logic_vector(15 downto 0);
signal num : std_logic_vector(15 downto 0);

signal exLbus, exRbus : std_logic_vector(15 downto 0);
signal exLbus2, exRbus2 : std_logic_vector(17 downto 0);
signal UnL_bus_out, UnR_bus_out : unsigned(17 downto 0);
signal move : signed(7 downto 0);
	
begin

    reset <= not reset_n;
    num <= "00000000" & switch(7 downto 0);
        
    phase : process(clk)
    begin
        if (rising_edge(clk)) then
            if (reset_n = '0') then
                phaseInc <= "0000000000011011";
            end if;
            
            if (btn(3) = '1') then 
                phaseInc <= phaseInc + signed(num); --increment phase increment
            elsif (btn(1) = '1') then 
                phaseInc <= phaseInc - signed(num); --decrememtn phase increment
            end if;
        end if;
    end process;
    
    move <= "00100000" when switch(1) = '1' else "00000000";

    amplifier : process(clk) --does not work properly
    begin
        if (rising_edge(clk)) then
            if (reset_n = '0') then
                ampint <= "000000000000000000000000"; --default value
                output <= interpolated;
            end if;
        
            if (cw(4) = '1') then
                ampint <= interpolated * move; --amplify math
                output <= signed(ampint(15 downto 0));
            else
                output <= "0000000000000000";
            end if;      
        end if;  
    end process;
    
    index_offset1 <= phaseInc + index_offset2; --add phaseInc to index_offset
    
    indexoffset : process(clk)
    begin
        if (reset = '1') then
            index_offset2 <= "0000000000000000";
        end if;
        if (rising_edge(clk)) then
            if(cw(0) = '1') then
                index_offset2 <= index_offset1;
                index <= index_offset2(15 downto 8); --seperate variables
                offset <= index_offset2(7 downto 0);
            end if;
        end if;
    end process;
    
    mux : process(clk)
    begin
        if(rising_edge(clk)) then
            if (cw(1) = '0') then 
                Addr1 <= index;
            elsif (cw(1) = '1') then
                Addr1 <= index + 1;
            end if;
        end if;
    end process;
    
    Addr2 <= "00" & Addr1;
    
    reg : process(clk)
    begin
        if(rising_edge(clk)) then   
            if (cw(2) = '1') then
                baseValue <= signed(data);
            elsif (cw(3) = '1') then
                nextValue <= signed(data);
            end if;
        end if;
    end process;
 
    delta <= nextValue - baseValue when cw(4) = '1' else delta; --interpolated math
    delta_offset <= delta * ("0" & offset);
    interpolated <= delta_offset(23 downto 8) + baseValue;
    
    base <= baseValue; --outputs to Lab2
    interp <= output;
    
    rightChannelMemory : BRAM_SDP_MACRO
    generic map (
        BRAM_SIZE => "18Kb",            -- Target BRAM, "18Kb" or "36Kb"
        DEVICE => "7SERIES",            -- Target device: "VIRTEX5", "VIRTEX6", "SPARTAN6", "7SERIES"
        DO_REG => 0,                    -- Optional output register (0 or 1)
        INIT => X"000000000000000000",            -- Initial values on output port
        INIT_FILE => "NONE",            -- Initial values on output port
        WRITE_WIDTH => 16,              -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
        READ_WIDTH => 16,               -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
        SIM_COLLISION_CHECK => "NONE",  -- Collision check enable "ALL", "WARNING_ONLY", "GENERATE_X_ONLY" or "NONE"
        SRVAL => X"000000000000000000", -- Set/Reset value for port output
        -- Here is where you insert the INIT_xx declarations to specify the initial contents of the RAM
          INIT_00 => X"2E102B1F2826252722231F191C0B18F815E112C70FAB0C8B096A064703240000",
          INIT_01 => X"584255F5539A51334EBF4C3F49B3471C447A41CD3F173C56398C36B933DE30FB",
          INIT_02 => X"750473B5725470E26F5E6DC96C236A6D68A666CF64E862F160EB5ED75CB35A82",
          INIT_03 => X"7FF57FD87FA67F617F097E9C7E1D7D897CE37C297B5C7A7C79897884776B7641",
          INIT_04 => X"776B788479897A7C7B5C7C297CE37D897E1D7E9C7F097F617FA67FD87FF57FFF",
          INIT_05 => X"5CB35ED760EB62F164E866CF68A66A6D6C236DC96F5E70E2725473B575047641",
          INIT_06 => X"33DE36B9398C3C563F1741CD447A471C49B34C3F4EBF5133539A55F558425A82",
          INIT_07 => X"03240647096A0C8B0FAB12C715E118F81C0B1F192223252728262B1F2E1030FB",
          INIT_08 => X"D1F0D4E1D7DADAD9DDDDE0E7E3F5E708EA1FED39F055F375F696F9B9FCDC0000",
          INIT_09 => X"A7BEAA0BAC66AECDB141B3C1B64DB8E4BB86BE33C0E9C3AAC674C947CC22CF05",
          INIT_0A => X"8AFC8C4B8DAC8F1E90A2923793DD9593975A99319B189D0F9F15A129A34DA57E",
          INIT_0B => X"800B8028805A809F80F7816481E38277831D83D784A485848677877C889589BF",
          INIT_0C => X"8895877C8677858484A483D7831D827781E3816480F7809F805A8028800B8001",
          INIT_0D => X"A34DA1299F159D0F9B189931975A959393DD923790A28F1E8DAC8C4B8AFC89BF",
          INIT_0E => X"CC22C947C674C3AAC0E9BE33BB86B8E4B64DB3C1B141AECDAC66AA0BA7BEA57E",
          INIT_0F => X"FCDCF9B9F696F375F055ED39EA1FE708E3F5E0E7DDDDDAD9D7DAD4E1D1F0CF05")
port map (
        DO => data,    -- Output read data port, width defined by READ_WIDTH parameter
        RDADDR => std_logic_vector(Addr2),            -- Input address, width defined by port depth
        RDCLK => clk,                   -- 1-bit input clock
        RST => reset,                 -- active high reset
        RDEN => '1',                    -- read enable
        REGCE => '1',                   -- 1-bit input read output register enable
        DI => std_logic_vector(interpolated), -- Input data port, width defined by WRITE_WIDTH parameter
        WE => "11",                     -- Input write enable, width defined by write port depth
        WRADDR => std_logic_vector(Addr2), -- Input write address, width defined by write port depth
        WRCLK => clk,                   -- 1-bit input write clock
        WREN => '0');              -- 1-bit input write port enable
        -- End of BRAM_SDP_MACRO_inst instantiation

end Behavioral;