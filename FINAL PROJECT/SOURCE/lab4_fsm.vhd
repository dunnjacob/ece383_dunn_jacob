--------------------------------------------------------------------
-- Name:	Jacob Dunn
-- Date:	April 9, 2022
-- File:	lab4_fsm.vhd
-- HW:	    Lab4
-- Crs:	    ECE 383
--
-- Purp:	This is the control unit for lab4
--
-- Documentation: None.
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
------------------------------------------------------------------------- 


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity lab4_fsm is
    Port ( clk, reset_n : in std_logic;
           sw : in std_logic; 
           cw : out std_logic_vector(4 downto 0) );
end lab4_fsm;

architecture Behavioral of lab4_fsm is

type state_type is (WaitReady, IncPhaseOffset, SendBRAMBaseAddr, SaveBaseValue, SendBRAMNextAddr, SaveNextValue, SaveOutput);
signal state: state_type;

begin

     state_process: process(clk)
	 begin
		if (rising_edge(clk)) then
			if (reset_n = '0') then 
				state <= WaitReady;
			else
				case state is
					when WaitReady => 
					    if (sw = '0') then state <= WaitReady; else state <= IncPhaseOffset; end if;
					when IncPhaseOffset =>
					    state <= SendBRAMBaseAddr;
                    when SendBRAMBaseAddr =>
                        state <= SaveBaseValue;
                    when SaveBaseValue =>
                        state <= SendBRAMNextAddr;
                    when SendBRAMNextAddr =>
                        state <= SaveNextValue;
                    when SaveNextValue =>
                        state <= SaveOutput;
                    when SaveOutput =>
                        state <= WaitReady;
				end case;
			end if;
		end if;
	end process;


	---------------------------------------------
	--		OUTPUT EQUATIONS - cw
	--	    bit 4           bit 3           bit 2           bit 1           bit 0
	--		0 - hold        0 -  Hold       0 - hold        0 - Send Base   0 - hold
	--		1 - Save Output 1 - Save Next   1 - Save Base   1 - Send Next   1 - Inc
	---------------------------------------------
	cw <=    "00001" when state = IncPhaseOffset else
	         "00000" when state = SendBRAMBaseAddr or state = WaitReady else
	         "00100" when state = SaveBaseValue else
			 "00010" when state = SendBRAMNextAddr else
			 "01000" when state = SaveNextValue else
			 "10000" when state = SaveOutput;	

end Behavioral;
